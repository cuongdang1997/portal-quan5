<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%
DateSearchEntry dateSearchEntry = new DateSearchEntry();

String displayStyle = workflowTaskDisplayContext.getDisplayStyle();
%>

<style scoped="scoped">
	.main-content-body {
		padding-left: 0px;
		padding-right: 0px;
		border: 1px solid #ddd;
	}
	
	.searchcontainer-content table {
		margin-bottom: 0 !important;
	}
	
	.searchcontainer-content table thead {
		background-color: #085791;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
		color: #fff;
	}
	
	.swt-workflow-task-mztop-15px {
		margin-top: -15px !important;
	}
	
	.swt-workflow-task-mztop5px {
		margin-bottom: 5px !important;
	}
	
	ul[id$='MyWorkflowTaskPortlet_navTag'] li {
		font-weight: bold;
		text-transform: uppercase;
	}
	
	.pagination-items-per-page{
		margin-left: 15px;
	}
</style>

<liferay-util:include page="/toolbar_custom.jsp" servletContext="<%= application %>" />

<div class="container-fluid-1280 main-content-body swt-workflow-task-mztop-15px">
	<c:choose>
		<c:when test="<%= workflowTaskDisplayContext.isAssignedToMeTabSelected() %>">

			<%
			WorkflowTaskSearch workflowTaskSearch = workflowTaskDisplayContext.getTasksAssignedToMe();
			%>

			<%@ include file="/workflow_tasks_custom.jspf" %>
		</c:when>
		<c:otherwise>

			<%
			WorkflowTaskSearch workflowTaskSearch = workflowTaskDisplayContext.getTasksAssignedToMyRoles();
			%>

			<%@ include file="/workflow_tasks_custom.jspf" %>
		</c:otherwise>
	</c:choose>
</div>