<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp"%>

<%
	String randomId = StringUtil.randomId();

	String redirect = ParamUtil.getString(request, "redirect");

	String backURL = ParamUtil.getString(request, "backURL", redirect);

	if (Validator.isNull(backURL)) {
		PortletURL renderURL = renderResponse.createRenderURL();

		backURL = renderURL.toString();
	}

	WorkflowTask workflowTask = workflowTaskDisplayContext.getWorkflowTask();

	long classPK = workflowTaskDisplayContext.getWorkflowContextEntryClassPK(workflowTask);

	WorkflowHandler<?> workflowHandler = workflowTaskDisplayContext.getWorkflowHandler(workflowTask);

	AssetRenderer<?> assetRenderer = workflowHandler.getAssetRenderer(classPK);

	AssetRendererFactory<?> assetRendererFactory = assetRenderer.getAssetRendererFactory();

	AssetEntry assetEntry = assetRendererFactory.getAssetEntry(workflowHandler.getClassName(),
			assetRenderer.getClassPK());

	String headerTitle = workflowTaskDisplayContext.getHeaderTitle(workflowTask);

	portletDisplay.setShowBackIcon(false);
	portletDisplay.setURLBack(backURL);

	renderResponse.setTitle(headerTitle);
%>

<style scoped="scoped">
	.portlet-content-container .container-fluid-1280, .portlet-content-container .container-fluid-1280 .lfr-asset-column-details {
		padding-left: 0 !important;
		padding-right: 0 !important;
	}

	.swt-workflow-task-btn-back {
		margin-bottom: 10px;
	}
	
	.task-panel-container > .panel {
		margin: 0 10px 10px 10px;
	}
	
	.task-panel-container .h4.panel-title {
		background-color: #085791;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
		color: #fff;
		font-weight: bold;
		text-transform: uppercase;
	}
	
	.control-label {
		font-weight: bold;
	}
	
	.task-content-title {
		margin-top: 0 !important;
	}
</style>

<div class="container-fluid-1280">
	<a href="<%=backURL%>" class="btn btn-default swt-workflow-task-btn-back" role="button"><i class="icon-chevron-left"></i> <liferay-ui:message key="back" /> </a>
	
	<aui:col cssClass="lfr-asset-column lfr-asset-column-details">
		<liferay-ui:error
			exception="<%=WorkflowTaskDueDateException.class%>"
			message="please-enter-a-valid-due-date" />

		<aui:fieldset-group markupView="lexicon">
			<aui:fieldset>
				<%
					request.removeAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
				%>

				<liferay-util:include page="/workflow_task_action.jsp" servletContext="<%=application%>" />

				<div class="row-fluid">
					<div class="col-md-6">
						<aui:field-wrapper label="assigned-to">
							<aui:fieldset>
								<div class="lfr-asset-assigned">
									<c:choose>
										<c:when test="<%=workflowTask.isAssignedToSingleUser()%>">
											
												<%=workflowTaskDisplayContext.getWorkflowTaskAssigneeUserName(workflowTask)%>
										</c:when>
										<c:otherwise>
												<%=workflowTaskDisplayContext.getWorkflowTaskUnassignedUserName()%>
										</c:otherwise>
									</c:choose>
								</div>
							</aui:fieldset>
						</aui:field-wrapper>
					</div>
					<div class="col-md-6">
						<aui:field-wrapper label="create-date">
							<aui:fieldset>
								<%=workflowTaskDisplayContext.getCreateDate(workflowTask)%>
							</aui:fieldset>
						</aui:field-wrapper>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="col-md-6">
						<aui:field-wrapper label="status">
							<aui:fieldset>
								<%=workflowTaskDisplayContext.getState(workflowTask)%>
							</aui:fieldset>
						</aui:field-wrapper>
					</div>
					<div class="col-md-6">
						<aui:field-wrapper label="due-date">
							<aui:fieldset>
								<%=workflowTaskDisplayContext.getDueDateString(workflowTask)%>
							</aui:fieldset>
						</aui:field-wrapper>
					</div>
				</div>

				<c:if test="<%=Validator.isNotNull(workflowTask.getDescription())%>">
					<div class="row-fluid">
						<div class="col-md-12">
							<aui:field-wrapper label="description">
								<aui:fieldset>
									<%=workflowTaskDisplayContext.getDescription(workflowTask)%>
								</aui:fieldset>
							</aui:field-wrapper>
						</div>
					</div>
				</c:if>
			</aui:fieldset>

			<liferay-ui:panel-container cssClass="task-panel-container" extended="<%=false%>">
				<c:if test="<%=assetRenderer != null%>">
					<liferay-ui:panel extended="<%=true%>" markupView="lexicon"
						title="<%=workflowTaskDisplayContext.getPreviewOfTitle(workflowTask)%>">
						<div class="task-content-actions">
							<liferay-ui:icon-list>
								<c:if test="<%=assetRenderer.hasViewPermission(permissionChecker)%>">
									<portlet:renderURL var="viewFullContentURL">
										<portlet:param name="mvcPath" value="/view_content_custom.jsp" />
										<portlet:param name="redirect" value="<%=currentURL%>" />

										<c:if test="<%=assetEntry != null%>">
											<portlet:param name="assetEntryId" value="<%=String.valueOf(assetEntry.getEntryId())%>" />
											<portlet:param name="assetEntryClassPK" value="<%=String.valueOf(assetEntry.getClassPK())%>" />
										</c:if>

										<portlet:param name="type" value="<%=assetRendererFactory.getType()%>" />
										<portlet:param name="showEditURL" value="<%=String.valueOf(workflowTaskDisplayContext.isShowEditURL(workflowTask))%>" />
										<portlet:param name="workflowTaskId" value="<%=String.valueOf(workflowTask.getWorkflowTaskId())%>" />
									</portlet:renderURL>

									<liferay-frontend:management-bar-button
										href="<%=assetRenderer.isPreviewInContext()
												? assetRenderer.getURLViewInContext(liferayPortletRequest, liferayPortletResponse, null)
												: viewFullContentURL.toString()%>"
										icon="view" label="view[action]" />

									<c:if test="<%=workflowTaskDisplayContext.hasViewDiffsPortletURL(workflowTask)%>">
										<liferay-frontend:management-bar-button 
											href="<%=workflowTaskDisplayContext.getTaglibViewDiffsURL(workflowTask)%>"
											icon="paste" label="compare" />
									</c:if>
								</c:if>

								<c:if test="<%=workflowTaskDisplayContext.hasEditPortletURL(workflowTask)%>">
									<c:choose>
										<c:when test="<%=assetRenderer.hasEditPermission(permissionChecker) 
											&& workflowTaskDisplayContext.isShowEditURL(workflowTask)%>">
											<liferay-frontend:management-bar-button
												href="<%=workflowTaskDisplayContext.getTaglibEditURL(workflowTask)%>"
												icon="pencil" label="edit" />
										</c:when>
										<c:when test="<%=assetRenderer.hasEditPermission(permissionChecker)
											&& !workflowTaskDisplayContext.isShowEditURL(workflowTask)
											&& !workflowTask.isCompleted()%>">
											<liferay-frontend:management-bar-button href=""
												icon="question-circle-full"
												label="please-assign-the-task-to-yourself-to-be-able-to-edit-the-content" />
										</c:when>
									</c:choose>
								</c:if>
							</liferay-ui:icon-list>
						</div>

						<h3 class="task-content-title">
							<%=workflowTaskDisplayContext.getTaskContentTitle(workflowTask)%>
						</h3>

						<liferay-ui:asset-display assetRenderer="<%=assetRenderer%>"
							template="<%=AssetRenderer.TEMPLATE_ABSTRACT%>" />
					</liferay-ui:panel>

					<liferay-ui:panel extended="<%=true%>" markupView="lexicon" title="comments">
						<liferay-ui:discussion assetEntryVisible="<%=false%>"
							className="<%=assetRenderer.getClassName()%>"
							classPK="<%=assetRenderer.getClassPK()%>"
							formName='<%="fm" + assetRenderer.getClassPK()%>'
							ratingsEnabled="<%=false%>" redirect="<%=currentURL%>"
							userId="<%=user.getUserId()%>" />
					</liferay-ui:panel>
				</c:if>

				<liferay-ui:panel markupView="lexicon" title="activities">
					<%
						List<WorkflowLog> workflowLogs = workflowTaskDisplayContext.getWorkflowLogs(workflowTask);
					%>

					<%@ include file="/workflow_logs_custom.jspf"%>
				</liferay-ui:panel>
			</liferay-ui:panel-container>
		</aui:fieldset-group>
	</aui:col>
</div>

<aui:script use="liferay-workflow-tasks">
	var onTaskClickFn = A.rbind('onTaskClick', Liferay.WorkflowTasks, '');

	Liferay.delegateClick('<portlet:namespace /><%= randomId %>taskAssignLink', onTaskClickFn);
</aui:script>