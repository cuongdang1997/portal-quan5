<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp"%>

<%
	String tabs1 = ParamUtil.getString(renderRequest, "tabs1", "assigned-to-me");

	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setParameter("mvcPath", "/view.jsp");
	portletURL.setParameter("tabs1", tabs1);

	// dung.nguyen: Add search URL.
	PortletURL searchURL = renderResponse.createActionURL();
	searchURL.setParameter("mvcPath", "/view.jsp");
	searchURL.setParameter("tabs1", tabs1);
	searchURL.setParameter("embedded", "true");
	searchURL.setParameter("usingStructurekey",String.valueOf(usingStructurekey));
	searchURL.setParameter("ddmStructureKey",String.valueOf(ddmStructureKey));
	searchURL.setParameter("usingMBDiscussion",String.valueOf(usingMBDiscussion));
%>

<style scoped="scoped">
	#_com_liferay_portal_workflow_task_web_portlet_MyWorkflowTaskPortlet_navTag li {
		margin-top: 7px;
	}
	
	#_com_liferay_portal_workflow_task_web_portlet_MyWorkflowTaskPortlet_navTag li.active a {
		background-color: inherit;
	}
	
	.management-bar-item-title {
		max-width: unset;
	}
</style>

<%--
<aui:nav-bar cssClass="collapse-basic-search" markupView="lexicon">
	<aui:nav cssClass="navbar-nav">
		<aui:nav-item label="my-workflow-tasks" selected="<%= true %>" />
	</aui:nav>

	<aui:nav-bar-search>
		<aui:form action="<%= portletURL.toString() %>" method="post" name="fm1">
			<liferay-ui:search-form
				page="/search.jsp"
				servletContext="<%= application %>"
			/>
		</aui:form>
	</aui:nav-bar-search>
</aui:nav-bar>
--%>

<aui:nav-bar cssClass="collapse-basic-search swt-workflow-task-mztop5px"
	markupView="lexicon">
	<aui:nav cssClass="nav-bar-workflow nav-tabs nav-tabs-default">
		<portlet:renderURL var="viewAssignedToMeURL">
			<portlet:param name="mvcPath" value="/view.jsp" />
			<portlet:param name="tabs1" value="assigned-to-me" />
			<portlet:param name="navigation" value="pending" />
		</portlet:renderURL>
		<aui:nav-item href="<%=viewAssignedToMeURL%>"
			label='<%=usingStructurekey ? "swt-workflowtask.assigned-to-me"
							: "swt-workflowtask.assigned-to-me-discussion"%>'
			selected='<%=tabs1.equals("assigned-to-me")%>' />

		<portlet:renderURL var="viewAssignedToMyRolesURL">
			<portlet:param name="mvcPath" value="/view.jsp" />
			<portlet:param name="tabs1" value="assigned-to-my-roles" />
			<portlet:param name="navigation" value="pending" />
		</portlet:renderURL>

		<aui:nav-item href="<%=viewAssignedToMyRolesURL%>"
			label="assigned-to-my-roles"
			selected='<%=tabs1.equals("assigned-to-my-roles")%>' />

		<!-- thinh.tran: di chuyen search o tren xuong duoi nay. -->
		<aui:nav-bar-search>
			<!-- dung.nguyen: Update search URL. -->
			<aui:form action="<%= searchURL.toString() %>" method="post" name="fm1">
				<liferay-ui:search-form
					page="/search_custom.jsp"
					servletContext="<%= application %>"
				/>
			</aui:form>
		</aui:nav-bar-search>
	</aui:nav>
</aui:nav-bar>

<liferay-frontend:management-bar includeCheckBox="<%=false%>">
	<%-- <liferay-frontend:management-bar-buttons>
		<c:if test="<%= !workflowTaskDisplayContext.isSearch() %>">
			<liferay-frontend:management-bar-display-buttons
				displayViews="<%= workflowTaskDisplayContext.getDisplayViews() %>"
				portletURL="<%= workflowTaskDisplayContext.getPortletURL() %>"
				selectedDisplayStyle="<%= workflowTaskDisplayContext.getDisplayStyle() %>"
			/>
		</c:if>
	</liferay-frontend:management-bar-buttons> --%>

	<liferay-frontend:management-bar-filters>
		<liferay-frontend:management-bar-navigation
			navigationKeys='<%=new String[] { "pending", "completed", "all" }%>'
			portletURL="<%=PortletURLUtil.clone(portletURL, liferayPortletResponse)%>" />

		<liferay-frontend:management-bar-sort
			orderByCol="<%=workflowTaskDisplayContext.getOrderByCol()%>"
			orderByType="<%=workflowTaskDisplayContext.getOrderByType()%>"
			orderColumns='<%=new String[] { "last-activity-date", "due-date" }%>'
			portletURL="<%=workflowTaskDisplayContext.getPortletURL()%>" />
	</liferay-frontend:management-bar-filters>
</liferay-frontend:management-bar>