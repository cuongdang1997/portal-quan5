package com.swt.portal.categorylist.portlet;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(
	factory = true,
	id = "com.swt.portal.categorylist.portlet.CategoryListConfiguration",
	localization = "content/Language"
)
public interface CategoryListConfiguration {

	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String vocabulary();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean showIcon();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean showDescription();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean showSequence();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean isHierarchical();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean usingSubVocabulary();
	
	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String subVocabulary();
	
}
