package com.swt.portal.categorylist.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.swt.portal.categorylist.model.CategoryConstants;
import com.swt.portal.categorylist.util.CategoryFileUtils;

@Component(
	configurationPid = "com.swt.portal.categorylist.portlet.CategoryListConfiguration",
	configurationPolicy = ConfigurationPolicy.OPTIONAL,
	immediate = true,
	property = {
		"javax.portlet.name=" + CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME
	},
	service = ConfigurationAction.class
)
public class CategoryListConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private LogService _log;

	private volatile CategoryListConfiguration _config;

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_config = ConfigurableUtil.createConfigurable(CategoryListConfiguration.class, properties);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		_log.log(LogService.LOG_INFO, CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME + " configuration action.");

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String vocabulary = ParamUtil.getString(actionRequest, "vocabulary");
		setPreference(actionRequest, "vocabulary", vocabulary);

		boolean showDescription = ParamUtil.getBoolean(actionRequest, "showDescription");
		setPreference(actionRequest, "showDescription", String.valueOf(showDescription));
		
		boolean showIcon = ParamUtil.getBoolean(actionRequest, "showIcon");
		setPreference(actionRequest, "showIcon", String.valueOf(showIcon));

		boolean showSequence = ParamUtil.getBoolean(actionRequest, "showSequence");
		setPreference(actionRequest, "showSequence", String.valueOf(showSequence));

		boolean isHierarchical = ParamUtil.getBoolean(actionRequest, "isHierarchical");
		setPreference(actionRequest, "isHierarchical", String.valueOf(isHierarchical));
		
		boolean usingSubVocabulary = ParamUtil.getBoolean(actionRequest, "usingSubVocabulary");
		setPreference(actionRequest, "usingSubVocabulary", String.valueOf(usingSubVocabulary));
		
		if(usingSubVocabulary) {
			String subVocabulary = ParamUtil.getString(actionRequest, "subVocabulary");
			if(subVocabulary.equals(vocabulary)) {
				SessionErrors.add(actionRequest, "unduplicatedVocabulary");
			}else {
				setPreference(actionRequest, "subVocabulary", subVocabulary);
			}
		}

		if (!Validator.isBlank(vocabulary)) {
			// Get the vocabulary ID by its name.
			long groupId = themeDisplay.getScopeGroupId();
			ServiceContext vocabularyServiceContext = ServiceContextFactory.getInstance(AssetVocabulary.class.getName(),
					actionRequest);
			long vocabularyId = -1;
			try {
				AssetVocabulary v = AssetVocabularyLocalServiceUtil.getGroupVocabulary(groupId, vocabulary);
				vocabularyId = v.getVocabularyId();
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			// If vocabulary does not exist, create it.
			if (vocabularyId == -1) {
				try {
					AssetVocabulary v = AssetVocabularyLocalServiceUtil.addVocabulary(themeDisplay.getUserId(), groupId,
							vocabulary, vocabularyServiceContext);
					vocabularyId = v.getVocabularyId();
				} catch (PortalException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "cannotAddVocabulary");
				}
			}

			// Create vocabulary icon.
			if (vocabularyId > 0) {
				// Delete icon file if requested.
				boolean isDeleteVocabularyIcon = ParamUtil.getBoolean(actionRequest, "isDeleteVocabularyIcon");
				if (isDeleteVocabularyIcon) {
					long existingIconId = ParamUtil.getLong(actionRequest, "existingVocabularyIconId");
					try {
						DLAppLocalServiceUtil.deleteFileEntry(existingIconId);
					} catch (PortalException e) {
						// Keep the file but don't use it anymore.
					}
				}

				// Get data of new icon file.
				UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
				File iconFile = uploadRequest.getFile("fileIcon");
				String iconName = uploadRequest.getFileName("fileIcon");
				String iconMimeType = uploadRequest.getContentType("fileIcon");
				long iconSize = uploadRequest.getSize("fileIcon");

				// The icon file size must not be greater than the predefined value.
				if (iconSize > CategoryConstants.ICON_FILE_SIZE_BYTES) {
					SessionErrors.add(actionRequest, "iconLessThan10kb");
				} else if (iconSize != 0) {
					// We cannot determine if icon is the existing or a new one. Thus, we will
					// remove (if any) and create the icon.
					long existingIconId = ParamUtil.getLong(actionRequest, "existingVocabularyIconId");
					DLFileEntry existingIcon = null;
					if (existingIconId != 0) {
						try {
							existingIcon = DLFileEntryLocalServiceUtil.getDLFileEntry(existingIconId);
						} catch (PortalException e) {
							// This exception will be thrown if the file does not exist.
							// In the case, just do nothing.
						}
					}

					boolean isIconChanged = false;

					// If the existing icon is different than the new one, delete it.
					if (existingIcon == null) {
						isIconChanged = true;
					} else {
						if (!existingIcon.getFileName().equals(CategoryFileUtils.getFileTile(vocabularyId, iconName))
								|| existingIcon.getSize() != iconSize) {
							isIconChanged = true;
							try {
								DLAppLocalServiceUtil.deleteFileEntry(existingIconId);
							} catch (PortalException e) {
								// Keep the file but don't use it anymore.
							}
						}
					}

					if (isIconChanged) {
						try {
							CategoryFileUtils.createOrUpdateFileEntry(vocabularyId, groupId,
									PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_PORTAL_DATA),
									PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_VOCABULARY), iconName,
									iconMimeType, new FileInputStream(iconFile), iconSize, actionRequest);
						} catch (Exception e) {
							SessionErrors.add(actionRequest, "cannotAddIcon");
							_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
						}
					}
				}
			}
		}

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		_log.log(LogService.LOG_INFO, CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME + " configuration include.");

		httpServletRequest.setAttribute(CategoryListConfiguration.class.getName(), _config);
		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}
}
