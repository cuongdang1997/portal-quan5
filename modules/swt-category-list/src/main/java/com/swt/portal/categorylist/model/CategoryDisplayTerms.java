package com.swt.portal.categorylist.model;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.util.ParamUtil;

public class CategoryDisplayTerms extends DisplayTerms {

	protected int status;
	protected String name;
	protected boolean includeChildren;

	public CategoryDisplayTerms(PortletRequest portletRequest) {
		super(portletRequest);

		status = ParamUtil.getInteger(portletRequest, "status");
		name = ParamUtil.getString(portletRequest, "name");
		includeChildren = ParamUtil.getBoolean(portletRequest, "includeChildren");
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isIncludeChildren() {
		return includeChildren;
	}

	public void setIncludeChildren(boolean includeChildren) {
		this.includeChildren = includeChildren;
	}

}
