package com.swt.portal.categorylist.model;

import java.util.List;

public interface CategorySearchUtil {

	List<CategoryDTO> search(long companyId, long groupId, String vocabulary, CategorySearchContainer searchContainer);
	
	List<CategoryDTO> search(long companyId, long groupId, String vocabulary, String name, int status,
			boolean includeChildren, String orderByCol, String orderByType);

	String getDeepLevelDisplay(int deepLevel);

}
