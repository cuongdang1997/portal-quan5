package com.swt.portal.categorylist.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanComparator;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * @see CategorySearchUtil
 * 
 * @author dung.nguyen@smartworld.com.vn
 *
 */
public class CategorySearchUtilImpl implements CategorySearchUtil {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.swt.portal.categorylist.model.CategorySearchUtil#search(long, long,
	 * java.lang.String, com.swt.portal.categorylist.model.CategorySearchContainer)
	 */
	@Override
	public List<CategoryDTO> search(long companyId, long groupId, String vocabulary,
			CategorySearchContainer searchContainer) {
		CategoryDisplayTerms searchTerms = (CategoryDisplayTerms) searchContainer.getSearchTerms();

		String orderByCol = searchContainer.getOrderByCol();
		String orderByType = searchContainer.getOrderByType();

		return search(companyId, groupId, vocabulary, searchTerms.getName(), searchTerms.getStatus(),
				searchTerms.isIncludeChildren(), orderByCol, orderByType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.swt.portal.categorylist.model.CategorySearchUtil#search(long, long,
	 * java.lang.String, java.lang.String, int, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoryDTO> search(long companyId, long groupId, String vocabulary, String name, int status,
			boolean includeChildren, String orderByCol, String orderByType) {
		List<CategoryDTO> result = new ArrayList<CategoryDTO>();

		if (vocabulary == null || vocabulary.trim().length() == 0) {
			return result;
		}

		// Get the vocabulary by its name.
		AssetVocabulary assetVocabulary = null;
		try {
			assetVocabulary = AssetVocabularyLocalServiceUtil.getGroupVocabulary(groupId, vocabulary.trim());
		} catch (PortalException e) {
			return result;
		}

		List<Long> categoryIds = searchCategoryIds(assetVocabulary.getVocabularyId(), name, status, orderByCol,
				orderByType);
		List<AssetCategory> categories = new ArrayList<>();

		if (categoryIds.size() != 0) {
			try {
				for (long cid : categoryIds) {
					AssetCategory c = AssetCategoryLocalServiceUtil.getAssetCategory(cid);
					categories.add(c);
				}
			} catch (PortalException e) {
				// Do nothing
			}
		}

		if (includeChildren) {
			categories = includeChildren(categories);
		}

		// Convert categories to desired type.
		AssetCategoryProperty tmp = null;
		for (AssetCategory c : categories) {
			CategoryDTO item = new CategoryDTO();
			item.setId(c.getCategoryId());
			item.setParentId(c.getParentCategoryId());
			item.setName(c.getName());
			item.setDescription(c.getDescription());

			// Get the status property.
			tmp = null;
			try {
				tmp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(c.getCategoryId(),
						CategoryConstants.CATEGORY_PROPERTY_STATUS);
			} catch (PortalException e) {
				// Do nothing
			}
			if (tmp != null) {

				if (tmp.getValue() != null && tmp.getValue().trim().equals("1")) {
					item.setStatus(CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE);
				} else {
					item.setStatus(CategoryConstants.CATEGORY_PROPERTY_STATUS_INACTIVE);
				}
			} else {
				item.setStatus(CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE);
			}

			// Get the sequence property.
			tmp = null;
			try {
				tmp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(c.getCategoryId(),
						CategoryConstants.CATEGORY_PROPERTY_SEQUENCE);
			} catch (PortalException e) {
				// Do nothing.
			}
			if (tmp != null && tmp.getValue() != null) {
				item.setSequence(GetterUtil.getInteger(tmp.getValue(), Integer.MAX_VALUE));
			} else {
				item.setSequence(Integer.MAX_VALUE);
			}

			// Get the icon property.
			tmp = null;
			try {
				tmp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(c.getCategoryId(),
						CategoryConstants.CATEGORY_PROPERTY_ICON);
			} catch (PortalException e) {
				// Do nothing.
			}
			if (tmp != null && tmp.getValue() != null) {
				item.setIconId(GetterUtil.getLong(tmp.getValue()));
			} else {
				item.setIconId(0L);
			}

			item.setCreateUser(c.getUserName());
			item.setCreateDate(c.getCreateDate());
			item.setModifiedDate(c.getModifiedDate());

			result.add(item);
		}

		if (orderByCol != null && orderByType != null) {
			BeanComparator comparator = new BeanComparator(orderByCol);
			if (orderByType.equalsIgnoreCase("asc")) {
				Collections.sort(result, comparator);
			} else {
				Collections.sort(result, Collections.reverseOrder(comparator));
			}
		}

		if (result.size() != 0) {
			long minParentId = Collections.min(result, Comparator.comparing(c -> c.getParentId())).getParentId();
			result = deepSort(result, minParentId, 0);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.swt.portal.categorylist.model.CategorySearchUtil#getDeepLevelDisplay(int)
	 */
	public String getDeepLevelDisplay(int deepLevel) {
		String display = GetterUtil.DEFAULT_STRING;

		for (int i = 0; i <= Integer.MAX_VALUE; i++) {
			if (i == deepLevel) {
				break;
			}
			display = display + StringPool.DOUBLE_DASH + StringPool.SPACE;
		}

		return display;
	}

	/**
	 * Sort items by their deep levels and return a hierarchy list.
	 * 
	 * @param items     The items to be sorted.
	 * @param parentId  The ID of top parent item or -1.
	 * @param deepLevel The deep level of top parent item or 0.
	 * 
	 * @return A hierarchy list.
	 */
	private List<CategoryDTO> deepSort(List<CategoryDTO> items, long parentId, int deepLevel) {
		List<CategoryDTO> remain = new ArrayList<>(items);
		List<CategoryDTO> result = new ArrayList<>();

		List<Long> ids = new ArrayList<>();
		for (CategoryDTO i : items) {
			ids.add(i.getId());
		}

		for (CategoryDTO i : items) {
			if (i.getParentId() == parentId) {
				i.setDeepLevel(deepLevel);
				if (!result.stream().anyMatch(c -> c.getId() == i.getId())) {
					result.add(i);
					remain.remove(i);
					result.addAll(deepSort(remain, i.getId(), deepLevel + 1));
				}
			}
		}

		return result;
	}

	/**
	 * Search category ID(s) by vocabulary ID, category name and category status.
	 * 
	 * @param vocabularyId The vocabulary ID
	 * @param name         The category name
	 * @param status       The category status
	 * @param orderByCol   The sorting column name
	 * @param orderByType  The sorting type
	 * 
	 * @return The list of category ID(s) that match criterias.
	 */
	private List<Long> searchCategoryIds(long vocabularyId, String name, int status, String orderByCol,
			String orderByType) {
		DynamicQuery queryCommon = DynamicQueryFactoryUtil.forClass(AssetCategory.class)
				.setProjection(ProjectionFactoryUtil.property("categoryId"))
				.add(PropertyFactoryUtil.forName("vocabularyId").eq(vocabularyId));
		if (name != null && name.trim().length() != 0) {
			queryCommon.add(PropertyFactoryUtil.forName("name").like("%" + name.trim() + "%"));
		}

		DynamicQuery queryByStatus = DynamicQueryFactoryUtil.forClass(AssetCategoryProperty.class);
		queryByStatus.add(PropertyFactoryUtil.forName("key").eq(String.valueOf("status")));
		if (status != 0) {
			queryByStatus.add(PropertyFactoryUtil.forName("value").eq(String.valueOf(status)));
		}
		queryByStatus.add(PropertyFactoryUtil.forName("categoryId").in(queryCommon));

		List<AssetCategoryProperty> properties = AssetCategoryPropertyLocalServiceUtil.dynamicQuery(queryByStatus);

		return properties.stream().map(AssetCategoryProperty::getCategoryId).collect(Collectors.toList());
	}

	/**
	 * Include all children to the origin list.
	 * 
	 * @param origin The origin categorys list
	 * 
	 * @return The list of origin categories including their children.
	 */
	private List<AssetCategory> includeChildren(List<AssetCategory> origin) {
		List<AssetCategory> categories = new ArrayList<>();

		for (AssetCategory o : origin) {
			if (categories.stream().anyMatch(c -> c.getCategoryId() == o.getCategoryId())) {
				continue;
			} else {
				categories.add(o);
			}

			List<AssetCategory> children = AssetCategoryLocalServiceUtil.getChildCategories(o.getCategoryId());
			categories.addAll(includeChildren(children));
		}

		return categories;
	}

}
