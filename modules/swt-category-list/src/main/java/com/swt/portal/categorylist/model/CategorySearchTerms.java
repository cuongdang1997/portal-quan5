package com.swt.portal.categorylist.model;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.util.ParamUtil;

public class CategorySearchTerms extends CategoryDisplayTerms {

	public CategorySearchTerms(PortletRequest portletRequest) {
		super(portletRequest);
		
		status = ParamUtil.getInteger(portletRequest, "status");
		name = ParamUtil.getString(portletRequest, "name");
		includeChildren = ParamUtil.getBoolean(portletRequest, "includeChildren");
	}

}
