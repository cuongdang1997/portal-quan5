package com.swt.portal.categorylist.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryConstants;
import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import com.swt.portal.categorylist.model.CategoryConstants;
import com.swt.portal.categorylist.util.CategoryFileUtils;

/**
 * @author Dung Nguyen
 */
@Component(configurationPid = "com.swt.portal.categorylist.portlet.CategoryListConfiguration", immediate = true, property = {
		"com.liferay.portlet.display-category=category.portal", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=" + CategoryConstants.SWT_CATEGORY_LIST_DISPLAY_NAME,
		"javax.portlet.init-param.config-template=/configuration.jsp", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class CategoryListPortlet extends MVCPortlet {

	@Reference
	private LogService _log;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet#doView(javax.portlet
	 * .RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.log(LogService.LOG_INFO,
				CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME + " " + CategoryConstants.ACTION_DO_VIEW);

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();

		try {
			renderRequest.setAttribute(CategoryListConfiguration.class.getName(),
					portletDisplay.getPortletInstanceConfiguration(CategoryListConfiguration.class));
		} catch (ConfigurationException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
		}

		String path = ParamUtil.getString(renderRequest, "path");
		if (path.equalsIgnoreCase(CategoryConstants.ACTION_ADD_CATEGORY)) {
			include("/addCategory.jsp", renderRequest, renderResponse);
		} else if (path.equalsIgnoreCase(CategoryConstants.ACTION_UPDATE_CATEGORY)) {
			include("/updateCategory.jsp", renderRequest, renderResponse);
		} else {
			super.doView(renderRequest, renderResponse);
		}
	}

	/**
	 * Add a new category.
	 * 
	 * @param actionRequest  The action request. Form parameters will be retrieved
	 *                       from this object.
	 * @param actionResponse The action response.
	 */
	@ProcessAction(name = CategoryConstants.ACTION_ADD_CATEGORY)
	public void addCategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException, PortalException {
		_log.log(LogService.LOG_INFO,
				CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME + " " + CategoryConstants.ACTION_ADD_CATEGORY);

		// Clear the previous messages.
		SessionErrors.clear(actionRequest);
		SessionMessages.clear(actionRequest);

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getLayout().getGroupId();

		ServiceContext vocabularyServiceContext = ServiceContextFactory.getInstance(AssetVocabulary.class.getName(),
				actionRequest);
		vocabularyServiceContext.setScopeGroupId(groupId);

		ServiceContext categoryServiceContext = ServiceContextFactory.getInstance(AssetCategory.class.getName(),
				actionRequest);

		// Get the current user.
		long createUserId = vocabularyServiceContext.getUserId();

		// Check if vocabulary has been configured.
		String vocabularyName = GetterUtil.getString(actionRequest.getParameter("vocabulary"), StringPool.BLANK).trim();
		if (vocabularyName.length() == 0) {
			SessionErrors.add(actionRequest, "vocabularyNotConfigured");
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
			return;
		}

		// The array of vocabulary Name(s).
		String[] vocabularyNames = null;
		List<AssetCategory> savedCategories = new ArrayList<>();

		// Check if sub vocabulary is enabled.
		boolean usingSubVocabulary = GetterUtil.getBoolean(actionRequest.getParameter("usingSubVocabulary"), false);

		if (usingSubVocabulary) {
			String subVocabularyName = GetterUtil
					.getString(actionRequest.getParameter("subVocabulary"), StringPool.BLANK).trim();
			if (subVocabularyName.length() == 0) {
				SessionErrors.add(actionRequest, "subVocabularyNotConfigured");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
				return;
			}

			vocabularyNames = new String[] { vocabularyName, subVocabularyName };
		} else {
			vocabularyNames = new String[] { vocabularyName };
		}

		for (String vName : vocabularyNames) {
			// Get the vocabulary ID by its name.
			long vocabularyId = -1;
			try {
				AssetVocabulary v = AssetVocabularyLocalServiceUtil.getGroupVocabulary(groupId, vName);
				vocabularyId = v.getVocabularyId();
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				// Do not return here. We will create vocabulary in below.
			}

			// If vocabulary does not exist, create it.
			if (vocabularyId == -1) {
				try {
					AssetVocabulary v = AssetVocabularyLocalServiceUtil.addVocabulary(createUserId, groupId, vName,
							vocabularyServiceContext);
					vocabularyId = v.getVocabularyId();

					cloneAllCategoryToSubCategory(savedCategories.get(0).getVocabularyId(), v.getVocabularyId(),
							categoryServiceContext);

				} catch (PortalException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "cannotAddVocabulary");
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
					return;
				}
			}

			// Check if category name is not blank.
			String name = GetterUtil.getString(actionRequest.getParameter("name"), StringPool.BLANK).trim();
			if (name.length() == 0) {
				SessionErrors.add(actionRequest, "categoryNameMustNotBlank");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
				return;
			}

			// Check if category name is available (i.e. has not been taken).
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(AssetCategory.class);
			Criterion criterion = null;
			criterion = RestrictionsFactoryUtil.ilike("name", name);
			criterion = RestrictionsFactoryUtil.and(criterion,
					RestrictionsFactoryUtil.eq("vocabularyId", vocabularyId));
			query.add(criterion);
			List<AssetCategory> matched = AssetCategoryLocalServiceUtil.dynamicQuery(query);
			if (matched != null && matched.size() != 0) {
				SessionErrors.add(actionRequest, "categoryNameAlreadyExist");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
				return;
			}

			// Prepare category data for adding.
			Map<Locale, String> titleMap = new HashMap<>();
			titleMap.put(themeDisplay.getLocale(), name);

			// The title of default locale will be used for category name.
			// Thus, it will throw exception if there's no title set for default locale.
			if (!themeDisplay.getSiteDefaultLocale().getLanguage().equals(themeDisplay.getLocale().getLanguage())) {
				titleMap.put(themeDisplay.getSiteDefaultLocale(), name);
			}

			Map<Locale, String> descriptionMap = new HashMap<>();
			String description = GetterUtil.getString(actionRequest.getParameter("description"), StringPool.BLANK)
					.trim();
			descriptionMap.put(themeDisplay.getLocale(), description);

			String[] categoryProperties = new String[3];
			categoryProperties[0] = CategoryConstants.CATEGORY_PROPERTY_STATUS
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR
					+ String.valueOf(CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE);
			int sequence = GetterUtil.getInteger(actionRequest.getParameter("sequence"), 1);
			categoryProperties[1] = CategoryConstants.CATEGORY_PROPERTY_SEQUENCE
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR + String.valueOf(sequence);

			categoryProperties[2] = CategoryConstants.CATEGORY_PROPERTY_ICON
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR + String.valueOf(GetterUtil.DEFAULT_LONG);

			long parentId = GetterUtil.getLong(actionRequest.getParameter("parentId"), 0L);

			// Lay ra chuyen muc cha cua chuyen muc phu
			if (parentId > 0 && usingSubVocabulary) {
				try {
					AssetCategory ac = AssetCategoryLocalServiceUtil.getAssetCategory(parentId);
					DynamicQuery querySubCa = DynamicQueryFactoryUtil.forClass(AssetCategory.class);
					Criterion criterionSubCa = null;
					criterionSubCa = RestrictionsFactoryUtil.eq("name", ac.getName());
					criterionSubCa = RestrictionsFactoryUtil.and(criterionSubCa,
							RestrictionsFactoryUtil.eq("vocabularyId", vocabularyId));
					querySubCa.add(criterionSubCa);
					List<AssetCategory> matchedSub = AssetCategoryLocalServiceUtil.dynamicQuery(querySubCa);
					if (matchedSub != null && matchedSub.size() > 0) {
						parentId = matchedSub.get(0).getCategoryId();// only one category by name
					}
				} catch (PortalException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "notFoundCategory");
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
					return;
				}
			}

			// Create the category.
			AssetCategory savedCategory = null;
			try {
				savedCategory = AssetCategoryLocalServiceUtil.addCategory(createUserId, groupId, parentId, titleMap,
						descriptionMap, vocabularyId, categoryProperties, categoryServiceContext);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				SessionErrors.add(actionRequest, "cannotAddCategory");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_ADD_CATEGORY);
				return;
			}

			// Create file entry for category icon.
			if (savedCategory != null) {
				createOrUpdateIconProperty(actionRequest, groupId, savedCategory.getCategoryId(),
						GetterUtil.DEFAULT_LONG);
			}

			savedCategories.add(savedCategory);
		}

		SessionMessages.add(actionRequest, "categoryAdded");
		PortalUtil.copyRequestParameters(actionRequest, actionResponse);
		actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
		actionResponse.setRenderParameter("catId", String.valueOf(savedCategories.get(0).getCategoryId()));
		actionResponse.setRenderParameter("status", String.valueOf(CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE));
		actionResponse.setRenderParameter("updated", String.valueOf(true));
	}

	/**
	 * Update an existing category.
	 * 
	 * @param actionRequest  The action request. Form parameters will be retrieved
	 *                       from this object.
	 * @param actionResponse The action response.
	 */
	@ProcessAction(name = CategoryConstants.ACTION_UPDATE_CATEGORY)
	public void updateCategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		_log.log(LogService.LOG_INFO,
				CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME + " " + CategoryConstants.ACTION_UPDATE_CATEGORY);

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getLayout().getGroupId();

		// Clear the previous messages.
		SessionErrors.clear(actionRequest);
		SessionMessages.clear(actionRequest);

		// Get the category by ID.
		long categoryId = GetterUtil.getLong(actionRequest.getParameter("catId"), 0);
		AssetCategory category = null;
		try {
			category = AssetCategoryLocalServiceUtil.getAssetCategory(categoryId);
		} catch (PortalException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			SessionErrors.add(actionRequest, "categoryNotFound");
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
			return;
		}

		ServiceContext vocabularyServiceContext = null;
		ServiceContext categoryServiceContext = null;
		try {
			vocabularyServiceContext = ServiceContextFactory.getInstance(AssetVocabulary.class.getName(),
					actionRequest);
			categoryServiceContext = ServiceContextFactory.getInstance(AssetCategory.class.getName(), actionRequest);
		} catch (PortalException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
		}

		AssetCategory[] categorys = null;

		// Check if sub vocabulary is enabled.
		boolean usingSubVocabulary = GetterUtil.getBoolean(actionRequest.getParameter("usingSubVocabulary"), false);

		if (usingSubVocabulary) {
			String subVocabularyName = GetterUtil
					.getString(actionRequest.getParameter("subVocabulary"), StringPool.BLANK).trim();
			if (subVocabularyName.length() == 0) {
				SessionErrors.add(actionRequest, "subVocabularyNotConfigured");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
				return;
			}

			AssetVocabulary subVocabulary = null;
			try {
				subVocabulary = AssetVocabularyLocalServiceUtil.getGroupVocabulary(groupId, subVocabularyName);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			// Lan dau tien thi clone tat cac cac chuyen muc hien co qua lam chuyen muc phu
			if (subVocabulary == null) {
				try {
					subVocabulary = AssetVocabularyLocalServiceUtil.addVocabulary(themeDisplay.getUserId(), groupId,
							subVocabularyName, vocabularyServiceContext);
				} catch (PortalException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "cannotAddSubVocabulary");
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
					return;
				}

				cloneAllCategoryToSubCategory(category.getVocabularyId(), subVocabulary.getVocabularyId(),
						categoryServiceContext);
			}

			AssetCategory subCategory = null;
			if (category != null) {
				try {
					DynamicQuery querySubCa = DynamicQueryFactoryUtil.forClass(AssetCategory.class);
					Criterion criterionSubCa = null;
					criterionSubCa = RestrictionsFactoryUtil.eq("name", category.getName().trim());
					criterionSubCa = RestrictionsFactoryUtil.and(criterionSubCa,
							RestrictionsFactoryUtil.eq("vocabularyId", subVocabulary.getVocabularyId()));
					querySubCa.add(criterionSubCa);
					List<AssetCategory> matchedSubCategory = AssetCategoryLocalServiceUtil.dynamicQuery(querySubCa);
					if (matchedSubCategory != null && matchedSubCategory.size() > 0) {
						subCategory = matchedSubCategory.get(0);// only one category by name in a vocabulary
					}
				} catch (Exception e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "subCategoryNotFound");
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
					return;
				}
			}

			categorys = new AssetCategory[] { category, subCategory };
		} else {
			categorys = new AssetCategory[] { category };
		}

		for (AssetCategory ac : categorys) {
			// Check if category name is available (i.e. has not been taken).
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(AssetCategory.class);
			String name = GetterUtil.getString(actionRequest.getParameter("name"), StringPool.BLANK);
			Criterion criterion = RestrictionsFactoryUtil.ilike("name", name);
			criterion = RestrictionsFactoryUtil.and(criterion,
					RestrictionsFactoryUtil.eq("vocabularyId", ac.getVocabularyId()));
			criterion = RestrictionsFactoryUtil.and(criterion,
					RestrictionsFactoryUtil.ne("categoryId", ac.getCategoryId()));
			query.add(criterion);

			List<AssetCategory> matched = AssetCategoryLocalServiceUtil.dynamicQuery(query);

			if (matched != null && matched.size() != 0) {
				SessionErrors.add(actionRequest, "categoryNameAlreadyExist");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
				return;
			}

			// Prepare category data for update.
			if (name == null || name.trim().length() == 0) {
				SessionErrors.add(actionRequest, "categoryNameMustNotBlank");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
				return;
			}
			name = name.trim();

			Map<Locale, String> titleMap = new HashMap<>();
			titleMap.put(themeDisplay.getLocale(), name);

			Map<Locale, String> descriptionMap = new HashMap<>();
			String description = GetterUtil.getString(actionRequest.getParameter("description"), StringPool.BLANK);
			descriptionMap.put(themeDisplay.getLocale(), description == null ? StringPool.BLANK : description.trim());

			String[] categoryProperties = new String[3];
			int status = GetterUtil.getInteger(actionRequest.getParameter("status"),
					CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE);
			categoryProperties[0] = CategoryConstants.CATEGORY_PROPERTY_STATUS
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR + String.valueOf(status);
			int sequence = GetterUtil.getInteger(actionRequest.getParameter("sequence"), 1);
			categoryProperties[1] = CategoryConstants.CATEGORY_PROPERTY_SEQUENCE
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR + String.valueOf(sequence);

			long iconId = ParamUtil.getLong(actionRequest, "iconId");
			boolean isDeleteIcon = ParamUtil.getBoolean(actionRequest, "isDeleteIcon", false);

			// Delete icon file if requested.
			if (isDeleteIcon) {
				try {
					DLAppLocalServiceUtil.deleteFileEntry(iconId);
					iconId = GetterUtil.DEFAULT_LONG;
				} catch (PortalException e) {
					// Keep the file but don't use it anymore.
				}
			}

			// Update icon property.
			iconId = createOrUpdateIconProperty(actionRequest, themeDisplay.getScopeGroupId(), ac.getCategoryId(),
					iconId);
			categoryProperties[2] = CategoryConstants.CATEGORY_PROPERTY_ICON
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR + String.valueOf(iconId);

			// Update category.
			long parentId = GetterUtil.getLong(actionRequest.getParameter("parentId"), 0L);

			// Lay ra chuyen muc cha cua chuyen muc phu
			if (parentId > 0 && usingSubVocabulary) {
				try {
					AssetCategory ca = AssetCategoryLocalServiceUtil.getAssetCategory(parentId);
					DynamicQuery querySubCa = DynamicQueryFactoryUtil.forClass(AssetCategory.class);
					Criterion criterionSubCa = null;
					criterionSubCa = RestrictionsFactoryUtil.eq("name", ca.getName());
					criterionSubCa = RestrictionsFactoryUtil.and(criterionSubCa,
							RestrictionsFactoryUtil.eq("vocabularyId", ac.getVocabularyId()));
					querySubCa.add(criterionSubCa);
					List<AssetCategory> matchedSub = AssetCategoryLocalServiceUtil.dynamicQuery(querySubCa);
					if (matchedSub != null && matchedSub.size() > 0) {
						parentId = matchedSub.get(0).getCategoryId();// only one category by name
					}
				} catch (PortalException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "notFoundCategory");
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
					return;
				}
			}

			try {
				AssetCategoryLocalServiceUtil.updateCategory(categoryServiceContext.getUserId(), ac.getCategoryId(),
						parentId, titleMap, descriptionMap, ac.getVocabularyId(), categoryProperties,
						categoryServiceContext);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				SessionErrors.add(actionRequest, "cannotUpdateCategory");
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
				return;
			}
		}

		SessionMessages.add(actionRequest, "categoryUpdated");
		PortalUtil.copyRequestParameters(actionRequest, actionResponse);
		actionResponse.setRenderParameter("path", CategoryConstants.ACTION_UPDATE_CATEGORY);
		actionResponse.setRenderParameter("updated", String.valueOf(true));
	}

	/**
	 * Remove an existing category.
	 * 
	 * @param actionRequest  The action request.
	 * @param actionResponse The action response.
	 */
	@ProcessAction(name = CategoryConstants.ACTION_REMOVE_CATEGORY)
	public void removeCategory(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		_log.log(LogService.LOG_INFO,
				CategoryConstants.SWT_CATEGORY_LIST_PORTLET_NAME + " " + CategoryConstants.ACTION_REMOVE_CATEGORY);

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getLayout().getGroupId();

		// Clear the previous messages.
		SessionErrors.clear(actionRequest);
		SessionMessages.clear(actionRequest);

		long categoryId = GetterUtil.getLong(actionRequest.getParameter("categoryId"), GetterUtil.DEFAULT_LONG);

		AssetCategory mainCategory = null;
		try {
			mainCategory = AssetCategoryLocalServiceUtil.getAssetCategory(categoryId);
		} catch (PortalException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			SessionErrors.add(actionRequest, "notFoundCategory");
		}

		long[] categoryIds = null;

		boolean usingSubVocabulary = GetterUtil.getBoolean(actionRequest.getParameter("usingSubVocabulary"), false);

		if (usingSubVocabulary) {
			String subVocabularyName = GetterUtil.getString(actionRequest.getParameter("subVocabulary"),
					GetterUtil.DEFAULT_STRING);

			if (subVocabularyName.length() == 0) {
				SessionErrors.add(actionRequest, "subVocabularyNotConfigured");
				return;
			}

			ServiceContext categoryServiceContext = null;
			ServiceContext vocabularyServiceContext = null;

			try {
				categoryServiceContext = ServiceContextFactory.getInstance(AssetCategory.class.getName(),
						actionRequest);

				vocabularyServiceContext = ServiceContextFactory.getInstance(AssetVocabulary.class.getName(),
						actionRequest);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			AssetVocabulary subVocabulary = null;
			try {
				subVocabulary = AssetVocabularyLocalServiceUtil.getGroupVocabulary(groupId, subVocabularyName);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			if (subVocabulary == null) {
				try {
					subVocabulary = AssetVocabularyLocalServiceUtil.addVocabulary(themeDisplay.getUserId(), groupId,
							subVocabularyName, vocabularyServiceContext);

				} catch (PortalException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					SessionErrors.add(actionRequest, "cannotAddSubVocabulary");
					return;
				}

				cloneAllCategoryToSubCategory(mainCategory.getVocabularyId(), subVocabulary.getVocabularyId(),
						categoryServiceContext);
			}

			long subCategoryId = 0;
			try {
				DynamicQuery querySubCa = DynamicQueryFactoryUtil.forClass(AssetCategory.class);
				Criterion criterionSubCa = null;
				criterionSubCa = RestrictionsFactoryUtil.eq("name", mainCategory.getName());
				criterionSubCa = RestrictionsFactoryUtil.and(criterionSubCa,
						RestrictionsFactoryUtil.eq("vocabularyId", subVocabulary.getVocabularyId()));
				querySubCa.add(criterionSubCa);
				List<AssetCategory> matchedSub = AssetCategoryLocalServiceUtil.dynamicQuery(querySubCa);
				if (matchedSub != null && matchedSub.size() > 0) {
					subCategoryId = matchedSub.get(0).getCategoryId();// only one category by name
				}
			} catch (Exception e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				SessionErrors.add(actionRequest, "notFoundCategory");
				return;
			}

			categoryIds = new long[] { categoryId, subCategoryId };
		} else {
			categoryIds = new long[] { categoryId };
		}

		for (long id : categoryIds) {
			// Get the icon property of given category.
			try {
				AssetCategoryProperty subIconProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(id,
						CategoryConstants.CATEGORY_PROPERTY_ICON);

				if (GetterUtil.getLong(subIconProp.getValue()) > 0) {
					try {
						DLAppLocalServiceUtil.deleteFileEntry(GetterUtil.getLong(subIconProp.getValue()));
					} catch (PortalException e) {
						// Keep the file but don't use it anymore.
						_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					}
				}

			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			try {
				AssetCategoryLocalServiceUtil.deleteCategories(new long[] { id });
				SessionMessages.add(actionRequest, "categoryRemoved");
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				SessionErrors.add(actionRequest, "cannotRemoveCategory");
			}
		}
	}

	/**
	 * Create or update category icon property.
	 * 
	 * @param actionRequest  The action request.
	 * @param groupId        The group ID.
	 * @param categoryId     The target category ID.
	 * @param existingIconId The existing icon file entry ID (for the case of
	 *                       update) or GetterUtil.DEFAULT_LONG.
	 * 
	 * @return The icon file entry ID or 0.
	 */
	private long createOrUpdateIconProperty(ActionRequest actionRequest, long groupId, long categoryId,
			long existingIconId) {
		// Set default return value.
		long iconId = GetterUtil.DEFAULT_LONG;

		// Get the icon property of given category.
		AssetCategoryProperty iconProp = null;
		try {
			iconProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(categoryId,
					CategoryConstants.CATEGORY_PROPERTY_ICON);
		} catch (PortalException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			return iconId;
		}

		// Get icon information from request.
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		File iconFile = uploadPortletRequest.getFile("fileIcon");
		String iconName = uploadPortletRequest.getFileName("fileIcon");
		String iconMimeType = uploadPortletRequest.getContentType("fileIcon");
		long iconSize = uploadPortletRequest.getSize("fileIcon");

		// Create file entry for icon.
		if (Validator.isNotNull(iconFile) && !Validator.isBlank(iconName.trim()) && !Validator.isBlank(iconMimeType)) {
			FileEntry iconFileEntry = null;
			if (iconSize <= CategoryConstants.ICON_FILE_SIZE_BYTES) {
				try {
					iconFileEntry = CategoryFileUtils.createOrUpdateFileEntry(categoryId, groupId,
							PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_PORTAL_DATA),
							PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_CATEGORY), iconName, iconMimeType,
							new FileInputStream(iconFile), iconSize, actionRequest);
				} catch (PortalException | FileNotFoundException e) {
					SessionErrors.add(actionRequest, "cannotAddIcon");
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					return iconId;
				}
			} else {
				SessionErrors.add(actionRequest, "iconLessThan10kb");
				return iconId;
			}

			// Update icon ID back to category property.
			if (iconFileEntry != null) {
				iconId = iconFileEntry.getFileEntryId();
				if (iconProp != null) {
					iconProp.setValue(String.valueOf(iconFileEntry.getFileEntryId()));
					AssetCategoryPropertyLocalServiceUtil.updateAssetCategoryProperty(iconProp);
					iconId = iconFileEntry.getFileEntryId();
				}
			}
		} else {
			iconId = existingIconId;
		}

		return iconId;
	}

	private void cloneAllCategoryToSubCategory(long mainVocabulary, long subVocabularyId,
			ServiceContext serviceContext) {
		try {
			AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(mainVocabulary);
			for (AssetCategory ac : assetVocabulary.getCategories()) {
				if (ac.getParentCategoryId() == 0) {
					addSubCategory(ac, subVocabularyId, GetterUtil.DEFAULT_LONG, serviceContext);
				}
			}
		} catch (PortalException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
		}
	}

	private void addSubCategory(AssetCategory prCategory, long subVocabularyId, long prCategoryId,
			ServiceContext serviceContext) {
		String[] categoryProperties = new String[3];

		AssetCategory subParentCategory = null;
		try {
			AssetCategoryProperty statusProp = null;
			AssetCategoryProperty sequenceProp = null;
			AssetCategoryProperty iconProp = null;

			try {
				statusProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(prCategory.getCategoryId(),
						CategoryConstants.CATEGORY_PROPERTY_STATUS);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			try {
				sequenceProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(prCategory.getCategoryId(),
						CategoryConstants.CATEGORY_PROPERTY_SEQUENCE);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			try {
				iconProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(prCategory.getCategoryId(),
						CategoryConstants.CATEGORY_PROPERTY_ICON);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			categoryProperties[0] = CategoryConstants.CATEGORY_PROPERTY_STATUS
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR
					+ (statusProp == null ? String.valueOf(CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE)
							: statusProp.getValue());
			categoryProperties[1] = CategoryConstants.CATEGORY_PROPERTY_SEQUENCE
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR
					+ (sequenceProp == null ? String.valueOf(1) : sequenceProp.getValue());
			categoryProperties[2] = CategoryConstants.CATEGORY_PROPERTY_ICON
					+ AssetCategoryConstants.PROPERTY_KEY_VALUE_SEPARATOR
					+ (iconProp == null ? String.valueOf(0) : iconProp.getValue());

			// ADD parent category
			subParentCategory = AssetCategoryLocalServiceUtil.addCategory(prCategory.getUserId(),
					prCategory.getGroupId(), prCategoryId, prCategory.getTitleMap(), prCategory.getDescriptionMap(),
					subVocabularyId, categoryProperties, serviceContext);
		} catch (PortalException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
		}

		// Add ChildCategories
		List<AssetCategory> categories = AssetCategoryLocalServiceUtil.getChildCategories(prCategory.getCategoryId());

		if (categories != null && categories.size() > 0 && subParentCategory != null) {
			for (AssetCategory ac : categories) {
				addSubCategory(ac, subVocabularyId, subParentCategory.getCategoryId(), serviceContext);
			}
		}
	}
}
