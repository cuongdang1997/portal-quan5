package com.swt.portal.categorylist.model;

/**
 * The constants to be used in this portlet.
 * 
 * @author Dung Nguyen
 */
public class CategoryConstants {

	/**
	 * Portlet unique name.
	 */
	public static final String SWT_CATEGORY_LIST_PORTLET_NAME = "SWTCategoryList";

	/**
	 * Portlet display name.
	 */
	public static final String SWT_CATEGORY_LIST_DISPLAY_NAME = "HCMGov Category List";

	/**
	 * The status property name of category.
	 */
	public static final String CATEGORY_PROPERTY_STATUS = "status";

	/**
	 * The "all" status of category (just using in search function, do not assign to
	 * category status).
	 */
	public static final int CATEGORY_PROPERTY_STATUS_ALL = 0;

	/**
	 * The "active" status of category.
	 */
	public static final int CATEGORY_PROPERTY_STATUS_ACTIVE = 1;

	/**
	 * The "inactive" status of category.
	 */
	public static final int CATEGORY_PROPERTY_STATUS_INACTIVE = 2;

	/**
	 * The sequence property name of category.
	 */
	public static final String CATEGORY_PROPERTY_SEQUENCE = "sequence";

	/**
	 * The icon property name of category.
	 */
	public static final String CATEGORY_PROPERTY_ICON = "icon";

	/**
	 * The rendering method name of portlet. To be shared between portlet and view
	 * sources.
	 */
	public static final String ACTION_DO_VIEW = "doView";

	/**
	 * The "add category" method name of porlet. To be shared between portlet and
	 * view sources.
	 */
	public static final String ACTION_ADD_CATEGORY = "addCategory";

	/**
	 * The "update category" method name of portlet. To be shared between portlet
	 * and view sources.
	 */
	public static final String ACTION_UPDATE_CATEGORY = "updateCategory";

	/**
	 * The "remove category" method name of portlet. To be shared between portlet
	 * and view sources.
	 */
	public static final String ACTION_REMOVE_CATEGORY = "removeCategory";

	/**
	 * The preference key of portal data folder.
	 */
	public static final String PREFKEY_FOLDER_PORTAL_DATA = "hcm.folders.portal-data";
	
	/**
	 * The preference key of category folder. 
	 */
	public static final String PREFKEY_FOLDER_CATEGORY = "hcm.folders.portal-data.category";
	
	public static final String PREFKEY_FOLDER_VOCABULARY = "hcm.folders.portal-data.vocabulary";
	
	/**
	 * The maximum size (in bytes) of icon file.
	 */
	public static final int ICON_FILE_SIZE_BYTES = 10 * 1024;
	
}