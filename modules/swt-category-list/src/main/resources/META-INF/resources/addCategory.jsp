<%@ include file="init.jsp"%>

<style scoped>
	#category-button-row {
		position: absolute;
	    bottom: 100%;
	    top: 90%;
	    right: 0;
	    margin-right: 3%;
	}
</style>

<%
	String iconId = request.getParameter("iconId");
	String urlIcon = StringPool.BLANK;
	if (GetterUtil.getLong(iconId) != 0) {
		urlIcon = CategoryFileUtils.getFileUrl(GetterUtil.getLong(iconId));
	}
%>

<portlet:actionURL var="addCategoryURL" name="<%=CategoryConstants.ACTION_ADD_CATEGORY%>">
	<portlet:param name="vocabulary" value="<%=vocabulary%>" />
	<portlet:param name="usingSubVocabulary" value="<%=String.valueOf(usingSubVocabulary)%>" />
	<portlet:param name="subVocabulary" value="<%=subVocabulary%>" />
</portlet:actionURL>

<liferay-ui:error key="cannotAddCategory" message="categorylist.cannot-save-data" />
<liferay-ui:error key="categoryNameMustNotBlank" message="categorylist.name-must-not-blank" />
<liferay-ui:error key="categoryNameAlreadyExist" message="categorylist.name-already-exist" />
<liferay-ui:error key="cannotGetVocabulary" message="that-vocabulary-does-not-exist" />
<liferay-ui:error key="cannotAddVocabulary" message="categorylist.cannot-add-vocabulary" />
<liferay-ui:error key="vocabularyNotConfigured" message="categorylist.vocabulary-has-not-configured" />
<liferay-ui:error key="subVocabularyNotConfigured" message="categorylist.sub-vocabulary-has-not-configured" />
<liferay-ui:error key="cannotAddSubVocabulary" message="categorylist.cannot-add-sub-vocabulary" />
<liferay-ui:error key="notFoundCategory" message="categorylist.not-found-category" />

<liferay-ui:success key="categoryAdded" message="successfully-saved" />
<liferay-ui:success key="categoryUpdated" message="successfully-saved" />

<div class="container-fluid">
	<aui:form name="<portlet:namespace />fmAdd" action="<%=addCategoryURL.toString()%>" method="POST" enctype="multipart/form-data">
		<fieldset>
			<% if (isHierarchical) { %>
				<aui:select name="parentId" value="0"
					label='<%=LanguageUtil.format(locale, "parent", dummyObj)%>'>
					<aui:option value="0"><%="--- " + LanguageUtil.format(locale, "select", dummyObj) + " ---"%></aui:option>
					<% List<CategoryDTO> items = searchUtil.search(themeDisplay.getLayout().getCompanyId(),
						themeDisplay.getLayout().getGroupId(), vocabulary, null, 0, true, null, null); %>
					<% for (CategoryDTO i : items) { %>
						<aui:option value="<%=i.getId()%>"><%=searchUtil.getDeepLevelDisplay(i.getDeepLevel()) + i.getName()%></aui:option>
					<% } %>
				</aui:select>
			<% } %>
			
			<aui:input name="name" type="text" max="75"
				label='<%=LanguageUtil.format(locale, "name", dummyObj)%>' >
				<aui:validator name="required" errorMessage='<%=LanguageUtil.format(locale, "this-field-is-required", dummyObj)%>' />
			</aui:input>
			
			<% if (showDescription) { %>
				<aui:input name="description" type="textarea" rows="3" 
					label='<%=LanguageUtil.format(locale, "description", dummyObj)%>' />
			<% } %>
			
			<% if (showSequence) { %>
				<aui:input name="sequence" type="number" value="1" min="1"
					label='<%=LanguageUtil.format(locale, "priority", dummyObj)%>' >
					<aui:validator name="min" errorMessage='<%=LanguageUtil.format(locale, "categorylist.validate-min-value", 1)%>'>1</aui:validator>
				</aui:input>
			<% } %>
			
			<% if (showIcon) { %>
				<aui:input type="file" name="fileIcon" label="icon" />
			<% } %>
		</fieldset>
		
		<div id="category-button-row" class="row-fluid">
			<aui:button type="submit" name="submit" />
			<aui:button type="button" name="cancel" value='<%=LanguageUtil.format(locale, "cancel", dummyObj)%>' />
		</div>
	</aui:form>
</div>

<aui:script use="aui-base">
	A.one('#<portlet:namespace/>cancel').on('click', function(event) {
	    var data = '';
		Liferay.Util.getOpener().<portlet:namespace/>closeCategoryDialog(data, '<portlet:namespace/>dlgCategory');
	});
</aui:script>

<aui:script>
	function hiddenIcon(id){
		$('.category-icon-display').css('display', 'none');
		$('.icon-category-id').val(id);
	}
</aui:script>