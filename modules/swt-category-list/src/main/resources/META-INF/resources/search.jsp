<%@ include file="init.jsp"%>

<%
	CategorySearchContainer searchContainer = (CategorySearchContainer) request
			.getAttribute("liferay-ui:search:searchContainer");
	CategoryDisplayTerms displayTerms = (CategoryDisplayTerms) searchContainer.getDisplayTerms();
%>

<style scoped>
	.location-btn-search-and-reset{
	    margin-top: 24px;
	}
</style>

<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<aui:input name="name" value="<%=displayTerms.getName()%>"
					label='<%=LanguageUtil.format(locale, "name", dummyObj)%>'>
				</aui:input>
			</div>
			<div class="col-md-4">
				<aui:select name="status"> value="<%=displayTerms.getStatus()%>">
					<aui:option
						value="<%=CategoryConstants.CATEGORY_PROPERTY_STATUS_ALL%>"
						label='<%="--- " + LanguageUtil.format(locale, "select", dummyObj) + " ---"%>'></aui:option>
					<aui:option
						value="<%=CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE%>"
						label='<%=LanguageUtil.format(locale, "active", dummyObj)%>'></aui:option>
					<aui:option
						value="<%=CategoryConstants.CATEGORY_PROPERTY_STATUS_INACTIVE%>"
						label='<%=LanguageUtil.format(locale, "inactive", dummyObj)%>'></aui:option>
				</aui:select>
			</div>
			<div class="col-md-4 location-btn-search-and-reset">
				<aui:button type="submit" name="search" value='<%=LanguageUtil.format(locale, "categorylist.search", dummyObj)%>' />
				<aui:button type="button" name="reset" value='<%=LanguageUtil.format(locale, "reset", dummyObj)%>' />
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<aui:input name="includeChildren" type="checkbox" value="<%=displayTerms.isIncludeChildren()%>"
					label='<%=LanguageUtil.format(locale, "categorylist.always-show-children-in-search-result", dummyObj)%>'>
				</aui:input>
			</div>
		</div>
	</div>
</div>

<aui:script>
	$("#<%=renderResponse.getNamespace()%>reset").on("click", function() {
		$("#<%=renderResponse.getNamespace()%>status").val("<%=CategoryConstants.CATEGORY_PROPERTY_STATUS_ALL%>");
		$("#<%=renderResponse.getNamespace()%>name").val("");
		$("#<%=renderResponse.getNamespace()%>includeChildren").prop("checked", false);
		$("#<%=renderResponse.getNamespace()%>search").trigger("click");
	});
</aui:script>