<%@page import="com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.document.library.kernel.model.DLFileEntry"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>

<%@page import="com.swt.portal.categorylist.model.CategoryConstants"%>
<%@page import="com.swt.portal.categorylist.model.CategoryDisplayTerms"%>
<%@page import="com.swt.portal.categorylist.model.CategoryDTO"%>
<%@page import="com.swt.portal.categorylist.model.CategorySearchContainer"%>
<%@page import="com.swt.portal.categorylist.model.CategorySearchTerms"%>
<%@page import="com.swt.portal.categorylist.model.CategorySearchUtil"%>
<%@page import="com.swt.portal.categorylist.model.CategorySearchUtilImpl"%>
<%@page import="com.swt.portal.categorylist.portlet.CategoryListConfiguration"%>
<%@page import="com.swt.portal.categorylist.util.CategoryFileUtils"%>

<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>

<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="javax.portlet.PortletURL"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	// Retrieve the saved configuration.
	CategoryListConfiguration configuration = (CategoryListConfiguration) GetterUtil
			.getObject(renderRequest.getAttribute(CategoryListConfiguration.class.getName()));
	String vocabulary = StringPool.BLANK, subVocabulary = StringPool.BLANK;

	boolean showIcon = false, showDescription = false, showSequence = false, isHierarchical = false, usingSubVocabulary = false;
	if (Validator.isNotNull(configuration)) {
		vocabulary = portletPreferences.getValue("vocabulary", configuration.vocabulary());
		showIcon = GetterUtil.get(
				portletPreferences.getValue("showIcon", String.valueOf(configuration.showIcon())),
				false);
		showDescription = GetterUtil.get(
				portletPreferences.getValue("showDescription", String.valueOf(configuration.showDescription())),
				false);
		showSequence = GetterUtil.get(
				portletPreferences.getValue("showSequence", String.valueOf(configuration.showSequence())),
				false);
		isHierarchical = GetterUtil.get(
				portletPreferences.getValue("isHierarchical", String.valueOf(configuration.isHierarchical())),
				false);
		usingSubVocabulary = GetterUtil.get(
				portletPreferences.getValue("usingSubVocabulary", String.valueOf(configuration.usingSubVocabulary())),
				false);
		subVocabulary = portletPreferences.getValue("subVocabulary", configuration.subVocabulary());
	}

	long vocabularyIconId = 0;
	String vocabularyIconUrl = StringPool.BLANK;

	// Get vocabulary icon ID.
	if (!Validator.isBlank(vocabulary)) {
		try {
			AssetVocabulary v = AssetVocabularyLocalServiceUtil
					.getGroupVocabulary(themeDisplay.getScopeGroupId(), vocabulary);
			vocabularyIconId = CategoryFileUtils.getVocabularyIconId(themeDisplay.getScopeGroupId(),
					themeDisplay.getCompanyId(), v.getVocabularyId());
			if (vocabularyIconId > 0) {
				vocabularyIconUrl = CategoryFileUtils.getFileUrl(vocabularyIconId);
			}
		} catch (PortalException e) {
			// Do nothing.
		}
	}
	
	CategorySearchUtil searchUtil = new CategorySearchUtilImpl();

	// Dummy object to be used in LanguageUtil.format function.
	Object dummyObj = new Object();
%>