<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>

<%@page import="java.util.ArrayList"%>

<%@ include file="init.jsp"%>

<style scoped>
	#category-button-row {
	    position: absolute;
	    bottom: 100%;
	    top: 90%;
	    right: 0;
	    margin-right: 3%;
	}
</style>

<portlet:actionURL var="updateCategoryURL" name="<%=CategoryConstants.ACTION_UPDATE_CATEGORY%>">
	<portlet:param name="vocabulary" value="<%=vocabulary%>" />
	<portlet:param name="usingSubVocabulary" value="<%=String.valueOf(usingSubVocabulary)%>" />
	<portlet:param name="subVocabulary" value="<%=subVocabulary%>" />
</portlet:actionURL>

<liferay-ui:success key="categoryAdded" message="successfully-saved" />
<liferay-ui:success key="categoryUpdated" message="successfully-saved" />

<liferay-ui:error key="categoryNameAlreadyExist" message="categorylist.name-already-exist" />
<liferay-ui:error key="categoryNameMustNotBlank" message="categorylist.name-must-not-blank" />
<liferay-ui:error key="categoryNotFound" message="categorylist.data-not-found" />
<liferay-ui:error key="subCategoryNotFound" message="categorylist.data-not-found-sub-category" />
<liferay-ui:error key="cannotUpdateSubCategory" message="categorylist.can-not-update-sub-category" />
<liferay-ui:error key="cannotAddSubVocabulary" message="categorylist.cannot-add-sub-vocabulary" />

<%
	String catId = request.getParameter("catId");
	String updated = request.getParameter("updated");
	String iconId = request.getParameter("iconId");
	String iconUrl = CategoryFileUtils.getFileUrlByCategoryId(GetterUtil.getLong(catId));
%>

<div class="container-fluid">
	<aui:form name="fmUpdate" action="<%=updateCategoryURL.toString()%>" method="POST" enctype="multipart/form-data">
		<fieldset>
			<aui:input name="catId" type="hidden" value="<%=catId%>" />
			<aui:input name="iconId" type="hidden" value="<%=iconId%>" />
			
			<% if (isHierarchical) { %>
				<aui:select name="parentId" value="0"
					label='<%=LanguageUtil.format(locale, "parent", dummyObj)%>'>
					<aui:option value="0"><%="--- " + LanguageUtil.format(locale, "select", dummyObj) + " ---"%></aui:option>
					<%
						List<CategoryDTO> items = searchUtil.search(themeDisplay.getLayout().getCompanyId(),
							themeDisplay.getLayout().getGroupId(), vocabulary, null, 0, true, null, null);
						List<Long> exclusion = new ArrayList<>();
						exclusion.add(Long.valueOf(catId));
					%>
					<% for (CategoryDTO i : items) { %>
						<!-- Don't add its children to the select list. -->
						<% if (exclusion.indexOf(i.getParentId()) != -1) { %>
							<% exclusion.add(i.getId()); %>
						<!-- Don't add itself to the select list. -->
						<% } else if (exclusion.indexOf(i.getId()) == -1) { %>
							<aui:option value="<%=i.getId()%>"><%=searchUtil.getDeepLevelDisplay(i.getDeepLevel())+ i.getName()%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			<% } %>
			
			<aui:input name="name" type="text" max="75"
				label='<%=LanguageUtil.format(locale, "name", dummyObj)%>' >
				<aui:validator name="required" errorMessage='<%=LanguageUtil.format(locale, "this-field-is-required", dummyObj)%>' />
			</aui:input>
			
			<% if (showDescription) { %>
				<aui:input name="description" type="textarea" rows="3" 
					label='<%=LanguageUtil.format(locale, "description", dummyObj)%>' />
			<% } %>
			
			<% if (showSequence) { %>
				<aui:input name="sequence" type="number" value="1" min="1"
					label='<%=LanguageUtil.format(locale, "priority", dummyObj)%>' >
					<aui:validator name="min" errorMessage='<%=LanguageUtil.format(locale, "categorylist.validate-min-value", 1)%>'>1</aui:validator>
				</aui:input>
			<% } %>
			
			<aui:select name="status" value="1"
				label='<%=LanguageUtil.format(locale, "status", dummyObj)%>'>
				<aui:option value="1"><%=LanguageUtil.format(locale, "active", dummyObj)%></aui:option>
				<aui:option value="2"><%=LanguageUtil.format(locale, "inactive", dummyObj)%></aui:option>
			</aui:select>
			
			<% if (showIcon) { %>
				<aui:input type="file" name="fileIcon" label="icon" />
				<c:if test="<%=!Validator.isBlank(iconUrl)%>">
					<div class="category-icon-display">
						<liferay-ui:icon src="<%=iconUrl%>" label="icon" message="icon" />
						<br />
						<aui:button type="button" cssClass="btn btn-sm btn-default" value="delete" onClick="javascript:deleteIcon()"></aui:button>
						<aui:input id="isDeleteIcon" name="isDeleteIcon" type="hidden" value="<%=false%>"/>
						<br /><br />
					</div>
				</c:if>
			<% } %>
			
		</fieldset>
		
		<div id="category-button-row" class="row-fluid">
			<aui:button type="submit" name="submit" />
			<aui:button type="button" name="cancel" value='<%=LanguageUtil.format(locale, "cancel", dummyObj)%>' />
		</div>
	</aui:form>
</div>

<aui:script use="aui-base">
	A.one('#<portlet:namespace/>cancel').on('click', function(event) {
	    var data = '';
	    var changed = $("#<portlet:namespace/>tbxChanged").val();
		Liferay.Util.getOpener().<portlet:namespace/>closeCategoryDialog(data, '<portlet:namespace/>dlgCategory', <%=updated%>);
	});
</aui:script>

<aui:script>
	function deleteIcon(){
		$("[id$='isDeleteIcon']").val(true);
		$(".category-icon-display").css("display", "none");
	}
</aui:script>