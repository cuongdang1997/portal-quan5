<%@ include file="init.jsp"%>

<style scoped>
	.fieldset {
		margin-top: 20px;
		padding: 20px;
	}
	
	.sub-vocabulary-display-none{
		display: none;
	}
	
	.sub-vocabulary-display-block{
		display: block;
	}
</style>

<liferay-ui:error key="cannotAddIcon" message="categorylist.icon-not-save" />
<liferay-ui:error key="iconLessThan10kb" message="categorylist.icon-less-than-10-kb" />
<liferay-ui:error key="cannotGetVocabulary" message="that-vocabulary-does-not-exist" />
<liferay-ui:error key="cannotAddVocabulary" message="categorylist.cannot-add-vocabulary" />
<liferay-ui:error key="unduplicatedVocabulary" message="categorylist.unduplicated-vocabulary" />

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="<%=true%>" var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" enctype="multipart/form-data" name="<portlet:namespace />fm">
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden" value="<%=configurationRenderURL%>"/>
		
		<aui:fieldset cssClass="panel panel-default">
			<aui:input name="vocabulary" label="Vocabulary" value="<%=vocabulary%>"></aui:input>
			<aui:input name="usingSubVocabulary" type="checkbox" value="<%=usingSubVocabulary%>" label="Using sub vocabulary" cssClass="using-sub-vocabulary" onChange="enableSubVocabulary();" />
			<aui:input name="subVocabulary" label="" value="<%=subVocabulary%>" cssClass="sub-vocabulary"/>
			<aui:input type="file" name="fileIcon" label="Icon" />
			<div class="vocabulary-icon-display">
				<c:if test="<%=!Validator.isBlank(vocabularyIconUrl)%>">
					<liferay-ui:icon src="<%=vocabularyIconUrl%>" label="icon" message="icon" />
					<br />
					<aui:button type="button" cssClass="btn btn-sm btn-default" value="delete" onClick="javascript:deleteVocabularyIcon()"></aui:button>
					<aui:input name="existingVocabularyIconId" type="hidden" value="<%=vocabularyIconId%>"/>
					<aui:input id="isDeleteVocabularyIcon" name="isDeleteVocabularyIcon" type="hidden" value="<%=false%>"/>
					<br /><br />
				</c:if>
			</div>
			
			<aui:select name="showIcon" label="Show category icon" value="<%=showIcon%>">
				<aui:option value="true">True</aui:option>
				<aui:option value="false">False</aui:option>
			</aui:select>
			<aui:select name="showDescription" label="Show description" value="<%=showDescription%>">
				<aui:option value="true">True</aui:option>
				<aui:option value="false">False</aui:option>
			</aui:select>
			<aui:select name="showSequence" label="Show sequence" value="<%=showSequence%>">
				<aui:option value="true">True</aui:option>
				<aui:option value="false">False</aui:option>
			</aui:select>
			<aui:select name="isHierarchical" label="Hierarchical" value="<%=isHierarchical%>">
				<aui:option value="true">True</aui:option>
				<aui:option value="false">False</aui:option>
			</aui:select>
		</aui:fieldset>
		
		<aui:button-row>
			<aui:button type="submit" cssClass="btn btn-lg btn-primary btn-default"></aui:button>
		</aui:button-row>
	</div>
</aui:form>

<aui:script>
	function deleteVocabularyIcon(){
		$("[id$='isDeleteVocabularyIcon']").val(true);
		$(".vocabulary-icon-display").css("display", "none");
	}
	
	$(document).ready(function() {
		var usingSubVocabulary = $(".using-sub-vocabulary").prop("checked");
		if(usingSubVocabulary){
			$(".sub-vocabulary").addClass("sub-vocabulary-display-block");
		}else{
			$(".sub-vocabulary").addClass("sub-vocabulary-display-none");
		}
	});
	
	function enableSubVocabulary(){
		
		var usingSubVocabulary = $(".using-sub-vocabulary").prop("checked");
		
		$(".sub-vocabulary").removeClass("sub-vocabulary-display-none").removeClass("sub-vocabulary-display-block");
		
		if(usingSubVocabulary){
			$(".sub-vocabulary").addClass("sub-vocabulary-display-block");
		}else{
			$(".sub-vocabulary").addClass("sub-vocabulary-display-none");
		}
	}
</aui:script>