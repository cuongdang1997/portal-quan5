<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategoryProperty"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>

<%@page import="org.apache.commons.beanutils.BeanComparator"%>

<%@ include file="init.jsp"%>

<%
	PortletURL iteratorURL = renderResponse.createActionURL();

	PortletURL portletURL = renderResponse.createRenderURL();
	CategorySearchContainer categorySearchContainer = new CategorySearchContainer(renderRequest, portletURL);
	
	// Get portlet title.
	String portletId = themeDisplay.getPortletDisplay().getId();
	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getExistingPortletSetup(themeDisplay.getLayout(), portletId);
	String portletTitle = themeDisplay.getPortletDisplay().getTitle();
	portletTitle = portletSetup.getValue("portlet-setup-title-" + themeDisplay.getLanguageId(), portletTitle).trim();
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
%>


<style scoped>
	.lfr-search-container th {
		font-weight: bold;
	}
	
	.alert.alert-info {
		margin-top: 20px;
	}
	
	.icon-white {
		color: white;
	}
	
	.icon-ok-sign {
		color: green;
	}
	
	.icon-ban-circle {
		color: gray;
	}
	
	.btn-swt-action {
		width: 38px;
		margin: 1px;
	}
	
	div[id$="dlgCategory"] .modal-body {
		padding: 16px 24px 16px 24px !important;
	}
	
	div[id$="dlgCategory"] iframe {
		height: 100% !important;
		width: 100% !important;
	}
	
	#<portlet:namespace />searchContainer th {
		text-align: center;
		text-transform: capitalize;
	}
</style>

<liferay-ui:success key="categoryRemoved" message="categorylist.successfully-removed" />

<liferay-ui:error key="cannotRemoveCategory" message="categorylist.cannot-remove-data" />
<liferay-ui:error key="cannotRemoveSubCategory" message="categorylist.cannot-remove-sub-category" />
<liferay-ui:error key="cannotAddIcon" message="categorylist.icon-not-save" />
<liferay-ui:error key="iconLessThan10kb" message="categorylist.icon-less-than-10-kb" />
<liferay-ui:error key="notFoundCategory" message="categorylist.not-found-category" />
<liferay-ui:error key="cannotAddSubVocabulary" message="categorylist.cannot-add-sub-vocabulary" />

<c:choose>
	<c:when test="<%=Validator.isNull(vocabulary)%>">
		<p><span style="font-style: italic; color: red;">Please configure the vocabulary for this porlet.</span></p>
	</c:when>
	<c:otherwise>
		<portlet:renderURL var="addCategoryURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
			<portlet:param name="path" value="<%=CategoryConstants.ACTION_ADD_CATEGORY%>" />
		</portlet:renderURL>
		
		<div class="row-fluid">
			<aui:button id="btnAddCategory" cssClass="btn btn-primary" icon="icon-plus"
				value='<%=LanguageUtil.format(request, "categorylist.add-new", dummyObj)%>' />
		</div>
		
		<br>

		<aui:form action="<%=portletURL.toString()%>" method="post" name="<portlet:namespace />fm">
			<liferay-ui:search-container id="searchContainer" emptyResultsMessage="there-are-no-results" 
				searchContainer="<%=categorySearchContainer%>">
				
				<liferay-ui:search-form page="/search.jsp" servletContext="<%=application%>"></liferay-ui:search-form>

				<liferay-ui:search-container-results
					results="<%=searchUtil.search(themeDisplay.getLayout().getCompanyId(),
						themeDisplay.getLayout().getGroupId(), vocabulary, (CategorySearchContainer)searchContainer)%>">
				</liferay-ui:search-container-results>

				<liferay-ui:search-container-row className="com.swt.portal.categorylist.model.CategoryDTO"
					modelVar="category" indexVar="index" keyProperty="id">
							
					<%
						String categoryIcon = StringPool.BLANK;
						if (category.getIconId() != 0) {
							categoryIcon = CategoryFileUtils.getFileUrl(category.getIconId());
						}
					%>
					
					<portlet:renderURL var="updateURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
						<portlet:param name="path" value="<%=CategoryConstants.ACTION_UPDATE_CATEGORY%>" />
						<portlet:param name="parentId" value="<%=String.valueOf(category.getParentId())%>" />
						<portlet:param name="catId" value="<%=String.valueOf(category.getId())%>" />
						<portlet:param name="name" value="<%=String.valueOf(category.getName())%>" />
						<portlet:param name="description" value="<%=category.getPlainDescription()%>" />
						<portlet:param name="sequence" value="<%=String.valueOf(category.getSequence())%>" />
						<portlet:param name="status" value="<%=String.valueOf(category.getStatus())%>" />
						<portlet:param name="iconId" value="<%=String.valueOf(category.getIconId())%>" />
					</portlet:renderURL>
					
					<portlet:actionURL var="removeURL" name="<%=CategoryConstants.ACTION_REMOVE_CATEGORY%>">
						<portlet:param name="categoryId" value="<%=String.valueOf(category.getId())%>" />
						<portlet:param name="iconId" value="<%=String.valueOf(category.getIconId())%>" />
						<portlet:param name="usingSubVocabulary" value="<%=String.valueOf(usingSubVocabulary)%>" />
						<portlet:param name="subVocabulary" value="<%=subVocabulary%>" />
					</portlet:actionURL>
					
					<liferay-ui:search-container-column-text align="center"
						name="<%=LanguageUtil.format(request, "id", dummyObj)%>"
						value="<%=String.valueOf(category.getId())%>"
						href="javascript:showCategoryDialog('${updateURL}');"
						orderable="true" orderableProperty="id"/>
						
					<liferay-ui:search-container-column-text 
						name='<%=LanguageUtil.format(request, "name", dummyObj)%>'
						href="javascript:showCategoryDialog('${updateURL}')"
						orderable="true" orderableProperty="name" >
						
						<% if (category.getDeepLevel() > 0){ %>
							<img src="<%=request.getContextPath()%>/img/level-indicator-1.png">
							<% for (int i = 1; i< category.getDeepLevel(); i++) { %>
								<img src="<%=request.getContextPath()%>/img/level-indicator-n.png">
							<% } %>
						<% } %>
						<%= category.getName()%>
					</liferay-ui:search-container-column-text>
					
					<c:choose>
						<c:when test="<%=showIcon%>">
							<liferay-ui:search-container-column-text  align="center" name="icon" >
								<img src="<%=categoryIcon %>" />
							</liferay-ui:search-container-column-text>
						</c:when>
					</c:choose>

					<c:choose>
						<c:when test="<%=showDescription%>">
							<liferay-ui:search-container-column-text
								name='<%=LanguageUtil.format(request, "description", dummyObj)%>'
								value="<%=String.valueOf(category.getDescription())%>" />
						</c:when>
					</c:choose>
					
					<c:choose>
						<c:when test="<%=showSequence%>">
							<liferay-ui:search-container-column-text
								name='<%=LanguageUtil.format(request, "priority", dummyObj)%>' 
								value="<%=String.valueOf(category.getSequence())%>" align="center"
								orderable="true" orderableProperty="sequence" />
						</c:when>
					</c:choose>

					<liferay-ui:search-container-column-text
						name='<%=LanguageUtil.format(request, "create-user", dummyObj)%>'
						value="<%=category.getCreateUser()%>" 
						orderable="true" orderableProperty="createUser"/>

					<liferay-ui:search-container-column-text 
						name='<%=LanguageUtil.format(request, "create-date", dummyObj)%>'
						value="<%=sdf.format(category.getCreateDate())%>"
						orderable="true" orderableProperty="createDate" />
						
					<liferay-ui:search-container-column-text 
						name='<%=LanguageUtil.format(request, "modified-date", dummyObj)%>'
						value="<%=sdf.format(category.getModifiedDate())%>"
						orderable="true" orderableProperty="modifiedDate" />

					 <liferay-ui:search-container-column-text align="center" 
						name='<%=LanguageUtil.format(request, "status", dummyObj)%>'
						orderable="true" orderableProperty="status">
						<% if (category.getStatus() == CategoryConstants.CATEGORY_PROPERTY_STATUS_ACTIVE) { %>
							<liferay-ui:icon iconCssClass="icon-ok-sign" message="active" />
						<% } else { %>
							<liferay-ui:icon iconCssClass="icon-ban-circle" message="inactive" />
						<% } %>
					</liferay-ui:search-container-column-text>
					
					<liferay-ui:search-container-column-text align="center"
						name='<%=LanguageUtil.format(request, "action", dummyObj)%>'>
						<button type="button" class="btn btn-info btn-swt-action" onclick="showCategoryDialog('${updateURL}')"
							title='<%=LanguageUtil.format(request, "update", dummyObj)%>'>
							<i class="icon-edit icon-white"></i>
						</button>
						<button type="button" class="btn btn-danger btn-swt-action" onclick="removeCategory('${removeURL}')"
							title='<%=LanguageUtil.format(request, "delete", dummyObj)%>'>
							<i class="icon-remove icon-white"></i>
						</button>
					</liferay-ui:search-container-column-text>
					
				</liferay-ui:search-container-row>
				
				<liferay-ui:search-iterator />
			</liferay-ui:search-container>
		</aui:form>
		
		<aui:script use="liferay-util-window">
			A.one('#<portlet:namespace/>btnAddCategory').on('click', function(event) {
				showCategoryDialog('<%=addCategoryURL%>');
			});
		</aui:script>
		<script>
			Liferay.provide(window,'<portlet:namespace/>closeCategoryDialog',
			    function(data, dialogId, changed) {
					var A = AUI();
					var dialog = Liferay.Util.Window.getById(dialogId);
					dialog.destroy();
					if (changed === true) {
						window.location.href = '<%=portletURL.toString()%>';
					}
				},
				['liferay-util-window']
			);
		
			function showCategoryDialog(renderURL) {
				Liferay.Util.openWindow({
					cache: false,
					dialog: {
						centered: true,
						height: 600,
						modal: true,
						width: 600
					},
					id: '<portlet:namespace/>dlgCategory',
					title: '<%=portletTitle%>',
					uri: renderURL
				});
			}
			
			function removeCategory(removeURL) {
				$.confirm({
					title: Liferay.Language.get("confirm"),
				    content: "Bạn có chắc muốn xóa dữ liệu này không?",
				    buttons: {
				        confirm: {
				        	text: Liferay.Language.get("confirm"),
				        	btnClass: "btn btn-danger",
				        	action: function () {
					        	window.location.href = removeURL;
					        }
				        },
				        cancel: {
				        	text: Liferay.Language.get("cancel"),
				        	action: function() {
					        	// Close.
					        }
				        }
				    }
				});
			}
			
		</script>
	</c:otherwise>
</c:choose>