<%@page import="com.liferay.dynamic.data.lists.model.DDLRecordSet"%>
<%@page import="com.liferay.dynamic.data.lists.service.DDLRecordSetLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.Group"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.util.LayoutDescription"%>
<%@page import="com.liferay.portal.util.LayoutListUtil"%>

<%@page import="com.swt.portal.journal.web.embedded.model.AttributeSearchDTO"%>

<%@page import="java.util.EnumSet"%>

<%@ include file="init.jsp"%>

<%
// Get the list of Web Content structures.
List<DDMStructure> structures = DDMStructureManagerUtil.getStructures(new long[] { groupId }, journalArticleClassNameId);
DDMStructure currentStructure = null;
if(!structureKey.equals(StringPool.BLANK)){
	currentStructure = DDMStructureManagerUtil.getStructure(themeDisplay.getScopeGroupId(), 
			journalArticleClassNameId, structureKey);
}
%>

<style scoped>
	.fieldset {
		margin-top: 20px;
		padding: 20px;
	}
	
	.control-group {
		margin-bottom: 0! important;
	}
	
	legend {
		text-transform: uppercase;
		font-weight: bold;
	}
	
	.btn-bottom-unset {
		bottom: unset !important;
	}
</style>

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%=true%>" var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL.toString()%>" method="post" name="<portlet:namespace />fm">
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden" value="<%=configurationRenderURL%>" />
		
		<aui:fieldset-group>
			<!-- Basic settings. -->
			<aui:fieldset cssClass="panel panel-default">
				<legend><%=LanguageUtil.format(request, "embeddedjournalweb.basic-settings", dummyObj)%></legend>
				
				<!-- Select structure. -->
				<div class="row-fluid">
					<div class="col-md-4">
						<aui:select name="structureKey" label='<%=LanguageUtil.format(request, "structure", dummyObj)%>'>
							<% for (DDMStructure i : structures) { %>
								<aui:option label="<%=i.getName(locale)%>"
									value="<%=i.getStructureKey()%>"
									selected="<%=structureKey.equals(i.getStructureKey())%>">
								</aui:option>
							<% } %>
						</aui:select>
					</div>
				</div>
				
				<!-- Check bad words -->
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="checkBadWords"
							checked="<%=checkBadWords%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.check-bad-words", dummyObj)%>' />
					</div>
				</div>
				<c:if test="<%= checkBadWords %>">
					<%
					List<DDLRecordSet> recordSets = DDLRecordSetLocalServiceUtil.getDDLRecordSets(0, Integer.MAX_VALUE);
					%>
					<div class="row-fluid">
						<div class="col-md-4">
							<aui:select name="badWordsSetId" label='<%=LanguageUtil.format(request, "embeddedjournalweb.select-bad-words-record-set", dummyObj)%>'>
								<% for (DDLRecordSet i : recordSets) { %>
									<aui:option label="<%=i.getName(locale)%>"
										value="<%=i.getRecordSetId()%>"
										selected="<%=badWordsSetId == i.getRecordSetId()%>">
									</aui:option>
								<% } %>
							</aui:select>
						</div>
					</div>
				</c:if>
				
				<!-- Allow adding article. -->
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="allowAddArticle"
							checked="<%=allowAddArticle%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.allow-add-article", dummyObj)%>' />
					</div>
				</div>
				
				<% 
				if(Validator.isNotNull(currentStructure) &&
						(currentStructure.getName(locale).equalsIgnoreCase(
						LanguageUtil.format(request, "embeddedjournalweb.question-answer", dummyObj)) ||
						currentStructure.getName(locale).equalsIgnoreCase(
						LanguageUtil.format(request, "embeddedjournalweb.question-answer-alt", dummyObj)))){ %>
				<!-- Allow exporting articles. -->
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="allowExportArticles"
							checked="<%=allowExportArticles%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.allow-export-articles", dummyObj)%>' />
					</div>
				</div>
				<% } %>
				<!-- Create folder structure basing on create date. -->
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="createDateFolders"
							checked="<%=createDateFolders%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.create-date-folders", dummyObj)%>' />
					</div>
				</div>
				
				<!-- Show search articles by status -->
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="statusSearch"
							checked="<%=statusSearch%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.status-search", dummyObj)%>' />
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="defaultAdvancedSearch"
							checked="<%=defaultAdvancedSearch%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.default-advanced-search", dummyObj)%>' />
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="enablePriority"
							checked="<%=enablePriority%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.show-priority", dummyObj)%>' />
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="enablePriority"
							checked="<%=enablePriority%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.show-priority", dummyObj)%>' />
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="enableReceiver"
							checked="<%=enableReceiver%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.show-receiver", dummyObj)%>' />
					</div>
				</div>
				
				<!-- Display style. -->
				<div class="row-fluid">
					<div class="col-md-4">
						<%
						List<WebContentDisplayStyle> displayStyles = 
							new ArrayList<WebContentDisplayStyle>(EnumSet.allOf(WebContentDisplayStyle.class));
						%>
						<aui:select name="displayStyle"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.display-style", dummyObj)%>'>
							<% for (WebContentDisplayStyle i : displayStyles) { %>
								<aui:option label="<%=i%>"
									value="<%=i.getCode()%>"
									selected="<%=displayStyle == i.getCode()%>">
								</aui:option>
							<% } %>
						</aui:select>
					</div>
				</div>
				
				<!-- Workflow page link. -->
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="redirectToWorkflowAfterSubmit"
							checked="<%=redirectToWorkflowAfterSubmit%>"
							label='<%=LanguageUtil.format(request, "embeddedjournalweb.redirect-to-workflow-after-submit", dummyObj)%>' />
					</div>
				</div>
				<c:if test="<%=redirectToWorkflowAfterSubmit%>">
					<div class="row-fluid">
						<div class="col-md-4">
							<aui:select label="embeddedjournalweb.choose-workflow-page" name="workflowPageUUID">
								<%
									Group group = layout.getGroup();
									List<LayoutDescription> layoutDescriptions = LayoutListUtil
										.getLayoutDescriptions(layout.getGroup().getGroupId(), layout.isPrivateLayout(), group.getGroupKey(), locale);
								%>
								<% for (LayoutDescription layoutDescription : layoutDescriptions) { %>
									<% Layout layoutDescriptionLayout = LayoutLocalServiceUtil.fetchLayout(layoutDescription.getPlid()); %>
									<% if (layoutDescriptionLayout != null) { %>
										<aui:option label="<%=layoutDescription.getDisplayName()%>" 
											selected="<%=layoutDescriptionLayout.getUuid().equals(workflowPageUUID)%>"
											value="<%=layoutDescriptionLayout.getUuid()%>" />
									<% } %>
								<% } %>
							</aui:select>
						</div>
					</div>
				</c:if>
			</aui:fieldset>
	
			<!-- Advanced search. -->
			<aui:fieldset cssClass="panel panel-default">
				<legend><%=LanguageUtil.format(request, "embeddedjournalweb.advanced-search", dummyObj)%></legend>
				
				<!-- Search by article attributes (advanced search). -->
				<p><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-by-attributes", dummyObj)%></strong></p>
				<table class="table table-bordered table-striped">
					<thead class="alert alert-info">
						<tr>
							<th><strong><%=LanguageUtil.format(request, "field", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-type", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.searchable", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-sequence", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.displayable", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.display-sequence", dummyObj)%></strong></th>
						</tr>
					</thead>
					<tbody>
						<% for (AttributeSearchDTO attr : attributes) { %>
							<%
							String ddmType = attr.getType();
							String prefix = "A" + attr.getName();
							
							boolean searchSupported = false;
			 				boolean searchable = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "searchable", "false"));;
			 				int searchType = GetterUtil.getInteger(portletPreferences.getValue(prefix + "searchType", "1"));
			 				int searchSequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "searchSequence", "0"));
			 				
			 				boolean displayable = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "displayable", "false"));
			 				int displaySequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "displaySequence", "0"));
				 			%>
				 			<tr>
								<td>
									<% if (displayStyle == WebContentDisplayStyle.DOCUMENT.getCode() && attr.getName().equals("description")) { %>
										<%= LanguageUtil.format(request, "embeddedjournalweb.document-description", dummyObj) %>
									<% } else { %>
										<%= attr.getLabel() %>
									<% } %>
								</td>
								<td>
									<% if (ddmType.equals("text") || ddmType.equals("textarea") || ddmType.equals("ddm-text-html")) { %>
										<% searchSupported = true; %>
										<%=LanguageUtil.format(request, "embeddedjournalweb.the-search-type-depends-on-indexing-type", dummyObj)%>
									<% } else if (ddmType.equals("ddm-date")) { %>
										<% searchSupported = true;%>
										<aui:select name='<%="preferences--" + prefix + "searchType--"%>'
											label="<%=StringPool.BLANK%>" >
											<aui:option
												value='<%=EmbeddedJournalWebConstants.SEARCH_TYPE_RANGE%>' 
												label='<%=LanguageUtil.format(request, "embeddedjournalweb.search-range", dummyObj)%>'
												selected='<%=searchType == EmbeddedJournalWebConstants.SEARCH_TYPE_RANGE%>'>
											</aui:option>
										</aui:select>
									<% } else { %>
										<%=LanguageUtil.format(request, "embeddedjournalweb.this-ddm-type-is-unsupported", dummyObj)%>
									<% } %>
								</td>
								<td valign="middle">
									<% if (searchSupported) { %>
										<aui:input type="checkbox" label="<%=StringPool.BLANK%>" 
											checked="<%=searchable%>" 
											name='<%="preferences--" + prefix + "searchable--"%>'/>
									<% } %>
								</td>
								<td valign="middle">
									<% if (searchSupported) { %>
										<aui:input type="number" label="<%=StringPool.BLANK%>" 
											value="<%=searchSequence > 0 ? String.valueOf(searchSequence) : StringPool.BLANK%>"
											name='<%="preferences--" + prefix + "searchSequence--"%>'/>
									<% } %>
								</td>
								<td valign="middle">
									<aui:input type="checkbox" label="<%=StringPool.BLANK%>"
										checked="<%=displayable%>" 
										name='<%="preferences--" + prefix + "displayable--"%>'/>
								</td>
								<td valign="middle">
									<aui:input type="number" label="<%=StringPool.BLANK%>" 
										value="<%=displaySequence > 0 ? String.valueOf(displaySequence) : StringPool.BLANK%>"
										name='<%="preferences--" + prefix + "displaySequence--"%>'/>
								</td>
							</tr>
						<% } %>
					</tbody>
				</table>
	
				<!-- Search by structure fields (advanced search). -->
				<% if (fields != null && fields.size() > 0) { %>
					<p><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-by-structure-fields", dummyObj)%></strong></p>
					<table class="table table-bordered table-striped">
						<thead class="alert alert-info">
							<tr>
								<th><strong><%=LanguageUtil.format(request, "field", dummyObj)%></strong></th>
								<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-type", dummyObj)%></strong></th>
								<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.searchable", dummyObj)%></strong></th>
								<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-sequence", dummyObj)%></strong></th>
								<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.displayable", dummyObj)%></strong></th>
								<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.display-sequence", dummyObj)%></strong></th>
							</tr>
						</thead>
						<tbody>
							<!-- Display search options basing on DDM field type. -->
							<% for (DDMFormField field : fields) { %>
								<%
								String ddmType = field.getType();
								String prefix = "S" + structureKey + field.getName();
								
								boolean searchSupported = false;
				 				boolean searchable = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "searchable", "false"));;
				 				int searchType = GetterUtil.getInteger(portletPreferences.getValue(prefix + "searchType", "1"));
				 				int searchSequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "searchSequence", "0"));
				 				
				 				boolean displayable = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "displayable", "false"));
				 				int displaySequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "displaySequence", "0"));
					 			%>
					 			<tr>
									<td>
										<%= field.getLabel().getString(locale) %>
									</td>
									<td>
										<% if (ddmType.equals("text") || ddmType.equals("textarea") || ddmType.equals("ddm-text-html")) { %>
											<% searchSupported = true; %>
											<%=LanguageUtil.format(request, "embeddedjournalweb.the-search-type-depends-on-indexing-type", dummyObj)%>
										<% } else if (ddmType.equals("ddm-date")) { %>
											<% searchSupported = true;%>
											<aui:select name='<%="preferences--" + prefix + "searchType--"%>'
												label="<%=StringPool.BLANK%>" >
												<aui:option
													value='<%=EmbeddedJournalWebConstants.SEARCH_TYPE_RANGE%>' 
													label='<%=LanguageUtil.format(request, "embeddedjournalweb.search-range", dummyObj)%>'
													selected='<%=searchType == EmbeddedJournalWebConstants.SEARCH_TYPE_RANGE%>'>
												</aui:option>
											</aui:select>
										<% } else { %>
											<%=LanguageUtil.format(request, "embeddedjournalweb.this-ddm-type-is-unsupported", dummyObj)%>
										<% } %>
									</td>
									<td valign="middle">
										<% if (searchSupported) { %>
											<aui:input type="checkbox" label="<%=StringPool.BLANK%>" 
												checked="<%=searchable%>" 
												name='<%="preferences--" + prefix + "searchable--"%>'/>
										<% } %>
									</td>
									<td valign="middle">
										<% if (searchSupported) { %>
											<aui:input type="number" label="<%=StringPool.BLANK%>" 
												value="<%=searchSequence > 0 ? String.valueOf(searchSequence) : StringPool.BLANK%>"
												name='<%="preferences--" + prefix + "searchSequence--"%>'/>
										<% } %>
									</td>
									<td valign="middle">
										<aui:input type="checkbox" label="<%=StringPool.BLANK%>"
											checked="<%=displayable%>" 
											name='<%="preferences--" + prefix + "displayable--"%>'/>
									</td>
									<td valign="middle">
										<aui:input type="number" label="<%=StringPool.BLANK%>" 
											value="<%=displaySequence > 0 ? String.valueOf(displaySequence) : StringPool.BLANK%>"
											name='<%="preferences--" + prefix + "displaySequence--"%>'/>
									</td>
								</tr>
							<% } %>
						</tbody>
					</table>
				<% } %>
				
				<!-- Search by vocabularies. -->
				<% if (structureKey != null && structureKey.length() != 0) { %>
					<% if (vocabularies != null && vocabularies.size() != 0) { %>
						<p><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-by-vocabularies", dummyObj)%></strong></p>
						<table class="table table-bordered table-striped">
							<thead class="alert alert-info">
								<tr>
									<th><strong><%=LanguageUtil.format(request, "vocabulary", dummyObj)%></strong></th>
									<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.searchable", dummyObj)%></strong></th>
									<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.search-sequence", dummyObj)%></strong></th>
									<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.displayable", dummyObj)%></strong></th>
									<th><strong><%=LanguageUtil.format(request, "embeddedjournalweb.display-sequence", dummyObj)%></strong></th>
								</tr>
							</thead>
							<tbody>
								<% for (AssetVocabulary vocabulary : vocabularies) { %>
									<%
										String prefix = "V" + vocabulary.getVocabularyId();
						 				
						 				boolean searchable = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "searchable", "false"));;
						 				int searchType = GetterUtil.getInteger(portletPreferences.getValue(prefix + "searchType", "1"));
						 				int searchSequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "searchSequence", "0"));
						 				
						 				boolean displayable = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "displayable", "false"));
						 				int displaySequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "displaySequence", "0"));
									%>
									<tr>
										<td>
											<%=vocabulary.getName().substring(vocabularyPrefix.length())%>
										</td>
										<td valign="middle">
											<aui:input type="checkbox" label="<%=StringPool.BLANK%>" 
												checked="<%=searchable%>" 
												name='<%="preferences--" + prefix + "searchable--"%>'/>
										</td>
										<td valign="middle">
											<aui:input type="number" label="<%=StringPool.BLANK%>" 
												value="<%=searchSequence > 0 ? String.valueOf(searchSequence) : StringPool.BLANK%>"
												name='<%="preferences--" + prefix + "searchSequence--"%>'/>
										</td>
										<td valign="middle">
											<aui:input type="checkbox" label="<%=StringPool.BLANK%>"
												checked="<%=displayable%>" 
												name='<%="preferences--" + prefix + "displayable--"%>'/>
										</td>
										<td valign="middle">
											<aui:input type="number" label="<%=StringPool.BLANK%>" 
												value="<%=displaySequence > 0 ? String.valueOf(displaySequence) : StringPool.BLANK%>"
												name='<%="preferences--" + prefix + "displaySequence--"%>'/>
										</td>
									</tr>
								<% } %>
							</tbody>
						</table>
					<% } %>
				<% } %>
			</aui:fieldset>
		</aui:fieldset-group>
		
		<!-- Form actions -->
		<aui:button-row cssClass="btn-bottom-unset">
			<aui:button type="submit" cssClass="btn btn-lg btn-primary btn-default " />
		</aui:button-row>
	</div>
</aui:form>