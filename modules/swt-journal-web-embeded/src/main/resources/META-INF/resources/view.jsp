<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>

<%@ include file="init.jsp"%>

<%
	String portletId = "com_liferay_journal_web_portlet_JournalPortlet";
	
	PermissionChecker permissionsChecker = themeDisplay.getPermissionChecker();
	boolean hasPermission = permissionsChecker.hasPermission(themeDisplay.getScopeGroupId(), portletId, portletId, "VIEW");
%>

<style scoped>
	.swt-header-title {
		text-transform: uppercase;
		font-weight: bold;
	}
	
	section#portlet_<%=portletId%> {
		padding-left: 0;
		padding-right: 0;
	}
	
	#portlet_<%=portletId%> > header.portlet-topper {
		display: none;
	}
	
	#portlet_<%=portletId%> > div.portlet-content-editable {
		border-color: transparent;
	}
</style>

<% if (!hasPermission) { %>
	<div class="container-fluid-1280">
		<p>
			<span style="font-style: italic; color: red;">
				Sorry, you don't have the permission to access this page!
			</span>
		</p>
	</div>
<% } else if (structureKey == null || structureKey.length() == 0) { %>
	<div class="container-fluid-1280">
		<p>
			<span style="font-style: italic; color: red;">
				Please configure the structure and search settings for this porlet.
			</span>
		</p>
	</div>
<% } else { %>
	<%
	// The JSON object which stores portlet preferences.
	JSONObject jsonPrefs = JSONFactoryUtil.createJSONObject();
	
	if (checkBadWords) {
		jsonPrefs.put("badWordsSetId", String.valueOf(badWordsSetId));
	}
	
	if (allowAddArticle) {
		jsonPrefs.put("allowAddArticle", String.valueOf(allowAddArticle));
	}
	
	if (allowExportArticles) {
		jsonPrefs.put("allowExportArticles", String.valueOf(allowExportArticles));
	}
	
	if (createDateFolders) {
		jsonPrefs.put("createDateFolders", String.valueOf(createDateFolders));
	}
	
	if (redirectToWorkflowAfterSubmit) {
		jsonPrefs.put("workflowPageUUID", workflowPageUUID);
	}
	
	if (statusSearch) {
		jsonPrefs.put("statusSearch", String.valueOf(statusSearch));
	}
	
	if (defaultAdvancedSearch) {
		jsonPrefs.put("defaultAdvancedSearch", String.valueOf(defaultAdvancedSearch));
	}
	
	// Iterate through structure fields to determine the list of search criterias/display columns.
	String key = null, val = null;
	for (DDMFormField f : fields) {
		key = "S" + structureKey + f.getName() + "searchable";
		val = portletPreferences.getValue(key, "0");
		key = "S" + f.getName() + "s"; // s stands for searchable
		jsonPrefs.put(key, val);
		
		key = "S" + structureKey + f.getName() + "searchType";
		val = portletPreferences.getValue(key, "0");
		key = "S" + f.getName() + "st"; // st stands for searchType
		jsonPrefs.put(key, val);
		
		key = "S" + structureKey + f.getName() + "searchSequence";
		val = portletPreferences.getValue(key, "0");
		key = "S" + f.getName() + "ss"; // ss stands for searchSequence
		jsonPrefs.put(key, val);
		
		key = "S" + structureKey + f.getName() + "displayable";
		val = portletPreferences.getValue(key, "false");
		key = "S" + f.getName() + "d"; // d stands for displayble
		jsonPrefs.put(key, val);
		
		key = "S" + structureKey + f.getName() + "displaySequence";
		val = portletPreferences.getValue(key, "0");
		key = "S" + f.getName() + "ds"; // ds stands for displaySequence
		jsonPrefs.put(key, val);
	}
	
	// Iterate through structure vocabularies to determine the list of search criterias/display columns.
	for (AssetVocabulary v : vocabularies) {
		key = "V" + v.getVocabularyId() + "searchable";
		val = portletPreferences.getValue(key, "0");
		key = "V" + v.getVocabularyId() + "s";
		jsonPrefs.put(key, val);
		
		key = "V" + v.getVocabularyId() + "searchType";
		val = portletPreferences.getValue(key, "0");
		key = "V" + v.getVocabularyId() + "st";
		jsonPrefs.put(key, val);
		
		key = "V" + v.getVocabularyId() + "searchSequence";
		val = portletPreferences.getValue(key, "0");
		key = "V" + v.getVocabularyId() + "ss";
		jsonPrefs.put(key, val);
		
		key = "V" + v.getVocabularyId() + "displayable";
		val = portletPreferences.getValue(key, "false");
		key = "V" + v.getVocabularyId() + "d";
		jsonPrefs.put(key, val);
		
		key = "V" + v.getVocabularyId() + "displaySequence";
		val = portletPreferences.getValue(key, "0");
		key = "V" + v.getVocabularyId() + "ds";
		jsonPrefs.put(key, val);
	}
	
	for (AttributeSearchDTO a : attributes) {
		key = "A" + a.getName() + "searchable";
		val = portletPreferences.getValue(key, "0");
		key = "A" + a.getName() + "s";
		jsonPrefs.put(key, val);
		
		key = "A" + a.getName() + "searchType";
		val = portletPreferences.getValue(key, "0");
		key = "A" + a.getName() + "st";
		jsonPrefs.put(key, val);
		
		key = "A" + a.getName() + "searchSequence";
		val = portletPreferences.getValue(key, "0");
		key = "A" + a.getName() + "ss";
		jsonPrefs.put(key, val);
		
		key = "A" + a.getName() + "displayable";
		val = portletPreferences.getValue(key, "false");
		key = "A" + a.getName() + "d";
		jsonPrefs.put(key, val);
		
		key = "A" + a.getName() + "displaySequence";
		val = portletPreferences.getValue(key, "0");
		key = "A" + a.getName() + "ds";
		jsonPrefs.put(key, val);
	}
	
	// Setup portlet query string.
	StringBundler queryString = new StringBundler();
	queryString.append("lifecycle=0");
	queryString.append("&state=maximized");
	queryString.append("&mode=view");
	queryString.append("&showEditActions=true");
	queryString.append("&searchContainerId=articles");
	queryString.append("&folderId=0");
	queryString.append("&navigation=structure");
	queryString.append("&ddmStructureKey=" + structureKey);
	queryString.append("&enablePriority=" + String.valueOf(enablePriority));
	queryString.append("&enableReceiver=" + String.valueOf(enableReceiver));
	queryString.append("&auth=" + AuthTokenUtil.getToken(request));
	queryString.append("&embedded=true");
	queryString.append("&childrenPage=" + childrenPage);
	queryString.append("&displayStyle=" + displayStyle);
	queryString.append("&jsonPrefs=" + jsonPrefs.toString());
	%>

	<liferay-portlet:runtime portletName="<%=portletId%>" queryString="<%=queryString.toString()%>" />
	
<% } %>