<%@page import="com.liferay.portal.kernel.model.Layout"%>
<%@page import="com.liferay.portal.kernel.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMFormField"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMStructure"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil"%>
<%@page import="com.liferay.journal.model.JournalArticle"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.search.BaseModelSearchResult"%>
<%@page import="com.liferay.portal.kernel.security.auth.AuthTokenUtil"%>
<%@page import="com.liferay.portal.kernel.security.permission.PermissionChecker"%>
<%@page import="com.liferay.portal.kernel.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.JavaConstants"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>

<%@page import="com.swt.portal.journal.web.embedded.model.AttributeSearchDTO"%>
<%@page import="com.swt.portal.journal.web.embedded.model.EmbeddedJournalWebConstants"%>
<%@page import="com.swt.portal.journal.web.embedded.model.WebContentDisplayStyle"%>
<%@page import="com.swt.portal.journal.web.embedded.portlet.EmbeddedJournalWebConfiguration"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
long companyId = themeDisplay.getCompanyId();
long groupId = themeDisplay.getScopeGroupId();
Object dummyObj = new Object();

EmbeddedJournalWebConfiguration configuration = (EmbeddedJournalWebConfiguration) GetterUtil
	.getObject(renderRequest.getAttribute(EmbeddedJournalWebConfiguration.class.getName()));

String structureKey = StringPool.BLANK;
String childrenPage = StringPool.BLANK;
long badWordsSetId = 0;
int displayStyle = WebContentDisplayStyle.NORMAL.getCode();
boolean checkBadWords = false, allowAddArticle = true, allowExportArticles = true, createDateFolders = true,
	statusSearch = true, redirectToWorkflowAfterSubmit = false, defaultAdvancedSearch = true, enablePriority = false, enableReceiver = false;
String workflowPageUUID = StringPool.BLANK;

if (Validator.isNotNull(configuration)) {
	structureKey = GetterUtil.getString(portletPreferences.getValue("structureKey", configuration.structureKey()));
	
	checkBadWords = GetterUtil.getBoolean(portletPreferences.getValue("checkBadWords", String.valueOf(configuration.checkBadWords())));
	badWordsSetId = GetterUtil.getLong(portletPreferences.getValue("badWordsSetId", String.valueOf(configuration.badWordsSetId())));
	
	allowAddArticle = GetterUtil.getBoolean(portletPreferences.getValue("allowAddArticle", String.valueOf(configuration.allowAddArticle())));
	
	enablePriority = GetterUtil.getBoolean(portletPreferences.getValue("enablePriority", String.valueOf(configuration.enablePriority())));
	
	enableReceiver = GetterUtil.getBoolean(portletPreferences.getValue("enableReceiver", String.valueOf(configuration.enableReceiver())));
	
	allowExportArticles = GetterUtil.getBoolean(portletPreferences.getValue("allowExportArticles", 
			String.valueOf(configuration.allowExportArticles())));
	
	createDateFolders = GetterUtil.getBoolean(portletPreferences.getValue("createDateFolders", 
			String.valueOf(configuration.createDateFolders())));
	
	String childrenPageUUID = portletPreferences.getValue("portletSetupLinkToLayoutUuid", StringPool.BLANK);
	if (childrenPageUUID.length() != 0) {
		Layout childrenPageLayout = LayoutLocalServiceUtil.fetchLayoutByUuidAndGroupId(childrenPageUUID, groupId, true);
		if (childrenPageLayout != null) {
			childrenPage = childrenPageLayout.getFriendlyURL();
		}
	}
	
	statusSearch = GetterUtil.getBoolean(portletPreferences.getValue("statusSearch", String.valueOf(configuration.statusSearch())));
	
	defaultAdvancedSearch = GetterUtil.getBoolean(portletPreferences.getValue("defaultAdvancedSearch", 
			String.valueOf(configuration.defaultAdvancedSearch())));
	
	displayStyle = GetterUtil.getInteger(portletPreferences.getValue("displayStyle", 
			String.valueOf(configuration.displayStyle())));

	redirectToWorkflowAfterSubmit = GetterUtil.getBoolean(portletPreferences.getValue("redirectToWorkflowAfterSubmit", 
			String.valueOf(configuration.redirectToWorkflowAfterSubmit())));
	workflowPageUUID = GetterUtil.getString(portletPreferences.getValue("workflowPageUUID", 
			configuration.workflowPageUUID()));
}

// The list of structure fields.
List<DDMFormField> fields = new ArrayList<>();
long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);

// The list of vocabularies which name is prefixed with structure ID.
List<AssetVocabulary> vocabularies = new ArrayList<>();
String vocabularyPrefix = StringPool.BLANK;

if (structureKey != null && structureKey.length() != 0) {
	// Get the selected structure by its ID.
	DDMStructure structure = null;
	try {
		structure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId, structureKey);
	} catch (PortalException e) {
		e.printStackTrace();
	} catch (SystemException e) {
		e.printStackTrace();
	}
	if (structure != null) {
		// Get the fields from selected structure.
		fields = structure.getDDMFormFields(false);
	}
	
	vocabularyPrefix = structure.getStructureId() + "_";
	
	try {
		BaseModelSearchResult<AssetVocabulary> tmp = AssetVocabularyLocalServiceUtil.searchVocabularies(
				themeDisplay.getCompanyId(), groupId, vocabularyPrefix, 0, Byte.MAX_VALUE);
		vocabularies = tmp.getBaseModels();
	} catch (PortalException e) {
		e.printStackTrace();
	}
}

List<AttributeSearchDTO> attributes = new ArrayList<>();
attributes.add(new AttributeSearchDTO("title", LanguageUtil.format(request, "title", dummyObj), "text"));
attributes.add(new AttributeSearchDTO("description", LanguageUtil.format(request, "description", dummyObj), "text"));
attributes.add(new AttributeSearchDTO("createUser", LanguageUtil.format(request, "metadata.DublinCore.CREATOR", dummyObj), "text"));
attributes.add(new AttributeSearchDTO("createDate", LanguageUtil.format(request, "create-date", dummyObj), "ddm-date"));
attributes.add(new AttributeSearchDTO("modifiedDate", LanguageUtil.format(request, "modified-date", dummyObj), "ddm-date"));
attributes.add(new AttributeSearchDTO("displayDate", LanguageUtil.format(request, "display-date", dummyObj), "ddm-date"));
%>