package com.swt.portal.journal.web.embedded.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import com.swt.portal.journal.web.embedded.model.EmbeddedJournalWebConstants;

/**
 * @author dung.nguyen@smartworld.com.vn
 */
@Component(
	configurationPid = "com.swt.portal.embedded.journalweb.portlet.EmbeddedJournalWebConfiguration",
	configurationPolicy = ConfigurationPolicy.OPTIONAL,
	immediate = true,
	property = {
		"javax.portlet.name=" + EmbeddedJournalWebConstants.PORTLET_NAME
	},
	service = ConfigurationAction.class
)
public class EmbeddedJournalWebConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private LogService _log;

	private volatile EmbeddedJournalWebConfiguration _config;

	/**
	 * Activate the configuration instance.
	 * 
	 * @param properties
	 */
	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_config = ConfigurableUtil.createConfigurable(EmbeddedJournalWebConfiguration.class, properties);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.portlet.SettingsConfigurationAction#processAction(
	 * javax.portlet.PortletConfig, javax.portlet.ActionRequest,
	 * javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		_log.log(LogService.LOG_INFO, EmbeddedJournalWebConstants.PORTLET_NAME + " configuration action.");

		String structureKey = ParamUtil.getString(actionRequest, "structureKey");
		setPreference(actionRequest, "structureKey", String.valueOf(structureKey));
		
		boolean checkBadWords = ParamUtil.getBoolean(actionRequest, "checkBadWords");
		setPreference(actionRequest, "checkBadWords", String.valueOf(checkBadWords));
		
		long badWordsSetId = ParamUtil.getLong(actionRequest, "badWordsSetId");
		setPreference(actionRequest, "badWordsSetId", String.valueOf(badWordsSetId));
		
		boolean allowAddArticle = ParamUtil.getBoolean(actionRequest, "allowAddArticle");
		setPreference(actionRequest, "allowAddArticle", String.valueOf(allowAddArticle));
		
		boolean allowExportArticles = ParamUtil.getBoolean(actionRequest, "allowExportArticles");
		setPreference(actionRequest, "allowExportArticles", String.valueOf(allowExportArticles));
		
		boolean createDateFolders = ParamUtil.getBoolean(actionRequest, "createDateFolders");
		setPreference(actionRequest, "createDateFolders", String.valueOf(createDateFolders));
		
		int displayStyle = ParamUtil.getInteger(actionRequest, "displayStyle");
		setPreference(actionRequest, "displayStyle", String.valueOf(displayStyle));
		
		boolean redirectToWorkflowAfterSubmit = ParamUtil.getBoolean(actionRequest, "redirectToWorkflowAfterSubmit");
		setPreference(actionRequest, "redirectToWorkflowAfterSubmit", String.valueOf(redirectToWorkflowAfterSubmit));
		
		String workflowPageUUID = ParamUtil.getString(actionRequest, "workflowPageUUID");
		setPreference(actionRequest, "workflowPageUUID", String.valueOf(workflowPageUUID));

		boolean statusSearch = ParamUtil.getBoolean(actionRequest, "statusSearch");
		setPreference(actionRequest, "statusSearch", String.valueOf(statusSearch));
		
		boolean defaultAdvancedSearch = ParamUtil.getBoolean(actionRequest, "defaultAdvancedSearch");
		setPreference(actionRequest, "defaultAdvancedSearch", String.valueOf(defaultAdvancedSearch));
		
		boolean enablePriority = ParamUtil.getBoolean(actionRequest, "enablePriority");
		setPreference(actionRequest, "enablePriority", String.valueOf(enablePriority));
		
		boolean enableReceiver = ParamUtil.getBoolean(actionRequest, "enableReceiver");
		setPreference(actionRequest, "enableReceiver", String.valueOf(enableReceiver));
		
		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.portlet.BaseJSPSettingsConfigurationAction#include(
	 * javax.portlet.PortletConfig, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		_log.log(LogService.LOG_INFO, EmbeddedJournalWebConstants.PORTLET_NAME + " configuration include.");

		httpServletRequest.setAttribute(EmbeddedJournalWebConfiguration.class.getName(), _config);
		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

}
