package com.swt.portal.journal.web.embedded.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.liferay.portal.kernel.util.StringPool;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(factory = true, id = "com.swt.portal.embedded.journalweb.portlet.EmbeddedJournalWebConfiguration", localization = "content/Language")
public interface EmbeddedJournalWebConfiguration {

	/**
	 * @return The key of structure which associates to this portlet.
	 */
	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String structureKey();

	/**
	 * @return If true, the porlet will support checking bad words in content.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean checkBadWords();

	/**
	 * @return The record set ID of bad words. Only available if the value of
	 *         {@link EmbeddedJournalWebConfiguration#checkBadWords()} is true.
	 */
	@Meta.AD(deflt = "0", required = false)
	public long badWordsSetId();

	/**
	 * @return If true, allow to add new article.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean allowAddArticle();

	/**
	 * @return If true, allow to export articles to file.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean allowExportArticles();

	/**
	 * @return If true, crreate folder structure basing on create date.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean createDateFolders();

	/**
	 * @return The display style of web content management form.
	 */
	@Meta.AD(deflt = "1", required = false)
	public int displayStyle();

	/**
	 * @return If true, redirect to workflow page after submitting article.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean redirectToWorkflowAfterSubmit();

	/**
	 * @return The UUID of workflow page. Only available if
	 *         redirectToWorkflowAfterSubmit is true.
	 */
	@Meta.AD(deflt = "", required = false)
	public String workflowPageUUID();

	/**
	 * @return Allow status search.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean statusSearch();
	
	/**
	 * @return Show advanced search form as default if true.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean defaultAdvancedSearch();
	
	/**
	 * @return Show priority if true.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean enablePriority();
	
	/**
	 * @return Show receiver if true.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean enableReceiver();
	
}