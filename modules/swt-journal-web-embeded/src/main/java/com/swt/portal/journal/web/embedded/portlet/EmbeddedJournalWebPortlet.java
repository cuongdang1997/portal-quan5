package com.swt.portal.journal.web.embedded.portlet;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.swt.portal.journal.web.embedded.model.EmbeddedJournalWebConstants;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

/**
 * @author dung.nguyen@smartworld.com.vn
 */
@Component(
	configurationPid = "com.swt.portal.embedded.journalweb.portlet.EmbeddedJournalWebConfiguration", 
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.portal",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.single-page-application=false",
		"javax.portlet.display-name=" + EmbeddedJournalWebConstants.PORTLET_DISPLAY_NAME,
		"javax.portlet.init-param.config-template=/configuration.jsp", 
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EmbeddedJournalWebConstants.PORTLET_NAME,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class EmbeddedJournalWebPortlet extends MVCPortlet {

	@Reference
	private LogService _log;
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.log(LogService.LOG_INFO,
				EmbeddedJournalWebConstants.PORTLET_NAME + " " + EmbeddedJournalWebConstants.ACTION_DO_VIEW);

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
		
		try {
			renderRequest.setAttribute(EmbeddedJournalWebConfiguration.class.getName(),
					portletDisplay.getPortletInstanceConfiguration(EmbeddedJournalWebConfiguration.class));
		} catch (ConfigurationException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
		}

		super.doView(renderRequest, renderResponse);
	}
	
}