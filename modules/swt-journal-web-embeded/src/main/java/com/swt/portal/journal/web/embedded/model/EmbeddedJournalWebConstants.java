package com.swt.portal.journal.web.embedded.model;

/**
 * @author dung.nguyen@smartworld.com.vn
 */
public class EmbeddedJournalWebConstants {

	/**
	 * The portlet unique name.
	 */
	public static final String PORTLET_NAME = "SWTEmbeddedJournalWeb";

	/**
	 * The portlet display name.
	 */
	public static final String PORTLET_DISPLAY_NAME = "HCMGov Embedded Journal Web";
	
	/**
	 * The do-view action name.
	 */
	public static final String ACTION_DO_VIEW = "doView";
	
	/**
	 * The "EQUAL" search type. To be used to configure the search type of some
	 * structure field types.
	 */
	public static final int SEARCH_TYPE_EQUAL = 1;

	/**
	 * The "RANGE" search type (from.. to..). To be used to configure the search
	 * type of some structure field types.
	 */
	public static final int SEARCH_TYPE_RANGE = 2;

}