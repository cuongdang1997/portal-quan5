package com.swt.portal.journal.web.embedded.model;

public class AttributeSearchDTO {

	private String name;
	private String label;
	private String type;

	public AttributeSearchDTO(String name, String label, String type) {
		super();
		this.name = name;
		this.label = label;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
