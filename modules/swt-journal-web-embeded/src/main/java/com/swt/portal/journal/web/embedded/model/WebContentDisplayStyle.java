package com.swt.portal.journal.web.embedded.model;

/**
 * The display style of web content management form.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public enum WebContentDisplayStyle {
	NORMAL(1), CMS(2), DOCUMENT(3), QUESTION_ANSWER(4), VIDEO(5), IMAGE(6), SCHEDULE(7), EBOOK(8), ORG_CHART(9), FEEDBACK(10), SEARCHSCORES(11);

	private final int code;

	private WebContentDisplayStyle(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static WebContentDisplayStyle toWebContentDisplayStyle(int code) {
		for (WebContentDisplayStyle me : WebContentDisplayStyle.values()) {
			if (me.getCode() == code) {
				return me;
			}
		}
		return null;
	}
}
