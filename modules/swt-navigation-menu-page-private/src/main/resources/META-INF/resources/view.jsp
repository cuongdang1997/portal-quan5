<%@page import="com.liferay.portal.kernel.service.permission.LayoutPermissionUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.swt.navigation.menu.page.util.NavigationMenuPagePrivateUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.Layout"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="java.util.HashMap"%>
<%@ include file="init.jsp"%>
	<style>
		.menu-top{
			  margin-top: 0.5%;
		      width: 100%;
		      z-index: 30
		}
		#menu {
		    width: 100%;
		    background: linear-gradient(#ffffff,#e0e7f1,#ccd4de); 
		    margin-left: 0px;
		    margin-top:-5px;
		    padding-left: 20px;
		    margin-bottom: 0px;
			height: 50px;
		    color: black;
		    font-weight: normal;
		    font-size: 14px; 
		}
		
		#menu li {
		   float: left;
		    box-shadow: 0px 0 0 white;
		    list-style: none;
		    margin-top: -6px;
		    position: relative;
		}
		.menu-a {
		    float: left;
		    padding-top: 20px;
		    color: black;
		    text-decoration: none;
		}
		
		#menu li:hover  a {
		   color: #222;
		}
		.home-page{
			height: 56px;
			margin-right : 26px;
		}
		.menu-father-margin{
		    margin-left: 10px;
   			margin-right: 26px;
		}
		.menu-father-li-a{
			height: 56px;
		}
		.extCong{
			float: left;
		    padding-top: 20px;
		    color: black;
		    margin-left: 10px;
		    margin-right: 26px
		}
		.extTru{
			float: left;
		    padding-top: 20px;
		    color: black;
		    margin-left: 10px;
		    margin-right: 26px;
		    display: none;
		}
		.menu-parent{
		  	width: 100%;
		    position: absolute;
		    background: white;
		    z-index: 62;
		    display:none;
		    background: linear-gradient(#f5f5f5, #f8f8f8, #f5f5f5);
   	  	}
		.menu-parent ul{
			margin: 0px;
			padding-inline-start: 0px !important;
		}
		.menu-parent li {
		    width: 100%;
		    padding-right: 10px;
		    box-shadow: 0px 0 0 white;
		    list-style: none;
		    position: relative;
		}	
	   	.menu-parent  a {    
			color: black;
			text-decoration: none;
			 padding-top: 20px;
		 }	

		.menu-parent-first{
			float: left;
		    width: 16%;
		    text-align: right;
		 	border-right: 1px solid #c0c0c0; 
		    margin-top: 10px;
		    margin-bottom: 10px;
		    background: linear-gradient(#f5f5f5, #f8f8f8, #f5f5f5);
		}
		.menu-parent :first-child menu-parent-first{
			width: 20% !important;
		}
		.menu-parent-first li{
		   box-shadow: 0px 0 0 white;
		    list-style: none;
		    margin-top: 10px !important;
		    position: relative;
		}
		.extCongParent{
    		float: right;
			color: #ffb700;
		    font-weight: bold;
		    margin-left: 10px;
		}
		.extTruParent{
    		float: right;
			color: #ffb700;
		    font-weight: bold;
		    display:none;
		    margin-left: 10px;
		}
		.menu-parent-last{
			float: left;
		    width: auto;
		    text-align: left;
		    background: linear-gradient(#f5f5f5, #f8f8f8, #f5f5f5);
		    margin-top: 10px;
		    margin-bottom: 10px;
		    display: none;
		}
		.ul-menu-parent-last{
	 		padding-left: 20px;
		}
		.ul-menu-parent-last li{
	 		 margin-top: 10px !important;
    		 margin-left: 10px !important;
		}
		.menu-back{
			float: right !important;
	    	margin-right: 25px;
		}
		#p_p_id_<%=themeDisplay.getPortletDisplay().getId()%>_ .container-fluid-1280{
			 margin-left: 0;
		    margin-right: 0;
		    max-width: 100%;
		    padding-left: 0px;
		    padding-right: 0px;
		}
	</style>

	<%
		String rootPage =themeDisplay.getPathFriendlyURLPrivateGroup()+themeDisplay.getScopeGroup().getFriendlyURL();
		String homePath = rootPage+ "/quan-tri-vien";
		long idLayoutFather =0;
		HashMap<Long, Integer> listMap = new HashMap<Long, Integer>(); 
	%>
<nav class="menu-top">
	<ul id="menu">
		<!-- link page quan tri vien -->
		<li><a class="home-page menu-a" href="<%=homePath%>"> 
			<span class="glyphicon glyphicon-home"></span>
			</a>
		</li>

		<!-- config -->
		<%
			if (Validator.isNotNull(listPage)) {
				String[] listIdLayout = listPage.split("/");
				String[] listSTTChecked = listSTT.split("/");
				
				// key idLayout, value Stt
				HashMap<Long, Integer> listMapPut = new HashMap<Long, Integer>();
					for (int i = 0; i < listSTTChecked.length; i++) {
						listMapPut.put(GetterUtil.getLong(listIdLayout[i]), GetterUtil.getInteger(listSTTChecked[i]));
					}
					// sort by STT
					 listMap = NavigationMenuPagePrivateUtil.sortByValues(listMapPut);		
		%>
				<%
					for (Map.Entry<Long, Integer> entry : listMap.entrySet()) {
						
						Layout objectLayout = LayoutLocalServiceUtil.getLayout(entry.getKey());
						idLayoutFather = objectLayout.getLayoutId();
						if(LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), objectLayout.getPlid(), "VIEW")){
				%>
				<!-- menu cap 1  -->
				<li class="menu-father <%if(!objectLayout.hasChildren()){ %> menu-father-margin <%}%>" >
					<a class="menu-father-li-a menu-a" id="menu-father<%=idLayoutFather%>"  onmouseover="mouseOver(<%=idLayoutFather%>)" onmouseout="mouseOut(<%=idLayoutFather%>)" href="<%=rootPage%><%=objectLayout.getFriendlyURL(themeDisplay.getLocale())%>"><%=objectLayout.getName(themeDisplay.getLocale())%> </a> 
					<%if(objectLayout.hasChildren()){ %>
						<span class="extCong" id="extCong<%=objectLayout.getLayoutId()%>">+</span>
					<%}%>
						<span class="extTru"  id="extTru<%=objectLayout.getLayoutId()%>">-</span>
					<%%>
				</li>
				<!--end menu cap 1  -->
				<%
						}
					}
				%>
				
		<%
			}else{
				long urlCurrent = themeDisplay.getPlid();
				Layout objectLayoutCurrent = NavigationMenuPagePrivateUtil.getPagePrivateCurrent(themeDisplay, themeDisplay.getPlid());
				 idLayoutFather = objectLayoutCurrent.getLayoutId();
		%>
				<!-- menu cap 1  -->
				<li  class="menu-father">
				<a  onmouseover="mouseOver(<%=objectLayoutCurrent.getLayoutId()%>)" onmouseout="mouseOut(<%=objectLayoutCurrent.getLayoutId()%>)" class="menu-father-li-a menu-a" id="menu-father<%=objectLayoutCurrent.getLayoutId()%>"  href="<%=rootPage%><%=objectLayoutCurrent.getFriendlyURL(themeDisplay.getLocale())%>"><%=objectLayoutCurrent.getName(themeDisplay.getLocale())%> </a> 
					<%if(objectLayoutCurrent.hasChildren()){ %>
						<span class="extCong" id="extCong<%=objectLayoutCurrent.getLayoutId()%>">+</span>
					<%}%>
						<span class="extTru"  id="extTru<%=objectLayoutCurrent.getLayoutId()%>">-</span>
				</li>
				<!--end menu cap 1  -->
				
		<%} %>
		<li class="menu-back" onclick="goBack()">
		<a href="#" class="menu-father-li-a menu-a"> <span class="glyphicon glyphicon-repeat"></span> <%=LanguageUtil.format(request, "menu.backHistory", new Object())%></a>
		</li>
	</ul>
</nav>


	<!-- menu cap 2 display config  -->
		<%
			if (Validator.isNotNull(listPage)) {
		%>
				<%
				int i= 1;
					for (Map.Entry<Long, Integer> entry : listMap.entrySet()) {
						
						Layout objectLayout = LayoutLocalServiceUtil.getLayout(entry.getKey());
						idLayoutFather = objectLayout.getLayoutId();
				%>
				 <!--menu cap 2  -->
					<% 
					long idLayoutParent = 0;
					List<Layout> layoutParentFirst = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), true, idLayoutFather); %>
					<div class="menu-parent" id="menu-parent<%=idLayoutFather%>"  onmouseover="mouseOverDiv(<%=objectLayout.getLayoutId()%>)" onmouseout="mouseOutDiv(<%=objectLayout.getLayoutId()%>)">
						<div class="menu-parent-first" style="width: <%=12*i%>%">
							<ul>
								<%
								if(Validator.isNotNull(layoutParentFirst)){
								for(Layout objectFirst : layoutParentFirst){
									if (objectFirst.getHidden()==false && LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), objectFirst.getPlid(), "VIEW")) {
									idLayoutParent =objectFirst.getLayoutId();
								%>
								<li><a class="menu-parent-a" id="menu-parent<%=objectFirst.getLayoutId()%>" onmouseover="mouseOverParent(<%=objectFirst.getLayoutId()%>)" onmouseout="mouseOutParent(<%=objectFirst.getLayoutId()%>)"  href="<%=rootPage%><%=objectFirst.getFriendlyURL(themeDisplay.getLocale())%>"><%=objectFirst.getName(themeDisplay.getLocale())%> </a> 
									<%if(objectFirst.hasChildren()){ %>
										<span class="extCongParent" id="extCongParent<%=objectFirst.getLayoutId()%>">+</span>
										<span class="extTruParent"  id="extTruParent<%=objectFirst.getLayoutId()%>">-</span>
									<%} %>
								</li>
								<%}
								}
								}
								%>
							</ul>
						</div>
						
						<!--menu cap 3  -->
						<% List<Layout> layoutParentLast = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), true, idLayoutParent); %>
						<div class="menu-parent-last" id="menu-parent-last<%=idLayoutParent%>">
							<ul class="ul-menu-parent-last">
								<%
								if(Validator.isNotNull(layoutParentLast)){
								for(Layout objectLast : layoutParentLast){ 
									if (objectLast.getHidden()==false&& LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), objectLast.getPlid(), "VIEW")) {
								%>
								<li><a id="menu-parent-last<%=objectLast.getLayoutId()%>" onmouseover="mouseOverParentLast(<%=objectLast.getLayoutId()%>)" onmouseout="mouseOutParentLast(<%=objectLast.getLayoutId()%>)"  href="<%=rootPage%><%=objectLast.getFriendlyURL(themeDisplay.getLocale())%>"><%=objectLast.getName(themeDisplay.getLocale())%> </a> 
								</li>
								<%}
								}
								}
								%>
							</ul>
						</div>	
						<!--end menu cap 3  -->
						
					</div>
				<!--end menu cap 2 config  --> 
				
				<%
				i++;
					}
					
			}else{
				%>
			<!--menu cap 2 auto display -->
					<% 
					long idLayoutParent = 0;
					List<Layout> layoutParentFirst = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), true, idLayoutFather); %>
					<div class="menu-parent" id="menu-parent<%=idLayoutFather%>"  onmouseover="mouseOverDiv(<%=idLayoutFather%>)" onmouseout="mouseOutDiv(<%=idLayoutFather%>)" >
						<div class= "menu-parent-first">
							<ul>
								<%
								if(Validator.isNotNull(layoutParentFirst)){
								for(Layout objectFirst : layoutParentFirst){ 
									if (objectFirst.getHidden()==false && LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), objectFirst.getPlid(), "VIEW")) {
									idLayoutParent =objectFirst.getLayoutId();
								%>
								<li><a class="menu-parent-a" id="menu-parent<%=objectFirst.getLayoutId()%>" onmouseover="mouseOverParent(<%=objectFirst.getLayoutId()%>)" onmouseout="mouseOutParent(<%=objectFirst.getLayoutId()%>)"  href="<%=rootPage%><%=objectFirst.getFriendlyURL(themeDisplay.getLocale())%>"><%=objectFirst.getName(themeDisplay.getLocale())%> </a> 
									<%if(objectFirst.hasChildren()){ %>
										<span class="extCongParent"  id="extCongParent<%=objectFirst.getLayoutId()%>">+</span>
										<span class="extTruParent"   id="extTruParent<%=objectFirst.getLayoutId()%>">-</span>
									<%} %>
								</li>
								<%}
								}
								}
								%>
							</ul>
						</div>
						
						<!--menu cap 3  -->
						<% List<Layout> layoutParentLast = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), true, idLayoutParent); %>
						<div class="menu-parent-last" id="menu-parent-last<%=idLayoutParent%>">
							<ul class="ul-menu-parent-last">
								<%
								if(Validator.isNotNull(layoutParentLast)){
								for(Layout objectLast : layoutParentLast){ 
									if (objectLast.getHidden()==false && LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), objectLast.getPlid(), "VIEW")) {
								%>
								<li><a id="menu-parent-last<%=objectLast.getLayoutId()%>" onmouseover="mouseOverParentLast(<%=objectLast.getLayoutId()%>)" onmouseout="mouseOutParentLast(<%=objectLast.getLayoutId()%>)"  href="<%=rootPage%><%=objectLast.getFriendlyURL(themeDisplay.getLocale())%>"><%=objectLast.getName(themeDisplay.getLocale())%> </a> 
								</li>
								<%}
								}
								}
								%>
							</ul>
						</div>	
						<!--end menu cap 3  -->
						
					</div>
				<!--end menu cap 2 auto display  -->
		<%} %>
	<script>
	/* hover menu cap 1 */
	function mouseOver(idFather){
		$("#extCong"+idFather).hide();
		$("#extTru"+idFather).show();
		$("#menu-father"+idFather).css({"border-bottom": "5px solid #ffb700"});
		$(".menu-parent").hide();
		$("#menu-parent"+idFather).slideDown("fast", function() {
			
		});
	}
	function mouseOut(idFather){
		 $("#extCong"+idFather).show();
		 $("#extTru"+idFather).hide();
		 $("#menu-father"+idFather).css({"border-bottom": "none"});
		 $("#menu-parent"+idFather).hide();
	} 
	/* hover menu div con */
	function mouseOverDiv(idFather){
		$("#extCong"+idFather).hide();
		$("#extTru"+idFather).show();
		$("#menu-father"+idFather).css({"border-bottom": "5px solid #ffb700"});
		$(".menu-parent").hide();
		$("#menu-parent"+idFather).show();
	}
	function mouseOutDiv(idFather){
		 $("#extCong"+idFather).show();
		 $("#extTru"+idFather).hide();
		 $("#menu-father"+idFather).css({"border-bottom": "none"});
		 $("#menu-parent"+idFather).hide();
	} 
	
	/* hover menu cap 2 */
	function mouseOverParent(idFather){
		$(".menu-parent-last").hide();
		$("#menu-parent"+idFather).css({"border-bottom": "5px solid #ffb700"});
		$("#extCongParent"+idFather).hide();
		$("#extTruParent"+idFather).show();
		$("#menu-parent-last"+idFather).show();
	}
	 function mouseOutParent(idFather){
		$(".menu-parent-a").css({"border-bottom": "none"});
		$("#extCongParent"+idFather).show();
		$("#extTruParent"+idFather).hide();
	} 
	
	/* hover menu cap 3 */
	function mouseOverParentLast(idFather){
		$("#menu-parent-last"+idFather).css({"border-bottom": "5px solid #ffb700"});
	}
	function mouseOutParentLast(idFather){
		$("#menu-parent-last"+idFather).css({"border-bottom": "none"});
	}
	function goBack(){
		window.history.go(-1);
	}
	</script>