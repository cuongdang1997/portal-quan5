<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.swt.navigation.menu.page.portlet.NavigationMenuPagePrivateConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />
	<%
	NavigationMenuPagePrivateConfiguration formConfiguration = (NavigationMenuPagePrivateConfiguration) renderRequest
	.getAttribute(NavigationMenuPagePrivateConfiguration.class.getName());
	String listPage = "";
	String listSTT = "";
	String typeDisplay ="";
	if (Validator.isNotNull(formConfiguration)) {
		listPage = portletPreferences.getValue("listPage", formConfiguration.listPage());
		listSTT = portletPreferences.getValue("listSTT", formConfiguration.listSTT());
		typeDisplay = portletPreferences.getValue("typeDisplay", formConfiguration.typeDisplay());
	}
	%>