<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.swt.navigation.menu.page.util.NavigationMenuPagePrivateUtil"%>
<%@page import="com.liferay.portal.kernel.model.Layout"%>
<%@page import="java.util.List"%>
<%@page
	import="com.liferay.portal.kernel.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="init.jsp"%>

<style>
	.css-config {
		margin-top: 20px;
		padding: 15px;
	}
	.div-list-layout{
		width: 50%;
		height: 250px;
		border: 1px solid #ddd;
		padding: 10px; 
		overflow-y: auto;
		margin-bottom: 10px;
		margin-top: 10px;
	}
</style>
<%
	List<Layout> listResult = NavigationMenuPagePrivateUtil.getListPagePrivate(themeDisplay);
	
	// key idLayout, value Stt
	HashMap<Long, Integer> listMap = new HashMap<Long, Integer>();
	if(listPage!=""){	
		String [] listIdLayout = listPage.split("/");
		String[] listSTTChecked = listSTT.split("/");
		for(int i=0; i< listSTTChecked.length; i++){
			listMap.put(GetterUtil.getLong(listIdLayout[i]), GetterUtil.getInteger(listSTTChecked[i]));
		}
	}
%>
<liferay-portlet:actionURL portletConfiguration="<%=true%>"
	var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%=true%>"
	var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" name="fm"
	id="myFormConfig">
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden"
			value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden"
			value="<%=configurationRenderURL%>" />
		<aui:fieldset cssClass="panel panel-default css-config">
			<%=LanguageUtil.format(request, "menu.listpage", new Object())%>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<td style="text-align: center;"><%=LanguageUtil.format(request, "menu.nameMenu", new Object())%></td>
							<td style="text-align: center;"><%=LanguageUtil.format(request, "STT", new Object())%></td>
						</tr>
					</thead>
					<tbody>
						<%
						for (Layout object : listResult) {
							if(listMap.containsKey(object.getPlid())){
							for(Map.Entry<Long, Integer> entry: listMap.entrySet()){ 
									if(entry.getKey()==object.getPlid()){
									%>
								<tr>
									<td>
									<input checked="checked"  name="checkBoxNameMenu"
										onchange="onChangeLayout(<%=object.getPlid()%>)"
										type="checkbox" value="<%=object.getPlid()%>"
										id="<%=object.getPlid()%>"> <%=object.getName(themeDisplay.getLocale())%>
									</td>
									<td style="text-align: center;"><input style="float: right;" type="number"
										id="giaTriSoSanh<%=object.getPlid()%>" min="0" max="9"
										name="giaTriSoSanh" value="<%=String.valueOf(entry.getValue())%>">
									</td>
								</tr>
							<%			}
								}
							%>
							<%}else{ %>
								<tr>
									<td>
									<input  name="checkBoxNameMenu"
										onchange="onChangeLayout(<%=object.getPlid()%>)"
										type="checkbox" value="<%=object.getPlid()%>"
										id="<%=object.getPlid()%>"> <%=object.getName(themeDisplay.getLocale())%>
									</td>
									<td style="text-align: center;"><input style="float: right;" type="number"
										id="giaTriSoSanh<%=object.getPlid()%>" min="0" max="9"
										name="giaTriSoSanh">
									</td>
								</tr>
							<%} %>
						
						<%} %>
					</tbody>
				</table>
			<%=LanguageUtil.format(request, "menu.display", new Object())%>
			<aui:select id="typeDisplay" name="typeDisplay" label=""
				value="<%=typeDisplay%>">
				<aui:option value="">
				 ---Display menu 1---
				 </aui:option>

			</aui:select>
			<aui:input type="hidden" name="listPage" id="listPage" value="<%=listPage%>"></aui:input>
			<aui:input type="hidden" name="listSTT" id="listSTT" value="<%=listSTT%>"></aui:input>
		</aui:fieldset>
		<button type="button" class="btn btn-primary btn-default" onClick="saveConfig()"><%=LanguageUtil.format(request, "config.OK", new Object()) %></button>
	</div>
</aui:form>
<script>
	function onChangeLayout(id){
		var checkboxes = document.getElementById(id);
		if(checkboxes.checked){
			document.getElementById('giaTriSoSanh' +id).value = '0';
		} else {
		    document.getElementById('giaTriSoSanh' +id).value = '';
		} 
	}
	
	function saveConfig(){
		var checkBoxNameMenu = document.getElementsByName("checkBoxNameMenu");
		var check = document.getElementsByName("giaTriSoSanh");
		var giaTriChecked = "";
		var listSTT = "";
		for (var i=0; i<checkBoxNameMenu.length; i++) {
		     // And stick the checked ones onto an array...
		     if (checkBoxNameMenu[i].checked) {
		    	 giaTriChecked += checkBoxNameMenu[i].value + "/";
		    	 listSTT += document.getElementById('giaTriSoSanh' +checkBoxNameMenu[i].value).value + "/";
		     }
		  }
		  $("#<portlet:namespace/>listPage").val(giaTriChecked);
		  $("#<portlet:namespace/>listSTT").val(listSTT);
		document.<portlet:namespace/>fm.submit();
		
	}


</script>