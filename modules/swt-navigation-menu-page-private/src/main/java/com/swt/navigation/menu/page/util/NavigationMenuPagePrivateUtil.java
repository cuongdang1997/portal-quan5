package com.swt.navigation.menu.page.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.permission.LayoutPermission;
import com.liferay.portal.kernel.service.permission.LayoutPermissionUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;

public class NavigationMenuPagePrivateUtil {
	// get list menu page
	public static List<Layout> getListPagePrivate(ThemeDisplay themeDisplay) throws Exception {
		List<Layout> listResult = new ArrayList<Layout>();
		List<Layout> listLayouFather = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), true,0);
		for (Layout object : listLayouFather) {
		
			 if (object.getHidden()==false) {
				 listResult =  object.getChildren();
			 }
		}	 
		return listResult;
	}
	// get  menu page current 
	public static Layout getPagePrivateCurrent(ThemeDisplay themeDisplay ,long plidCurrent) throws Exception {
		Layout objectLayout = LayoutLocalServiceUtil.getLayout(plidCurrent);
		Layout objectLayoutCurrent = null;
		if(objectLayout.hasChildren()) {
			// page current
			objectLayoutCurrent= LayoutLocalServiceUtil.getLayout(plidCurrent);
		}else{
			// search page father current
			objectLayoutCurrent = LayoutLocalServiceUtil.getLayout(objectLayout.getParentPlid());
		}
		
		return objectLayoutCurrent;
	}
	// sort by value 
		public static HashMap<Long,Integer> sortByValues(HashMap<Long,Integer> map) { 
		       List list = new LinkedList(map.entrySet());
		       // Defined Custom Comparator here
		       Collections.sort(list, new Comparator() {
		            public int compare(Object o1, Object o2) {
		               return ((Comparable) ((Map.Entry) (o1)).getValue())
		                  .compareTo(((Map.Entry) (o2)).getValue());
		            }
		       });

		       HashMap<Long,Integer> sortedHashMap = new LinkedHashMap<Long, Integer>();
		       for (Iterator it = list.iterator(); it.hasNext();) {
		              Map.Entry entry = (Map.Entry) it.next();
		              sortedHashMap.put(GetterUtil.getLong(entry.getKey()), GetterUtil.getInteger(entry.getValue()));
		       } 
		     
		       return sortedHashMap;
		  }
}
