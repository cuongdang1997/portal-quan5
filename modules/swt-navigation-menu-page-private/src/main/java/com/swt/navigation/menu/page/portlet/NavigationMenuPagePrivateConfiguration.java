package com.swt.navigation.menu.page.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;
@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(factory = true,
		id = "com.swt.navigation.menu.page.portlet.NavigationMenuPagePrivateConfiguration",
		localization = "content/Language")
public interface NavigationMenuPagePrivateConfiguration {
	@Meta.AD(deflt = "", required = false)
	public String listPage();
	
	@Meta.AD(deflt = "", required = false)
	public String listSTT();
	
	@Meta.AD(deflt = "", required = false)
	public String typeDisplay();
}
