package com.swt.navigation.menu.page.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.swt.navigation.menu.page.constants.NavigationMenuPagePrivatePortletKeys;

@Component(
	    configurationPid = "com.swt.navigation.menu.page.portlet.NavigationMenuPagePrivateConfiguration",
	    configurationPolicy = ConfigurationPolicy.OPTIONAL,
	    immediate = true,
	    property = {
	        "javax.portlet.name=" + NavigationMenuPagePrivatePortletKeys.navigationMenuPagePrivatePortlet
	    },
	    service = ConfigurationAction.class
	)
public class NavigationMenuPagePrivateConfigurationAction extends DefaultConfigurationAction{
	@Override
    public void processAction(
            PortletConfig portletConfig, ActionRequest actionRequest,
            ActionResponse actionResponse)
        throws Exception {

        String listPage = ParamUtil.getString(actionRequest, "listPage");
        setPreference(actionRequest, "listPage", listPage);
        
        String listSTT = ParamUtil.getString(actionRequest, "listSTT");
        setPreference(actionRequest, "listSTT", listSTT);
        
        String typeDisplay = ParamUtil.getString(actionRequest, "typeDisplay");
        setPreference(actionRequest, "typeDisplay", typeDisplay);

        super.processAction(portletConfig, actionRequest, actionResponse);
    }

    @Override
    public void include(
        PortletConfig portletConfig, HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse) throws Exception {

        httpServletRequest.setAttribute(
        		NavigationMenuPagePrivateConfiguration.class.getName(),
            _formConfiguration);

        super.include(portletConfig, httpServletRequest, httpServletResponse);
    }

    @Activate
    @Modified
    protected void activate(Map<Object, Object> properties) {
    	_formConfiguration = ConfigurableUtil.createConfigurable(
    		   NavigationMenuPagePrivateConfiguration.class, properties);
    }

   private volatile NavigationMenuPagePrivateConfiguration _formConfiguration;

}
