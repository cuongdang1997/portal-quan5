package com.swt.navigation.menu.page.constants;

/**
 * @author Nhut.Dang
 */
public class NavigationMenuPagePrivatePortletKeys {

	public static final String navigationMenuPagePrivate = "HCMGov Navigation Menu Page Private";
	public static final String navigationMenuPagePrivatePortlet = "SWTNavigationMenuPagePrivate";

}