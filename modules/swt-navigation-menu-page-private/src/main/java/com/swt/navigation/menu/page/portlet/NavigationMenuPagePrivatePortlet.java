package com.swt.navigation.menu.page.portlet;

import com.swt.navigation.menu.page.constants.NavigationMenuPagePrivatePortletKeys;


import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.util.Map;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

/**
 * @author Nhut.Dang
 */
@Component( configurationPid = "com.swt.navigation.menu.page.portlet.NavigationMenuPagePrivateConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.portal",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=" + NavigationMenuPagePrivatePortletKeys.navigationMenuPagePrivate,
		"javax.portlet.init-param.config-template=/configuration.jsp",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + NavigationMenuPagePrivatePortletKeys.navigationMenuPagePrivatePortlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user", "Language=properties"
	},
	service = Portlet.class
)
public class NavigationMenuPagePrivatePortlet extends MVCPortlet {
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		renderRequest.setAttribute(NavigationMenuPagePrivateConfiguration.class.getName(), formConfiguration);

		super.doView(renderRequest, renderResponse);
	}

	public String getListPage(Map labels) {
		return (String) labels.get(formConfiguration.listPage());
	}
	public String getListSTT(Map labels) {
		return (String) labels.get(formConfiguration.listSTT());
	}
	public String getTypeDisplay(Map labels) {
		return (String) labels.get(formConfiguration.typeDisplay());
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		formConfiguration = ConfigurableUtil.createConfigurable(NavigationMenuPagePrivateConfiguration.class, properties);
	}

	private volatile NavigationMenuPagePrivateConfiguration formConfiguration;
	
}

