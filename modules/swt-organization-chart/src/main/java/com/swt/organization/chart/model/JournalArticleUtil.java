package com.swt.organization.chart.model;

import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public interface JournalArticleUtil {

	List<JournalArticle> search(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay, String structurekey, PortletRequest portletRequest);
	int searchCount(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay, String structurekey, long categoryId, PortletRequest portletRequest);
	List<JournalArticle> search(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay, String structureKey,
			long parrentLevelID, PortletRequest portletRequest);
	int searchCount(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay, String structureKey,
			long categoryId, long parrentLevelId, PortletRequest portletRequest);
	List<JournalArticle> search(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay,
			String structureKey, long parrentLevelID, PortletRequest portletRequest, boolean inclParrent);
}
