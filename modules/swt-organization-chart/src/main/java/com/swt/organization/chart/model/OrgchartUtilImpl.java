package com.swt.organization.chart.model;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

public class OrgchartUtilImpl implements OrgchartUtil {

	@Override
	public List<OrgchartUnitDTO> search(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay,
			String structurekey, PortletRequest portletRequest) {
		JournalArticleUtil journalArticleUtil = new JournalArticleUtilImpl();
		List<JournalArticle> journalArticles = journalArticleUtil.search(searchContainer, themeDisplay, structurekey, portletRequest);
		
		List<OrgchartUnitDTO> orgchartUnitDTOs = new ArrayList<OrgchartUnitDTO>();
		
		for(JournalArticle journalArticle:journalArticles) {
			orgchartUnitDTOs.add(convertJournalToOrgchartUnit(journalArticle));
		}
		
		return orgchartUnitDTOs;

	}

	@Override
	public int searchCount(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay, String structurekey,
			long categoryId, PortletRequest portletRequest) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<OrgchartUnitDTO> search(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay,
			String structureKey, long parrentLevelID, PortletRequest portletRequest) {
		JournalArticleUtil journalArticleUtil = new JournalArticleUtilImpl();
		List<JournalArticle> journalArticles = journalArticleUtil.search(searchContainer, themeDisplay, structureKey, parrentLevelID, portletRequest);
		
		List<OrgchartUnitDTO> orgchartUnitDTOs = new ArrayList<OrgchartUnitDTO>();
		
		for(JournalArticle journalArticle:journalArticles) {
			orgchartUnitDTOs.add(convertJournalToOrgchartUnit(journalArticle));
		}
		
		return orgchartUnitDTOs;

	}

	@Override
	public int searchCount(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay, String structureKey,
			long categoryId, long parrentLevelId, PortletRequest portletRequest) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<OrgchartUnitDTO> search(JournalArticleContainer searchContainer, ThemeDisplay themeDisplay,
			String structureKey, long parrentLevelID, PortletRequest portletRequest, boolean inclParrent) {
		JournalArticleUtil journalArticleUtil = new JournalArticleUtilImpl();
		List<JournalArticle> journalArticles = journalArticleUtil.search(searchContainer, themeDisplay, structureKey,parrentLevelID, portletRequest,inclParrent);
		
		List<OrgchartUnitDTO> orgchartUnitDTOs = new ArrayList<OrgchartUnitDTO>();
		
		for(JournalArticle journalArticle:journalArticles) {
			orgchartUnitDTOs.add(convertJournalToOrgchartUnit(journalArticle));
		}
		
		return orgchartUnitDTOs;
	}

	@Override
	public OrgchartUnitDTO convertJournalToOrgchartUnit(JournalArticle journalArticle) {
		// TODO Auto-generated method stub

		com.liferay.portal.kernel.xml.Document document;
		try {
			
			OrgchartUnitDTO orgchartUnitDTo = new OrgchartUnitDTO();
			
			document = SAXReaderUtil.read(journalArticle.getContent());
			Node node = document.selectSingleNode("/root/dynamic-element[@name='level']/dynamic-content");
			orgchartUnitDTo.setParentId(GetterUtil.getLong(node.getStringValue(),0));
			
			node = document.selectSingleNode("/root/dynamic-element[@name='name']/dynamic-content");
			orgchartUnitDTo.setName(node.getStringValue());
			
			node = document.selectSingleNode("/root/dynamic-element[@name='address']/dynamic-content");
			orgchartUnitDTo.setAddress(node.getStringValue());
			
			node = document.selectSingleNode("/root/dynamic-element[@name='desciption']/dynamic-content");
			orgchartUnitDTo.setDescription(node.getStringValue());
			
			node = document.selectSingleNode("/root/dynamic-element[@name='phone']/dynamic-content");
			orgchartUnitDTo.setPhoneNumber(node.getStringValue());
			
			node = document.selectSingleNode("/root/dynamic-element[@name='fax']/dynamic-content");
			orgchartUnitDTo.setFaxNumber(node.getStringValue());
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	

}
