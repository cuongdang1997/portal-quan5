package com.swt.organization.chart.model;

public class JournalAricleConstants {

	public static final String PORTLET_NAME = "SWTOrganizationChart";
	public static final String PORTLET_DISPLAY_NAME = "HCMGov Organization Chart";

	public static final int SEARCH_TYPE_EQUAL = 1;
	public static final int SEARCH_TYPE_RANGE = 2;
	
	public static final long TABLE_STYLE_1 = 1;
	public static final long TABLE_STYLE_2_ONLYCHILD = 3;
	public static final long CHART_STYLE_1 = 2; //View org chart for Don vi
	public static final long CHART_STYLE_2 = 4; //View org chart for human resource
	
	public static final String ACTION_ADD_ORGANIZATION = "addOrganization";
	public static final String ACTION_UPDATE_ORGANIZATION = "updateOrganization";
	public static final String ACTION_DO_VIEW = "doView";
	public static final String ACTION_REMOVE_ORGANIZATION = "deleteOrganization";
	public static final String ACTION_DO_VIEW_ORGANIZATION = "doViewOrganization";
	//public static final String ACTION_REMOVE_ORGANIZATION = "deleteOrganization";

	
}