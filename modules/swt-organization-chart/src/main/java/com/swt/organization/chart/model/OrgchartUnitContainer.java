package com.swt.organization.chart.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.swt.organization.chart.model.OrgchartUnitDTO;

public class OrgchartUnitContainer extends SearchContainer<OrgchartUnitDTO> {
	static List<String> headerNames = new ArrayList<>();
	static Map<String, String> orderableHeaders = new HashMap<String, String>();

	static {
		headerNames.add("status");
		headerNames.add("alias");
	}

	public static final String EMPTY_RESULTS_MESSAGE = "there-are-no-results";

	public OrgchartUnitContainer(PortletRequest portletRequest, PortletURL iteratorURL) {
		
		super(portletRequest, new CategoryDisplayTerms(portletRequest), new CategoryDisplayTerms(portletRequest),
				DEFAULT_CUR_PARAM, DEFAULT_DELTA, iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);
		
		CategoryDisplayTerms displayTerms = (CategoryDisplayTerms) getDisplayTerms();
		iteratorURL.setParameter("status", String.valueOf(displayTerms.getStatus()));
		iteratorURL.setParameter("name", displayTerms.getName());

	}
}
