package com.swt.organization.chart.model;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.dao.search.DisplayTerms;

public class JournalArticleDisplayTerms extends DisplayTerms {

	public JournalArticleDisplayTerms(PortletRequest portletRequest) {
		super(portletRequest);
	}
	
}
