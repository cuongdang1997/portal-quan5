package com.swt.organization.chart.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleResourceLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.swt.organization.chart.model.CategoryConstants;

public class JournalArticleUtil {

	
	public enum SortOrder
	 {
	   Ascending,
	   Descending
	 }
		//get articles by root level
	public static List<JournalArticle> getArticlesbyRootLevel(List<JournalArticle> journalArticles, long rootLevel) {
		List<JournalArticle> orgCharts = new ArrayList<JournalArticle>();
		com.liferay.portal.kernel.xml.Document document;
		
		//get root item
		for (JournalArticle journalArticle : journalArticles) {
			
			if (journalArticle.getId() == rootLevel) {
				orgCharts.add(journalArticle);
				break;
			}

		}
		
		com.liferay.portal.kernel.xml.Document documentArticle;
		for (JournalArticle article : journalArticles) {

			try {
				documentArticle = SAXReaderUtil.read(article.getContent());
				Node nodeArticle = documentArticle
						.selectSingleNode("/root/dynamic-element[@name='level']/dynamic-content");
				if (nodeArticle != null) {
					if (GetterUtil.getLong(nodeArticle.getStringValue()) == orgCharts.get(0).getId()) {
						orgCharts.add(article);
					}
				}
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}

		}
		return orgCharts;
		
	}

	//sort to draw as level 0
	public static List<JournalArticle> sortByLevel(List<JournalArticle> journalArticles) {

		List<JournalArticle> orgCharts = new ArrayList<JournalArticle>();
		com.liferay.portal.kernel.xml.Document document;
		//System.out.println("sortByLevel:" +orgCharts.size());
		for (int j=journalArticles.size()-1;j>=0 ;j--){
			JournalArticle journalArticle = journalArticles.get(j);
			try {
				//System.out.println("orgCharts.size() 1:" +orgCharts.size());
				document = SAXReaderUtil.read(journalArticle.getContent());
				Node node = document.selectSingleNode("/root/dynamic-element[@name='level']/dynamic-content");
				if (node != null) {
					// System.out.println("node.getStringValue():" +node.getStringValue());
					if (node.getStringValue().equalsIgnoreCase("0")) {
						orgCharts.add(journalArticle);
						journalArticles.remove(j);
					}
				}
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Sort articles by level to have the article list as: parent,child,parent,child
		for (int i = 0; i < orgCharts.size(); i++) {
			//System.out.println("orgCharts.size()2: " +orgCharts.size());
			// System.out.println("orgCharts.size():" +orgCharts.size());
			com.liferay.portal.kernel.xml.Document documentArticle;
			try {
				int index = i + 1;
				for (int j=journalArticles.size() -1;j>=0 ;j--){
					JournalArticle article = journalArticles.get(j);
					//for (JournalArticle article : journalArticles) {

					documentArticle = SAXReaderUtil.read(article.getContent());
					Node nodeArticle = documentArticle
							.selectSingleNode("/root/dynamic-element[@name='level']/dynamic-content");
					if (nodeArticle != null) {
						
						if (GetterUtil.getLong(nodeArticle.getStringValue()) == orgCharts.get(i).getId()) {
							orgCharts.add(index, article);
							journalArticles.remove(j);
						}
						
					}else {
						//Remove if content is null
						journalArticles.remove(j);
					}
				}
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();		
			}

		}
		
		//These articles remain is missing root level, should organize again
		if(journalArticles.size() > 0) {
			//System.out.println("orgCharts.size() 3:" +orgCharts.size());
			orgCharts.addAll(journalArticles);
		}
		
		return orgCharts;
	}

	//This method sort by property of DDM structure
	
	public static List<JournalArticle> sort(String structureKey, ThemeDisplay themeDisplay, List<JournalArticle> journalArticles, String compareOption,
				SortOrder order) {
			System.out.println("Sort is called, compareOption " + compareOption);
			long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
			try {
				DDMStructure structureObject = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(),
						journalArticleClassNameId , structureKey);
				
				//DDM
				List<com.liferay.dynamic.data.mapping.model.DDMFormField> ddmFormFields = new ArrayList<com.liferay.dynamic.data.mapping.model.DDMFormField>();
				if (structureObject != null) {
					ddmFormFields = structureObject.getDDMFormFields(false);
				}
				
				for (com.liferay.dynamic.data.mapping.model.DDMFormField objectField :ddmFormFields ) {
					//System.out.println("objectField, " + objectField.getName().trim());
					
					if (objectField.getName().trim().equalsIgnoreCase(compareOption)) {
						//System.out.println("Start sort compareOption " + compareOption);
						journalArticles.sort((journalArticle1, journalArticle2) -> {
							int result =0;
							try {
								com.liferay.portal.kernel.xml.Document document1 = SAXReaderUtil.read(journalArticle1.getContent());
								com.liferay.portal.kernel.xml.Document document2 = SAXReaderUtil.read(journalArticle2.getContent());
								
								Node node1 = document1.selectSingleNode("/root/dynamic-element[@name='" +compareOption +"']/dynamic-content");
								Node node2 = document2.selectSingleNode("/root/dynamic-element[@name='" +compareOption +"']/dynamic-content");
								
								String value1 = node1.getStringValue();
								String value2 = node2.getStringValue();
								
								
								
								switch(objectField.getType()) {
								   case "text" :
								      // Statements
									   result = value1.compareTo(value2);
								      break; // optional
								   
								   case "ddm-integer" :
									   System.out.println("Sort Type ddm-integer" );
									   long v1 = GetterUtil.getLong(value1);
									   long v2 = GetterUtil.getLong(value2);
									   if(v1 == v2) result = 0;
									   result = v1>v1?1:-1;
								      break; // optional
								   case "ddm-date" :
									      // Statements
									   result = value1.compareTo(value2);
									   break; // optional
								   // You can have any number of case statements.
								   default : // Optional
								      // Statements
								}
								
								
							} catch (DocumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if(order == SortOrder.Descending) {
								result = -result;
							}
							
							return result;
						});
						
						
					}
					
				}
				
			} catch (PortalException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			
			return journalArticles;
			
		}
	public static List<AssetCategory> sort(List<AssetCategory> categoriesGroupArticle){
		categoriesGroupArticle.sort((cate1, cate2) ->{
			try {
				AssetCategoryProperty assetCate1 = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(
						cate1.getCategoryId(), CategoryConstants.CATEGORY_PROPERTY_SEQUENCE);
				AssetCategoryProperty assetCate2 = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(
						cate2.getCategoryId(), CategoryConstants.CATEGORY_PROPERTY_SEQUENCE);
				
				long val1 = GetterUtil.getLong(assetCate1.getValue(),0);
				long val2 = GetterUtil.getLong(assetCate2.getValue(),0);
				
				if(val1 == val2) return 0;
				
				return val1>val2?1:-1;
			}catch(PortalException ex){
				
			}
			return 0;
		});
		return categoriesGroupArticle;
	}
	
	public static JournalArticle createJournalArticle(ThemeDisplay themeDisplay, ActionRequest request, String structure, String template, String title,
			String contentLast, long idArticle, long categoryId) throws PortalException {
		JournalArticle article = null;
		
		//first: create article
		article = createJournalArticle(themeDisplay,request, structure, title, contentLast, idArticle, categoryId);
		if(article == null) {
			throw new PortalException("Error when creating new article");
		}else {
			//article.setFolderId(folderId);
			JournalArticleLocalServiceUtil.updateJournalArticle(article);
		}
		
		return article;
	}
	
	///Create article with categoryId
	public static JournalArticle createJournalArticle(ThemeDisplay themeDisplay, ActionRequest request, String structureKey, String title, String contentLast,
			long idArticle, long categoryId)
	
			throws PortalException {
			double version = 1;
			long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
			
			String articleId = String.valueOf(CounterLocalServiceUtil.increment());
			
			// @param titleMap
			Map<Locale, String> titleMap = new HashMap<Locale, String>();
			titleMap.put(themeDisplay.getLocale(), title);
			
			// @param ddmTemplateKey
			//DDMStructure structureObjetc = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureId));
			DDMStructure structureObjetc = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(),
					journalArticleClassNameId , structureKey);
			List<DDMTemplate> listTemplate = structureObjetc.getTemplates();
			String ddmTemplateKey = "";
			
			  if(listTemplate.size()!=0) {
				  if(listTemplate.get(0).getType().equals("display")) { 
					  ddmTemplateKey = listTemplate.get(0).getTemplateKey(); 
				  } 
			  } 
			  
			 // @param layoutUuid
			String layoutUuid = "";
			List<Layout> listLayoutParrent = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), false,
					themeDisplay.getLayout().getLayoutId());
			if (listLayoutParrent.size() != 0) {
				for (Layout object : listLayoutParrent) {
					if (object.isContentDisplayPage()) {
						layoutUuid = object.getUuid();
					}
				}
			}
			
			// @param folderIdJounalArticle
			long folderId = 0;
			try {
				folderId =	DDMStructureUtil.getIdJournalFolderByStructureName(themeDisplay, request,structureObjetc.getName(themeDisplay.getLocale()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			//Map image
			Map<String, byte[]> byteImage = null;
			ServiceContext serviceContextArticle = ServiceContextFactory.getInstance(JournalArticle.class.getName(),
					request);
			File file = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			
			try {
				JournalArticle objectArticle = null;
				if(idArticle>=0) {
					//articleId = GetterUtil.getString(idArticle);
					objectArticle = JournalArticleLocalServiceUtil.getArticle(GetterUtil.getLong(idArticle));
					AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), objectArticle.getResourcePrimKey());
					
					//objectArticle.setSmallImageURL(smallImageURL);
					//Reset all category 
					List<AssetCategory> assetCategories =  entry.getCategories();
					for(AssetCategory assetCategory:assetCategories) {
						AssetEntryLocalServiceUtil.deleteAssetCategoryAssetEntry(assetCategory.getCategoryId(), entry);
					}
					
					AssetEntryLocalServiceUtil.addAssetCategoryAssetEntry(categoryId, entry.getEntryId());
					//objectArticle.setTreePath(objectArticle.buildTreePath());
					objectArticle.setTitle(title);
					objectArticle.setContent(contentLast);
					
					JournalArticleLocalServiceUtil.updateJournalArticle(objectArticle);
				}else {
					objectArticle =	JournalArticleLocalServiceUtil.addArticle(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
							GetterUtil.getLong(folderId), GetterUtil.getLong(0), structureObjetc.getPrimaryKey(), 
							articleId, true, version, titleMap, titleMap, contentLast, structureKey, ddmTemplateKey, layoutUuid, cal.get(Calendar.MONTH),
							cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), 0, 0, 0, 0, 0, 
							true, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY),
							cal.get(Calendar.MINUTE), false, true, false, "", file, byteImage, "", serviceContextArticle);
					AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), objectArticle.getResourcePrimKey());
					AssetEntryLocalServiceUtil.addAssetCategoryAssetEntry(categoryId, entry.getEntryId());
				}
					
				return objectArticle;
				
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
		
	}
	

	public static JournalArticle createJournalArticle(ThemeDisplay themeDisplay, String structureKey, String template, String title,
			String contentLast, String idArticle) throws PortalException {
		ServiceContext serviceContext = new ServiceContext();
		try {
			long userId = 0;
			String userName = "";
			if (themeDisplay.isSignedIn()) {
				userId = themeDisplay.getUserId();
				userName = themeDisplay.getUser().getLastName() + themeDisplay.getUser().getMiddleName()
						+ themeDisplay.getUser().getFirstName();
			}
			double version = 1;
			String articleId = "";
			long id = 0;
			JournalArticle article = null;
			if (idArticle == null) {
				articleId = String.valueOf(CounterLocalServiceUtil.increment());
				id = CounterLocalServiceUtil.increment();
				article = JournalArticleLocalServiceUtil.createJournalArticle(id);
				long resourcePrimKey = JournalArticleResourceLocalServiceUtil
						.getArticleResourcePrimKey(themeDisplay.getScopeGroupId(), articleId);

				createJournalArticleResource(themeDisplay, resourcePrimKey, articleId);
				ResourceAction actionObject = ResourceActionLocalServiceUtil.fetchResourceAction("136",
						ActionKeys.ACCESS_IN_CONTROL_PANEL);
				createResourcePermission(themeDisplay, JournalArticle.class.getName(), resourcePrimKey,
						String.valueOf(actionObject.getResourceActionId()));
				article.setResourcePrimKey(resourcePrimKey);

			} else {
				articleId = GetterUtil.getString(idArticle);
				article = JournalArticleLocalServiceUtil.getArticle(GetterUtil.getLong(idArticle));
			}

			article.setUuid(serviceContext.getUuid());

			article.setArticleId(articleId);
			article.setCompanyId(themeDisplay.getCompanyId());
			article.setGroupId(themeDisplay.getScopeGroupId());
			article.setDDMStructureKey(structureKey);
			article.setTemplateId(template);
			article.setCreateDate(new Date());
			article.setTreePath(article.buildTreePath());
			article.setTitle(title);
			article.setContent(contentLast);
			article.setUserId(themeDisplay.getUserId());
			article.setUserName(userName);
			article.setIndexable(true);
			article.setVersion(version);
			if (idArticle == null) {
				JournalArticleLocalServiceUtil.addJournalArticle(article);
			} else {
				JournalArticleLocalServiceUtil.updateJournalArticle(article);
			}
			return article;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	public static JournalArticleResource createJournalArticleResource(ThemeDisplay themeDisplay, long resourcePrimKey,
			String articleId) {
		JournalArticleResource object = JournalArticleResourceLocalServiceUtil
				.createJournalArticleResource(resourcePrimKey);
		object.setArticleId(articleId);
		object.setCompanyId(themeDisplay.getCompanyId());
		object.setGroupId(themeDisplay.getScopeGroupId());
		JournalArticleResourceLocalServiceUtil.addJournalArticleResource(object);
		return object;
	}

	public static ResourcePermission createResourcePermission(ThemeDisplay themeDisplay, String name,
			long resourcePrimKey, String permissions) {
		long resourcePermissionId = CounterLocalServiceUtil.increment();
		ResourcePermission object = ResourcePermissionLocalServiceUtil.createResourcePermission(resourcePermissionId);
		Role role = RoleLocalServiceUtil.fetchRole(themeDisplay.getCompanyId(), RoleConstants.ADMINISTRATOR);
		object.setName(name);
		object.setRoleId(role.getRoleId());
		object.setCompanyId(themeDisplay.getCompanyId());
		object.setScope(4);
		object.setPrimKey(String.valueOf(resourcePrimKey));
		object.setPrimKeyId(resourcePrimKey);
		object.setActionIds(GetterUtil.getLong(permissions));
		object.setViewActionId(true);

		ResourcePermissionLocalServiceUtil.addResourcePermission(object);
		return object;
	}

	public static String setContent(List<DDMFormField> listFormFile, Map<String, String> listParameter) {
		String contentArticle = "";
		String type = "";
		for (DDMFormField object : listFormFile) {
			type = object.getType();
			if (object.getType().equals("select")) {
				type = "list";
			}

			if (object.getType().equals("text")) {
				type = "text";
			}
			
			for (Map.Entry<String, String> s : listParameter.entrySet()) {
				
				if (object.getName() == s.getKey()) {
					System.out.println(object.getName() + " " + s.getValue());
					contentArticle += "	<dynamic-element name=\"" + object.getName() + "\" type=\"" + type
							+ "\" index-type=\"" + object.getIndexType() + "\" instance-id=\"" + object.getName()
							+ "\">\r\n" + "		<dynamic-content language-id=\"vi_VN\"><![CDATA[" + s.getValue()
							+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n";
				}
			}
		}
		String xml = "<?xml version=\"1.0\"?>\r\n" + "\r\n"
				+ "<root available-locales=\"vi_VN\" default-locale=\"vi_VN\">\r\n" + contentArticle + "</root>";

		return xml;

	}

}
