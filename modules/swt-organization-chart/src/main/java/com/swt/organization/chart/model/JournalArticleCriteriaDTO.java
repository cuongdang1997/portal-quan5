package com.swt.organization.chart.model;

public class JournalArticleCriteriaDTO implements Comparable<JournalArticleCriteriaDTO> {

	private String type;
	private String name;
	private String label;
	private int searchType;
	private int sequence;

	public JournalArticleCriteriaDTO(String type, int searchType, String name, String label, int sequence) {
		super();
		this.type = type;
		this.searchType = searchType;
		this.name = name;
		this.label = label;
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	public int compareTo(JournalArticleCriteriaDTO o) {
		return sequence - o.getSequence();
	}

}
