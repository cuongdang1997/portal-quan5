package com.swt.organization.chart.model;

import java.util.Date;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

public class CategoryDTO {

	private long id;
	private long parentId;
	private int deepLevel;
	private String name;
	private String description;
	private int status;
	private int sequence;
	private long iconId;
	private String createUser;
	private Date createDate;
	private Date modifiedDate;

	public CategoryDTO() {
		super();
	}

	public CategoryDTO(long id, long parentId, int deepLevel, String name, String description, int status, int sequence, long iconId, String createUser,
			Date createDate, Date modifiedDate) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.deepLevel = deepLevel;
		this.name = name;
		this.description = description;
		this.status = status;
		this.sequence = sequence;
		this.iconId = iconId;
		this.createUser = createUser;
		this.createDate = createDate;
		this.modifiedDate = modifiedDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	
	public int getDeepLevel() {
		return deepLevel;
	}

	public void setDeepLevel(int deepLevel) {
		this.deepLevel = deepLevel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	
	public String getPlainDescription() {
		if (description != null && description.length() != 0) {
			Document descDoc = null;
			try {
				descDoc = SAXReaderUtil.read(description);
			} catch (DocumentException e) {
				e.printStackTrace();
				// The description is not in XML format, just return it.
				return description;
			}
			
			// Since HCM Portal only uses Vietnamese, I ignored the language checking.
			Node descNode = descDoc.selectSingleNode("/root/Description");
			if (descNode != null) {
				return descNode.getText();
			}
		}
		
		return StringPool.BLANK;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	public long getIconId() {
		return iconId;
	}

	public void setIconId(long iconId) {
		this.iconId = iconId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
