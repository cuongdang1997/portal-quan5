package com.swt.organization.chart.model;

public class JournalArticleColumnDTO implements Comparable<JournalArticleColumnDTO> {

	private String type;
	private String name;
	private String label;
	private int sequence;

	public JournalArticleColumnDTO(String type, String name, String label, int sequence) {
		super();
		this.type = type;
		this.name = name;
		this.label = label;
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	public int compareTo(JournalArticleColumnDTO o)
	{
	     return sequence - o.getSequence();
	}
	
}
