package com.swt.organization.chart.util;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletRequest;

import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.swt.organization.chart.model.CategoryConstants;

/**
 * The utility class for working with Document & Media files in this portlet.
 * 
 * @author dungnt
 */
public class CategoryFileUtils {

	/**
	 * Create a new file entry or update if it already exists.
	 * 
	 * @param primaryKeyId     The resource primary key.
	 * @param repositoryId     The file repository ID.
	 * @param parentFolderPath The paremt folder path.
	 * @param folderName       The name of folder which stores the file.
	 * @param fileName         The file name to be added or updated.
	 * @param fileType         The file MIME type.
	 * @param inputStream      The input stream which holds file data.
	 * @param fileSize         The file size.
	 * @param request          The portlet report.
	 * 
	 * @return The newly-added or updated file entry.
	 */
	public static FileEntry createOrUpdateFileEntry(long primaryKeyId, long repositoryId, String parentFolderPath,
			String folderName, String fileName, String fileType, InputStream inputStream, long fileSize,
			PortletRequest request) throws PortalException {
		ServiceContext folderSctx = ServiceContextFactory.getInstance(Folder.class.getName(), request);
		ServiceContext fileSctx = ServiceContextFactory.getInstance(FileEntry.class.getName(), request);

		String folderPath = parentFolderPath + File.pathSeparator + folderName;
		Folder folder = getOrCreateFolder(repositoryId, folderPath, folderSctx);
		long folderId = folder.getFolderId();
		String fileTitle = getFileTile(primaryKeyId, fileName);

		// Check if the file already exists.
		DLFileEntry existingDlFileEntry = null;
		try {
			existingDlFileEntry = DLFileEntryLocalServiceUtil.getFileEntry(repositoryId, folderId, fileTitle);
		} catch (PortalException e) {
			// This exception will be thrown if the file does not exist.
			// In the case, just do nothing.
		}

		// If file does not exist, create it; Otherwise, update it.
		FileEntry fileEntry = null;
		if (existingDlFileEntry == null) {
			fileEntry = DLAppLocalServiceUtil.addFileEntry(fileSctx.getUserId(), repositoryId, folderId, fileName,
					fileType, fileTitle, StringPool.BLANK, StringPool.BLANK, inputStream, fileSize, fileSctx);
		} else {
			fileEntry = DLAppLocalServiceUtil.updateFileEntry(fileSctx.getUserId(),
					existingDlFileEntry.getFileEntryId(), fileName, fileType, fileTitle, StringPool.BLANK,
					StringPool.BLANK, true, inputStream, fileSize, fileSctx);
		}

		return fileEntry;
	}

	/**
	 * Construct a new title for the given file name.
	 * 
	 * @param primaryKeyId The resource primary key ID.
	 * @param fileName     The original file name.
	 * 
	 * @return File tile.
	 */
	public static String getFileTile(long primaryKeyId, String fileName) {
		return primaryKeyId + StringPool.UNDERLINE + fileName;
	}

	/**
	 * Get or create the given folder.
	 * 
	 * @param repositoryId         The repository ID.
	 * @param path                 The folder path.
	 * @param serviceContextFolder The service context for the case of creating.
	 * 
	 * @return The folder.
	 */
	public static Folder getOrCreateFolder(long repositoryId, String path, ServiceContext serviceContextFolder)
			throws PortalException {
		// Split the path by separator.
		if (path.startsWith(File.pathSeparator)) {
			path = path.substring(1);
		}
		if (path.endsWith(File.pathSeparator)) {
			path = path.substring(0, path.length() - 1);
		}
		String[] subPaths = path.split(File.pathSeparator);

		Folder folder = null;
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

		// Create the folder (and its parents, if any).
		for (int i = 0; i < subPaths.length; i++) {
			try {
				folder = DLAppLocalServiceUtil.getFolder(repositoryId, parentFolderId, subPaths[i]);
			} catch (PortalException e) {
				folder = DLAppLocalServiceUtil.addFolder(serviceContextFolder.getUserId(), repositoryId, parentFolderId,
						subPaths[i], StringPool.BLANK, serviceContextFolder);
			}
			parentFolderId = folder.getFolderId();
		}

		return folder;
	}

	/**
	 * Get the folder by its path.
	 * 
	 * @param repositoryId The repository ID of folder.
	 * @param path         The folder path.
	 * 
	 * @return The folder object.
	 */
	public static Folder getFolder(long repositoryId, String path) {
		// Split the path by separator.
		if (path.startsWith(File.pathSeparator)) {
			path = path.substring(1);
		}
		if (path.endsWith(File.pathSeparator)) {
			path = path.substring(0, path.length() - 1);
		}
		String[] subPaths = path.split(File.pathSeparator);

		Folder folder = null;
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

		// Create the folder (and its parents, if any).
		for (int i = 0; i < subPaths.length; i++) {
			try {
				folder = DLAppLocalServiceUtil.getFolder(repositoryId, parentFolderId, subPaths[i]);
			} catch (PortalException e) {
				return null;
			}
			parentFolderId = folder.getFolderId();
		}

		return folder;
	}

	/**
	 * Get the ID of vocabulary icon.
	 * 
	 * @param vocabularyId The vocabulary ID.
	 * @param groupId      The group ID.
	 * 
	 * @return The ID of vocabulary icon or 0 if not found.
	 */
	public static long getVocabularyIconId(long groupId, long companyId, long vocabularyId) {
		long fileEntryId = 0;

		String folderPath = PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_PORTAL_DATA) + File.pathSeparator
				+ PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_VOCABULARY);
		Folder folder = getFolder(groupId, folderPath);
		if (folder == null) {
			return fileEntryId;
		}

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DLFileEntry.class)
				.add(PropertyFactoryUtil.forName("groupId").eq(groupId))
				.add(PropertyFactoryUtil.forName("companyId").eq(companyId))
				.add(PropertyFactoryUtil.forName("folderId").eq(folder.getFolderId()))
				.add(PropertyFactoryUtil.forName("fileName").like("%" + vocabularyId + StringPool.UNDERLINE + "%"));

		List<DLFileEntry> dlFileEntries = DLFileEntryLocalServiceUtil.dynamicQuery(dynamicQuery);

		if (dlFileEntries.size() > 0) {
			Optional<DLFileEntry> tmp = dlFileEntries.stream()
					.filter(f -> f.getStatus() == WorkflowConstants.STATUS_APPROVED).skip(dlFileEntries.size() - 1)
					.findFirst();
			if (tmp.isPresent()) {
				fileEntryId = tmp.get().getFileEntryId();
			}
		}

		return fileEntryId;
	}

	/**
	 * Get the icon file URL by its ID.
	 * 
	 * @param fileEntryId File entry ID.
	 * 
	 * @return The path to file.
	 */
	public static String getFileUrl(long fileEntryId) {
		String url = StringPool.BLANK;

		if (fileEntryId > 0) {
			try {
				DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(fileEntryId);
				url = "/documents/" + dlFileEntry.getGroupId() + StringPool.FORWARD_SLASH + dlFileEntry.getFolderId()
						+ StringPool.FORWARD_SLASH + dlFileEntry.getTitle() + StringPool.FORWARD_SLASH
						+ dlFileEntry.getUuid();
			} catch (PortalException e) {
				// This exception will be thrown if the file does not exist.
				// In the case, just do nothing.
			}
		}

		return url;
	}

	/**
	 * Get the icon file URL by category ID.
	 * 
	 * @param categoryId The category ID.
	 * 
	 * @return The path to file.
	 */
	public static String getFileUrlByCategoryId(long categoryId) {
		long iconId = GetterUtil.DEFAULT_LONG;
		String iconUrl = GetterUtil.DEFAULT_STRING;
		
		if (categoryId != GetterUtil.DEFAULT_LONG) {
			AssetCategoryProperty iconProp = null;
			try {
				iconProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(categoryId,
						CategoryConstants.CATEGORY_PROPERTY_ICON);
			} catch (PortalException e) {
				// Do nothing.
			}
			if (iconProp != null) {
				iconId = GetterUtil.getLong(iconProp.getValue());
			}
		}

		// Get icon ID from category property.
		iconUrl = getFileUrl(iconId);

		return iconUrl;
	}

}
