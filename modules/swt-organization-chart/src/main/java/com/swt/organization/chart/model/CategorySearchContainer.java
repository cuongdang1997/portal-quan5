package com.swt.organization.chart.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import com.liferay.portal.kernel.dao.search.SearchContainer;

public class CategorySearchContainer extends SearchContainer<CategoryDTO> {

	static List<String> headerNames = new ArrayList<>();
	static Map<String, String> orderableHeaders = new HashMap<String, String>();

	static {
		headerNames.add("status");
		headerNames.add("alias");
	}

	public static final String EMPTY_RESULTS_MESSAGE = "there-are-no-results";

	public CategorySearchContainer(PortletRequest portletRequest, PortletURL iteratorURL) {
		
		super(portletRequest, new CategoryDisplayTerms(portletRequest), new CategoryDisplayTerms(portletRequest),
				DEFAULT_CUR_PARAM, DEFAULT_DELTA, iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);
		
		CategoryDisplayTerms displayTerms = (CategoryDisplayTerms) getDisplayTerms();
		iteratorURL.setParameter("status", String.valueOf(displayTerms.getStatus()));
		iteratorURL.setParameter("name", displayTerms.getName());
	}

}
