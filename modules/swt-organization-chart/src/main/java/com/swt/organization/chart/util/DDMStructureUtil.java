package com.swt.organization.chart.util;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMFormField;
import com.liferay.dynamic.data.mapping.kernel.DDMStructure;
import com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;

public class DDMStructureUtil {

	public static List<DDMFormField> getDDMFormFieldByStructureKey(String structureKey, ThemeDisplay themeDisplay) {
		DDMStructure structure = null;
		long groupId = themeDisplay.getScopeGroupId();
		long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
		try {
			structure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId,
					structureKey);
		} catch (PortalException e) {
		}

		// Get the DDM structure's fields.
		List<DDMFormField> fields = new ArrayList<DDMFormField>();
		if (structure != null) {
			fields = structure.getDDMFormFields(false);
		}
		return fields;
	}

	public static long getIdJournalFolderByStructureName(ThemeDisplay themeDisplay,ActionRequest request, String structureName) throws Exception {
		long idFolder =0;
		ServiceContext serviceContext = ServiceContextFactory.getInstance(JournalFolder.class.getName(),
				request);
		JournalFolder journalFolder   = null;
		try {
			journalFolder =	JournalFolderLocalServiceUtil.fetchFolder(themeDisplay.getScopeGroupId(), structureName);
			idFolder =	journalFolder.getFolderId(); 
		} catch (Exception e) {
			journalFolder = JournalFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(), GetterUtil.getLong(0), structureName, structureName, serviceContext);
			idFolder =	journalFolder.getFolderId();
		}
		
		return idFolder;
		
	}
	
	public static List<JournalFolder> getJournalFolder(long groupId,long idCompany) {

		List<JournalFolder> resultList = new ArrayList<JournalFolder>();
		JournalFolderLocalServiceUtil.getFolders(groupId);
		List<JournalFolder> fields = JournalFolderLocalServiceUtil.getFolders(groupId);
		if (fields.size() != 0) {
			for (JournalFolder object : fields) {
				if (object.getCompanyId() == idCompany) {
					resultList.add(object);
				}
			}
		}

		return resultList;
	}
	
	public static List<JournalArticle> getListJounalArticleByIdStructure(ThemeDisplay themeDisplay, String structurekey){
		List<JournalArticle> listArticle = new ArrayList<JournalArticle>();
		listArticle =JournalArticleLocalServiceUtil.getStructureArticles(themeDisplay.getScopeGroupId(),structurekey);
		
		return listArticle;

	}
	
	public static List<AssetCategory> getListCategoryByIdVocabulary(ThemeDisplay themeDisplay, String vocabularyId){
		List<AssetCategory> listCate = new ArrayList<AssetCategory>();
		DynamicQuery dynamic = DynamicQueryFactoryUtil.forClass(AssetCategory.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(themeDisplay.getCompanyId()))
				.add(PropertyFactoryUtil.forName("vocabularyId").eq(GetterUtil.getLong(vocabularyId)));
		listCate = AssetCategoryLocalServiceUtil.dynamicQuery(dynamic);
		
		return listCate;
	
	}
	
}
