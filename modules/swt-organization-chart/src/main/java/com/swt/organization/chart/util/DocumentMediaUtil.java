package com.swt.organization.chart.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.portlet.PortletRequest;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.StringPool;

public class DocumentMediaUtil {

	public static long createFolderMedia(ThemeDisplay themeDisplay, ServiceContext serviceContext, String nameFolder) {
		boolean flag = false;
		DLFolder folder = null;
		long folderId = 0;
		try {
			System.out.println("Get folder start");
			folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, nameFolder);
			folderId = folder.getFolderId();

			System.out.println("Get folder end");
		} catch (PortalException e) {
			flag = true;
		}
		if (flag) {
			try {
				System.out.println("Add folder start");
				folder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						themeDisplay.getScopeGroupId(), false, 0, nameFolder, "", false, serviceContext);

				System.out.println("Add folder end");
				ResourceAction actionObject = ResourceActionLocalServiceUtil
						.fetchResourceAction(DLFolder.class.getName(), ActionKeys.ADD_FOLDER);
				// createResourcePermission(themeDisplay, DLFolder.class.getName(), folderId,
				// String.valueOf(actionObject.getResourceActionId()));
				folderId = folder.getFolderId();
				createResourcePermission(themeDisplay, DLFolder.class.getName(), folderId,
						String.valueOf(actionObject.getResourceActionId()));

			} catch (PortalException e) {
				e.printStackTrace();
			}
		}

		return folderId;
	}

	
	public static String getFileTile(long primaryKeyId, String fileName) {
		return primaryKeyId + StringPool.UNDERLINE + fileName;
	}

	/**
	 * Get or create the given folder.
	 * 
	 * @param repositoryId         The repository ID.
	 * @param path                 The folder path.
	 * @param serviceContextFolder The service context for the case of creating.
	 * 
	 * @return The folder.
	 */
	public static Folder getOrCreateFolder(long repositoryId, String path, ServiceContext serviceContextFolder)
			throws PortalException {
		// Split the path by separator.
		if (path.startsWith(File.pathSeparator)) {
			path = path.substring(1);
		}
		if (path.endsWith(File.pathSeparator)) {
			path = path.substring(0, path.length() - 1);
		}
		String[] subPaths = path.split(File.pathSeparator);

		Folder folder = null;
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

		// Create the folder (and its parents, if any).
		for (int i = 0; i < subPaths.length; i++) {
			try {
				folder = DLAppLocalServiceUtil.getFolder(repositoryId, parentFolderId, subPaths[i]);
			} catch (PortalException e) {
				folder = DLAppLocalServiceUtil.addFolder(serviceContextFolder.getUserId(), repositoryId, parentFolderId,
						subPaths[i], StringPool.BLANK, serviceContextFolder);
			}
			parentFolderId = folder.getFolderId();
		}

		return folder;
	}

	/**
	 * Get the folder by its path.
	 * 
	 * @param repositoryId The repository ID of folder.
	 * @param path         The folder path.
	 * 
	 * @return The folder object.
	 */
	public static Folder getFolder(long repositoryId, String path) {
		// Split the path by separator.
		if (path.startsWith(File.pathSeparator)) {
			path = path.substring(1);
		}
		if (path.endsWith(File.pathSeparator)) {
			path = path.substring(0, path.length() - 1);
		}
		String[] subPaths = path.split(File.pathSeparator);

		Folder folder = null;
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

		// Create the folder (and its parents, if any).
		for (int i = 0; i < subPaths.length; i++) {
			try {
				folder = DLAppLocalServiceUtil.getFolder(repositoryId, parentFolderId, subPaths[i]);
			} catch (PortalException e) {
				return null;
			}
			parentFolderId = folder.getFolderId();
		}

		return folder;
	}
	public static FileEntry createOrUpdateFileEntry(long primaryKeyId, long repositoryId, String parentFolderPath,
			String folderName, String fileName, String fileType, InputStream inputStream, long fileSize,
			PortletRequest request) throws PortalException {
		ServiceContext folderSctx = ServiceContextFactory.getInstance(Folder.class.getName(), request);
		ServiceContext fileSctx = ServiceContextFactory.getInstance(FileEntry.class.getName(), request);

		String folderPath = parentFolderPath + File.pathSeparator + folderName;
		Folder folder = getOrCreateFolder(repositoryId, folderPath, folderSctx);
		long folderId = folder.getFolderId();
		String fileTitle = getFileTile(primaryKeyId, fileName);

		// Check if the file already exists.
		DLFileEntry existingDlFileEntry = null;
		try {
			existingDlFileEntry = DLFileEntryLocalServiceUtil.getFileEntry(repositoryId, folderId, fileTitle);
		} catch (PortalException e) {
			// This exception will be thrown if the file does not exist.
			// In the case, just do nothing.
		}

		// If file does not exist, create it; Otherwise, update it.
		FileEntry fileEntry = null;
		if (existingDlFileEntry == null) {
			fileEntry = DLAppLocalServiceUtil.addFileEntry(fileSctx.getUserId(), repositoryId, folderId, fileName,
					fileType, fileTitle, StringPool.BLANK, StringPool.BLANK, inputStream, fileSize, fileSctx);
		} else {
			fileEntry = DLAppLocalServiceUtil.updateFileEntry(fileSctx.getUserId(),
					existingDlFileEntry.getFileEntryId(), fileName, fileType, fileTitle, StringPool.BLANK,
					StringPool.BLANK, true, inputStream, fileSize, fileSctx);
		}

		return fileEntry;
	}
	public static String addFileAttachment(ThemeDisplay themeDisplay, long folderIdAttachment, String fileName,
			File file, ServiceContext serviceContext) throws PortalException {

		String contentType = MimeTypesUtil.getContentType(fileName);
		String path = "";
		DLFileEntry fileEntry = null;
		DLFolder dlFolder = null;

		try {
			
			System.out.println("Add file attachement start");
			dlFolder = DLFolderLocalServiceUtil.getDLFolder(folderIdAttachment);
			
			System.out.println("Get folder end " + dlFolder.getFolderId());
			long fileEntryTypeId = dlFolder.getDefaultFileEntryTypeId();
			//DLFileEntryLocalServiceUtil.get
			System.out.println("Get folder type " + fileEntryTypeId);
			// ServiceContext serviceContext =
			// ServiceContextFactory.getInstance(DLFileEntry.class.getName(),
			// renderRequest);
			//InputStream is = new FileInputStream(file);
			//dlFolder = DLFolderLocalServiceUtil.getDLFolder(folderIdAttachment);
			
			fileEntry = DLFileEntryLocalServiceUtil.addFileEntry(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(),
					dlFolder.getFolderId(),
					file.getName(), contentType, file.getName(), file.getName(), "" , fileEntryTypeId, null, file, null,
					file.getTotalSpace(), serviceContext);
			System.out.println("Add file attachement finish " + fileEntry.getFileEntryId());
			

			path = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/" + fileEntry.getTitle()
					+ "/" + fileEntry.getUuid();
			System.out.println("Add file attachment end");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return path;
	}

	public static ResourcePermission createResourcePermission(ThemeDisplay themeDisplay, String name,
			long resourcePrimKey, String permissions) {
		long resourcePermissionId = CounterLocalServiceUtil.increment();
		ResourcePermission object = ResourcePermissionLocalServiceUtil.createResourcePermission(resourcePermissionId);
		Role role = RoleLocalServiceUtil.fetchRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
		object.setName(name);
		object.setRoleId(role.getRoleId());
		object.setCompanyId(themeDisplay.getCompanyId());
		object.setScope(4);
		object.setPrimKey(String.valueOf(resourcePrimKey));
		object.setPrimKeyId(resourcePrimKey);
		object.setActionIds(GetterUtil.getLong(permissions));
		object.setViewActionId(true);

		ResourcePermissionLocalServiceUtil.addResourcePermission(object);
		return object;
	}
}
