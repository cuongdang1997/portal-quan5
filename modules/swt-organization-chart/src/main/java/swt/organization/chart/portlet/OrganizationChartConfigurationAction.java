package swt.organization.chart.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.swt.organization.chart.model.JournalAricleConstants;


@Component(
	configurationPid = "swt.organization.chart.portlet.OrganizationChartConfiguration",
	configurationPolicy = ConfigurationPolicy.OPTIONAL,
	immediate = true,
	property = {
		"javax.portlet.name=" + JournalAricleConstants.PORTLET_NAME
	},
	service = ConfigurationAction.class
)
public class OrganizationChartConfigurationAction extends DefaultConfigurationAction {
	
	@Reference
	private LogService _log;

	private volatile OrganizationChartConfiguration _config;

	
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		_log.log(LogService.LOG_INFO, JournalAricleConstants.PORTLET_NAME + " configuration action.");
		
		String structureKey = ParamUtil.getString(actionRequest, "structureKey");
		setPreference(actionRequest, "structureKey", String.valueOf(structureKey));
		
		String structureParentKey = ParamUtil.getString(actionRequest, "structureParentKey");
		setPreference(actionRequest, "structureParentKey", String.valueOf(structureParentKey));
		
		long displayStyle = ParamUtil.getLong(actionRequest, "displayStyle");
		setPreference(actionRequest, "displayStyle", String.valueOf(displayStyle));
		
		boolean searchByKeywords = ParamUtil.getBoolean(actionRequest, "searchByKeywords");
		setPreference(actionRequest, "searchByKeywords", String.valueOf(searchByKeywords));
		
		boolean showOrderInResult = ParamUtil.getBoolean(actionRequest, "showOrderInResult");
		setPreference(actionRequest, "showOrderInResult", String.valueOf(showOrderInResult));
		
		boolean showTitleInResult = ParamUtil.getBoolean(actionRequest, "showTitleInResult");
		setPreference(actionRequest, "showTitleInResult", String.valueOf(showTitleInResult));
		
		boolean showSummaryInResult = ParamUtil.getBoolean(actionRequest, "showSummaryInResult");
		setPreference(actionRequest, "showSummaryInResult", String.valueOf(showSummaryInResult));
		
		boolean showDownloadInResult = ParamUtil.getBoolean(actionRequest, "showDownloadInResult");
		setPreference(actionRequest, "showDownloadInResult", String.valueOf(showDownloadInResult));
		
		String displayDetailPage = ParamUtil.getString(actionRequest, "displayDetailPage");
		setPreference(actionRequest, "displayDetailPage", String.valueOf(displayDetailPage));
		
		String vocabulary =  ParamUtil.getString(actionRequest, "vocabulary");
		setPreference(actionRequest, "vocabulary", String.valueOf(vocabulary));
		
		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		_log.log(LogService.LOG_INFO, JournalAricleConstants.PORTLET_NAME + " configuration include.");
		
		httpServletRequest.setAttribute(OrganizationChartConfiguration.class.getName(), _config);
		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}
	 @Activate
	    @Modified
	    protected void activate(Map<Object, Object> properties) {
		  _config = ConfigurableUtil.createConfigurable(
				  OrganizationChartConfiguration.class, properties);
	    }


}
