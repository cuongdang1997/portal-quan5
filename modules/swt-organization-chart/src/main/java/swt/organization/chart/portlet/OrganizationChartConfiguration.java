package swt.organization.chart.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.liferay.portal.kernel.util.StringPool;


import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(
	factory = true,
	id = "swt.organization.chart.portlet.OrganizationChartConfiguration",
	localization = "content/Language"
)
public interface OrganizationChartConfiguration {

	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String structureKey();
	
	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String structureParentKey();
	
	@Meta.AD(deflt ="1", required = false)
	public long displayStyle();

	@Meta.AD(deflt = "1", required = false)
	public boolean searchByKeywords();
	
	@Meta.AD(deflt = "1", required = false)
	public boolean showOrderInResult();
	
	@Meta.AD(deflt = "1", required = false)
	public boolean showTitleInResult();
	
	@Meta.AD(deflt = "1", required = false)
	public boolean showSummaryInResult();
	
	@Meta.AD(deflt = "1", required = false)
	public boolean showDownloadInResult();
	
	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String vocabulary();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean showIcon();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean showDescription();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean showSequence();
	
	@Meta.AD(deflt = "false", required = false)
	public boolean isHierarchical();

}

