package swt.organization.chart.portlet;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMFormField;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMStructure;
import com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.swt.organization.chart.model.JournalAricleConstants;
import com.swt.organization.chart.util.DDMStructureUtil;
import com.swt.organization.chart.util.OrganizationChartUtil;
import com.swt.organization.chart.util.DocumentMediaUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

/**
 * @author dungnt
 */
@Component(configurationPid = "swt.organization.chart.portlet.OrganizationChartConfiguration", immediate = true, property = {
		"com.liferay.portlet.display-category=category.portal", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=" + JournalAricleConstants.PORTLET_DISPLAY_NAME,
		"javax.portlet.init-param.config-template=/configuration.jsp", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + JournalAricleConstants.PORTLET_NAME,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class SwtOrganizationChartPortlet extends MVCPortlet {

	@Reference
	private LogService _log;
	private JournalFolderLocalService _journalFolderService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet#doView(javax.portlet
	 * .RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.log(LogService.LOG_INFO,
				JournalAricleConstants.PORTLET_NAME + " " + JournalAricleConstants.ACTION_DO_VIEW);

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
		javax.portlet.PortletPreferences portletPreferences = portletDisplay.getPortletSetup();
		OrganizationChartConfiguration configuration = null;
		try {
			configuration = portletDisplay.getPortletInstanceConfiguration(OrganizationChartConfiguration.class);
		} catch (ConfigurationException e1) {
			e1.printStackTrace();
		}
				
		//Get displaystyle
		long displayStyle = GetterUtil.getLong(
				portletPreferences.getValue("displayStyle", String.valueOf(configuration.displayStyle())));

		try {
			renderRequest.setAttribute(OrganizationChartConfiguration.class.getName(),
					portletDisplay.getPortletInstanceConfiguration(OrganizationChartConfiguration.class));
		} catch (ConfigurationException e) {
			_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
		}

		String path = ParamUtil.getString(renderRequest, "path");
	
		if (displayStyle == JournalAricleConstants.CHART_STYLE_1 ) {
			include("/viewOrganization.jsp", renderRequest, renderResponse);
		}else if (displayStyle == JournalAricleConstants.CHART_STYLE_2 ) {
			include("/viewHumanResource.jsp", renderRequest, renderResponse);
			
		}else {
			if (path.equalsIgnoreCase(JournalAricleConstants.ACTION_ADD_ORGANIZATION)) {
				include("/addOrganization.jsp", renderRequest, renderResponse);
			} else if (path.equalsIgnoreCase(JournalAricleConstants.ACTION_UPDATE_ORGANIZATION)) {
				include("/updateOrganization.jsp", renderRequest, renderResponse);
		
			} else {
				super.doView(renderRequest, renderResponse);
			}
		}
		
	}

	@ProcessAction(name = JournalAricleConstants.ACTION_ADD_ORGANIZATION)
	public void addOrganization(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String structureKey = uploadPortletRequest.getParameter("structureKey");
		String structureId = uploadPortletRequest.getParameter("structureId");
		String template = uploadPortletRequest.getParameter("template");
		String title = uploadPortletRequest.getParameter("name");
		String urlCurrent = uploadPortletRequest.getParameter("urlCurrent");
		String articleId = uploadPortletRequest.getParameter("");
		String categoryId  = uploadPortletRequest.getParameter("categoryId");
		long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
		long folderId = -1;
		long parentFolderId = JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		long groupId = themeDisplay.getScopeGroupId();
		String languageId = themeDisplay.getLanguageId();
		Locale locale = LocaleUtil.fromLanguageId(languageId);
		
		DDMStructure selectedStructure = null;
		ServiceContext serviceContext = ServiceContextFactory.getInstance(JournalFolder.class.getName(), actionRequest);
		if(!themeDisplay.isSignedIn()) {
			actionResponse.sendRedirect(themeDisplay.getURLSignIn());
		}else {
			try {
				 selectedStructure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId,
						structureKey);
				

				// Get folder to store article
				JournalFolder journalFolder = JournalFolderLocalServiceUtil.fetchFolder(groupId, parentFolderId,
						selectedStructure.getName(locale));
				
				// Create new one incase it is not existed
				if (journalFolder == null) {
					
					journalFolder = JournalFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), groupId, parentFolderId,
							selectedStructure.getName(locale), selectedStructure.getName(locale), 
							serviceContext);
				}
				folderId = journalFolder.getFolderId();

				if (folderId < 0) {
					folderId = JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			Map<Locale, String> titleMap = new HashMap<Locale, String>();
			titleMap.put(locale, title);

			Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
			descriptionMap.put(locale, title);

			List<DDMFormField> listFormFile = DDMStructureUtil.getDDMFormFieldByStructureKey(structureKey, themeDisplay);
			// key=nameField , value =parameter
			Map<String, String> listParameter = new HashMap<String, String>();
			
			
			for (DDMFormField object : listFormFile) {
				try {
					
					if (!object.getType().equals("ddm-image")) {
						listParameter.put(object.getName(), 
								HtmlUtil.escape(uploadPortletRequest.getParameter(object.getName()).trim()));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			// convert content to xml
			try {
				String contentArticle = OrganizationChartUtil.setContent(listFormFile, listParameter);

				JournalArticle jounalArticle = OrganizationChartUtil.createJournalArticle(themeDisplay, actionRequest,
						structureKey, title, contentArticle,-1, GetterUtil.getLong(categoryId));
				
				//contentArticle.length()
				for (DDMFormField object : listFormFile) {
					try {
						
						if (object.getType().equals("ddm-image")) {
							String fileName = uploadPortletRequest.getFileName(object.getName().trim());
							
							//iconFileEntry = CategoryFileUtils.createOrUpdateFileEntry(categoryId, groupId,
							//		PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_PORTAL_DATA),
							//		PrefsPropsUtil.getString(CategoryConstants.PREFKEY_FOLDER_CATEGORY), iconName, iconMimeType,
							//		new FileInputStream(iconFile), iconSize, actionRequest);
							File file = uploadPortletRequest.getFile(object.getName().trim());
							String fileMimeType = uploadPortletRequest.getContentType(object.getName().trim());
							String nameFolder = selectedStructure.getName(locale);
							long fileSize = uploadPortletRequest.getSize(object.getName().trim());
							
							if (Validator.isNotNull(file)) {
								
								FileEntry fileEntry = DocumentMediaUtil.createOrUpdateFileEntry(jounalArticle.getId(), themeDisplay.getScopeGroupId(), 
										nameFolder, title, fileName, fileMimeType, 
										new FileInputStream(file), fileSize, actionRequest);
								String path = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/" + fileEntry.getTitle()
								+ "/" + fileEntry.getUuid();
								
								jounalArticle.setSmallImageURL(path);
								jounalArticle.setSmallImageId(fileEntry.getFileEntryId());
								JournalArticleLocalServiceUtil.updateJournalArticle(jounalArticle);
								
							}else {
								System.out.println("File is not uploaded sucessfully");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				
			} catch (Exception ex) {
				ex.printStackTrace();
				
			}
			
			actionResponse.sendRedirect(urlCurrent);
			
		}
		
	}

	/*
	 * Delete article using article ID
	 * */
	public void updateOrganization(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String structureKey = uploadPortletRequest.getParameter("structureKey");
		String structureId = uploadPortletRequest.getParameter("structureId");
		String template = uploadPortletRequest.getParameter("template");
		String title = HtmlUtil.escape(uploadPortletRequest.getParameter("name"));
		String urlCurrent = uploadPortletRequest.getParameter("urlCurrent");
		String articleId = uploadPortletRequest.getParameter("articleId");
		String categoryId  = uploadPortletRequest.getParameter("categoryId");
		
		long folderId = -1;
		long parentFolderId = JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		long groupId = themeDisplay.getScopeGroupId();
		long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
		String languageId = "vi_VN";
		Locale locale = LocaleUtil.fromLanguageId(languageId);
		
		if(!themeDisplay.isSignedIn()) {
			actionResponse.sendRedirect(themeDisplay.getURLSignIn());
		}else {

			try {
				DDMStructure selectedStructure = DDMStructureManagerUtil.getStructure(groupId,journalArticleClassNameId,structureKey);
				ServiceContext serviceContext = ServiceContextFactory.getInstance(JournalArticle.class.getName(),
						actionRequest);

				// Get folder to store article
				JournalFolder journalFolder = JournalFolderLocalServiceUtil.fetchFolder(groupId, parentFolderId,
						selectedStructure.getName(locale));
				
				// Create new one incase it is not existed
				if (journalFolder == null) {
					folderId = CounterLocalServiceUtil.getService().increment();
					journalFolder = JournalFolderLocalServiceUtil.updateFolder(themeDisplay.getUserId(), groupId, folderId,
							parentFolderId, selectedStructure.getName(), selectedStructure.getName(locale), false, serviceContext);
				}
				folderId = journalFolder.getFolderId();
				
				List<DDMFormField> listFormFile = DDMStructureUtil.getDDMFormFieldByStructureKey(structureKey, themeDisplay);
				// key=nameField , value =parameter
				Map<String, String> listParameter = new HashMap<String, String>();
				for (DDMFormField object : listFormFile) {
					try {
						
						listParameter.put(object.getName(), 
								HtmlUtil.escape(uploadPortletRequest.getParameter(object.getName()).trim()));
					} catch (Exception e) {
					}

				}
				// convert content to xml
				String contentArticle = OrganizationChartUtil.setContent(listFormFile, listParameter);
				try {
					JournalArticle journalArticle = OrganizationChartUtil.createJournalArticle(themeDisplay, actionRequest, structureKey, title, 
							contentArticle, GetterUtil.getLong(articleId,0), GetterUtil.getLong(categoryId));
					
					for (DDMFormField object : listFormFile) {
						try {
							
							if (object.getType().equals("ddm-image")) {
								String fileName = uploadPortletRequest.getFileName(object.getName().trim());
								File file = uploadPortletRequest.getFile(object.getName().trim());
								String fileMimeType = uploadPortletRequest.getContentType(object.getName().trim());
								String nameFolder = selectedStructure.getName(locale);
								long fileSize = uploadPortletRequest.getSize(object.getName().trim());
								
								if (Validator.isNotNull(file)) {
									
									FileEntry fileEntry = DocumentMediaUtil.createOrUpdateFileEntry(journalArticle.getId(), themeDisplay.getScopeGroupId(), 
											nameFolder, title, fileName, fileMimeType, 
											new FileInputStream(file), fileSize, actionRequest);
									String path = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/" + fileEntry.getTitle()
									+ "/" + fileEntry.getUuid();
									
									journalArticle.setSmallImageURL(path);
									journalArticle.setSmallImageId(fileEntry.getFileEntryId());
									JournalArticleLocalServiceUtil.updateJournalArticle(journalArticle);
								}else {
									System.out.println("File is not uploaded sucessfully or no file is uploaded");
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			
			actionResponse.sendRedirect(urlCurrent);
		}
		
	}

	@ProcessAction(name = JournalAricleConstants.ACTION_REMOVE_ORGANIZATION)
	public void deleteOrganization(ActionRequest request, ActionResponse response) throws PortalException, Exception {
		request.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		uploadPortletRequest.getParameter("structureKey");
		uploadPortletRequest.getParameter("structureId");
		uploadPortletRequest.getParameter("template");
		uploadPortletRequest.getParameter("name");
		String urlCurrent = uploadPortletRequest.getParameter("urlCurrent");
		String articleId = uploadPortletRequest.getParameter("articleId");
		try {
			JournalArticleLocalServiceUtil.deleteJournalArticle(GetterUtil.getLong(articleId));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		response.sendRedirect(urlCurrent);
	}
}
