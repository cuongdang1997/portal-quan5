<%@page import="com.liferay.portal.kernel.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.Layout"%>
<%@page import="com.liferay.journal.model.JournalFolder"%>
<%@page
	import="com.liferay.journal.service.JournalFolderLocalServiceUtil"%>
<%@page import="com.liferay.journal.service.JournalFolderLocalService"%>
<%@page import="com.swt.organization.chart.model.JournalAricleConstants"%>
<%@page
	import="swt.organization.chart.portlet.OrganizationChartConfiguration"%>

<%@page
	import="com.swt.organization.chart.model.JournalAricleConstants"%>
	
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page
	import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMFormField"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMStructure"%>
<%@page
	import="com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil"%>
<%@page import="com.liferay.journal.model.JournalArticle"%>
<%@page
	import="com.liferay.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.journal.model.JournalFolderConstants"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.search.BaseModelSearchResult"%>
<%@page	import="com.liferay.portal.kernel.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.swt.organization.chart.util.DDMStructureUtil"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletPreferences"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<link rel="stylesheet"	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	OrganizationChartConfiguration configuration = (OrganizationChartConfiguration) GetterUtil
	.getObject(renderRequest.getAttribute(OrganizationChartConfiguration.class.getName()));
	
	String vocabulary = StringPool.BLANK;

	boolean showIcon = false, showDescription = false, showSequence = false, isHierarchical = false;

	long companyId = themeDisplay.getCompanyId();
	long groupId = themeDisplay.getScopeGroupId();
	Object dummyObj = new Object();

	String structureKey = StringPool.BLANK;
	String structureKeyParrent = "0";
	String structureKeyParrentParam = "0";
	String displayDetailPage =StringPool.BLANK;

	long displayStyle = 1;
	long parentLevelId = 0;
	long parentFolderId = JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID;
	long folderId = -1;
	String uuidLayout ="";
	boolean privateLayout = true;
	boolean searchByKeywords = true, showOrderInResult = true, showTitleInResult = true,
	showSummaryInResult = true, showDownloadInResult = true;
	Layout layoutPortlet =null;

	if (Validator.isNotNull(configuration)) {
		vocabulary = portletPreferences.getValue("vocabulary", configuration.vocabulary());

		showDescription = GetterUtil.get(
				portletPreferences.getValue("showDescription", String.valueOf(configuration.showDescription())),
				false);
		showSequence = GetterUtil.get(
				portletPreferences.getValue("showSequence", String.valueOf(configuration.showSequence())),
				false);
		isHierarchical = GetterUtil.get(
				portletPreferences.getValue("isHierarchical", String.valueOf(configuration.isHierarchical())),
				false);
		structureKey = GetterUtil
				.getString(portletPreferences.getValue("structureKey", configuration.structureKey()));

		structureKeyParrent = GetterUtil.getString(
				portletPreferences.getValue("structureParentKey", configuration.structureParentKey()));

		displayStyle = GetterUtil.getLong(
				portletPreferences.getValue("displayStyle", String.valueOf(configuration.displayStyle())));
		
		searchByKeywords = GetterUtil.getBoolean(portletPreferences.getValue("searchByKeywords",
				String.valueOf(configuration.searchByKeywords())));
		showOrderInResult = GetterUtil.getBoolean(portletPreferences.getValue("showOrderInResult",
				String.valueOf(configuration.showOrderInResult())));
		showTitleInResult = GetterUtil.getBoolean(portletPreferences.getValue("showTitleInResult",
				String.valueOf(configuration.showTitleInResult())));
		showSummaryInResult = GetterUtil.getBoolean(portletPreferences.getValue("showSummaryInResult",
				String.valueOf(configuration.showSummaryInResult())));
		showDownloadInResult = GetterUtil.getBoolean(portletPreferences.getValue("showDownloadInResult",
				String.valueOf(configuration.showDownloadInResult())));
		
		uuidLayout = GetterUtil.getString(portletPreferences.getValue("portletSetupLinkToLayoutUuid", ""));
		
		layoutPortlet = LayoutLocalServiceUtil.fetchLayoutByUuidAndGroupId(uuidLayout, groupId, themeDisplay.getLayout().isPrivateLayout());
		
		displayDetailPage = portletPreferences.getValue("displayDetailPage", "");
	}
	
	// The list of structure fields.
	List<DDMFormField> fields = new ArrayList<>();
	long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);

	//List ID category for structure 
	List<AssetCategory> listCategory = null; 
	//System.out.println("vocabulary : " + vocabulary);
	
	AssetVocabulary assetVocabulary = null;
	
	if(!vocabulary.equalsIgnoreCase(StringPool.BLANK)){
		listCategory = DDMStructureUtil.getListCategoryByIdVocabulary(themeDisplay, vocabulary);
		try {
			assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(GetterUtil.getLong(vocabulary));
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	String vocabularyPrefix = StringPool.BLANK;

	long vocabularyIconId = 0;
	String vocabularyIconUrl = StringPool.BLANK;
	
	if (structureKey != null && structureKey.length() != 0) {
		// Get the selected structure by its ID.
		DDMStructure selectedStructure = null;
		try {
			selectedStructure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId,
					structureKey);
			
			//System.out.println("init.selectedStructure: " + selectedStructure.getStructureId());
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if (selectedStructure != null) {
			// Get the fields from selected structure.
			fields = selectedStructure.getDDMFormFields(false);
		}

	}
%>
