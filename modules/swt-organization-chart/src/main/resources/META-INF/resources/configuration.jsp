<%@ include file="init.jsp"%>

<style scoped>
.fieldset {
	margin-top: 20px;
	padding: 20px;
}

.control-group {
	margin-bottom: 0 ! important;
}

legend {
	text-transform: uppercase;
	font-weight: bold;
}

.btn-bottom-unset {
	bottom: unset !important;
}
</style>

<%
	//System.out.println("configuration.fields: " + fields.size());
	// Get the list of Web Content structures.
	List<DDMStructure> structures = DDMStructureManagerUtil.getStructures(new long[] { groupId },
			journalArticleClassNameId);
	//System.out.println("configuration.displaystyle: " + displayStyle);
%>
<liferay-portlet:actionURL portletConfiguration="<%=true%>"
	var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%=true%>"
	var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL.toString()%>" method="post"
	name="<portlet:namespace />fm">
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden"
			value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden"
			value="<%=configurationRenderURL%>" />

		<aui:fieldset-group>
			<!-- Basic settings. -->
			<aui:fieldset cssClass="panel panel-default">
				<legend><%=LanguageUtil.format(request, "swtorganizationchart.basic-settings", dummyObj)%></legend>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input name="vocabulary" label="Vocabulary" value="<%=vocabulary%>"></aui:input>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="col-md-3">
						<aui:select name="structureKey"
							label='<%=LanguageUtil.format(request, "structure", dummyObj)%>'
							value="<%=structureKey%>">

							<%
								for (DDMStructure i : structures) {
							%>
							<aui:option value="<%=String.valueOf(i.getStructureKey())%>">
								<%=i.getName(locale)%>
							</aui:option>
							<%
								}
							%>
						</aui:select>
					</div>
					<div class="col-md-3">
						<aui:select name="structureParentKey"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.structure-parrent", dummyObj)%>'
							value="<%=structureKeyParrent%>">
							<aui:option value="0">
									vui long chon
							</aui:option>
							<%
								for (DDMStructure i : structures) {
							%>
							<aui:option value="<%=String.valueOf(i.getStructureKey())%>">
								<%=i.getName(locale)%>
							</aui:option>
							<%
								}
							%>
						</aui:select>
					</div>
					<div class="col-md-3">
						<aui:select name="displayStyle"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.display-style", dummyObj)%>'>
							<option label="Table style 1"
								value="<%=JournalAricleConstants.TABLE_STYLE_1%>"
									<%if (displayStyle == JournalAricleConstants.TABLE_STYLE_1) {%>">
										selected
									<%
								}
							%>
							</option>
							<option label="OrganizationChart"
								value="<%=JournalAricleConstants.CHART_STYLE_1%>"
									<%if (displayStyle == JournalAricleConstants.CHART_STYLE_1) {%>">
										selected
									<%
								}
							%>
							</option>
							<option label="Table style - child items"
								value="<%=JournalAricleConstants.TABLE_STYLE_2_ONLYCHILD%>" 
								<%if (displayStyle == JournalAricleConstants.TABLE_STYLE_2_ONLYCHILD) {%>">
									selected
								<%
								}
							%>
							</option>
							<option label="Organization Chart - View all items"
								value="<%=JournalAricleConstants.CHART_STYLE_2%>"
								<%if (displayStyle == JournalAricleConstants.CHART_STYLE_2) {%>">
									selected
								<%
								}
							%>
							</option>
						</aui:select>

					</div>
					<div class="col-md-3">
						<aui:input name="displayDetailPage"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.display-detail-page", dummyObj)%>'
							value='<%=displayDetailPage%>'>
						</aui:input>

					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="showOrderInResult"
							checked="<%=showOrderInResult%>"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.show-order-number-in-result",
								dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="showTitleInResult"
							checked="<%=showTitleInResult%>"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.show-title-in-result", dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="showSummaryInResult"
							checked="<%=showSummaryInResult%>"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.show-summary-in-result", dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="showDownloadInResult"
							checked="<%=showDownloadInResult%>"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.show-download-in-result", dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="searchByKeywords"
							checked="<%=searchByKeywords%>"
							label='<%=LanguageUtil.format(request, "swtorganizationchart.search-by-keywords", dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:select name="showSequence" label="Show sequence"
							value="<%=showSequence%>">
							<aui:option value="true">True</aui:option>
							<aui:option value="false">False</aui:option>
						</aui:select>
					</div>
				</div>
			</aui:fieldset>

			<!-- Advanced search. -->
			<aui:fieldset cssClass="panel panel-default">
				<legend><%=LanguageUtil.format(request, "swtorganizationchart.advanced-search", dummyObj)%></legend>

				<!-- Search by structure fields (advanced search). -->
				<%
					if (fields != null && fields.size() > 0) {
				%>
				<p>
					<strong><%=LanguageUtil.format(request, "swtorganizationchart.search-by-structure-fields",
									dummyObj)%></strong>
				</p>
				<table class="table table-bordered table-striped">
					<thead class="alert alert-info">
						<tr>
							<th><strong><%=LanguageUtil.format(request, "field", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "swtorganizationchart.search-type", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "swtorganizationchart.show-in-search-box", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "swtorganizationchart.show-in-result", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "swtorganizationchart.display-sequence", dummyObj)%></strong></th>
						</tr>
					</thead>
					<tbody>
						<!-- Display search options basing on DDM field type. -->
						<%
							for (DDMFormField field : fields) {
						%>
						<%
							String ddmType = field.getType();

												String prefix = "structure" + structureKey + field.getName();

												boolean searchable = false;
												boolean selected = GetterUtil
														.getBoolean(portletPreferences.getValue(prefix + "selected", "false"), false);
												int searchType = GetterUtil
														.getInteger(portletPreferences.getValue(prefix + "type", "1"), 1);
												int sequence = GetterUtil
														.getInteger(portletPreferences.getValue(prefix + "sequence", "0"), 0);
												boolean showInResult = GetterUtil.getBoolean(
														portletPreferences.getValue(prefix + "showInResult", "false"), false);
						%>
						<tr>
							<td><%=field.getLabel().getString(locale)%></td>
							<td>
								<%
									if (ddmType.equals("text") || ddmType.equals("textarea")
																|| ddmType.equals("ddm-text-html")) {
								%>
								<%
									searchable = true;
								%> <%=LanguageUtil.format(request,
											"swtorganizationchart.the-search-type-depends-on-indexing-type", dummyObj)%>
								<%
									} else if (ddmType.equals("ddm-date")) {
																%> <%
								 	searchable = true;
								 %>
								<aui:select name='<%="preferences--" + prefix + "type--"%>'
									label="<%=StringPool.BLANK%>">
									<aui:option
										value='<%=JournalAricleConstants.SEARCH_TYPE_EQUAL%>'
										label='<%=LanguageUtil.format(request, "swtorganizationchart.search-equal",
													dummyObj)%>'
										selected='<%=searchType == JournalAricleConstants.SEARCH_TYPE_EQUAL%>'>
									</aui:option>
									<aui:option
										value='<%=JournalAricleConstants.SEARCH_TYPE_RANGE%>'
										label='<%=LanguageUtil.format(request, "swtorganizationchart.search-range",
													dummyObj)%>'
										selected='<%=searchType == JournalAricleConstants.SEARCH_TYPE_RANGE%>'>
									</aui:option>
								</aui:select> <%
 	} else {
 %> <%=LanguageUtil.format(request,
											"swtorganizationchart.this-ddm-type-is-unsupported", dummyObj)%>
								<%
									}
								%>
							</td>
							<td valign="middle">
								<%
									if (searchable) {
								%> <aui:input type="checkbox"
									label="<%=StringPool.BLANK%>" checked="<%=selected%>"
									name='<%="preferences--" + prefix + "selected--"%>' /> <%
 	}
 %>
							</td>
							<td valign="middle">
								<%
									if (searchable) {
								%> <aui:input type="checkbox"
									label="<%=StringPool.BLANK%>" checked="<%=showInResult%>"
									name='<%="preferences--" + prefix + "showInResult--"%>' /> <%
 	}
 %>
							</td>
							<td valign="middle">
								<%
									if (searchable) {
								%> <aui:input type="number"
									label="<%=StringPool.BLANK%>"
									value="<%=sequence > 0 ? String.valueOf(sequence) : StringPool.BLANK%>"
									name='<%="preferences--" + prefix + "sequence--"%>' /> <%
 	}
 %>
							</td>
						</tr>
						<%
							}
						%>
					</tbody>
				</table>
				<%
					}
				%>


			</aui:fieldset>


		</aui:fieldset-group>

		<!-- Form actions -->
		<aui:button-row cssClass="btn-bottom-unset">
			<aui:button type="submit"
				cssClass="btn btn-lg btn-primary btn-default " />
		</aui:button-row>
	</div>
</aui:form>