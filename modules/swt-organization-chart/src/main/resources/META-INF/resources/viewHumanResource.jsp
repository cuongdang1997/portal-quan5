<%@page import="com.liferay.taglib.portlet.RenderURLParamsTag"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="com.swt.organization.chart.util.DDMStructureUtil"%>
<%@page import="com.swt.organization.chart.model.JournalArticleUtilImpl"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleDisplayTerms"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleContainer"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleColumnDTO"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleCriteriaDTO"%>
<%@page	import="com.swt.organization.chart.util.OrganizationChartUtil"%>
<%@page	import="com.swt.organization.chart.model.CategoryConstants"%>
<%@page	import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.asset.kernel.model.AssetRenderer"%>
<%@page	import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page	import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page	import="com.liferay.asset.kernel.model.AssetCategoryProperty"%>
<%@page	import="com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil"%>

<%@page	import="com.liferay.journal.web.asset.JournalArticleAssetRenderer"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>

<%@page import="java.io.StringReader"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>

<%@include file="init.jsp"%>
<style scoped>

.map-politics {
    padding-bottom: 8px;
    text-align: left;
    font-family: arial;
    font-size: 12px;
}

.title-pl {

    padding: 0px 0px 8px 25px;
    font-weight: bold;
    color: #d70000;
    text-transform: uppercase;
    }
    
.content-pl {
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/l-item-space.gif) repeat-x left top;
    padding-bottom: 8px;
    padding-top: 8px;
    margin-left: 5px;
    margin-right: 5px;
}

.firstHR {
    text-align: center;
}

.firstThumbnailHR img {
    cursor: pointer;
}


.firstcntHR {
    padding: 5px;
}

.groupHR {
    padding-top: 10px;
    overflow: hidden;
    clear: both;
    padding-left: 60px;
}

.elementHR {
    width: 170px;
    text-align: center;
    padding: 10px 0px;
    float: left;
}

.siblingname {
    color: #0b5090;
    font-weight: bold;
}
.HrNameHR {
    font-family: arial;
    font-size: 12px;
    font-weight: bold;
}

.ThumbnailHR img {
    cursor: pointer;
}

.m2-content {
    padding: 10px 12px;
}
 .m2-item {
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/l-item-space.gif) repeat-x left bottom;
    padding: 7px 0px;
    overflow: hidden;
    clear: both;
}

.m2-img {
    float: left;
    width: 80px;
}
.m2-title {
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/r-ms-thanh-uy1.jpg) no-repeat left top;
    color: #fff;
    font-family: arial;
    font-size: 12px;
    font-weight: bold!important;
    text-transform: uppercase;
    padding: 12px 8px 6px 40px;
}   

div.m2-nickname {
    float: left;
    padding-right: 2px;
    font-size: 11px;
}
div.m2-hrname {
    font-size: 11px;
    font-weight: bold;
    color: #555;
    cursor: pointer;
}

.m2-thumbnail {
    width: 72px;
    height: 90px;
}

.ThumbnailHR {
    width: 125px;
    height: 125px;
    margin: 0 auto;
    position: relative;
    overflow: hidden;
}
.ThumbnailHR img {
    position: absolute;
    width: max-content;
    height: max-content;
    left: -4px;
    margin: 0 auto;
    right: 0;
    top: 0;
    bottom: 0;
    max-width: 115%;
    max-height: 120%;
}


</style>
<script type="text/javascript">

function printPost() {
	 window.print();
}

var valueWidth = 0;

$(".organization-child-of-child-swt").each(function(){
	valueWidth = valueWidth + $(this).width() + 75;
});

$(".organization-father").hover(function(){
	$(this).find(".organization-describle-one").slideDown();
},function(){
	$(this).find(".organization-describle-one").css("display","none");
});

</script>

<%
	//Get all level child of parrent level
	HttpServletRequest httpReq = PortalUtil
	.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
	com.swt.organization.chart.model.JournalArticleUtil searchUtil = new JournalArticleUtilImpl();
	PortletURL portletURL = renderResponse.createRenderURL();
	
	parentLevelId = GetterUtil.getLong(ParamUtil.get(httpReq, "parentArticleId","0"));
	
	JournalArticleContainer structuredSearchContainer = new JournalArticleContainer(renderRequest, portletURL);
	
	List<JournalArticle> results = null;
	
	results = searchUtil.search(structuredSearchContainer, themeDisplay, structureKey,
	parentLevelId, renderRequest);
	
	 String viewURL = "";
	 if(themeDisplay.getURLCurrent().indexOf("?")>=0){
		 viewURL = themeDisplay.getURLCurrent() + "&parentArticleId=";
	 }else{
		 viewURL = themeDisplay.getURLCurrent() + "?parentArticleId=";
	 }
	 
	 String viewDetailUrl = "";
	 viewDetailUrl =  PortalUtil.getLayoutFriendlyURL(layoutPortlet, themeDisplay) +"?parentArticleId=";
	 
	 List<AssetCategory> categoriesGroupArticle = null;
	 List<JournalArticle> displayArticles = new ArrayList<JournalArticle>();
	 
	 if (results.size() > 0){
	 for (int i = (results.size()-1); i >= 0;i--){
		 JournalArticle tempArticle = (JournalArticle) results.get(i);
		 displayArticles.add(tempArticle);
	 }
	 
	//Group all article follow category
	 for(JournalArticle tempArticle: displayArticles){
	 	AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), tempArticle.getResourcePrimKey());
		List<AssetCategory> assetCategories =  entry.getCategories();
		
		System.out.println("assetCategories human" + assetCategories.size());
		//first get all category group from article
		if (categoriesGroupArticle == null){
			categoriesGroupArticle = assetCategories;
		}else{
			for(AssetCategory assetCategory:assetCategories){
				if(!categoriesGroupArticle.contains(assetCategory)){
					categoriesGroupArticle.add(assetCategory);
				}
			}
		}
	 }

	OrganizationChartUtil.sort(categoriesGroupArticle);
		 	OrganizationChartUtil.sort(structureKey, themeDisplay, displayArticles, "sequence", OrganizationChartUtil.SortOrder.Ascending);
		 
	 //System.out.println("assetCategories" + categoriesGroupArticle.size());
%>
<%if (categoriesGroupArticle.size() >0) {%>

	<div class="map-politics model1">
	    <%-- <div class="title-pl">
	       <%=categoriesGroupArticle.get(0).getName() %>
	    </div> --%>
	    <div class="content-pl">
	    	<%for (int i = 0; i < displayArticles.size(); i++) { 
	    		
	    		 JournalArticle tempArticle = displayArticles.get(i);
				 AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), 
						 tempArticle.getResourcePrimKey());
				 List<AssetCategory> assetCategories =  entry.getCategories();
				 
				 if(assetCategories.get(0).getCategoryId() == categoriesGroupArticle.get(0).getCategoryId()){
					 
		    		String articleContent = displayArticles.get(i).getContentByLocale(themeDisplay.getLanguageId());
					Document articleDoc = null;
					Node node = null;
					//System.out.println(articleContent);
					String imagePath = displayArticles.get(i).getSmallImageURL();
					System.out.println("imagePath " + imagePath);
					try {
	
						articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
						Element root = articleDoc.getRootElement();
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='name']/dynamic-content");
						String name =  node.getStringValue();
						
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='birthDate']/dynamic-content");
						String birthDate =  node.getStringValue();
						
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='diploma']/dynamic-content");
						String diploma =  node.getStringValue();
						
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='entryTime']/dynamic-content");
						String entryTime =  node.getStringValue();
						
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='additionalInfo']/dynamic-content");
						String additionalInfo =  node.getStringValue();
						
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='assignedPosition']/dynamic-content");
						String assignedPosition =  node.getStringValue();
						
						node = articleDoc.selectSingleNode(
								"/root/dynamic-element[@name='position']/dynamic-content");
						String position =  node.getStringValue();
						
						
		    	
		    		if(i==0){
		    	%>
			        <div class="firstHR detailHr" nickname="<%=assignedPosition %>" hrname="<%=name%>">
			            <div class="firstThumbnailHR" >
			            <div  class="ThumbnailHR">
			            <img  src="<%=imagePath %>"
			               alt="" class="imgfirstthumbnailhr">
			            </div>
			              
			            </div>
			            <div class="firstcntHR">
			                <div class="firstNickNameHR siblingname">
			                    <%=position %>
			                </div>
			                <div class="firstHrNameHR">
			                    <%=name%>
			                </div>
			            </div>
			            
			        </div>
			        <div class="groupHR">
			        <%}else{ %>
			            <div class="elementHR detailHr" nickname="<%=assignedPosition %>" hrname="<%=name%>">
			                 <div class="ThumbnailHR">
			                   <img src="<%=imagePath %>"
			                   alt="" class="imgthumbnailhr">
			                </div>
			                <div class="CntHR">
			                    <div class="NickNameHR siblingname">
			                        <%=position %>
			                    </div>
			                    <div class="HrNameHR">
			                       <%=name%>
			                    </div>
			                </div>
			            </div>     
			         <%}%>
			        
		        <%}catch(Exception ex){
		        	ex.printStackTrace();
		        }} %>
		    
	    <%}%>
	    </div>
	    </div>
	</div>   
	<%for(int j=1;j<categoriesGroupArticle.size();j++){
	AssetCategory assetCategory = categoriesGroupArticle.get(j);
	
	%>
		<div class="map-politics model2">
             <div class="m2-title">
                <%=assetCategory.getName() %>
             </div>
             
             <div class="m2-content">
             	<div id="<%=assetCategory.getCategoryId() %>">
              		<div class="group0 groupelement">
					<%for (int i = 0; i < displayArticles.size(); i++) {
						JournalArticle tempArticle = displayArticles.get(i);
						 AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), 
								 tempArticle.getResourcePrimKey());
						 List<AssetCategory> assetCategories =  entry.getCategories();
						 
						 if(assetCategories.get(0).getCategoryId() == assetCategory.getCategoryId()){
							 
				    		String articleContent = displayArticles.get(i).getContentByLocale(themeDisplay.getLanguageId());
							Document articleDoc = null;
							Node node = null;
							//System.out.println(articleContent);
							String imagePath = displayArticles.get(i).getSmallImageURL();
							System.out.println("imagePath " + imagePath);
							try {
			
								articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
								Element root = articleDoc.getRootElement();
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='name']/dynamic-content");
								String name =  node.getStringValue();
								
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='birthDate']/dynamic-content");
								String birthDate =  node.getStringValue();
								
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='diploma']/dynamic-content");
								String diploma =  node.getStringValue();
								
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='entryTime']/dynamic-content");
								String entryTime =  node.getStringValue();
								
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='additionalInfo']/dynamic-content");
								String additionalInfo =  node.getStringValue();
								
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='assignedPosition']/dynamic-content");
								String assignedPosition =  node.getStringValue();
								
								node = articleDoc.selectSingleNode(
										"/root/dynamic-element[@name='position']/dynamic-content");
								String position =  node.getStringValue();
	
					
					%>
	                 <div class="m2-item" nickname="<%=position %>" hrname="<%=name%>">
	                     <div class="m2-img ">
	                         <img class="m2-thumbnail" src="<%=imagePath %>" alt="">
	                     </div>
	                     <div class="m2-nickname">
	                        <%=position %>:
	                     </div>
	                     <div class="m2-hrname">
	                         <%=name.toUpperCase()%>
	                     </div>
	                 </div>
	                <%}catch(Exception ex){
			        	ex.printStackTrace();
			        }}}%>
	                 
	              </div>
             
                 
             </div>

         </div>

   </div>   
	
	<%} %>
      <div class="print-org" onclick="printPost()">
         Bản in
      </div>

<%}%>

<%}else{%>
 	<div class="title-pl">
        Khong co du lieu
    </div>
<%}%>