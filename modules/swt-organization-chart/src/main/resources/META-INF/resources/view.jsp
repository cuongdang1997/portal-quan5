<%@page import="com.liferay.taglib.portlet.RenderURLParamsTag"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.swt.organization.chart.util.DDMStructureUtil"%>
<%@page import="com.swt.organization.chart.model.JournalArticleUtilImpl"%>

<%@page
	import="com.swt.organization.chart.model.JournalArticleDisplayTerms"%>
<%@page
	import="com.swt.organization.chart.model.JournalArticleContainer"%>
<%@page
	import="com.swt.organization.chart.model.JournalArticleColumnDTO"%>
<%@page
	import="com.swt.organization.chart.model.JournalArticleCriteriaDTO"%>
<%@page
	import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.asset.kernel.model.AssetRenderer"%>
<%@page
	import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page
	import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page
	import="com.liferay.journal.web.asset.JournalArticleAssetRenderer"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>

<%@page import="java.io.StringReader"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>

<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>

<%@include file="init.jsp"%>
<style scoped>
.lfr-search-container th {
	font-weight: bold;
}

.alert.alert-info {
	margin-top: 20px;
}

.icon-white {
	color: white;
}

.icon-ok-sign {
	color: green;
}

.icon-ban-circle {
	color: gray;
}

div[id$="dlgCategory"] .modal-body {
	padding: 16px 24px 16px 24px !important;
}

div[id$="dlgCategory"] iframe {
	height: 100% !important;
	width: 100% !important;
}

#<portlet:namespace />searchContainer th {
	text-align: center;
	text-transform: capitalize;
}

.icon-level0 {
	background-image: url(/o/hcm-admin-theme/images/orgchart/cha.gif);
	width: 20px;
	height: 15px;
	display: inline-block;
	background-repeat: no-repeat;
}

.icon-level-child-0 {
	background-image: url(/o/hcm-admin-theme/images/orgchart/cmcon.gif);
	width: 35px;
	height: 15px;
	display: inline-block;
	background-repeat: no-repeat;
}

.icon-level-child-1 {
	background-image: url(/o/hcm-admin-theme/images/orgchart/cmcon.gif);
	width: 35px;
	height: 15px;
	display: inline-block;
	background-repeat: no-repeat;
}

.icon-level-child-2 {
	background-image: url(/o/hcm-admin-theme/images/orgchart/cmcon.gif);
	width: 35px;
	height: 15px;
	display: inline-block;
	background-repeat: no-repeat;
	margin-left: 20px
}

.icon-level-child-3 {
	background-image: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/cmcon.gif);
	width: 35px;
	height: 15px;
	display: inline-block;
	background-repeat: no-repeat;
	margin-left: 40px
}

.icon-level-child-3 {
	background-image: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/cmcon.gif);
	width: 35px;
	height: 15px;
	display: inline-block;
	background-repeat: no-repeat;
	margin-left: 60px
}
</style>
<%
	//getting portlet configuration information
	PortletURL portletURL = renderResponse.createRenderURL();

	JournalArticleContainer structuredSearchContainer = new JournalArticleContainer(renderRequest, portletURL);
	JournalArticleDisplayTerms searchTerms = (JournalArticleDisplayTerms) structuredSearchContainer
			.getSearchTerms();
	
	String portletId = GetterUtil.getString(themeDisplay.getPortletDisplay().getId());
	
	com.swt.organization.chart.model.JournalArticleUtil searchUtil = new JournalArticleUtilImpl();
	PortletPreferences portletSetup = PortletPreferencesFactoryUtil
			.getExistingPortletSetup(themeDisplay.getLayout(), portletId);
	
	String portletTitle = themeDisplay.getPortletDisplay().getTitle();
	
	portletTitle = portletSetup.getValue("portlet-setup-title-" + themeDisplay.getLanguageId(), portletTitle)
			.trim();
	HttpServletRequest httpReq = PortalUtil
			.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));

	long categoryId = 0;// GetterUtil.getLong(httpReq.getParameter("categoryId"));

	//This is parrent level to use for updating/adding new item
	parentLevelId =GetterUtil.getLong(ParamUtil.get(httpReq, "parentArticleId","0"));
	
	structureKeyParrentParam = httpReq.getParameter("structureparentkey") != null
			? httpReq.getParameter("structureparentkey")
			: "0";

	List<JournalArticleColumnDTO> displayColumns = new ArrayList<>();
	List<JournalArticleCriteriaDTO> searchCriterias = new ArrayList<>();

	boolean selected = false, showInResult = false;
	int searchType = -1, sequence = -1;

	// Iterate through structure fields to determine the list of search criterias/display columns.
	for (DDMFormField f : fields) {
		selected = GetterUtil.getBoolean(
				portletPreferences.getValue("structure" + structureKey + f.getName() + "selected", "false"));
		showInResult = GetterUtil.getBoolean(portletPreferences
				.getValue("structure" + structureKey + f.getName() + "showInResult", "false"));
		searchType = GetterUtil.getInteger(
				portletPreferences.getValue("structure" + structureKey + f.getName() + "type", "0"));
		sequence = GetterUtil.getInteger(
				portletPreferences.getValue("structure" + structureKey + f.getName() + "sequence", "0"));

		if (selected) {
			JournalArticleCriteriaDTO dto = new JournalArticleCriteriaDTO(f.getType(), searchType, f.getName(),
					f.getLabel().getString(locale), sequence);
			searchCriterias.add(dto);
		}
		if (showInResult) {
			JournalArticleColumnDTO dto = new JournalArticleColumnDTO(f.getType(), f.getName(),
					f.getLabel().getString(locale), sequence);
			displayColumns.add(dto);
		}

	}
	// Sort by sequence.
	Collections.sort(searchCriterias);
	//Collections.sort(displayColumns);

	// Retrieve the search result from request parameter (if any).
	String articleIds = StringPool.BLANK;// request.getParameter("articleIds");
	if (articleIds == null) {
		articleIds = StringPool.BLANK;
	}

	// Check if the current view is basic or advanced search.
	boolean isAdvancedSearch = GetterUtil.getBoolean(request.getParameter("isAdvancedSearch"), false);
	boolean isFirstLoad = GetterUtil.getBoolean(request.getParameter("isFirstLoad"), true);
	if (categoryId == 0) {
		categoryId = GetterUtil.getLong(request.getParameter("categoryId"));
	}
	// The date formatter in Vietnamese locale.
	SimpleDateFormat viDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	// The date format used when Liferay stores value by structure.
	SimpleDateFormat lfDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	String viewUrl = displayDetailPage + "?parentArticleId=";

	String languageId = themeDisplay.getLanguageId();
	//System.out.println("PortalUtil.getLayoutFriendlyURL(selectedLayout, themeDisplay); " + PortalUtil.getLayoutFriendlyURL(layoutPortlet, themeDisplay));

%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<portlet:param name="mvcPath" value="view.jsp" />
</liferay-portlet:renderURL>

<portlet:renderURL var="viewhighlevel">
</portlet:renderURL>

<c:choose>
	<c:when test="<%=structureKey == null || structureKey.length() == 0%>">
		<p>
			<span style="font-style: italic; color: red;">Please configure
				the structure and search settings for this porlet.</span>
		</p>
	</c:when>
	<c:otherwise>
		<%
		 	// Display table for manage
			if (displayStyle == JournalAricleConstants.TABLE_STYLE_1 || displayStyle == JournalAricleConstants.TABLE_STYLE_2_ONLYCHILD) {
		%>
		<portlet:renderURL var="addCategoryURL"
			windowState="<%=LiferayWindowState.POP_UP.toString()%>">
			<portlet:param name="path"
				value="<%=JournalAricleConstants.ACTION_ADD_ORGANIZATION%>" />
			<portlet:param name="level"
									value="<%=String.valueOf(parentLevelId)%>" />	
			<portlet:param name="parentArticleId"
									value="<%=String.valueOf(parentLevelId)%>" />
		</portlet:renderURL>

		<div class="row-fluid">
			<aui:button id="btnAddCategory" cssClass="btn btn-primary"
				icon="icon-plus"
				value='<%=LanguageUtil.format(httpReq, "swtorganizationchart.add-new", dummyObj)%>' />
		</div>

		<br>
		<div class="container-fluid padding-top-15px">
			<aui:form action="<%=portletURL.toString()%>" method="post" name="fm"
				cssClass="navbar-search search-form">
				<aui:input type="hidden" value="<%=categoryId%>" name="categoryId" />
				<!-- BASIC SEARCH: Search by keywords. -->
				<div class="panel panel-primary">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<aui:input name="name"
									label='<%=LanguageUtil.format(locale, "name", dummyObj)%>'>
								</aui:input>
							</div>
							<div class="col-md-4">
								<aui:select name="status">
									<aui:option value=""
										label='<%="--- " + LanguageUtil.format(locale, "select", dummyObj) + " ---"%>'></aui:option>
									<aui:option value=""
										label='<%=LanguageUtil.format(locale, "active", dummyObj)%>'></aui:option>

								</aui:select>
							</div>
							<div class="col-md-4 location-btn-search-and-reset">
								<aui:button type="submit"
									value='<%=LanguageUtil.format(locale, "swtorganizationchart.search", dummyObj)%>'></aui:button>
								<aui:button type="reset" value="reset"></aui:button>
							</div>
						</div>
					</div>
				</div>

				<!-- SEARCH CONTAINER -->
				<%if (displayStyle == JournalAricleConstants.TABLE_STYLE_1){ %>
				<div id="<portlet:namespace />searchContainer"
					class="padding-top-35px">
					<liferay-ui:search-container id="tblSearchResults"
						cssClass="table-search-result"
						searchContainer="<%=structuredSearchContainer%>"
						emptyResultsMessage="there-are-no-results" delta="3"
						deltaConfigurable="true">
						<%
							int totalArticle = searchUtil.searchCount(structuredSearchContainer, themeDisplay,
														structureKey, categoryId, renderRequest);
												structuredSearchContainer.setTotal(totalArticle);
						%>

						<div class="row-fluid row-total-records">
							<span><%=LanguageUtil.format(httpReq, "swtorganizationchart.total-records", dummyObj) + ": " + totalArticle%></span>
						</div>

						<liferay-ui:search-container-results
							results="<%=searchUtil.search(structuredSearchContainer, themeDisplay, structureKey,
										renderRequest)%>" />
						<%
						HashMap map = new HashMap();

						if (parentLevelId <= 0) {

							for (int i = 0; i < results.size(); i++) {

								JournalArticle tempArticle = (JournalArticle) results.get(i);
								
								String articleContent = tempArticle
										.getContentByLocale(themeDisplay.getLanguageId());
								Document articleDoc = null;
								Node node = null;

								try {

									articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
									node = articleDoc.selectSingleNode(
											"/root/dynamic-element[@name='level']/dynamic-content");
									String value = node.getStringValue();

									if (GetterUtil.getLong(value) == 0) {
										map.put(tempArticle.getId(), 0);
										//System.out.println("SWT+"+ tempArticle.getId());
									} else if (map.size() > 0) {
										//System.out.println("SWT+"+ map.get(GetterUtil.getLong(value)));
										if (map.get(GetterUtil.getLong(value)) != null) {
											//System.out.println("SWT Set value");
											int position = (int) map.get(GetterUtil.getLong(value));
											map.put(tempArticle.getId(), position + 1);
										} else {
											map.put(tempArticle.getId(), 0);
										}

										//System.out.println("SWT+"+ map.get(tempArticle.getId()));
									} else {
										map.put(tempArticle.getId(), 0);
									}

								} catch (Exception e) {

									e.printStackTrace();
								}
							}
						}
						%>
						<liferay-ui:search-container-row
							className="com.liferay.journal.model.JournalArticle"
							modelVar="article" indexVar="index" keyProperty="id">
							<%
								// Create the URL for detail view. It's required a display page configured for the entry.
								String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
								Document articleDoc = null;
								try {
									articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
								} catch (DocumentException e) {
									e.printStackTrace();
								}

								Document document = SAXReaderUtil.read(article.getContent());
								Element root = document.getRootElement();
														// System.out.println("Read doc");
							%>
							<portlet:actionURL var="updateURL"
								windowState="<%=LiferayWindowState.POP_UP.toString()%>">
								<portlet:param name="path"
									value="<%=JournalAricleConstants.ACTION_UPDATE_ORGANIZATION%>" />
								<portlet:param name="parentArticleId"
									value="<%=String.valueOf(parentLevelId)%>" />
									
								<portlet:param name="articleId"
									value="<%=String.valueOf(article.getId())%>" />
								<%
									for (Element em : root.elements()) {
								%>
								
								<portlet:param name="<%=em.attributeValue("name")%>"
									value="<%=GetterUtil.getString(em.element("dynamic-content").getText())%>" />
								<%
									}
								%>
							</portlet:actionURL>
							<!-- Order number column. -->
							<%
								if (showOrderInResult) {
							%>
							<liferay-ui:search-container-column-text align="center"
								name="<%=LanguageUtil.format(httpReq, "swtorganizationchart.order-number",
												dummyObj)%>"
								value="<%=String.valueOf(structuredSearchContainer.getStart() + index + 1)%>"
								cssClass="col-order-number" />
							<%
								}
							%>
							<!-- Title column. -->
							<%
								if (showTitleInResult) {
							%>
							<portlet:renderURL var="viewdetail">
								<portlet:param name="structureParentKey"
									value="<%=structureKeyParrent%>" />
								<portlet:param name="parentArticleId"
									value="<%=String.valueOf(article.getId())%>" />
							</portlet:renderURL>
							<liferay-ui:search-container-column-text href="<%=viewUrl + String.valueOf(article.getId())%>"
								name='<%=LanguageUtil.format(request, "title", dummyObj)%>'>
								<%
									if (map.size() > 0) {
								%>
									<%
										if ((int) map.get(article.getId()) == 0) {
											%>
											<liferay-ui:icon iconCssClass="icon-level0" />
											<%
										} else if ((int) map.get(article.getId()) > 0) {
											%>
											<liferay-ui:icon
												iconCssClass='<%="icon-level-child-" + map.get(article.getId()).toString()%>' />
											<%
										} else {
											%>
											<liferay-ui:icon iconCssClass="icon-level0" />
											<%
										}
											%>
								<%
									} else {
								%>
								<liferay-ui:icon iconCssClass="icon-level0" />
								<%
									}
								%>
								<%=String.valueOf(article.getTitle(locale))%>

							</liferay-ui:search-container-column-text>
							<%
								}
							%>
							<!-- Summary column. -->
							<%
								if (showSummaryInResult) {
							%>
							<liferay-ui:search-container-column-text
								name='<%=LanguageUtil.format(request, "description", dummyObj)%>'
								value="<%=String.valueOf(article.getDescription(locale))%>" />
							<%
								}
							%>
							<!-- Additional columns from structure and vocabularies. -->
							<%
								if (articleDoc != null) {
							%>
							<%
								Node node = null;
							%>
							<%
								for (JournalArticleColumnDTO col : displayColumns) {
							%>
							<%

							String title = null, valueStr = null;
							switch (col.getType()) {
								case "text" :
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='"
											+ col.getName() + "']/dynamic-content");
									if (node != null) {
										if (node.getText().length() > 0) {
											title = col.getLabel();
											valueStr = node.getText();
										}
									}

									break;
								case "textarea" :
								case "ddm-text-html" :
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='"
											+ col.getName() + "']/dynamic-content");
									if (node != null) {
										if (node.getText().length() > 0) {
											title = col.getLabel();
											valueStr = node.getText();
										}
									}

									break;
								case "ddm-date" :
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='"
											+ col.getName() + "']/dynamic-content");
									if (node != null) {

										if (node.getText().length() > 0) {
											title = col.getLabel();
											valueStr = node.getText();

											if (valueStr == null) {
												valueStr = StringPool.BLANK;
											}

											// Parse date value to expected format.
											if (valueStr.length() != 0) {
												try {
													Date valueDate = lfDateFormat.parse(valueStr);
													valueStr = viDateFormat.format(valueDate);
												} catch (ParseException e) {
													break;
												}
											}
										}
									}
									break;
								default :
									break;
							}
							%>
							<%
								if (title != null) {
							%>

							<liferay-ui:search-container-column-text name="<%=title.trim()%>"
								value="<%=(valueStr == null ? StringPool.BLANK : valueStr.trim())%>" />
							<%
								}}}
							%>
							<liferay-ui:search-container-column-text align="center"
								name='<%=LanguageUtil.format(request, "action", dummyObj)%>'>
								<liferay-ui:icon cssClass="btn btn-info"
									iconCssClass="icon-edit icon-white" message="update"
									url="javascript:showCategoryDialog('${updateURL}')" />
								<portlet:actionURL var="removeURL"
									name="<%=JournalAricleConstants.ACTION_REMOVE_ORGANIZATION%>">
									<portlet:param name="articleId"
										value="<%=String.valueOf(article.getId())%>" />
								</portlet:actionURL>
								<liferay-ui:icon cssClass="btn btn-danger"
									iconCssClass="icon-remove icon-white" message="delete"
									url="javascript:removeCategory('${removeURL}')" />
							</liferay-ui:search-container-column-text>

						</liferay-ui:search-container-row>

						<liferay-ui:search-iterator />
					</liferay-ui:search-container>
				</div>
				<%}else if (displayStyle == JournalAricleConstants.TABLE_STYLE_2_ONLYCHILD){
					//System.out.println("displayStyle " + JournalAricleConstants.TABLE_STYLE_2_ONLYCHILD);
				%>
			
					<div id="<portlet:namespace />searchContainer"
						class="padding-top-35px">
						<liferay-ui:search-container id="tblSearchResults"
							cssClass="table-search-result"
							searchContainer="<%=structuredSearchContainer%>"
							emptyResultsMessage="there-are-no-results" delta="3"
							deltaConfigurable="true">
							<%
								int totalArticle = searchUtil.searchCount(structuredSearchContainer, themeDisplay,
															structureKey, categoryId,parentLevelId, renderRequest);
								structuredSearchContainer.setTotal(totalArticle);
							%>
	
							<div class="row-fluid row-total-records">
								<span><%=LanguageUtil.format(httpReq, "swtorganizationchart.total-records", dummyObj)
											+ ": " + totalArticle%></span>
							</div>
	
							<liferay-ui:search-container-results
								results="<%=searchUtil.search(structuredSearchContainer, themeDisplay, structureKey,
										parentLevelId, renderRequest)%>" />
							<%
							HashMap map = new HashMap();
							//System.out.println("results: " + results.size());
							%>
							<liferay-ui:search-container-row
								className="com.liferay.journal.model.JournalArticle"
								modelVar="article" indexVar="index" keyProperty="id">
								<%
									// Create the URL for detail view. It's required a display page configured for the entry.
									String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
									Document articleDoc = null;
									try {
										articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
									} catch (DocumentException e) {
										e.printStackTrace();
									}
	
									Document document = SAXReaderUtil.read(article.getContent());
									Element root = document.getRootElement();
															// System.out.println("Read doc");
								%>
								<portlet:actionURL var="updateURL"
									windowState="<%=LiferayWindowState.POP_UP.toString()%>">
									<portlet:param name="path"
										value="<%=JournalAricleConstants.ACTION_UPDATE_ORGANIZATION%>" />
									<portlet:param name="articleId"
										value="<%=String.valueOf(article.getId())%>" />
									<portlet:param name="parentArticleId"
										value="<%=String.valueOf(parentLevelId)%>" />
									<%
										for (Element em : root.elements()) {
									%>
									<portlet:param name="<%=em.attributeValue("name")%>"
										value="<%=GetterUtil.getString(em.element("dynamic-content").getText())%>" />
									<%
										}
									%>
								</portlet:actionURL>
								<!-- Order number column. -->
								<%
									if (showOrderInResult) {
								%>
								<liferay-ui:search-container-column-text align="center"
									name="<%=LanguageUtil.format(httpReq, "swtorganizationchart.order-number",
													dummyObj)%>"
									value="<%=String.valueOf(structuredSearchContainer.getStart() + index + 1)%>"
									cssClass="col-order-number" />
								<%
									}
								%>
								<!-- Title column. -->
								<%
									if (showTitleInResult) {
								%>
								<portlet:renderURL var="viewdetail">
									<portlet:param name="structureParentKey"
										value="<%=structureKeyParrent%>" />
									<portlet:param name="parentArticleId"
										value="<%= GetterUtil.getString(parentLevelId)%>" />
								</portlet:renderURL>
								<liferay-ui:search-container-column-text href="<%=viewdetail%>"
									name='<%=LanguageUtil.format(request, "title", dummyObj)%>'>
									<liferay-ui:icon iconCssClass="icon-level0" />
									<%=String.valueOf(article.getTitle(locale))%>
	
								</liferay-ui:search-container-column-text>
								<%
									}
								%>
								<!-- Summary column. -->
								<%
									if (showSummaryInResult) {
								%>
								<liferay-ui:search-container-column-text
									name='<%=LanguageUtil.format(request, "description", dummyObj)%>'
									value="<%=String.valueOf(article.getDescription(locale))%>" />
								<%
									}
								%>
								<!-- Additional columns from structure and vocabularies. -->
								<%
									if (articleDoc != null) {
								%>
								<%
									Node node = null;
								%>
								<%
									for (JournalArticleColumnDTO col : displayColumns) {
								%>
								<%
	
								String title = null, valueStr = null;
								switch (col.getType()) {
									case "text" :
										node = articleDoc.selectSingleNode("/root/dynamic-element[@name='"
												+ col.getName() + "']/dynamic-content");
										if (node != null) {
											if (node.getText().length() > 0) {
												title = col.getLabel();
												valueStr = node.getText();
											}
										}
	
										break;
									case "textarea" :
									case "ddm-text-html" :
										node = articleDoc.selectSingleNode("/root/dynamic-element[@name='"
												+ col.getName() + "']/dynamic-content");
										if (node != null) {
											if (node.getText().length() > 0) {
												title = col.getLabel();
												valueStr = node.getText();
											}
										}
	
										break;
									case "ddm-date" :
										node = articleDoc.selectSingleNode("/root/dynamic-element[@name='"
												+ col.getName() + "']/dynamic-content");
										if (node != null) {
	
											if (node.getText().length() > 0) {
												title = col.getLabel();
												valueStr = node.getText();
	
												if (valueStr == null) {
													valueStr = StringPool.BLANK;
												}
	
												// Parse date value to expected format.
												if (valueStr.length() != 0) {
													try {
														Date valueDate = lfDateFormat.parse(valueStr);
														valueStr = viDateFormat.format(valueDate);
													} catch (ParseException e) {
														break;
													}
												}
											}
										}
										break;
									default :
										break;
								}
								%>
								<%
									if (title != null) {
								%>
	
								<liferay-ui:search-container-column-text name="<%=title.trim()%>"
									value="<%=(valueStr == null ? StringPool.BLANK : valueStr.trim())%>" />
								<%
									}}}
								%>
								<liferay-ui:search-container-column-text align="center"
									name='<%=LanguageUtil.format(request, "action", dummyObj)%>'>
									<liferay-ui:icon cssClass="btn btn-info"
										iconCssClass="icon-edit icon-white" message="update"
										url="javascript:showCategoryDialog('${updateURL}')" />
									<portlet:actionURL var="removeURL"
										name="<%=JournalAricleConstants.ACTION_REMOVE_ORGANIZATION%>">
										<portlet:param name="articleId"
											value="<%=String.valueOf(article.getId())%>" />
									</portlet:actionURL>
									<liferay-ui:icon cssClass="btn btn-danger"
										iconCssClass="icon-remove icon-white" message="delete"
										url="javascript:removeCategory('${removeURL}')" />
								</liferay-ui:search-container-column-text>
	
							</liferay-ui:search-container-row>
	
							<liferay-ui:search-iterator />
						</liferay-ui:search-container>
					</div>
				
				<%}%>
				</aui:form>
			</div>
			<aui:script use="liferay-util-window">
				A.one('#<portlet:namespace/>btnAddCategory').on('click', function(event) {
					showCategoryDialog('<%=addCategoryURL%>');
				});
	       </aui:script>
			<script>
			Liferay.provide(window,'<portlet:namespace/>closeCategoryDialog',
			    function(data, dialogId, changed) {
					var A = AUI();
					var dialog = Liferay.Util.Window.getById(dialogId);
					dialog.destroy();
					if (changed === true) {
						window.location.href = '<%=portletURL.toString()%>';
					}
				},
				['liferay-util-window']
			);
		
			function showCategoryDialog(renderURL) {
				Liferay.Util.openWindow({
					cache: false,
					dialog: {
						centered: true,
						height: 670,
						modal: true,
						width: 600
					},
					id: '<portlet:namespace/>dlgCategory',
					title: '<%=portletTitle%>',
						uri : renderURL
					});
				}
	
				function removeCategory(removeURL) {
					$.confirm({
						title : Liferay.Language.get("confirm"),
						content : "Bạn có chắc muốn xóa dữ liệu này không?",
						buttons : {
							confirm : {
								text : Liferay.Language.get("confirm"),
								btnClass : "btn btn-danger",
								action : function() {
									window.location.href = removeURL;
								}
							},
							cancel : {
								text : Liferay.Language.get("cancel"),
								action : function() {
									// Close.
								}
							}
						}
					});
				}
			</script>
		<%
		// Display orgchar for view only
		} else if (displayStyle == JournalAricleConstants.CHART_STYLE_1) {
			
		List<JournalArticle> journalArticles= 
				JournalArticleLocalServiceUtil.getStructureArticles
				(themeDisplay.getScopeGroupId(), structureKey);
		%>
					<div class="container-fluid-sdtc">
						<div class="row row-swt-responsive">
							<div class="organization-user-detail-title">So do to chuc</div>
						</div>
						<div class="row" id="organizationalChart-row-swt-id"
							style="float: left; width: 100%">
							<div class="tree"
								style="float: left; width: 100%; height: 600px; padding: 5% 0px 10% 9%;">
								<ul>
									<li id="organization-first-father-of-father-id-swt">
									<ul>
									<li id="organization-first-father-of-father-id-swt">
									<a class="organization-father"
									 href="http://demo.hochiminhcity.gov.vn/so-do-to-chuc?so-do-to-chuc-chi-tiet-phong=124001">
									 <div class="print-father"></div><span class="organization-title-box-all-content"><p class="style-white" style="color:#fffbfb;">Ủy ban nhân dân thành phố</p><p class="organization-box-child" style="margin-top:1%;">5</p></span><span class="organization-describle-one" style="display: none;"><span style="float:left;width:100%;"><span class="organization-describle-one-line"><span class="organization-describle-one-three">Địa chỉ</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Điện thoại</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Fax</span><span class="organization-describle-one-seven"></span></span></span></span></a><div class="div-father"></div><ul><li><a class="organization-child-of-child-swt" href="http://demo.hochiminhcity.gov.vn/so-do-to-chuc?so-do-to-chuc-chi-tiet-phong=124002"><span class="organization-title-box-all-content"><p class="style-white" style="color:#fffbfb;">Sở - Ban - Ngành</p><p class="organization-box-child" style="margin-top:1%;">1</p></span><span class="organization-describle-one"><span style="float:left;width:100%;"><span class="organization-describle-one-line"><span class="organization-describle-one-three">Địa chỉ</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Điện thoại</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Fax</span><span class="organization-describle-one-seven"></span></span></span></span></a></li><li><a class="organization-child-of-child-swt" href="http://demo.hochiminhcity.gov.vn/so-do-to-chuc?so-do-to-chuc-chi-tiet-phong=124004"><span class="organization-title-box-all-content"><p class="style-white" style="color:#fffbfb;">Quận huyện</p><p class="organization-box-child" style="margin-top:1%;">0</p></span><span class="organization-describle-one"><span style="float:left;width:100%;"><span class="organization-describle-one-line"><span class="organization-describle-one-three">Địa chỉ</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Điện thoại</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Fax</span><span class="organization-describle-one-seven"></span></span></span></span></a></li><li><a class="organization-child-of-child-swt" href="http://demo.hochiminhcity.gov.vn/so-do-to-chuc?so-do-to-chuc-chi-tiet-phong=124005"><span class="organization-title-box-all-content"><p class="style-white" style="color:#fffbfb;">Đoàn Hội</p><p class="organization-box-child" style="margin-top:1%;">0</p></span><span class="organization-describle-one"><span style="float:left;width:100%;"><span class="organization-describle-one-line"><span class="organization-describle-one-three">Địa chỉ</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Điện thoại</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Fax</span><span class="organization-describle-one-seven"></span></span></span></span></a></li><li><a class="organization-child-of-child-swt" href="http://demo.hochiminhcity.gov.vn/so-do-to-chuc?so-do-to-chuc-chi-tiet-phong=124006"><span class="organization-title-box-all-content"><p class="style-white" style="color:#fffbfb;">Cơ quan đại diện</p><p class="organization-box-child" style="margin-top:1%;">0</p></span><span class="organization-describle-one"><span style="float:left;width:100%;"><span class="organization-describle-one-line"><span class="organization-describle-one-three">Địa chỉ</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Điện thoại</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Fax</span><span class="organization-describle-one-seven"></span></span></span></span></a></li><li><a class="organization-child-of-child-swt" href="http://demo.hochiminhcity.gov.vn/so-do-to-chuc?so-do-to-chuc-chi-tiet-phong=124007"><span class="organization-title-box-all-content"><p class="style-white" style="color:#fffbfb;">Ban quản lý, Tổng công ty</p><p class="organization-box-child" style="margin-top:1%;">0</p></span><span class="organization-describle-one"><span style="float:left;width:100%;"><span class="organization-describle-one-line"><span class="organization-describle-one-three">Địa chỉ</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Điện thoại</span><span class="organization-describle-one-seven"></span></span><span class="organization-describle-one-line"><span class="organization-describle-one-three">Fax</span><span class="organization-describle-one-seven"></span></span></span></span></a></li></ul></li></ul>
								</li>
								</ul>
							</div>
						</div>
					</div>
		<%
		}
		%>

	</c:otherwise>
</c:choose>

