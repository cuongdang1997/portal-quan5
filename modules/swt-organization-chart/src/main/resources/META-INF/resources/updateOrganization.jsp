<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="com.swt.organization.chart.util.DDMStructureUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>

<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.asset.kernel.model.AssetRenderer"%>

<%@page import="java.io.StringReader"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@ include file="init.jsp"%>
<%
DDMStructure structure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId,structureKey);
List<DDMFormField> listFile = DDMStructureUtil.getDDMFormFieldByStructureKey(structureKey, themeDisplay);

%>
<portlet:actionURL  var="updateCategoryURL" name="<%=JournalAricleConstants.ACTION_UPDATE_ORGANIZATION%>">

</portlet:actionURL>

<liferay-ui:success key="categoryAdded" message="successfully-saved" />
<liferay-ui:success key="categoryUpdated" message="successfully-saved" />

<liferay-ui:error key="categoryNameAlreadyExist" message="categorylist.name-already-exist" />
<liferay-ui:error key="categoryNameMustNotBlank" message="categorylist.name-must-not-blank" />
<liferay-ui:error key="categoryNotFound" message="categorylist.data-not-found" />

<%
	//Item to edit
	String articleId = request.getParameter("articleId");
	//Parrentlevel
	String parentArticleId = request.getParameter("parentArticleId");
	
	JournalArticle journalArticle =  JournalArticleLocalServiceUtil.fetchJournalArticle(GetterUtil.getLong(articleId));
	AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
	List<AssetCategory> assetCategories =  entry.getCategories();
	AssetCategory assetCategory = assetCategories.get(0);
	
	String articleContent = journalArticle.getContentByLocale(themeDisplay.getLanguageId());
	Document articleDoc = null;
	Node node = null;
	
	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
	//Element root = articleDoc.getRootElement();
	node = articleDoc.selectSingleNode(
			"/root/dynamic-element[@name='level']/dynamic-content");
	String level =  node.getStringValue();
%>
       <div class="container-fluid">
	        <aui:form name="<portlet:namespace />fmupdate" action="<%=updateCategoryURL.toString()%>" method="POST">
				<input type="hidden" name="urlCurrent" value="<%=themeDisplay.getURLCurrent()%>"> 
				<input type="hidden" name="structureId" value="<%= String.valueOf(structure.getStructureId())%>"> 
				<input type="hidden" name="structureKey" value="<%=structure.getStructureKey()%>"> 
				<input type="hidden" name="articleId" value="<%=articleId%>"> 
			<aui:select class="form-control" name="categoryId" label="Phân Loại" >
			<% for(AssetCategory objectField: listCategory){
			%>
				  <option value="<%=objectField.getCategoryId()%>" 
					  <%if (assetCategory.getCategoryId() == objectField.getCategoryId()){%>
					  	selected
					  <%} %> 
				  >
					<%=objectField.getName() %>
				 </option>
		    <%}%>
			</aui:select>
		    <%
		       for(DDMFormField object :listFile){ 
		    	  
			%>
				<%if(!object.getType().equals("ddm-image")){%>
					<%if(object.getType().equals("text")){ %>
							
						<aui:input class="field form-control" label="<%=object.getLabel().getString(locale)%>" name="<%=object.getName()%>" >
						 	<%if(object.isRequired()){ %>
						 		<aui:validator name="required" />
						 	<%} %>
						 </aui:input>
						
						<%}else if(object.getType().equals("textarea")){ %>
						 <aui:input  class="field form-control" type="textarea" name="<%=object.getName()%>" >
						 	<%if(object.isRequired()){ %>
						 		<aui:validator name="required" />
						 	<%} %>
						 </aui:input>
				
					<%}else if(object.getType().equals("select")){ %>
						<aui:select class="form-control" name="<%=object.getName()%>" id="<%=object.getName()%>" type="<%=object.getType()%>">
							
							<%if(object.isRequired()){ %>
						 		<aui:validator name="required" />
						 	<%} %>
						 	
							 <%if(structureKeyParrent.equalsIgnoreCase("0")){%>
								 <aui:option value="0"><%=LanguageUtil.format(locale, "vui-long-chon", dummyObj)%></aui:option>
							 <%}%>
							 <%
					 
							 if(structureKey!=""){
								 List<JournalArticle> listArticle = 
										 DDMStructureUtil.getListJounalArticleByIdStructure(
												 themeDisplay, structureKeyParrent.equals("0")?structureKey:structureKeyParrent);				 %>
							 <%if(listArticle.size()!=0){
								
								 //Do not allow to change parrent org for "nhan su" structure
									 if(!structureKeyParrent.equalsIgnoreCase("0")){
										 for(JournalArticle objectField: listArticle){
											 
											 long selectedParrent = GetterUtil.getLong(parentArticleId);
											 //System.out.println(selectedParrent);
											 if(objectField.getId() == selectedParrent){
												 %>
												 <option value="<%=objectField.getId()%>">
										       		<%=objectField.getTitle() %>
										 		</option>
											<%}		 
									 }}else 
										 for(JournalArticle objectField: listArticle){
											 
											 if( objectField.getId() != GetterUtil.getLong(articleId)){
												 %>
												  <option value="<%=objectField.getId()%>" 
													  <%if(GetterUtil.getLong(level) ==objectField.getId() ){ %>
													  		selected
													  <%}%>
												  >
												       <%=objectField.getTitle() %>
												 </option>
									 		<%}
											 } }%>
							 <%} %>
						</aui:select>
					<%}else{%>
			    	
					<aui:input class="field form-control" label="<%=object.getLabel().getString(locale)%>" name="<%=object.getName()%>" >
						<%if(object.isRequired()){ %>
				 			<aui:validator name="required" />
				 		<%} %>
					</aui:input>
			    
				<%}}else{%>
					<aui:input  name="<%=object.getName()%>" type="file" multiple="false" label="<%=object.getLabel().getString(locale)%>" >
						<aui:validator name="acceptFiles">
					        'jpg, png'
					    </aui:validator>
					    <%if(object.isRequired()){ %>
			 			<aui:validator name="required" />
			 			<%} %>
					</aui:input>	
				<%}%>	
			<%}%>
		<div id="category-button-row" class="row-fluid">
			<aui:button type="submit" name="submit" />
			<aui:button type="button" name="cancel" value='<%=LanguageUtil.format(locale, "cancel", dummyObj)%>' />
		</div>
	</aui:form>
</div>


<aui:script>
	function deleteIcon(){
		$("[id$='isDeleteIcon']").val(true);
		$(".category-icon-display").css("display", "none");
	}
</aui:script>