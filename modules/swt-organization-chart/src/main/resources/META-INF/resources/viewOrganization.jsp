<%@page import="com.liferay.taglib.portlet.RenderURLParamsTag"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="com.swt.organization.chart.util.DDMStructureUtil"%>
<%@page import="com.swt.organization.chart.model.JournalArticleUtilImpl"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleDisplayTerms"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleContainer"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleColumnDTO"%>
<%@page	import="com.swt.organization.chart.model.JournalArticleCriteriaDTO"%>
<%@page	import="com.swt.organization.chart.util.OrganizationChartUtil"%>
<%@page	import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.asset.kernel.model.AssetRenderer"%>
<%@page	import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page	import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page	import="com.liferay.journal.web.asset.JournalArticleAssetRenderer"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="java.io.StringReader"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>

<%@include file="init.jsp"%>
<head>
<base href="/">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/main.css"/>
</head>

<style media="all" scoped>
@media only screen and (max-width: 969px){

    .min_wrap {
        width: 100% !important;;
    }
    .div-father{
        display: unset !important;;
    }
    .organization-elenment-father-of-father-id-swt{
        margin: 20px auto 25px auto !important;
    }
    #organization-first-father-of-father-id-swt ul li ul {
        width: 50% !important;;
        height: 0 !important;;
    }
    
    .div-father ul, .div-father ul:nth-child(2n){
        border: 0 !important;
        display: table !important;;
    }
    .organization-elenment-father-of-father-id-swt:first-child::after {
        left: 0 !important;;
    }
    .organization-elenment-father-of-father-id-swt li::after, .organization-elenment-father-of-father-id-swt:nth-child(2n) li::after, .organization-father:after{

	background: unset !important;
    background-color: unset !important;
	width: 0 !important;
}

.organization-elenment-father-of-father-id-swt::after{
	height: 0 !important;
}

}

.tree{
    float: left;
    width: 100%;
}
.tree ul {
    position: relative;
    padding: 0;
    margin: 0;
}
.tree li {
    text-align: center;
    list-style-type: none;
    position: relative;
    padding: 0;
    margin: 45px auto -34px auto;
    width: 100%;
}
.tree li a {
    position: relative;
    height: 60px;
    width: 149px;
    color: white;
    font-weight: bold;
    font-size: 11px;
    display: inline-block;
    text-transform: uppercase;
    text-align: center;
    background: linear-gradient(#00467e, #1682c0);
    margin: 0 3px;
    outline: 2px solid #1670a5;
    border: 1px solid #308fc7;

}

.organization-title-box-all-content {
    font-size: 14px;
    position: absolute;
    display: table;
    left: 7px;
    top: 0px;
    width: 91%;
    height: 62px;
}
.organization-title-box-all-content p {
    color: #fffbfb;
    vertical-align: middle;
    text-align: center;
    padding:3px;
}
.organization-father .organization-box-child {
    display: none;
}
#organization-first-father-of-father-id-swt ul li ul {
    width: 40%;
}

.div-father {
    display: flex;
    height: max-content;
    margin: -5px auto;
}


.organization-father:after {
   content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    background-color: #0774A9 !important;
    width: 2px;
    height: 22px;

}
.div-father ul li a:after {
    display: none;
}
.div-father ul {
    border-right: 2px solid #0774A9;
    height: 100%;
    margin: 20px auto 10% -2px;

}
.div-father ul:nth-child(2n) {
    border-left: 2px solid #0774A9; 
    border-right: 0;
    height: 100%;
    margin: 20px 0 20px -2px;
    
}
.organization-elenment-father-of-father-id-swt{
    
}

.organization-elenment-father-of-father-id-swt::after{
    content: " ";
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/bar.png);
    display: block;
    width: 100%;
    height: 2px;
    position: absolute;
    top: 0;
    right: 0;
}

.organization-elenment-father-of-father-id-swt:first-child::after{
    content: " ";
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/bar.png);
  	width: 52%;
    height: 2px;
    position: absolute;
    top: 0;
    left: 100%;

}


.organization-elenment-father-of-father-id-swt li::after{
    content: " ";
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/arrow_left.png) no-repeat;
    display: block;
    width: 28px;
    height: 20px;
    position: absolute;
    top: 16px;
    right: 0;
}

.organization-elenment-father-of-father-id-swt:nth-child(2n) li::after{
    content: " ";
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/arrow_right.png) no-repeat;
    display: block;
    width: 28px;
    height: 20px;
    position: absolute;
    top: 16px;
    left: 0;
}

.organization-elenment-father-of-father-id-swt:nth-child(3)::after, .organization-elenment-father-of-father-id-swt:nth-child(5)::after{
   background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/bar.png) !important;
}

.organization-elenment-father-of-father-id-swt:last-child::after {
    background: unset;
}
.organization-elenment-father-of-father-id-swt:only-child{
	display: grid;
   /*  grid-template-columns: repeat(2, 1fr); */
    width: 100% !important;
    background: unset !important;
    border-left: 2px solid #0774A9;
    margin-left: 50%;
}


.organization-elenment-father-of-father-id-swt li:only-child {
    grid-column: 1;
}

.div-father ul:only-child{
	border-right: none;
}
.organization-elenment-father-of-father-id-swt:only-child {
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/arrow_right.png) no-repeat 0 50%;
	border-left: 2px solid #0774A9;
	margin-left: 50%;
}
.div-father ul:only-child {
    border-right: none;
}
.organization-elenment-father-of-father-id-swt:only-child>li::after {
   content: " ";
    background: url(<%=themeDisplay.getPathThemeImages()%>/orgchart/arrow_right.png) no-repeat;
    display: block;
    width: 28px;
    height: 20px;
    position: absolute;
    top: 16px;
    left: 0;
}

.organization-describle-title {
    text-align: center;
    font-size: 13px;
    background-color: #deefff;
    padding: 8px;
    border-bottom: 1px solid #3ca1ff;
    color: #074489;
    height: 32px;

}

.organization-elenment-father-of-father-id-swt:only-child .arrow_organization {
    grid-column: 1;
	grid-row: 1;
}

.organization-elenment-father-of-father-id-swt:only-child .arrow_organization::after {
    content: '';
    display: block;
    margin: 0px 0px 0 -8px;
    padding: 0;
    width: 8px;
    height: 8px;
    transform: rotate(90deg);
    position: absolute;
    top: 14px;
    left: 100%;
    border: 0 !important;
}

.organization-elenment-father-of-father-id-swt:last-child > li > a > .print-father > .organization-describle-one{
	left: 0 !important;
}

.organization-describle-one-line {
    display: list-item;
    text-align: left;
}
.organization-describle-one-three {
    font-weight: bold;
    color: #2268d7;
}
.organization-describle-one-seven {
    font-weight: normal;
}

.organization-describle-one {
    position: absolute;
    z-index: 2;
    display: none;
    color: #000;
    border: 2px solid #3ca1ff;
    background-color: #ffffff;
    width: 350px;
    margin: -240px 0px 0px -150px;
    left: 150px !important;
}

.organization-describle-title {
    text-align: center;
    font-size: 13px;
    background-color: #deefff;
    padding: 8px;
    border-bottom: 1px solid #3ca1ff;
    color: #074489;
    height: 32px;
}

.tree {
height: 100% !important;
margin-bottom: 50px;
}

.organization-title-box-all-content p {
    color: #fffbfb;
    vertical-align: middle;
    text-align: center;
    font-size: 13px;
    margin:unset !important;
padding: 5px;
}

.organization-describle-one {
    width: 350px;
}

.print-org{
    background: url(/o/com.liferay.asset.publisher.web/icons/print.gif) no-repeat 46px 0px;
    color: #686868;
    cursor: pointer;
    font-weight: normal;
    padding: 1px 0px 0px 65px;
    float: right;
    font-size: 14px;
    margin-top: -21px;

}



</style>


<script type="text/javascript">

var valueWidth = 0;

$(".organization-child-of-child-swt").each(function(){
	valueWidth =  $(this).width() + 75;
});

$(function() {
	
	//$('.print-father').mousetip('.organization-describle-one', 20, 30);		
	$.fn.mousetip = function(tip, x, y) {	
		var $this = $(this);	
		$this.hover(function() {		
			$(tip, this).show();	
		}, function() {	
			$(tip, this).hide().removeAttr('style');	
		}).mousemove(function(e) {		
			var mouseX = e.pageX +10 ;
			var mouseY = e.offsetY + 30;	
			$(tip, this).show().css({			
				top:mouseY, left:mouseX			
			});
		});
	};
	$('.print-father').mousetip('.organization-describle-one');
});

function printPost() {
	
	 var title = document.getElementsByClassName('father')[0];
	var gridData = document.getElementsByClassName('div-father')[0];
	var fileCss = '<link rel=\"stylesheet\" href=\"'+window.location.origin+'/Portal_OrganizationalChart-portlet/css/sodotochuc.css\" media=\"print\">';
   var windowUrl = ' ';
   //set print document name for gridview
   var uniqueName = new Date();
   var windowName = 'Print_' + uniqueName.getTime();
   var prtWindow = window.open(windowUrl, windowName, '');
       prtWindow.document.write('<html><head>');
       prtWindow.document.write(fileCss);
       prtWindow.document.write('</head>');
       prtWindow.document.write('<body onload="window.print()">');
       prtWindow.document.write(fileCss);
       prtWindow.document.write('<h1>');
       prtWindow.document.write(title.outerHTML);
       prtWindow.document.write('</h1>');
       prtWindow.document.write(gridData.outerHTML);
       prtWindow.document.write('</body></html>');
       prtWindow.document.close();
       prtWindow.focus();
       prtWindow.print();
       prtWindow.close(); 

} 

</script>

<%
	//Get all level child of parrent level
	HttpServletRequest httpReq = PortalUtil
	.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
	com.swt.organization.chart.model.JournalArticleUtil searchUtil = new JournalArticleUtilImpl();
	PortletURL portletURL = renderResponse.createRenderURL();
	
	parentLevelId = GetterUtil.getLong(ParamUtil.get(httpReq, "parentArticleId","0"));
	
	JournalArticleContainer structuredSearchContainer = new JournalArticleContainer(renderRequest, portletURL);
	structuredSearchContainer.setTotal(Integer.MAX_VALUE);
	
	List<JournalArticle> results = null;
	
	if(parentLevelId <=0){
		results = searchUtil.search(structuredSearchContainer, themeDisplay, structureKeyParrent.equals("0")?structureKey:structureKeyParrent,
		renderRequest);
	}else{
		results = searchUtil.search(structuredSearchContainer, themeDisplay, structureKeyParrent.equals("0")?structureKey:structureKeyParrent,
		parentLevelId, renderRequest, true);
	}
	 
	 String viewURL = "";
	 if(themeDisplay.getURLCurrent().indexOf("?")>=0){
		 
		 viewURL = themeDisplay.getURLCurrent();
		 if(viewURL.indexOf("&parentArticleId") >=0){
	 viewURL = viewURL.substring(0,viewURL.indexOf("&parentArticleId"));
	 
	 viewURL += "&parentArticleId=";
		 }else if(viewURL.indexOf("?parentArticleId") >=0){
	 viewURL = viewURL.substring(0,viewURL.indexOf("?parentArticleId"));
	 viewURL += "?parentArticleId=";
		 }else{
	 viewURL += "&parentArticleId=";
		 }
		 
	 }else{
		 viewURL = themeDisplay.getURLCurrent() + "?parentArticleId=";
	 }
	 
	 String viewDetailUrl = "";
	 viewDetailUrl =  PortalUtil.getLayoutFriendlyURL(layoutPortlet, themeDisplay) +"?parentArticleId=";
	 
	 long rootArticleId = 0;
	 JournalArticle rootArticle = null;
	 
	 List<AssetCategory> categoriesGroupArticle = null;
	 List<JournalArticle> displayArticles = new ArrayList<JournalArticle>();
	 
	 results = OrganizationChartUtil.sort(structureKey, themeDisplay, results, "sequence",OrganizationChartUtil.SortOrder.Ascending);
	 
	 if (results.size() > 0){
		 try{
			 
		 rootArticle = (JournalArticle)results.get(0);
		 rootArticleId = rootArticle.getId();
		 
		
		 
		 for (int i = (results.size()-1); i > 0;i--){
			
			Document articleDoc = null;
			Node node = null;
			JournalArticle tempArticle = (JournalArticle) results.get(i);
			try {
				
				String articleContent = tempArticle.getContentByLocale(themeDisplay.getLanguageId());
				articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
				//Element root = articleDoc.getRootElement();
				
				node = articleDoc.selectSingleNode(
						"/root/dynamic-element[@name='level']/dynamic-content");
				if (Validator.isNotNull(node) ){
					
					String level =  node.getStringValue();
					
					if(GetterUtil.getLong(level) == rootArticleId){
						//displayArticles = 
						displayArticles.add(tempArticle);
					}
					
				}
				
			}catch(NullPointerException ex){
				
				//System.out.println("displayArticles EX");
				ex.printStackTrace();
				break;
				
			}
	 }
	 
	//System.out.println("displayArticles" + displayArticles.size());
	
		 }catch(NullPointerException ex){
	 
	 ex.printStackTrace();
	 
		 }
		 
		//Group all article follow category
	 for(JournalArticle tempArticle: displayArticles){
	 	AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), tempArticle.getResourcePrimKey());
		List<AssetCategory> assetCategories =  entry.getCategories();
		
		//System.out.println("assetCategories" + assetCategories.size());
		//first get all category group from article
		if (categoriesGroupArticle == null){
			categoriesGroupArticle = assetCategories;
		}else{
			for(AssetCategory assetCategory:assetCategories){
				if(!categoriesGroupArticle.contains(assetCategory)){
					categoriesGroupArticle.add(assetCategory);
				}
			}
		}
	 }
	 }
%>

   <style>
	</style>

   <div class="container-fluid-sdtc">
   <div class="row" id="organizationalChart-row-swt-id" style="float:left; width: 100%">
      <div class="tree" style="float:left; width: 100%; ">
         <ul>
            <li id="organization-first-father-of-father-id-swt">
               <ul>
                  <li id="organization-first-father-of-father-id-swt">
                  		<%
		                 // layoutPortlet.get
		                
								String articleContent = rootArticle.getContentByLocale(themeDisplay.getLanguageId());
								Document articleDoc = null;
								Node node = null;
								//System.out.println(articleContent);
								try {
		
									articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
									Element root = articleDoc.getRootElement();
									node = articleDoc.selectSingleNode(
											"/root/dynamic-element[@name='name']/dynamic-content");
									String name =  node.getStringValue();
									
									node = articleDoc.selectSingleNode(
											"/root/dynamic-element[@name='address']/dynamic-content");
									String address =  node.getStringValue();
									
									node = articleDoc.selectSingleNode(
											"/root/dynamic-element[@name='fax']/dynamic-content");
									String fax =  node.getStringValue();
									
									node = articleDoc.selectSingleNode(
											"/root/dynamic-element[@name='phone']/dynamic-content");
									String phone =  node.getStringValue();
									
									node = articleDoc.selectSingleNode(
											"/root/dynamic-element[@name='level']/dynamic-content");
									String level =  node.getStringValue();
									
										%>
										<div class="print-father"></div>
										<div class="father">
										<%if ( GetterUtil.getInteger(level) == 0){ %>
										   <a class="organization-father" href="<%=viewDetailUrl + String.valueOf(rootArticleId)%>">
										   <%}else{ %>
										    <a class="organization-father" href="javascript:void(0)">
										   <%} %>
										      <span class="organization-title-box-all-content">
										         <p class="style-white" style="color:#fffbfb;"><%=name.toUpperCase()%></p>
										         
										      </span>
										      <%if ( GetterUtil.getInteger(level) != 0){ %>
										      <span  style="display:none;" class="organization-describle-one">
											  
											      <div class="organization-describle-title" ><%=name.toUpperCase()%></div>
											      <span style="float:left;width:100%; padding: 10px;">
												      <span class="organization-describle-one-line">
												      		<span class="organization-describle-one-three">Địa chỉ:</span>
												      		<span class="organization-describle-one-seven"><%=address%></span>
												      </span>
											      	  <span class="organization-describle-one-line">
													      <span class="organization-describle-one-three">Điện thoại:</span>
													      <span class="organization-describle-one-seven"><%=phone%></span>
											      	  </span>
												      <span class="organization-describle-one-line">
													      <span class="organization-describle-one-three">Fax:</span>
													      <span class="organization-describle-one-seven"><%=fax%></span>
												      </span>
											      </span>
										     
										      </span>
										      <%} %>
										   </a>
										</div>
										<%									
									} catch (Exception e) {
			
										e.printStackTrace();
										
									}%>
										<!-- This part is for child items -->
										 <div class="div-father"> 
										<%
										 try{
											 
										for( AssetCategory assetCategory: categoriesGroupArticle){
											%>
											<ul class="organization-elenment-father-of-father-id-swt">
											<%
											 for (int i = 0; i < displayArticles.size(); i++) {
												 
												 JournalArticle tempArticle = displayArticles.get(i);
												 
												 AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), 
														 tempArticle.getResourcePrimKey());
												 
												 List<AssetCategory> assetCategories =  entry.getCategories();
												 
												 //System.out.println("assetCategories" + assetCategories.size());
												 
												 if(assetCategory.getCategoryId() == assetCategories.get(0).getCategoryId()){
													try {
														//get content of article
														articleContent = tempArticle.getContentByLocale(themeDisplay.getLanguageId());
														
														articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
														//Element root = articleDoc.getRootElement();
														
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='name']/dynamic-content");
														String name =  node.getStringValue();
														
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='address']/dynamic-content");
														String address =  node.getStringValue();
														
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='fax']/dynamic-content");
														String fax =  node.getStringValue();
														
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='phone']/dynamic-content");
														String phone =  node.getStringValue();
														
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='level']/dynamic-content");
														String level =  node.getStringValue();
														
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='website']/dynamic-content");
														String website ="";
														
														if(Validator.isNotNull(node)){
															website =  node.getStringValue();
														}
														
														String email ="";
														node = articleDoc.selectSingleNode(
																"/root/dynamic-element[@name='email']/dynamic-content");
														if(Validator.isNotNull(node)){
															email =  node.getStringValue();
														}
														
														//Check to handle click
														List<JournalArticle> resultTemp = searchUtil.search(structuredSearchContainer, themeDisplay, structureKeyParrent.equals("0")?structureKey:structureKeyParrent,
																tempArticle.getId(), renderRequest, false);
														String viewDetail = "javascript:void(0)";
														
														if(resultTemp.size() >0){
															viewDetail = viewURL + String.valueOf(tempArticle.getId());
														}
														
														%>				
														 <li >
								                           <a class="organization-father" href="<%=viewDetail%>">
														   <div class="print-father">
															   <span class="organization-title-box-all-content">
															      <p class="style-white" style="color:#fffbfb;"><%=name.toUpperCase()%></p>
															     
															   </span>
															   <%if ( parentLevelId > 0){ %>
															   
															  <span style="display:none" class="organization-describle-one">
															  
																  	<div class="organization-describle-title" ><%=name.toUpperCase()%>
																  	</div>
																      <span style="float:left;width:100%; padding: 10px;">
																	      <span class="organization-describle-one-line">
																	      		<span class="organization-describle-one-three">Địa chỉ:</span>
																	      		<span class="organization-describle-one-seven"><%=address %></span>
																	      </span>
																      	  <span class="organization-describle-one-line">
																		      <span class="organization-describle-one-three">Điện thoại:</span>
																		      <span class="organization-describle-one-seven"><%=phone %></span>
																      	  </span>
																      	   <span class="organization-describle-one-line">
																		      <span class="organization-describle-one-three">Fax:</span>
																		      <span class="organization-describle-one-seven"><%=fax %></span>
																	      </span>
																      	  <span class="organization-describle-one-line">
																		      <span class="organization-describle-one-three">Website:</span>
																		      <span class="organization-describle-one-seven"><%=website %></span>
																	      </span>
																	      <span class="organization-describle-one-line">
																		      <span class="organization-describle-one-three">Email:</span>
																		      <span class="organization-describle-one-seven"><%=email %></span>
																	      </span>
																	     
																      </span>
															     </span>
															    <%} %>
														      </div>
														     
															</a>
															<div class="arrow_organization"></div>
								                        </li>
														<%
													} catch (Exception e) {
														e.printStackTrace();
													}
											  	}
											}
											%>
											</ul>
											<%
										}} catch (Exception e) {
											
											e.printStackTrace();
										}
		                 			 %>
                     		</div>
                 		</li>
               		</ul>
            	</li>
         	</ul>
      
      </div>
      <div class="print-org" onclick="printPost()">Bản in</div>
   </div>
</div>