<%@page import="com.swt.organization.chart.util.DDMStructureUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="init.jsp"%>

<%
if(structureKey!=""){
	DDMStructure structure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId,structureKey);
	List<DDMFormField> listFile = DDMStructureUtil.getDDMFormFieldByStructureKey(structureKey, themeDisplay);
	String parentArticleId = request.getParameter("parentArticleId");
%>
	<portlet:actionURL var="addOrganizationURL" name="<%=JournalAricleConstants.ACTION_ADD_ORGANIZATION%>">
	
	</portlet:actionURL>
	
	<liferay-ui:error key="cannotGetVocabulary" message="that-vocabulary-does-not-exist" />
	<liferay-ui:success key="categoryAdded" message="successfully-saved" />
	<liferay-ui:success key="categoryUpdated" message="successfully-saved" />

	<div class="container-fluid">
	<aui:form name="<portlet:namespace />fmAdd" action="<%=addOrganizationURL.toString()%>" method="POST" 
	 enctype='multipart/form-data'
	 >
				<input type="hidden" name="urlCurrent" value="<%=themeDisplay.getURLCurrent()%>"> 
				<input type="hidden" name="structureId" value="<%=structureKey%>"> 
				<input type="hidden" name="structureKey" value="<%=structure.getStructureKey()%>"> 
					
			<aui:select class="form-control" name="categoryId" label="Phân Loại" >
				<% for(AssetCategory objectField: listCategory){
				%>
					  <option value="<%=objectField.getCategoryId()%>">
						<%=objectField.getName() %>
					 </option>
			    <%}%>
			</aui:select>
		    <%
		       for(DDMFormField object :listFile){ 
			%>
					<!--nhung truong type text  -->
					<%if(!object.getType().equals("ddm-image")){%>
					
						<%if(object.getType().equals("text")){ %>
						<aui:input class="field form-control" label="<%=object.getLabel().getString(locale)%>" name="<%=object.getName()%>" >
						 	<%if(object.isRequired()){ %>
						 		<aui:validator name="required" />
						 	<%} %>
						 </aui:input>
						<%}else if(object.getType().equals("textarea")){ %>
						 <aui:input  class="field form-control" type="textarea" label="<%=object.getLabel().getString(locale)%>" name="<%=object.getName()%>" >
						 	<%if(object.isRequired()){ %>
						 		<aui:validator name="required" />
						 	<%} %>
						 </aui:input>
						<%}else if(object.getType().equals("select")){ %>
						<aui:select class="form-control" label="<%=object.getLabel().getString(locale)%>" name="<%=object.getName()%>" >
							<%if(object.isRequired()){ %>
						 		<aui:validator name="required" />
						 	<%} %>
						 	
							 <%if(structureKeyParrent.equalsIgnoreCase("0")){%>
								 <aui:option value="0"><%=LanguageUtil.format(locale, "vui-long-chon", dummyObj)%>
								 	
								 </aui:option>
							 <%}%>
							
							 <%if(structureKey != StringPool.BLANK){ 
								// System.out.println("structureParentKey: " + structureKeyParrent);
								 List<JournalArticle> listArticle = 
										 DDMStructureUtil.getListJounalArticleByIdStructure(
												 themeDisplay, structureKeyParrent.equals("0")?structureKey:structureKeyParrent);
							 %>
								 <%if(listArticle.size()!=0){
									 if(!structureKeyParrent.equalsIgnoreCase("0")){
										 for(JournalArticle objectField: listArticle){
											 long selectedParrent = GetterUtil.getLong(parentArticleId);
											 //System.out.println("objectField.getTitle(): " +parentArticleId);
											 if(objectField.getId() == selectedParrent){
												 %>
												 <option value="<%=objectField.getId()%>">
										       		<%=objectField.getTitle() %>
										 		</option>
											<%}		 
									 		}
										 }else {
										 	for(JournalArticle objectField: listArticle){
											 	boolean selected = false;
												 if(GetterUtil.getLong(objectField.getId()) == parentLevelId){
													 selected = true;
												 }
										 	%>
											  <option value="<%=objectField.getId()%>" selected="<%=selected%>" >
											       <%=objectField.getTitle() %>
											 </option>
									 		<%}%>
								 		<%} %>
						
									<%}} %>
							</aui:select>
						<% 
					    }else{%>
					    	
							<aui:input class="field form-control" label="<%=object.getLabel().getString(locale)%>" name="<%=object.getName()%>" >
								<%if(object.isRequired()){ %>
						 			<aui:validator name="required" />
						 		<%} %>
							</aui:input>
					    
						<%}%>
						
					<%}else{%>

						<aui:input  name="<%=object.getName()%>" type="file" multiple="false" label="<%=object.getLabel().getString(locale)%>" >
							<aui:validator name="acceptFiles">
						        'jpg, png'
						    </aui:validator>
						</aui:input>

					<%}}%>
		<div id="category-button-row" class="row-fluid">
			<aui:button type="submit" name="submit" />
			<aui:button type="button" name="cancel" value='<%=LanguageUtil.format(locale, "cancel", dummyObj)%>' />
		</div>
	</aui:form>
</div>

	<aui:script use="aui-base">
	A.one('#<portlet:namespace/>cancel').on('click', function(event) {
	    var data = '';
		Liferay.Util.getOpener().<portlet:namespace/>closeCategoryDialog(data, '<portlet:namespace/>dlgCategory');
	});
</aui:script>

	<aui:script>
	function hiddenIcon(id){
		$('.category-icon-display').css('display', 'none');
		$('.icon-category-id').val(id);
	}
</aui:script>
<% }else{ %>
	<div class="alert alert-danger" role="alert">
				<span class="glyphicon glyphicon-warning-sign"></span>
	<%=LanguageUtil.format(request, "chua-cau-hinh", new Object()) %>
			</div>

<%}%>