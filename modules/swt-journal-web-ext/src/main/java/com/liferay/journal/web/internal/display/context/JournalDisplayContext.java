/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.journal.web.internal.display.context;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMIndexer;
import com.liferay.frontend.taglib.servlet.taglib.ManagementBarFilterItem;
import com.liferay.journal.configuration.JournalServiceConfiguration;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.journal.constants.JournalWebKeys;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.model.JournalArticleDisplay;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.journal.service.JournalFolderServiceUtil;
import com.liferay.journal.util.JournalConverter;
import com.liferay.journal.util.comparator.FolderArticleArticleIdComparator;
import com.liferay.journal.util.comparator.FolderArticleDisplayDateComparator;
import com.liferay.journal.util.comparator.FolderArticleModifiedDateComparator;
import com.liferay.journal.web.configuration.JournalWebConfiguration;
import com.liferay.journal.web.internal.portlet.action.ActionUtil;
import com.liferay.journal.web.internal.search.EntriesChecker;
import com.liferay.journal.web.internal.search.EntriesMover;
import com.liferay.journal.web.internal.search.JournalSearcher;
import com.liferay.journal.web.util.JournalPortletUtil;
import com.liferay.message.boards.kernel.model.MBMessage;
import com.liferay.message.boards.kernel.service.MBMessageLocalServiceUtil;
import com.liferay.portal.kernel.bean.BeanParamUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.PortalPreferences;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.search.BaseModelSearchResult;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentImpl;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.WorkflowDefinitionLinkLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

/**
 * @author Eudaldo Alonso
 */
@SuppressWarnings("deprecation")
public class JournalDisplayContext {
	
	private static DateTimeFormatter indexedDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static DateTimeFormatter databaseDateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	private static DateTimeFormatter viVnDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private static DateTimeFormatter enUsDateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");

	public JournalDisplayContext(
		HttpServletRequest request, LiferayPortletRequest liferayPortletRequest,
		LiferayPortletResponse liferayPortletResponse,
		PortletPreferences portletPreferences) {

		_request = request;
		_liferayPortletRequest = liferayPortletRequest;
		_liferayPortletResponse = liferayPortletResponse;
		_portletPreferences = portletPreferences;

		_journalWebConfiguration =
			(JournalWebConfiguration)_request.getAttribute(
				JournalWebConfiguration.class.getName());
		_portalPreferences = PortletPreferencesFactoryUtil.getPortalPreferences(
			_request);
	}

	public JournalArticle getArticle() throws PortalException {
		if (_article != null) {
			return _article;
		}

		_article = ActionUtil.getArticle(_request);

		return _article;
	}

	public JournalArticleDisplay getArticleDisplay() throws Exception {
		if (_articleDisplay != null) {
			return _articleDisplay;
		}

		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		long groupId = ParamUtil.getLong(_request, "groupId");
		String articleId = ParamUtil.getString(_request, "articleId");
		double version = ParamUtil.getDouble(_request, "version");
		int page = ParamUtil.getInteger(_request, "page");

		JournalArticle article = JournalArticleLocalServiceUtil.fetchArticle(
			groupId, articleId, version);

		if (article == null) {
			return _articleDisplay;
		}

		_articleDisplay = JournalArticleLocalServiceUtil.getArticleDisplay(
			article, null, null, themeDisplay.getLanguageId(), page,
			new PortletRequestModel(
				_liferayPortletRequest, _liferayPortletResponse),
			themeDisplay);

		return _articleDisplay;
	}

	public List<Locale> getAvailableArticleLocales() throws PortalException {
		JournalArticle article = getArticle();

		if (article == null) {
			return Collections.emptyList();
		}

		List<Locale> availableLocales = new ArrayList<>();

		for (String languageId : article.getAvailableLanguageIds()) {
			availableLocales.add(LocaleUtil.fromLanguageId(languageId));
		}

		return availableLocales;
	}

	public String[] getCharactersBlacklist() throws PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		JournalServiceConfiguration journalServiceConfiguration =
			ConfigurationProviderUtil.getCompanyConfiguration(
				JournalServiceConfiguration.class, themeDisplay.getCompanyId());

		return journalServiceConfiguration.charactersblacklist();
	}

	public SearchContainer<MBMessage> getCommentsSearchContainer()
		throws PortalException {

		SearchContainer<MBMessage> searchContainer = new SearchContainer(
			_liferayPortletRequest, _liferayPortletResponse.createRenderURL(),
			null, null);

		SearchContext searchContext = SearchContextFactory.getInstance(
			_liferayPortletRequest.getHttpServletRequest());

		searchContext.setAttribute(
			Field.CLASS_NAME_ID,
			PortalUtil.getClassNameId(JournalArticle.class));

		searchContext.setAttribute("discussion", Boolean.TRUE);

		List<MBMessage> mbMessages = new ArrayList<>();

		Indexer indexer = IndexerRegistryUtil.getIndexer(MBMessage.class);

		Hits hits = indexer.search(searchContext);

		for (Document document : hits.getDocs()) {
			long entryClassPK = GetterUtil.getLong(
				document.get(Field.ENTRY_CLASS_PK));

			MBMessage mbMessage = MBMessageLocalServiceUtil.fetchMBMessage(
				entryClassPK);

			mbMessages.add(mbMessage);
		}

		searchContainer.setResults(mbMessages);

		searchContainer.setTotal(hits.getLength());

		return searchContainer;
	}

	public int getCommentsTotal() throws PortalException {
		SearchContainer<MBMessage> commentsSearchContainer =
			getCommentsSearchContainer();

		return commentsSearchContainer.getTotal();
	}

	public DDMFormValues getDDMFormValues(DDMStructure ddmStructure)
		throws PortalException {

		if (_ddmFormValues != null) {
			return _ddmFormValues;
		}

		JournalArticle article = getArticle();

		if (article == null) {
			return _ddmFormValues;
		}

		String content = article.getContent();

		if (Validator.isNull(content)) {
			return _ddmFormValues;
		}

		JournalConverter journalConverter = getJournalConverter();

		Fields fields = journalConverter.getDDMFields(ddmStructure, content);

		if (fields == null) {
			return _ddmFormValues;
		}

		_ddmFormValues = journalConverter.getDDMFormValues(
			ddmStructure, fields);

		return _ddmFormValues;
	}

	public String getDDMStructureKey() {
		if (_ddmStructureKey != null) {
			return _ddmStructureKey;
		}

		_ddmStructureKey = ParamUtil.getString(_request, "ddmStructureKey");

		return _ddmStructureKey;
	}

	public String getDDMStructureName() throws PortalException {
		if (_ddmStructureName != null) {
			return _ddmStructureName;
		}

		_ddmStructureName = LanguageUtil.get(_request, "basic-web-content");

		if (Validator.isNull(getDDMStructureKey())) {
			return _ddmStructureName;
		}

		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		DDMStructure ddmStructure = DDMStructureLocalServiceUtil.fetchStructure(
			themeDisplay.getSiteGroupId(),
			PortalUtil.getClassNameId(JournalArticle.class),
			getDDMStructureKey(), true);

		if (ddmStructure != null) {
			_ddmStructureName = ddmStructure.getName(themeDisplay.getLocale());
		}

		return _ddmStructureName;
	}

	public long getDDMStructurePrimaryKey() throws PortalException {
		String ddmStructureKey = getDDMStructureKey();

		if (Validator.isNull(ddmStructureKey)) {
			return 0;
		}

		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		DDMStructure ddmStructure = DDMStructureLocalServiceUtil.fetchStructure(
			themeDisplay.getSiteGroupId(),
			PortalUtil.getClassNameId(JournalArticle.class),
			getDDMStructureKey(), true);

		if (ddmStructure == null) {
			return 0;
		}

		return ddmStructure.getPrimaryKey();
	}

	public List<DDMStructure> getDDMStructures() throws PortalException {
		Integer restrictionType = getRestrictionType();

		return getDDMStructures(restrictionType);
	}

	public List<DDMStructure> getDDMStructures(Integer restrictionType)
		throws PortalException {

		if (_ddmStructures != null) {
			return _ddmStructures;
		}

		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		if (restrictionType == null) {
			restrictionType = getRestrictionType();
		}

		_ddmStructures = JournalFolderServiceUtil.getDDMStructures(
			PortalUtil.getCurrentAndAncestorSiteGroupIds(
				themeDisplay.getScopeGroupId()),
			getFolderId(), restrictionType);

		Locale locale = themeDisplay.getLocale();

		if (_journalWebConfiguration.journalBrowseByStructuresSortedByName()) {
			_ddmStructures.sort(
				(ddmStructure1, ddmStructure2) -> {
					String name1 = ddmStructure1.getName(locale);
					String name2 = ddmStructure2.getName(locale);

					return name1.compareTo(name2);
				});
		}

		return _ddmStructures;
	}

	public String getDDMTemplateKey() {
		if (_ddmTemplateKey != null) {
			return _ddmTemplateKey;
		}

		_ddmTemplateKey = ParamUtil.getString(_request, "ddmTemplateKey");

		return _ddmTemplateKey;
	}

	public String getDisplayStyle() {
		if (_displayStyle != null) {
			return _displayStyle;
		}

		String[] displayViews = getDisplayViews();

		PortalPreferences portalPreferences =
			PortletPreferencesFactoryUtil.getPortalPreferences(_request);

		_displayStyle = ParamUtil.getString(_request, "displayStyle");

		if (Validator.isNull(_displayStyle)) {
			_displayStyle = portalPreferences.getValue(
				JournalPortletKeys.JOURNAL, "display-style",
				_journalWebConfiguration.defaultDisplayView());
		}
		else if (ArrayUtil.contains(displayViews, _displayStyle)) {
			portalPreferences.setValue(
				JournalPortletKeys.JOURNAL, "display-style", _displayStyle);

			_request.setAttribute(
				WebKeys.SINGLE_PAGE_APPLICATION_CLEAR_CACHE, Boolean.TRUE);
		}

		if (!ArrayUtil.contains(displayViews, _displayStyle)) {
			_displayStyle = displayViews[0];
		}

		return _displayStyle;
	}

	public String[] getDisplayViews() {
		if (_displayViews == null) {
			_displayViews = StringUtil.split(
				PrefsParamUtil.getString(
					_portletPreferences, _request, "displayViews",
					StringUtil.merge(_journalWebConfiguration.displayViews())));
		}

		return _displayViews;
	}

	public JournalFolder getFolder() throws PortalException {
		if (_folder != null) {
			return _folder;
		}

		_folder = (JournalFolder)_request.getAttribute(WebKeys.JOURNAL_FOLDER);

		if (_folder != null) {
			return _folder;
		}

		_folder = ActionUtil.getFolder(_request);

		return _folder;
	}

	public long getFolderId() throws PortalException {
		if (_folderId != null) {
			return _folderId;
		}

		JournalFolder folder = getFolder();

		_folderId = BeanParamUtil.getLong(
			folder, _request, "folderId",
			JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID);

		return _folderId;
	}

	public String getFolderTitle() throws PortalException {
		JournalFolder folder = getFolder();

		if (folder != null) {
			return folder.getName();
		}

		return StringPool.BLANK;
	}

	public JournalConverter getJournalConverter() {
		return (JournalConverter)_request.getAttribute(
			JournalWebKeys.JOURNAL_CONVERTER);
	}

	public String getKeywords() {
		if (_keywords != null) {
			return _keywords;
		}

		_keywords = ParamUtil.getString(_request, "keywords");

		return _keywords;
	}

	public List<ManagementBarFilterItem> getManagementBarStatusFilterItems()
		throws PortalException, PortletException {

		List<ManagementBarFilterItem> managementBarFilterItems =
			new ArrayList<>();

		managementBarFilterItems.add(
			getManagementBarFilterItem(WorkflowConstants.STATUS_ANY));
		managementBarFilterItems.add(
			getManagementBarFilterItem(WorkflowConstants.STATUS_DRAFT));

		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		int workflowDefinitionLinksCount =
			WorkflowDefinitionLinkLocalServiceUtil.
				getWorkflowDefinitionLinksCount(
					themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(),
					JournalFolder.class.getName());

		if (workflowDefinitionLinksCount > 0) {
			managementBarFilterItems.add(
				getManagementBarFilterItem(WorkflowConstants.STATUS_PENDING));
			managementBarFilterItems.add(
				getManagementBarFilterItem(WorkflowConstants.STATUS_DENIED));
		}

		managementBarFilterItems.add(
			getManagementBarFilterItem(WorkflowConstants.STATUS_SCHEDULED));
		managementBarFilterItems.add(
			getManagementBarFilterItem(WorkflowConstants.STATUS_APPROVED));
		managementBarFilterItems.add(
			getManagementBarFilterItem(WorkflowConstants.STATUS_EXPIRED));

		return managementBarFilterItems;
	}

	public String getManagementBarStatusFilterValue() {
		return WorkflowConstants.getStatusLabel(getStatus());
	}

	public String getNavigation() {
		if (_navigation != null) {
			return _navigation;
		}

		_navigation = ParamUtil.getString(_request, "navigation", "all");

		return _navigation;
	}

	public String getOrderByCol() {
		if (_orderByCol != null) {
			return _orderByCol;
		}

		_orderByCol = ParamUtil.getString(_request, "orderByCol");

		if (Validator.isNull(_orderByCol)) {
			_orderByCol = _portalPreferences.getValue(
				JournalPortletKeys.JOURNAL, "order-by-col", "modified-date");
		}
		else {
			boolean saveOrderBy = ParamUtil.getBoolean(_request, "saveOrderBy");

			if (saveOrderBy) {
				_portalPreferences.setValue(
					JournalPortletKeys.JOURNAL, "order-by-col", _orderByCol);
			}
		}

		return _orderByCol;
	}

	public String getOrderByType() {
		if (_orderByType != null) {
			return _orderByType;
		}

		_orderByType = ParamUtil.getString(_request, "orderByType");

		if (Validator.isNull(_orderByType)) {
			_orderByType = _portalPreferences.getValue(
				JournalPortletKeys.JOURNAL, "order-by-type", "asc");
		}
		else {
			boolean saveOrderBy = ParamUtil.getBoolean(_request, "saveOrderBy");

			if (saveOrderBy) {
				_portalPreferences.setValue(
					JournalPortletKeys.JOURNAL, "order-by-type", _orderByType);
			}
		}

		return _orderByType;
	}

	public String[] getOrderColumns() {
		String[] orderColumns = {"display-date", "modified-date"};

		if (!_journalWebConfiguration.journalArticleForceAutogenerateId()) {
			orderColumns = ArrayUtil.append(orderColumns, "id");
		}

		return orderColumns;
	}

	public PortletURL getPortletURL() throws PortalException {
		PortletURL portletURL = _liferayPortletResponse.createRenderURL();

		String navigation = ParamUtil.getString(_request, "navigation");
		if (Validator.isNotNull(navigation)) {
			portletURL.setParameter(
				"navigation", HtmlUtil.escapeJS(getNavigation()));
		}

		portletURL.setParameter("folderId", String.valueOf(getFolderId()));

		String ddmStructureKey = getDDMStructureKey();
		if (isNavigationStructure()) {
			portletURL.setParameter("ddmStructureKey", ddmStructureKey);
		}

		String status = ParamUtil.getString(_request, "status");
		if (Validator.isNotNull(status)) {
			portletURL.setParameter("status", String.valueOf(getStatus()));
		}

		String delta = ParamUtil.getString(_request, "delta");
		if (Validator.isNotNull(delta)) {
			portletURL.setParameter("delta", delta);
		}

		String deltaEntry = ParamUtil.getString(_request, "deltaEntry");
		if (Validator.isNotNull(deltaEntry)) {
			portletURL.setParameter("deltaEntry", deltaEntry);
		}

		String displayStyle = ParamUtil.getString(_request, "displayStyle");
		if (Validator.isNotNull(displayStyle)) {
			portletURL.setParameter("displayStyle", getDisplayStyle());
		}

		String keywords = ParamUtil.getString(_request, "keywords");
		if (Validator.isNotNull(keywords)) {
			portletURL.setParameter("keywords", keywords);
		}

		String orderByCol = getOrderByCol();
		if (Validator.isNotNull(orderByCol)) {
			portletURL.setParameter("orderByCol", orderByCol);
		}

		String orderByType = getOrderByType();
		if (Validator.isNotNull(orderByType)) {
			portletURL.setParameter("orderByType", orderByType);
		}

		if (!isShowEditActions()) {
			portletURL.setParameter(
				"showEditActions", String.valueOf(isShowEditActions()));
		}

		String tabs1 = getTabs1();
		if (Validator.isNotNull(tabs1)) {
			portletURL.setParameter("tabs1", tabs1);
		}
		
		// START -- dung.nguyen: Add advanced search paramters.
		String advSearch = ParamUtil.getString(_request, "advSearch");
		if (Validator.isNotNull(advSearch)) {
			portletURL.setParameter("advSearch", advSearch);
			
			Map<String, String[]> paramsMap = _request.getParameterMap();
			for (Map.Entry<String, String[]> param : paramsMap.entrySet()) {
				if (!param.getKey().startsWith("advs_")) {
					continue;
				}
				portletURL.setParameter(param.getKey(), param.getValue());
			}
		}
		// END -- dung.nguyen: Add advanced search paramters.

		return portletURL;
	}

	public int getRestrictionType() throws PortalException {
		if (_restrictionType != null) {
			return _restrictionType;
		}

		JournalFolder folder = getFolder();

		if (folder != null) {
			_restrictionType = folder.getRestrictionType();
		}
		else {
			_restrictionType = JournalFolderConstants.RESTRICTION_TYPE_INHERIT;
		}

		return _restrictionType;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SearchContainer getSearchContainer(boolean showVersions)
		throws PortalException {
		String parrentId = ParamUtil.getString(_request, "parrentId");
		
		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		SearchContainer articleSearchContainer = new SearchContainer(
			_liferayPortletRequest, getPortletURL(), null, null);

		if (!isSearch()) {
			articleSearchContainer.setEmptyResultsMessageCssClass(
				"taglib-empty-result-message-header-has-plus-btn");
		}
		else {
			articleSearchContainer.setSearch(true);
		}
		
		OrderByComparator<JournalArticle> orderByComparator =
			JournalPortletUtil.getArticleOrderByComparator(
				getOrderByCol(), getOrderByType());

		articleSearchContainer.setOrderByCol(getOrderByCol());
		articleSearchContainer.setOrderByComparator(orderByComparator);
		articleSearchContainer.setOrderByType(getOrderByType());

		EntriesChecker entriesChecker = new EntriesChecker(
			_liferayPortletRequest, _liferayPortletResponse);

		entriesChecker.setCssClass("entry-selector");
		entriesChecker.setRememberCheckBoxStateURLRegex(
			StringBundler.concat(
				"^(?!.*", _liferayPortletResponse.getNamespace(),
				"redirect).*(folderId=", String.valueOf(getFolderId()), ")"));

		articleSearchContainer.setRowChecker(entriesChecker);

		EntriesMover entriesMover = new EntriesMover(
			themeDisplay.getScopeGroupId());

		articleSearchContainer.setRowMover(entriesMover);

		if (isNavigationMine() || isNavigationRecent()) {
			boolean includeOwner = true;
			if (isNavigationMine()) {
				includeOwner = false;
			}

			if (isNavigationRecent()) {
				articleSearchContainer.setOrderByCol("create-date");
				articleSearchContainer.setOrderByType(getOrderByType());
			}

			int total = JournalArticleServiceUtil.getGroupArticlesCount(
				themeDisplay.getScopeGroupId(), themeDisplay.getUserId(),
				getFolderId(), getStatus(), includeOwner);

			articleSearchContainer.setTotal(total);

			List results = JournalArticleServiceUtil.getGroupArticles(
				themeDisplay.getScopeGroupId(), themeDisplay.getUserId(),
				getFolderId(), getStatus(), includeOwner,
				articleSearchContainer.getStart(),
				articleSearchContainer.getEnd(),
				articleSearchContainer.getOrderByComparator());

			articleSearchContainer.setResults(results);
		}
		// START -- dung.nguyen: Add search logic for SWT Embedded Journal Web portlet
		else if (isEmbedded() && Validator.isNotNull(getDDMStructureKey()) || Validator.isNotNull(parrentId)) {
			boolean orderByAsc = false;
			if (Objects.equals(getOrderByType(), "asc")) {
				orderByAsc = true;
			}

			Sort sort = null;
			if (Objects.equals(getOrderByCol(), "display-date")) {
				sort = new Sort("displayDate", Sort.LONG_TYPE, orderByAsc);
			} else if (Objects.equals(getOrderByCol(), "id")) {
				sort = new Sort(DocumentImpl.getSortableFieldName(Field.ARTICLE_ID), Sort.STRING_TYPE, !orderByAsc);
			} else if (Objects.equals(getOrderByCol(), "modified-date")) {
				sort = new Sort(Field.MODIFIED_DATE, Sort.LONG_TYPE, orderByAsc);
			}
			
			List<Long> folderIds = new ArrayList<>(1);
			if (getFolderId() != JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {
				folderIds.add(getFolderId());
			}
			
			boolean bsSearchType = ParamUtil.getBoolean(_request, "bs_searchType");
			
			String keywords = getKeywords();
			if (keywords == null) {
				keywords = StringPool.BLANK;
			} else {
				keywords = keywords.trim();
				if(bsSearchType) {
					keywords = "\"" + keywords + "\"";
				}
			}
			
			Hits hits = null;

			boolean advSearch = ParamUtil.getBoolean(_request, "advSearch");
			if (_journalWebConfiguration.journalArticlesSearchWithIndex()) {
				if (advSearch || Validator.isNotNull(parrentId)) {
					//Get searchType
					boolean advs_SearchType = ParamUtil.getBoolean(_request, "advs_searchType");
					
					SearchContext sctx = new SearchContext();
					
					// The list of search clauses.
					List<BooleanClause> searchClauses = new ArrayList<>();

					// Get the selected DDM structure.
					DDMStructure structure = null;
					try {
						structure = DDMStructureLocalServiceUtil.fetchStructure(themeDisplay.getScopeGroupId(),
								PortalUtil.getClassNameId(JournalArticle.class), getDDMStructureKey(), true);
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}

					// Get the DDM structure's fields.
					List<DDMFormField> fields = new ArrayList<>();
					if (structure != null) {
						fields = structure.getDDMFormFields(true);
					}

					BooleanQuery ddmFieldQuery = BooleanQueryFactoryUtil.create(sctx);
					String fieldSearchVal = null, fieldDataType = null, fieldEncodedName = null;

					// Iterate through each field and check if it's selected for search.
					for (DDMFormField f : fields) {
						fieldDataType = f.getType();

						switch (fieldDataType) {
						case "text":
						case "textarea":
						case "ddm-text-html":
							// Get the search parameter from request.
							fieldSearchVal = ParamUtil.getString(_request, "advs_" + f.getName());
							if (fieldSearchVal == null) {
								fieldSearchVal = StringPool.BLANK;
							} else {
								fieldSearchVal = fieldSearchVal.trim();
								if(!fieldSearchVal.equals(StringPool.BLANK) && advs_SearchType) {
									fieldSearchVal = "\"" + fieldSearchVal + "\"";
								}
							}
							
							if (fieldSearchVal.length() == 0) {
								continue;
							}

							fieldEncodedName = encodeName(structure.getStructureId(), f.getName(),
									themeDisplay.getLocale(), f.getIndexType());
							
							ddmFieldQuery.addTerm(fieldEncodedName, fieldSearchVal, true);
							
							break;

						case "ddm-date":
							fieldEncodedName = encodeName(structure.getStructureId(), f.getName(),
									themeDisplay.getLocale(), f.getIndexType());
							
							LocalDateTime fromDateVal = null, toDateVal = null;

							// Get the "from date" parameter from request.
							fieldSearchVal = ParamUtil.getString(_request, "advs_" + f.getName() + "_from");
							if (fieldSearchVal == null) {
								fieldSearchVal = StringPool.BLANK;
							}
							fieldSearchVal = fieldSearchVal.trim();
							if (fieldSearchVal.length() != 0) {
								try {
									if (themeDisplay.getLanguageId().equals("vi_VN")) {
										fromDateVal = LocalDate.parse(fieldSearchVal, viVnDateFormat).atStartOfDay();
									} else {
										fromDateVal = LocalDate.parse(fieldSearchVal, enUsDateFormat).atStartOfDay();
									}
								} catch (DateTimeParseException e) {
									e.printStackTrace();
									break;
								}
							}

							// Get the "to date" parameter from request.
							fieldSearchVal = ParamUtil.getString(_request, "advs_" + f.getName() + "_to");
							if (fieldSearchVal == null) {
								fieldSearchVal = StringPool.BLANK;
							}
							fieldSearchVal = fieldSearchVal.trim();
							if (fieldSearchVal.length() != 0) {
								try {
									if (themeDisplay.getLanguageId().equals("vi_VN")) {
										toDateVal = LocalDate.parse(fieldSearchVal, viVnDateFormat).atStartOfDay();
									} else {
										toDateVal = LocalDate.parse(fieldSearchVal, enUsDateFormat).atStartOfDay();
									}
								} catch (DateTimeParseException e) {
									e.printStackTrace();
									break;
								}
							}

							// If both "from date" and "to date" are null -> break.
							// Otherwise, set default value for the null one.
							if (fromDateVal == null && toDateVal == null) {
								break;
							} else {
								if (fromDateVal == null) {
									fromDateVal = LocalDateTime.of(1970, 1, 1, 0, 0, 0);
								}
								if (toDateVal == null) {
									toDateVal = LocalDateTime.now();
								}
							}

							// Add the date range search term.
							ddmFieldQuery.addRangeTerm(fieldEncodedName, indexedDateFormat.format(fromDateVal), 
									indexedDateFormat.format(toDateVal));

							break;
						}
					}
					
					// Add the search clause for structure fields.
					searchClauses.add(BooleanClauseFactoryUtil.create(sctx, ddmFieldQuery,
							BooleanClauseOccur.MUST.getName()));

					// Check if create date is selected for search.
					BooleanClause<Query> createDateClause = buildDateRangeClause("advs_createDate", themeDisplay.getLanguageId(), sctx);
					if (createDateClause != null) {
						searchClauses.add(createDateClause);
					}
					
					// Check if modified date is selected for search.
					BooleanClause<Query> modifiedDateClause = buildDateRangeClause("advs_modifiedDate", themeDisplay.getLanguageId(), sctx);
					if (modifiedDateClause != null) {
						searchClauses.add(modifiedDateClause);
					}
					
					// Check if publish date (aka display date) is selected for search.
					BooleanClause<Query> displayDateClause = buildDateRangeClause("advs_displayDate", themeDisplay.getLanguageId(), sctx);
					if (displayDateClause != null) {
						searchClauses.add(displayDateClause);
					}
					
					// Add bad words to query.
					long badWordsSetId = ParamUtil.getLong(_request, "advs_badWordsSetId", 0);
					if (badWordsSetId != 0) {
						BooleanQuery badWordsQuery = BooleanQueryFactoryUtil.create(sctx);
						List<DDLRecord> records = DDLRecordLocalServiceUtil.getRecords(badWordsSetId);
						Value recordVal = null;
						String badWord = null;
						
						for (DDLRecord r : records) {
							List<DDMFormFieldValue> values = r.getDDMFormFieldValues("value");
							for (DDMFormFieldValue v : values) {
								if (v == null) {
									continue;
								}
								recordVal = v.getValue();
								if (recordVal == null) {
									continue;
								}
								badWord = recordVal.getString(themeDisplay.getLocale());
								if (badWord == null) {
									continue;
								}
								badWord = badWord.trim();
								if (badWord.length() == 0) {
									continue;
								}
								badWord = "\"" + badWord.toLowerCase() + "\"";
								
								badWordsQuery.addTerm("title", badWord, true);
								badWordsQuery.addTerm("description", badWord, true);
								badWordsQuery.addTerm("content", badWord, true);
							}
						}
						
						if (badWordsQuery.hasClauses()) {
							searchClauses.add(BooleanClauseFactoryUtil.create(sctx, badWordsQuery,
									BooleanClauseOccur.MUST.getName()));
						}
					}
					
					// Get the list of vocabularies which name starts with the structure ID.
					String vocabularyPrefix = structure.getStructureId() + "_";
					List<AssetVocabulary> vocabularies = null;
					try {
						BaseModelSearchResult<AssetVocabulary> av = AssetVocabularyLocalServiceUtil.searchVocabularies(
								themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), vocabularyPrefix, 0,
								Byte.MAX_VALUE);
						vocabularies = av.getBaseModels();
					} catch (PortalException e) {
						e.printStackTrace();
					}

					// Iterate through each vocabulary and check if it's selected for search.
					List<Long> categoryIdsList = new ArrayList<>();
					if (vocabularies != null) {
						String vocabularyVal = null;
						
						for (AssetVocabulary v : vocabularies) {
							vocabularyVal = ParamUtil.getString(_request, "advs_vocabulary_" + v.getVocabularyId());
							if (vocabularyVal == null) {
								vocabularyVal = StringPool.BLANK;
							}
							vocabularyVal = vocabularyVal.trim();
							
							if (vocabularyVal.length() != 0 && !vocabularyVal.equals("0")) {
								long selectedCategoryId = Long.valueOf(vocabularyVal);
								categoryIdsList.add(selectedCategoryId);
							}
						}
					}

					// Set category query parameters.
					long[] categoryIds = new long[categoryIdsList.size()];
					for (int i = 0; i < categoryIds.length; i++) {
						categoryIds[i] = categoryIdsList.get(i);
					}
					if (categoryIds.length != 0) {
						sctx.setAssetCategoryIds(categoryIds);
					}
					
					// Set journal article's attribute parameters.
					Map<String, Serializable> attributes = new HashMap<>();
					attributes.put("ddmStructureKey", getDDMStructureKey());
					
					String title = ParamUtil.getString(_request, "advs_title", StringPool.BLANK).trim();
					if (title.length() != 0) {
						if(advs_SearchType)
							title = "\"" + title + "\"";
						attributes.put(Field.TITLE, title);
					}
					
					String description = ParamUtil.getString(_request, "advs_description", StringPool.BLANK).trim();
					if (description.length() != 0) {
						if(advs_SearchType)
							description = "\"" + description + "\"";
						attributes.put(Field.DESCRIPTION, description);
					}
					
					String createUser = ParamUtil.getString(_request, "advs_createUser", StringPool.BLANK).trim();
					if (createUser.length() != 0) {
						if(advs_SearchType)
							createUser = "\"" + createUser + "\"";
						attributes.put(Field.USER_NAME, createUser);
					}
					
					String modifiedUser = ParamUtil.getString(_request, "advs_modifiedUser", StringPool.BLANK).trim();
					if (modifiedUser.length() != 0) {
						if(advs_SearchType)
							modifiedUser = "\"" + modifiedUser + "\"";
						attributes.put("statusByUserName", modifiedUser);
					}
					
					// Filter logged-in user's articles
					long loggedInUser = themeDisplay.getUserId();
					// Bug #4104 START nhut.dang, If Workflow status = STATUS_DRAFT is user create and Administrator can access
					boolean checkAdmin = false;
					List<Role> lst_role = RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
					for(Role object :lst_role ) {
						if(object.getName().equals("Administrator")|| object.getName().contains("_QuanTri")) {
							checkAdmin = true;
						}
					}
					
					if (getStatus() == WorkflowConstants.STATUS_DRAFT) {
						if (loggedInUser != 0) {
							if(!checkAdmin) {
							attributes.put(Field.USER_ID, loggedInUser);
							}
							attributes.put(Field.STATUS, getStatus());
						}
					} else if (getStatus() == WorkflowConstants.STATUS_ANY) {
						attributes.put(Field.STATUS, getStatus());
						BooleanQuery booleanStatusDraft = BooleanQueryFactoryUtil.create(sctx);
						BooleanQuery booleanStatusInTrash = BooleanQueryFactoryUtil.create(sctx);
						BooleanQuery booleanStatusUserId = BooleanQueryFactoryUtil.create(sctx);
						BooleanQuery innerBooleanQuery = BooleanQueryFactoryUtil.create(sctx);

						booleanStatusDraft.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_DRAFT);
						booleanStatusInTrash.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_IN_TRASH);
						if(!checkAdmin) {
						booleanStatusUserId.addRequiredTerm(Field.USER_ID, loggedInUser);
						}
						// Bug #4104 End nhut.dang, If Workflow status = STATUS_DRAFT is user create and Administrator can access
						innerBooleanQuery.add(booleanStatusDraft, BooleanClauseOccur.MUST);
						innerBooleanQuery.add(booleanStatusUserId, BooleanClauseOccur.MUST_NOT);

						searchClauses.add(BooleanClauseFactoryUtil.create(sctx, innerBooleanQuery,
								BooleanClauseOccur.MUST_NOT.getName()));
						searchClauses.add(BooleanClauseFactoryUtil.create(sctx, booleanStatusInTrash,
								BooleanClauseOccur.MUST_NOT.getName()));
					} else {
						attributes.put(Field.STATUS, getStatus());
					}
					
					sctx.setBooleanClauses(searchClauses.toArray(new BooleanClause[searchClauses.size()]));
					
					sctx.setAttributes(attributes);
					
					sctx.setAttribute("head", !showVersions);
					sctx.setAttribute("latest", !showVersions);
					
					if (!showVersions) {
						sctx.setAttribute("showNonindexable", Boolean.TRUE);
					}
					
					// Set other settings.
					sctx.setCompanyId(themeDisplay.getCompanyId());
					sctx.setGroupIds(new long[] { themeDisplay.getScopeGroupId() });
					sctx.setFolderIds(folderIds);
					sctx.setStart(articleSearchContainer.getStart());
					sctx.setEnd(articleSearchContainer.getEnd());
					
					if (sort != null) {
						sctx.setSorts(sort);
					}
					QueryConfig queryConfig = new QueryConfig();
					queryConfig.setHighlightEnabled(false);
					queryConfig.setScoreEnabled(false);
					sctx.setQueryConfig(queryConfig);
					
					sctx.setIncludeAttachments(false);
					sctx.setIncludeFolders(false);

					Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);
					try {
						hits = indexer.search(sctx);
					} catch (SearchException e) {
						e.printStackTrace();
					}
				} else {
					LinkedHashMap<String, Object> params = new LinkedHashMap<>();
					params.put("expandoAttributes", keywords);

					Indexer indexer = null;
					if (!showVersions) {
						indexer = JournalArticleSearcher.getInstance();
					} else {
						indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);
					}
					SearchContext searchContext = buildSearchContext(themeDisplay.getCompanyId(),
							themeDisplay.getScopeGroupId(), folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
							getDDMStructureKey(), getDDMTemplateKey(), keywords, params,
							articleSearchContainer.getStart(), articleSearchContainer.getEnd(), 
							sort, showVersions, themeDisplay.getUserId());
					
					hits = indexer.search(searchContext);
				}

				int total = hits.getLength();
				articleSearchContainer.setTotal(total);

				Document[] documents = hits.getDocs();
				List<JournalArticle> results = new ArrayList<>();

				for (Document document : documents) {
					JournalArticle article = null;

					String className = document.get(Field.ENTRY_CLASS_NAME);
					long classPK = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));

					if (className.equals(JournalArticle.class.getName())) {
						if (!showVersions) {
							article = JournalArticleLocalServiceUtil.fetchLatestArticle(classPK,
									WorkflowConstants.STATUS_ANY, false);
						} else {
							String articleId = document.get(Field.ARTICLE_ID);
							long groupId = GetterUtil.getLong(document.get(Field.GROUP_ID));
							double version = GetterUtil.getDouble(document.get(Field.VERSION));
							article = JournalArticleLocalServiceUtil.fetchArticle(groupId, articleId, version);
						}
						results.add(article);
					}
				}

				articleSearchContainer.setResults(results);

			} else {
				int total = JournalArticleServiceUtil.searchCount(themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId(), folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
						keywords, -1.0, getDDMStructureKey(), getDDMTemplateKey(), null, null, getStatus(), null);
				articleSearchContainer.setTotal(total);

				List<JournalArticle> results = JournalArticleServiceUtil.search(themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId(), folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
						keywords, -1.0, getDDMStructureKey(), getDDMTemplateKey(), null, null, getStatus(), null,
						articleSearchContainer.getStart(), articleSearchContainer.getEnd(),
						articleSearchContainer.getOrderByComparator());
				articleSearchContainer.setResults(results);
			}
		}
		// END -- dung.nguyen: Add search logic for SWT Embedded Journal Web portlet
		else if (Validator.isNotNull(getDDMStructureKey())) {
			int total = JournalArticleServiceUtil.getArticlesCountByStructureId(
				themeDisplay.getScopeGroupId(), getDDMStructureKey(),
				getStatus());

			articleSearchContainer.setTotal(total);

			List results = JournalArticleServiceUtil.getArticlesByStructureId(
				themeDisplay.getScopeGroupId(), getDDMStructureKey(),
				getStatus(), articleSearchContainer.getStart(),
				articleSearchContainer.getEnd(),
				articleSearchContainer.getOrderByComparator());

			articleSearchContainer.setResults(results);
		}
		else if (Validator.isNotNull(getDDMTemplateKey())) {
			List<Long> folderIds = new ArrayList<>(1);

			if (getFolderId() !=
					JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {

				folderIds.add(getFolderId());
			}

			int total = JournalArticleServiceUtil.searchCount(
				themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(),
				folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
				getKeywords(), -1.0, getDDMStructureKey(), getDDMTemplateKey(),
				null, null, getStatus(), null);

			articleSearchContainer.setTotal(total);

			List results = JournalArticleServiceUtil.search(
				themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(),
				folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
				getKeywords(), -1.0, getDDMStructureKey(), getDDMTemplateKey(),
				null, null, getStatus(), null,
				articleSearchContainer.getStart(),
				articleSearchContainer.getEnd(),
				articleSearchContainer.getOrderByComparator());

			articleSearchContainer.setResults(results);
		}
		else if (isSearch()) {
			List<Long> folderIds = new ArrayList<>(1);

			if (getFolderId() !=
					JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {

				folderIds.add(getFolderId());
			}

			if (_journalWebConfiguration.journalArticlesSearchWithIndex()) {
				boolean orderByAsc = false;

				if (Objects.equals(getOrderByType(), "asc")) {
					orderByAsc = true;
				}

				Sort sort = null;

				if (Objects.equals(getOrderByCol(), "display-date")) {
					sort = new Sort("displayDate", Sort.LONG_TYPE, orderByAsc);
				}
				else if (Objects.equals(getOrderByCol(), "id")) {
					sort = new Sort(
						DocumentImpl.getSortableFieldName(Field.ARTICLE_ID),
						Sort.STRING_TYPE, !orderByAsc);
				}
				else if (Objects.equals(getOrderByCol(), "modified-date")) {
					sort = new Sort(
						Field.MODIFIED_DATE, Sort.LONG_TYPE, orderByAsc);
				}

				LinkedHashMap<String, Object> params = new LinkedHashMap<>();

				params.put("expandoAttributes", getKeywords());

				Indexer indexer = null;

				if (!showVersions) {
					indexer = JournalSearcher.getInstance();
				}
				else {
					indexer = IndexerRegistryUtil.getIndexer(
						JournalArticle.class);
				}

				SearchContext searchContext = buildSearchContext(
					themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(),
					folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
					getDDMStructureKey(), getDDMTemplateKey(), getKeywords(),
					params, articleSearchContainer.getStart(),
					articleSearchContainer.getEnd(), sort, showVersions, 0);

				Hits hits = indexer.search(searchContext);

				int total = hits.getLength();

				articleSearchContainer.setTotal(total);

				List results = new ArrayList<>();

				Document[] documents = hits.getDocs();

				for (Document document : documents) {
					JournalArticle article = null;
					JournalFolder folder = null;

					String className = document.get(Field.ENTRY_CLASS_NAME);
					long classPK = GetterUtil.getLong(
						document.get(Field.ENTRY_CLASS_PK));

					if (className.equals(JournalArticle.class.getName())) {
						if (!showVersions) {
							article =
								JournalArticleLocalServiceUtil.
									fetchLatestArticle(
										classPK, WorkflowConstants.STATUS_ANY,
										false);
						}
						else {
							String articleId = document.get(Field.ARTICLE_ID);
							long groupId = GetterUtil.getLong(
								document.get(Field.GROUP_ID));
							double version = GetterUtil.getDouble(
								document.get(Field.VERSION));

							article =
								JournalArticleLocalServiceUtil.fetchArticle(
									groupId, articleId, version);
						}

						results.add(article);
					}
					else if (className.equals(JournalFolder.class.getName())) {
						folder = JournalFolderLocalServiceUtil.getFolder(
							classPK);

						results.add(folder);
					}
				}

				articleSearchContainer.setResults(results);
			}
			else {
				int total = JournalArticleServiceUtil.searchCount(
					themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(),
					folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
					getKeywords(), -1.0, getDDMStructureKey(),
					getDDMTemplateKey(), null, null, getStatus(), null);

				articleSearchContainer.setTotal(total);

				List results = JournalArticleServiceUtil.search(
					themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(),
					folderIds, JournalArticleConstants.CLASSNAME_ID_DEFAULT,
					getKeywords(), -1.0, getDDMStructureKey(),
					getDDMTemplateKey(), null, null, getStatus(), null,
					articleSearchContainer.getStart(),
					articleSearchContainer.getEnd(),
					articleSearchContainer.getOrderByComparator());

				articleSearchContainer.setResults(results);
			}
		}
		else {
			int total = JournalFolderServiceUtil.getFoldersAndArticlesCount(
				themeDisplay.getScopeGroupId(), 0, getFolderId(), getStatus());

			articleSearchContainer.setTotal(total);

			OrderByComparator<Object> folderOrderByComparator = null;

			boolean orderByAsc = false;

			if (Objects.equals(getOrderByType(), "asc")) {
				orderByAsc = true;
			}

			if (Objects.equals(getOrderByCol(), "display-date")) {
				folderOrderByComparator =
					new FolderArticleDisplayDateComparator(orderByAsc);
			}
			else if (Objects.equals(getOrderByCol(), "id")) {
				folderOrderByComparator = new FolderArticleArticleIdComparator(
					orderByAsc);
			}
			else if (Objects.equals(getOrderByCol(), "modified-date")) {
				folderOrderByComparator =
					new FolderArticleModifiedDateComparator(orderByAsc);
			}

			List results = JournalFolderServiceUtil.getFoldersAndArticles(
				themeDisplay.getScopeGroupId(), 0, getFolderId(), getStatus(),
				articleSearchContainer.getStart(),
				articleSearchContainer.getEnd(), folderOrderByComparator);

			articleSearchContainer.setResults(results);
		}

		return articleSearchContainer;
	}
	
	/**
	 * Encode the name before using with Liferay indexing engine.
	 * 
	 * @param ddmStructureId The DDMStructure ID.
	 * @param fieldName      The DDMStucture field name.
	 * @param locale         The DDMStructure locale.
	 * @param indexType      The index type (none|text|key).
	 * 
	 * @return The encoded field name.
	 */
	private String encodeName(long ddmStructureId, String fieldName, Locale locale, String indexType) {
		StringBundler sb = new StringBundler(8);

		sb.append(DDMIndexer.DDM_FIELD_PREFIX);

		if (Validator.isNotNull(indexType)) {
			sb.append(indexType);
			sb.append(DDMIndexer.DDM_FIELD_SEPARATOR);
		}

		sb.append(ddmStructureId);
		sb.append(DDMIndexer.DDM_FIELD_SEPARATOR);
		sb.append(fieldName);

		if (locale != null) {
			sb.append(StringPool.UNDERLINE);
			sb.append(LocaleUtil.toLanguageId(locale));
		}

		return sb.toString();
	}
	
	private BooleanClause<Query> buildDateRangeClause(String param, String languageId, SearchContext sctx) {
		// Check if the param is submitted.
		String fromDateStr = ParamUtil.getString(_request, param + "_from", StringPool.BLANK);
		String toDateStr = ParamUtil.getString(_request, param + "_to", StringPool.BLANK);
		
		if (fromDateStr.length() != 0 || toDateStr.length() != 0) {
			BooleanQuery dateQuery = BooleanQueryFactoryUtil.create(sctx);
			LocalDateTime fromDateVal = null, toDateVal = null;

			// Get the "from date" parameter from request.
			fromDateStr = fromDateStr.trim();
			if (fromDateStr.length() != 0) {
				try {
					if (languageId.equals("vi_VN")) {
						fromDateVal = LocalDate.parse(fromDateStr, viVnDateFormat).atStartOfDay();
					} else {
						fromDateVal = LocalDate.parse(fromDateStr, enUsDateFormat).atStartOfDay();
					}
				} catch (DateTimeParseException e) {
					e.printStackTrace();
				}
			}

			// Get the "to date" parameter from request.
			toDateStr = toDateStr.trim();
			if (toDateStr.length() != 0) {
				try {
					if (languageId.equals("vi_VN")) {
						toDateVal = LocalDate.parse(toDateStr, viVnDateFormat).atStartOfDay();
					} else {
						toDateVal = LocalDate.parse(toDateStr, enUsDateFormat).atStartOfDay();
					}
				} catch (DateTimeParseException e) {
					e.printStackTrace();
				}
			}

			// If both "from date" and "to date" are null -> break.
			// Otherwise, set default value for the null one.
			if (fromDateVal != null || toDateVal != null) {
				if (fromDateVal == null) {
					fromDateVal = LocalDateTime.of(1970, 1, 1, 0, 0, 0);
				}
				if (toDateVal == null) {
					toDateVal = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS).plusDays(1);
				} else {
					toDateVal = toDateVal.plusDays(1);
				}
			}

			// Add the date range search term.
			dateQuery.addRangeTerm(param, databaseDateFormat.format(fromDateVal), databaseDateFormat.format(toDateVal));
			
			return BooleanClauseFactoryUtil.create(sctx, dateQuery, BooleanClauseOccur.MUST.getName());
		}
		
		return null;
	}

	public int getStatus() {
		if (_status != null) {
			return _status;
		}

		ThemeDisplay themeDisplay = (ThemeDisplay)_request.getAttribute(
			WebKeys.THEME_DISPLAY);

		int defaultStatus = WorkflowConstants.STATUS_ANY;

		PermissionChecker permissionChecker =
			themeDisplay.getPermissionChecker();

		if (permissionChecker.isContentReviewer(
				themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId()) ||
			isNavigationMine()) {

			defaultStatus = WorkflowConstants.STATUS_ANY;
		}

		_status = ParamUtil.getInteger(_request, "status", defaultStatus);

		return _status;
	}

	public String getTabs1() {
		if (_tabs1 != null) {
			return _tabs1;
		}

		_tabs1 = ParamUtil.getString(_request, "tabs1");

		return _tabs1;
	}
	
	/**
	 * Check if the request is made from SWT Embedded Journal Web portlet.
	 * 
	 * @return True if the request is made from SWT Embedded Journal Web portlet.
	 * 
	 * @author Dung Nguyen (dung.nguyen@smartworld.com.vn)
	 */
	public boolean isEmbedded() {
		if (_embedded != null) {
			return _embedded;
		}

		_embedded = ParamUtil.getBoolean(_request, "embedded", true);

		return _embedded;
	}

	public int getTotal() throws PortalException {
		SearchContainer articleSearch = getSearchContainer(false);

		return articleSearch.getTotal();
	}

	public int getVersionsTotal() throws PortalException {
		SearchContainer articleSearch = getSearchContainer(true);

		return articleSearch.getTotal();
	}

	public boolean hasCommentsResults() throws PortalException {
		if (getCommentsTotal() > 0) {
			return true;
		}

		return false;
	}

	public boolean hasResults() throws PortalException {
		if (getTotal() > 0) {
			return true;
		}

		return false;
	}

	public boolean hasVersionsResults() throws PortalException {
		if (getVersionsTotal() > 0) {
			return true;
		}

		return false;
	}

	public boolean isDisabledManagementBar() throws PortalException {
		if (hasResults()) {
			return false;
		}

		if (isSearch()) {
			return false;
		}

		if (!isNavigationHome() ||
			(getStatus() != WorkflowConstants.STATUS_ANY)) {

			return false;
		}

		return true;
	}

	public boolean isNavigationHome() {
		if (Objects.equals(getNavigation(), "all")) {
			return true;
		}

		return false;
	}

	public boolean isNavigationMine() {
		if (Objects.equals(getNavigation(), "mine")) {
			return true;
		}

		return false;
	}

	public boolean isNavigationRecent() {
		if (Objects.equals(getNavigation(), "recent")) {
			return true;
		}

		return false;
	}

	public boolean isNavigationStructure() {
		if (Objects.equals(getNavigation(), "structure")) {
			return true;
		}

		return false;
	}

	public boolean isSearch() {
		if (Validator.isNotNull(getKeywords())) {
			return true;
		}

		return false;
	}

	public boolean isShowBreadcrumb() throws PortalException {
		if (isNavigationStructure()) {
			return false;
		}

		if (isNavigationRecent()) {
			return false;
		}

		if (isNavigationMine()) {
			return false;
		}

		if (isSearch()) {
			return false;
		}

		if (!hasResults()) {
			return false;
		}

		return true;
	}

	public boolean isShowEditActions() {
		if (_showEditActions != null) {
			return _showEditActions;
		}

		_showEditActions = ParamUtil.getBoolean(
			_request, "showEditActions", true);

		return _showEditActions;
	}

	public boolean isShowInfoPanel() {
		if (Validator.isNotNull(getDDMStructureKey())) {
			return false;
		}

		if (isNavigationMine()) {
			return false;
		}

		if (isNavigationRecent()) {
			return false;
		}

		if (isSearch()) {
			return false;
		}

		return true;
	}

	public boolean isShowSearch() throws PortalException {
		if (hasResults()) {
			return true;
		}

		if (isSearch()) {
			return true;
		}

		return false;
	}

	protected SearchContext buildSearchContext(
		long companyId, long groupId, List<Long> folderIds, long classNameId,
		String ddmStructureKey, String ddmTemplateKey, String keywords,
		LinkedHashMap<String, Object> params, int start, int end, Sort sort,
		boolean showVersions, long loggedInUser) {

		String articleId = null;
		String title = null;
		String description = null;
		String content = null;
		boolean andOperator = false;

		if (Validator.isNotNull(keywords)) {
			articleId = keywords;
			title = keywords;
			description = keywords;
			content = keywords;
		}
		else {
			andOperator = true;
		}

		if (params != null) {
			params.put("keywords", keywords);
		}

		SearchContext searchContext = new SearchContext();

		searchContext.setAndSearch(andOperator);

		Map<String, Serializable> attributes = new HashMap<>();

		attributes.put(Field.ARTICLE_ID, articleId);
		attributes.put(Field.CLASS_NAME_ID, classNameId);
		attributes.put(Field.CONTENT, content);
		attributes.put(Field.DESCRIPTION, description);
		attributes.put(Field.STATUS, getStatus());
		attributes.put(Field.TITLE, title);
		attributes.put("ddmStructureKey", ddmStructureKey);
		attributes.put("ddmTemplateKey", ddmTemplateKey);
		attributes.put("params", params);
		
		if(loggedInUser != 0) {
			// Bug #4104 Start nhut.dang, If Workflow status = STATUS_DRAFT is user create and Administrator can access
			boolean checkAdmin = false;
			List<Role> lst_role = RoleLocalServiceUtil.getUserRoles(loggedInUser);
			for(Role object :lst_role ) {
				if(object.getName().equals("Administrator")|| object.getName().contains("_QuanTri")) {
					checkAdmin = true;
				}
			}
			try {
				BooleanQuery booleanStatusDraft = BooleanQueryFactoryUtil.create(searchContext);
				BooleanQuery booleanStatusInTrash = BooleanQueryFactoryUtil.create(searchContext);
				BooleanQuery booleanStatusUserId = BooleanQueryFactoryUtil.create(searchContext);
				BooleanQuery innerBooleanQuery = BooleanQueryFactoryUtil.create(searchContext);
				
				booleanStatusDraft.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_DRAFT);
				booleanStatusInTrash.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_IN_TRASH);
				if(!checkAdmin) {
				booleanStatusUserId.addRequiredTerm(Field.USER_ID, loggedInUser);
				}
				// Bug #4104 End nhut.dang, If Workflow status = STATUS_DRAFT is user create and Administrator can access
				innerBooleanQuery.add(booleanStatusDraft, BooleanClauseOccur.MUST);
				innerBooleanQuery.add(booleanStatusUserId, BooleanClauseOccur.MUST_NOT);
				
				List<BooleanClause> searchClauses = new ArrayList<>();
				searchClauses.add(BooleanClauseFactoryUtil.create(searchContext, innerBooleanQuery, 
						BooleanClauseOccur.MUST_NOT.getName()));
				searchClauses.add(BooleanClauseFactoryUtil.create(searchContext, booleanStatusInTrash, 
						BooleanClauseOccur.MUST_NOT.getName()));
				searchContext.setBooleanClauses(searchClauses.toArray(new BooleanClause[searchClauses.size()]));
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		

		searchContext.setAttributes(attributes);

		searchContext.setCompanyId(companyId);
		searchContext.setEnd(end);
		searchContext.setFolderIds(folderIds);
		searchContext.setGroupIds(new long[] {groupId});
		searchContext.setIncludeDiscussions(
			GetterUtil.getBoolean(params.get("includeDiscussions"), true));

		if (params != null) {
			keywords = (String)params.remove("keywords");

			if (Validator.isNotNull(keywords)) {
				searchContext.setKeywords(keywords);
			}
		}

		searchContext.setAttribute("head", !showVersions);
		searchContext.setAttribute("latest", !showVersions);
		searchContext.setAttribute("params", params);

		if (!showVersions) {
			searchContext.setAttribute("showNonindexable", Boolean.TRUE);
		}

		searchContext.setEnd(end);
		searchContext.setFolderIds(folderIds);
		searchContext.setStart(start);

		QueryConfig queryConfig = new QueryConfig();

		queryConfig.setHighlightEnabled(false);
		queryConfig.setScoreEnabled(false);

		searchContext.setQueryConfig(queryConfig);

		if (sort != null) {
			searchContext.setSorts(sort);
		}

		searchContext.setStart(start);

		return searchContext;
	}

	protected ManagementBarFilterItem getManagementBarFilterItem(int status)
		throws PortalException, PortletException {

		boolean active = false;

		if (status == getStatus()) {
			active = true;
		}

		PortletURL portletURL = PortletURLUtil.clone(
			getPortletURL(), _liferayPortletResponse);

		portletURL.setParameter("status", String.valueOf(status));

		return new ManagementBarFilterItem(
			active, WorkflowConstants.getStatusLabel(status),
			portletURL.toString());
	}
	
	private static final Log _log = LogFactoryUtil.getLog(
		JournalDisplayContext.class);

	private JournalArticle _article;
	private JournalArticleDisplay _articleDisplay;
	private DDMFormValues _ddmFormValues;
	private String _ddmStructureKey;
	private String _ddmStructureName;
	private List<DDMStructure> _ddmStructures;
	private String _ddmTemplateKey;
	private String _displayStyle;
	private String[] _displayViews;
	private JournalFolder _folder;
	private Long _folderId;
	private final JournalWebConfiguration _journalWebConfiguration;
	private String _keywords;
	private final LiferayPortletRequest _liferayPortletRequest;
	private final LiferayPortletResponse _liferayPortletResponse;
	private String _navigation;
	private String _orderByCol;
	private String _orderByType;
	private final PortalPreferences _portalPreferences;
	private final PortletPreferences _portletPreferences;
	private final HttpServletRequest _request;
	private Integer _restrictionType;
	private Boolean _showEditActions;
	private Integer _status;
	private String _tabs1;
	
	/**
	 * If true, the request is made from SWT Embedded Journal Web portlet.
	 * 
	 * @author Dung Nguyen (dung.nguyen@smartworld.com.vn)
	 */
	private Boolean _embedded;
	
}