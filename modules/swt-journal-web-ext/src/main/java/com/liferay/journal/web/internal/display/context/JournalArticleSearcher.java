package com.liferay.journal.web.internal.display.context;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.web.internal.search.JournalSearcher;
import com.liferay.portal.kernel.search.Indexer;

/**
 * The searcher utility class for Journal Article. The source code logic is
 * copied from {@link JournalSearcher}. I just removed JournalFolder from
 * CLASS_NAMES array.
 * 
 * @author Dung Nguyen (dung.nguyen@smartworld.com.vn)
 */
public class JournalArticleSearcher extends JournalSearcher {

	public static final String[] CLASS_NAMES = { JournalArticle.class.getName() };

	public static Indexer<?> getInstance() {
		return new JournalArticleSearcher();
	}

	public JournalArticleSearcher() {
		super();
	}

	@Override
	public String[] getSearchClassNames() {
		return CLASS_NAMES;
	}

}
