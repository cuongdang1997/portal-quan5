package com.liferay.journal.web.util;

import java.awt.Point;

import net.coobird.thumbnailator.geometry.Position;

public enum WatermarkPosition implements Position {

	BOTTOM_LEFT() {
		public Point calculate(int enclosingWidth, int enclosingHeight, int width, int height, int insetLeft,
				int insetRight, int insetTop, int insetBottom) {
			int x = insetLeft + enclosingWidth - (int) (enclosingWidth * 0.98);
			int y = enclosingHeight - (int) (enclosingHeight * 0.05) - insetBottom;
			return new Point(x, y);
		}
	}

}
