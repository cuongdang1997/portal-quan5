/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.journal.web.util;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.journal.util.comparator.ArticleCreateDateComparator;
import com.liferay.journal.util.comparator.ArticleDisplayDateComparator;
import com.liferay.journal.util.comparator.ArticleIDComparator;
import com.liferay.journal.util.comparator.ArticleModifiedDateComparator;
import com.liferay.journal.util.comparator.ArticleReviewDateComparator;
import com.liferay.journal.util.comparator.ArticleTitleComparator;
import com.liferay.journal.util.comparator.ArticleVersionComparator;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletURL;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Brian Wing Shun Chan
 * @author Raymond Augé
 * @author Wesley Gong
 * @author Angelo Jefferson
 * @author Hugo Huijser
 * @author Eduardo Garcia
 */
public class JournalPortletUtil {

	public static void addPortletBreadcrumbEntries(
			JournalArticle article, HttpServletRequest request,
			PortletURL portletURL)
		throws Exception {

		JournalFolder folder = article.getFolder();

		if (folder.getFolderId() !=
				JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {

			addPortletBreadcrumbEntries(folder, request, portletURL);
		}

		JournalArticle unescapedArticle = article.toUnescapedModel();

		portletURL.setParameter("mvcPath", "/edit_article.jsp");
		portletURL.setParameter(
			"groupId", String.valueOf(article.getGroupId()));
		portletURL.setParameter(
			"articleId", String.valueOf(article.getArticleId()));

		PortalUtil.addPortletBreadcrumbEntry(
			request, unescapedArticle.getTitle(), portletURL.toString());
	}

	public static void addPortletBreadcrumbEntries(
			JournalFolder folder, HttpServletRequest request,
			PortletURL portletURL)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
			WebKeys.THEME_DISPLAY);

		String mvcPath = ParamUtil.getString(request, "mvcPath");

		portletURL.setParameter(
			"folderId",
			String.valueOf(JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID));

		if (mvcPath.equals("/select_folder.jsp")) {
			portletURL.setWindowState(LiferayWindowState.POP_UP);

			PortalUtil.addPortletBreadcrumbEntry(
				request, themeDisplay.translate("home"), portletURL.toString());
		}
		else {
			Map<String, Object> data = new HashMap<>();

			data.put("direction-right", Boolean.TRUE.toString());
			data.put(
				"folder-id", JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID);

			PortalUtil.addPortletBreadcrumbEntry(
				request, themeDisplay.translate("home"), portletURL.toString(),
				data);
		}

		if (folder == null) {
			return;
		}

		List<JournalFolder> ancestorFolders = folder.getAncestors();

		Collections.reverse(ancestorFolders);

		for (JournalFolder ancestorFolder : ancestorFolders) {
			portletURL.setParameter(
				"folderId", String.valueOf(ancestorFolder.getFolderId()));

			Map<String, Object> data = new HashMap<>();

			data.put("direction-right", Boolean.TRUE.toString());
			data.put("folder-id", ancestorFolder.getFolderId());

			PortalUtil.addPortletBreadcrumbEntry(
				request, ancestorFolder.getName(), portletURL.toString(), data);
		}

		portletURL.setParameter(
			"folderId", String.valueOf(folder.getFolderId()));

		if (folder.getFolderId() !=
				JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {

			JournalFolder unescapedFolder = folder.toUnescapedModel();

			Map<String, Object> data = new HashMap<>();

			data.put("direction-right", Boolean.TRUE.toString());
			data.put("folder-id", folder.getFolderId());

			PortalUtil.addPortletBreadcrumbEntry(
				request, unescapedFolder.getName(), portletURL.toString(),
				data);
		}
	}

	public static void addPortletBreadcrumbEntries(
			long folderId, HttpServletRequest request, PortletURL portletURL)
		throws Exception {

		if (folderId == JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {
			return;
		}

		JournalFolder folder = JournalFolderLocalServiceUtil.getFolder(
			folderId);

		addPortletBreadcrumbEntries(folder, request, portletURL);
	}

	public static OrderByComparator<JournalArticle> getArticleOrderByComparator(
		String orderByCol, String orderByType) {

		boolean orderByAsc = false;

		if (orderByType.equals("asc")) {
			orderByAsc = true;
		}

		OrderByComparator<JournalArticle> orderByComparator = null;

		if (orderByCol.equals("create-date")) {
			orderByComparator = new ArticleCreateDateComparator(orderByAsc);
		}
		else if (orderByCol.equals("display-date")) {
			orderByComparator = new ArticleDisplayDateComparator(orderByAsc);
		}
		else if (orderByCol.equals("id")) {
			orderByComparator = new ArticleIDComparator(orderByAsc);
		}
		else if (orderByCol.equals("modified-date")) {
			orderByComparator = new ArticleModifiedDateComparator(orderByAsc);
		}
		else if (orderByCol.equals("review-date")) {
			orderByComparator = new ArticleReviewDateComparator(orderByAsc);
		}
		else if (orderByCol.equals("title")) {
			orderByComparator = new ArticleTitleComparator(orderByAsc);
		}
		else if (orderByCol.equals("version")) {
			orderByComparator = new ArticleVersionComparator(orderByAsc);
		}

		return orderByComparator;
	}
	public static String setContent(List<DDMFormField> listFormFile, Map<String, String> listParameter) {

		String contentArticle = "";
		String type = "";
		for (DDMFormField object : listFormFile) {
			if (object.getType().equals("textarea")) {
				type = "text_box";
			}
			if (object.getType().equals("ddm-documentlibrary")) {
				type = "document_library";
			}
			if (object.getType().equals("select")) {
				type = "list";
			}
			if (object.getType().equals("radio")) {
				type = "radio";
			}
			if (object.getType().equals("text")) {
				type = "text";
			}
			for (Map.Entry<String, String> s : listParameter.entrySet()) {
				if (s.getKey().equals(object.getName())) {
					contentArticle += "	<dynamic-element name=\"" + object.getName() + "\" type=\"" + type
							+ "\" index-type=\"" + object.getIndexType() + "\" instance-id=\"" + object.getName()
							+ "\">\r\n" + "		<dynamic-content language-id=\"vi_VN\"><![CDATA[" + s.getValue()
							+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n";
					break;
				}
				
			}
		}
		String xml = "<?xml version=\"1.0\"?>\r\n" + "\r\n"
				+ "<root available-locales=\"vi_VN\" default-locale=\"vi_VN\">\r\n" + contentArticle + "</root>";
		return xml;

	}
	public static void createJournalArticle(ThemeDisplay themeDisplay, ResourceRequest resourceRequest, String structure,
			long structureId, String titleName, String contentName, String contentLast, long categoryId,long folderId)
			throws PortalException {
		double version = 1;
		String articleId = String.valueOf(CounterLocalServiceUtil.increment());

		// @param titleMap
		Map<Locale, String> titleMap = new HashMap<Locale, String>();
		titleMap.put(themeDisplay.getLocale(), titleName);

		// @param descriptionMap
		Map<Locale, String> contentMap = new HashMap<Locale, String>();
		contentMap.put(themeDisplay.getLocale(), contentName);

		// @param ddmTemplateKey
		DDMStructure structureObjetc = DDMStructureLocalServiceUtil.getStructure(structureId);
		List<DDMTemplate> listTemplate = structureObjetc.getTemplates();
		String ddmTemplateKey = "";

		if (listTemplate.size() != 0) {
			if (listTemplate.get(0).getType().equals("display")) {
				ddmTemplateKey = listTemplate.get(0).getTemplateKey();
			}
		}
		// @param layoutUuid
		String layoutUuid = "";
		List<Layout> listLayoutParrent = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), false,
				themeDisplay.getLayout().getLayoutId());
		if (listLayoutParrent.size() != 0) {
			for (Layout object : listLayoutParrent) {
				if (object.isContentDisplayPage()) {
					layoutUuid = object.getUuid();
				}
			}
		}
		
		// Map image
		Map<String, byte[]> byteImage = null;
		ServiceContext serviceContextArticle = ServiceContextFactory.getInstance(JournalArticle.class.getName(),
				resourceRequest);
		serviceContextArticle.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
		File file = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		try {

			JournalArticle objectArticle = JournalArticleLocalServiceUtil.addArticle(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), GetterUtil.getLong(folderId), GetterUtil.getLong(0),
					structureObjetc.getPrimaryKey(), articleId, true, version, titleMap, contentMap, contentLast,
					structure, ddmTemplateKey, layoutUuid, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),
					cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), 0, 0, 0, 0, 0,
					true, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR),
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false, true, false, "", file, byteImage,
					"", serviceContextArticle);
			AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(),
					objectArticle.getResourcePrimKey());
			AssetEntryLocalServiceUtil.addAssetCategoryAssetEntry(GetterUtil.getLong(categoryId), entry.getEntryId());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}