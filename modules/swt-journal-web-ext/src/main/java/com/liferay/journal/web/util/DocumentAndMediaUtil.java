package com.liferay.journal.web.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.Validator;

import net.coobird.thumbnailator.filters.Caption;

public class DocumentAndMediaUtil {

	private static final Color WATERMARK_COLOR = new Color(255, 255, 255);
	private static final float WATERMARK_COLOR_ALPHA = 0.4F;
	private static final Font WATERMARK_FONT = new Font("Arial", Font.BOLD, 50);
	private static final int WATERMARK_INSETS = 0;

	public static String createArticleDLFiles(String articleContent, ThemeDisplay themeDisplay, String folderName)
			throws Exception {
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setCreateDate(new Date());
		serviceContext.setModifiedDate(new Date());

		String filePath = StringPool.BLANK;
		Document articleDoc = null;

		if (Validator.isNotNull(articleContent)) {
			articleDoc = Jsoup.parse(articleContent);
			Elements imageElements = articleDoc.select("img[src]");

			if (imageElements.size() != 0) {
				for (Element e : imageElements) {
					String baseUrl = e.attr("src");
					FileEntry fileEntry = getFileByURL(baseUrl, themeDisplay, folderName);

					if (fileEntry == null) {
						continue;
					}

					filePath = "/documents/" + fileEntry.getGroupId() + StringPool.SLASH + fileEntry.getFolderId()
							+ StringPool.SLASH + fileEntry.getFileName() + StringPool.SLASH + fileEntry.getUuid();
					e.attr("src", filePath);
				}
			}
		}

		return articleDoc.toString();
	}

	public static FileEntry getFileByURL(String linkFile, ThemeDisplay themeDisplay, String nameFolder) {
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setDeriveDefaultPermissions(true);

		FileEntry fileEntry = null;
		long folderId = 0L;

		try {
			// Download the file from URL.
			String fileURL = linkFile.toString();
			URL url = new URL(fileURL);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			int responseCode = httpConn.getResponseCode();

			if (responseCode / 100 != 2) {
				return null;
			}

			// Create D&L folder structrure.
			try {
				folderId = createDLFolderStructure(themeDisplay, serviceContext, nameFolder);
			} catch (PortalException e) {
				e.printStackTrace();
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			String fileName = StringPool.BLANK;
			String disposition = httpConn.getHeaderField("Content-Disposition");

			if (disposition != null) {
				int index = disposition.indexOf("filename=");
				if (index > 0) {
					fileName = disposition.substring(index + 10, disposition.length() - 1);
				}
			} else {
				fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());
			}

			InputStream inputStream = httpConn.getInputStream();
			int bytesRead = -1;
			byte[] buffer = new byte[4096];

			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}

			inputStream.close();
			baos.flush();

			String salt = String.valueOf(Calendar.getInstance().getTimeInMillis());
			fileEntry = DLAppServiceUtil.addFileEntry(themeDisplay.getScopeGroupId(), folderId, fileName,
					MimeTypesUtil.getContentType(fileName), salt + fileName, StringPool.BLANK, StringPool.BLANK,
					baos.toByteArray(), serviceContext);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}

		return fileEntry;
	}

	public static long createDLFolderStructure(ThemeDisplay themeDisplay, ServiceContext serviceContext,
			String parentFolderName) throws PortalException {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
		
		String nameFolderMonth ="";
		String nameFolderDay ="";
		
		if(month<10) {
			nameFolderMonth = 	"0" + String.valueOf(month);
		}else {
			nameFolderMonth = String.valueOf(month);
		}
		
		if(day<10) {
			nameFolderDay = "0" + String.valueOf(day);
		}else {
			nameFolderDay = String.valueOf(day);
		}
		long parentFolderId = createFolder(themeDisplay, serviceContext, parentFolderName);
		DLFolder dayFolder = null;

		if (!isFolderExist(themeDisplay, parentFolderId, String.valueOf(year))) {
			DLFolder yearFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false, parentFolderId,
					String.valueOf(year), StringPool.BLANK, false, serviceContext);
			DLFolder monthFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false, yearFolder.getFolderId(),
					String.valueOf(nameFolderMonth), StringPool.BLANK, false, serviceContext);
			dayFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
					themeDisplay.getScopeGroupId(), false, monthFolder.getFolderId(), String.valueOf(nameFolderDay),
					StringPool.BLANK, false, serviceContext);
		} else {
			DLFolder yearFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), parentFolderId,
					String.valueOf(year));

			if (!isFolderExist(themeDisplay, yearFolder.getFolderId(), String.valueOf(nameFolderMonth))) {
				DLFolder monthFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
						themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false, yearFolder.getFolderId(),
						String.valueOf(nameFolderMonth), StringPool.BLANK, false, serviceContext);
				dayFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						themeDisplay.getScopeGroupId(), false, monthFolder.getFolderId(), String.valueOf(nameFolderDay),
						StringPool.BLANK, false, serviceContext);
			} else {
				DLFolder monthFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
						yearFolder.getFolderId(), String.valueOf(nameFolderMonth));

				if (!isFolderExist(themeDisplay, monthFolder.getFolderId(), String.valueOf(nameFolderDay))) {
					dayFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
							themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false,
							monthFolder.getFolderId(), String.valueOf(nameFolderDay), StringPool.BLANK, false, serviceContext);
				} else {
					dayFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
							monthFolder.getFolderId(), String.valueOf(nameFolderDay));
				}
			}
		}

		return dayFolder.getFolderId();
	}

	public static boolean isFolderExist(ThemeDisplay themeDisplay, long parentFolderId, String folerName) {
		try {
			DLFolder folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), parentFolderId,
					folerName);
			if (folder != null) {
				return true;
			}
		} catch (PortalException e) {
			// Do nothing.
		}

		return false;
	}

	private static long createFolder(ThemeDisplay themeDisplay, ServiceContext serviceContext, String folderName)
			throws PortalException {
		DLFolder folder = null;

		try {
			folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0L, folderName);
		} catch (PortalException e) {
			folder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
					themeDisplay.getScopeGroupId(), false, 0L, folderName, StringPool.BLANK, false, serviceContext);
		}

		if (folder == null) {
			return 0L;
		}

		return folder.getFolderId();
	}

	public static FileEntry addFileEntry(ThemeDisplay themeDisplay, long folderId, String fileName, File file,
			ServiceContext serviceContext) throws PortalException {
		FileEntry fileEntry = null;
		String salt = String.valueOf(Calendar.getInstance().getTimeInMillis());
		fileEntry = DLAppServiceUtil.addFileEntry(themeDisplay.getScopeGroupId(), folderId, fileName,
				MimeTypesUtil.getContentType(fileName), salt + fileName, StringPool.BLANK, StringPool.BLANK, file,
				serviceContext);
		return fileEntry;
	}
	
	/**
	 * Add watermark to an image using Thumbnailator library.
	 * 
	 * @param source    The source image file.
	 * @param imageType The source image type (png, jpg...).
	 * @param text      The watermark text.
	 */
	public static void addWatermark(File source, String imageType, String text) throws IOException {
		BufferedImage originalImage = ImageIO.read(source);
		text = text.trim().toUpperCase();

		// Apply watermark to the image.
		Caption filter = new Caption(text, WATERMARK_FONT, WATERMARK_COLOR, WATERMARK_COLOR_ALPHA,
				WatermarkPosition.BOTTOM_LEFT, WATERMARK_INSETS);
		BufferedImage watermarkImage = filter.apply(originalImage);

		// Save the watermarked image to source file.
		ImageIO.write(watermarkImage, imageType, source);
	}

}