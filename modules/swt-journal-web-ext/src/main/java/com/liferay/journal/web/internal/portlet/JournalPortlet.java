/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.journal.web.internal.portlet;

import com.liferay.asset.kernel.exception.AssetCategoryException;
import com.liferay.asset.kernel.exception.AssetTagException;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.document.library.kernel.exception.DuplicateFileEntryException;
import com.liferay.document.library.kernel.exception.FileSizeException;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.dynamic.data.mapping.exception.NoSuchStructureException;
import com.liferay.dynamic.data.mapping.exception.NoSuchTemplateException;
import com.liferay.dynamic.data.mapping.exception.StorageFieldRequiredException;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.service.permission.DDMStructurePermission;
import com.liferay.dynamic.data.mapping.storage.Field;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMUtil;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.service.ExpandoColumnLocalServiceUtil;
import com.liferay.item.selector.ItemSelector;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.journal.constants.JournalWebKeys;
import com.liferay.journal.exception.ArticleContentException;
import com.liferay.journal.exception.ArticleContentSizeException;
import com.liferay.journal.exception.ArticleDisplayDateException;
import com.liferay.journal.exception.ArticleExpirationDateException;
import com.liferay.journal.exception.ArticleIdException;
import com.liferay.journal.exception.ArticleSmallImageNameException;
import com.liferay.journal.exception.ArticleSmallImageSizeException;
import com.liferay.journal.exception.ArticleTitleException;
import com.liferay.journal.exception.ArticleVersionException;
import com.liferay.journal.exception.DuplicateArticleIdException;
import com.liferay.journal.exception.DuplicateFeedIdException;
import com.liferay.journal.exception.DuplicateFolderNameException;
import com.liferay.journal.exception.FeedContentFieldException;
import com.liferay.journal.exception.FeedIdException;
import com.liferay.journal.exception.FeedNameException;
import com.liferay.journal.exception.FeedTargetLayoutFriendlyUrlException;
import com.liferay.journal.exception.FeedTargetPortletIdException;
import com.liferay.journal.exception.FolderNameException;
import com.liferay.journal.exception.InvalidDDMStructureException;
import com.liferay.journal.exception.NoSuchArticleException;
import com.liferay.journal.exception.NoSuchFeedException;
import com.liferay.journal.exception.NoSuchFolderException;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFeed;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.journal.service.JournalContentSearchLocalService;
import com.liferay.journal.service.JournalFeedService;
import com.liferay.journal.service.JournalFolderService;
import com.liferay.journal.util.JournalContent;
import com.liferay.journal.util.JournalConverter;
import com.liferay.journal.util.impl.JournalUtil;
import com.liferay.journal.web.asset.JournalArticleAssetRenderer;
import com.liferay.journal.web.configuration.JournalWebConfiguration;
import com.liferay.journal.web.internal.portlet.action.ActionUtil;
import com.liferay.journal.web.util.DocumentAndMediaUtil;
import com.liferay.journal.web.util.JournalPortletUtil;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.diff.CompareVersionsException;
import com.liferay.portal.kernel.exception.LocaleException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Release;
import com.liferay.portal.kernel.model.TrashedModel;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletProvider.Action;
import com.liferay.portal.kernel.portlet.PortletProviderUtil;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.MultiSessionMessages;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.LiferayFileItemException;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.RSSUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.util.PropsValues;
import com.liferay.trash.kernel.service.TrashEntryService;
import com.liferay.trash.kernel.util.TrashUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletContext;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Eduardo Garcia
 */
@Component(
	configurationPid = "com.liferay.journal.web.configuration.JournalWebConfiguration",
	configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.css-class-wrapper=portlet-journal",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.icon=/icons/journal.png",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.preferences-owned-by-group=true",
		"com.liferay.portlet.preferences-unique-per-layout=false",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.scopeable=true",
		"com.liferay.portlet.use-default-template=true",
		"com.liferay.portlet.webdav-storage-token=journal",
		"javax.portlet.display-name=Web Content",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.mvc-action-command-package-prefix=com.liferay.journal.web.portlet.action",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + JournalPortletKeys.JOURNAL,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html"
	},
	service = {JournalPortlet.class, Portlet.class}
)
public class JournalPortlet extends MVCPortlet {

	public static final String VERSION_SEPARATOR = "_version_";

	public void addArticle(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		updateArticle(actionRequest, actionResponse);
	}

	public void addFeed(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		updateFeed(actionRequest, actionResponse);
	}

	public void addFolder(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		updateFolder(actionRequest, actionResponse);
	}

	public void deleteArticle(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteArticles(actionRequest, actionResponse, false);
	}

	public void deleteArticles(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteArticles(actionRequest, actionResponse, false);
	}

	public void deleteEntries(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteEntries(actionRequest, actionResponse, false);
	}

	public void deleteFeeds(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		long[] deleteFeedIds = null;

		long deleteFeedId = ParamUtil.getLong(actionRequest, "deleteFeedId");

		if (deleteFeedId > 0) {
			deleteFeedIds = new long[] {deleteFeedId};
		}
		else {
			deleteFeedIds = ParamUtil.getLongValues(actionRequest, "rowIds");
		}

		for (long curDeleteFeedId : deleteFeedIds) {
			_journalFeedService.deleteFeed(
				themeDisplay.getScopeGroupId(),
				String.valueOf(curDeleteFeedId));
		}
	}

	public void deleteFolder(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteFolder(actionRequest, actionResponse, false);
	}

	public void expireArticles(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		String articleId = ParamUtil.getString(actionRequest, "articleId");

		if (Validator.isNotNull(articleId)) {
			ActionUtil.expireArticle(actionRequest, articleId);
		}
		else {
			String[] expireArticleIds = ParamUtil.getParameterValues(
				actionRequest, "rowIds");

			for (String expireArticleId : expireArticleIds) {
				ActionUtil.expireArticle(
					actionRequest, HtmlUtil.unescape(expireArticleId));
			}
		}

		sendEditArticleRedirect(actionRequest, actionResponse);
	}

	public void expireEntries(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		long[] expireFolderIds = ParamUtil.getLongValues(
			actionRequest, "rowIdsJournalFolder");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			JournalArticle.class.getName(), actionRequest);

		for (long expireFolderId : expireFolderIds) {
			ActionUtil.expireFolder(
				themeDisplay.getScopeGroupId(), expireFolderId, serviceContext);
		}

		String[] expireArticleIds = ParamUtil.getStringValues(
			actionRequest, "rowIdsJournalArticle");

		for (String expireArticleId : expireArticleIds) {
			ActionUtil.expireArticle(
				actionRequest, HtmlUtil.unescape(expireArticleId));
		}

		sendEditEntryRedirect(actionRequest, actionResponse);
	}

	public void moveEntries(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		long newFolderId = ParamUtil.getLong(actionRequest, "newFolderId");

		long[] folderIds = ParamUtil.getLongValues(
			actionRequest, "rowIdsJournalFolder");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			JournalArticle.class.getName(), actionRequest);

		for (long folderId : folderIds) {
			_journalFolderService.moveFolder(
				folderId, newFolderId, serviceContext);
		}

		List<String> invalidArticleIds = new ArrayList<>();

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		String[] articleIds = ParamUtil.getStringValues(
			actionRequest, "rowIdsJournalArticle");

		for (String articleId : articleIds) {
			try {
				_journalArticleService.moveArticle(
					themeDisplay.getScopeGroupId(),
					HtmlUtil.unescape(articleId), newFolderId, serviceContext);
			}
			catch (InvalidDDMStructureException iddmse) {
				if (_log.isWarnEnabled()) {
					_log.warn(iddmse.getMessage());
				}

				invalidArticleIds.add(articleId);
			}
		}

		if (!invalidArticleIds.isEmpty()) {
			StringBundler sb = new StringBundler(4);

			sb.append("Folder ");
			sb.append(newFolderId);
			sb.append(" does not allow the structures for articles: ");
			sb.append(StringUtil.merge(invalidArticleIds));

			throw new InvalidDDMStructureException(sb.toString());
		}

		sendEditEntryRedirect(actionRequest, actionResponse);
	}

	
	public void moveEntriesToTrash(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteEntries(actionRequest, actionResponse, true);
	}

	public void moveFolderToTrash(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteFolder(actionRequest, actionResponse, true);
	}

	public void moveToTrash(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		deleteArticles(actionRequest, actionResponse, true);
	}

	public void previewArticle(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		updateArticle(actionRequest, actionResponse);
	}

	@Override
	public void render(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {

		String path = getPath(renderRequest, renderResponse);

		if (Objects.equals(path, "/edit_article.jsp")) {
			renderRequest.setAttribute(
				JournalWebKeys.ITEM_SELECTOR, _itemSelector);
		}

		renderRequest.setAttribute(
			JournalWebConfiguration.class.getName(), _journalWebConfiguration);

		renderRequest.setAttribute(
			JournalWebKeys.JOURNAL_CONTENT, _journalContent);

		renderRequest.setAttribute(
			JournalWebKeys.JOURNAL_CONVERTER, _journalConverter);

		super.render(renderRequest, renderResponse);
	}

	public void restoreTrashEntries(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		long[] restoreTrashEntryIds = StringUtil.split(
			ParamUtil.getString(actionRequest, "restoreTrashEntryIds"), 0L);

		for (long restoreTrashEntryId : restoreTrashEntryIds) {
			_trashEntryService.restoreEntry(restoreTrashEntryId);
		}
	}

	@Override
	public void serveResource(
			ResourceRequest resourceRequest, ResourceResponse resourceResponse)
		throws IOException, PortletException {

		resourceRequest.setAttribute(
			JournalWebConfiguration.class.getName(), _journalWebConfiguration);

		String resourceID = GetterUtil.getString(
			resourceRequest.getResourceID());

		HttpServletRequest request = _portal.getHttpServletRequest(
			resourceRequest);

		HttpServletResponse response = _portal.getHttpServletResponse(
			resourceResponse);
		
		// START -- Feature #3811 -- nhut.dang: Upload multiple images.
		if (resourceID.equals("uploadImage")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ServiceContext serviceContext = new ServiceContext();
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			String ddmStructureKey = uploadPortletRequest.getParameter("ddmStructureKey");
			String urlImage = uploadPortletRequest.getParameter("urlImage");
			String textWater = uploadPortletRequest.getParameter("textWater");
			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();

			String[] sourceFileName = null;
			InputStream[] lstIpts = null;
			long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
			long folderId = 0;
			DDMStructure structure = null;
			try {
				 structure = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(), 
						classNameId, ddmStructureKey);
				folderId = DocumentAndMediaUtil.createDLFolderStructure(themeDisplay, serviceContext,
						structure.getName(themeDisplay.getLocale()));
			} catch (PortalException e2) {
				e2.printStackTrace();
			}
				if (Validator.isNotNull(uploadPortletRequest.getFilesAsStream("imageToUpload"))) {
					lstIpts = uploadPortletRequest.getFilesAsStream("imageToUpload");
					sourceFileName = uploadPortletRequest.getFileNames("imageToUpload");
					int count = 0;
					for (InputStream i : lstIpts) {
						if (Validator.isNotNull(i)) {
							JSONObject json = JSONFactoryUtil.createJSONObject();
							File tempFile = FileUtil.createTempFile(i);
							// Text-Watermark
							if (Validator.isNotNull(textWater)) {
								DocumentAndMediaUtil.addWatermark(tempFile, "png", textWater);
							}
							
							try {
								
								FileEntry fileEntry = DocumentAndMediaUtil.addFileEntry(themeDisplay, folderId,
										sourceFileName[count], tempFile, serviceContext);
								String imgURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId()
										+ "/" + fileEntry.getFileName() + "/" + fileEntry.getUuid();

								json.put("imgURL", imgURL);
								json.put("idImage", fileEntry.getFileEntryId());
								FileUtil.delete(tempFile);
								json.put("errorExtension", "true");
							} catch (Exception e1) {
								
								e1.printStackTrace();
							}
							count++;
							jsonArray.put(json);
						}
					}
				}
				
				if (Validator.isNotNull(urlImage)) {
					JSONObject json = JSONFactoryUtil.createJSONObject();
					FileEntry fileEntry = DocumentAndMediaUtil.getFileByURL(urlImage, themeDisplay,
							structure.getName(themeDisplay.getLocale()));
					String imgURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/"
							+ fileEntry.getFileName() + "/" + fileEntry.getUuid();

					json.put("imgURL", imgURL);
					json.put("idImage", fileEntry.getFileEntryId());
					jsonArray.put(json);
				}
			

			resourceResponse.getWriter().write(jsonArray.toString());
			
		}
		// START -- Feature #4094 -- nhut.dang: Upload video,file,audio.
		else if (resourceID.equals("uploadVideo")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ServiceContext serviceContext = new ServiceContext();
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			String ddmStructureKey = uploadPortletRequest.getParameter("ddmStructureKey");
			String urlVideo = uploadPortletRequest.getParameter("linkVideo");
			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();

			String[] sourceFileName = null;
			InputStream[] lstIpts = null;
			long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
			long folderId = 0;
			DDMStructure structure = null;
			try {
				structure = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(), 
						classNameId, ddmStructureKey);
				folderId = DocumentAndMediaUtil.createDLFolderStructure(themeDisplay, serviceContext,
						structure.getName(themeDisplay.getLocale()));
			} catch (PortalException e2) {
				e2.printStackTrace();
			}
				if (Validator.isNotNull(uploadPortletRequest.getFilesAsStream("videoToUpload"))) {
					lstIpts = uploadPortletRequest.getFilesAsStream("videoToUpload");
					sourceFileName = uploadPortletRequest.getFileNames("videoToUpload");
					int count = 0;
					for (InputStream i : lstIpts) {
						if (Validator.isNotNull(i)) {
							JSONObject json = JSONFactoryUtil.createJSONObject();
							File tempFile = FileUtil.createTempFile(i);
							
							try {
								FileEntry fileEntry = DocumentAndMediaUtil.addFileEntry(themeDisplay, folderId,
										sourceFileName[count], tempFile, serviceContext);
								String srcURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId()
										+ "/" + fileEntry.getFileName() + "/" + fileEntry.getUuid();
								
								json.put("srcURL", srcURL);
								json.put("id", fileEntry.getFileEntryId());
								json.put("fileNameURL", sourceFileName[count]);
								FileUtil.delete(tempFile);
								json.put("errorExtension", "true");
							} catch (Exception e1) {
								
								e1.printStackTrace();
							}
							count++;
							jsonArray.put(json);
						}
					}
				}
				
			resourceResponse.getWriter().write(jsonArray.toString());
			
		}
		else if (resourceID.equals("uploadFile")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ServiceContext serviceContext = new ServiceContext();
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			String ddmStructureKey = uploadPortletRequest.getParameter("ddmStructureKey");
			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
			
			String[] sourceFileName = null;
			InputStream[] lstIpts = null;
			long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
			long folderId = 0;
			try {
				DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(), 
						classNameId, ddmStructureKey);
				folderId = DocumentAndMediaUtil.createDLFolderStructure(themeDisplay, serviceContext,
						structure.getName(themeDisplay.getLocale()));
			} catch (PortalException e2) {
				e2.printStackTrace();
			}
				if (Validator.isNotNull(uploadPortletRequest.getFilesAsStream("fileToUpload"))) {
					lstIpts = uploadPortletRequest.getFilesAsStream("fileToUpload");
					sourceFileName = uploadPortletRequest.getFileNames("fileToUpload");
					int count = 0;
					for (InputStream i : lstIpts) {
						if (Validator.isNotNull(i)) {
							JSONObject json = JSONFactoryUtil.createJSONObject();
							File tempFile = FileUtil.createTempFile(i);
							
							try {
								FileEntry fileEntry = DocumentAndMediaUtil.addFileEntry(themeDisplay, folderId,
										sourceFileName[count], tempFile, serviceContext);
								String srcURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId()
								+ "/" + fileEntry.getFileName() + "/" + fileEntry.getUuid();
								
								json.put("srcURL", srcURL);
								json.put("id", fileEntry.getFileEntryId());
								json.put("fileNameURL", sourceFileName[count]);
								FileUtil.delete(tempFile);
							} catch (Exception e1) {
								e1.printStackTrace();
							}
							count++;
							jsonArray.put(json);
						}
					}
				}
				
			resourceResponse.getWriter().write(jsonArray.toString());
			
		}
		else if (resourceID.equals("uploadAudio")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ServiceContext serviceContext = new ServiceContext();
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			String ddmStructureKey = uploadPortletRequest.getParameter("ddmStructureKey");
			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
			
			String[] sourceFileName = null;
			InputStream[] lstIpts = null;
			long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
				if (Validator.isNotNull(uploadPortletRequest.getFilesAsStream("audioToUpload"))) {
					long folderId = 0;
					try {
						DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(), 
								classNameId, ddmStructureKey);
						folderId = DocumentAndMediaUtil.createDLFolderStructure(themeDisplay, serviceContext,
								structure.getName(themeDisplay.getLocale()));
					} catch (PortalException e) {
						e.printStackTrace();
					}
					lstIpts = uploadPortletRequest.getFilesAsStream("audioToUpload");
					sourceFileName = uploadPortletRequest.getFileNames("audioToUpload");
					int count = 0;
					for (InputStream i : lstIpts) {
						if (Validator.isNotNull(i)) {
							JSONObject json = JSONFactoryUtil.createJSONObject();
							File tempFile = FileUtil.createTempFile(i);
							try {
								FileEntry fileEntry = DocumentAndMediaUtil.addFileEntry(themeDisplay, folderId,
										sourceFileName[count], tempFile, serviceContext);
								String srcURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId()
								+ "/" + fileEntry.getFileName() + "/" + fileEntry.getUuid();
								
								json.put("srcURL", srcURL);
								json.put("id", fileEntry.getFileEntryId());
								json.put("fileNameURL", sourceFileName[count]);
								FileUtil.delete(tempFile);
							} catch (Exception e1) {
								e1.printStackTrace();
							}
							count++;
							jsonArray.put(json);
						}
					}
				}
				
			
				resourceResponse.getWriter().write(jsonArray.toString());
				
			
			
		}
		// END -- Feature #4094 -- nhut.dang: Upload video, file, audio.
		
		
		else if (resourceID.equals("deleteMedia")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			long idFile = GetterUtil.getLong(uploadPortletRequest.getParameter("idFile"));
			JSONObject json = JSONFactoryUtil.createJSONObject();
			
			if (idFile != 0) {
				try {
					DLAppLocalServiceUtil.deleteFileEntry(idFile);
					json.put("success", true);
				} catch (Exception e) {
					json.put("success", false);
					e.printStackTrace();
				}
			}

			resourceResponse.getWriter().write(json.toString());
		}
		// END -- Feature #3811 -- nhut.dang: Upload multiple images.
		//START -- nhut.dang import data from file excel
		else if (resourceID.equals("importDataScores")) {
			
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String ddmStructureKey = uploadPortletRequest.getParameter("ddmStructureKey");
			String folderId = uploadPortletRequest.getParameter("folderId");
			File fileScore = uploadPortletRequest.getFile("fileScore");
			long categoryId = GetterUtil.getLong(uploadPortletRequest.getParameter("categoryId"));
			if(fileScore!=null) {
			InputStream inputStream = new FileInputStream(fileScore);
			
			JSONObject json = JSONFactoryUtil.createJSONObject();
			
			long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
			try {
				DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(), 
						classNameId, ddmStructureKey);
				// Get the DDM structure's fields.
				List<DDMFormField> fields = structure.getDDMFormFields(false);
				Workbook workbook = WorkbookFactory.create(inputStream);
				Sheet sheet = workbook.getSheetAt(0); // Getting the first Sheet
				Map<String, String> listParameter = new HashMap<String, String>();
				String titleName =""; 
				String description ="";
				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					Row row = sheet.getRow(i);
					Row rowFirst = sheet.getRow(0);
							for (Cell cell : row) {
								String cellValue = "";
								switch (cell.getCellType()) {
								case Cell.CELL_TYPE_STRING:
									cellValue =String.valueOf(cell.getRichStringCellValue());
									break;
								case Cell.CELL_TYPE_NUMERIC:
									if (DateUtil.isCellDateFormatted(cell)) {
										cellValue = df.format(cell.getDateCellValue());
									} else {
										int number = (int) cell.getNumericCellValue();
										cellValue =String.valueOf(number);
									}
									break;
								case Cell.CELL_TYPE_BOOLEAN:
									cellValue =String.valueOf(cell.getBooleanCellValue());
									break;
								case Cell.CELL_TYPE_FORMULA:
									cellValue =String.valueOf(cell.getCellFormula());
									break;
									
								default:
								}
								
								if(String.valueOf(rowFirst.getCell(cell.getColumnIndex()).getRichStringCellValue()).equals("soBaoDanh")) {
									titleName = String.valueOf(cell.getRichStringCellValue());
								}
								if(String.valueOf(rowFirst.getCell(cell.getColumnIndex()).getRichStringCellValue()).equals("hoTen")) {
									description = String.valueOf(cell.getRichStringCellValue());
								}
								listParameter.put(String.valueOf(rowFirst.getCell(cell.getColumnIndex()).getRichStringCellValue()), cellValue);
					}
							String contentArticle = JournalPortletUtil.setContent(fields, listParameter);
							JournalPortletUtil.createJournalArticle(themeDisplay, resourceRequest, ddmStructureKey, structure.getStructureId(), titleName, description, contentArticle, categoryId, GetterUtil.getLong(folderId));
						
				
				}
				
				json.put("success", true);
				
			} catch (Exception e) {
				json.put("success", false);
				e.printStackTrace();
			}
				
			resourceResponse.getWriter().write(json.toString());
			}
			
		}
		//END -- nhut.dang import data from file excel
		
		else if (resourceID.equals("uploadZip")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String sourceFileName = StringPool.BLANK;
			InputStream ips = null;
			JSONObject json = JSONFactoryUtil.createJSONObject();
			try {
				if(Validator.isNotNull(uploadPortletRequest.getFileAsStream("zipImgFile"))) {
					ips = uploadPortletRequest.getFileAsStream("zipImgFile");
					sourceFileName = uploadPortletRequest.getFileName("zipImgFile").replace(".zip", "");
					
					ExpandoColumn imageStorageColumn = ExpandoColumnLocalServiceUtil.getColumn(themeDisplay.getCompanyId(), 
							Group.class.getName(), "CUSTOM_FIELDS", "ImageTilesStorageDirectory");
					ExpandoColumn imageAccessColumn = ExpandoColumnLocalServiceUtil.getColumn(themeDisplay.getCompanyId(), 
							Group.class.getName(), "CUSTOM_FIELDS", "ImageTilesAccessPrefix");
					FileSystem fileSystem = FileSystems.getDefault();
					if(!Files.exists(fileSystem.getPath(imageStorageColumn.getDefaultData()))) {
						Files.createDirectory(fileSystem.getPath(imageStorageColumn.getDefaultData()));
					}
					
					ZipInputStream zis = new ZipInputStream(new BufferedInputStream(ips));
					ZipEntry zipEntry = null;
					String storagePath = imageStorageColumn.getDefaultData() + "/" + sourceFileName;
					String accessPath = imageAccessColumn.getDefaultData() + "/" + sourceFileName;
					
					if(Files.exists(fileSystem.getPath(storagePath))) {
						String salt = String.valueOf(Calendar.getInstance().getTimeInMillis());
						storagePath = storagePath + "_" + salt;
						accessPath = accessPath + "_" + salt;
						Files.createDirectory(fileSystem.getPath(storagePath));
					}else {
						Files.createDirectory(fileSystem.getPath(storagePath));
					}
					String mainFolderName = StringPool.BLANK;
					while((zipEntry = zis.getNextEntry()) != null) {
						try {
							if(zipEntry.isDirectory()) {
								Files.createDirectory(fileSystem.getPath(storagePath + "/" + zipEntry.getName()));
							}else {
								FileOutputStream fos = null;
								byte[] tmp = new byte[4*1024];
								String osPath = storagePath + "/" + zipEntry.getName();
								fos = new FileOutputStream(osPath);
								int size = 0;
								while((size = zis.read(tmp)) != -1){
									fos.write(tmp, 0 , size);
								}
								fos.flush();
								fos.close();
							}
							if(mainFolderName.equals(StringPool.BLANK))
								mainFolderName = zipEntry.getName().split("/")[0];
							
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
					zis.close();
					String folderDir = StringPool.BLANK;
					folderDir = themeDisplay.getPortalURL() + accessPath + "/" + mainFolderName + "/";
					json.put("success", "true");
					json.put("folderDir", folderDir);
					resourceResponse.getWriter().write(json.toString());
				} else {
					json.put("success", "false");
					resourceResponse.getWriter().write(json.toString());
				}
			}catch (Exception e) {
				json.put("success", "false");
				resourceResponse.getWriter().write(json.toString());
			}
		}
		
		// START -- khang.ho: Export article to Excel file.
		else if (resourceID.equals("exportArticles")) {
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) uploadPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			SimpleDateFormat viDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String articleIds = uploadPortletRequest.getParameter("ids");
			String structureKey = uploadPortletRequest.getParameter("structureKey");
			JSONObject json = JSONFactoryUtil.createJSONObject();
			
			DDMStructure structure = null;
			try {
				structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureKey) + 1);
			} catch (PortalException e1) {
				e1.printStackTrace();
			}

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("sheet1");
			Cell cell;
			Row row;

			// Title row
			HSSFFont titleFont = workbook.createFont();
			titleFont.setFontName("Times New Roman");
			titleFont.setBold(true);
			titleFont.setFontHeightInPoints((short) 18);
			
			HSSFCellStyle titleStyle = workbook.createCellStyle();
			titleStyle.setFont(titleFont);
			titleStyle.setAlignment(HorizontalAlignment.CENTER);

			// Table styles
			HSSFFont tableFont = workbook.createFont();
			tableFont.setFontName("Times New Roman");
			tableFont.setFontHeightInPoints((short) 12);
			HSSFCellStyle tableStyle = workbook.createCellStyle();
			tableStyle.setFont(tableFont);
			tableStyle.setAlignment(HorizontalAlignment.CENTER);
			tableStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			tableStyle.setWrapText(true);

			HSSFCellStyle tableTitleStyle = workbook.createCellStyle();
			tableTitleStyle.cloneStyleFrom(tableStyle);
			tableTitleStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
			FillPatternType solid = FillPatternType.SOLID_FOREGROUND;
			tableTitleStyle.setFillPattern(solid);

			// Border
			tableTitleStyle.setBorderBottom(BorderStyle.THIN);
			tableTitleStyle.setBorderTop(BorderStyle.THIN);
			tableTitleStyle.setBorderLeft(BorderStyle.THIN);
			tableTitleStyle.setBorderRight(BorderStyle.THIN);

			tableStyle.setBorderBottom(BorderStyle.THIN);
			tableStyle.setBorderTop(BorderStyle.THIN);
			tableStyle.setBorderLeft(BorderStyle.THIN);
			tableStyle.setBorderRight(BorderStyle.THIN);

			if (structure.getName(resourceRequest.getLocale())
				.equals(LanguageUtil.format(request, "swt-journal-web.question.answer", new Object()))) {

				int rowNum = 0, stt = 1;

				for (String s : articleIds.split(",")) {
					try {
						JournalArticle article = JournalArticleLocalServiceUtil
								.getArticle(themeDisplay.getScopeGroupId(), s);
						if (rowNum == 0) {
							row = sheet.createRow(rowNum);
							sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));
							cell = row.createCell(0);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.title.question", new Object()));
							cell.setCellStyle(titleStyle);

							rowNum++;

							row = sheet.createRow(rowNum);
							row.setHeightInPoints((2 * sheet.getDefaultRowHeightInPoints()));
							cell = row.createCell(0);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue("STT");

							cell = row.createCell(1);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.title", new Object()));

							cell = row.createCell(2);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.full.name", new Object()));

							cell = row.createCell(3);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.question", new Object()));

							cell = row.createCell(4);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.email", new Object()));

							cell = row.createCell(5);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.attachment", new Object()));

							cell = row.createCell(6);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.category", new Object()));

							cell = row.createCell(7);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.create.date", new Object()));

							cell = row.createCell(8);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.status", new Object()));

							rowNum++;
						}

						String articleCateName = "";
						AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(),
								article.getResourcePrimKey());
						for (AssetCategory cate : entry.getCategories()) {
							AssetVocabulary vocab = AssetVocabularyLocalServiceUtil
									.getAssetVocabulary(cate.getVocabularyId());
							if (!vocab.getName().equals(""))
								articleCateName = cate.getName();
						}

						Document document = SAXReaderUtil.read(article.getContent());

						String fullName = "";
						Node fullNameNote = document
								.selectSingleNode("/root/dynamic-element[@name='name']/dynamic-content");
						if (Validator.isNotNull(fullNameNote))
							fullName = fullNameNote.getText();

						String question = "";
						Node questionNote = document
								.selectSingleNode("/root/dynamic-element[@name='content']/dynamic-content");
						if (Validator.isNotNull(questionNote))
							question = questionNote.getText();

						String mail = "";
						Node mailNote = document
								.selectSingleNode("/root/dynamic-element[@name='email']/dynamic-content");
						if (Validator.isNotNull(mailNote))
							mail = mailNote.getText();

						String attachment = LanguageUtil.format(uploadPortletRequest,
								"swt-journal-web.export.file.attchment.no", new Object());
						Node attachmentNote = document
								.selectSingleNode("/root/dynamic-element[@name='attachment']/dynamic-content");
						if (Validator.isNotNull(attachmentNote.getText()))
							attachment = LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.attchment.yes", new Object());

						String status = LanguageUtil.format(uploadPortletRequest,
								"swt-journal-web.export.file.status.approved", new Object());
						if (article.getStatus() != 0)
							status = LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.status.not.approved", new Object());

						String createDate = viDateFormat.format(article.getCreateDate());

						row = sheet.createRow(rowNum);
						cell = row.createCell(0);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(stt);

						cell = row.createCell(1);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(article.getTitle(resourceRequest.getLocale()));

						cell = row.createCell(2);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(fullName);

						cell = row.createCell(3);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(question);

						cell = row.createCell(4);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(mail);

						cell = row.createCell(5);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(attachment);

						cell = row.createCell(6);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(articleCateName);

						cell = row.createCell(7);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(createDate);

						cell = row.createCell(8);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(status);

						rowNum++;
						stt++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				File file = File.createTempFile("tempFile", ".temp");
				FileOutputStream fos = new FileOutputStream(file);

				// Adjusting width
				sheet.setColumnWidth(0, 5 * 256);
				sheet.setColumnWidth(1, 35 * 256);
				sheet.setColumnWidth(2, 20 * 256);
				sheet.setColumnWidth(3, 25 * 256);
				sheet.setColumnWidth(4, 25 * 256);
				sheet.setColumnWidth(5, 15 * 256);
				sheet.setColumnWidth(6, 15 * 256);
				sheet.setColumnWidth(7, 15 * 256);
				sheet.setColumnWidth(8, 10 * 256);

				workbook.write(fos);
				workbook.close();

				try {
					DLFolder portalDataFolder = null;
					String fileURL = "";
					String salt = String.valueOf(Calendar.getInstance().getTimeInMillis());
					Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
					portalDataFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
							parentFolderId, PrefsPropsUtil.getString("hcm.folders.portal-data"));
					FileEntry fileEntry = DLAppServiceUtil.addTempFileEntry(themeDisplay.getScopeGroupId(),
							portalDataFolder.getFolderId(), "", salt + "_ThongKeHoiDap.xls", file, ".xls");
					fileURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/"
							+ fileEntry.getTitle() + "/" + fileEntry.getUuid();
					json.put("success", true);
					json.put("fileURL", fileURL);
					resourceResponse.getWriter().write(json.toString());
				} catch (PortalException e) {
					e.printStackTrace();
					json.put("success", false);
					resourceResponse.getWriter().write(json.toString());
				}

			}
			if (structure.getName(resourceRequest.getLocale())
					.equals(LanguageUtil.format(request, "swt-journal-web.question.answer.alt", new Object()))) {
				int rowNum = 0, stt = 1;

				for (String s : articleIds.split(",")) {
					try {
						JournalArticle article = JournalArticleLocalServiceUtil
								.getArticle(themeDisplay.getScopeGroupId(), s);
						if (rowNum == 0) {
							row = sheet.createRow(rowNum);
							sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));
							cell = row.createCell(0);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.title.question.alt", new Object()));
							cell.setCellStyle(titleStyle);

							rowNum++;

							row = sheet.createRow(rowNum);
							row.setHeightInPoints((2 * sheet.getDefaultRowHeightInPoints()));
							cell = row.createCell(0);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue("STT");

							cell = row.createCell(1);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.title", new Object()));

							cell = row.createCell(2);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.full.name", new Object()));

							cell = row.createCell(3);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.email", new Object()));

							cell = row.createCell(4);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.attachment", new Object()));

							cell = row.createCell(5);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.category", new Object()));

							cell = row.createCell(6);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.create.date", new Object()));

							cell = row.createCell(7);
							cell.setCellStyle(tableTitleStyle);
							cell.setCellValue(LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.table.status", new Object()));

							rowNum++;
						}

						String articleCateName = "";
						AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(),
								article.getResourcePrimKey());
						for (AssetCategory cate : entry.getCategories()) {
							AssetVocabulary vocab = AssetVocabularyLocalServiceUtil
									.getAssetVocabulary(cate.getVocabularyId());
							if (!vocab.getName().equals(""))
								articleCateName = cate.getName();
						}

						Document document = SAXReaderUtil.read(article.getContent());

						String fullName = "";
						Node fullNameNote = document
								.selectSingleNode("/root/dynamic-element[@name='name']/dynamic-content");
						if (Validator.isNotNull(fullNameNote))
							fullName = fullNameNote.getText();

						String mail = "";
						Node mailNote = document
								.selectSingleNode("/root/dynamic-element[@name='email']/dynamic-content");
						if (Validator.isNotNull(mailNote))
							mail = mailNote.getText();

						String attachment = LanguageUtil.format(uploadPortletRequest,
								"swt-journal-web.export.file.attchment.no", new Object());
						Node attachmentNote = document
								.selectSingleNode("/root/dynamic-element[@name='attachment']/dynamic-content");
						if (Validator.isNotNull(attachmentNote.getText()))
							attachment = LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.attchment.yes", new Object());

						String status = LanguageUtil.format(uploadPortletRequest,
								"swt-journal-web.export.file.status.approved", new Object());
						if (article.getStatus() != 0)
							status = LanguageUtil.format(uploadPortletRequest,
									"swt-journal-web.export.file.status.not.approved", new Object());

						String createDate = viDateFormat.format(article.getCreateDate());

						row = sheet.createRow(rowNum);
						cell = row.createCell(0);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(stt);

						cell = row.createCell(1);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(article.getTitle(resourceRequest.getLocale()));

						cell = row.createCell(2);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(fullName);

						cell = row.createCell(3);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(mail);

						cell = row.createCell(4);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(attachment);

						cell = row.createCell(5);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(articleCateName);

						cell = row.createCell(6);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(createDate);

						cell = row.createCell(7);
						cell.setCellStyle(tableStyle);
						cell.setCellValue(status);

						rowNum++;
						stt++;

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				File file = File.createTempFile("tempFile", ".temp");
				FileOutputStream fos = new FileOutputStream(file);

				// Adjusting width
				sheet.setColumnWidth(0, 5 * 256);
				sheet.setColumnWidth(1, 35 * 256);
				sheet.setColumnWidth(2, 20 * 256);
				sheet.setColumnWidth(3, 25 * 256);
				sheet.setColumnWidth(4, 25 * 256);
				sheet.setColumnWidth(5, 15 * 256);
				sheet.setColumnWidth(6, 15 * 256);
				sheet.setColumnWidth(7, 15 * 256);
				sheet.setColumnWidth(8, 10 * 256);

				workbook.write(fos);
				workbook.close();

				try {
					DLFolder portalDataFolder = null;
					String fileURL = "";
					String salt = String.valueOf(Calendar.getInstance().getTimeInMillis());
					Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
					portalDataFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
							parentFolderId, PrefsPropsUtil.getString("hcm.folders.portal-data"));
					FileEntry fileEntry = DLAppServiceUtil.addTempFileEntry(themeDisplay.getScopeGroupId(),
							portalDataFolder.getFolderId(), "", salt + "_ThongKeGopY.xls", file, ".xls");
					fileURL = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/"
							+ fileEntry.getTitle() + "/" + fileEntry.getUuid();
					json.put("success", true);
					json.put("fileURL", fileURL);
					resourceResponse.getWriter().write(json.toString());
				} catch (PortalException e) {
					e.printStackTrace();
					json.put("success", false);
					resourceResponse.getWriter().write(json.toString());
				}
			}
		}
		// END -- khang.ho: Export article to Excel file.
		
		else if (resourceID.equals("compareVersions")) {
			ThemeDisplay themeDisplay =
				(ThemeDisplay)resourceRequest.getAttribute(
					WebKeys.THEME_DISPLAY);

			long groupId = ParamUtil.getLong(resourceRequest, "groupId");
			String articleId = ParamUtil.getString(
				resourceRequest, "articleId");
			double sourceVersion = ParamUtil.getDouble(
				resourceRequest, "filterSourceVersion");
			double targetVersion = ParamUtil.getDouble(
				resourceRequest, "filterTargetVersion");
			String languageId = ParamUtil.getString(
				resourceRequest, "languageId");

			String diffHtmlResults = null;

			try {
				diffHtmlResults = JournalUtil.diffHtml(
					groupId, articleId, sourceVersion, targetVersion,
					languageId,
					new PortletRequestModel(resourceRequest, resourceResponse),
					themeDisplay);
			}
			catch (CompareVersionsException cve) {
				resourceRequest.setAttribute(
					WebKeys.DIFF_VERSION, cve.getVersion());
			}
			catch (Exception e) {
				try {
					_portal.sendError(e, request, response);
				}
				catch (ServletException se) {
				}
			}

			resourceRequest.setAttribute(
				WebKeys.DIFF_HTML_RESULTS, diffHtmlResults);

			PortletSession portletSession = resourceRequest.getPortletSession();

			PortletContext portletContext = portletSession.getPortletContext();

			PortletRequestDispatcher portletRequestDispatcher =
				portletContext.getRequestDispatcher(
					"/compare_versions_diff_html.jsp");

			portletRequestDispatcher.include(resourceRequest, resourceResponse);
		}
		else {
			super.serveResource(resourceRequest, resourceResponse);
		}
		
	}

	@Reference(unbind = "-")
	public void setItemSelector(ItemSelector itemSelector) {
		_itemSelector = itemSelector;
	}

	public void subscribeFolder(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		long folderId = ParamUtil.getLong(actionRequest, "folderId");

		_journalFolderService.subscribe(
			themeDisplay.getScopeGroupId(), folderId);
	}

	public void subscribeStructure(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		long ddmStructureId = ParamUtil.getLong(
			actionRequest, "ddmStructureId");

		_journalArticleService.subscribeStructure(
			themeDisplay.getScopeGroupId(), themeDisplay.getUserId(),
			ddmStructureId);

		sendEditArticleRedirect(actionRequest, actionResponse);
	}

	public void unsubscribeFolder(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		long folderId = ParamUtil.getLong(actionRequest, "folderId");

		_journalFolderService.unsubscribe(
			themeDisplay.getScopeGroupId(), folderId);
	}

	public void unsubscribeStructure(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		long ddmStructureId = ParamUtil.getLong(
			actionRequest, "ddmStructureId");

		_journalArticleService.unsubscribeStructure(
			themeDisplay.getScopeGroupId(), themeDisplay.getUserId(),
			ddmStructureId);

		sendEditArticleRedirect(actionRequest, actionResponse);
	}

	public void updateArticle(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		UploadException uploadException =
			(UploadException)actionRequest.getAttribute(
				WebKeys.UPLOAD_EXCEPTION);

		if (uploadException != null) {
			Throwable cause = uploadException.getCause();

			if (uploadException.isExceededLiferayFileItemSizeLimit()) {
				throw new LiferayFileItemException(cause);
			}

			if (uploadException.isExceededFileSizeLimit() ||
				uploadException.isExceededUploadRequestSizeLimit()) {

				throw new ArticleContentSizeException(cause);
			}

			throw new PortalException(cause);
		}

		UploadPortletRequest uploadPortletRequest =
			_portal.getUploadPortletRequest(actionRequest);

		if (_log.isDebugEnabled()) {
			_log.debug(
				"Updating article " +
					MapUtil.toString(uploadPortletRequest.getParameterMap()));
		}

		String actionName = ParamUtil.getString(
			actionRequest, ActionRequest.ACTION_NAME);

		long groupId = ParamUtil.getLong(uploadPortletRequest, "groupId");
		long folderId = ParamUtil.getLong(uploadPortletRequest, "folderId");
		long classNameId = ParamUtil.getLong(
			uploadPortletRequest, "classNameId");
		long classPK = ParamUtil.getLong(uploadPortletRequest, "classPK");

		String articleId = ParamUtil.getString(
			uploadPortletRequest, "articleId");

		boolean autoArticleId = ParamUtil.getBoolean(
			uploadPortletRequest, "autoArticleId");
		double version = ParamUtil.getDouble(uploadPortletRequest, "version");

		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(
			actionRequest, "title");
		Map<Locale, String> descriptionMap =
			LocalizationUtil.getLocalizationMap(actionRequest, "description");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			JournalArticle.class.getName(), uploadPortletRequest);
		
		String ddmStructureKey = ParamUtil.getString(
			uploadPortletRequest, "ddmStructureKey");

		DDMStructure ddmStructure = _ddmStructureLocalService.getStructure(
			_portal.getSiteGroupId(groupId),
			_portal.getClassNameId(JournalArticle.class), ddmStructureKey,
			true);

		Fields fields = DDMUtil.getFields(
			ddmStructure.getStructureId(), serviceContext);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		// START -- nhut.dang: Download images in content and save to D&L folder.
		if (fields.contains("content")) {
			Field contentField = fields.get("content");
			String contentSource = contentField.getValue().toString();
			if (contentSource.contains("http") || contentSource.contains("https")) {
				contentSource = DocumentAndMediaUtil
						.createArticleDLFiles(contentSource, themeDisplay, 
								ddmStructure.getName(themeDisplay.getLocale()));
			}
			contentField.setValue(contentSource);
		}
		// END -- nhut.dang: Download images in content and save to D&L folder.

		String structureContent = _journalConverter.getContent(
			ddmStructure, fields);

		Map<String, byte[]> structureImages = ActionUtil.getImages(
			structureContent, fields);

		Object[] contentAndImages = {structureContent, structureImages};
		
		String content = (String)contentAndImages[0];
				
		Map<String, byte[]> images =
			(HashMap<String, byte[]>)contentAndImages[1];

		String ddmTemplateKey = ParamUtil.getString(
			uploadPortletRequest, "ddmTemplateKey");
		String layoutUuid = ParamUtil.getString(
			uploadPortletRequest, "layoutUuid");

		Layout targetLayout = JournalUtil.getArticleLayout(layoutUuid, groupId);

		if (targetLayout == null) {
			layoutUuid = null;
		}

		int displayDateMonth = ParamUtil.getInteger(
			uploadPortletRequest, "displayDateMonth");
		int displayDateDay = ParamUtil.getInteger(
			uploadPortletRequest, "displayDateDay");
		int displayDateYear = ParamUtil.getInteger(
			uploadPortletRequest, "displayDateYear");
		int displayDateHour = ParamUtil.getInteger(
			uploadPortletRequest, "displayDateHour");
		int displayDateMinute = ParamUtil.getInteger(
			uploadPortletRequest, "displayDateMinute");
		int displayDateAmPm = ParamUtil.getInteger(
			uploadPortletRequest, "displayDateAmPm");

		if (displayDateAmPm == Calendar.PM) {
			displayDateHour += 12;
		}

		int expirationDateMonth = ParamUtil.getInteger(
			uploadPortletRequest, "expirationDateMonth");
		int expirationDateDay = ParamUtil.getInteger(
			uploadPortletRequest, "expirationDateDay");
		int expirationDateYear = ParamUtil.getInteger(
			uploadPortletRequest, "expirationDateYear");
		int expirationDateHour = ParamUtil.getInteger(
			uploadPortletRequest, "expirationDateHour");
		int expirationDateMinute = ParamUtil.getInteger(
			uploadPortletRequest, "expirationDateMinute");
		int expirationDateAmPm = ParamUtil.getInteger(
			uploadPortletRequest, "expirationDateAmPm");
		boolean neverExpire = ParamUtil.getBoolean(
			uploadPortletRequest, "neverExpire");

		if (!PropsValues.SCHEDULER_ENABLED) {
			neverExpire = true;
		}

		if (expirationDateAmPm == Calendar.PM) {
			expirationDateHour += 12;
		}

		int reviewDateMonth = ParamUtil.getInteger(
			uploadPortletRequest, "reviewDateMonth");
		int reviewDateDay = ParamUtil.getInteger(
			uploadPortletRequest, "reviewDateDay");
		int reviewDateYear = ParamUtil.getInteger(
			uploadPortletRequest, "reviewDateYear");
		int reviewDateHour = ParamUtil.getInteger(
			uploadPortletRequest, "reviewDateHour");
		int reviewDateMinute = ParamUtil.getInteger(
			uploadPortletRequest, "reviewDateMinute");
		int reviewDateAmPm = ParamUtil.getInteger(
			uploadPortletRequest, "reviewDateAmPm");

		boolean neverReview = ParamUtil.getBoolean(
			uploadPortletRequest, "neverReview");

		if (!PropsValues.SCHEDULER_ENABLED) {
			neverReview = true;
		}

		if (reviewDateAmPm == Calendar.PM) {
			reviewDateHour += 12;
		}

		boolean indexable = ParamUtil.getBoolean(
			uploadPortletRequest, "indexable");

		boolean smallImage = ParamUtil.getBoolean(
			uploadPortletRequest, "smallImage");
		String smallImageURL = ParamUtil.getString(
			uploadPortletRequest, "smallImageURL");
		File smallFile = uploadPortletRequest.getFile("smallFile");

		String articleURL = ParamUtil.getString(
			uploadPortletRequest, "articleURL");

		JournalArticle article = null;
		String oldUrlTitle = StringPool.BLANK;

		if (actionName.equals("addArticle")) {

			// Add article

			article = _journalArticleService.addArticle(
				groupId, folderId, classNameId, classPK, articleId,
				autoArticleId, titleMap, descriptionMap, content,
				ddmStructureKey, ddmTemplateKey, layoutUuid, displayDateMonth,
				displayDateDay, displayDateYear, displayDateHour,
				displayDateMinute, expirationDateMonth, expirationDateDay,
				expirationDateYear, expirationDateHour, expirationDateMinute,
				neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear,
				reviewDateHour, reviewDateMinute, neverReview, indexable,
				smallImage, smallImageURL, smallFile, images, articleURL,
				serviceContext);
		}
		else {

			// Update article

			article = _journalArticleService.getArticle(
				groupId, articleId, version);
			
			long oldArticleId = article.getId();
			
			String tempOldUrlTitle = article.getUrlTitle();
			
			folderId = article.getFolderId();

			if (actionName.equals("previewArticle") ||
				actionName.equals("updateArticle")) {

				article = _journalArticleService.updateArticle(
					groupId, folderId, articleId, version, titleMap,
					descriptionMap, content, ddmStructureKey, ddmTemplateKey,
					layoutUuid, displayDateMonth, displayDateDay,
					displayDateYear, displayDateHour, displayDateMinute,
					expirationDateMonth, expirationDateDay, expirationDateYear,
					expirationDateHour, expirationDateMinute, neverExpire,
					reviewDateMonth, reviewDateDay, reviewDateYear,
					reviewDateHour, reviewDateMinute, neverReview, indexable,
					smallImage, smallImageURL, smallFile, images, articleURL,
					serviceContext);
			}

			if (!tempOldUrlTitle.equals(article.getUrlTitle())) {
				oldUrlTitle = tempOldUrlTitle;
			}
			
			//Set existing article status to expired
			JournalArticle oldArticle = JournalArticleLocalServiceUtil.getArticle(oldArticleId);
			if(oldArticle.getStatus() == WorkflowConstants.STATUS_APPROVED) {
				oldArticle.setStatus(WorkflowConstants.STATUS_EXPIRED);
				JournalArticleLocalServiceUtil.updateJournalArticle(oldArticle);
			}
		}
		
		// START -- dung.nguyen: Update view count.
		if (fields.contains("viewCount")) {
			// Check if the user has permission to update view count.
			// If you change the permission checking logic, remmeber to update source code of client side accordingly.
			PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
			boolean hasUpdateViewCountPermission = permissionChecker.isOmniadmin()
					|| permissionChecker.isGroupAdmin(groupId)
					|| DDMStructurePermission.contains(permissionChecker, ddmStructure, "UPDATE_VIEW_COUNT");
			
			// Get the view count value from content.
			Field viewCountField = fields.get("viewCount");
			int viewCount = GetterUtil.getInteger(viewCountField.getValue().toString(), -1);
			
			// Update view count if the user has permission and view count has been changed.
			if (hasUpdateViewCountPermission && viewCount != -1) {
				String journalArticleClassName = JournalArticle.class.getName();
				AssetEntry journalAssetEntry = AssetEntryLocalServiceUtil.getEntry(journalArticleClassName, article.getResourcePrimKey());
				if (journalAssetEntry.getViewCount() != viewCount) {
					AssetEntryLocalServiceUtil.incrementViewCounter(themeDisplay.getUserId(), journalArticleClassName, journalAssetEntry.getClassPK(), viewCount);
				}
			}
		}
		// END -- dung.nguyen: Update view count.

		// Recent articles

		JournalUtil.addRecentArticle(actionRequest, article);

		// Journal content

		String portletResource = ParamUtil.getString(
			actionRequest, "portletResource");

		long referringPlid = ParamUtil.getLong(actionRequest, "referringPlid");

		if (Validator.isNotNull(portletResource) && (referringPlid > 0)) {
			Layout layout = _layoutLocalService.getLayout(referringPlid);

			PortletPreferences portletPreferences =
				PortletPreferencesFactoryUtil.getStrictPortletSetup(
					layout, portletResource);

			if (portletPreferences != null) {
				portletPreferences.setValue(
					"groupId", String.valueOf(article.getGroupId()));
				portletPreferences.setValue(
					"articleId", article.getArticleId());

				portletPreferences.store();

				updateContentSearch(
					actionRequest, portletResource, article.getArticleId());
			}

			hideDefaultSuccessMessage(actionRequest);
		}

		sendEditArticleRedirect(
			actionRequest, actionResponse, article, oldUrlTitle);

		long ddmStructureClassNameId = _portal.getClassNameId(
			DDMStructure.class);

		if (article.getClassNameId() == ddmStructureClassNameId) {
			String ddmPortletId = PortletProviderUtil.getPortletId(
				DDMStructure.class.getName(), Action.EDIT);

			MultiSessionMessages.add(
				actionRequest, ddmPortletId + "requestProcessed");
		}
	}

	public void updateFeed(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		String actionName = ParamUtil.getString(
			actionRequest, ActionRequest.ACTION_NAME);

		long groupId = ParamUtil.getLong(actionRequest, "groupId");

		String feedId = ParamUtil.getString(actionRequest, "feedId");
		boolean autoFeedId = ParamUtil.getBoolean(actionRequest, "autoFeedId");

		String name = ParamUtil.getString(actionRequest, "name");
		String description = ParamUtil.getString(actionRequest, "description");
		String ddmStructureKey = ParamUtil.getString(
			actionRequest, "ddmStructureKey");
		String ddmTemplateKey = ParamUtil.getString(
			actionRequest, "ddmTemplateKey");
		String ddmRendererTemplateKey = ParamUtil.getString(
			actionRequest, "ddmRendererTemplateKey");
		int delta = ParamUtil.getInteger(actionRequest, "delta");
		String orderByCol = ParamUtil.getString(actionRequest, "orderByCol");
		String orderByType = ParamUtil.getString(actionRequest, "orderByType");
		String targetLayoutFriendlyUrl = ParamUtil.getString(
			actionRequest, "targetLayoutFriendlyUrl");
		String targetPortletId = ParamUtil.getString(
			actionRequest, "targetPortletId");
		String contentField = ParamUtil.getString(
			actionRequest, "contentField");
		String feedType = ParamUtil.getString(
			actionRequest, "feedType", RSSUtil.FEED_TYPE_DEFAULT);

		String feedFormat = RSSUtil.getFeedTypeFormat(feedType);
		double feedVersion = RSSUtil.getFeedTypeVersion(feedType);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			JournalFeed.class.getName(), actionRequest);

		if (actionName.equals("addFeed")) {

			// Add feed

			_journalFeedService.addFeed(
				groupId, feedId, autoFeedId, name, description, ddmStructureKey,
				ddmTemplateKey, ddmRendererTemplateKey, delta, orderByCol,
				orderByType, targetLayoutFriendlyUrl, targetPortletId,
				contentField, feedFormat, feedVersion, serviceContext);
		}
		else {

			// Update feed

			_journalFeedService.updateFeed(
				groupId, feedId, name, description, ddmStructureKey,
				ddmTemplateKey, ddmRendererTemplateKey, delta, orderByCol,
				orderByType, targetLayoutFriendlyUrl, targetPortletId,
				contentField, feedFormat, feedVersion, serviceContext);
		}
	}

	public void updateFolder(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		long folderId = ParamUtil.getLong(actionRequest, "folderId");

		long parentFolderId = ParamUtil.getLong(
			actionRequest, "parentFolderId");
		String name = ParamUtil.getString(actionRequest, "name");
		String description = ParamUtil.getString(actionRequest, "description");

		boolean mergeWithParentFolder = ParamUtil.getBoolean(
			actionRequest, "mergeWithParentFolder");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			JournalFolder.class.getName(), actionRequest);

		if (folderId <= 0) {

			// Add folder

			_journalFolderService.addFolder(
				serviceContext.getScopeGroupId(), parentFolderId, name,
				description, serviceContext);
		}
		else {

			// Update folder

			long[] ddmStructureIds = StringUtil.split(
				ParamUtil.getString(
					actionRequest, "ddmStructuresSearchContainerPrimaryKeys"),
				0L);
			int restrinctionType = ParamUtil.getInteger(
				actionRequest, "restrictionType");

			_journalFolderService.updateFolder(
				serviceContext.getScopeGroupId(), folderId, parentFolderId,
				name, description, ddmStructureIds, restrinctionType,
				mergeWithParentFolder, serviceContext);
		}
	}

	public void updateWorkflowDefinitions(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		long[] ddmStructureIds = StringUtil.split(
			ParamUtil.getString(
				actionRequest, "ddmStructuresSearchContainerPrimaryKeys"),
			0L);
		int restrinctionType = ParamUtil.getInteger(
			actionRequest, "restrictionType");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			JournalFolder.class.getName(), actionRequest);

		_journalFolderService.updateFolder(
			serviceContext.getScopeGroupId(),
			JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID,
			JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, null, null,
			ddmStructureIds, restrinctionType, false, serviceContext);
	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_journalWebConfiguration = ConfigurableUtil.createConfigurable(
			JournalWebConfiguration.class, properties);
	}

	protected void deleteArticles(
			ActionRequest actionRequest, ActionResponse actionResponse,
			boolean moveToTrash)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		String[] deleteArticleIds = null;

		String articleId = ParamUtil.getString(actionRequest, "articleId");

		if (Validator.isNotNull(articleId)) {
			deleteArticleIds = new String[] {articleId};
		}
		else {
			deleteArticleIds = ParamUtil.getParameterValues(
				actionRequest, "rowIds");
		}

		List<TrashedModel> trashedModels = new ArrayList<>();

		for (String deleteArticleId : deleteArticleIds) {
			if (moveToTrash) {
				JournalArticle article =
					_journalArticleService.moveArticleToTrash(
						themeDisplay.getScopeGroupId(),
						HtmlUtil.unescape(deleteArticleId));

				trashedModels.add(article);
			}
			else {
				ActionUtil.deleteArticle(
					actionRequest, HtmlUtil.unescape(deleteArticleId));
			}
		}

		if (moveToTrash && !trashedModels.isEmpty()) {
			TrashUtil.addTrashSessionMessages(actionRequest, trashedModels);

			hideDefaultSuccessMessage(actionRequest);
		}

		sendEditArticleRedirect(actionRequest, actionResponse);
	}

	protected void deleteEntries(
			ActionRequest actionRequest, ActionResponse actionResponse,
			boolean moveToTrash)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		List<TrashedModel> trashedModels = new ArrayList<>();

		long[] deleteFolderIds = ParamUtil.getLongValues(
			actionRequest, "rowIdsJournalFolder");

		for (long deleteFolderId : deleteFolderIds) {
			if (moveToTrash) {
				JournalFolder folder = _journalFolderService.moveFolderToTrash(
					deleteFolderId);

				trashedModels.add(folder);
			}
			else {
				_journalFolderService.deleteFolder(deleteFolderId);
			}
		}

		String[] deleteArticleIds = ParamUtil.getStringValues(
			actionRequest, "rowIdsJournalArticle");

		for (String deleteArticleId : deleteArticleIds) {
			if (moveToTrash) {
				JournalArticle article =
					_journalArticleService.moveArticleToTrash(
						themeDisplay.getScopeGroupId(),
						HtmlUtil.unescape(deleteArticleId));

				trashedModels.add(article);
			}
			else {
				ActionUtil.deleteArticle(
					actionRequest, HtmlUtil.unescape(deleteArticleId));
			}
		}

		if (moveToTrash && !trashedModels.isEmpty()) {
			TrashUtil.addTrashSessionMessages(actionRequest, trashedModels);

			hideDefaultSuccessMessage(actionRequest);
		}

		sendEditEntryRedirect(actionRequest, actionResponse);
	}

	protected void deleteFolder(
			ActionRequest actionRequest, ActionResponse actionResponse,
			boolean moveToTrash)
		throws Exception {

		long folderId = ParamUtil.getLong(actionRequest, "folderId");

		List<TrashedModel> trashedModels = new ArrayList<>();

		if (moveToTrash) {
			JournalFolder folder = _journalFolderService.moveFolderToTrash(
				folderId);

			trashedModels.add(folder);
		}
		else {
			_journalFolderService.deleteFolder(folderId);
		}

		if (moveToTrash && !trashedModels.isEmpty()) {
			TrashUtil.addTrashSessionMessages(actionRequest, trashedModels);

			hideDefaultSuccessMessage(actionRequest);
		}
	}

	@Override
	protected void doDispatch(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {

		if (SessionErrors.contains(
				renderRequest, NoSuchArticleException.class.getName()) ||
			SessionErrors.contains(
				renderRequest, NoSuchFeedException.class.getName()) ||
			SessionErrors.contains(
				renderRequest, NoSuchFolderException.class.getName()) ||
			SessionErrors.contains(
				renderRequest, NoSuchStructureException.class.getName()) ||
			SessionErrors.contains(
				renderRequest, NoSuchTemplateException.class.getName()) ||
			SessionErrors.contains(
				renderRequest, PrincipalException.getNestedClasses())) {

			include("/error.jsp", renderRequest, renderResponse);
		}
		else {
			super.doDispatch(renderRequest, renderResponse);
		}
	}

	protected String getSaveAndContinueRedirect(
			ActionRequest actionRequest, JournalArticle article,
			String redirect)
		throws Exception {

		String referringPortletResource = ParamUtil.getString(
			actionRequest, "referringPortletResource");

		PortletURL portletURL = PortletURLFactoryUtil.create(
			actionRequest, JournalPortletKeys.JOURNAL,
			PortletRequest.RENDER_PHASE);

		portletURL.setParameter("mvcPath", "/edit_article.jsp");
		portletURL.setParameter("redirect", redirect);
		portletURL.setParameter(
			"referringPortletResource", referringPortletResource);
		portletURL.setParameter(
			"resourcePrimKey", String.valueOf(article.getResourcePrimKey()));
		portletURL.setParameter(
			"groupId", String.valueOf(article.getGroupId()));
		portletURL.setParameter(
			"folderId", String.valueOf(article.getFolderId()));
		portletURL.setParameter("articleId", article.getArticleId());
		portletURL.setParameter(
			"version", String.valueOf(article.getVersion()));
		portletURL.setWindowState(actionRequest.getWindowState());

		return portletURL.toString();
	}

	@Override
	protected boolean isAlwaysSendRedirect() {
		return true;
	}

	@Override
	protected boolean isSessionErrorException(Throwable cause) {
		if (cause instanceof ArticleContentException ||
			cause instanceof ArticleContentSizeException ||
			cause instanceof ArticleDisplayDateException ||
			cause instanceof ArticleExpirationDateException ||
			cause instanceof ArticleIdException ||
			cause instanceof ArticleSmallImageNameException ||
			cause instanceof ArticleSmallImageSizeException ||
			cause instanceof ArticleTitleException ||
			cause instanceof ArticleVersionException ||
			cause instanceof AssetCategoryException ||
			cause instanceof AssetTagException ||
			cause instanceof DuplicateArticleIdException ||
			cause instanceof DuplicateFeedIdException ||
			cause instanceof DuplicateFileEntryException ||
			cause instanceof DuplicateFolderNameException ||
			cause instanceof FeedContentFieldException ||
			cause instanceof FeedIdException ||
			cause instanceof FeedNameException ||
			cause instanceof FeedTargetLayoutFriendlyUrlException ||
			cause instanceof FeedTargetPortletIdException ||
			cause instanceof FileSizeException ||
			cause instanceof FolderNameException ||
			cause instanceof InvalidDDMStructureException ||
			cause instanceof LiferayFileItemException ||
			cause instanceof LocaleException ||
			cause instanceof StorageFieldRequiredException ||
			super.isSessionErrorException(cause)) {

			return true;
		}

		return false;
	}

	protected void sendEditArticleRedirect(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		sendEditArticleRedirect(
			actionRequest, actionResponse, null, StringPool.BLANK);
	}

	protected void sendEditArticleRedirect(
			ActionRequest actionRequest, ActionResponse actionResponse,
			JournalArticle article, String oldUrlTitle)
		throws Exception {

		String actionName = ParamUtil.getString(
			actionRequest, ActionRequest.ACTION_NAME);

		ThemeDisplay themeDisplay =
				(ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		String redirect = ParamUtil.getString(actionRequest, "redirect");
		
		String workflowPageUUID = ParamUtil.getString(actionRequest, "workflowPageUUID");
		String workflowPageURL = StringPool.BLANK;
		
		if (workflowPageUUID.length() != 0) {
			Layout workflowPageLayout = LayoutLocalServiceUtil.fetchLayout(workflowPageUUID, themeDisplay.getScopeGroupId(), true);
			if (workflowPageLayout != null) {
				workflowPageURL = themeDisplay.getURLPortal() + "/group/vi" + workflowPageLayout.getFriendlyURL(themeDisplay.getLocale());
				workflowPageURL += "?_com_liferay_portal_workflow_task_web_portlet_MyWorkflowTaskPortlet_navigation=pending";
			}
		}
		if (workflowPageURL != null && workflowPageURL.length() != 0) {
			redirect = workflowPageURL;
		}

		int workflowAction = ParamUtil.getInteger(
			actionRequest, "workflowAction", WorkflowConstants.ACTION_PUBLISH);

		String portletId = _httpUtil.getParameter(redirect, "p_p_id", false);

		String namespace = _portal.getPortletNamespace(portletId);

		if (Validator.isNotNull(oldUrlTitle)) {
			String oldRedirectParam = namespace + "redirect";

			String oldRedirect = _httpUtil.getParameter(
				redirect, oldRedirectParam, false);

			if (Validator.isNotNull(oldRedirect)) {
				String newRedirect = _httpUtil.decodeURL(oldRedirect);

				newRedirect = StringUtil.replace(
					newRedirect, oldUrlTitle, article.getUrlTitle());
				newRedirect = StringUtil.replace(
					newRedirect, oldRedirectParam, "redirect");

				redirect = StringUtil.replace(
					redirect, oldRedirect, newRedirect);
			}
		}

		if ((actionName.equals("deleteArticle") ||
			 actionName.equals("deleteArticles")) &&
			!ActionUtil.hasArticle(actionRequest)) {

			PortletURL portletURL = PortletURLFactoryUtil.create(
				actionRequest, themeDisplay.getPpid(),
				PortletRequest.RENDER_PHASE);

			redirect = portletURL.toString();
		}

		if ((article != null) &&
			(workflowAction == WorkflowConstants.ACTION_SAVE_DRAFT)) {

			redirect = getSaveAndContinueRedirect(
				actionRequest, article, redirect);

			if (actionName.equals("previewArticle")) {
				SessionMessages.add(actionRequest, "previewRequested");

				hideDefaultSuccessMessage(actionRequest);
			}
		}
		else {
			WindowState windowState = actionRequest.getWindowState();

			if (windowState.equals(LiferayWindowState.POP_UP)) {
				redirect = _portal.escapeRedirect(redirect);

				if (Validator.isNotNull(redirect)) {
					if (actionName.equals("addArticle") && (article != null)) {
						redirect = _httpUtil.addParameter(
							redirect, namespace + "className",
							JournalArticle.class.getName());
						redirect = _httpUtil.addParameter(
							redirect, namespace + "classPK",
							JournalArticleAssetRenderer.getClassPK(article));
					}
				}
			}
		}
		
		actionRequest.setAttribute(WebKeys.REDIRECT, redirect);
	}

	protected void sendEditEntryRedirect(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		String redirect = _portal.escapeRedirect(
			ParamUtil.getString(actionRequest, "redirect"));

		WindowState windowState = actionRequest.getWindowState();

		if (!windowState.equals(LiferayWindowState.POP_UP)) {
			sendRedirect(actionRequest, actionResponse);
		}
		else if (Validator.isNotNull(redirect)) {
			actionResponse.sendRedirect(redirect);
		}
	}

	@Reference
	protected void setDDMStructureLocalService(
		DDMStructureLocalService ddmStructureLocalService) {

		_ddmStructureLocalService = ddmStructureLocalService;
	}

	@Reference
	protected void setJournalArticleService(
		JournalArticleService journalArticleService) {

		_journalArticleService = journalArticleService;
	}

	@Reference
	protected void setJournalContent(JournalContent journalContent) {
		_journalContent = journalContent;
	}

	@Reference
	protected void setJournalContentSearchLocalService(
		JournalContentSearchLocalService journalContentSearchLocalService) {

		_journalContentSearchLocalService = journalContentSearchLocalService;
	}

	@Reference
	protected void setJournalConverter(JournalConverter journalConverter) {
		_journalConverter = journalConverter;
	}

	@Reference
	protected void setJournalFeedService(
		JournalFeedService journalFeedService) {

		_journalFeedService = journalFeedService;
	}

	@Reference
	protected void setJournalFolderService(
		JournalFolderService journalFolderService) {

		_journalFolderService = journalFolderService;
	}

	@Reference
	protected void setLayoutLocalService(
		LayoutLocalService layoutLocalService) {

		_layoutLocalService = layoutLocalService;
	}

	@Reference(
		target = "(&(release.bundle.symbolic.name=com.liferay.journal.web)(release.schema.version=1.0.0))",
		unbind = "-"
	)
	protected void setRelease(Release release) {
	}

	@Reference
	protected void setTrashEntryService(TrashEntryService trashEntryService) {
		_trashEntryService = trashEntryService;
	}

	protected void unsetDDMStructureLocalService(
		DDMStructureLocalService ddmStructureLocalService) {

		_ddmStructureLocalService = null;
	}

	protected void unsetJournalArticleService(
		JournalArticleService journalArticleService) {

		_journalArticleService = null;
	}

	protected void unsetJournalContent(JournalContent journalContent) {
		_journalContent = null;
	}

	protected void unsetJournalContentSearchLocalService(
		JournalContentSearchLocalService journalContentSearchLocalService) {

		_journalContentSearchLocalService = null;
	}

	protected void unsetJournalConverter(JournalConverter journalConverter) {
		_journalConverter = null;
	}

	protected void unsetJournalFeedService(
		JournalFeedService journalFeedService) {

		_journalFeedService = null;
	}

	protected void unsetJournalFolderService(
		JournalFolderService journalFolderService) {

		_journalFolderService = null;
	}

	protected void unsetLayoutLocalService(
		LayoutLocalService layoutLocalService) {

		_layoutLocalService = null;
	}

	protected void unsetTrashEntryService(TrashEntryService trashEntryService) {
		_trashEntryService = null;
	}

	protected void updateContentSearch(
			ActionRequest actionRequest, String portletResource,
			String articleId)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		Layout layout = themeDisplay.getLayout();

		long referringPlid = ParamUtil.getLong(actionRequest, "referringPlid");

		if (referringPlid > 0) {
			layout = _layoutLocalService.fetchLayout(referringPlid);
		}

		_journalContentSearchLocalService.updateContentSearch(
			layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(),
			portletResource, articleId, true);
	}

	private static final Log _log = LogFactoryUtil.getLog(JournalPortlet.class);

	private DDMStructureLocalService _ddmStructureLocalService;

	@Reference
	private HttpUtil _httpUtil;

	private ItemSelector _itemSelector;
	private JournalArticleService _journalArticleService;
	private JournalContent _journalContent;
	private JournalContentSearchLocalService _journalContentSearchLocalService;
	private JournalConverter _journalConverter;
	private JournalFeedService _journalFeedService;
	private JournalFolderService _journalFolderService;
	private volatile JournalWebConfiguration _journalWebConfiguration;
	private LayoutLocalService _layoutLocalService;

	@Reference
	private Portal _portal;

	private TrashEntryService _trashEntryService;

}