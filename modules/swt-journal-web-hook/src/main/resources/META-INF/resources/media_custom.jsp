<style>
.backgroud-header{
	border-bottom: 1px solid #eee ;
    background-color: #337ab7;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    color: white;
}
.modal-title-header{
    color: white !important;
}
.close-header{
    color: white !important;
}
.modal-position{
	position: relative;
}
.swt-label-name{
    margin-top: 10px;
}
.video-server{
    margin-top: 20px;
}
.img-grid{
    height: 250px;
    overflow-y: scroll;
}
.video-grid{
    height: 250px;
    overflow-y: scroll;
}
.file-grid{
    height: 250px;
    overflow-y: scroll;
}
.audio-grid{
    height: 250px;
    overflow-y: scroll;
}

</style>
<script  type="text/javascript"  src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>
<liferay-portlet:resourceURL id="deleteMedia" var="deleteMedia" copyCurrentRenderParameters="false"/>
<liferay-portlet:resourceURL id="uploadImage" var="uploadImage" copyCurrentRenderParameters="false" />
<liferay-portlet:resourceURL id="uploadVideo" var="uploadVideo" copyCurrentRenderParameters="false"/>
<liferay-portlet:resourceURL id="uploadFile" var="uploadFile" copyCurrentRenderParameters="false"/>
<liferay-portlet:resourceURL id="uploadAudio" var="uploadAudio" copyCurrentRenderParameters="false"/>
<!-- chen hinh -->
<div class="modal fade" id="myModalImage" role="dialog" style="display:none">
	<div class="modal-dialog modal-position">
		<div  id="success-action" style="display:none;">
		  <div class="alert alert-success alert-dismissible fade in" id="success-message">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  </div>
		</div>
		<div class="modal-content">
			<div class="modal-header backgroud-header">
				<button type="button" class="close close-header" data-dismiss="modal" id="btnCloseMyModal1">&times;</button>
				<h3 class="modal-title modal-title-header"><%=LanguageUtil.format(request, "swt-media-image",new Object())%></h3>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data">
					<div class="container" style="width:100%;">
						 <div class="row" style="margin-bottom:2%;">
							<div  class="col-xs-3 swt-label-name"><%=LanguageUtil.format(request, "select-type-upload", new Object())%></div>
							<div  class="col-xs-9">
								<select class="field form-control" onchange="changeSelectTypeUpload(1)" id="typeUpload">
								<option value="1"><%=LanguageUtil.format(request, "swt-file-from-computer", new Object())%></option>
								<option value="2"><%=LanguageUtil.format(request, "swt-file-from-url", new Object())%></option>
								</select>
							</div>
						</div> 
						 <div class="row" style="margin-bottom:2%;" id="fileComputer">
							<div  class="col-xs-3 swt-label-name"><%=LanguageUtil.format(request, "swt-file-image", new Object())%></div>
							<div  class="col-xs-9">
								<input class="field form-control" style="max-width: 100%;" type="file" name="imageToUpload" id="imageToUpload" accept="image/*" multiple="multiple" > </input>
							<text style="color: red;"><%=LanguageUtil.format(request, "acept-image", new Object())%></text>
							<div  class="row row-swt-responsive image-progress img-margin" id="image-progress">
							</div>
							</div>
						</div> 
						<div class="row" style="display: none;" id="fileFromUrl">
							<div  class="col-xs-3 swt-label-name" ><%=LanguageUtil.format(request, "swt-link-image", new Object())%></div>
							<div  class="col-xs-9"><aui:input name="linkImage" id="linkImage" label="" type="text" style="width: 100%;"></aui:input></div>
						</div>
						 <div class="row">
							<div  class="col-xs-3 swt-label-name" >
							</div>
							<div class="col-xs-9">
								<input type="checkbox" name="checkAddWatermark"  id="checkAddWatermark" style="float: left;margin-right: 5px;" value="false" checked ></input>
								<p style="padding-top: 2px;"><%=LanguageUtil.format(request, "swt-watermark", new Object()) %></p>
							</div>
							
						</div>
						
						<!-- TZ-begin-textWatermark -->
					<div class="row" id="loadLogoWatermark">
						<div  class="col-xs-3 swt-label-name">
							<%=LanguageUtil.format(request, "text-watermark", new Object())+":"%>
						</div>
						<div  class="col-xs-9">
							<aui:input name="textWater" id="textWater" type="text" style="font-style: italic;" 
										placeholder='<%=LanguageUtil.format(request, "swt-watermark-empty",new Object())%>' 
										label="">
							</aui:input>
						</div>
					</div>
					<!-- TZ-end-textWatermark -->
						
					<div class="btn btn-primary" name="upload" id="uploadImage" onclick="uploadImage()">
						<%=LanguageUtil.format(request, "swt-upload",new Object())%>
					</div>
					</div>
					
				</form>
					</div>
				<h3 style="margin-left: 10px;display: none;" id="captionModalImage"><%=LanguageUtil.format(request, "image-uploaded",new Object())%></h3>
				
				<div  class="row row-swt-responsive img-grid img-margin"></div>
			</div>

		</div>

	</div>
	<!-- nhut.dang start Feature #4094 uploadVideo, upload file, upload audio  -->
	<!-- start upload video -->
	<div class="modal fade" id="myModalVideo" role="dialog" style="display:none">
	<div class="modal-dialog modal-position">
		<div  id="success-video-action" style="display:none;">
		  <div class="alert alert-success alert-dismissible fade in" id="success-video-message">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  </div>
		</div>
		<div class="modal-content">
			<div class="modal-header backgroud-header">
				<button onclick="stopAllVideo()" type="button" class="close close-header" data-dismiss="modal" id="btnCloseMyModal1">&times;</button>
				<h3 class="modal-title modal-title-header"><%=LanguageUtil.format(request, "swt-media-video",new Object())%></h3>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data" id="form-video">
					<div class="container" style="width:100%;">
						 <div class="row" style="margin-bottom:2%;">
							<div  class="col-xs-3 swt-label-name"><%=LanguageUtil.format(request, "select-type-upload", new Object())%></div>
							<div  class="col-xs-9">
								<select class="field form-control" onchange="changeSelectTypeUpload(2)" id="typeUploadVideo">
								<option value="1"><%=LanguageUtil.format(request, "swt-file-from-computer", new Object())%></option>
								<option value="2"><%=LanguageUtil.format(request, "swt-embedded-from-url", new Object())%></option>
								<option value="3"><%=LanguageUtil.format(request, "swt-embedded-from-video-server", new Object())%></option>
								</select>
							</div>
						</div> 
						 <div class="row" style="margin-bottom:2%;" id="fileComputerVideo">
							<div  class="col-xs-3 swt-label-name"><%=LanguageUtil.format(request, "swt-file-image", new Object())%></div>
							<div  class="col-xs-9">
								<input class="field form-control" style="max-width: 100%;" type="file" name="videoToUpload" id="videoToUpload" accept="video/*"> </input>
								<text style="color: red;"><%=LanguageUtil.format(request, "acept-video", new Object())%></text>
							<div  class="row row-swt-responsive video-progress img-margin" id="video-progress">
							</div>
							</div>
						</div> 
						<div class="row" style="display: none;" id="fileFromUrlVideo">
							<div  class="col-xs-3 swt-label-name" ><%=LanguageUtil.format(request, "swt-embedded-from-url", new Object())%></div>
							<div  class="col-xs-9"><aui:input name="linkVideo" id="linkVideo" label="" type="text" style="width: 100%;"></aui:input></div>
						</div>
						
						<div class="row" style="display: none;" id="fileFromUrlVideoServer">
							<div  class="col-xs-3 swt-label-name" ><%=LanguageUtil.format(request, "swt-embedded-from-video-server", new Object())%></div>
							<div  class="col-xs-9"><aui:input name="linkVideoServer" id="linkVideoServer" label="" type="text" style="width: 100%;"></aui:input>
							<div class="video-server btn btn-warning" onclick="openVideoServer()"><span class="glyphicon glyphicon-cog"></span>Video server </div>
							</div>
						</div>
						
					<div class="btn btn-primary" name="upload" id="uploadVideo" onclick="uploadVideo()">
						<%=LanguageUtil.format(request, "swt-upload",new Object())%>
					</div>
					</div>
					
				</form>
					</div>
				<h3 style="margin-left: 10px;display: none;" id="captionModalVideo"><%=LanguageUtil.format(request, "video-uploaded",new Object())%></h3>
				<div  class="row row-swt-responsive video-grid img-margin" id="video-grid"></div>
			</div>

		</div>

	</div>
	<!-- end upload video -->
	<!-- start upload file -->
	<div class="modal fade" id="myModalFile" role="dialog" style="display:none">
	<div class="modal-dialog modal-position">
		<div  id="success-file-action" style="display:none;">
		  <div class="alert alert-success alert-dismissible fade in" id="success-file-message">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  </div>
		</div>
		<div class="modal-content">
			<div class="modal-header backgroud-header">
				<button type="button" class="close close-header" data-dismiss="modal" id="btnCloseMyModal1">&times;</button>
				<h3 class="modal-title modal-title-header"><%=LanguageUtil.format(request, "swt-media-file",new Object())%></h3>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data">
					<div class="container" style="width:100%;">
						 <div class="row" style="margin-bottom:2%;">
							<div  class="col-xs-3 swt-label-name"><%=LanguageUtil.format(request, "swt-file-image", new Object())%></div>
							<div  class="col-xs-9">
								<input class="field form-control" style="max-width: 100%;" multiple="multiple" type="file" name="fileToUpload" id="fileToUpload" accept=".pdf,.doc,.docx,.xls,.xlsx,.rar,.zip,.csv"> </input>
								<text style="color: red;"><%=LanguageUtil.format(request, "acept-file", new Object())%></text>
							<div  class="row row-swt-responsive file-progress img-margin" id="file-progress">
							</div>
							</div>
						</div> 
					<div class="btn btn-primary" name="upload" id="uploadFile" onclick="uploadFile()">
						<%=LanguageUtil.format(request, "swt-upload",new Object())%>
					</div>
					</div>
					
				</form>
					</div>
				<h3 style="margin-left: 10px;display: none;" id="captionModalFile"><%=LanguageUtil.format(request, "file-uploaded",new Object())%></h3>
				
				<div  class="row row-swt-responsive file-grid img-margin" id="file-grid"></div>
			</div>

		</div>

	</div>
	<!-- end upload file -->
	<!-- start upload audio -->
	<div class="modal fade" id="myModalAudio" role="dialog" style="display:none">
	<div class="modal-dialog modal-position">
		<div  id="success-audio-action" style="display:none;">
		  <div class="alert alert-success alert-dismissible fade in" id="success-audio-message">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  </div>
		</div>
		<div class="modal-content">
			<div class="modal-header backgroud-header">
				<button type="button" onclick="stopAllAudio()" class="close close-header" data-dismiss="modal" id="btnCloseMyModal1">&times;</button>
				<h3 class="modal-title modal-title-header"><%=LanguageUtil.format(request, "swt-media-audio",new Object())%></h3>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data">
					<div class="container" style="width:100%;">
						 <div class="row" style="margin-bottom:2%;">
							<div  class="col-xs-3 swt-label-name"><%=LanguageUtil.format(request, "swt-file-image", new Object())%></div>
							<div  class="col-xs-9">
								<input class="field form-control" style="max-width: 100%;" multiple="multiple" type="file" name="audioToUpload" id="audioToUpload"  accept=".mp3,.ogg,.wav,.m4a,.flac"> </input>
							<text style="color: red;"><%=LanguageUtil.format(request, "acept-audio", new Object())%></text>
							<div  class="row row-swt-responsive audio-progress" id="audio-progress"></div>
							</div>
						</div> 
					<div class="btn btn-primary" name="upload" id="uploadAudio" onclick="uploadAudio()">
						<%=LanguageUtil.format(request, "swt-upload",new Object())%>
					</div>
					</div>
					
				</form>
					</div>
				<h3 style="margin-left: 10px;display: none;" id="captionModalAudio"><%=LanguageUtil.format(request, "file-uploaded",new Object())%></h3>
				
				<div  class="row row-swt-responsive audio-grid img-margin" id="audio-grid"></div>
			</div>

		</div>

	</div>
	<!-- end upload file -->
	<!-- nhut.dang end Feature #4094 uploadVideo, upload file, upload audio  -->
<script type="text/javascript">
var ckeditorInstances = null;
function getCKeditorInstance(){
	if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
		for (var k in CKEDITOR.instances) {
		    if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith('<%=renderResponse.getNamespace()%>')) {
		        continue;
		    }
		    ckeditorInstances = CKEDITOR.instances[k];
		}
	}
}

</script>
<script type="text/javascript">
function changeSelectTypeUpload(number){
	if(number=="1"){
		var typeUpload = $("#typeUpload").val();
		if(typeUpload=="1"){
			$("#fileComputer").show();
			$("#fileFromUrl").hide();
		}else{
			$("#fileComputer").hide();
			$("#fileFromUrl").show();
			
		}
	}
	if(number=="2"){
		var typeUpload = $("#typeUploadVideo").val();
		if(typeUpload=="1"){
			$("#fileComputerVideo").show();
			$("#fileFromUrlVideo").hide();
			$("#fileFromUrlVideoServer").hide();
		}
		if(typeUpload=="2"){
			$("#fileFromUrlVideo").show();
			$("#fileComputerVideo").hide();
			$("#fileFromUrlVideoServer").hide();
		}
		
		if(typeUpload=="3"){
			$("#fileFromUrlVideo").hide();
			$("#fileComputerVideo").hide();
			$("#fileFromUrlVideoServer").show();
		}
	}
	
}
function uploadImage(){
	var ddmStructureKey = $("#<portlet:namespace />ddmStructureKey").val();
	var urlImage = $("#<portlet:namespace />linkImage").val();
	var textWater = $("#<portlet:namespace/>textWater").val();
		if($('#imageToUpload').get(0).files.length == 0 && urlImage == ""){
			alert('<%=LanguageUtil.format(request, "note-select-file", new Object())%>');
		}else{
			if(checkExtensionFile($('#imageToUpload').val())){
				var htmlBar = '<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">' ;
				htmlBar += '<%=LanguageUtil.format(request, "note-loading", new Object())%>';
				htmlBar	+= '</div></div>';
				$('.image-progress').append(htmlBar);
				 $.ajaxFileUpload({
					type : "POST", 
					fileElementId : 'imageToUpload',
					dataType : 'json',
					data : {
						ddmStructureKey:ddmStructureKey,
						urlImage: urlImage,
						textWater: textWater
					},
				   	url:'<%=uploadImage%>',
					success : function(data){
							$("#image-progress").empty();
							var message = '<%=LanguageUtil.format(request, "insert-success", new Object())%>';
								$("#success-message").empty();
								$("#success-message").append(message);
								$("#success-action").show();
								$("#success-action").fadeOut(5000);
								$('#captionModalImage').show();
							for (var index in data) {
								var html = '<div class="col-lg-3 col-md-4 col-xs-6 thumb" id="' + data[index].idImage + '">';
									html += '<input type="checkbox" class="image-checkbox" onclick="addImage(this,' + data[index].idImage + ')" >';
									html += '<span class="glyphicon glyphicon-remove image-remove"  onclick="deleteMedia(' + data[index].idImage + ')" style="float: right; margin-top: 4px; cursor: pointer;"></span>';
									html += '<a class="thumbnail" href="#">';
									html += '<img  style="width:110px; height:68px;object-fit: cover;" class="img-responsive image-upload" src="' + data[index].imgURL + '" alt=""></a></div>';
								$("#imageToUpload").val("");
								$('.img-grid').append(html);
							}
						
					},
					error : function(err) {
						 console.log("AJAX error in request: " + err);
					}
			});
		}
	}
		
}

function addImage(el, id){
	getCKeditorInstance();
	var url = jQuery(el).closest('div').find('img').attr('src');
	$("<img/>").load(function(){
	    var width  = this.width,
	        height = this.height; 
	    if(width < 670){
	    	if(jQuery(el).is( ":checked" ) == true){
	    		var html = '<div class="ExternalClass" id="div'+id+'" style="text-align:center;margin:0px;">'
	    				+ '<table width="'+width+'" align="center" border="0" cellpadding="1" cellspacing="1" style="width:100%">'
	    				+ '<tbody><tr><td style="text-align:center">'
	    				+ '<p><img alt="" style="width:' + width + 'px;height:' + height + 'px" class="imageContent imgSmallContent" id="image'+id+'" src="'+ url + '" border="0"/></p>'
	    				+ '</td></tr><tr><td style="text-align:center"><span style="color:rgb(0, 0, 255);font-size:12px">'
	    				+ '<%=LanguageUtil.format(request, "note-image-media", new Object())%></span></td></tr>'
	    				+ '</tbody></table></div><p>&nbsp;</p>';
	    				ckeditorInstances.insertHtml(html);
	    		}
	    }else{
	    	if(jQuery(el).is( ":checked" ) == true){
	    		var c = 480/width;
	    		var customHeight = height*c;
	    		var html = '<div class="ExternalClass" id="div'+id+'" style="text-align:center;margin:0px;">'
						+ '<table width="'+width+'" align="center" border="0" cellpadding="1" cellspacing="1" style="width:100%">'
						+ '<tbody><tr><td style="text-align:center">'
						+ '<p><img alt="" style="width:480px;height:' + customHeight +'px" class="imageContent imgSmallContent" id="image'+id+'" src="'+ url + '" border="0"/></p>'
						+ '</td></tr><tr><td style="text-align:center"><span style="color:rgb(0, 0, 255);font-size:12px">'
						+ '<%=LanguageUtil.format(request, "note-image-media", new Object())%></span></td></tr>'
						+ '</tbody></table></div><p>&nbsp;</p>';
						ckeditorInstances.insertHtml(html);
				}
	    }
	}).attr("src", url);
	
	if(jQuery(el).is( ":checked" ) == false){
		 deleteImageInCkeditor(id);
}
 }
 
function uploadVideo(){
	var typeUpload = $("#typeUploadVideo").val();
	var ddmStructureKey = $("#<portlet:namespace />ddmStructureKey").val();
	var linkVideo = $("#<portlet:namespace />linkVideo").val();
	var linkYoutube = $("#<portlet:namespace />linkYoutube").val();
	var linkVideoServer = $("#<portlet:namespace />linkVideoServer").val();
	if(typeUpload=="1"){
		if($('#videoToUpload').get(0).files.length == 0){
			alert('<%=LanguageUtil.format(request, "note-select-file", new Object())%>');
		}else{
			if(checkExtensionFile($('#videoToUpload').val())){
				var htmlBar = '<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">' ;
				htmlBar += '<%=LanguageUtil.format(request, "note-loading", new Object())%>';
				htmlBar	+= '</div></div>';
				$('.video-progress').append(htmlBar);
				$.ajaxFileUpload({
					type : "POST", 
					fileElementId : 'videoToUpload',
					dataType : 'json',
					data : {
						ddmStructureKey:ddmStructureKey,
						linkVideo: linkVideo,
					},
				   	url:'<%=uploadVideo%>',
					success : function(data){
							$("#video-progress").empty();
							var message = '<%=LanguageUtil.format(request, "insert-success", new Object())%>';
								$("#success-video-message").empty();
								$("#success-video-message").append(message);
								$("#success-video-action").show();
								$("#success-video-message").fadeOut(5000);
								$('#captionModalVideo').show();
							for (var index in data) {
								var html = '<div class="col-lg-3 col-md-4 col-xs-6 video" id="' + data[index].id + '">';
									html += '<input type="checkbox" class="video-checkbox" onclick="addVideo(this,' + data[index].id + ')" >';
									html += '<span class="glyphicon glyphicon-remove image-remove"  onclick="deleteMedia(' + data[index].id + ')" style="float: right; margin-top: 4px; cursor: pointer;"></span>';
									html += '<a class="thumbnail" href="#">';
									html += '<video id="' + data[index].id +'" width="100%" height="315" onplay="playOneVideo('+data[index].id+')" src="'+ data[index].srcURL +'"controls>';
									html += '<source src="' + data[index].srcURL + '"'+ '"type="video/mp4" '+ 'name="' + data[index].fileNameURL   +'"></video></div>';
								
								$("#videoToUpload").val("");
								$('.video-grid').append(html);
							}
						
					},
					error : function(err) {
						 console.log("AJAX error in request: " + err);
					}
			});
		}
	}
}
	getCKeditorInstance();
	if(typeUpload=="2"){
		if(linkVideo==""){
			alert('<%=LanguageUtil.format(request, "note-input-link-file", new Object())%>');
		}else{
			var htmlVideo ="";
			htmlVideo = '<div class="video" style="text-align: center;"><video width="600px" height="400px" controls>'
				+ '<source src="' +linkVideo+ '" "type="video/mp4"></video>'
				+ '<p  class="caption-img" style="text-align: center;font-size:13px;color:#0072bc;padding: 10px; padding-bottom:0px;">'
				+ '<i>'+ '<%=LanguageUtil.format(request, "note-image-media", new Object())%>'+'</i></p>'
				'</div><p>&nbsp;</p>';
			ckeditorInstances.insertHtml(htmlVideo);
		}
	}		
		
	if(typeUpload=="3"){
		if(linkVideoServer==""){
			alert('<%=LanguageUtil.format(request, "note-input-link-file", new Object())%>');
		}else{
			var htmlVideoServer ="";
				
			htmlVideoServer = '<div class="video" style="text-align: center;"><video width="600px" height="400px" controls>'
				+ '<source src="' +linkVideoServer+ '" "type="video/mp4"></video>'
				+ '<p  class="caption-img" style="text-align: center;font-size:13px;color:#0072bc;padding: 10px; padding-bottom:0px;">'
				+ '<i>'+ '<%=LanguageUtil.format(request, "note-image-media", new Object())%>'+'</i></p>'
				'</div><p>&nbsp;</p>';
			ckeditorInstances.insertHtml(htmlVideoServer);
		}
	}		
			
		
}
	function addVideo(el,fileId){
		getCKeditorInstance();
		var url = jQuery(el).closest('div').find('video').find('source').attr('src'); 
		var name= jQuery(el).closest('div').find('video').find('source').attr('name'); 
		if(jQuery(el).is( ":checked" ) == true){
				var html = '<div class="video" id="div'+fileId+'" style="text-align: center;"><video width="600px" id="'+fileId+'" onplay="playOneVideo('+fileId+')" height="400px" controls>'
					+ '<source src="' +url+ '" "type="video/mp4" name="' +name+'"></video>'
					+ '<p id="'+fileId+'" class="caption-img" style="text-align: center;font-size:13px;color:#0072bc;padding: 10px; padding-bottom:0px;">'
					+ '<i>'+ name+'</i></p>'
					'</div><p>&nbsp;</p>';
					ckeditorInstances.insertHtml(html);
				
			 }
			if(jQuery(el).is( ":checked" ) == false){
				deleteImageInCkeditor(fileId);
			}
		}
	
	//play one video when upload
	function playOneVideo(idFile){
		var x = document.getElementById("video-grid").querySelectorAll("video");
		if(x.length>1){
		    for(var i =0; i<x.length ;i++){
		    	var id =x[i].getAttribute('id');
		    	if(id!=idFile){
		        	x[i].pause();
		        }
		    }
		}
	}
	function stopAllVideo(){
		var x = document.getElementById("video-grid").querySelectorAll("video");
		if(x.length >= 1){
		    for(var i =0; i<x.length ;i++){
		        	x[i].pause();
		    }
		}
	}
	function uploadFile(){
		var ddmStructureKey = $("#<portlet:namespace />ddmStructureKey").val();
			if($('#fileToUpload').get(0).files.length == 0){
				alert('<%=LanguageUtil.format(request, "note-select-file", new Object())%>');
			}else{
				if(checkExtensionFile($('#fileToUpload').val())){
					var htmlBar = '<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">' ;
					htmlBar += '<%=LanguageUtil.format(request, "note-loading", new Object())%>';
					htmlBar	+= '</div></div>';
					$('.file-progress').append(htmlBar);
					 $.ajaxFileUpload({
						type : "POST", 
						fileElementId : 'fileToUpload',
						dataType : 'json',
						data : {
							ddmStructureKey:ddmStructureKey,
						},
					   	url:'<%=uploadFile%>',
						success : function(data){
								$("#file-progress").empty();
								var message = '<%=LanguageUtil.format(request, "insert-success", new Object())%>';
									$("#success-file-message").empty();
									$("#success-file-message").append(message);
									$("#success-file-action").show();
									$("#success-file-message").fadeOut(5000);
									$('#captionModalFile').show();
								for (var index in data) {
									var html = '<div class="col-lg-3 col-md-4 col-xs-6 file" id="' + data[index].id + '">';
										html += '<input type="checkbox" class="file-checkbox" onclick="addFile(this,' + data[index].id + ')" >';
										html += '<span class="glyphicon glyphicon-remove image-remove"  onclick="deleteMedia(' + data[index].id + ')" style="float: right; margin-top: 4px; cursor: pointer;"></span>';
										html += '<a class="thumbnail" id="' + data[index].id + '" href="'+data[index].srcURL +'" name="'+data[index].fileNameURL+'">'+data[index].fileNameURL+'</a></div>';
									
									$("#fileToUpload").val("");
									$('.file-grid').append(html);
								}
							
						},
						error : function(err) {
							 console.log("AJAX error in request: " + err);
						}
					
				});
			}
		}
			
	}
		function addFile(el,fileId){
			getCKeditorInstance();
			var url = jQuery(el).closest('div').find('a').attr('href'); 
			var name= jQuery(el).closest('div').find('a').attr('name'); 
			if(jQuery(el).is(":checked") == true){
					var html = '<div id="div'+fileId+'"><a href="'+ url + '" name="'+name+ '">'+ name+ '</a></div><p id="'+fileId+'"></p>';
						ckeditorInstances.insertHtml(html);
				 }
				if(jQuery(el).is(":checked") == false){
					deleteImageInCkeditor(fileId);
				}
	}
	function uploadAudio(){
		var ddmStructureKey = $("#<portlet:namespace />ddmStructureKey").val();
			if($('#audioToUpload').get(0).files.length == 0){
				alert('<%=LanguageUtil.format(request, "note-select-file", new Object())%>');
			}else{
				if(checkExtensionFile($('#audioToUpload').val())){
					var htmlBar = '<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">' ;
					htmlBar += '<%=LanguageUtil.format(request, "note-loading", new Object())%>';
					htmlBar	+= '</div></div>';
					$('.audio-progress').append(htmlBar);
					 $.ajaxFileUpload({
						type : "POST", 
						fileElementId : 'audioToUpload',
						dataType : 'json',
						data : {
							ddmStructureKey:ddmStructureKey,
						},
					   	url:'<%=uploadAudio%>',
						success : function(data){
								$("#audio-progress").empty();
								var message = '<%=LanguageUtil.format(request, "insert-success", new Object())%>';
								$("#success-audio-message").empty();
								$("#success-audio-message").append(message);
								$("#success-audio-action").show();
								$("#success-audio-message").fadeOut(5000);
								$('#captionModalAudio').show();
							for (var index in data) {
								var html = '<div class="col-lg-3 col-md-4 col-xs-6 audio" id="' + data[index].id + '">';
									html += '<input type="checkbox" class="audio-checkbox" onclick="addAudio(this,' + data[index].id + ')" >';
									html += '<span class="glyphicon glyphicon-remove image-remove"  onclick="deleteMedia(' + data[index].id + ')" style="float: right; margin-top: 4px; cursor: pointer;"></span>';
									html += '<audio id="'+data[index].id+'" onplay="playOneAudio('+data[index].id+')" controls>';
									html += '<source src="' + data[index].srcURL + '" type="audio/mp3" name="'+ data[index].fileNameURL  + '"></audio><p style="text-align: center;">' +data[index].fileNameURL+ '</p></div>';
								
								$("#audioToUpload").val("");
								$('.audio-grid').append(html);
							}
						},
						error : function(err) {
						
						}
					
				});
			}
		}
			
	}
	function addAudio(el,fileId){
			getCKeditorInstance();
			var url = jQuery(el).closest('div').find('audio').find('source').attr('src'); 
			var name = jQuery(el).closest('div').find('audio').find('source').attr('name'); 
			if(jQuery(el).is( ":checked" ) == true){
					var html = '<div id="div'+fileId+'" style="text-align: center;width:600px;">'
						+ '<audio id="'+fileId+'" onplay="playOneAudio('+fileId+')" controls>'
						+ '<source src="' +url+ '"'
						+ 'type="audio/mp3" >'
						+ '</audio>'
						+ '<p class="caption-img" style="text-align: center;font-size:12px;color:#0072bc; padding:10px;" contentEditable>'
						+ '<i>'
						+ name
						+' </i></p>'
						'</div><p>&nbsp;</p>';
						ckeditorInstances.insertHtml(html);
				 }
				if(jQuery(el).is( ":checked" ) == false){
					deleteImageInCkeditor(fileId);
				}
			}
	 	
		// play one audio when upload
		function playOneAudio(idFile){
			var x = document.getElementById("audio-grid").querySelectorAll("audio");
			if(x.length>1){
			    for(var i =0; i<x.length ;i++){
			    	var id=x[i].getAttribute('id');
			    	if(id!=idFile){
			        	x[i].pause();
			        }
			    }
			}
		}
		// stop all audio 
		function stopAllAudio(){
			var x = document.getElementById("audio-grid").querySelectorAll("audio");
			if(x.length>1){
			    for(var i =0; i<x.length ;i++){
			        	x[i].pause();
			    }
			}
		}
	
	
/* xoa hinh khoi server  */
function deleteMedia(idFile){
	$.ajax({
		  type: "POST",
		  url: '<%=deleteMedia%>',
		  data:{
			  idFile : idFile,
		  },
		  dataType: 'json',
		  success: function(data){
			  var message = '<%=LanguageUtil.format(request, "delete-success", new Object())%>';
				$("#success-message").empty();
				$("#success-message").append(message);
				$("#success-action").show();
				$("#success-action").fadeOut(5000);
			  if (data["success"] ==true){
				  $("#"+idFile).remove();
				  deleteImageInCkeditor(idFile);
			  }
			 
		 }
	});
}

/*xoa hinh trong ckeditor  */
function deleteImageInCkeditor(id){
	if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
		for (var k in CKEDITOR.instances) {
			if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith("_com_liferay")) {
				continue;
			}
			
			var finalHtml = "";
			var contentSelector = $(CKEDITOR.instances[k].getData()).not("#div" + id);
			
			contentSelector.each(function() {
				finalHtml += $(this).prop("outerHTML");
			});
			
			CKEDITOR.instances[k].setData(finalHtml);
		}
	}
	
}
function checkExtensionFile(srcFile){
	  var extension = srcFile.substr(srcFile.lastIndexOf('.') + 1).toLowerCase();
	  var allowedExtensions = <%=LanguageUtil.format(request, "file-allowedExtensions", new Object())%>;
	          if (allowedExtensions.indexOf(extension) === -1) {
	        		alert('<%=LanguageUtil.format(request, "message-file-extension", new Object())%>');
	               return false;
	           }else{
	               return true;
	           }
}

$("#<portlet:namespace />textWater").val('<%=LanguageUtil.format(request, "watermark-default", new Object())%>');
		$("#checkAddWatermark").on('change', function() {
		 if ($(this).is(':checked')) {
			$(this).attr('value', 'true');
			$("#<portlet:namespace />textWater").val('<%=LanguageUtil.format(request, "watermark-default", new Object())%>');
			$('#loadLogoWatermark').css('display','block');
		} else {
			$(this).attr('value', 'false');
			$("#<portlet:namespace />textWater").val('');
			$('#loadLogoWatermark').css('display','none');
		 }
		});
</script>