<%@ include file="/init.jsp" %>

<c:choose>
	<c:when test="<%= embedded %>">
		<%@ include file="/article/metadata_custom.jsp" %>
	</c:when>
	<c:otherwise>
		<%@ include file="/article/metadata_origin.jsp" %>
	</c:otherwise>
</c:choose>