<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%
JournalArticle article = journalDisplayContext.getArticle();

boolean smallImage = BeanParamUtil.getBoolean(article, request, "smallImage");

boolean changeStructure = GetterUtil.getBoolean(request.getAttribute("edit_article.jsp-changeStructure"));

String imgUrl = null;
if (article != null && !article.isNew()) {
	imgUrl = article.getArticleImageURL(themeDisplay);
}
%>

<style scoped>
	.swt-hidden {
		display: none;
	}
	
	.input-boolean-wrapper, .input-text-wrapper {
		margin-bottom: 0 !important;
	}
	
	.swt-img-container {
		margin-top: 10px;
		max-width: 300px;
	    position: relative;
	    width: 100%;
	}
	
	.swt-img-container img {
	    width: 100%;
	    height: auto;
	    object-fit: cover;
	}
	
	.swt-img-container button {
	    position: absolute;
	    top: 0%;
	    left: 100%;
	    transform: translate(-100%, 0%);
	    -ms-transform: translate(-100%, 0%);
	    background-color: #5555;
	    color: white;
	    padding: 10px 20px;
	    border: none;
	    border-radius: unset;
	    cursor: pointer;
	    text-align: center;
	}
	
	.swt-img-container button:hover {
	    background-color: black;
	}
</style>

<liferay-ui:error-marker
	key="<%= WebKeys.ERROR_SECTION %>"
	value="small_image"
/>

<aui:model-context bean="<%= article %>" model="<%= JournalArticle.class %>" />

<liferay-ui:error exception="<%= ArticleSmallImageNameException.class %>">

	<%
	String[] imageExtensions = PrefsPropsUtil.getStringArray(PropsKeys.JOURNAL_IMAGE_EXTENSIONS, StringPool.COMMA);
	%>

	<liferay-ui:message key="image-names-must-end-with-one-of-the-following-extensions" /> <%= HtmlUtil.escape(StringUtil.merge(imageExtensions, ", ")) %>.
</liferay-ui:error>

<liferay-ui:error exception="<%= ArticleSmallImageSizeException.class %>">

	<%
	long imageMaxSize = PrefsPropsUtil.getLong(PropsKeys.JOURNAL_IMAGE_SMALL_MAX_SIZE);
	%>

	<liferay-ui:message arguments="<%= TextFormatter.formatStorageSize(imageMaxSize, locale) %>" key="please-enter-a-small-image-with-a-valid-file-size-no-larger-than-x" translateArguments="<%= false %>" />
</liferay-ui:error>

<div id="<portlet:namespace />smallImageContainer">
	<p>Nếu bạn không tải thêm ảnh mới, ảnh đầu tiên trong bài viết sẽ được chọn làm ảnh đại diện.</p>
	<div class="lfr-journal-small-image-header swt-hidden">
		<label for="useSmallImage">
			<input type="checkbox" name="smallImage" id="useSmallImage" />
			<%=LanguageUtil.format(request, "upload-small-image", new Object())%>
		</label>
	</div>

	<div class="lfr-journal-small-image-content">
		<div class="row-fluid">
			<aui:input cssClass="lfr-journal-small-image-type swt-hidden" 
				ignoreRequestValue="<%= changeStructure %>" 
				inlineField="<%= true %>" label="" name="smallImageType" type="radio" />
			<aui:input cssClass="lfr-journal-small-image-value" 
				ignoreRequestValue="<%= changeStructure %>" 
				inlineField="<%= true %>" label="" name="smallFile" type="file"
				onChange="onSmallFileChanged(this)" />
		</div>
		
		<%-- Small image preview --%>
		<div class="swt-img-container">
			<img alt="<liferay-ui:message escapeAttribute="<%= true %>" key="preview" />" 
				class="img-responsive lfr-journal-small-image-preview" 
				src='<%= imgUrl == null ? StringPool.BLANK : imgUrl %>' />
			<button id="removeSmallImage">X</button>
		</div>
	</div>
</div>

<script>
	function onSmallFileChanged(input) {
		var val = input.files.length !== 0;
		
		$("#useSmallImage").prop("checked", val);
		$("#<%=renderResponse.getNamespace()%>smallImageType_1").prop("checked", val);
		
		if (val) {
			readURL(input);
		} else {
			$(".swt-img-container").css("display", "none");
		}
	}

	function readURL(input) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$(".lfr-journal-small-image-preview").attr("src", e.target.result);
			$(".swt-img-container").css("display", "inherit");
		}
		
		reader.readAsDataURL(input.files[0]);
	}

	$(document).ready(function() {
		<% if (imgUrl != null && imgUrl.length() != 0) { %>
			$("#useSmallImage").prop("checked", true);
			$("#<%=renderResponse.getNamespace()%>smallImageType_1").prop("checked", true);
		<% } else { %>
			$(".swt-img-container").css("display", "none");
		<% } %>
		
		$("[id$='removeSmallImage']").on("click", function() {
			$("#useSmallImage").prop("checked", false);
			$("#<%=renderResponse.getNamespace()%>smallFile").val("");
			$(".swt-img-container").css("display", "none");
			return false;
		});
	});
</script>