<%@ include file="/init.jsp" %>

<c:choose>
	<c:when test="<%= embedded %>">
		<%@ include file="/article/small_image_custom.jsp" %>
	</c:when>
	<c:otherwise>
		<%@ include file="/article/small_image_origin.jsp" %>
	</c:otherwise>
</c:choose>