<%@ include file="/init.jsp" %>

<%!
private String _getLayoutBreadcrumb(HttpServletRequest request, Layout layout, Locale locale) throws Exception {
	List<Layout> ancestors = layout.getAncestors();

	StringBundler sb = new StringBundler(4 * ancestors.size() + 5);

	if (layout.isPrivateLayout()) {
		sb.append(LanguageUtil.get(request, "private-pages"));
	}
	else {
		sb.append(LanguageUtil.get(request, "public-pages"));
	}

	sb.append(StringPool.SPACE);
	sb.append(StringPool.GREATER_THAN);
	sb.append(StringPool.SPACE);

	Collections.reverse(ancestors);

	for (Layout ancestor : ancestors) {
		sb.append(HtmlUtil.escape(ancestor.getName(locale)));
		sb.append(StringPool.SPACE);
		sb.append(StringPool.GREATER_THAN);
		sb.append(StringPool.SPACE);
	}

	sb.append(HtmlUtil.escape(layout.getName(locale)));

	return sb.toString();
}
%>

<c:choose>
	<c:when test="<%= embedded %>">
		<%@ include file="/article/display_page_custom.jsp" %>
	</c:when>
	<c:otherwise>
		<%@ include file="/article/display_page_origin.jsp" %>
	</c:otherwise>
</c:choose>