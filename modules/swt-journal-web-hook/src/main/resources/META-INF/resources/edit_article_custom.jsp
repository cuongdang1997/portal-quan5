<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="com.liferay.portal.kernel.model.Role"%>
<%@page import="com.liferay.portal.kernel.service.RoleLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page
	import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.lists.model.DDLRecord"%>
<%@page
	import="com.liferay.dynamic.data.lists.service.DDLRecordLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.model.Value"%>
<%@page
	import="com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue"%>
<%@page import="com.liferay.journal.model.JournalFolder"%>
<%@page
	import="com.liferay.journal.service.JournalFolderLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.repository.model.Folder"%>
<%@page
	import="com.liferay.portal.kernel.service.permission.LayoutPermissionUtil"%>
<%@page
	import="com.liferay.portal.kernel.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.service.ServiceContextFactory"%>

<%@page import="java.io.File"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@ include file="/media_custom.jsp"%>

<link
	href="<%=request.getContextPath()%>/css/jquery.highlight-within-textarea.css"
	rel="stylesheet" />

<script
	src="<%=request.getContextPath()%>/js/jquery.highlight-within-textarea.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<%
	String redirect = ParamUtil.getString(request, "redirect");

	String portletResource = ParamUtil.getString(request, "portletResource");

	long referringPlid = ParamUtil.getLong(request, "referringPlid");
	String referringPortletResource = ParamUtil.getString(request, "referringPortletResource");

	boolean changeStructure = GetterUtil.getBoolean(ParamUtil.getString(request, "changeStructure"));

	JournalArticle article = journalDisplayContext.getArticle();

	long groupId = BeanParamUtil.getLong(article, request, "groupId", scopeGroupId);

	long folderId = BeanParamUtil.getLong(article, request, "folderId",
			JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID);

	long classNameId = BeanParamUtil.getLong(article, request, "classNameId");
	long classPK = BeanParamUtil.getLong(article, request, "classPK");

	String articleId = BeanParamUtil.getString(article, request, "articleId");

	double version = BeanParamUtil.getDouble(article, request, "version",
			JournalArticleConstants.VERSION_DEFAULT);

	String ddmStructureKey = ParamUtil.getString(request, "ddmStructureKey");

	if (Validator.isNull(ddmStructureKey) && (article != null)) {
		ddmStructureKey = article.getDDMStructureKey();
	}

	DDMStructure ddmStructure = null;

	long ddmStructureId = ParamUtil.getLong(request, "ddmStructureId");

	if (ddmStructureId > 0) {
		ddmStructure = DDMStructureLocalServiceUtil.fetchStructure(ddmStructureId);
	} else if (Validator.isNotNull(ddmStructureKey)) {
		ddmStructure = DDMStructureLocalServiceUtil.fetchStructure(
				(article != null) ? article.getGroupId() : themeDisplay.getSiteGroupId(),
				PortalUtil.getClassNameId(JournalArticle.class), ddmStructureKey, true);
	}

	String ddmTemplateKey = ParamUtil.getString(request, "ddmTemplateKey");

	if (Validator.isNull(ddmTemplateKey) && (article != null)
			&& Objects.equals(article.getDDMStructureKey(), ddmStructureKey)) {
		ddmTemplateKey = article.getDDMTemplateKey();
	}

	DDMTemplate ddmTemplate = null;

	long ddmTemplateId = ParamUtil.getLong(request, "ddmTemplateId");

	if (ddmTemplateId > 0) {
		ddmTemplate = DDMTemplateLocalServiceUtil.fetchDDMTemplate(ddmTemplateId);
	} else if (Validator.isNotNull(ddmTemplateKey)) {
		ddmTemplate = DDMTemplateLocalServiceUtil.fetchTemplate(
				(article != null) ? article.getGroupId() : themeDisplay.getSiteGroupId(),
				PortalUtil.getClassNameId(DDMStructure.class), ddmTemplateKey, true);
	}

	if (ddmTemplate == null) {
		List<DDMTemplate> ddmTemplates = DDMTemplateServiceUtil.getTemplates(company.getCompanyId(),
				ddmStructure.getGroupId(), PortalUtil.getClassNameId(DDMStructure.class),
				ddmStructure.getStructureId(), PortalUtil.getClassNameId(JournalArticle.class), true,
				WorkflowConstants.STATUS_APPROVED);

		if (!ddmTemplates.isEmpty()) {
			ddmTemplate = ddmTemplates.get(0);
		}
	}

	String defaultLanguageId = LocaleUtil.toLanguageId(LocaleUtil.getSiteDefault());

	boolean changeableDefaultLanguage = journalWebConfiguration.changeableDefaultLanguage();

	if (article != null) {
		String articleDefaultLanguageId = LocalizationUtil.getDefaultLanguageId(article.getContent(),
				LocaleUtil.getSiteDefault());

		if (!Objects.equals(defaultLanguageId, articleDefaultLanguageId)) {
			changeableDefaultLanguage = true;
		}

		defaultLanguageId = articleDefaultLanguageId;
	}

	boolean showHeader = ParamUtil.getBoolean(request, "showHeader", true);

	request.setAttribute("edit_article.jsp-redirect", redirect);

	request.setAttribute("edit_article.jsp-structure", ddmStructure);
	request.setAttribute("edit_article.jsp-template", ddmTemplate);

	request.setAttribute("edit_article.jsp-defaultLanguageId", defaultLanguageId);

	request.setAttribute("edit_article.jsp-changeStructure", changeStructure);
%>

<%
	//Parse portlet preferences to JSON object.
	String jsonPrefs = ParamUtil.getString(request, "jsonPrefs");
	JSONObject prefs = null;
	if (jsonPrefs == null || jsonPrefs.length() == 0) {
		prefs = JSONFactoryUtil.createJSONObject();
	} else {
		prefs = JSONFactoryUtil.createJSONObject(jsonPrefs);
	}

	// dung.nguyen (2018/11/12): If the request is made from the embedded portlet,
	// the portlet request of protected service context will be null. It causes the 
	// Null Pointer Exception in JournalScheduleFormNavigatorEntry.isVisible function.
	// In order to get workaround this issue, I replace the protected service context
	// by the service context of current request.
	ServiceContext tmpServletContext = ServiceContextThreadLocal.getServiceContext();
	PortletRequest tmpPortletRequest = (PortletRequest) tmpServletContext.getRequest()
			.getAttribute("javax.portlet.request");
	if (tmpPortletRequest == null) {
		ServiceContextThreadLocal.popServiceContext();
		PortletRequest pRequest = (PortletRequest) request.getAttribute("javax.portlet.request");
		ServiceContext newServiceContext = ServiceContextFactory.getInstance(JournalArticle.class.getName(),
				pRequest);
		ServiceContextThreadLocal.pushServiceContext(newServiceContext);
	}

	// The default "Document & Media" folder for this article.
	long dmFolderId = 0;

	// The ddmStructure is just initialized if user selects filter by structure.
	// So, if it's null, we'll instantiate it by structure key which is always available.
	DDMStructure tmpStructure = ddmStructure;
	if (tmpStructure == null) {
		tmpStructure = DDMStructureLocalServiceUtil.getStructure(groupId,
				ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class), ddmStructureKey);
	}

	// The paths must be ordered by folder hierarchy.
	String[] paths = new String[]{tmpStructure.getName(locale),};

	// Auto-create the "Web Content" folder if not exist.
	JournalFolder jnFolder = null;
	ServiceContext jnFolderSctx = null;
	long jnFolderId = 0;
	for (String path : paths) {
		jnFolder = JournalFolderLocalServiceUtil.fetchFolder(groupId, jnFolderId, path);
		if (jnFolder != null) {
			jnFolderId = jnFolder.getFolderId();
			continue;
		}

		if (jnFolderSctx == null) {
			jnFolderSctx = ServiceContextFactory.getInstance(JournalFolder.class.getName(), request);
			jnFolderSctx.setDeriveDefaultPermissions(true);
		}
		jnFolder = JournalFolderLocalServiceUtil.addFolder(jnFolderSctx.getUserId(), groupId, jnFolderId, path,
				StringPool.BLANK, jnFolderSctx);
		jnFolderId = jnFolder.getFolderId();
	}

	// Update request's folder ID with the new one. The new article will be saved to this folder.
	if (jnFolderId != 0) {
		folderId = jnFolderId;
		request.setAttribute("folderId", jnFolderId);
	}

	//Prepare the path for creating "Document & Media" folder.
	boolean createDateFolders = prefs.getBoolean("createDateFolders", false);
	if (createDateFolders) {
		Calendar cal = Calendar.getInstance();

		// If the article has been created, use the create date. Don't create new folders.
		if ((article != null) && !article.isNew()) {
			cal.setTime(article.getCreateDate());
		}

		cal.setTime(new Date());
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		String monthStr = (month < 10 ? "0" : StringPool.BLANK) + month;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		String dayStr = (day < 10 ? "0" : StringPool.BLANK) + day;
		paths = new String[]{tmpStructure.getName(locale), String.valueOf(year), monthStr, dayStr};
	} else {
		paths = new String[]{tmpStructure.getName(locale)};
	}
	
	//Get value show priority 
	boolean enablePriority = prefs.getBoolean("enablePriority", false);

	// Auto-create the "Document & Media" folder if not exist.
	Folder dmFolder = null;
	ServiceContext dmFolderSctx = null;
	for (String path : paths) {
		try {
			dmFolder = DLAppLocalServiceUtil.getFolder(groupId, dmFolderId, path);
		} catch (PortalException e) {
			if (dmFolderSctx == null) {
				dmFolderSctx = ServiceContextFactory.getInstance(Folder.class.getName(), request);
				dmFolderSctx.setDeriveDefaultPermissions(true);
			}
			dmFolder = DLAppLocalServiceUtil.addFolder(dmFolderSctx.getUserId(), groupId, dmFolderId, path,
					StringPool.BLANK, dmFolderSctx);
		}
		dmFolderId = dmFolder.getFolderId();
	}

	// Load the "bad word" records from Dynamic Data List.
	List<String> badWordsList = new ArrayList<>();
	long ddlBadWordId = prefs.getLong("badWordsSetId", 0);
	if (ddlBadWordId != 0 && swtDisplayStyle == WebContentDisplayStyle.CMS.getCode()) {
		List<DDLRecord> records = DDLRecordLocalServiceUtil.getRecords(ddlBadWordId);

		Value tmpVal = null;
		String tmpStr = null;

		for (DDLRecord r : records) {
			List<DDMFormFieldValue> values = r.getDDMFormFieldValues("value");
			for (DDMFormFieldValue v : values) {
				if (v == null) {
					continue;
				}

				tmpVal = v.getValue();
				if (tmpVal == null) {
					continue;
				}

				tmpStr = tmpVal.getString(locale);
				if (tmpStr == null) {
					continue;
				}

				tmpStr = tmpStr.trim();
				if (tmpStr.length() == 0) {
					continue;
				}

				badWordsList.add(tmpStr.toLowerCase());
			}
		}
	}

	// Check if the user has permission to update view count.
	// If you change the permission checking logic, remmeber to update source code of server side accordingly.
	boolean hasUpdateViewCountPermission = permissionChecker.isOmniadmin()
			|| permissionChecker.isGroupAdmin(groupId)
			|| DDMStructurePermission.contains(permissionChecker, ddmStructure, "UPDATE_VIEW_COUNT");
%>

<%
	String workflowPageUUID = prefs.getString("workflowPageUUID", StringPool.BLANK);

	boolean redirectWorkflowPage = false;
	if (workflowPageUUID.length() != 0) {
		Layout workflowPageLayout = LayoutLocalServiceUtil.fetchLayout(workflowPageUUID, groupId, true);
		if (workflowPageLayout != null) {
			redirectWorkflowPage = LayoutPermissionUtil.contains(permissionChecker, workflowPageLayout, "VIEW");
		}
	}
	
	//Get current User's organizationId ([0] - user's only in ONE organization)
	User currentUser = themeDisplay.getUser();
	long userOrgId = 0;
	if(currentUser.getOrganizations().size() > 0)
		userOrgId = currentUser.getOrganizationIds()[0];
	
	//RSS article
	//Get module category name
	String moduleCategoryName = "";
	try {
		AssetEntry entryTmp = null;
		if(article != null){
			entryTmp = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
			if(entryTmp.getCategories().size() > 0){
				for(AssetCategory cate : entryTmp.getCategories()){
					AssetVocabulary vocabTmp = AssetVocabularyLocalServiceUtil.getVocabulary(cate.getVocabularyId());
					if(vocabTmp.getName().equals("MODULE")){
						moduleCategoryName = cate.getName();
					}
				}
			}
		}
	} catch (PortalException e) {
		// Do nothing.
		e.printStackTrace();
	}
	//Get structure by module category name
	DDMStructure structureFromCategoryName = null;
	if(!moduleCategoryName.equals("")){
		try{
			DynamicQuery structureQr = DDMStructureLocalServiceUtil.dynamicQuery()
					.add(RestrictionsFactoryUtil.like("name", "%" + moduleCategoryName + "%"));
			List<DDMStructure> listTmp = DDMStructureLocalServiceUtil.dynamicQuery(structureQr);
			if(listTmp.size() > 0)
				structureFromCategoryName = listTmp.get(0);
		} catch (Exception e) {
			// Do nothing.
			System.out.println("error finding structure??");
			e.printStackTrace();
		}
	}
%>
<style scoped>
/* Update the form size. */
#content {
	min-height: unset !important;
}

.panel-group {
	margin: 5px;
}

form[id$='fm1'] {
	padding-left: 0 !important;
	padding-right: 0 !important;
}

.portlet-icon-back {
	display: none;
}

.portlet-journal .lfr-ddm-field-group {
	padding: 0;
}

.portlet-journal .form-builder-field {
	box-shadow: unset;
	margin: 0;
	-webkit-box-shadow: unset;
}

.portlet-journal .lfr-ddm-field-group .button-holder {
	margin: 0;
}

.portlet-journal fieldset {
	margin-top: 5px;
}

.portlet-journal fieldset .panel-body {
	padding: 10px;
}

.input-checkbox-wrapper {
	margin: 0;
}

.input-checkbox-wrapper label, .input-boolean-wrapper label {
	font-weight: normal;
}

.swt-btn-toolbar {
	margin-top: 20px;
}

/* Hide the permission, display page, structure & template tabs. */
#displayPage {
	display: none;
}

#permissions {
	display: none;
}

#structureAndTemplate {
	display: none;
}

/* Highlight the panel title. */
.panel-title {
	text-transform: uppercase;
	font-weight: bold;
}

/* Increase the top margin of vocabulary labels. */
#metadataContent .field-content {
	margin-top: 15px
}

/* Hide the empty message of "related-asset" section. */
div[id$='assetLinksSearchContainerEmptyResultsMessage'] {
	display: none;
}

/* Update style of "related-asset" table */
div[id$='assetLinksSearchContainer'] table {
	border: 1px solid #ddd;
	margin-top: 15px;
}

div[id$='assetLinksSearchContainer'] table th {
	background-color: #F5F8FA;
	font-weight: bold;
	text-transform: uppercase;
}

/*
	dung.nguyen: We'll hide some unnecessary elements when document loaded.
	By then, the user will see thoses elements appear then disappear at init.
	Thus, we should hide the entire form then display it on loaded.
	*/
form #<%=renderResponse.getNamespace()%>fm1 {
	display: none;
}

/* Increase height and fix width size of description textarea. */
#content textarea #<%=renderResponse.getNamespace()%>description {
	height: 80px !important;
	resize: vertical;
}

/* Update style of section header */
.panel-default {
	border-color: #869cad;
}

.panel-default>.panel-heading {
	color: #fff;
	border-color: #869cad;
	background: #085791;
}

.panel-default>.panel-heading+.panel-collapse>.panel-body {
	border-top-color: #869cad;
	background: #f5f5f5;
}

/* khang.ho: review pop ups for unsaved news articles */
	#articleReviewModal{
		background: black;
        display: none;
	}
	.modal-dialog{
		width: 92%;
	    left: 4%;
	    margin: 0;
	}
	.review-title{
		color: #fff;
	    margin-bottom: 3%;
	    margin-top: 2%;
	    font-size: 14px;
    	margin-left: 2px;
	}
	.modal-body{
		font-size: 14px;
	    height: 100%;
	    overflow-y: scroll;
	}
	.review-content{
		height: 550px;
	}
	.close-review{
		cursor: pointer;
	}
	.article-review-content{
		max-width: 550px;
	    margin: 0 auto;
	}
	#articleDescription{
		font-weight: bold;
		text-align: justify;
	}
	.article-review-content img{
		max-width: 550px;
	}
	
	.btn-min-width-100 {
		min-width: 100px;
	}
	.zip-btn{
		margin-left: 20px;
	}
	#zipUploadModal{
		display: none;
	}
	.zip-modal-dialog{
		top: 30%;
	}
	.progress-upload{
		display: none;
	}
	.progress-upload img{
		height: 20px;
    	width: 280px;
	}
</style>

<style>
mark, .mark {
	background-color: yellow !important;
}
</style>

<%-- Hide the header since we've already displayed the portlet title.
<c:if test="<%= showHeader %>">

	<%
	portletDisplay.setShowBackIcon(true);

	if (Validator.isNotNull(redirect)) {
		portletDisplay.setURLBack(redirect);
	}
	else if ((classNameId == JournalArticleConstants.CLASSNAME_ID_DEFAULT) && (article != null)) {
		PortletURL backURL = liferayPortletResponse.createRenderURL();

		backURL.setParameter("groupId", String.valueOf(article.getGroupId()));
		backURL.setParameter("folderId", String.valueOf(article.getFolderId()));

		portletDisplay.setURLBack(backURL.toString());
	}

	String title = StringPool.BLANK;

	if (classNameId > JournalArticleConstants.CLASSNAME_ID_DEFAULT) {
		title = LanguageUtil.get(request, "structure-default-values");
	}
	else if ((article != null) && !article.isNew()) {
		title = article.getTitle(locale);
	}
	else {
		title = LanguageUtil.get(request, "new-web-content");
	}

	renderResponse.setTitle(title);
	%>

</c:if>
--%>

<aui:model-context bean="<%=article%>"
	model="<%=JournalArticle.class%>" />

<portlet:actionURL var="editArticleActionURL"
	windowState="<%=WindowState.NORMAL.toString()%>">
	<portlet:param name="mvcPath" value="/edit_article.jsp" />
	<portlet:param name="ddmStructureKey" value="<%=ddmStructureKey%>" />
</portlet:actionURL>

<portlet:renderURL var="editArticleRenderURL"
	windowState="<%=WindowState.NORMAL.toString()%>">
	<portlet:param name="mvcPath" value="/edit_article.jsp" />
</portlet:renderURL>

<aui:form action="<%=editArticleActionURL%>"
	cssClass="container-fluid-1280" enctype="multipart/form-data"
	method="post" name="fm1" onSubmit="event.preventDefault();">
	<aui:input name="<%=ActionRequest.ACTION_NAME%>" type="hidden" />
	<aui:input name="hideDefaultSuccessMessage" type="hidden"
		value="<%=classNameId == PortalUtil.getClassNameId(DDMStructure.class)%>" />
	<aui:input name="redirect" type="hidden" value="<%=redirect%>" />
	<aui:input name="workflowPageUUID" type="hidden" value="" />
	<aui:input name="portletResource" type="hidden"
		value="<%=portletResource%>" />
	<aui:input name="referringPlid" type="hidden"
		value="<%=referringPlid%>" />
	<aui:input name="referringPortletResource" type="hidden"
		value="<%=referringPortletResource%>" />
	<aui:input name="groupId" type="hidden" value="<%=groupId%>" />
	<aui:input name="privateLayout" type="hidden"
		value="<%=layout.isPrivateLayout()%>" />
	<aui:input name="folderId" type="hidden" value="<%=folderId%>" />
	<aui:input name="classNameId" type="hidden" value="<%=classNameId%>" />
	<aui:input name="classPK" type="hidden" value="<%=classPK%>" />
	<aui:input name="articleId" type="hidden" value="<%=articleId%>" />
	<aui:input name="articleIds" type="hidden"
		value="<%=articleId + JournalPortlet.VERSION_SEPARATOR + version%>" />
	<aui:input name="version" type="hidden"
		value="<%=((article == null) || article.isNew()) ? version : article.getVersion()%>" />
	<aui:input name="articleURL" type="hidden"
		value="<%=editArticleRenderURL%>" />
	<aui:input name="changeStructure" type="hidden" />
	<aui:input name="ddmStructureId" type="hidden" />
	<aui:input name="ddmTemplateId" type="hidden" />
	<aui:input name="workflowAction" type="hidden"
		value="<%=String.valueOf(WorkflowConstants.ACTION_SAVE_DRAFT)%>" />
	<aui:input name="embedded" type="hidden" value="true" />

	<%
		DDMFormValues ddmFormValues = journalDisplayContext.getDDMFormValues(ddmStructure);

			Set<Locale> availableLocalesSet = new HashSet<>();

			availableLocalesSet.add(LocaleUtil.fromLanguageId(defaultLanguageId));
			availableLocalesSet.addAll(journalDisplayContext.getAvailableArticleLocales());

			if (ddmFormValues != null) {
				availableLocalesSet.addAll(ddmFormValues.getAvailableLocales());
			}

			Locale[] availableLocales = availableLocalesSet.toArray(new Locale[availableLocalesSet.size()]);
	%>

	<div class="lfr-form-content">
		<liferay-ui:error exception="<%=ArticleContentSizeException.class%>"
			message="you-have-exceeded-the-maximum-web-content-size-allowed" />
		<liferay-ui:error exception="<%=DuplicateFileEntryException.class%>"
			message="a-file-with-that-name-already-exists" />

		<liferay-ui:error exception="<%=FileSizeException.class%>">

			<%
				long fileMaxSize = PrefsPropsUtil.getLong(PropsKeys.DL_FILE_MAX_SIZE);

						if (fileMaxSize == 0) {
							fileMaxSize = PrefsPropsUtil.getLong(PropsKeys.UPLOAD_SERVLET_REQUEST_IMPL_MAX_SIZE);
						}
			%>

			<liferay-ui:message
				arguments="<%=TextFormatter.formatStorageSize(fileMaxSize, locale)%>"
				key="please-enter-a-file-with-a-valid-file-size-no-larger-than-x"
				translateArguments="<%=false%>" />
		</liferay-ui:error>

		<liferay-ui:error exception="<%=LiferayFileItemException.class%>">
			<liferay-ui:message
				arguments="<%=TextFormatter.formatStorageSize(LiferayFileItem.THRESHOLD_SIZE, locale)%>"
				key="please-enter-valid-content-with-valid-content-size-no-larger-than-x"
				translateArguments="<%=false%>" />
		</liferay-ui:error>

		<%-- Hide the version number and workflow status since they're not so useful.
		<c:if test="<%= (article != null) && !article.isNew() && (classNameId == JournalArticleConstants.CLASSNAME_ID_DEFAULT) %>">
			<liferay-frontend:info-bar>
				<aui:workflow-status id="<%= String.valueOf(article.getArticleId()) %>" markupView="lexicon" showHelpMessage="<%= false %>" showIcon="<%= false %>" showLabel="<%= false %>" status="<%= article.getStatus() %>" version="<%= String.valueOf(article.getVersion()) %>" />
			</liferay-frontend:info-bar>
		</c:if>
		--%>

		<%-- HCM Portal only supports Vietnamese language. It's not necessary to display the translation manager.
		<aui:translation-manager availableLocales="<%= availableLocales %>" changeableDefaultLanguage="<%= changeableDefaultLanguage %>" defaultLanguageId="<%= defaultLanguageId %>" id="translationManager" />
		--%>

		<%
			boolean approved = false;
				boolean pending = false;

				long inheritedWorkflowDDMStructuresFolderId = JournalFolderLocalServiceUtil
						.getInheritedWorkflowFolderId(folderId);

				boolean workflowEnabled = WorkflowDefinitionLinkLocalServiceUtil.hasWorkflowDefinitionLink(
						themeDisplay.getCompanyId(), groupId, JournalFolder.class.getName(), folderId,
						ddmStructure.getStructureId())
						|| WorkflowDefinitionLinkLocalServiceUtil.hasWorkflowDefinitionLink(themeDisplay.getCompanyId(),
								groupId, JournalFolder.class.getName(), inheritedWorkflowDDMStructuresFolderId,
								ddmStructure.getStructureId())
						|| WorkflowDefinitionLinkLocalServiceUtil.hasWorkflowDefinitionLink(themeDisplay.getCompanyId(),
								groupId, JournalFolder.class.getName(), inheritedWorkflowDDMStructuresFolderId,
								JournalArticleConstants.DDM_STRUCTURE_ID_ALL);

				if ((article != null) && (version > 0)) {
					approved = article.isApproved();

					if (workflowEnabled) {
						pending = article.isPending();
					}
				}
		%>

		<%-- Hide the workflow notification since it's not so useful.
		<c:if test="<%= classNameId == JournalArticleConstants.CLASSNAME_ID_DEFAULT %>">
			<c:if test="<%= approved %>">
				<div class="alert alert-info">
					<liferay-ui:message key="a-new-version-is-created-automatically-if-this-content-is-modified" />
				</div>
			</c:if>

			<c:if test="<%= pending %>">
				<div class="alert alert-info">
					<liferay-ui:message key="there-is-a-publication-workflow-in-process" />
				</div>
			</c:if>
		</c:if>
		--%>
		<liferay-ui:form-navigator formModelBean="<%=article%>"
			formName="fm1"
			id="<%=FormNavigatorConstants.FORM_NAVIGATOR_ID_JOURNAL%>"
			markupView="lexicon" showButtons="<%=false%>" />
	</div>

	<aui:button-row cssClass="journal-article-button-row">

		<%
			boolean hasSavePermission = false;

					if ((article != null) && !article.isNew()) {
						hasSavePermission = JournalArticlePermission.contains(permissionChecker, article,
								ActionKeys.UPDATE);
					} else {
						hasSavePermission = JournalFolderPermission.contains(permissionChecker, groupId, folderId,
								ActionKeys.ADD_ARTICLE);
					}

					String saveButtonLabel = "save";
					
					//Structure has default values
					if (article != null && article.getId() == 0){
						saveButtonLabel = "save-as-draft";
					}
					
					if ((article == null) || article.isApproved() || article.isDraft() || article.isExpired()
							|| article.isScheduled()) {
						saveButtonLabel = "save-as-draft";
					}

					String publishButtonLabel = "Lưu";

					/* dung.nguyen - 2018/12/28: Always display button label as "save".
					String publishButtonLabel = "publish";
					if (workflowEnabled) {
						publishButtonLabel = "submit-for-publication";
					}
					
					if (classNameId > JournalArticleConstants.CLASSNAME_ID_DEFAULT) {
						publishButtonLabel = "save";
					}
					*/
					
					// Submit and go to workflow button
					List<Role> currentUserRoles =  RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
					boolean hasPublishRole = false;
					String submitBtnLabel = "Lưu & Chuyển kiểm duyệt";
					for(Role role : currentUserRoles){
						if(role.getName().contains("_XuatBan") || role.getName().contains("_QuanTri") 
								|| role.getName().contains("Administrator")){
							hasPublishRole = true;
						}
					}
					if(hasPublishRole){
						submitBtnLabel = "Lưu & Duyệt xuất bản";
					}
		%>

		<c:if test="<%=hasSavePermission%>">
			<%-- Hide the original buttons. Add "fake" buttons to inject our logic before submit. --%>
			<aui:button cssClass="btn btn-hidden"
				data-actionname="<%=Constants.PUBLISH%>" disabled="<%=pending%>"
				name="publishButton" type="submit" value="<%=publishButtonLabel%>" />
			<aui:button cssClass="btn btn-primary btn-min-width-100"
				disabled="<%=pending%>" name="publishButtonFake" type="button"
				value="<%=publishButtonLabel%>" />

			<c:if test="<%=workflowEnabled && redirectWorkflowPage%>">
				<aui:button cssClass="btn btn-primary" disabled="<%=pending%>"
					name="submitAndGotoWorkflowButton" type="button"
					value="<%=submitBtnLabel %>" />
			</c:if>

			<c:if
				test="<%=classNameId == JournalArticleConstants.CLASSNAME_ID_DEFAULT%>">
				<aui:button cssClass="btn btn-hidden"
					data-actionname='<%=((article == null) || Validator.isNull(article.getArticleId()))
									? "addArticle"
									: "updateArticle"%>'
					name="saveButton" primary="<%=false%>" type="submit"
					value="<%=saveButtonLabel%>" />
				<aui:button cssClass="btn btn-default" name="saveButtonFake"
					primary="<%=false%>" type="button" value="<%=saveButtonLabel%>" />
			</c:if>
		</c:if>

		<aui:button href="<%=redirect%>" type="button" id="cancelButton"
			cssClass="btn btn-cancel" value="cancel" />
	</aui:button-row>
</aui:form>

<liferay-portlet:renderURL
	plid="<%=JournalUtil.getPreviewPlid(article, themeDisplay)%>"
	var="previewArticleContentURL"
	windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="mvcPath" value="/preview_article_content.jsp" />

	<c:if test="<%=article != null%>">
		<portlet:param name="groupId"
			value="<%=String.valueOf(article.getGroupId())%>" />
		<portlet:param name="articleId" value="<%=article.getArticleId()%>" />
		<portlet:param name="version"
			value="<%=String.valueOf(article.getVersion())%>" />
		<portlet:param name="ddmTemplateKey"
			value="<%=(ddmTemplate != null) ? ddmTemplate.getTemplateKey() : article.getDDMTemplateKey()%>" />
	</c:if>
</liferay-portlet:renderURL>

<portlet:renderURL var="editArticleURL">
	<portlet:param name="redirect" value="<%=redirect%>" />
	<portlet:param name="mvcPath" value="/edit_article.jsp" />
	<portlet:param name="groupId" value="<%=String.valueOf(groupId)%>" />
	<portlet:param name="articleId" value="<%=articleId%>" />
	<portlet:param name="version" value="<%=String.valueOf(version)%>" />
</portlet:renderURL>

<%-- The div element for displaying modal dialog. --%>
<div id="swt_popup_content"></div>

<!-- Article review modal (Tin tức) -->
<!-- Title - Description - Content (in CKEditor) - Author/Source -->
<div class="modal" id="articleReviewModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="review-title">
			<svg class="lexicon-icon lexicon-icon-angle-left close-review"
				data-dismiss="modal" focusable="false" role="image"
				viewBox="0 0 512 512" style="color: gray;">
				<title>angle-left</title>
			<path class="lexicon-icon-outline"
					d="M114.106 254.607c0.22 6.936 2.972 13.811 8.272 19.11l227.222 227.221c11.026 11.058 28.94 11.058 39.999 0 11.058-11.026 11.058-28.94 0-39.999l-206.333-206.333c0 0 206.333-206.333 206.333-206.333 11.058-11.059 11.058-28.973 0-39.999-11.058-11.059-28.973-11.059-39.999 0l-227.221 227.221c-5.3 5.3-8.052 12.174-8.273 19.111z"></path>
		</svg>
			<span id="articleTitle"></span>
		</div>
		<div class="modal-content review-content">
			<div class="modal-body">
				<div class="article-review-content">
					<div id="articleDescription"></div>
					<div id="articleContent"></div>
					<p style="float: right; font-weight: bold">
						<a target="blank" id="articleSourceUrl"> </a>
					</p>
					<p style="float: right; font-weight: bold; padding-right: 5px;"
						id="articleSource">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- khang.ho: upload zip file modal -->
<div class="modal fade" id=zipUploadModal role="dialog">
	<div class="modal-dialog zip-modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload file zip</h4>
        </div>
        <div class="modal-body">
        	<div class="col-md-12">
        		<div class="col-md-5">
        			File .zip:
        		</div>
        		<div class="col-md-7">
        			<input name="zipImgFile" id="zipImgFile" type="file" accept=".zip" />
        		</div>
        	</div>
        </div>
        <div class="modal-footer">
          <div class="col-md-1">
          	  <button type="button" class="btn btn-default" data-dismiss="modal">
	          	<%=LanguageUtil.format(request, "close",new Object())%>
	          </button>
          </div>
          <div class="col-md-1">
          	  <button type="button" class="btn btn-primary" onclick="uploadZipFile()">
          		<%=LanguageUtil.format(request, "swt-upload",new Object())%>
              </button>
          </div>
          <div class="col-md-10">
	          <div class="progress-bar progress-bar-info progress-bar-striped active progress-upload" role="progressbar" 
	          	aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">
	          		<%=LanguageUtil.format(request, "swt-uploading",new Object())%>
	          </div>
          </div>
        </div>
      </div>
      
    </div>
</div>
<portlet:resourceURL id="uploadZip" var="uploadZip" />
<!-- khang.ho: upload zip file modal -->

<script type="text/javascript">
	/* khang.ho: uncheck images in upload modal if not loaded in CKEditor */
	function checkUnloadedImg(){
		var content = "";
		for (var k in CKEDITOR.instances) {
			if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith("_com_liferay")) {
				continue;
			}
			content = CKEDITOR.instances[k].getData();
		}
		$('.thumb').each(function(index){
			var strTest = "image" + $(this).attr('id');
			if(!content.includes(strTest)){
				$(this).find("input").prop("checked", false);
			}
		});
		 $('.video').each(function(index){
				var strTest = "div" + $(this).attr('id');
				if(!content.includes(strTest)){
					$(this).find("input").prop("checked", false);
				}
		}); 
		$('.file').each(function(index){
				var strTest = "div" + $(this).attr('id');
				if(!content.includes(strTest)){
					$(this).find("input").prop("checked", false);
				}
		}); 
		$('.audio').each(function(index){
				var strTest = "div" + $(this).attr('id');
				if(!content.includes(strTest)){
					$(this).find("input").prop("checked", false);
				}
		}); 
	}
	//Hide orgId input if exist
	if($("input[name*='<portlet:namespace />orgId']").length){
		$("input[name*='<portlet:namespace />orgId']").hide();
		$("input[name*='<portlet:namespace />orgId']").val(<%=String.valueOf(userOrgId)%>);
		$("input[name*='<portlet:namespace />orgId']").parent().closest('div').hide();
	}
	
	/* khang.ho: uploadzip file function */
	function uploadZipFile(){
		var ddmStructureKey = $("#<portlet:namespace />ddmStructureKey").val();
		if ($('#zipImgFile').get(0).files.length == 0) {
			alert('<%=LanguageUtil.format(request, "note-select-file", new Object())%>');
		} else {
			$(".progress-upload").show();
			$.ajaxFileUpload({
				type : "POST", 
				fileElementId : 'zipImgFile',
				dataType : 'json',
				data : {
					ddmStructureKey: ddmStructureKey,
				},
			 	url: '<%= uploadZip %>',
			 	success : function(data) {
			 		alert('<%=LanguageUtil.format(request, "image-uploaded", new Object())%>');
			 		$(".progress-upload").hide();
			 		$("input[name*='<portlet:namespace />imagezip']").val(data.folderDir);
			 	},
			 	error : function(err) {
			 		alert('<%=LanguageUtil.format(request, "failed", new Object())%>');
			 		$(".progress-upload").hide();
					console.log("AJAX error in request: " + err);
				},
			});
		}
	}
</script>
	
<!-- Set global variable de custom js lay duoc gia tri nay ma dung -->
<script type="text/javascript">

	var dmFolderIdInstance = <%=dmFolderId%> ;

</script>

<aui:script use="liferay-portlet-journal">
	new Liferay.Portlet.Journal(
		{
			article: {
				editUrl: '<%= editArticleURL %>',
				id: '<%= (article != null) ? HtmlUtil.escape(articleId) : StringPool.BLANK %>',

				<c:if test="<%= (article != null) && !article.isNew() %>">
					previewUrl: '<%= HtmlUtil.escapeJS(previewArticleContentURL.toString()) %>',
				</c:if>

				title: '<%= (article != null) ? HtmlUtil.escapeJS(article.getTitle(locale)) : StringPool.BLANK %>'
			},
			namespace: '<portlet:namespace />',
			'strings.addTemplate': '<liferay-ui:message key="please-add-a-template-to-render-this-structure" />',
			'strings.saveAsDraftBeforePreview': '<liferay-ui:message key="in-order-to-preview-your-changes,-the-web-content-is-saved-as-a-draft" />'
		}
	);
</aui:script>

<aui:script use="aui-modal, aui-overlay-manager">
	
	AUI().ready(function(){
		var CKEDITOR_MARK_OPEN_TAG = '<mark style="z-index:-1;background-color:yellow">';
		var CKEDITOR_MARK_END_TAG = '</mark>';
		
		var badWords = [];
		<% for (String bw : badWordsList) { %>
			badWords.push('<%= bw %>');
		<% } %>
		
		// khang.ho: moment date format
		var formats = [
			moment.ISO_8601,
			"DD/MM/YYYY HH:mm"
		];
		
		//Custom button select related asset
		if ($(".select-existing-selector").length == 1){
			var idAssetSelector = "";
			$(".asset-selector").each(function(){
				if($(this).children("a").attr("data-href").includes("<%=ddmStructure.getStructureId()%>")){
					idAssetSelector = $(this).children("a").attr("id");
				}
			});
			var customBtn = '<div class="btn-group lfr-icon-menu"><a target="_self" id="fakeAssetSelectorBtn" class="btn btn-default" title="Chọn"><span class="lfr-icon-menu-text">Chọn</span></a></div>';
			$(".select-existing-selector").append(customBtn);
			$(".select-existing-selector > .dropdown-toggle").hide();
			$("#fakeAssetSelectorBtn").click(function(){
				A.one("#" + idAssetSelector).simulate('click');
			});
		}
		
		if ($("#content .form-builder-field[data-fieldname='code']").length == 1) {
			var code = $("#content .form-builder-field[data-fieldname='code'] input").val().trim();
			$(".form-group.article-content-title input").val(code);
			$("#content .form-builder-field[data-fieldname='code']").css("display", "none");
			
			var titleChildren = $(".form-group.article-content-title label").children();
			$(".form-group.article-content-title label").html("Số ký hiệu ").append(titleChildren);
			
			$("label[for='<portlet:namespace />description']").text("Trích yếu");
		}
		
		//khang.ho: upload zip file button (Bản đồ quy hoạch)
		if($("input[name*='<portlet:namespace />imagezip']").length){
			var htmlButtonAddZip = "<div class='btn btn-primary zip-btn' data-toggle='modal' data-target='#zipUploadModal'>Upload file zip</div>";
			$("#content .form-builder-field[data-fieldname='imagezip']").find("label").first().append(htmlButtonAddZip);
		}
		
		// Hide the language flag in summary textarea.
		if ($("#<%=renderResponse.getNamespace()%>descriptionContentBox .palette-container").length != 0) {
			$("#<%=renderResponse.getNamespace()%>descriptionContentBox .palette-container").css('display', 'none');
		}
		
		// luat.pham: Hide the parentId field.
		if ($("#content .form-builder-field[data-fieldname='parrentId']").length == 1) {
			$(".form-builder-field[data-fieldname='parrentId']").css('display', 'none');
		}
		// nhut.dang : Feature #4058 Update File FeedBack
		<% if (swtDisplayStyle == WebContentDisplayStyle.FEEDBACK.getCode()){ %>
			if ($("#content .form-builder-field[data-fieldname='attachment']").length == 1) {
				<% if(article.getUserId()!= themeDisplay.getUserId()){%>
					$(".form-builder-field[data-fieldname='attachment']").find("button").css('display', 'none');
				<%}%>
			}
		<%}%>
		<!-- START SNV search scores -->
		<% if (swtDisplayStyle == WebContentDisplayStyle.SEARCHSCORES.getCode()){ %>
			 $('label[for="<%=renderResponse.getNamespace()%>title"]').text("Số báo danh");
			 $('label[for="<%=renderResponse.getNamespace()%>description"]').text("Họ tên");
		<%}%>
		<!-- END SNV search scores -->
		
		if ($("#<%=renderResponse.getNamespace()%>indexable").length != 0) {
			// Hide the indexable toggle-switch.
			$("#<%=renderResponse.getNamespace()%>indexable").parent().closest("div").css('display', 'none');
			
			var htmlButtonGroup = "<div class='btn-toolbar' role='toolbar'>";
			
			// Inject the "check bad words" and "clear bad word marks" buttons to form.
			<c:if test="<%= ddlBadWordId != 0 %>">
				htmlButtonGroup += "<div class='btn-toolbar swt-btn-toolbar' role='toolbar'><div class='btn-group mr-2' role='group'><button class='btn btn-primary' id='btnCheckBadWords' type='button'><span class='lfr-btn-label'>Kiểm tra từ xấu</span></button><button class='btn btn-secondary' id='btnClearBadWordMarks' type='button'><span class='lfr-btn-label'>Xóa đánh dấu từ xấu</span></button></div>";
			</c:if>
			
			// Inject the "basic preview" button.
			htmlButtonGroup += "<div class='btn-group mr-2' role='group'><button class='btn btn-primary' id='btnBasicPreview' type='button' data-toggle='modal' data-target='#articleReviewModal' style='display: none'><span class='lfr-btn-label'>Xem trước</span></button></div>";
			
			htmlButtonGroup += "</div>";
			
			$("#<%=renderResponse.getNamespace()%>indexable").parent().closest("div").parent().append(htmlButtonGroup);
		}
		
		// Hide the review date picker.
		if ($("#scheduleContent label[for='<%=renderResponse.getNamespace()%>reviewDate']").length != 0) {
			$("#scheduleContent label[for='<%=renderResponse.getNamespace()%>reviewDate']").parent().css('display', 'none');
		}
		
		// Move the "select related asset" button to before empty message.
		if ($(".select-existing-selector").length == 1 && $("[id$='assetLinksSearchContainerEmptyResultsMessage']").length == 1) {
			$(".select-existing-selector").insertBefore($("[id$='assetLinksSearchContainerEmptyResultsMessage']"));
		}
		
		// Hide the vocabularies which are not belong to this structure.
		var categoryCount = 0;
		var vocabularyPrefix = "";
		<% if (ddmStructure != null) { %>
			vocabularyPrefix = "<%=ddmStructure.getStructureId()%>_";
		<% } %>
		
		<% if (structureFromCategoryName != null) { %>
			vocabularyPrefix = "<%=structureFromCategoryName.getStructureId()%>_";
		<% } %>
		
		if (vocabularyPrefix !== "") {
			$("#metadata .field-content label").each(function(index) {
				var categoryHtml = $(this).html().trim();
				if (categoryHtml.startsWith(vocabularyPrefix)) {
					categoryHtml = categoryHtml.replace(vocabularyPrefix, "");
					$(this).html(categoryHtml);
					categoryCount++;
				} else {
					$(this).parent(".field-content").css('display', 'none');
				}
				
				var categoriesSelector = $(this).next("div.categoriesselector");
				if (categoriesSelector.length !== 0) {
					categoriesSelector.find("button").off("click.swt").on("click.swt", onAssetCategoriesSelectorButtonClicked);
				}
			});
		}
		
		// Hide the metadata tab if there's no vocabularies belong to this structure.
		if (categoryCount == 0) {
			$("#metadata").css("display", "none");
		}
		
		if (A.one("#btnCheckBadWords")) {
			A.one("#btnCheckBadWords").on("click", onBtnCheckBadWordsClick);
		}
		
		if (A.one("#btnClearBadWordMarks")) {
			A.one("#btnClearBadWordMarks").on("click", clearBadWordMarks);
		}
		
		if (A.one("#<%=renderResponse.getNamespace()%>publishButtonFake")) {
			A.one("#<%=renderResponse.getNamespace()%>publishButtonFake").on("click", function() {
				// khang.ho: check if startDate and endDate are valid (if any)
				var startDate = $("input[name*='startDate']" ).val();
				var endDate = $("input[name*='endDate']" ).val();
				if (startDate == '' && endDate == '') {
					doFormSubmit($("#<%=renderResponse.getNamespace()%>publishButton"));
				} else {
					if (startDate != '' && endDate != '') {
						var startHour = $("select[name*='startHour'] option:selected").text();
						var endHour = $("select[name*='endHour'] option:selected").text();
						startDate = startDate + " " + startHour;
						endDate = endDate + " " + endHour;
						var startDateCheck = moment(startDate, formats, true);
						var endDateCheck = moment(endDate, formats, true);
						if (startDateCheck.diff(endDateCheck, 'hours') >= 0) {
							alert('<%=LanguageUtil.format(request, "swt-journal-web-hook.invalid.dates", dummyObj)%>');
						} else {
							doFormSubmit($("#<%=renderResponse.getNamespace()%>publishButton"));
						}
					} else {
						doFormSubmit($("#<%=renderResponse.getNamespace()%>publishButton"));
					}
				}
			});
		}
		
		if (A.one("#<%=renderResponse.getNamespace()%>submitAndGotoWorkflowButton")) {
			A.one("#<%=renderResponse.getNamespace()%>submitAndGotoWorkflowButton").on("click", function(event) {
				$("#<%=renderResponse.getNamespace()%>workflowPageUUID").val("<%=workflowPageUUID%>");
				doFormSubmit($("#<%=renderResponse.getNamespace()%>publishButton"));
			});
		}
		
		if (A.one("#<%=renderResponse.getNamespace()%>saveButtonFake")) {
			A.one("#<%=renderResponse.getNamespace()%>saveButtonFake").on("click", function(event) {
				//khang.ho: check if startDate and endDate are valid (if any)
				var startDate = $("input[name*='startDate']" ).val();
				var endDate = $("input[name*='endDate']" ).val();
				if (startDate == '' && endDate == '') {
					doFormSubmit($("#<%=renderResponse.getNamespace()%>saveButton"));
				} else {
					if (startDate != '' && endDate != '') {
						var startHour = $("select[name*='startHour'] option:selected").text();
						var endHour = $("select[name*='endHour'] option:selected").text();
						startDate = startDate + " " + startHour;
						endDate = endDate + " " + endHour;
						var startDateCheck = moment(startDate, formats, true);
						var endDateCheck = moment(endDate, formats, true);
						if (startDateCheck.diff(endDateCheck, 'hours') >= 0) {
							alert('<%=LanguageUtil.format(request, "swt-journal-web-hook.invalid.dates", dummyObj)%>');
						} else {
							doFormSubmit($("#<%=renderResponse.getNamespace()%>saveButton"));
						}
					} else {
						doFormSubmit($("#<%=renderResponse.getNamespace()%>saveButton"));
					}
				}
			});
		}
		
		if (A.one("button[id$='btnBasicPreview']")) {
			A.one("button[id$='btnBasicPreview']").on("click", onBtnBasicPreviewClick);
		}
		
		waitForElementToDisplay("#content", document, 100, onContentSectionDisplayed);
		
		waitForElementToDisplay("#relatedAssets", document, 100, onRelatedAssetsSectionDisplayed);
	});
	
	/**
	 * Wait for the element to display.
	 *
	 * @param {string} selector - The element selector text.
	 * @param {object} context - The element selector context.
	 * @param {number} time - The interval (in miliseconds) of checking loop.
	 * @param {function} callback - The callback function invoked when the element gets displayed.
	 */
	function waitForElementToDisplay(selector, context, time, callback) {
        if ($(selector, context).length != 0) {
        	callback();
            return;
        } else {
            setTimeout(function() {
                waitForElementToDisplay(selector, context, time, callback);
            }, time);
        }
    }
	
	/**
	 * The click handling method of "check bad words" button.
	 */
	function onBtnCheckBadWordsClick () {
		var message = "<p>Bên dưới là các từ xấu được phát hiện trong bài viết này.</p>";
		var count = 0;
		
		// Check bad words in text input fields.
		var inputs = $("#contentContent input[type='text'], textarea");
		for (var i = 0; i < inputs.length; i++) {
			// Get the label text of input field.
			var labelText = $(inputs[i]).attr('id');
			var label = $("label[for='" + $(inputs[i]).attr('id') + "']");
			if (label.length != 0) {
				labelText = $(label).clone().children().remove().end().text().trim();
			}
			
			// Find bad words in input field.
			var found = findBadWords(inputs[i], badWords);
			if (!found || found.length == 0) {
				continue;
			}
			
			// Mark the bad words found.
			$(inputs[i]).highlightWithinTextarea({
			    highlight: found
			});
			
			count++;
			
			// Add the checking result to popup message.
			message += "<p><b>" + labelText + ":</b></p>";
			var temp = "<p>";
			for (var j = 0 ; j < found.length; j++) {
				if (temp.length != 3) {
					temp += ", ";
				}
				temp += found[j];
			}
			temp += "</p><p></p>";
			message += temp;
		}
		
		// Check bad words occur in CKEditor control.
		if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
			clearBadWordsMarks_CKEditors();
			
			for (var k in CKEDITOR.instances) {
			    if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith("<%=renderResponse.getNamespace()%>")) {
			        continue;
			    }
				
				// Get the label text of CKEditor.
				var labelText = "";
				var label = $("label[for='" + CKEDITOR.instances[k].element.getId() + "']");
				if (label.length !== 0) {
					labelText = $(label).clone().children().remove().end().text().trim();
				}
				
				// Find bad words in CKEditor.
				var text = CKEDITOR.instances[k].getData();
				var found = [];
				for (var i = 0; i < badWords.length; i++) {
					if (text.indexOf(badWords[i]) !== -1) {
						found.push(badWords[i]);
						text = text.replace(badWords[i], CKEDITOR_MARK_OPEN_TAG + badWords[i] + CKEDITOR_MARK_END_TAG);
						count++;
					}
				}
				
				if (!found || found.length == 0) {
					continue;
				}
				
				// Mark the bad words found.
				CKEDITOR.instances[k].editable().setHtml(text);
				
				// Add the checking result to popup message.
				message += "<p><b>" + labelText + ":</b></p>";
				var temp = "<p>";
				for (var j = 0 ; j < found.length; j++) {
					if (temp.length != 3) {
						temp += ", ";
					}
					temp += found[j];
				}
				temp += "</p><p></p>";
				message += temp;
			}
		}
		
		// Show checking result.
		if (count != 0) {
			showPopup("KIỂM TRA TỪ XẤU", message, 480, 360);
		} else {
			showPopup("KIỂM TRA TỪ XẤU", "Không tìm thấy từ xấu xuất hiện trong bài viết.", 480, 180);
		}
	}
	
	/**
	 * Find the bad words occur in given input field.
	 * 
	 * @param {element} input - The input element.
	 * @param {string[]} badWords - The array of bad words.
	 */
	function findBadWords(input, badWords) {
		var found = [];
		
		var text = $(input).val().trim().toLowerCase();
		if (!text) {
			return found;
		}
		
		for (var i = 0; i < badWords.length; i++) {
			if (text.indexOf(badWords[i]) !== -1) {
				found.push(badWords[i]);
			}
		}
		
		return found;
	}

	/**
	 * Show a popup dialog.
	 * 
	 * @param {string} header - The popup title.
	 * @param {string} content - The popup content.
	 * @param {number} width - The popup width.
	 * @param {number} height - The popup width.
	 */
	function showPopup(header, content, width, height) {
		var dialog = new A.Modal({
			headerContent: header,
			bodyContent: content,
			centered: true,
			modal: true,
			width: width,
			height: height,
			render: "#swt_popup_content",
			close: true
		 });
		dialog.render();
	}
	
	/**
	 * Clear the bad word marks attached in elements.
	 */
	function clearBadWordMarks() {
		$("#contentContent input[type='text'], textarea").highlightWithinTextarea("destroy");
		clearBadWordsMarks_CKEditors();
	}
	
	/**
	 * Clear the bad word marks attached in CKEditors.
	 */
	function clearBadWordsMarks_CKEditors() {
		if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
			var regex1 = new RegExp(CKEDITOR_MARK_OPEN_TAG, "g");
			var regex2 = new RegExp(CKEDITOR_MARK_END_TAG, "g");
			
			for (var k in CKEDITOR.instances) {
			    if (CKEDITOR.instances.hasOwnProperty(k) && k.startsWith("_com_liferay")) {
					var text = CKEDITOR.instances[k].getData();
					text = text.replace(regex1, "").replace(regex2, "");
					CKEDITOR.instances[k].editable().setHtml(text);
			    }
			}
		}
	}
	
	/**
	 * The "click" event handling method of "basic preview" button.
	 * 
	 * khang.ho: Show preview content from current inputs.
	 */
	function onBtnBasicPreviewClick() {
		//Get artcile's inputs
		var title = $("#<portlet:namespace />title").val();
		var description = $("#<portlet:namespace />description").val();
		var sourceType = $("input[name*='<portlet:namespace />sourceType']:checked").val();
		var author = $("select[name*='<portlet:namespace />author'] option:selected").text();
		var sourceVal = $("select[name*='<portlet:namespace />source']").val();
		var sourceUrl = $("input[name*='<portlet:namespace />sourceUrl']").val();
		var content = "";
		if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
			for (var k in CKEDITOR.instances) {
				if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith("_com_liferay")) {
					continue;
				}
				
				content = CKEDITOR.instances[k].getData();
			}
		}
		/* Nhut.dang -- Start Bug #4037 -- replace figcaption caption if user doesn't input caption */
		if(content!=""){
			content = replaceCaptionReview(content);
		} 
		/* Nhut.dang -- End Bug #4037 -- replace figcaption caption if user doesn't input caption */
		
		// Load article's inputs into modal
		$("#articleTitle").text(title);
		$("#articleDescription").text(description);
		$("#articleContent").html(content);
		if (sourceType == 1) {
			if (author.endsWith(")") && author.startsWith("(")) {
				$("#articleSource").text("");
				$("#articleSourceUrl").text("");
			} else {
				$("#articleSource").text(author);
				$("#articleSourceUrl").text("");
			}
		} else {
			if (sourceVal != -1) {
				var sourceTxt = $("select[name*='<portlet:namespace />source'] option:selected").text();
				if (sourceUrl != "") {
					$("#articleSourceUrl").text(" " + sourceTxt);
					$("#articleSourceUrl").attr("href",sourceUrl);
					$("#articleSource").text("Nguồn:");
				} else {
					$("#articleSource").text('Nguồn: ' + sourceTxt);
					$("#articleSourceUrl").text("");
				}
			} else {
				if (sourceUrl != ""){
					var paths = sourceUrl.split("\\.");
					var tempUrl = paths[0];
					if (tempUrl.includes("www.")) {
						tempUrl = tempUrl.replace("www.", "");
					}
					if (tempUrl.includes("http://")) {
						tempUrl = tempUrl.replace("http://", "");
					} else if (tempUrl.includes("https://")) {
						tempUrl = tempUrl.replace("https://", "");
					}
					
					var splashIndex = tempUrl.indexOf("/");
					if(splashIndex != -1){
						tempUrl = tempUrl.substring(0, splashIndex);
					}
					$("#articleSourceUrl").text(" " + tempUrl);
					$("#articleSourceUrl").attr("href", sourceUrl)
					$("#articleSource").text("Nguồn:");
				} else {
					$("#articleSource").text("");
					$("#articleSourceUrl").text("");
				}
			}
		}
		
		// Show modal
		$("#articleReviewModal").show();
		
		/*
		Liferay.fire(
			'previewArticle',
			{
				title: '<%= (article != null) ? HtmlUtil.escapeJS(article.getTitle(locale)) : StringPool.BLANK %>',
				uri: '<%= HtmlUtil.escapeJS(previewArticleContentURL.toString()) %>'
			}
		);
		*/
	}

	/**
	 * The "before submit" event handling method of this form.
	 */
	function doFormSubmit(submitBtnSelector){
		// Clear bad word marks before submit if it's CMS style.
		clearBadWordMarks();
		replaceCaptionArticle();
		// Special process for DOCUMENT stype.
		<% if (swtDisplayStyle == WebContentDisplayStyle.DOCUMENT.getCode()) { %>
			if ($("#content .form-builder-field[data-fieldname='code']").length == 1) {
				var title = $(".form-group.article-content-title input").val().trim();
				$("#content .form-builder-field[data-fieldname='code'] input").val(title);
			}
		<% } %>
		
		// Set fullName for ORG_CHART display style
		<% if (swtDisplayStyle == WebContentDisplayStyle.ORG_CHART.getCode()) { %>
		if ($("#content .form-builder-field[data-fieldname='name']").length == 1) {
			var title = $(".form-group.article-content-title input").val().trim();
			$("#content .form-builder-field[data-fieldname='name'] input").val(title);
		}
	<% } %>
		
		// Trigger click event on target button.
		submitBtnSelector.trigger("click");
	}
	/* Nhut.dang -- Start Bug #4037 -- replace figcaption caption if user doesn't input caption */
	function replaceCaptionReview(contentSelector){
		var caption= /<%=LanguageUtil.format(request, "note-image-media", new Object())%>/gi;
		return contentSelector.replace(caption, "");
	   			
	}
	function replaceCaptionArticle(){
		var content = "";
		if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
			for (var k in CKEDITOR.instances) {
				if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith("_com_liferay")) {
					continue;
				}
				
				content = CKEDITOR.instances[k].getData();
				if(content!=""){
					content = replaceCaptionReview(content);
				}
				CKEDITOR.instances[k].editable().setHtml(content);
			}
		}
	}
	/* Nhut.dang -- End Bug #4037 -- replace figcaption caption if user doesn't input caption */
	/**
	 * The "after displayed" event handling method of "content" section.
	 */
	function onContentSectionDisplayed() {
		// Special processing for CMS dislay style.
		<% if (ddmStructure != null) { %>
			<% if (swtDisplayStyle == WebContentDisplayStyle.CMS.getCode()) { %>
				$("#btnBasicPreview").css("display", "block");
			
				// Hide the view count field if the user doesn't have permission.
				if ($("#content .form-builder-field[data-fieldname='viewCount']").length == 1) {
					$(".form-builder-field[data-fieldname='viewCount']").css('display', 'none');
				}
			
				<% if (article == null || article.isNew()) { %>
					if ($("#content .form-builder-field[data-fieldname='author'] select").length == 1) {
						$("#content .form-builder-field[data-fieldname='author'] select option:eq(0)").prop("selected", true);
					}
					if ($("#content .form-builder-field[data-fieldname='source'] select").length == 1) {
						$("#content .form-builder-field[data-fieldname='source'] select option:eq(0)").prop("selected", true);
					}
				<% } else { %>
					<%
					// Get the AssetEntry object associates with this journal article.
					AssetEntry journalAssetEntry = null;
					try {
						journalAssetEntry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
					} catch (PortalException e) {
						// Do nothing.
					}
					%>
					<% if (hasUpdateViewCountPermission && journalAssetEntry != null) { %>
						if ($("#content .form-builder-field[data-fieldname='viewCount']").length == 1) {
							// Set the view count to DDMForm field.
							$(".form-builder-field[data-fieldname='viewCount'] input").val("<%= journalAssetEntry.getViewCount() %>");
						}
					<% } %>
				<% } %>
				
				// nhut.dang: Upload multiple images.
				if ($("#content .form-builder-field[data-fieldname='content']").length == 1) {
					var htmlButtonImage = "<div onclick='checkUnloadedImg()' class='row row-swt-responsive'><div style='display:flex;'>";
						htmlButtonImage	+= '<img src="/o/hcm-admin-theme/images/cms/icon-image.png" title="Tải lên hình ảnh" data-toggle="modal" data-target="#myModalImage" width="60" height="60" style="cursor: pointer; margin-right: 2%;" class="hvr-shadow-cms">';
						htmlButtonImage	+= '<img src="/o/hcm-admin-theme/images/cms/icon-video.png" title="Tải lên video" data-toggle="modal" data-target="#myModalVideo" width="60" height="60" style="cursor: pointer; margin-right: 2%;" class="hvr-shadow-cms">';
						htmlButtonImage	+= '<img src="/o/hcm-admin-theme/images/cms/icon-file.png" title="Tải lên file" data-toggle="modal" data-target="#myModalFile" width="60" height="60" style="cursor: pointer; margin-right: 2%;" class="hvr-shadow-cms">';
						htmlButtonImage	+= '<img src="/o/hcm-admin-theme/images/cms/icon-audio.png" title="Tải lên audio" data-toggle="modal" data-target="#myModalAudio" width="60" height="60" style="cursor: pointer; margin-right: 2%;" class="hvr-shadow-cms"></div></div>';
					$("#content .form-builder-field[data-fieldname='content']").find("label").first().append(htmlButtonImage);
				}
				
				if ($("#content .form-builder-field[data-fieldname='sourceType']").length == 1
					&& $("#content .form-builder-field[data-fieldname='source']").length == 1
					&& $("#content .form-builder-field[data-fieldname='sourceUrl']").length == 1
					&& $("#content .form-builder-field[data-fieldname='author']").length == 1) {
					
					$("#content .form-builder-field[data-fieldname='sourceType'] span.control-label").css("font-weight", "bold");
					$("#content .form-builder-field[data-fieldname='sourceType'] input").change(onSourceTypeChanged);
					
					$("#content .form-builder-field[data-fieldname='source'] select").change(function() {
						$("#content .form-builder-field[data-fieldname='sourceUrl'] input").val("");
					});
					
					setTimeout(function() {
						onSourceTypeChanged();
					}, 0);
				}
				
				function onSourceTypeChanged() {
					if ($("#content .form-builder-field[data-fieldname='sourceType'] input[value='1']").prop("checked")) {
						$("#content .form-builder-field[data-fieldname='author']").css("display", "inherit");
						$("#content .form-builder-field[data-fieldname='source']").css("display", "none");
						$("#content .form-builder-field[data-fieldname='sourceUrl']").css("display", "none");
					} else {
						$("#content .form-builder-field[data-fieldname='author']").css("display", "none");
						$("#content .form-builder-field[data-fieldname='source']").css("display", "inherit");
						$("#content .form-builder-field[data-fieldname='sourceUrl']").css("display", "inherit");
					}
				}
			<% } else if (swtDisplayStyle == WebContentDisplayStyle.DOCUMENT.getCode()) { %>
				if ($("#content .form-builder-field[data-fieldname='code']").length == 1) {
					var code = $("#content .form-builder-field[data-fieldname='code'] input").val().trim();
					$(".form-group.article-content-title input").val(code);
					$("#content .form-builder-field[data-fieldname='code']").css("display", "none");
					
					var titleChildren = $(".form-group.article-content-title label").children();
					$(".form-group.article-content-title label").html("Số ký hiệu ").append(titleChildren);
					
					$("label[for='<portlet:namespace />description']").text("Trích yếu");
				}
			<% } else if (swtDisplayStyle == WebContentDisplayStyle.QUESTION_ANSWER.getCode()) {  %>
				$("#smallImage").css("display", "none");
				$("#relatedAssets").css("display", "none");
				$("#_com_liferay_journal_web_portlet_JournalPortlet_descriptionBoundingBox").css("display", "none");
				$("label[for='<portlet:namespace />description']").css("display", "none");
			<%}else if(swtDisplayStyle == WebContentDisplayStyle.SCHEDULE.getCode()) {%>
				$("label[for='<portlet:namespace />description']").text("Nội dung");
				$("#relatedAssets").css("display", "none");
			<%}else if(swtDisplayStyle == WebContentDisplayStyle.ORG_CHART.getCode()) {%>
				if ($("#content .form-builder-field[data-fieldname='name']").length == 1) {
					var name = $("#content .form-builder-field[data-fieldname='name'] input").val().trim();
					$(".form-group.article-content-title input").val(name);
					$("#content .form-builder-field[data-fieldname='name']").css("display", "none");
					
					var titleChildren = $(".form-group.article-content-title label").children();
					$(".form-group.article-content-title label").html("Họ tên ").append(titleChildren);
				}
			<%}%>
		<% } %>
		
		// Set default folder paths for CKEditor's upload functions.
		<% if (dmFolderId != 0) { %>
			if (typeof CKEDITOR !== "undefined" && CKEDITOR && CKEDITOR.instances) {
				var folderIdParam = "_com_liferay_item_selector_web_portlet_ItemSelectorPortlet_folderId=<%=dmFolderId%>";
				
				for (var k in CKEDITOR.instances) {
					if (!CKEDITOR.instances.hasOwnProperty(k) || !k.startsWith("_com_liferay")) {
						continue;
					}
					
					CKEDITOR.instances[k].config.filebrowserAudioBrowseLinkUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserAudioBrowseLinkUrl, folderIdParam);
					CKEDITOR.instances[k].config.filebrowserAudioBrowseUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserAudioBrowseUrl, folderIdParam);
					
					CKEDITOR.instances[k].config.filebrowserBrowseUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserBrowseUrl, folderIdParam);
					
					if (CKEDITOR.instances[k].config.filebrowserUploadUrl) {
						CKEDITOR.instances[k].config.filebrowserUploadUrl = 
							addUrlParam(CKEDITOR.instances[k].config.filebrowserUploadUrl, folderIdParam);
					} else if (CKEDITOR.instances[k].config.filebrowserBrowseUrl) {
						CKEDITOR.instances[k].config.filebrowserUploadUrl = 
							addUrlParam(CKEDITOR.instances[k].config.filebrowserBrowseUrl, folderIdParam);
					}
					
					if (CKEDITOR.instances[k].config.filebrowserFlashBrowseUrl) {
						CKEDITOR.instances[k].config.filebrowserFlashBrowseUrl = 
							addUrlParam(CKEDITOR.instances[k].config.filebrowserFlashBrowseUrl, folderIdParam);
					} else if (CKEDITOR.instances[k].config.filebrowserAudioBrowseUrl) {
						CKEDITOR.instances[k].config.filebrowserFlashBrowseUrl = 
							addUrlParam(CKEDITOR.instances[k].config.filebrowserAudioBrowseUrl, folderIdParam);
					}
					
					CKEDITOR.instances[k].config.filebrowserImageBrowseLinkUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserImageBrowseLinkUrl, folderIdParam);
					CKEDITOR.instances[k].config.filebrowserImageBrowseUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserImageBrowseUrl, folderIdParam);
					
					CKEDITOR.instances[k].config.filebrowserVideoBrowseLinkUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserVideoBrowseLinkUrl, folderIdParam);
					CKEDITOR.instances[k].config.filebrowserVideoBrowseUrl = 
						addUrlParam(CKEDITOR.instances[k].config.filebrowserVideoBrowseUrl, folderIdParam);
				}
			}
		<% } %>
	}
	
	/**
	 * Add a parameter to URL.
	 * 
	 * @param {string} url - The original URL.
	 * @param {string} param - The parameter (including its value).
	 *
	 * @return {number} The updated URL or blank.
	 */
	function addUrlParam(url, param) {
		if (url && url.indexOf(param) == -1) {
			return url += "&" + param;
		}
		return url;
	}

	/**
	 * The "after displayed" event handling method of "related assets" section.
	 */
	function onRelatedAssetsSectionDisplayed() {
		// Move the "select" button to the left side of categories list.
		$("#metadataContent div[class*='btn-toolbar-content']").each(function(index) {
			$(this).prev("ul").insertAfter($(this));
		});
		$("#metadataContent div[class*='btn-toolbar-content']").css("float", "left");
		$("#metadataContent div[class*='btn-toolbar-content']").css("margin-right", "5px");
		
		// Update the title/tooltip of "select" buttons to its label text.
		$("div[id*='assetCategoriesSelector']").each(function(index) {
		    var label = $("label[for='" + $(this).parent().attr('id') + "']").text();
			var btn = $(this).find("button");
			if (btn.length != 0) {
				var newTitle = btn.text() + " " + label.trim().toLowerCase();
				btn.attr("title", newTitle);
			}
		});
		
		<% if (swtDisplayStyle == WebContentDisplayStyle.DOCUMENT.getCode()) { %>
			$("#relatedAssetsTitle a").text('<%=LanguageUtil.get(request, "swt-journal-web.relatedAssets.document.title")%>');
		<% } %>
		
		<% if ((swtDisplayStyle != WebContentDisplayStyle.CMS.getCode()) && (swtDisplayStyle != WebContentDisplayStyle.VIDEO.getCode())&& (swtDisplayStyle != WebContentDisplayStyle.EBOOK.getCode())) { %>
			if ($("#smallImage").length != 0) {
				$("#smallImage").css('display', 'none');
			}
		<% } %>
		
		// Bug #3947: Update the date-picker's placeholder text. 
		var placeholder = $("input[placeholder='dd/thángtháng/nămnămnămnăm']");
		$(placeholder).attr("placeholder", "ngày/tháng/năm");
		
		// This line of code should be placed at the end of $.ready() function.
		$("form#<%=renderResponse.getNamespace()%>fm1").css("display", "inherit");
		
		//START Feature #4102 : display button to link video server
		<% if (swtDisplayStyle == WebContentDisplayStyle.VIDEO.getCode()) { %>
			if ($("#content .form-builder-field[data-fieldname='videoUrl']").length == 1) {
				var htmlUrlVideo = '<div class="btn btn-warning" onclick="openVideoServer()"><span class="glyphicon glyphicon-cog"></span>Video server </div>';
				$("#content .form-builder-field[data-fieldname='videoUrl']").append(htmlUrlVideo);
			}
		<% } %>
		//END Feature #4102 : display button to link video server

	}
	/**
	 * The "click" event handling method of asset-categories-selector button.
	 */
	function onAssetCategoriesSelectorButtonClicked() {
		var idStructure = 0;
		<% if (ddmStructure == null) { %>
			return;
		<% } else{ %>
		idStructure = <%=ddmStructure.getStructureId()%>;
		<% } %>
		
		<% if (structureFromCategoryName == null) { %>
			return;
		<% } else{ %>
		idStructure = <%=structureFromCategoryName.getStructureId()%>;
		<% } %>
		
		var str = "div.lfr-categories-selector-list div.tree-view > ul > li > div.tree-node-content > span.tree-label";
		
		if ($(str, window.parent.document).length === 0) {
			waitForElementToDisplay(str, window.parent.document, 500, function() {
				updateCategoryDisplay(idStructure);
			});
		} else {
			setTimeout(function() {
				updateCategoryDisplay(idStructure);
			}, 1000);
		}
	}
	
	/**
	 * Update the display style of category selection popup.
	 */
	function updateCategoryDisplay(structureId) {
		var vocabularyName = "";
		var isBelongToStructure = false;
		
		$("div.lfr-categories-selector-list div.tree-view > ul > li > div.tree-node-content > span.tree-label", window.parent.document).each(function() {
			vocabularyName = $(this).text();
			isBelongToStructure = false;
			
			if (vocabularyName.startsWith(structureId)) {
				// Remove the structure ID from vocabulary name.
				$(this).text(" " + vocabularyName.substring(structureId.toString().length + 1));
				
				// Highlight the vocabulary name.
				$(this).css("font-weight", "bold");
				$(this).css("text-transform", "uppercase");
				
				isBelongToStructure = true;
			}
			
			if (!isBelongToStructure) {
				// Hide the vocabularies not belongs to selected structures.
				$(this).closest("div.lfr-categories-selector-list div.tree-view").css("display", "none");
			}
			
			var categoriesList = $(this).closest("li[id^='vocabulary']").find("ul.tree-container");
			
			if (categoriesList.children().length === 0) {
				// Wait for content gets loaded.
				setTimeout(function() {
					// Show all sub-categories.
					categoriesList.find("span.tree-hitarea.glyphicon-plus").each(function() {
						$(this).trigger("click");
					});
					
					// Update the style of OK icon.
					categoriesList.find("span.glyphicon-ok-sign").css("color", "#5cb85c");
				}, 1000);
			} else {
				// Show all sub-categories.
				categoriesList.find("span.tree-hitarea.glyphicon-plus").each(function() {
					$(this).trigger("click");
				});
				
				// Update the style of OK icon.
				categoriesList.find("span.glyphicon-ok-sign").css("color", "#5cb85c");
			}
		});
	}

</aui:script>
<!-- START Feature #4102 : display button to link video server -->
<% if (swtDisplayStyle == WebContentDisplayStyle.VIDEO.getCode() || swtDisplayStyle == WebContentDisplayStyle.CMS.getCode()) { %>
<% 
String roleAdminHCM = "0";
List<Role> lst_role =  RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
for(Role object : lst_role){
	if(object.getName().equals("RoleAdminHCM")){
		roleAdminHCM = "1";
	}
}
%>
<script>
function openVideoServer(){
	var domainVideoServer = '<%=PropsUtil.get("video.server.url")%>';
	var userId = '<%=themeDisplay.getUserId()%>';
	var groupId = '<%=themeDisplay.getScopeGroupId()%>';
	var companyId = '<%=themeDisplay.getCompanyId() %>';
	var url = domainVideoServer+ "?userId=" + userId + "&groupId=" + groupId + "&companyId="+ companyId + "&role=" + '<%=roleAdminHCM%>';
	var child =	 window.open(url,'newwin', 'width=' + $(window).width() + ',height=' + $(window).height()+'');
	
}
</script>
<%} %>
<!-- END Feature #4102 : display button to link video server -->