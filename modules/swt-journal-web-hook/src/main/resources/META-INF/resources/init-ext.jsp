<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.util.HttpUtil"%>

<%@page import="java.text.SimpleDateFormat"%>

<%
// Get embedded param from request.
boolean embedded = ParamUtil.getBoolean(request, "embedded", false);

if (!embedded) {
	// dung.nguyen (2018/12/01): In some actions, the embedded parameter is ignored
	// after submitted. Thus, I've checked again with the embedded parameter attached
	// in redirect URL.
	if (ParamUtil.getString(request, "redirect").indexOf("_embedded=true") != -1) {
		embedded = true;
	}
}

//Get the display style from preferences.
int swtDisplayStyle = ParamUtil.getInteger(request, "displayStyle", WebContentDisplayStyle.NORMAL.getCode());

// Dummy object used in LanguageUtil.format(...) function.
Object dummyObj = new Object();

// A list of attributes allowed to search/display.
List<AttributeSearchDTO> attributes = new ArrayList<>();

if (embedded) {
	// Remove portlet preferences from current URL to reduce the URL length.
	currentURL = HttpUtil.removeParameter(currentURL, "_com_liferay_journal_web_portlet_JournalPortlet_jsonPrefs");
	
	attributes.add(new AttributeSearchDTO("title", LanguageUtil.format(request, "title", dummyObj), "text"));
	attributes.add(new AttributeSearchDTO("description", LanguageUtil.format(request, "description", dummyObj), "text"));
	attributes.add(new AttributeSearchDTO("createUser", LanguageUtil.format(request, "metadata.DublinCore.CREATOR", dummyObj), "text"));
	attributes.add(new AttributeSearchDTO("createDate", LanguageUtil.format(request, "create-date", dummyObj), "ddm-date"));
	attributes.add(new AttributeSearchDTO("modifiedDate", LanguageUtil.format(request, "modified-date", dummyObj), "ddm-date"));
	attributes.add(new AttributeSearchDTO("displayDate", LanguageUtil.format(request, "display-date", dummyObj), "ddm-date"));
}

// The date formatter in Vietnamese locale.
SimpleDateFormat viDateFormat = new SimpleDateFormat("dd/MM/yyyy");
%>

<% if (embedded) { %>
	<style scoped>
		.swt-btn-toolbar {
			margin-bottom: 15px;
		}
	</style>
<% } %>

<%!
/**
 * The article attributes to be used in search form.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public class AttributeSearchDTO {

	private String name;
	private String label;
	private String type;

	public AttributeSearchDTO(String name, String label, String type) {
		super();
		this.name = name;
		this.label = label;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
%>

<%!
/**
 * The column to be displayed in search result.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public class DisplayColumnDTO implements Comparable<DisplayColumnDTO> {

	// The field data type.
	private String type;
	// The field unique name.
	private String name;
	// The field label.
	private String label;
	// The sequence of field column in search result.
	private int sequence;

	public DisplayColumnDTO(String type, String name, String label, int sequence) {
		super();
		this.type = type;
		this.name = name;
		this.label = label;
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	public int compareTo(DisplayColumnDTO o) {
		int tmp = sequence - o.getSequence();
		if (tmp == 0) {
			tmp = o.getLabel().compareTo(getLabel());
		}
		return tmp;
	}

}
%>

<%!
/**
 * The search criteria to be used in search form.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public class SearchCriteriaDTO implements Comparable<SearchCriteriaDTO> {
	
	// The field data type.
	private String type;
	// The field unique name.
	private String name;
	// The field label.
	private String label;
	// The search type.
	private int searchType;
	// The sequence of field in search form.
	private int sequence;

	public SearchCriteriaDTO(String type, int searchType, String name, String label, int sequence) {
		super();
		this.type = type;
		this.searchType = searchType;
		this.name = name;
		this.label = label;
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	public int compareTo(SearchCriteriaDTO o) {
		int tmp = sequence - o.getSequence();
		if (tmp == 0) {
			tmp = o.getLabel().compareTo(getLabel());
		}
		return tmp;
	}

}
%>

<%!
public enum WebContentDisplayStyle {
	NORMAL(1), CMS(2), DOCUMENT(3), QUESTION_ANSWER(4), VIDEO(5), IMAGE(6), SCHEDULE(7), EBOOK(8), ORG_CHART(9),FEEDBACK(10),SEARCHSCORES(11);

	private final int code;

	private WebContentDisplayStyle(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static WebContentDisplayStyle toWebContentDisplayStyle(int code) {
		for (WebContentDisplayStyle me : WebContentDisplayStyle.values()) {
			if (me.getCode() == code) {
				return me;
			}
		}
		return null;
	}
}
%>