<%@ include file="/init.jsp" %>

<c:choose>
	<c:when test="<%= embedded %>">
		<%@ include file="/view_custom.jsp" %>
	</c:when>
	<c:otherwise>
		<%@ include file="/view_origin.jsp" %>
	</c:otherwise>
</c:choose>