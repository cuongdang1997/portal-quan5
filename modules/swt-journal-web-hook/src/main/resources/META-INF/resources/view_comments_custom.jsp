<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

<%
//String displayStyle = journalDisplayContext.getDisplayStyle();
%>

<style scoped>
	div[id$='_infoPanelId'] {
		padding: 0 !important;
	}
	
	.searchcontainer-content table thead tr {
		background-color: #F5F8FA;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
	}
	
	.searchcontainer-content table {
		margin-bottom: 0px !important;
	}
	
	.searchcontainer-content table th {
		font-weight: bold;
		text-transform : uppercase;
	}
	
	tr.splitter {
		display: none;
	}
	
	.pagination-items-per-page {
		margin-left: 15px;
	}
	
	.dialog-iframe-bd {
		padding: unset;
	}
</style>

<liferay-ui:search-container
	emptyResultsMessage="no-comment-was-found"
	searchContainer="<%= journalDisplayContext.getCommentsSearchContainer() %>"
	cssClass="table-bordered"
>
	<liferay-ui:search-container-row
		className="com.liferay.message.boards.kernel.model.MBMessage"
		cssClass="entry-display-style selectable"
		modelVar="mbMessage"
	>

		<%
		//User userDisplay = UserLocalServiceUtil.fetchUserById(mbMessage.getUserId());

		boolean translate = mbMessage.isFormatBBCode();

		String content = mbMessage.getBody(translate);
		%>
		
		<liferay-ui:search-container-column-text
			name="author"
			property="userName"
			truncate="<%= true %>"
		/>

		<liferay-ui:search-container-column-text
			name="message"
			truncate="<%= true %>"
			value="<%= HtmlUtil.extractText(content) %>"
		/>

		<liferay-ui:search-container-column-date
			name="modified-date"
			property="modifiedDate"
		/>

		<%-- dung.nguyen: We only support the "list" display style.
		<c:choose>
			<c:when test='<%= displayStyle.equals("descriptive") %>'>
				<liferay-ui:search-container-column-image
					src="<%= (userDisplay != null) ? userDisplay.getPortraitURL(themeDisplay) : UserConstants.getPortraitURL(themeDisplay.getPathImage(), true, 0, null) %>"
					toggleRowChecker="<%= false %>"
				/>

				<liferay-ui:search-container-column-text
					colspan="<%= 2 %>"
				>
					<h6 class="text-default">
						<%= HtmlUtil.extractText(content) %>
					</h6>

					<h6 class="text-default">
						<strong><liferay-ui:message key="last-updated" />:</strong>

						<liferay-ui:message arguments="<%= new String[] {LanguageUtil.getTimeDescription(locale, System.currentTimeMillis() - mbMessage.getModifiedDate().getTime(), true), HtmlUtil.escape(mbMessage.getUserName())} %>" key="x-ago-by-x" translateArguments="<%= false %>" />
					</h6>
				</liferay-ui:search-container-column-text>
			</c:when>
			<c:when test='<%= displayStyle.equals("icon") %>'>

				<%
				row.setCssClass("entry-card lfr-asset-item");
				%>

				<liferay-ui:search-container-column-text>
					<liferay-frontend:vertical-card
						cssClass="entry-display-style"
						imageUrl="<%= (userDisplay != null) ? userDisplay.getPortraitURL(themeDisplay) : UserConstants.getPortraitURL(themeDisplay.getPathImage(), true, 0, null) %>"
						resultRow="<%= row %>"
					>
						<liferay-frontend:vertical-card-header>
							<liferay-ui:message arguments="<%= new String[] {LanguageUtil.getTimeDescription(locale, System.currentTimeMillis() - mbMessage.getModifiedDate().getTime(), true), HtmlUtil.escape(mbMessage.getUserName())} %>" key="x-ago-by-x" translateArguments="<%= false %>" />
						</liferay-frontend:vertical-card-header>

						<liferay-frontend:vertical-card-footer>
							<%= HtmlUtil.extractText(content) %>
						</liferay-frontend:vertical-card-footer>
					</liferay-frontend:vertical-card>
				</liferay-ui:search-container-column-text>
			</c:when>
			<c:otherwise>
				<liferay-ui:search-container-column-text
					name="author"
					property="userName"
					truncate="<%= true %>"
				/>

				<liferay-ui:search-container-column-text
					name="message"
					truncate="<%= true %>"
					value="<%= HtmlUtil.extractText(content) %>"
				/>

				<liferay-ui:search-container-column-date
					name="modified-date"
					property="modifiedDate"
				/>
			</c:otherwise>
		</c:choose>
		--%>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator
		displayStyle="list"
		markupView="lexicon"
	/>
</liferay-ui:search-container>