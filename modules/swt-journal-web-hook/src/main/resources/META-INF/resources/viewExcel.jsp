<link rel="stylesheet" href="<%=request.getContextPath()%>/css/tabulator.css">
<script  type="text/javascript"  src="<%=request.getContextPath()%>/js/tabulator.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>
<script  type="text/javascript"  src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>
<style>
.class-uploadFile{
width: 100%;
float: left;
margin: 10px 0px;
}
.swt-label-name{
    margin-top: 10px;
}
</style>
<%
String structureKeys = ParamUtil.getString(request, "ddmStructureKey");
long classNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(), 
		classNameId, structureKeys);
String nameVocab = structure.getStructureId() + "_";
List<AssetVocabulary> assetVocab = new ArrayList<AssetVocabulary>();
List<AssetCategory> listCategories =  new ArrayList<>();
BaseModelSearchResult<AssetVocabulary> baseAssetVacab = AssetVocabularyLocalServiceUtil.searchVocabularies(
		themeDisplay.getCompanyId(), scopeGroupId, nameVocab, 0, Byte.MAX_VALUE);
assetVocab = baseAssetVacab.getBaseModels();

if(assetVocab.size()!=0){
	AssetVocabulary vocabularyObject = AssetVocabularyLocalServiceUtil.getAssetVocabulary(assetVocab.get(0).getVocabularyId());
	 listCategories = vocabularyObject.getCategories();
}
%>
 <div class="modal fade" id="myModalImportScores" role="dialog" style="display:none">
	<div class="modal-dialog modal-position" style="width:100%;">
	<div  id="success-import-file-action" style="display:none;">
		  <div class="alert alert-success alert-dismissible fade in" id="success-import-file-message">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  </div>
	</div>
		<div class="modal-content">
			<div class="modal-header backgroud-header">
				<button type="button" class="close close-header" data-dismiss="modal" id="btnCloseMyModal1">&times;</button>
				<h3 class="modal-title modal-title-header"><%=LanguageUtil.format(request, "swt-import-file-score",new Object())%></h3>
			</div>
			<div class="modal-body">
			<div class="row class-uploadFile">
									
							<div  class="col-xs-1 swt-label-name">
								<%=LanguageUtil.format(request, "swt-dot-thi",new Object())%>
							</div>
							<div class="col-xs-11">
							<%if (listCategories.size() != 0) { %>
								<aui:select name="categoryId" label="">
									<% 
										for (AssetCategory a : listCategories) { %>
										<aui:option value='<%=a.getCategoryId()%>' label='<%=a.getName()%>' />
									<%
										}
									%>
								</aui:select>
								<%}  %>
							</div>
						</div>
				<div class="row class-uploadFile">
							<div  class="col-xs-1 swt-label-name"><%=LanguageUtil.format(request, "swt-import-file-score", new Object())%></div>
							<div  class="col-xs-11">
								<input class="field form-control" style="max-width: 100%;"  type="file" id="fileScore" name="fileScore" accept=".xls,.xlsx">
							</div>
				
				</div>
				<div class="class-uploadFile" id="progress-import-file"></div> 
				<div id="example-table"></div>
			</div>
		</div>

	</div>

</div>
<liferay-portlet:resourceURL id="importDataScores" var="importDataScores" copyCurrentRenderParameters="false"/>
<script>
var ExcelToJSON = function() {
    this.parseExcel = function(file) {
    var reader = new FileReader();

      reader.onload = function(e) {
        var data = e.target.result;
        var workbook = XLSX.read(data, {
          type: 'binary'
        });
        workbook.SheetNames.forEach(function(sheetName) {
          // Here is your object
          var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          var json_object = JSON.stringify(XL_row_object);
         
          		var table = new Tabulator("#example-table", {
          		    data:JSON.parse(json_object),
          		    autoColumns:true,
	          		layout:"fitColumns",
	          	    pagination:"local",
	          	    paginationSize:10,
	          	    paginationSizeSelector:[5, 10, 15, 20],
          		});
     $("#example-table").append( '<button class="btn btn-primary" value="OK" onclick="importScores()">'+'<%=LanguageUtil.format(request, "import-data-scores", new Object())%>'+'</button>');
        })
      };

      reader.onerror = function(ex) {
        console.log(ex);
      };

     reader.readAsBinaryString(file);

    };
};
 $("#fileScore").change(function(evt){
	 var files = evt.target.files; // FileList object
	    var xl2json = new ExcelToJSON();
		var data = xl2json.parseExcel(files[0]);
}); 
function importScores(){
	var folderId = $("#<portlet:namespace />folderId").val();
	var categoryId = $("#<portlet:namespace />categoryId").val();
	var htmlBar = '<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">' ;
	htmlBar += '<%=LanguageUtil.format(request, "note-loading", new Object())%>';
	htmlBar	+= '</div></div>';
	$('#progress-import-file').append(htmlBar);
	$.ajaxFileUpload({
		  type: "POST",
		  fileElementId : 'fileScore',
		  url: '<%=importDataScores%>',
		  data:{
			  ddmStructureKey:'<%=structureKeys%>',
			  folderId:folderId,
			  categoryId:categoryId,
		  },
		  dataType: 'json',
		  success: function(data){
				$("#progress-import-file").empty();
				var message = '<%=LanguageUtil.format(request, "insert-success", new Object())%>';
				$("#success-import-file-message").empty();
				$("#success-import-file-message").append(message);
				$("#success-import-file-action").show();
				$("#success-import-file-message").fadeOut(5000);
				if (data["success"] ==true){
					window.location.reload(true);
				}
		 }
	});
}
</script> 