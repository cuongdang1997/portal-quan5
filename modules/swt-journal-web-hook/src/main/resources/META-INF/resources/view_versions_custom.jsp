<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@ include file="/init.jsp" %>

<%
SearchContainer articleSearchContainer = journalDisplayContext.getSearchContainer(true);

//String displayStyle = journalDisplayContext.getDisplayStyle();

String searchContainerId = ParamUtil.getString(request, "searchContainerId");

String referringPortletResource = ParamUtil.getString(request, "referringPortletResource");
%>

<style scoped>
	div[id$='_infoPanelId'] {
		padding: 0 !important;
	}
	
	.searchcontainer-content table thead tr {
		background-color: #F5F8FA;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
	}
	
	.searchcontainer-content table {
		margin-bottom: 0px !important;
	}
	
	.searchcontainer-content table th {
		font-weight: bold;
		text-transform : uppercase;
	}
	
	tr.splitter {
		display: none;
	}
	
	.pagination-items-per-page {
		margin-left: 15px;
	}
	
	.dialog-iframe-bd {
		padding: unset;
	}
</style>

<liferay-ui:search-container
	emptyResultsMessage="no-web-content-was-found"
	id="<%= searchContainerId %>"
	searchContainer="<%= articleSearchContainer %>"
	cssClass="table-bordered"
>
	<liferay-ui:search-container-row
		className="com.liferay.journal.model.JournalArticle"
		modelVar="articleVersion"
	>

		<%
		row.setPrimaryKey(articleVersion.getArticleId() + JournalPortlet.VERSION_SEPARATOR + articleVersion.getVersion());
		
		boolean latestVersion = JournalArticleLocalServiceUtil.isLatestVersion(articleVersion.getGroupId(), articleVersion.getArticleId(), articleVersion.getVersion());
		
		PortletURL rowURL = null;
		
		if (latestVersion && JournalArticlePermission.contains(permissionChecker, articleVersion, ActionKeys.UPDATE)) {
			rowURL = liferayPortletResponse.createRenderURL();

			rowURL.setParameter("mvcPath", "/edit_article.jsp");
			rowURL.setParameter("redirect", currentURL);
			rowURL.setParameter("referringPortletResource", referringPortletResource);
			rowURL.setParameter("groupId", String.valueOf(articleVersion.getGroupId()));
			rowURL.setParameter("folderId", String.valueOf(articleVersion.getFolderId()));
			rowURL.setParameter("articleId", articleVersion.getArticleId());
			rowURL.setParameter("version", String.valueOf(articleVersion.getVersion()));
		}
		%>
		
		<liferay-ui:search-container-column-text
			name="id"
			value="<%= HtmlUtil.escape(articleVersion.getArticleId()) %>"
		/>

		<% if (rowURL != null) { %>
			<liferay-ui:search-container-column-text
				cssClass="table-cell-content"
				name="title"
				value="<%= HtmlUtil.escape(articleVersion.getTitle(locale)) %>"
				href="<%= rowURL %>"
			/>
		<% } else { %>
			<liferay-ui:search-container-column-text
				cssClass="table-cell-content"
				name="title"
				value="<%= HtmlUtil.escape(articleVersion.getTitle(locale)) %>"
			/>
		<% } %>

		<liferay-ui:search-container-column-text
			name="version"
			orderable="<%= true %>"
		/>

		<liferay-ui:search-container-column-status
			name="status"
		/>

		<liferay-ui:search-container-column-date
			name="modified-date"
			orderable="<%= true %>"
			property="modifiedDate"
		/>

		<c:if test="<%= articleVersion.getDisplayDate() != null %>">
			<liferay-ui:search-container-column-date
				name="display-date"
				orderable="<%= true %>"
				property="displayDate"
			/>
		</c:if>

		<liferay-ui:search-container-column-text
			name="author"
			value="<%= HtmlUtil.escape(PortalUtil.getUserName(articleVersion)) %>"
		/>

		<liferay-ui:search-container-column-jsp
			path="/article_version_action.jsp"
		/>

		<%-- dung.nguyen: We only support the "list" display style.
		<c:choose>
			<c:when test='<%= displayStyle.equals("descriptive") %>'>
				<liferay-ui:search-container-column-text>
					<liferay-ui:user-portrait
						cssClass="user-icon-lg"
						userId="<%= articleVersion.getUserId() %>"
					/>
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text
					colspan="<%= 2 %>"
				>

					<%
					Date createDate = articleVersion.getModifiedDate();

					String modifiedDateDescription = LanguageUtil.getTimeDescription(request, System.currentTimeMillis() - createDate.getTime(), true);
					%>

					<h6 class="text-default">
						<liferay-ui:message arguments="<%= new String[] {HtmlUtil.escape(articleVersion.getUserName()), modifiedDateDescription} %>" key="x-modified-x-ago" />
					</h6>

					<h5>
						<%= HtmlUtil.escape(articleVersion.getTitle(locale)) %>
					</h5>

					<h6 class="text-default">
						<aui:workflow-status markupView="lexicon" showHelpMessage="<%= false %>" showIcon="<%= false %>" showLabel="<%= false %>" status="<%= articleVersion.getStatus() %>" version="<%= String.valueOf(articleVersion.getVersion()) %>" />
					</h6>
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-jsp
					path="/article_version_action.jsp"
				/>
			</c:when>
			<c:when test='<%= displayStyle.equals("icon") %>'>

				<%
				row.setCssClass("entry-card lfr-asset-item");
				%>

				<liferay-ui:search-container-column-text>

					<%
					String articleImageURL = articleVersion.getArticleImageURL(themeDisplay);
					%>

					<c:choose>
						<c:when test="<%= Validator.isNotNull(articleImageURL) %>">
							<liferay-frontend:vertical-card
								actionJsp="/article_version_action.jsp"
								actionJspServletContext="<%= application %>"
								imageUrl="<%= articleImageURL %>"
								resultRow="<%= row %>"
								rowChecker="<%= searchContainer.getRowChecker() %>"
								title="<%= articleVersion.getTitle(locale) %>"
							>
								<%@ include file="/article_version_vertical_card.jspf" %>
							</liferay-frontend:vertical-card>
						</c:when>
						<c:otherwise>
							<liferay-frontend:icon-vertical-card
								actionJsp="/article_version_action.jsp"
								actionJspServletContext="<%= application %>"
								icon="web-content"
								resultRow="<%= row %>"
								rowChecker="<%= searchContainer.getRowChecker() %>"
								title="<%= articleVersion.getTitle(locale) %>"
							>
								<%@ include file="/article_version_vertical_card.jspf" %>
							</liferay-frontend:icon-vertical-card>
						</c:otherwise>
					</c:choose>
				</liferay-ui:search-container-column-text>
			</c:when>
			<c:when test='<%= displayStyle.equals("list") %>'>
				<liferay-ui:search-container-column-text
					name="id"
					value="<%= HtmlUtil.escape(articleVersion.getArticleId()) %>"
				/>

				<liferay-ui:search-container-column-text
					cssClass="table-cell-content"
					name="title"
					value="<%= HtmlUtil.escape(articleVersion.getTitle(locale)) %>"
				/>

				<liferay-ui:search-container-column-text
					name="version"
					orderable="<%= true %>"
				/>

				<liferay-ui:search-container-column-status
					name="status"
				/>

				<liferay-ui:search-container-column-date
					name="modified-date"
					orderable="<%= true %>"
					property="modifiedDate"
				/>

				<c:if test="<%= articleVersion.getDisplayDate() != null %>">
					<liferay-ui:search-container-column-date
						name="display-date"
						orderable="<%= true %>"
						property="displayDate"
					/>
				</c:if>

				<liferay-ui:search-container-column-text
					name="author"
					value="<%= HtmlUtil.escape(PortalUtil.getUserName(articleVersion)) %>"
				/>

				<liferay-ui:search-container-column-jsp
					path="/article_version_action.jsp"
				/>
			</c:when>
		</c:choose>
		--%>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator
		displayStyle="list"
		markupView="lexicon"
		resultRowSplitter="<%= journalDisplayContext.isSearch() ? null : new JournalResultRowSplitter() %>"
		searchContainer="<%= articleSearchContainer %>"
	/>
</liferay-ui:search-container>

<br />
<p><i>Bạn chỉ được phép thực hiện thay đổi trên các phiên bản mới nhất.</i></i></p>
