<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@ include file="/init.jsp" %>

<%
long groupId = ParamUtil.getLong(request, "groupId");
String articleId = ParamUtil.getString(request, "articleId");
double sourceVersion = ParamUtil.getDouble(request, "sourceVersion");
String displayStyle = ParamUtil.getString(request, "displayStyle", "list");
String eventName = ParamUtil.getString(request, "eventName", renderResponse.getNamespace() + "selectVersionFm");

PortletURL portletURL = renderResponse.createRenderURL();

portletURL.setParameter("mvcPath", "/select_version_custom.jsp");
portletURL.setParameter("redirect", currentURL);
portletURL.setParameter("groupId", String.valueOf(groupId));
portletURL.setParameter("articleId", articleId);
portletURL.setParameter("sourceVersion", String.valueOf(sourceVersion));
%>

<style scoped>
	.lfr-search-container-wrapper {
		margin-top: 15px;
	}

	.searchcontainer-content table {
		margin-bottom: 0px !important;
	}
	
	.searchcontainer-content table thead tr {
		background-color: #F5F8FA;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
	}
	
	.searchcontainer-content table th {
		font-weight: bold;
		text-transform : uppercase;
	}
</style>

<%-- Remove the tab panel to reduce form size.
<aui:nav-bar markupView="lexicon">
	<aui:nav cssClass="navbar-nav">
		<aui:nav-item label="versions" selected="<%= true %>" />
	</aui:nav>
</aui:nav-bar>
--%>

<%-- Remove the filter section since it only has one option.
Remove the display styles selection since we just support "list" style.
<liferay-frontend:management-bar>
	<liferay-frontend:management-bar-buttons>
		<liferay-frontend:management-bar-filters>
			<liferay-frontend:management-bar-navigation
				navigationKeys='<%= new String[] {"all"} %>'
				portletURL="<%= PortletURLUtil.clone(portletURL, liferayPortletResponse) %>"
			/>
		</liferay-frontend:management-bar-filters>

		<liferay-frontend:management-bar-display-buttons
			displayViews='<%= new String[] {"list"} %>'
			portletURL="<%= PortletURLUtil.clone(portletURL, liferayPortletResponse) %>"
			selectedDisplayStyle="<%= displayStyle %>"
		/>
	</liferay-frontend:management-bar-buttons>
</liferay-frontend:management-bar>
--%>

<aui:form action="<%= portletURL.toString() %>" cssClass="container-fluid-1280" method="post" name="selectVersionFm">
	<br />
	<p>Click vào mã phiên bản để tiến hành so sánh.</p>
	
	<liferay-ui:search-container
		cssClass="table-bordered"
		emptyResultsMessage="there-are-no-results"
		iteratorURL="<%= portletURL %>"
		total="<%= JournalArticleLocalServiceUtil.getArticlesCount(groupId, articleId) %>"
	>
		<%
		List<JournalArticle> temp = JournalArticleLocalServiceUtil.getArticles(groupId, articleId, searchContainer.getStart(), searchContainer.getEnd(), new ArticleVersionComparator());
		List<JournalArticle> results = new ArrayList<>();
		
		// Exclude the source version from results list.
		for (JournalArticle t : temp) {
			if (t.getVersion() != sourceVersion) {
				results.add(t);
			}
		}
		
		searchContainer.setResults(results);
		%>

		<liferay-ui:search-container-row
			className="com.liferay.journal.model.JournalArticle"
			modelVar="curArticle"
		>
			<liferay-ui:search-container-column-text
				name="version"
			>

				<%
				double curSourceVersion = sourceVersion;
				double curTargetVersion = curArticle.getVersion();

				if (curTargetVersion < curSourceVersion) {
					double tempVersion = curTargetVersion;

					curTargetVersion = curSourceVersion;
					curSourceVersion = tempVersion;
				}

				Map<String, Object> data = new HashMap<String, Object>();

				data.put("sourceversion", curSourceVersion);
				data.put("targetversion", curTargetVersion);
				%>

				<aui:a cssClass="selector-button" data="<%= data %>" href="javascript:;">
					<%= String.valueOf(curArticle.getVersion()) %>
				</aui:a>
			</liferay-ui:search-container-column-text>
			
			<liferay-ui:search-container-column-status
				name="status"
			/>
			
			<liferay-ui:search-container-column-text
				name="author"
				value="<%= HtmlUtil.escape(PortalUtil.getUserName(curArticle)) %>"
			/>

			<liferay-ui:search-container-column-text
				name="modified-date"
				value="<%= viDateFormat.format(curArticle.getModifiedDate()) %>"
			/>
			
			<c:if test="<%= curArticle.getDisplayDate() != null %>">
				<liferay-ui:search-container-column-text
					name="display-date"
					value="<%= viDateFormat.format(curArticle.getDisplayDate()) %>"
				/>
			</c:if>
		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator
			markupView="lexicon"
		/>
	</liferay-ui:search-container>
</aui:form>

<aui:script>
	Liferay.Util.selectEntityHandler('#<portlet:namespace />selectVersionFm', '<%= HtmlUtil.escapeJS(eventName) %>');
</aui:script>