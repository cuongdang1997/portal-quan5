<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.swt.portal.journal.web.util.JournalWebUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.comparator.WorkflowComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowLogManagerUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowLog"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>

<%@page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.lists.model.DDLRecord"%>
<%@page import="com.liferay.dynamic.data.lists.service.DDLRecordLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.model.Value"%>
<%@page import="com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue"%>

<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.search.BaseModelSearchResult"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowTask"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowTaskManagerUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>

<%@page import="java.io.StringReader"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.Map"%>

<%@ include file="/init.jsp" %>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.1/jszip.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip-utils/0.0.2/jszip-utils.js"></script>

<%
String referringPortletResource = ParamUtil.getString(request, "referringPortletResource");

SearchContainer articleSearchContainer = journalDisplayContext.getSearchContainer(false);

String searchContainerId = ParamUtil.getString(request, "searchContainerId");

// Get the DDMStructure key from request.
String structureKey = ParamUtil.getString(request, "ddmStructureKey");

// Get the enableReceiver key from request.
boolean enableReceiver = ParamUtil.getBoolean(request, "enableReceiver");

// Parse portlet preferences to JSON object.
String jsonPrefs = ParamUtil.getString(request, "jsonPrefs");
JSONObject prefs = null;
if (jsonPrefs == null || jsonPrefs.length() == 0) {
	prefs = JSONFactoryUtil.createJSONObject();
} else {
	prefs = JSONFactoryUtil.createJSONObject(jsonPrefs);
}

// The list of structure fields.
List<DDMFormField> fields = new ArrayList<>();
long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
DDMStructure ddmStructure = null;
try {
	ddmStructure = DDMStructureLocalServiceUtil.getStructure(scopeGroupId, journalArticleClassNameId,
			structureKey);
} catch (PortalException e) {
	e.printStackTrace();
} catch (SystemException e) {
	e.printStackTrace();
}
if (ddmStructure != null) {
	// Get the fields from selected structure.
	fields = ddmStructure.getDDMFormFields(false);
}

// The list of vocabularies which name is prefixed with structure ID.
List<AssetVocabulary> vocabularies = new ArrayList<>();
String vocabularyPrefix = StringPool.BLANK;
vocabularyPrefix = ddmStructure.getStructureId() + "_";
try {
	BaseModelSearchResult<AssetVocabulary> tmp = AssetVocabularyLocalServiceUtil.searchVocabularies(
			themeDisplay.getCompanyId(), scopeGroupId, vocabularyPrefix, 0, Byte.MAX_VALUE);
	vocabularies = tmp.getBaseModels();
} catch (PortalException e) {
	e.printStackTrace();
}

// The list of columns to be displayed in result form.
List<DisplayColumnDTO> displayColumns = new ArrayList<>();

// Temporary variables used for checking search/display columns.
boolean displayable = false;
int displaySequence = -1;

// Columns from article attributes. 
for (AttributeSearchDTO attr : attributes) {
	displayable = prefs.getBoolean("A" + attr.getName() + "d", false);
	if (displayable) {
		displaySequence = prefs.getInt("A" + attr.getName() + "ds", 0);
		DisplayColumnDTO dc = new DisplayColumnDTO("attribute", attr.getName(), attr.getLabel(), displaySequence);
		displayColumns.add(dc);
	}
}

// Additional columns from selected structure fields.
for (DDMFormField f : fields) {
	displayable = prefs.getBoolean("S" + f.getName() + "d", false);
	if (displayable) {
		displaySequence = prefs.getInt("S" + f.getName() + "ds", 0);
		DisplayColumnDTO dc = new DisplayColumnDTO(f.getType(), f.getName(), f.getLabel().getString(locale), displaySequence);
		displayColumns.add(dc);
	}
}

// Additional columns from selected vocabularies.
for (AssetVocabulary v : vocabularies) {
	displayable = prefs.getBoolean("V" + v.getVocabularyId() + "d", false);
	if (displayable) {
		displaySequence = prefs.getInt("V" + v.getVocabularyId() + "ds", 0);
		DisplayColumnDTO dc = new DisplayColumnDTO("vocabulary", String.valueOf(v.getVocabularyId()), v.getTitle(locale), displaySequence);
		displayColumns.add(dc);
	}
}

// Sort by sequence.
Collections.sort(displayColumns);

// The date format used when Liferay stores value by structure.
SimpleDateFormat lfDateFormat = new SimpleDateFormat("yyyy-MM-dd");

// Check if the portlet is configured to check bad words.
long badWordsSetId = prefs.getLong("badWordsSetId", 0);

// Load the bad words list from Dynamic Data List.
List<String> badWords = new ArrayList<String>();
if (badWordsSetId != 0) {
	List<DDLRecord> records = DDLRecordLocalServiceUtil.getRecords(badWordsSetId);
	Value recordVal = null;
	String badWord = null;
	
	for (DDLRecord r : records) {
		List<DDMFormFieldValue> values = r.getDDMFormFieldValues("value");
		for (DDMFormFieldValue v : values) {
			if (v == null) {
				continue;
			}
			recordVal = v.getValue();
			if (recordVal == null) {
				continue;
			}
			badWord = recordVal.getString(locale);
			if (badWord == null) {
				continue;
			}
			badWord = badWord.trim();
			if (badWord.length() == 0) {
				continue;
			}
			badWords.add(badWord.toLowerCase());
		}
	}
}
%>

<style scoped>
	div[id$='_infoPanelId'] {
		padding: 0 !important;
	}
	
	.searchcontainer-content table thead tr {
		background-color: #085791;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
		color: #fff;
	}
	
	.searchcontainer-content table {
		margin-bottom: 0px !important;
	}
	
	.searchcontainer-content table th {
		font-weight: bold;
		text-transform : uppercase;
	}
	
	.col-download-attachments {
		min-width: 100px;
		text-align: center;
		vertical-align: middle !important;
	}
	
	.col-ddm-image > div.sticker {
		width: 132px;
		height: 132px;
	}
	
	tr.splitter {
		display: none;
	}
	
	.pagination-items-per-page {
		margin-left: 15px;
	}
	
	.dialog-iframe-bd {
		padding: unset;
	}
</style>
<liferay-ui:search-container
	emptyResultsMessage="no-web-content-was-found"
	id="<%= searchContainerId %>"
	searchContainer="<%= articleSearchContainer %>"
	cssClass="table-bordered"
>
	<liferay-ui:search-container-row
		className="Object"
		cssClass="entry-display-style"
		indexVar="index"
		modelVar="object"
	>

		<%
		JournalArticle curArticle = null;
		JournalFolder curFolder = null;

		Object result = row.getObject();

		if (result instanceof JournalFolder) {
			curFolder = (JournalFolder)result;
		}
		else {
			curArticle = (JournalArticle)result;
		}
		%>

		<c:choose>
			<c:when test="<%= curArticle != null %>">

				<%
				Map<String, Object> rowData = new HashMap<String, Object>();

				if (journalDisplayContext.isShowEditActions()) {
					rowData.put("draggable", JournalArticlePermission.contains(permissionChecker, curArticle, ActionKeys.DELETE) || JournalArticlePermission.contains(permissionChecker, curArticle, ActionKeys.UPDATE));
				}

				rowData.put("title", HtmlUtil.escape(curArticle.getTitle(locale)));

				row.setData(rowData);

				row.setPrimaryKey(HtmlUtil.escape(curArticle.getArticleId()));

				PortletURL rowURL = null;

				if (journalDisplayContext.isShowEditActions() && JournalArticlePermission.contains(permissionChecker, curArticle, ActionKeys.UPDATE)) {
					rowURL = liferayPortletResponse.createRenderURL();

					rowURL.setParameter("mvcPath", "/edit_article.jsp");
					rowURL.setParameter("redirect", currentURL);
					rowURL.setParameter("referringPortletResource", referringPortletResource);
					rowURL.setParameter("groupId", String.valueOf(curArticle.getGroupId()));
					rowURL.setParameter("folderId", String.valueOf(curArticle.getFolderId()));
					rowURL.setParameter("articleId", curArticle.getArticleId());
					rowURL.setParameter("version", String.valueOf(curArticle.getVersion()));
				}
				%>
				
				<c:if test="<%= !journalWebConfiguration.journalArticleForceAutogenerateId() %>">
					<liferay-ui:search-container-column-text
						name="id"
						value="<%= HtmlUtil.escape(curArticle.getArticleId()) %>"
					/>
				</c:if>
				
				<liferay-ui:search-container-column-text
					align="center"
					name="STT"
					value="<%=String.valueOf(articleSearchContainer.getStart() +  index + 1)%>"
					cssClass="col-order-number"
				/>
				<!-- START Bug #4101 nhut.dang ebook -->
				<% if (swtDisplayStyle == WebContentDisplayStyle.EBOOK.getCode()) { 
				%>
				<liferay-ui:search-container-column-text
					align="center"
					name="<%=LanguageUtil.format(request, "image-avartar", new Object())%>"
				>
				<% 
				String urlAvatar = "";
				if(curArticle.getSmallImage()){
					urlAvatar = "/image/image_gallery?img_id="+curArticle.getSmallImageId();
				}else{
					urlAvatar = "/o/hcm-default-theme/images/cms/no-image.jpg";
				}
				%>
				<img  style="width:110px; height:68px;object-fit: cover;" title="Hình đại diện" src="<%=urlAvatar%>" alt="">
				</liferay-ui:search-container-column-text>
				<%} %>
				<!-- END Bug #4101 nhut.dang ebook -->
				<%
				String articleTitleLower = curArticle.getTitle(themeDisplay.getLanguageId()).toLowerCase();
				
				String articleDescriptionLower = curArticle.getDescription(themeDisplay.getLanguageId()).toLowerCase();
				
				String articleContent = curArticle.getContentByLocale(themeDisplay.getLanguageId());
				String articleContentLower = articleContent.toLowerCase();
				
				// Check the bad words occur in content.
				for (String bw : badWords) {
					if (articleTitleLower.indexOf(bw) != -1
						|| articleDescriptionLower.indexOf(bw) != -1
						|| articleContentLower.indexOf(bw) != -1) {
						row.setClassName("bg-warning");
						break;
					}
				}
				
				// Get the XML document from content.
				Document articleDoc = null;
			    try {
			    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
			    } catch (DocumentException e) {
			        e.printStackTrace();
			    }
			    
			    List<AssetCategory> categories = new ArrayList<>();
			    try {
			    	categories = AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), curArticle.getResourcePrimKey());
			    } catch (Exception e) {
			    	e.printStackTrace();
			    }
				
				Node node = null;
				%>
				
				<!-- Additional columns from structure and vocabularies. -->
				<% for (DisplayColumnDTO col : displayColumns) { %>
					<%
					String title = null, valueStr = null;
					
					switch (col.getType()) {
					case "vocabulary":
						title = col.getLabel();
						if (title.startsWith(vocabularyPrefix)) {
							title = title.substring(vocabularyPrefix.length());
						}
						
						valueStr = StringPool.BLANK;
						for (AssetCategory ac : categories) {
							if (!String.valueOf(ac.getVocabularyId()).equals(col.getName())) {
								continue;
							}
							if (valueStr.length() != 0) {
								valueStr += ", " + ac.getTitle(locale);
							} else {
								valueStr += ac.getTitle(locale);
							}
						}
						break;
						
					case "checkbox":
						title = col.getLabel();
						node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
						if (node != null && node.getText().length() > 0) {
				            valueStr = node.getText();
				            if (valueStr == null) {
				            	valueStr = StringPool.BLANK;
				            }
				            
							// Convert the check state to a friendly display text.
				            if (valueStr.equals("true")) {
				            	valueStr = "Đã chọn";
				            } else {
				            	valueStr = "";
				            }
				        }
						break;
						
					case "ddm-date":
						title = col.getLabel();
						node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
				        if (node != null && node.getText().length() > 0) {
				            valueStr = node.getText();
				            if (valueStr == null) {
				            	valueStr = StringPool.BLANK;
				            }
				            
				            // Parse date value to expected format.
				            if (valueStr.length() != 0) {
				            	try {
				            		Date valueDate = lfDateFormat.parse(valueStr);
				            		valueStr = viDateFormat.format(valueDate);
				            	} catch (ParseException e) {
				            		break;
				            	}
				            }
				        }
						break;
						
					case "select":
						title = col.getLabel();
						node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
						
				        if (node != null && node.getText().length() > 0) {
				        	boolean isValueDetermined = false;
				        	
				        	if (swtDisplayStyle == WebContentDisplayStyle.CMS.getCode()) {
				        		if (col.getName().equals("source") || col.getName().equals("author")) {
			        				Node sourceTypeNode = articleDoc.selectSingleNode("/root/dynamic-element[@name='sourceType']/dynamic-content");
			        				String sourceTypeStr = sourceTypeNode == null ? StringPool.BLANK : sourceTypeNode.getText();
			        				boolean isAuthorSelected = sourceTypeStr.equals("[\"1\"]");
			        				
		        					if (col.getName().equals("author")) {
		        						if (!isAuthorSelected) {
		        							valueStr = StringPool.BLANK;
		        							isValueDetermined = true;
		        						}
		        					} else {
		        						if (isAuthorSelected) {
		        							valueStr = StringPool.BLANK;
		        							isValueDetermined = true;
		        						}
		        					}
				        		}
							}
				        	
				        	if (!isValueDetermined) {
				        		for (DDMFormField f : fields) {
					        		if (!f.getName().equals(col.getName())) {
					        			continue;
					        		}
					        		
					        		DDMFormFieldOptions options = f.getDDMFormFieldOptions();
					        		
					        		// Get the option display text.
						            if (options != null) {
						            	Map<String, LocalizedValue> optionsMap = options.getOptions();
						            	LocalizedValue localizedValue = optionsMap.get(node.getText());
						            	if (localizedValue != null) {
						            		valueStr = localizedValue.getString(locale);
						            		// Exclude the case of hint text.
						            		if (valueStr.indexOf("---") != -1) {
						            			valueStr = StringPool.BLANK;
						            		}
						            	} else {
							            	valueStr = StringPool.BLANK;
							            }
						            } else {
						            	valueStr = StringPool.BLANK;
						            }
					        		
					        		break;
					        	}
				        	}
				        }
						break;
						
					case "radio":
						title = col.getLabel();
						node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
				        if (node != null && node.getText().length() > 0) {
				        	for (DDMFormField f : fields) {
				        		if (!f.getName().equals(col.getName())) {
				        			continue;
				        		}
				        		
				        		DDMFormFieldOptions options = f.getDDMFormFieldOptions();
				        		
				        		// Get the option display text.
					            if (options != null) {
					            	// The option value of radio field is stored as: ["1"], ["2"]...
					            	// But the option values in structure does not contain double-quote and bracket symbols.
					            	String nodeText = node.getText();
					            	if (nodeText.startsWith("[\"") && nodeText.length() > 2) {
					            		nodeText = nodeText.substring(2);
					            	}
									if (nodeText.endsWith("\"]") && nodeText.length() > 2) {
										nodeText = nodeText.substring(0, nodeText.length() - 2);
					            	}
									
									Map<String, LocalizedValue> optionsMap = options.getOptions();
					            	LocalizedValue localizedValue = optionsMap.get(nodeText);
					            	
					            	if (localizedValue != null) {
					            		valueStr = localizedValue.getString(locale);
					            	} else {
						            	valueStr = StringPool.BLANK;
						            }
					            } else {
					            	valueStr = StringPool.BLANK;
					            }
				        		
				        		break;
				        	}
				        }
						break;
						
					default:
						title = col.getLabel();
						node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
				        if (node != null && node.getText().length() > 0) {
				            valueStr = node.getText();
				        }
						break;
						
					}
					%>
					
					<%-- If it's "documents-and-media" type, display the "download all" link. --%>
					<% if (col.getType().equals("ddm-documentlibrary")) { %>
						<c:if test="<%= title != null %>">
							<%
							String files = StringPool.BLANK;
							List<Node> nodes = articleDoc.selectNodes("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							
							for (Node n : nodes) {
								String f = GetterUtil.getString(n.getText());
								if (f.length() == 0) {
									continue;
								}
								files += "'" + f + "',";
							}
							
							if (files.length() != 0) {
								files = "[" + files.substring(0, files.lastIndexOf(",")) + "]";
							}
							
							// The document code.
							String codeTxt = StringPool.BLANK;
							Node codeNote = articleDoc.selectSingleNode("/root/dynamic-element[@name='code']/dynamic-content");
							if (Validator.isNotNull(codeNote)) {
								codeTxt = codeNote.getText();
							}
							
							// Check if the attachment file is image.
							boolean isImageAttachment = false;
							for (String i: ".jpg_.jpeg_.bmp_.gif_.png".split("_")) {
								if (files.contains(i)) {
									isImageAttachment = true;
								}
							}
							%>
							<% if (isImageAttachment == true && swtDisplayStyle == WebContentDisplayStyle.IMAGE.getCode()) {
								String imageAttachmentSource = GetterUtil.getString(nodes.get(0).getText()) + "&imageThumbnail=1";
							%>
								<c:set var="imageSource" value="<%= imageAttachmentSource %>" />
								<liferay-ui:search-container-column-image
									name="<%= title.trim() %>"
									cssClass="col-ddm-image"
									src="${imageSource}">
								</liferay-ui:search-container-column-image>
							<% } else { %>
								<c:set var="files" value="<%= files.trim() %>" />
								<c:set var="codeTxt" value="<%= codeTxt.trim() %>" />
								<liferay-ui:search-container-column-text
									name="<%= title.trim() %>"
									cssClass="col-download-attachments"
									href="javascript:downloadAllFiles(${files}, '${codeTxt}')">
									<i class='<%= files.length() != 0 ? "icon-download-alt" : StringPool.BLANK %>'></i>
								</liferay-ui:search-container-column-text>
							<% } %>
						</c:if>
					<% } else if (col.getType().equals("ddm-image")) { %>
						<c:if test="<%= title != null %>">
							<%
							String files = StringPool.BLANK;
							List<Node> nodes = articleDoc.selectNodes("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							%>
							
							<% if (nodes.size() == 0) { %>
								<liferay-ui:search-container-column-text name="<%= title.trim() %>" value="" />
							<% } else if (nodes.size() == 1) { %>
								<% files = GetterUtil.getString(nodes.get(0).getText()); %>
								<c:set var="imageSource" value="<%=files.trim()%>" />
								<liferay-ui:search-container-column-image
									name="<%= title.trim() %>"
									cssClass="col-ddm-image"
									src="${imageSource}">
								</liferay-ui:search-container-column-image>
							<% } else { %>
								<liferay-ui:search-container-column-text name="<%= title.trim() %>" value="Có nhiều hình" />
							<% } %>
						</c:if>
					<% } else if (col.getType().equals("attribute")) { %>
						<c:choose>
							<c:when test="<%= col.getName().equals("title") %>">
								<liferay-ui:search-container-column-jsp
									cssClass="table-cell-content"
									href="<%= rowURL %>"
									name="title"
									path="/article_title.jsp"
								/>
							</c:when>
							<c:when test="<%= col.getName().equals("description") %>">
								<% if (swtDisplayStyle == WebContentDisplayStyle.DOCUMENT.getCode()) { %>
									<liferay-ui:search-container-column-text
										cssClass="table-cell-content"
										href="<%= rowURL %>"
										name="Trích yếu"
										value="<%= HtmlUtil.escape(curArticle.getDescription(locale)) %>"
									/>
								<% } else { %>
									<liferay-ui:search-container-column-text
										cssClass="table-cell-content"
										name="description"
										value="<%= HtmlUtil.escape(curArticle.getDescription(locale)) %>"
									/>
								<% } %>
							</c:when>
							<c:when test="<%= col.getName().equals("createUser") %>">
								<liferay-ui:search-container-column-text
									name="metadata.DublinCore.CREATOR"
									value="<%= HtmlUtil.escape(curArticle.getUserName()) %>"
								/>
							</c:when>
							<c:when test="<%= col.getName().equals("createDate") %>">
								<liferay-ui:search-container-column-text
									name="create-date"
									value="<%= curArticle.getCreateDate() == null ? StringPool.BLANK :
										viDateFormat.format(curArticle.getCreateDate()) %>"
								/>
							</c:when>
							<c:when test="<%= col.getName().equals("modifiedDate") %>">
								<liferay-ui:search-container-column-text
									name="modified-date"
									value="<%= curArticle.getModifiedDate() == null ? StringPool.BLANK : 
										viDateFormat.format(curArticle.getModifiedDate()) %>"
								/>
							</c:when>
							<c:when test="<%= col.getName().equals("displayDate") %>">
								<liferay-ui:search-container-column-text
									name="display-date"
									value="<%= curArticle.getDisplayDate() == null ? StringPool.BLANK : 
										viDateFormat.format(curArticle.getDisplayDate()) %>"
								/>
							</c:when>
						</c:choose>
					<% } else { %>
						<c:if test="<%= title != null %>">
							<liferay-ui:search-container-column-text
								name="<%=title.trim()%>"
								value="<%=(valueStr == null ? StringPool.BLANK : HtmlUtil.escape(valueStr.trim()))%>"
							/>
						</c:if>
					<% } %>
				<% } %>
				
				<c:if test="<%=enableReceiver%>">
					<liferay-ui:search-container-column-text name="<%=LanguageUtil.format(request, "swt-journal-web-hook.receiver", new Object())%>">
					
					 <%
					 String nameReceiver = "";
					 try{
						 WorkflowInstanceLink wil = WorkflowInstanceLinkLocalServiceUtil
									.getWorkflowInstanceLink(themeDisplay.getCompanyId(),
											themeDisplay.getScopeGroupId(), JournalArticle.class.getName(),
											curArticle.getId());
						 
						List<WorkflowLog> workflowLogs = JournalWebUtil. getWorkflowLogs(themeDisplay.getCompanyId(), wil.getWorkflowInstanceId());
						
						nameReceiver = JournalWebUtil.getAssignedToMeMessage(workflowLogs.get(workflowLogs.size()-1));
						 
					 }catch(PortalException e){
					 }
					 
					 %>
					<%=nameReceiver %>
					
					</liferay-ui:search-container-column-text>
				</c:if>
				<liferay-ui:search-container-column-text name="status">
					<% String statusTxt = StringPool.BLANK;
						if (curArticle.getStatus() == WorkflowConstants.STATUS_DRAFT) {
							statusTxt = "Bản nháp";
						} else if (curArticle.getStatus() == WorkflowConstants.STATUS_APPROVED) {
							statusTxt = "Đã xuất bản";
							 if (swtDisplayStyle == WebContentDisplayStyle.FEEDBACK.getCode()){
								 statusTxt = "Đã xử lý";
								} 
						} else if (curArticle.getStatus() == WorkflowConstants.STATUS_EXPIRED) {
							statusTxt = "Đã hết hạn";
						} else {
							String tmp = "Chờ xuất bản";
							try {
								WorkflowInstanceLink wil = WorkflowInstanceLinkLocalServiceUtil
										.getWorkflowInstanceLink(themeDisplay.getCompanyId(),
												themeDisplay.getScopeGroupId(), JournalArticle.class.getName(),
												curArticle.getId());
								OrderByComparator orderByComparator = null;
								List<WorkflowTask> listTasks = WorkflowTaskManagerUtil
										.getWorkflowTasksByWorkflowInstance(themeDisplay.getCompanyId(),
												wil.getUserId(), wil.getWorkflowInstanceId(), false, 0,
												Byte.MAX_VALUE, orderByComparator);

								if (listTasks.size() > 0) {
									tmp = listTasks.get(0).getName();
								} else {
									listTasks = WorkflowTaskManagerUtil.getWorkflowTasksBySubmittingUser(
											themeDisplay.getCompanyId(), wil.getUserId(), false, 0,
											Byte.MAX_VALUE, orderByComparator);
									for (WorkflowTask wk : listTasks) {
										if (wk.getWorkflowInstanceId() == wil.getWorkflowInstanceId()) {
											tmp = wk.getName();
										}
									}
								}
								
								if (tmp.toLowerCase().contains("đăng bài")
										&& !tmp.toLowerCase().contains("admin")) {
									tmp = "Đang soạn";
								} else if (tmp.toLowerCase().contains("duyệt")) {
									tmp = "Đang kiểm duyệt";
									  if (swtDisplayStyle == WebContentDisplayStyle.FEEDBACK.getCode()){
										tmp = "Đã chuyển xử lý";
									} 
								} else if (tmp.toLowerCase().contains("xuất bản")
										|| tmp.toLowerCase().contains("admin")) {
									tmp = "Chờ xuất bản";
									if (swtDisplayStyle == WebContentDisplayStyle.FEEDBACK.getCode()){
										tmp = "Đã tiếp nhận";
									} 
								}
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							statusTxt = tmp;
						}
					%>
					<%= statusTxt %>
				</liferay-ui:search-container-column-text> 
				
				<c:if test="<%= journalDisplayContext.isShowEditActions() %>">
					<liferay-ui:search-container-column-jsp path="/article_action_custom.jsp" />
				</c:if>

				<%-- dung.nguyen: We only support the "list" display style.
				<c:choose>
					<c:when test='<%= displayStyle.equals("descriptive") %>'>
						<liferay-ui:search-container-column-text>
							<liferay-ui:user-portrait
								cssClass="user-icon-lg"
								userId="<%= curArticle.getUserId() %>"
							/>
						</liferay-ui:search-container-column-text>

						<liferay-ui:search-container-column-text
							colspan="<%= 2 %>"
						>

							<%
							Date createDate = curArticle.getModifiedDate();

							String modifiedDateDescription = LanguageUtil.getTimeDescription(request, System.currentTimeMillis() - createDate.getTime(), true);
							%>

							<h6 class="text-default">
								<liferay-ui:message arguments="<%= new String[] {HtmlUtil.escape(curArticle.getUserName()), modifiedDateDescription} %>" key="x-modified-x-ago" />
							</h6>

							<h5>
								<aui:a href="<%= rowURL != null ? rowURL.toString() : null %>">
									<%= HtmlUtil.escape(curArticle.getTitle(locale)) %>
								</aui:a>
							</h5>

							<c:if test="<%= journalDisplayContext.isSearch() %>">
								<h5>
									<%= JournalUtil.getAbsolutePath(liferayPortletRequest, curArticle.getFolderId()) %>
								</h5>
							</c:if>

							<h6 class="text-default">
								<aui:workflow-status markupView="lexicon" showIcon="<%= false %>" showLabel="<%= false %>" status="<%= curArticle.getStatus() %>" />
							</h6>
						</liferay-ui:search-container-column-text>

						<c:if test="<%= journalDisplayContext.isShowEditActions() %>">
							<liferay-ui:search-container-column-jsp
								path="/article_action.jsp"
							/>
						</c:if>
					</c:when>
					<c:when test='<%= displayStyle.equals("icon") %>'>

						<%
						row.setCssClass("entry-card lfr-asset-item " + row.getCssClass());
						%>

						<liferay-ui:search-container-column-text>

							<%
							String articleImageURL = curArticle.getArticleImageURL(themeDisplay);
							%>

							<c:choose>
								<c:when test="<%= Validator.isNotNull(articleImageURL) %>">
									<liferay-frontend:vertical-card
										actionJsp='<%= journalDisplayContext.isShowEditActions() ? "/article_action.jsp" : null %>'
										actionJspServletContext="<%= application %>"
										imageUrl="<%= HtmlUtil.escape(articleImageURL) %>"
										resultRow="<%= row %>"
										rowChecker="<%= articleSearchContainer.getRowChecker() %>"
										title="<%= curArticle.getTitle(locale) %>"
										url="<%= rowURL != null ? rowURL.toString() : null %>"
									>
										<%@ include file="/article_vertical_card.jspf" %>
									</liferay-frontend:vertical-card>
								</c:when>
								<c:otherwise>
									<liferay-frontend:icon-vertical-card
										actionJsp='<%= journalDisplayContext.isShowEditActions() ? "/article_action.jsp" : null %>'
										actionJspServletContext="<%= application %>"
										icon="web-content"
										resultRow="<%= row %>"
										rowChecker="<%= articleSearchContainer.getRowChecker() %>"
										title="<%= curArticle.getTitle(locale) %>"
										url="<%= rowURL != null ? rowURL.toString() : null %>"
									>
										<%@ include file="/article_vertical_card.jspf" %>
									</liferay-frontend:icon-vertical-card>
								</c:otherwise>
							</c:choose>
						</liferay-ui:search-container-column-text>
					</c:when>
					<c:otherwise>
						<c:if test="<%= !journalWebConfiguration.journalArticleForceAutogenerateId() %>">
							<liferay-ui:search-container-column-text
								name="id"
								value="<%= HtmlUtil.escape(curArticle.getArticleId()) %>"
							/>
						</c:if>

						<liferay-ui:search-container-column-jsp
							cssClass="table-cell-content"
							href="<%= rowURL %>"
							name="title"
							path="/article_title.jsp"
						/>

						<liferay-ui:search-container-column-text
							cssClass="table-cell-content"
							name="description"
							value="<%= HtmlUtil.escape(curArticle.getDescription(locale)) %>"
						/>

						<c:if test="<%= journalDisplayContext.isSearch() %>">
							<liferay-ui:search-container-column-text
								cssClass="table-cell-content"
								name="path"
								value="<%= JournalUtil.getAbsolutePath(liferayPortletRequest, curArticle.getFolderId()) %>"
							/>
						</c:if>

						<liferay-ui:search-container-column-text
							name="author"
							value="<%= HtmlUtil.escape(PortalUtil.getUserName(curArticle)) %>"
						/>

						<liferay-ui:search-container-column-status
							name="status"
						/>

						<liferay-ui:search-container-column-date
							name="modified-date"
							value="<%= curArticle.getModifiedDate() %>"
						/>

						<liferay-ui:search-container-column-date
							name="display-date"
							value="<%= curArticle.getDisplayDate() %>"
						/>

						<%
						DDMStructure ddmStructure = DDMStructureLocalServiceUtil.getStructure(scopeGroupId, PortalUtil.getClassNameId(JournalArticle.class), curArticle.getDDMStructureKey(), true);
						%>

						<liferay-ui:search-container-column-text
							name="type"
							value="<%= HtmlUtil.escape(ddmStructure.getName(locale)) %>"
						/>

						<c:if test="<%= journalDisplayContext.isShowEditActions() %>">
							<liferay-ui:search-container-column-jsp
								path="/article_action.jsp"
							/>
						</c:if>
					</c:otherwise>
				</c:choose>
				--%>
			</c:when>

			<%-- dung.nguyen: We don't display folder structure in embedded portlet.
			<c:when test="<%= curFolder != null %>">

				<%
				Map<String, Object> rowData = new HashMap<String, Object>();

				rowData.put("draggable", JournalFolderPermission.contains(permissionChecker, curFolder, ActionKeys.DELETE) || JournalFolderPermission.contains(permissionChecker, curFolder, ActionKeys.UPDATE));
				rowData.put("folder", true);
				rowData.put("folder-id", curFolder.getFolderId());
				rowData.put("title", HtmlUtil.escape(curFolder.getName()));

				row.setData(rowData);
				row.setPrimaryKey(String.valueOf(curFolder.getPrimaryKey()));

				PortletURL rowURL = liferayPortletResponse.createRenderURL();

				rowURL.setParameter("redirect", currentURL);
				rowURL.setParameter("groupId", String.valueOf(curFolder.getGroupId()));
				rowURL.setParameter("folderId", String.valueOf(curFolder.getFolderId()));
				rowURL.setParameter("displayStyle", displayStyle);
				rowURL.setParameter("showEditActions", String.valueOf(journalDisplayContext.isShowEditActions()));
				%>

				<c:choose>
					<c:when test='<%= displayStyle.equals("descriptive") %>'>
						<liferay-ui:search-container-column-icon
							icon="folder"
							toggleRowChecker="<%= true %>"
						/>

						<liferay-ui:search-container-column-text
							colspan="<%= 2 %>"
						>

							<%
							Date createDate = curFolder.getCreateDate();

							String createDateDescription = LanguageUtil.getTimeDescription(request, System.currentTimeMillis() - createDate.getTime(), true);
							%>

							<h6 class="text-default">
								<liferay-ui:message arguments="<%= new String[] {HtmlUtil.escape(curFolder.getUserName()), createDateDescription} %>" key="x-modified-x-ago" />
							</h6>

							<h5>
								<aui:a href="<%= rowURL != null ? rowURL.toString() : null %>">
									<%= HtmlUtil.escape(curFolder.getName()) %>
								</aui:a>
							</h5>

							<h6 class="text-default">
								<aui:workflow-status markupView="lexicon" showIcon="<%= false %>" showLabel="<%= false %>" status="<%= curFolder.getStatus() %>" />
							</h6>
						</liferay-ui:search-container-column-text>

						<c:if test="<%= journalDisplayContext.isShowEditActions() %>">
							<liferay-ui:search-container-column-jsp
								path="/folder_action.jsp"
							/>
						</c:if>
					</c:when>
					<c:when test='<%= displayStyle.equals("icon") %>'>

						<%
						row.setCssClass("entry-card lfr-asset-folder " + row.getCssClass());
						%>

						<liferay-ui:search-container-column-text
							colspan="<%= 2 %>"
						>
							<liferay-frontend:horizontal-card
								actionJsp='<%= journalDisplayContext.isShowEditActions() ? "/folder_action.jsp" : null %>'
								actionJspServletContext="<%= application %>"
								resultRow="<%= row %>"
								rowChecker="<%= articleSearchContainer.getRowChecker() %>"
								text="<%= HtmlUtil.escape(curFolder.getName()) %>"
								url="<%= rowURL.toString() %>"
							>
								<liferay-frontend:horizontal-card-col>
									<liferay-frontend:horizontal-card-icon
										icon="folder"
									/>
								</liferay-frontend:horizontal-card-col>
							</liferay-frontend:horizontal-card>
						</liferay-ui:search-container-column-text>
					</c:when>
					<c:otherwise>
						<c:if test="<%= !journalWebConfiguration.journalArticleForceAutogenerateId() %>">
							<liferay-ui:search-container-column-text
								name="id"
								value="<%= HtmlUtil.escape(String.valueOf(curFolder.getFolderId())) %>"
							/>
						</c:if>

						<liferay-ui:search-container-column-text
							cssClass="table-cell-content"
							href="<%= rowURL.toString() %>"
							name="title"
							value="<%= HtmlUtil.escape(curFolder.getName()) %>"
						/>

						<liferay-ui:search-container-column-text
							cssClass="table-cell-content"
							name="description"
							value="<%= HtmlUtil.escape(curFolder.getDescription()) %>"
						/>

						<liferay-ui:search-container-column-text
							name="author"
							value="<%= HtmlUtil.escape(PortalUtil.getUserName(curFolder)) %>"
						/>

						<liferay-ui:search-container-column-text
							name="status"
							value="--"
						/>

						<liferay-ui:search-container-column-date
							name="modified-date"
							value="<%= curFolder.getModifiedDate() %>"
						/>

						<liferay-ui:search-container-column-text
							name="display-date"
							value="--"
						/>

						<liferay-ui:search-container-column-text
							name="type"
							value='<%= LanguageUtil.get(request, "folder") %>'
						/>

						<c:if test="<%= journalDisplayContext.isShowEditActions() %>">
							<liferay-ui:search-container-column-jsp
								path="/folder_action.jsp"
							/>
						</c:if>
					</c:otherwise>
				</c:choose>
			</c:when>
			--%>
		</c:choose>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator
		displayStyle="list"
		markupView="lexicon"
		resultRowSplitter="<%= journalDisplayContext.isSearch() ? null : new JournalResultRowSplitter() %>"
		searchContainer="<%= articleSearchContainer %>"
	/>
</liferay-ui:search-container>

<% if (badWordsSetId != 0) { %>
	<br />
	<div class="row-fluid"><em>Ghi chú: Những bài viết có chứa từ xấu được đánh dấu nền màu vàng.</em></div>
<% } %>

<a id="swt-hidden-file-download" style="display: none;"></a>

<script>
		<!-- START SNV search scores -->
		<% if (swtDisplayStyle == WebContentDisplayStyle.SEARCHSCORES.getCode()){ %>
			 $('th[id="<%=renderResponse.getNamespace()%>articles_col-title"]').text("Số báo danh");
			 $('th[id="<%=renderResponse.getNamespace()%>articles_col-description"]').text("Họ tên");
		<%}%>
		<!-- END SNV search scores -->
	//khang.ho: BanDoQuyHoach show image instead of text
	if($('.lfr-hinh-anh-column').length != 0){
		$('.lfr-hinh-anh-column').each(function(i, obj) {
		    if($(this).text().trim().startsWith("http")){
				var html = "<img src='" + $(this).text().trim() + "TileGroup0/0-0-0.jpg' />";
				$(this).text("");
				$(this).append(html);
		    }
		});
	}

	function downloadAllFiles(urls, code) {
		var jszip = new JSZip();
		doZip(jszip, urls, 0, code);
	}
	
	function doZip(jszip, urls, index, code) {
		var fileName = urls[index].split('/')[4];
		
		JSZipUtils.getBinaryContent(urls[index], function(err, data) {
			if (!err) {
				jszip.file(fileName, data, {
					binary : true
				});
				
				if (++index == urls.length) {
					jszip.generateAsync({
						type : "blob"
					}).then(function(content) {
						var zipName = "tailieudinhkem";
						if (code != "") {
							zipName = code;
						}
						var a = $("#swt-hidden-file-download");
						a.attr("download", zipName);
						a.attr("href", URL.createObjectURL(content));
						document.getElementById("swt-hidden-file-download").click();
					});
				} else {
					doZip(jszip, urls, index, code);
				}
			}
		});
	}
</script>