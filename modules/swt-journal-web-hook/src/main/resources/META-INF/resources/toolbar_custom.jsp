<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>

<%@page import="com.liferay.frontend.taglib.servlet.taglib.ManagementBarFilterItem"%>

<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.search.BaseModelSearchResult"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.model.Role"%>
<%@page import="com.liferay.portal.kernel.service.RoleLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.List"%>

<%@ include file="/init.jsp" %>
<%@ include file="/viewExcel.jsp"%>
<%
String searchContainerId = ParamUtil.getString(request, "searchContainerId");
String parrentId = ParamUtil.getString(request, "parrentId");
String childrenPage = ParamUtil.getString(request, "childrenPage");
String currentParrentURL = ParamUtil.getString(request, "currentParrentURL");
%>

<%
String structureKey = ParamUtil.getString(request, "ddmStructureKey");

long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);

//Get the DDM structure.
DDMStructure ddmStructure = null;
try {
	ddmStructure = DDMStructureLocalServiceUtil.getStructure(scopeGroupId, journalArticleClassNameId,
			structureKey);
} catch (PortalException e) {
	e.printStackTrace();
} catch (SystemException e) {
	e.printStackTrace();
}
%>

<%
//The web-content folder paths must be ordered by folder hierarchy.
String[] paths = new String[] { ddmStructure.getName(locale), };

//Temporary variables will be used later.
JournalFolder jnFolder = null;
ServiceContext jnFolderSctx = null;
long jnFolderId = 0;

//Auto-create the web-content folder if not exist.
for (String path : paths) {
	jnFolder = JournalFolderLocalServiceUtil.fetchFolder(scopeGroupId, jnFolderId, path);
	if (jnFolder != null) {
		jnFolderId = jnFolder.getFolderId();
		continue;
	}
	
	if (jnFolderSctx == null) {
		jnFolderSctx = ServiceContextFactory.getInstance(JournalFolder.class.getName(), request);
	}
	
	jnFolder = JournalFolderLocalServiceUtil.addFolder(jnFolderSctx.getUserId(), scopeGroupId, jnFolderId,
			path, StringPool.BLANK, jnFolderSctx);
	jnFolderId = jnFolder.getFolderId();
}
%>

<%
String LABEL_ADVANCED_SEARCH = "Mở rộng";
String LABEL_BASIC_SEARCH = "Thu gọn";

// The list of structure fields.
List<DDMFormField> fields = new ArrayList<>();
if (ddmStructure != null) {
	// Get the fields from selected structure.
	fields = ddmStructure.getDDMFormFields(false);
}

// The list of vocabularies which name is prefixed with structure ID.
List<AssetVocabulary> vocabularies = new ArrayList<>();
String vocabularyPrefix = StringPool.BLANK;
vocabularyPrefix = ddmStructure.getStructureId() + "_";
try {
	BaseModelSearchResult<AssetVocabulary> tmp = AssetVocabularyLocalServiceUtil.searchVocabularies(
			themeDisplay.getCompanyId(), scopeGroupId, vocabularyPrefix, 0, Byte.MAX_VALUE);
	vocabularies = tmp.getBaseModels();
} catch (PortalException e) {
	e.printStackTrace();
}

// Parse portlet preferences to JSON object.
String jsonPrefs = ParamUtil.getString(request, "jsonPrefs");
JSONObject prefs = null;
if (jsonPrefs == null || jsonPrefs.length() == 0) {
	prefs = JSONFactoryUtil.createJSONObject();
} else {
	prefs = JSONFactoryUtil.createJSONObject(jsonPrefs);
}

// The list of search criterias to be displayed in search form.
List<SearchCriteriaDTO> searchCriterias = new ArrayList<>();

// Temporary variables used for checking search/display columns.
boolean searchable = false;
int searchType = -1, searchSequence = -1;

// Additional columns from selected structure fields.
for (DDMFormField f : fields) {
	searchable = prefs.getBoolean("S" + f.getName() + "s", false);
	if (searchable) {
		searchType = prefs.getInt("S" + f.getName() + "st", 0);
		searchSequence = prefs.getInt("S" + f.getName() + "ss", 0);
		SearchCriteriaDTO sc = new SearchCriteriaDTO(f.getType(), searchType, f.getName(), f.getLabel().getString(locale), searchSequence);
		searchCriterias.add(sc);
	}
}

// Additional columns from selected vocabularies.
for (AssetVocabulary v : vocabularies) {
	searchable = prefs.getBoolean("V" + v.getVocabularyId() + "s", false);
	if (searchable) {
		searchSequence = prefs.getInt("V" + v.getVocabularyId() + "ss", 0);
		SearchCriteriaDTO sc = new SearchCriteriaDTO("vocabulary", -1, String.valueOf(v.getVocabularyId()), v.getTitle(locale), searchSequence);
		searchCriterias.add(sc);
	}
}

for (AttributeSearchDTO a : attributes) {
	searchable = prefs.getBoolean("A" + a.getName() + "s", false);
	if (searchable) {
		searchSequence = prefs.getInt("A" + a.getName() + "ss", 0);
		SearchCriteriaDTO sc = new SearchCriteriaDTO(a.getType(), -1, a.getName(), a.getLabel(), searchSequence);
		searchCriterias.add(sc);
	}
}

//Sort by sequence.
Collections.sort(searchCriterias);

long badWordsSetId = prefs.getLong("badWordsSetId", 0);

boolean allowAddArticle = prefs.getBoolean("allowAddArticle");

boolean statusSearch = prefs.getBoolean("statusSearch");

boolean defaultAdvancedSearch = prefs.getBoolean("defaultAdvancedSearch");

boolean allowExportArticles = prefs.getBoolean("allowExportArticles");

boolean advSearch = GetterUtil.getBoolean(request.getParameter("advSearch"));
%>

<style scoped>
	.swt-btn-toolbar {
		padding-left: 5px;
		margin-bottom: 0 !important;
	}
	
	.swt-col-pl-0 {
		padding-left: 0;
	}
	
	.swt-col-pr-0 {
		padding-right: 0;
	}
	
	.advanced-search-container {
		display: none;
		margin-bottom: 15px;
		margin-top: 15px;
		padding-left: 5px;
	}
	
	.basic-search-container {
		display: none;
	}
	
	.col-label {
		padding-right: 0;
		line-height: 34px;
	}
	
	.input-group {
		margin-bottom: 10px;
	}
	
	.input-group .search-datepicker input {
		border-radius: 0 4px 4px 0;
	}
	
	.swt-col-btn-max-width {
		padding-left: 2px;
		padding-right: 2px;
	}
	
	.swt-col-btn-max-width button, .swt-col-btn-max-width a {
		width: 100%;
	}
	
	.search-datepicker input::placeholder {
		visibility: hidden;
	}
	
	a[data-qa-id="moveButton"] {
		display: none;
	}
	
	.management-bar {
		margin-bottom: 5px;
	}
	
	.management-bar-item-title {
		max-width: unset;
	}
</style>

<%
PortletURL searchURL = liferayPortletResponse.createActionURL();
searchURL.setParameter("mvcPath", "/view.jsp");
searchURL.setParameter("groupId", String.valueOf(scopeGroupId));
searchURL.setParameter("folderId", String.valueOf(jnFolderId));
searchURL.setParameter("showEditActions", String.valueOf(journalDisplayContext.isShowEditActions()));
searchURL.setParameter("ddmStructureKey", structureKey);
searchURL.setParameter("embedded", String.valueOf(embedded));
searchURL.setParameter("parrentId", String.valueOf(parrentId));
searchURL.setParameter("childrenPage", String.valueOf(childrenPage));
searchURL.setParameter("currentParrentURL", String.valueOf(currentParrentURL));
%>

<div class="btn-toolbar swt-btn-toolbar" role="toolbar">
	<div class="col-md-4 col-sm-4 swt-col-pl-0">
		<div class="row-fluid">
			<!-- Back button when being on children page. -->
			<c:if test="<%= currentParrentURL.length() != 0 %>">
				<div class="col-md-5 swt-col-btn-max-width">
					<aui:button href="<%=currentParrentURL%>" name="btnBack" value="Quay lại"
						cssClass="btn btn-default" icon="icon-chevron-left" />
				</div>
			</c:if>
			<c:if test="<%= allowAddArticle && JournalFolderPermission.contains(permissionChecker, scopeGroupId, jnFolderId, ActionKeys.ADD_ARTICLE)&&currentParrentURL=="" %>">
				<div class="col-md-5 swt-col-btn-max-width">
					<portlet:renderURL var="addArticleURL">
						<portlet:param name="mvcPath" value="/edit_article.jsp" />
						<portlet:param name="redirect" value="<%=currentURL%>" />
						<portlet:param name="groupId" value="<%=String.valueOf(scopeGroupId)%>" />
						<portlet:param name="folderId" value="<%=String.valueOf(jnFolderId)%>" />
						<portlet:param name="ddmStructureKey" value="<%=structureKey%>" />
					</portlet:renderURL>
					<aui:button name="btnAddArticle" value="Đăng bài"
						cssClass="btn btn-primary" icon="icon-plus" 
						href="<%=addArticleURL.toString()%>" />
				
				</div>
				<!-- START Feature #4102 : display button to link video server -->
				<% if (swtDisplayStyle == WebContentDisplayStyle.VIDEO.getCode()) { %>
					<div class="col-md-3 swt-col-btn-max-width">
						<div class="btn btn-warning" onclick="openVideoServer()">
						<span class="glyphicon glyphicon-cog"></span>Video server </div>
					</div>
				<%} %>
				<!-- END Feature #4102 : display button to link video server -->
				<% if (swtDisplayStyle == WebContentDisplayStyle.SEARCHSCORES.getCode()) { %>
					<div class="col-md-3 swt-col-btn-max-width">
						<div class="btn btn-primary" data-toggle="modal" data-target="#myModalImportScores">
						<span class="fa fa-file-text"></span>Import file</div>
					</div>
				<%} %>
			</c:if>
			<c:if test="<%= allowExportArticles %>">
				<div class="col-md-3 swt-col-btn-max-width">
					<aui:button name="btnExportArticles" value="Xuất file"
						cssClass="btn btn-default" icon="icon-file" />
				</div>
			</c:if>
		</div>
	</div>
	
	<div class="col-md-8 col-sm-8 swt-col-pr-0 basic-search-container">
		<aui:form action="<%= searchURL %>" method="post" name="fmSearch1" cssClass="search-form">
			<aui:input name="jsonPrefs" type="hidden" value="<%=jsonPrefs%>" />
			<aui:input name="status" type="hidden" value="<%=String.valueOf(WorkflowConstants.STATUS_ANY)%>" />
			<div class="row-fluid">
				<div class="col-md-5" style="padding-right: 17px">
					<div class="row">
						<aui:input name="keywords" type="text" label="<%=StringPool.BLANK%>"
							placeholder="Nhập từ khóa cần tìm kiếm" />
					</div>
				</div>
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-5 swt-col-btn-max-width">
							<aui:select name="bs_searchType" label="">
								<aui:option value="1">Tìm kiếm mặc định</aui:option>
								<aui:option value="0">Tìm kiếm gần đúng</aui:option>
							</aui:select>
						</div>
						<div class="col-md-4 swt-col-btn-max-width">
							<aui:button type="submit" icon="icon-search" cssClass="btn-search"
								value='<%=LanguageUtil.format(request, "search", dummyObj)%>' />
						</div>
						<div class="col-md-3 swt-col-btn-max-width">
							<% if (searchCriterias.size() > 0) { %>
								<aui:button id="btnToAdvancedSearch" icon="icon-chevron-down"
									cssClass="btn hidden-xs" onClick="switchSearchMode(false);"
									value="<%=LABEL_ADVANCED_SEARCH%>" />
							<% } %>
						</div>
					</div>
				</div>
			</div>
		</aui:form>
	</div>
</div>

<div class="btn-toolbar advanced-search-container" role="toolbar">
	<aui:form action="<%= searchURL %>" method="post" name="fmSearch2" cssClass="search-form">
		<aui:input name="advSearch" type="hidden" value="<%=advSearch%>" />
		<aui:input name="embedded" type="hidden" value="<%=embedded%>" />
		<aui:input name="jsonPrefs" type="hidden" value="<%=jsonPrefs%>" />
		
		<div class="row-fluid">
			<% for (SearchCriteriaDTO c : searchCriterias) { %>
				<% if (c.getType().equals("text") || c.getType().equals("textarea") || c.getType().equals("ddm-text-html")) { %>
					<div class="col-md-6 swt-col-pr-0">
						<div class="row">
			 				<div class="col-md-2 col-label">
			 					<% if (swtDisplayStyle == WebContentDisplayStyle.DOCUMENT.getCode() && c.getName().equals("description")) { %>
			 						<label class="pull-right"><strong>Trích yếu</strong></label>
			 					<% } else { %>
			 						<label class="pull-right" for="<%=c.getName()%>" ><strong><%=c.getLabel()%></strong></label>
			 					<% } %>
							</div>
			 				<div class="col-md-10">
		 						<aui:input type="text" name="<%="advs_" + c.getName()%>" label="<%=StringPool.BLANK%>" />
			 				</div>
			 			</div>
					</div>
		 		<% } else if (c.getType().equals("ddm-date")) { %>
		 			<div class="col-md-6 swt-col-pr-0">
			 			<div class="row">
			 				<div class="col-md-2 col-label">
			 					<label class="pull-right"><strong><%=c.getLabel()%></strong></label>
			 				</div>
			 				
							<%
							Calendar today = Calendar.getInstance();
							today.setTime(new Date());
							%>
							
							<div class="col-md-10">
								<div class="row">
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"><%=LanguageUtil.format(request, "from", dummyObj)%></span>
											<liferay-ui:input-date 
												cssClass=""
												firstDayOfWeek="<%=today.getFirstDayOfWeek() - 1%>"
												name='<%="advs_" + c.getName() + "_from"%>'
												nullable="<%=true%>"
											/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"><%=LanguageUtil.format(request, "to", dummyObj)%></span>
											<liferay-ui:input-date
												cssClass=""
												firstDayOfWeek="<%=today.getFirstDayOfWeek() - 1%>"
												name='<%="advs_" + c.getName() + "_to"%>'
												nullable="<%=true%>"
											/>
										</div>
									</div>
								</div>
							</div>
			 			</div>
		 			</div>
				<% } else if (c.getType().equals("vocabulary")) { %>
					<%
					long vocabularyId = Long.valueOf(c.getName());
					AssetVocabulary vocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(vocabularyId);
					List<AssetCategory> categories = vocabulary.getCategories();
					
					if (categories.size() != 0) {
						long minParentId = Long.MAX_VALUE;
						for (AssetCategory ac : categories) {
							if (minParentId > ac.getParentCategoryId()) {
								minParentId = ac.getParentCategoryId();
							}
						}
						categories = CategoryUtil.deepSort(categories, minParentId, 0);
					}
					%>
					
					<div class="col-md-6 swt-col-pr-0">
						<div class="row">
							<div class="col-md-2 col-label">
								<label class="pull-right"><strong><%=c.getLabel().substring((structureKey + "_").length())%></strong></label>
							</div>
							<div class="col-md-10">
								<aui:select name='<%="advs_" + "vocabulary_" + vocabularyId%>' label="<%=StringPool.BLANK%>">
									<aui:option value='0' label='<%=LanguageUtil.format(request, "any", dummyObj)%>'>
									</aui:option>
									<% for (AssetCategory a : categories) { %>
										<aui:option value='<%=a.getCategoryId()%>' label='<%=a.getName()%>' />
									<% } %>
								</aui:select>
							</div>
						</div>
					</div>
				<% } %>
			<% } %>
			<% if (statusSearch){%>
				<div class="col-md-6 swt-col-pr-0">
					<div class="row">
						<div class="col-md-2 col-label">
							<label class="pull-right"><strong><%=LanguageUtil.format(request, "status", dummyObj)%></strong></label>
						</div>
						<div class="col-md-10">
							<aui:select name="status" label="<%=StringPool.BLANK%>">
								<% for (ManagementBarFilterItem itm : journalDisplayContext.getManagementBarStatusFilterItems()) {
									if (WorkflowConstants.getLabelStatus(itm.getLabel()) == WorkflowConstants.STATUS_APPROVED) {
										%>
										<aui:option value='<%=WorkflowConstants.getLabelStatus(itm.getLabel())%>' label='Đã xuất bản' />
										<% 
									} else {
										%>
										<aui:option value='<%=WorkflowConstants.getLabelStatus(itm.getLabel())%>' label='<%=itm.getLabel()%>' />
										<% 
									}
								} %>
							</aui:select>
						</div>
					</div>
				</div>
			<% } %>
		
			<% if (badWordsSetId != 0) { %>
				<div class="col-md-6 swt-col-pr-0">
					<div class="row">
						<div class="col-md-offset-2 col-md-10">
							<aui:input type="toggle-switch" name="advs_badWordsSetId" value="<%= badWordsSetId %>"
								checked="false" label="Chỉ hiển thị tin bài có chứa từ xấu" />
						</div>
					</div>
				</div>
			<% } %>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-offset-4 col-md-8">
				<div class="row">
					<div class="col-md-5">
					</div>
					<div class="col-md-3">
						<aui:select name="advs_searchType" label="">
							<aui:option value="1">Tìm kiếm mặc định</aui:option>
							<aui:option value="0">Tìm kiếm gần đúng</aui:option>
						</aui:select>
					</div>
					<div class="col-md-2 swt-col-btn-max-width">
						<aui:button type="submit" icon="icon-search" cssClass="btn-search"
							value='<%=LanguageUtil.format(request, "search", dummyObj)%>' />
					</div>
					<div class="col-md-2 swt-col-btn-max-width">
						<% if (searchCriterias.size() > 0) { %>
							<aui:button id="btnToBasicSearch" icon="icon-chevron-up"
								cssClass="btn hidden-xs" onClick="switchSearchMode(true);"
								value="<%=LABEL_BASIC_SEARCH%>" />
						<% } %>
					</div>
				</div>
			</div>
		</div>
	</aui:form>
</div>

<liferay-frontend:management-bar
	disabled="<%= journalDisplayContext.isDisabledManagementBar() %>"
	includeCheckBox="<%= !user.isDefaultUser() && journalDisplayContext.isShowEditActions() %>"
	searchContainerId="<%= searchContainerId %>"
>
	<liferay-frontend:management-bar-buttons>
		<c:if test="<%= journalDisplayContext.isShowInfoPanel() %>">
			<liferay-frontend:management-bar-sidenav-toggler-button
				icon="info-circle"
				label="info"
			/>
		</c:if>

		<%-- Remove the display styles selection since we just support "list" style.
		<liferay-frontend:management-bar-display-buttons
			displayViews="<%= journalDisplayContext.getDisplayViews() %>"
			portletURL="<%= journalDisplayContext.getPortletURL() %>"
			selectedDisplayStyle="<%= journalDisplayContext.getDisplayStyle() %>"
		/>
		--%>
	</liferay-frontend:management-bar-buttons>

	<%
	String label = null;

	if (journalDisplayContext.isNavigationStructure()) {
		label = LanguageUtil.get(request, "structure") + StringPool.COLON + StringPool.SPACE + HtmlUtil.escape(journalDisplayContext.getDDMStructureName());
	}
	%>

	<liferay-frontend:management-bar-filters>
		<%-- Remove the filter seclection since we've already filtered by structure.
		<liferay-frontend:management-bar-navigation
			label="<%= label %>"
		>
			<portlet:renderURL var="viewArticlesHomeURL">
				<portlet:param name="folderId" value="<%= String.valueOf(JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) %>" />
				<portlet:param name="showEditActions" value="<%= String.valueOf(journalDisplayContext.isShowEditActions()) %>" />
			</portlet:renderURL>

			<liferay-frontend:management-bar-filter-item
				active="<%= journalDisplayContext.isNavigationHome() %>"
				label="all"
				url="<%= viewArticlesHomeURL.toString() %>"
			/>

			<portlet:renderURL var="viewRecentArticlesURL">
				<portlet:param name="navigation" value="recent" />
				<portlet:param name="folderId" value="<%= String.valueOf(JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) %>" />
				<portlet:param name="showEditActions" value="<%= String.valueOf(journalDisplayContext.isShowEditActions()) %>" />
			</portlet:renderURL>

			<liferay-frontend:management-bar-filter-item
				active="<%= journalDisplayContext.isNavigationRecent() %>"
				label="recent"
				url="<%= viewRecentArticlesURL.toString() %>"
			/>

			<portlet:renderURL var="viewMyArticlesURL">
				<portlet:param name="navigation" value="mine" />
				<portlet:param name="folderId" value="<%= String.valueOf(JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) %>" />
				<portlet:param name="showEditActions" value="<%= String.valueOf(journalDisplayContext.isShowEditActions()) %>" />
			</portlet:renderURL>

			<liferay-frontend:management-bar-filter-item
				active="<%= journalDisplayContext.isNavigationMine() %>"
				label="mine"
				url="<%= viewMyArticlesURL.toString() %>"
			/>

			<liferay-frontend:management-bar-filter-item
				active="<%= journalDisplayContext.isNavigationStructure() %>"
				id="structures"
				label="structures"
				url="javascript:;"
			/>
		</liferay-frontend:management-bar-navigation>
		--%>

		<%-- We have already added status filter to advanced search form.
		<liferay-frontend:management-bar-filter
			label="status"
			managementBarFilterItems="<%= journalDisplayContext.getManagementBarStatusFilterItems() %>"
			value="<%= journalDisplayContext.getManagementBarStatusFilterValue() %>"
		/>
		--%>

		<liferay-frontend:management-bar-sort
			orderByCol="<%= journalDisplayContext.getOrderByCol() %>"
			orderByType="<%= journalDisplayContext.getOrderByType() %>"
			orderColumns="<%= journalDisplayContext.getOrderColumns() %>"
			portletURL="<%= journalDisplayContext.getPortletURL() %>"
		/>
	</liferay-frontend:management-bar-filters>

	<liferay-frontend:management-bar-action-buttons>
		<c:if test="<%= journalDisplayContext.isShowInfoPanel() %>">
			<liferay-frontend:management-bar-sidenav-toggler-button
				icon="info-circle"
				label="info"
			/>
		</c:if>

		<liferay-frontend:management-bar-button
			href='<%= "javascript:" + renderResponse.getNamespace() + "deleteEntries();" %>'
			icon='<%= TrashUtil.isTrashEnabled(scopeGroupId) ? "trash" : "times" %>'
			label='<%= TrashUtil.isTrashEnabled(scopeGroupId) ? "recycle-bin" : "delete" %>'
		/>

		<%
		String taglibURL = "javascript:Liferay.fire('" + renderResponse.getNamespace() + "editEntry', {action: 'expireEntries'}); void(0);";
		%>

		<liferay-frontend:management-bar-button
			href="<%= taglibURL %>"
			icon="time"
			label="expire"
		/>

		<%
		taglibURL = "javascript:Liferay.fire('" + renderResponse.getNamespace() + "editEntry', {action: 'moveEntries'}); void(0);";
		%>

		<%-- Remove the "move" function since we don't display folder structure in embedded portlet.
		<liferay-frontend:management-bar-button
			href="<%= taglibURL %>"
			icon="change"
			label="move"
		/>
		--%>
	</liferay-frontend:management-bar-action-buttons>
</liferay-frontend:management-bar>
<aui:input name="folderId" type="hidden" value="<%=String.valueOf(jnFolderId)%>" />
<aui:script>
	function <portlet:namespace />deleteEntries() {
		if (<%= TrashUtil.isTrashEnabled(scopeGroupId) %> || confirm(' <%= UnicodeLanguageUtil.get(request, "are-you-sure-you-want-to-delete-the-selected-entries") %>')) {
			Liferay.fire(
				'<%= renderResponse.getNamespace() %>editEntry',
				{
					action: '<%= TrashUtil.isTrashEnabled(scopeGroupId) ? "moveEntriesToTrash" : "deleteEntries" %>'
				}
			);
		}
	}
</aui:script>

<portlet:resourceURL id="exportArticles" var="exportArticles" />

<aui:script>
	<portlet:renderURL var="viewDDMStructureArticlesURL">
		<portlet:param name="navigation" value="structure" />
		<portlet:param name="folderId" value="<%= String.valueOf(jnFolderId) %>" />
		<portlet:param name="showEditActions" value="<%= String.valueOf(journalDisplayContext.isShowEditActions()) %>" />
	</portlet:renderURL>

	$('#<portlet:namespace />btnExportArticles').on(
		'click',
		function(event) {
			var text = confirm('<%=LanguageUtil.format(request, "swt-journal-web.confirm.export.file", dummyObj)%>');
			if(text){
				var txt = $("input[name*='rowIdsJournalArticle']");
				var ids = '';
				var i;
				for(i = 0; i<txt.length; i++){
					ids = ids + txt[i].value + ',';
				}
				$.ajax({
					type : "POST",
					dataType : 'json',
					data:{
						ids : ids,
						structureKey : '<%=structureKey%>'
					},
					url : '<%=exportArticles%>',
					success : function(data){
						if(data.success == true){
							window.location.href = data.fileURL;
						}else{
							alert('Failed');
						}
					},
					error : function(data) {
						alert('Error!!!!');
					}
				});
			}
		}
	);
	
	$('#<portlet:namespace />structures').on(
		'click',
		function(event) {
			Liferay.Util.openDDMPortlet(
				{
					basePortletURL: '<%= PortletURLFactoryUtil.create(request, PortletProviderUtil.getPortletId(DDMStructure.class.getName(), PortletProvider.Action.VIEW), themeDisplay.getPlid(), PortletRequest.RENDER_PHASE) %>',
					classPK: <%= journalDisplayContext.getDDMStructurePrimaryKey() %>,
					dialog: {
						destroyOnHide: true
					},
					eventName: '<portlet:namespace />selectStructure',
					groupId: <%= themeDisplay.getScopeGroupId() %>,
					mvcPath: '/select_structure.jsp',
					navigationStartsOn: '<%= DDMNavigationHelper.SELECT_STRUCTURE %>',
					refererPortletName: '<%= JournalPortletKeys.JOURNAL + ".filterStructure" %>',

					<%
					Portlet portlet = PortletLocalServiceUtil.getPortletById(portletDisplay.getId());
					%>

					refererWebDAVToken: '<%= HtmlUtil.escapeJS(WebDAVUtil.getStorageToken(portlet)) %>',

					showAncestorScopes: true,
					title: '<%= UnicodeLanguageUtil.get(request, "structures") %>'
				},
				function(event) {
					var uri = '<%= viewDDMStructureArticlesURL %>';

					uri = Liferay.Util.addParams('<portlet:namespace />ddmStructureKey=' + event.ddmstructurekey, uri);

					location.href = uri;
				}
			);
		}
	);
	
	/** Switch the search form to advanced or basic display mode. */
	function switchSearchMode(toBasic) {
		"use strict";

		if (toBasic === true) {
			$('.basic-search-container').css('display', 'inherit');
			$('.basic-search-container :input').prop("disabled", false);
			
			$('.advanced-search-container').css('display', 'none');
			$('.advanced-search-container :input').prop("disabled", true);
			
			$('[id$="advSearch"]').val("false");
		} else {
			$('.basic-search-container').css('display', 'none');
			$('.basic-search-container :input').prop("disabled", true);
			
			$('.advanced-search-container').css('display', 'inherit');
			$('.advanced-search-container :input').prop("disabled", false);
			
			$('[id$="advSearch"]').val("true");
		}

		return false;
	}
	
	<% if (advSearch) { %>
		switchSearchMode(false);
	<% } else { %>
		switchSearchMode(true);
	<% } %>
	
	<% if(!themeDisplay.getURLCurrent().contains("?") && defaultAdvancedSearch){ %>
		switchSearchMode(false);
	<% } %>
	
	// Remove the "Disable" check-boxes of date-pickers.
	$("form[id$='fmSearch2'] span.lfr-input-date").next("div.input-checkbox-wrapper").remove();
	//Replace placeholder
	var placeholder =  $("input[placeholder='dd/thángtháng/nămnămnămnăm']");
	$(placeholder).attr("placeholder", "ngày/tháng/năm");
	
	<!-- START SNV search scores -->
	<% if (swtDisplayStyle == WebContentDisplayStyle.SEARCHSCORES.getCode()){ %>
		 $('label[for="title"]').text("Số báo danh");
		 $('label[for="description"]').text("Họ tên");
	<%}%>
<!-- END SNV search scores -->
</aui:script>

<%!
public static class CategoryUtil {
	
	public static List<AssetCategory> deepSort(List<AssetCategory> items, long parentId, int deepLevel) {
		List<AssetCategory> remain = new ArrayList<>(items);
		List<AssetCategory> result = new ArrayList<>();

		List<Long> ids = new ArrayList<>();
		for (AssetCategory ac : items) {
			ids.add(ac.getCategoryId());
		}

		for (AssetCategory ac : items) {
			if (ac.getParentCategoryId() == parentId) {
				String name = getDeepLevelDisplay(deepLevel) + ac.getName();
				ac.setName(name);
				
				if (!result.stream().anyMatch(m -> m.getCategoryId() == ac.getCategoryId())) {
					result.add(ac);
					remain.remove(ac);
					result.addAll(deepSort(remain, ac.getCategoryId(), deepLevel + 1));
				}
			}
		}

		return result;
	}
	
	public static String getDeepLevelDisplay(int deepLevel) {
		String display = GetterUtil.DEFAULT_STRING;

		for (int i = 0; i <= Integer.MAX_VALUE; i++) {
			if (i == deepLevel) {
				break;
			}
			display = display + StringPool.DOUBLE_DASH + StringPool.SPACE;
		}

		return display;
	}
	
}
%>
<!-- START Feature #4102 : display button to link video server -->
<% if (swtDisplayStyle == WebContentDisplayStyle.VIDEO.getCode()) { %>
<% 
String roleAdminHCM = "0";
List<Role> lst_role =  RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
for(Role object : lst_role){
	if(object.getName().equals("RoleAdminHCM")){
		roleAdminHCM = "1";
	}
}

%>
<script>
function openVideoServer(){
	var domainVideoServer = '<%=PropsUtil.get("video.server.url")%>';
	var userId = '<%=themeDisplay.getUserId()%>';
	var groupId = '<%=themeDisplay.getScopeGroupId()%>';
	var companyId = '<%=themeDisplay.getCompanyId() %>';
	var url = domainVideoServer+ "?userId=" + userId + "&groupId=" + groupId + "&companyId="+ companyId + "&role=" + '<%=roleAdminHCM%>';
	var child =	 window.open(url,'newwin', 'width=' + $(window).width() + ',height=' + $(window).height()+'');
	
}
</script>
<%}%>
<!-- END Feature #4102 : display button to link video server -->