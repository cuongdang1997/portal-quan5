<%@ include file="/init.jsp" %>

<%-- dung.nguyen (2018/11/12): Liferay developer hardcoded the "edit_article" path in portlet source code. --%>
<%-- Thus, we have to update the original "edit_article" file to redirect to our customized file in case of embedded. --%>
<c:choose>
	<c:when test="<%= embedded %>">
		<%@ include file="/edit_article_custom.jsp" %>
	</c:when>
	<c:otherwise>
		<%@ include file="/edit_article_origin.jsp" %>
	</c:otherwise>
</c:choose>