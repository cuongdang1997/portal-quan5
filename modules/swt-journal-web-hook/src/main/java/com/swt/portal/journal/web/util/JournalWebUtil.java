package com.swt.portal.journal.web.util;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.workflow.WorkflowLog;
import com.liferay.portal.kernel.workflow.WorkflowLogManagerUtil;
import com.liferay.portal.kernel.workflow.comparator.WorkflowComparatorFactoryUtil;

public class JournalWebUtil {
	public static List<WorkflowLog> getWorkflowLogs(long companyId, long WorkflowInstanceId)
			throws PortalException {

		List<Integer> logTypes = new ArrayList<>();

		logTypes.add(WorkflowLog.TASK_ASSIGN);
		logTypes.add(WorkflowLog.TASK_COMPLETION);
		logTypes.add(WorkflowLog.TASK_UPDATE);
		logTypes.add(WorkflowLog.TRANSITION);
		
		return WorkflowLogManagerUtil.getWorkflowLogsByWorkflowInstance(companyId, WorkflowInstanceId, logTypes,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, WorkflowComparatorFactoryUtil.getLogCreateDateComparator(true));
	}

	public static String getAssignedToMeMessage(WorkflowLog workflowLog) throws PortalException {

		String message = StringPool.BLANK;

		String userName = getAuditUserName(workflowLog);

		if (workflowLog.getUserId() != 0) {
			User user = UserLocalServiceUtil.getUser(workflowLog.getUserId());

			if (workflowLog.getAuditUserId() == workflowLog.getUserId()) {
				if (user.isMale()) {
					message = userName;
				}
			}
		}

		return message;
	}

	private static String getAuditUserName(WorkflowLog workflowLog) throws PortalException {

		User user = UserLocalServiceUtil.getUser(workflowLog.getAuditUserId());

		return user.getFullName();
	}

}
