<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="swt.questionanswer.form.constants.SwtQuestionanswerFormPortletKeys"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.journal.model.JournalFolder"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMStructure"%>
<%@page import="com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="swt.questionanswer.form.portlet.SwtQuestionanswerConfiguration"%>
<%@page import="com.swt.questionanswer.form.util.DDMStructureUtil"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@page import="javax.portlet.PortletPreferences"%>



<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	List<DDMStructure> listStructure = DDMStructureLocalServiceUtil.getStructures();
	List<JournalFolder> listJournalFolder = DDMStructureUtil.getJournalFolder(themeDisplay.getScopeGroupId(),
	themeDisplay.getCompanyId());
	List<AssetVocabulary> listVocabulary = AssetVocabularyLocalServiceUtil
	.getCompanyVocabularies(themeDisplay.getCompanyId());
	SwtQuestionanswerConfiguration formConfiguration = (SwtQuestionanswerConfiguration) renderRequest
	.getAttribute(SwtQuestionanswerConfiguration.class.getName());
	
	String structureForm = "";
	String vocabulary = "";
	String labelSubmit = "";
	boolean buttonBack = true;
	
	if (Validator.isNotNull(formConfiguration)) {
		structureForm = portletPreferences.getValue("structureForm", formConfiguration.structureForm());
		vocabulary = portletPreferences.getValue("vocabulary", formConfiguration.vocabulary());
		labelSubmit = portletPreferences.getValue("labelSubmit", formConfiguration.labelSubmit());
		buttonBack = GetterUtil.getBoolean(portletPreferences.getValue("buttonBack", formConfiguration.buttonBack()));
	}
	
	
	StringBuilder sb = new StringBuilder();
    for (String n : SwtQuestionanswerFormPortletKeys.ALLOWED_ATTACHMENT_TYPES) {
    	sb.append(n).append(",");
    }
    sb.deleteCharAt(sb.length() - 1);

    String inputFileAcceptTypes = sb.toString();
%>