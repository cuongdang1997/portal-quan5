<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="init.jsp"%>

	<style>
		.css-config{
			margin-top: 20px;
			padding: 15px;
		}
	</style>

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%=true%>" var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" class="form"
	enctype="multipart/form-data" name="<portlet:namespace/>fm" id="myFormConfig" >
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden"
			value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden"
			value="<%=configurationRenderURL%>" />
		<aui:fieldset cssClass="panel panel-default css-config">
			<%=LanguageUtil.format(request, "swt-questionanswer-from.chon-cau-truc-form", new Object())%>
			<aui:select id="structureForm" name="structureForm"  label=""  value="<%=structureForm%>">
				 <aui:option value="">
				 ---<%=LanguageUtil.format(request, "swt-questionanswer-from.chon-cau-truc-form", new Object())%>---
				 </aui:option> ---
				<%
				if(listStructure.size()!=0){
					
					for (DDMStructure object : listStructure) {
									if (object.getCompanyId() == themeDisplay.getCompanyId()&& object.getUserName()!="") {
				%>
				
				<aui:option value="<%=String.valueOf(object.getStructureId()) %>"><%=object.getName(themeDisplay.getLocale())%></aui:option>
				<%
					}
									}
					
								}
				%>
			</aui:select>
			<%=LanguageUtil.format(request, "swt-questionanswer-from.chon-linh-vuc", new Object())%>
			<aui:select id="vocabulary" name="vocabulary" label=""  value="<%=vocabulary%>">
			<aui:option value="">
				 ---<%=LanguageUtil.format(request, "swt-questionanswer-from.chon-linh-vuc", new Object())%>---
				 </aui:option>
				<%
				if(listVocabulary.size()!=0){
					for (AssetVocabulary object : listVocabulary) {
				%>
				<aui:option value="<%=String.valueOf(object.getVocabularyId()) %>"><%=object.getName()%></aui:option>
				<%
					}
					}
					
				%>
			</aui:select>
			<%=LanguageUtil.format(request, "swt-questionanswer-from.luu", new Object())%>
			<aui:input name="labelSubmit" label="" value="<%=labelSubmit %>"></aui:input>
			
			<%=LanguageUtil.format(request, "swt-questionanswer-from.cau.hinh.quay-lai", new Object())%>
			<aui:input type="checkbox" name="buttonBack" label="" checked="<%=buttonBack%>"></aui:input>

		</aui:fieldset>
		<div class="button-holder dialog-footer">
			<button type="submit" class="btn btn-lg btn-primary btn-default">
			<span class="lfr-btn-label"><%=LanguageUtil.format(request, "swt-questionanswer-from.config.OK", new Object())%></span>
			</button>
		</div>
	</div>
</aui:form>
