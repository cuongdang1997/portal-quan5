<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategoryProperty"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetLinkLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMFormField"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMTemplate"%>
<%@page import="com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.service.LayoutFriendlyURLLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.service.persistence.LayoutFinder"%>
<%@page import="com.liferay.portal.kernel.service.persistence.LayoutUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.swt.questionanswer.form.util.DDMStructureUtil"%>

<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>

<%@page import="javax.swing.ViewportLayout"%>

<%@ include file="init.jsp" %>

<style>
	.total-question-answer {
		padding-left: 10px;
		padding-top: 10px;
		color: #d70000 !important;
		font-size: 14px;
		font-style: normal;
		font-weight: bold;
		font-family: Arial, Helvetica, sans-serif;
		TEXT-DECORATION: none;
	}
	
	#form-question-answer {
		padding: 0px 10px 0px 10px;
	}
	
	.warning-validator {
		float: left;
		width: 100%;
		display: none;
	}
	
	.lfr-portal-tooltip {
		width: 10%;
		margin-top: 10px;
	}

	.lfr-portal-tooltip a {
		font-size: 22px;
		margin-left: 30%;
	}
	
	.text-warning-required {
		color: #f5984c;
	}
	
	.col-form-label {
		padding-right: 0px !important;
		padding-left: 5px !important;
		margin-top: 10px;
	}
	
	.textarea{
		height: 120px !important;
		resize: vertical;
	}
</style>

<liferay-ui:success key="addQuestionSuccessfully" message="swt-questionanswer-from.addQuestionSuccessfully" />

<liferay-ui:error key="captchaError" message="swt-questionanswer-from.captchaNotCorrect" />
<liferay-ui:error key="addQuestionSuccessfully" message="swt-questionanswer-from.addQuestionFailed" />
<liferay-ui:error key="attachmentFileTypeInvalid" message="swt-questionanswer-from.attachmentFileTypeInvalid" />
<liferay-ui:error key="attachmentFileSizeInvalid" message="swt-questionanswer-from.attachmentFileSizeInvalid" />

<% if (structureForm != null && structureForm.length() != 0) { %>
	<%
		DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureForm));
		List<DDMFormField> listFile = DDMStructureUtil.getDDMFormFieldByIdStructure(structureForm);
	%>
	
	<portlet:actionURL name="addQuestion" var="formQuestionURL" />
	
	<div id="form-question-answer">
		<div class="warning-validator">
			<div class="alert alert-danger" role="alert">
				<span class="glyphicon glyphicon-warning-sign"></span>
				<span id="erroFeedBack"> </span>
			</div>
		</div>
		
		<aui:form cssClass="form-horizontal" action="<%= formQuestionURL %>" name="fm" enctype="multipart/form-data" method="POST" >
			<aui:input type="hidden" name="urlCurrent" value="<%=themeDisplay.getURLCurrent()%>" ></aui:input>
			<aui:input type="hidden" name="structureKey" value="<%=structure.getStructureKey()%>" ></aui:input>
			<aui:input type="hidden" name="structureId" value="<%=structureForm%>" ></aui:input>
			
			<!-- title -->
			<div class="row-fluid">
				<label class="col-form-label col-sm-2"><%=LanguageUtil.format(request, "swt-questionanswer-from.form.title", new Object())%>
					<span class="icon-asterisk text-warning-required">
						<span class="hide-accessible">*</span>
					</span>
				</label>
				<div class="col-sm-10 input-padding-left">
					<aui:input cssClass="field form-control" name="titleName" type="text" label="" ></aui:input>
				</div>
			</div>
			<!-- end title -->

			<% for (DDMFormField object :listFile) { %>
				<% if (!object.getType().equals("ddm-documentlibrary") && !object.getName().contains("answer")) { %>
					<div class="row-fluid" id="group-<%=object.getName()%>">
						<label class="col-form-label col-sm-2 col-md-2">
							<%= object.getLabel().getString(themeDisplay.getLocale()) %>
							
							<% if (object.getName().equals("name") || object.getName().equals("email")) { %>
								<span class="icon-asterisk text-warning-required">
									<span class="hide-accessible">*</span>
								</span>
							<% } %>
						</label>
						<div class="col-sm-10 col-md-10">
							<% if (object.getType().equals("text")) { %>
								<aui:input type="text" class="field form-control" name="<%=object.getName()%>" id="<%=object.getName()%>" label="" ></aui:input>
							<% } else if (object.getType().equals("radio")) { %>
								<%
									String valueDefault = "";
									Map<Locale, String> mapValue = object.getPredefinedValue().getValues();
									
									for (Map.Entry<Locale, String> s: mapValue.entrySet()) {
										valueDefault = s.getValue();
									}
								%>
								<div class="clearfix" style="margin-left: -15px;">
									<% for (String s : object.getDDMFormFieldOptions().getOptionsValues()) { %>
										<div class="radio" style="padding-top: 0px;">
											<label>
												<% if (valueDefault.contains(s)) { %>
													<aui:input type="radio" value="<%=s%>" cssClass="field" name="<%=object.getName()%>" label="" checked="true" />
												<% } else { %>
													<aui:input type="radio" value="<%=s%>" cssClass="field" name="<%=object.getName()%>" label=""/>
												<% } %>
												<%= object.getDDMFormFieldOptions().getOptionLabels(s).getString(themeDisplay.getLocale()) %>
											</label>
										</div>
									<% } %>
								</div>
							<% } else if (object.getType().equals("textarea")) { %>
								<aui:input type="textarea" cssClass="field form-control textarea" id="<%=object.getName()%>" name="<%=object.getName()%>" label=""  ></aui:input>
							<% } %>
						</div>
					</div>
				<% } %>
			<% } %>

			<!-- Vocabulary options -->
			<aui:input type="hidden" name="vocabulary" value="<%=vocabulary%>" ></aui:input>
			
			<% if (vocabulary != null && vocabulary.length() != 0) { %>
				<%
				List<AssetCategory> tempCategory = DDMStructureUtil.getListCategoryByIdVocabulary(themeDisplay, vocabulary);
				List<AssetCategory> listCategory = new ArrayList<>();
				AssetCategoryProperty statusProp = null;
				
				// Filter the inactive categories.
				for (AssetCategory ac : tempCategory) {
					try {
						statusProp = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(ac.getCategoryId(), "status");
					} catch (PortalException e) {
						statusProp = null;
					}
					if (statusProp != null && statusProp.getValue().equals("1")) {
						listCategory.add(ac);
					}
				}
				%>
				<div class="row-fluid">
					<label class="col-form-label col-sm-2">
						<%=LanguageUtil.format(request, "swt-questionanswer-from.form.category", new Object())%>
					</label>
					<div class="col-sm-10 input-padding-left">
						<aui:select name="categoryId" cssClass="form-control" label="">
							<% for(AssetCategory objectField : listCategory) { %>
								<aui:option value="<%=objectField.getCategoryId()%>">
									<%= objectField.getName() %>
								</aui:option>
							<% } %>
						</aui:select>
					</div>
				</div>
			<% } %>

			<!-- Attachment file -->
			<div class="row-fluid" id="group-documentlibrary-id">
				<label class="col-form-label col-sm-2">
					<%=LanguageUtil.format(request, "swt-questionanswer-from.file-dinh-kem", new Object())%>
				</label>
				<div class="col-sm-10 input-padding-left">
					<aui:input type="file" name="attachment" cssClass="field form-control" id="attachmentId" label="" onchange="return checkAttachment()"></aui:input>
				</div>
				<div class="row" style="margin-left: 28%; margin-top: 6%">
					<p style="color: red; font-size: 14px; font-family: italic; margin-left: -16%;" id="erroFilemin"></p>
				</div>
			</div>

			<!-- captcha -->
			<div class="row-fluid">
				<label class="col-form-label col-sm-2">
					<%=LanguageUtil.format(request, "swt-questionanswer-from.ma-captcha", new Object())%>
				</label>
				<div class="col-sm-10 input-padding-left" style="padding-left: 0; display: flex; margin: 0 0 10px 0; height: 50px;">
					<portlet:resourceURL var="captchaURL" />
					<img alt="captcha" class="captcha" id="captchaImg" src="${captchaURL}" />
					<liferay-ui:icon cssClass="refresh" iconCssClass="icon-refresh"
						id="refreshCaptcha" label="<%= false %>"
						localizeMessage="<%= true %>" message="refresh-captcha"
						url="javascript:;" />
					<aui:input style="height: 50px; width: auto; margin-left: 17px;"
						name="captchaText" id="captchaText" size="10"
						type="text" value="" required="required" label="" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-form-label col-sm-2"></div>
				<div class="col-sm-10 input-padding-left" style="padding: 10px;">
					<button id="buttonSumit" type="button" onclick="requestForm()" class="btn btn-primary btn-default">
						<%= labelSubmit %>
					</button>
					
					<% if (buttonBack == true) { %>
					<button type="button" class="btn btn-default" onclick="backListQuestion()">
						<%=LanguageUtil.format(request, "swt-questionanswer-from.quay-lai", new Object())%>
					</button>
					<% } %>
				</div>
			</div>
		</aui:form>
	</div>

	<aui:script>
	    $('#<portlet:namespace />refreshCaptcha').on("click", function(ev) {
	        $("#captchaImg").attr('src', '${captchaURL}&force=' + encodeURIComponent(Math.floor(Math.random() * Math.pow(2, 53))));
	    });
	</aui:script>

	<script type="text/javascript">
		var allowedFileTypes = [];
		<% for (String ft : SwtQuestionanswerFormPortletKeys.ALLOWED_ATTACHMENT_TYPES) { %>
			allowedFileTypes.push("<%= ft %>");
		<% } %>
	
		//check form before send 
		function requestForm() {
			$(".warning-validator").slideUp();
			
			var title = $('#<portlet:namespace />titleName').val();
			var name = $('#<portlet:namespace />name').val();
			var email = $('#<portlet:namespace />email').val();
			var attachment = $('#<portlet:namespace />attachmentId').val();
			var captchaText = $('#<portlet:namespace />captchaText').val();
			
			if (name == '' || email == '' || title == '' ) {
				$("#erroFeedBack").empty();
				$("#erroFeedBack").text("<%=LanguageUtil.format(request, "swt-questionanswer-from.vui-long-nhap-cac-thong-tin-da-danh-dau", new Object())%>");
				$(".warning-validator").slideDown();
				return false;
			}
			
			if (!checkEmailFormat(email)) {
				$("#erroFeedBack").empty();
				$("#erroFeedBack").text("<%=LanguageUtil.format(request, "swt-questionanswer-from.nhap-dung-dinh-dang-email", new Object())%>");
				$(".warning-validator").slideDown();
				return false;
			}
			
			if (captchaText == "") {
				$("#erroFeedBack").empty();
				$("#erroFeedBack").text('<%=LanguageUtil.format(request, "swt-questionanswer-from.vui-long-nhap-captcha", new Object())%>');
				$(".warning-validator").slideDown();
				return false;
			}
			
			$('#<portlet:namespace />fm').submit();
		}
		
		function checkEmailFormat(email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
		}
		
		function checkAttachment() {
			return;
			var checkFile = document.getElementById("<portlet:namespace/>attachmentId").value;
			var fileSize = '';
			if (checkFile != "") {
				fileSize = document.getElementById("<portlet:namespace/>attachmentId").files[0].size;
			}
			var idxDot = checkFile.lastIndexOf(".");
			var extFile = checkFile.substr(idxDot, checkFile.length).toLowerCase();
			
	        var erroFilemin = document.getElementById("erroFilemin");
	        
	        if (fileSize > <%= 5 * 1024 * 1024 %> ) {
	        	erroFilemin.innerHTML = "<%=LanguageUtil.format(request, "swt-questionanswer-from.attachmentFileSizeInvalid", new Object())%>";
				document.getElementById("buttonSumit").disabled = true;
			}  else if(allowedFileTypes.includes(extFile) == false && checkFile != "" ){
				erroFilemin.innerHTML = "<%=LanguageUtil.format(request, "swt-questionanswer-from.attachmentFileTypeInvalid", new Object())%>";
				document.getElementById("buttonSumit").disabled = true;
			} else {
				erroFilemin.innerHTML = "";
				document.getElementById("buttonSumit").disabled = false;
			}
		}
	</script>
	
<% } else { %>
	<div class="alert alert-danger" role="alert">
		<span class="glyphicon glyphicon-warning-sign"></span>
		<%= LanguageUtil.format(request, "swt-questionanswer-from.chua-cau-hinh", new Object()) %>
	</div>
<% } %>