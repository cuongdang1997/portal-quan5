package com.swt.questionanswer.form.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.StringPool;

public class DocumentMediaUtil {

	public static long createFolderMedia(ThemeDisplay themeDisplay, ServiceContext serviceContext, String nameFolder) {
		boolean isFolderNotExist = false;
		DLFolder folder = null;
		long folderId = 0;

		try {
			folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, nameFolder);
			folderId = folder.getFolderId();
		} catch (PortalException e) {
			isFolderNotExist = true;
		}

		if (isFolderNotExist) {
			try {
				folder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						themeDisplay.getScopeGroupId(), false, 0, nameFolder, "", false, serviceContext);
			} catch (PortalException e) {
				e.printStackTrace();
			}

			if (folder != null) {
				folderId = folder.getFolderId();
				ResourceAction actionObject = ResourceActionLocalServiceUtil
						.fetchResourceAction(DLFolder.class.getName(), ActionKeys.ADD_DOCUMENT);
				createResourcePermission(themeDisplay, DLFolder.class.getName(), folderId,
						String.valueOf(actionObject.getResourceActionId()));
			}
		}

		return folderId;
	}
	
	public static ResourcePermission createResourcePermission(ThemeDisplay themeDisplay, String name,
			long resourcePrimKey, String permissions) {
		long resourcePermissionId = CounterLocalServiceUtil.increment();
		

		ResourcePermission perm = ResourcePermissionLocalServiceUtil.createResourcePermission(resourcePermissionId);
		Role role = RoleLocalServiceUtil.fetchRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
		try {
			ResourcePermissionLocalServiceUtil.addResourcePermission(themeDisplay.getCompanyId(), name, ResourceConstants.SCOPE_GROUP, String.valueOf(resourcePrimKey), role.getRoleId(), String.valueOf(resourcePrimKey));
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return perm;
	}
	

	public static String addFileAttachment(ThemeDisplay themeDisplay, long folderIdAttachment, String fileName,
			File file, ServiceContext serviceContext, String nameFolder) throws Exception {
		String path = StringPool.BLANK;

		FileEntry fileEntry = null;
		long folderId = createFolderByDayMonthYear(themeDisplay, serviceContext, nameFolder);

		if (file != null) {
			fileEntry = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(),folderId, file.getAbsoluteFile().getName(), MimeTypesUtil.getContentType(fileName), fileName, "", "", file, serviceContext);
			path = "/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/" + fileEntry.getFileName()
					+ "/" + fileEntry.getUuid();
		}

		return path;
	}

	
	
	private static long createFolderFather(ThemeDisplay themeDisplay, ServiceContext serviceContext, String nameFolder) throws Exception {
		DLFolder folder = null;
		long folderId = 0L;
		try {
			folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0L, nameFolder);
			folderId = folder.getFolderId();
		} catch (PortalException e) {
			folder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
					themeDisplay.getScopeGroupId(), false, 0L, nameFolder, "", false, serviceContext);
			folderId = folder.getFolderId();
		}
		
		return folderId;
	}
		

	public static long createFolderByDayMonthYear(ThemeDisplay themeDisplay, ServiceContext serviceContext,
			String nameFolder) throws Exception {
		Calendar calendar = Calendar.getInstance();
	    int nam = calendar.get(1);
	    int thang = calendar.get(2) + 1;
	    int ngay = calendar.get(5);
		long folderId = createFolderFather(themeDisplay, serviceContext, nameFolder);

		DLFolder folderNgay = null;

		if (!CheckNam(nam, themeDisplay, folderId)) {
			DLFolder folderNam = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false, folderId,
					String.valueOf(nam), "", false, serviceContext);

			DLFolder folderThang = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false, folderNam.getFolderId(),
					String.valueOf(thang), "", false, serviceContext);

			folderNgay = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
					themeDisplay.getScopeGroupId(), false, folderThang.getFolderId(), String.valueOf(ngay), "", false,
					serviceContext);
		} else {
			DLFolder foldernam = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), folderId,
					String.valueOf(nam));

			if (!CheckThangNgay(foldernam.getFolderId(), thang, themeDisplay)) {
				DLFolder folderThang = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
						themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false, foldernam.getFolderId(),
						String.valueOf(thang), "", false, serviceContext);
				folderNgay = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
						themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false,
						folderThang.getFolderId(), String.valueOf(ngay), "", false, serviceContext);
			} else {
				DLFolder folderthang = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
						foldernam.getFolderId(), String.valueOf(thang));

				if (!CheckThangNgay(folderthang.getFolderId(), ngay, themeDisplay)) {
					folderNgay = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(),
							themeDisplay.getScopeGroupId(), themeDisplay.getScopeGroupId(), false,
							folderthang.getFolderId(), String.valueOf(ngay), "", false, serviceContext);
				} else {
					folderNgay = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
							folderthang.getFolderId(), String.valueOf(ngay));
				}
			}
		}
		return folderNgay.getFolderId();
	}

	private static boolean CheckNam(int nam, ThemeDisplay themeDisplay, long folderId) {

		boolean valueReturn = false;
		DLFolder folder = null;
		try {
			folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), folderId, String.valueOf(nam));
			if (folder != null) {
				valueReturn = true;
			}

		} catch (PortalException e) {
			valueReturn = false;
		}
		return valueReturn;
	}

	private static boolean CheckThangNgay(long nam, int thang, ThemeDisplay themeDisplay) {
		boolean valueReturn = false;
		DLFolder folder = null;
		try {
			folder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), nam, String.valueOf(thang));

			if (folder != null) {
				valueReturn = true;
			}
		} catch (PortalException e) {
			valueReturn = false;
		}
		return valueReturn;
	}
}
