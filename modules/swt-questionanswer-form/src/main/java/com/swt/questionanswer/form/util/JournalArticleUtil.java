package com.swt.questionanswer.form.util;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

public class JournalArticleUtil {

	public static void createJournalArticle(ThemeDisplay themeDisplay, ActionRequest request, String structure,
			String structureId, String titleName, String contentName, String contentLast, long categoryId)
			throws PortalException {
		double version = 1;
		String articleId = String.valueOf(CounterLocalServiceUtil.increment());

		// @param titleMap
		Map<Locale, String> titleMap = new HashMap<Locale, String>();
		titleMap.put(themeDisplay.getLocale(), titleName);

		// @param descriptionMap
		Map<Locale, String> contentMap = new HashMap<Locale, String>();
		contentMap.put(themeDisplay.getLocale(), contentName);

		// @param ddmTemplateKey
		DDMStructure structureObjetc = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureId));
		List<DDMTemplate> listTemplate = structureObjetc.getTemplates();
		String ddmTemplateKey = "";

		if (listTemplate.size() != 0) {
			if (listTemplate.get(0).getType().equals("display")) {
				ddmTemplateKey = listTemplate.get(0).getTemplateKey();
			}
		}
		// @param layoutUuid
		String layoutUuid = "";
		List<Layout> listLayoutParrent = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), false,
				themeDisplay.getLayout().getLayoutId());
		if (listLayoutParrent.size() != 0) {
			for (Layout object : listLayoutParrent) {
				if (object.isContentDisplayPage()) {
					layoutUuid = object.getUuid();
				}
			}
		}
		// @param folderIdJounalArticle
		long folderId = 0;
		try {
			folderId = DDMStructureUtil.getIdJournalFolderByStructureName(themeDisplay, request,
					structureObjetc.getName(themeDisplay.getLocale()));
		} catch (Exception e) {
		}
		// Map image
		Map<String, byte[]> byteImage = null;
		ServiceContext serviceContextArticle = ServiceContextFactory.getInstance(JournalArticle.class.getName(),
				request);
		serviceContextArticle.setWorkflowAction(WorkflowConstants.ACTION_SAVE_DRAFT);
		File file = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		try {

			JournalArticle objectArticle = JournalArticleLocalServiceUtil.addArticle(themeDisplay.getUserId(),
					themeDisplay.getScopeGroupId(), GetterUtil.getLong(folderId), GetterUtil.getLong(0),
					structureObjetc.getPrimaryKey(), articleId, true, version, titleMap, contentMap, contentLast,
					structure, ddmTemplateKey, layoutUuid, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),
					cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), 0, 0, 0, 0, 0,
					true, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR),
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false, true, false, "", file, byteImage,
					"", serviceContextArticle);
			AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(),
					objectArticle.getResourcePrimKey());
			AssetEntryLocalServiceUtil.addAssetCategoryAssetEntry(categoryId, entry.getEntryId());

		} catch (Exception e) {
		}

	}

	public static String setContent(List<DDMFormField> listFormFile, Map<String, String> listParameter,
			String attachment) {

		String contentArticle = "";
		String type = "";
		for (DDMFormField object : listFormFile) {
			if (object.getType().equals("textarea")) {
				type = "text_box";
			}
			if (object.getType().equals("ddm-documentlibrary")) {
				type = "document_library";
			}
			if (object.getType().equals("select")) {
				type = "list";
			}
			if (object.getType().equals("radio")) {
				type = "radio";
			}
			if (object.getType().equals("text")) {
				type = "text";
			}
			for (Map.Entry<String, String> s : listParameter.entrySet()) {
				if (object.getName() == s.getKey()) {
					contentArticle += "	<dynamic-element name=\"" + object.getName() + "\" type=\"" + type
							+ "\" index-type=\"" + object.getIndexType() + "\" instance-id=\"" + object.getName()
							+ "\">\r\n" + "		<dynamic-content language-id=\"vi_VN\"><![CDATA[" + s.getValue()
							+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n";
				}
			}
		}
		String xml = "<?xml version=\"1.0\"?>\r\n" + "\r\n"
				+ "<root available-locales=\"vi_VN\" default-locale=\"vi_VN\">\r\n" + contentArticle + "</root>";

		return xml;

	}
}
