package com.swt.questionanswer.form.util;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;

public class DDMStructureUtil {

	public static List<DDMFormField> getDDMFormFieldByIdStructure(String idStructure) {
		DDMStructure structure = null;
		
		try {
			structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(idStructure));
		} catch (PortalException e) {
		}

		// Get the DDM structure's fields.
		List<DDMFormField> fields = new ArrayList<DDMFormField>();
		if (structure != null) {
			fields = structure.getDDMFormFields(false);
		}
		return fields;
	}
	/*@param structureName from name of structure
	 * return idFolderJournal
	 * */
	public static long getIdJournalFolderByStructureName(ThemeDisplay themeDisplay,ActionRequest request, String structureName) throws Exception {
		long idFolder =0;
		ServiceContext serviceContext = ServiceContextFactory.getInstance(JournalFolder.class.getName(),
				request);
		JournalFolder journalFolder   = null;
		try {
			journalFolder =	JournalFolderLocalServiceUtil.fetchFolder(themeDisplay.getScopeGroupId(), structureName);
			idFolder =	journalFolder.getFolderId(); 
		} catch (Exception e) {
			journalFolder = JournalFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(), GetterUtil.getLong(0), structureName, structureName, serviceContext);
			idFolder =	journalFolder.getFolderId();
		}
		
		return idFolder;
		
	}
	public static List<JournalFolder> getJournalFolder(long groupId,long idCompany) {

		List<JournalFolder> resultList = new ArrayList<JournalFolder>();
		JournalFolderLocalServiceUtil.getFolders(groupId);
		List<JournalFolder> fields = JournalFolderLocalServiceUtil.getFolders(groupId);
		if (fields.size() != 0) {
			for (JournalFolder object : fields) {
				if (object.getCompanyId() == idCompany) {
					resultList.add(object);
				}
			}
		}

		return resultList;
	}
	public static List<AssetCategory> getListCategoryByIdVocabulary(ThemeDisplay themeDisplay, String vocabularyId) throws Exception{
		List<AssetCategory> listCate = new ArrayList<AssetCategory>();
		DynamicQuery dynamic = DynamicQueryFactoryUtil.forClass(AssetCategory.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(themeDisplay.getCompanyId()))
				.add(PropertyFactoryUtil.forName("vocabularyId").eq(GetterUtil.getLong(vocabularyId)));
		listCate = AssetCategoryLocalServiceUtil.dynamicQuery(dynamic);
		List<AssetCategoryProperty> listAssetCategoryProperty = new ArrayList<AssetCategoryProperty>();
		if(listCate.size()!=0) {
			for(AssetCategory object : listCate) {
				AssetCategoryProperty categoryProperty = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(object.getCategoryId(), "status");
				if(categoryProperty.getValue().equals("1")) {
					listAssetCategoryProperty.add(categoryProperty);
				}
			}
			
		}
		List<AssetCategory> listCateReturn = new ArrayList<AssetCategory>();
		if(listAssetCategoryProperty.size()!=0) {
			for(AssetCategoryProperty objectPoperty : listAssetCategoryProperty) {
				AssetCategory objectLast = AssetCategoryLocalServiceUtil.getAssetCategory(objectPoperty.getCategoryId());
				listCateReturn.add(objectLast);
			}
		}
		return listCateReturn;
		
		
		
	}
}
