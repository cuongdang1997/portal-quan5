package swt.questionanswer.form.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import swt.questionanswer.form.constants.SwtQuestionanswerFormPortletKeys;

@Component(configurationPid = "swt.questionanswer.form.portlet.SwtQuestionanswerConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true, property = {
		"javax.portlet.name="
				+ SwtQuestionanswerFormPortletKeys.SwtQuestionanswerFormPortlet }, service = ConfigurationAction.class)
public class SwtQuestionanswerConfigurationAction extends DefaultConfigurationAction {

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		String structureForm = ParamUtil.getString(actionRequest, "structureForm");
		setPreference(actionRequest, "structureForm", structureForm);

		String vocabulary = ParamUtil.getString(actionRequest, "vocabulary");
		setPreference(actionRequest, "vocabulary", vocabulary);

		String labelSubmit = ParamUtil.getString(actionRequest, "labelSubmit");
		setPreference(actionRequest, "labelSubmit", labelSubmit);

		boolean buttonBack = ParamUtil.getBoolean(actionRequest, "buttonBack");
		setPreference(actionRequest, "buttonBack", String.valueOf(buttonBack));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {

		httpServletRequest.setAttribute(SwtQuestionanswerConfiguration.class.getName(), _formConfiguration);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_formConfiguration = ConfigurableUtil.createConfigurable(SwtQuestionanswerConfiguration.class, properties);
	}

	private volatile SwtQuestionanswerConfiguration _formConfiguration;

}
