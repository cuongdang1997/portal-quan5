package swt.questionanswer.form.portlet;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.swt.questionanswer.form.util.DDMStructureUtil;
import com.swt.questionanswer.form.util.DocumentMediaUtil;
import com.swt.questionanswer.form.util.JournalArticleUtil;

import swt.questionanswer.form.constants.SwtQuestionanswerFormPortletKeys;

/**
 * @author Nhut.Dang
 */
@Component(configurationPid = "swt.questionanswer.form.portlet.SwtQuestionanswerConfiguration", immediate = true, property = {
		"com.liferay.portlet.display-category=category.portal", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=" + SwtQuestionanswerFormPortletKeys.SwtQuestionanswerForm,
		"javax.portlet.init-param.config-template=/configuration.jsp", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + SwtQuestionanswerFormPortletKeys.SwtQuestionanswerFormPortlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class SwtQuestionanswerFormPortlet extends MVCPortlet {

	private volatile SwtQuestionanswerConfiguration formConfiguration;

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		renderRequest.setAttribute(SwtQuestionanswerConfiguration.class.getName(), formConfiguration);
		super.doView(renderRequest, renderResponse);
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		formConfiguration = ConfigurableUtil.createConfigurable(SwtQuestionanswerConfiguration.class, properties);
	}

	// add question
	public void addQuestion(ActionRequest request, ActionResponse response) throws PortalException, Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext = new ServiceContext();

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		String structureKey = uploadPortletRequest.getParameter("structureKey");
		String structureId = uploadPortletRequest.getParameter("structureId");
		String currentUrl = uploadPortletRequest.getParameter("urlCurrent");
		String vocabulary = uploadPortletRequest.getParameter("vocabulary");
		String titleName = uploadPortletRequest.getParameter("titleName");
		String contentName = uploadPortletRequest.getParameter("contentName");
		String enterdCaptcha = uploadPortletRequest.getParameter("captchaText");

		boolean checkCaptcha = checkCaptcha(request, enterdCaptcha);

		if (checkCaptcha) {
			try {

				File attachment = uploadPortletRequest.getFile("attachment");
				String attachmentName = GetterUtil
						.getString(uploadPortletRequest.getFileName("attachment"), StringPool.BLANK).trim();
				long attachmentSize = uploadPortletRequest.getSize("attachment");

				long categoryId = 0;
				String attachmentUrl = "";

				// check attachment
				if (attachmentSize != 0 && attachmentName.length() != 0) {
					// Prevent duplicate file entry exception.
					attachmentName = System.currentTimeMillis() + " " + attachmentName;

					// Check if file size is vailid.
					if (attachmentSize > SwtQuestionanswerFormPortletKeys.FILE_SIZE_BYTES) {
						SessionErrors.add(request, "attachmentFileSizeInvalid");
						PortalUtil.copyRequestParameters(request, response);
						// response.sendRedirect(currentUrl);
						return;
					}

					String attachmentLowerName = attachmentName.toLowerCase();
					boolean isFileTypeAllowed = false;

					// Check if file extension is valid.
					for (String ft : SwtQuestionanswerFormPortletKeys.ALLOWED_ATTACHMENT_TYPES) {
						if (attachmentLowerName.endsWith(ft)) {
							isFileTypeAllowed = true;
							break;
						}
					}
					if (!isFileTypeAllowed) {
						SessionErrors.add(request, "attachmentFileTypeInvalid");
						PortalUtil.copyRequestParameters(request, response);
						// response.sendRedirect(currentUrl);
						return;
					}

					// Save file to Document and Library.DDMStructure structure = null;

					DDMStructure structure = null;

					try {
						structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureId));
					} catch (PortalException e) {
					}
					String nameFolder = structure.getName(LocaleUtil.toLanguageId(themeDisplay.getLocale()).toString());
					long folderIdAttachment = DocumentMediaUtil.createFolderMedia(themeDisplay, serviceContext,nameFolder);
					attachmentUrl = DocumentMediaUtil.addFileAttachment(themeDisplay, folderIdAttachment,
							attachmentName, attachment, serviceContext, nameFolder);
				}

				// check category
				if (vocabulary != null && vocabulary.length() != 0) {
					categoryId = GetterUtil.getLong(uploadPortletRequest.getParameter("categoryId"));
				}

				// key: field name; value = field value
				Map<String, String> articleParameters = new HashMap<String, String>();
				List<DDMFormField> articleFormFields = DDMStructureUtil.getDDMFormFieldByIdStructure(structureId);

				for (DDMFormField formField : articleFormFields) {
					if (formField.getName().equals("attachment")) {
						articleParameters.put(formField.getName(), attachmentUrl);
					} else {
						if (formField.getName().equals("attachmentAnswer")) {
							articleParameters.put(formField.getName(), "");
						} else {
							if (formField.getName().contains("answer")) {
								articleParameters.put(formField.getName(), "");
							} else {
								if (formField.getType().equals("radio")) {
									articleParameters.put(formField.getName(), "[\""
											+ uploadPortletRequest.getParameter(formField.getName()).trim() + "\"]");
								} else {
									articleParameters.put(formField.getName(),
											uploadPortletRequest.getParameter(formField.getName()).trim());
								}
							}
						}
					}
				}
				
				//Send e-mail
				String emailAdress = uploadPortletRequest.getParameter("email").trim();
				if(!emailAdress.equals(StringPool.BLANK)) {
					String subjectMail = uploadPortletRequest.getParameter("titleName").trim();
					Properties props = new Properties();
					
					props.put("mail.smtp.host", PropsUtil.get(PropsKeys.MAIL_SESSION_MAIL_SMTP_HOST));
					props.put("mail.smtp.socketFactory.port", PropsUtil.get(PropsKeys.MAIL_SESSION_MAIL_SMTP_PORT));
					props.put("mail.smtp.socketFactory.class",
							"javax.net.ssl.SSLSocketFactory");
					props.put("mail.smtp.auth", "true");
					props.put("mail.smtp.port", PropsUtil.get(PropsKeys.MAIL_SESSION_MAIL_SMTP_PORT));
					
					javax.mail.Session session = javax.mail.Session.getInstance(props, new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(PropsUtil.get(PropsKeys.MAIL_SESSION_MAIL_SMTP_USER),PropsUtil.get(PropsKeys.MAIL_SESSION_MAIL_SMTP_PASSWORD));
						}
					});
					
					try {
						MimeMessage message = new MimeMessage(session);
						message.setFrom(new InternetAddress(PropsUtil.get(PropsKeys.MAIL_SESSION_MAIL_SMTP_USER)));
						message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAdress));
						message.setSubject(subjectMail);
						message.setText(LanguageUtil.format(themeDisplay.getLocale(), "cam-on-ban-da-gop-y", new Object()));
						
						Transport.send(message);
					}catch (MessagingException e) {
						throw new RuntimeException(e);
					}
				}

				// convert content to xml
				String contentArticle = JournalArticleUtil.setContent(articleFormFields, articleParameters,
						attachmentUrl);
				JournalArticleUtil.createJournalArticle(themeDisplay, request, structureKey, structureId, titleName,
						contentName, contentArticle, categoryId);

			} catch (Exception ex) {
				ex.printStackTrace();
				SessionErrors.add(request, "addQuestionFailed");
				PortalUtil.copyRequestParameters(request, response);
				// response.sendRedirect(currentUrl);
				return;
			}

			SessionMessages.add(request, "addQuestionSuccessfully");
			response.sendRedirect(currentUrl);
		} else {
			SessionErrors.add(request, "captchaError");
			response.sendRedirect(currentUrl);
		}
	}

	private boolean checkCaptcha(PortletRequest request, String enteredCaptchaText) throws Exception {
		boolean captchaMatched = false;

		PortletSession session = request.getPortletSession();
		String captchaTextFromSession = getCaptchaValueFromSession(session);

		if (Validator.isNotNull(captchaTextFromSession)) {
			if (enteredCaptchaText.equals(captchaTextFromSession)) {
				captchaMatched = true;
			}
		}
		return captchaMatched;
	}

	private String getCaptchaValueFromSession(PortletSession session) {
		Enumeration<String> atNames = session.getAttributeNames();
		while (atNames.hasMoreElements()) {
			String name = atNames.nextElement();
			if (name.contains("CAPTCHA_TEXT")) {
				String captchaValueFromSession = (String) session.getAttribute(name);
				return captchaValueFromSession;
			}
		}
		return null;
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		try {
			CaptchaUtil.serveImage(resourceRequest, resourceResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}