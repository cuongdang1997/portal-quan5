package swt.questionanswer.form.constants;

/**
 * @author Nhut.Dang
 */
public class SwtQuestionanswerFormPortletKeys {

	public static final String SwtQuestionanswerForm = "HCMGov Form";
	public static final String SwtQuestionanswerFormPortlet = "SWTForm";
	public static final String SwtCapcha = "CaptchaMVCResourceCommand";
	public static final int workFlow_DangBai = 1;

	public static final int FILE_SIZE_BYTES = 5 * 1024 * 1024;

	public static final String[] ALLOWED_ATTACHMENT_TYPES = new String[] { ".xls", ".xlsx", ".doc", ".docx", ".pdf",
			".rar", ".zip", ".png", ".jpg", ".jpeg", ".txt" };

}