package swt.questionanswer.form.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(factory = true, id = "swt.questionanswer.form.portlet.SwtQuestionanswerConfiguration", localization = "content/Language")
public interface SwtQuestionanswerConfiguration {
	@Meta.AD(deflt = "", required = false)
	public String structureForm();

	@Meta.AD(deflt = "", required = false)
	public String vocabulary();

	@Meta.AD(deflt = "Send", required = false)
	public String labelSubmit();
	
	@Meta.AD(deflt = "true", required = false)
	public String buttonBack();
}
