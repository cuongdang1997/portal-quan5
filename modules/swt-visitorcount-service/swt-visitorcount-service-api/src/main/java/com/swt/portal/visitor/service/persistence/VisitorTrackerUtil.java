/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.swt.portal.visitor.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.swt.portal.visitor.model.VisitorTracker;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.Date;
import java.util.List;

/**
 * The persistence utility for the visitor tracker service. This utility wraps {@link com.swt.portal.visitor.service.persistence.impl.VisitorTrackerPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VisitorTrackerPersistence
 * @see com.swt.portal.visitor.service.persistence.impl.VisitorTrackerPersistenceImpl
 * @generated
 */
@ProviderType
public class VisitorTrackerUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(VisitorTracker visitorTracker) {
		getPersistence().clearCache(visitorTracker);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<VisitorTracker> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<VisitorTracker> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<VisitorTracker> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static VisitorTracker update(VisitorTracker visitorTracker) {
		return getPersistence().update(visitorTracker);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static VisitorTracker update(VisitorTracker visitorTracker,
		ServiceContext serviceContext) {
		return getPersistence().update(visitorTracker, serviceContext);
	}

	/**
	* Returns all the visitor trackers where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the visitor trackers where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid(String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the visitor trackers where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid(String uuid, int start,
		int end, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the visitor trackers where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid(String uuid, int start,
		int end, OrderByComparator<VisitorTracker> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first visitor tracker in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByUuid_First(String uuid,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first visitor tracker in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByUuid_First(String uuid,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByUuid_Last(String uuid,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByUuid_Last(String uuid,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the visitor trackers before and after the current visitor tracker in the ordered set where uuid = &#63;.
	*
	* @param visitorId the primary key of the current visitor tracker
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next visitor tracker
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker[] findByUuid_PrevAndNext(long visitorId,
		String uuid, OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByUuid_PrevAndNext(visitorId, uuid, orderByComparator);
	}

	/**
	* Removes all the visitor trackers where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of visitor trackers where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching visitor trackers
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the visitor tracker where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchVisitorTrackerException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByUUID_G(String uuid, long groupId)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the visitor tracker where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the visitor tracker where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the visitor tracker where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the visitor tracker that was removed
	*/
	public static VisitorTracker removeByUUID_G(String uuid, long groupId)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of visitor trackers where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching visitor trackers
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the visitor trackers where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the visitor trackers where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid_C(String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the visitor trackers where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the visitor trackers where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first visitor tracker in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByUuid_C_First(String uuid,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first visitor tracker in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByUuid_C_First(String uuid,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the visitor trackers before and after the current visitor tracker in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param visitorId the primary key of the current visitor tracker
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next visitor tracker
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker[] findByUuid_C_PrevAndNext(long visitorId,
		String uuid, long companyId,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(visitorId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the visitor trackers where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of visitor trackers where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching visitor trackers
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the visitor trackers where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the visitor trackers where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId(long groupId, int start,
		int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the visitor trackers where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId(long groupId, int start,
		int end, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the visitor trackers where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId(long groupId, int start,
		int end, OrderByComparator<VisitorTracker> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first visitor tracker in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByGroupId_First(long groupId,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first visitor tracker in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByGroupId_First(long groupId,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByGroupId_Last(long groupId,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByGroupId_Last(long groupId,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the visitor trackers before and after the current visitor tracker in the ordered set where groupId = &#63;.
	*
	* @param visitorId the primary key of the current visitor tracker
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next visitor tracker
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker[] findByGroupId_PrevAndNext(long visitorId,
		long groupId, OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(visitorId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the visitor trackers where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of visitor trackers where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching visitor trackers
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Returns all the visitor trackers where vistorTime = &#63;.
	*
	* @param vistorTime the vistor time
	* @return the matching visitor trackers
	*/
	public static List<VisitorTracker> findByVistorTime(Date vistorTime) {
		return getPersistence().findByVistorTime(vistorTime);
	}

	/**
	* Returns a range of all the visitor trackers where vistorTime = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param vistorTime the vistor time
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByVistorTime(Date vistorTime,
		int start, int end) {
		return getPersistence().findByVistorTime(vistorTime, start, end);
	}

	/**
	* Returns an ordered range of all the visitor trackers where vistorTime = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param vistorTime the vistor time
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByVistorTime(Date vistorTime,
		int start, int end, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .findByVistorTime(vistorTime, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the visitor trackers where vistorTime = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param vistorTime the vistor time
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByVistorTime(Date vistorTime,
		int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByVistorTime(vistorTime, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first visitor tracker in the ordered set where vistorTime = &#63;.
	*
	* @param vistorTime the vistor time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByVistorTime_First(Date vistorTime,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByVistorTime_First(vistorTime, orderByComparator);
	}

	/**
	* Returns the first visitor tracker in the ordered set where vistorTime = &#63;.
	*
	* @param vistorTime the vistor time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByVistorTime_First(Date vistorTime,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .fetchByVistorTime_First(vistorTime, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where vistorTime = &#63;.
	*
	* @param vistorTime the vistor time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByVistorTime_Last(Date vistorTime,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByVistorTime_Last(vistorTime, orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where vistorTime = &#63;.
	*
	* @param vistorTime the vistor time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByVistorTime_Last(Date vistorTime,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .fetchByVistorTime_Last(vistorTime, orderByComparator);
	}

	/**
	* Returns the visitor trackers before and after the current visitor tracker in the ordered set where vistorTime = &#63;.
	*
	* @param visitorId the primary key of the current visitor tracker
	* @param vistorTime the vistor time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next visitor tracker
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker[] findByVistorTime_PrevAndNext(
		long visitorId, Date vistorTime,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByVistorTime_PrevAndNext(visitorId, vistorTime,
			orderByComparator);
	}

	/**
	* Removes all the visitor trackers where vistorTime = &#63; from the database.
	*
	* @param vistorTime the vistor time
	*/
	public static void removeByVistorTime(Date vistorTime) {
		getPersistence().removeByVistorTime(vistorTime);
	}

	/**
	* Returns the number of visitor trackers where vistorTime = &#63;.
	*
	* @param vistorTime the vistor time
	* @return the number of matching visitor trackers
	*/
	public static int countByVistorTime(Date vistorTime) {
		return getPersistence().countByVistorTime(vistorTime);
	}

	/**
	* Returns all the visitor trackers where groupId = &#63; and companyId = &#63;.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @return the matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId_CompanyId(long groupId,
		long companyId) {
		return getPersistence().findByGroupId_CompanyId(groupId, companyId);
	}

	/**
	* Returns a range of all the visitor trackers where groupId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId_CompanyId(long groupId,
		long companyId, int start, int end) {
		return getPersistence()
				   .findByGroupId_CompanyId(groupId, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the visitor trackers where groupId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId_CompanyId(long groupId,
		long companyId, int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .findByGroupId_CompanyId(groupId, companyId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the visitor trackers where groupId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching visitor trackers
	*/
	public static List<VisitorTracker> findByGroupId_CompanyId(long groupId,
		long companyId, int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId_CompanyId(groupId, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first visitor tracker in the ordered set where groupId = &#63; and companyId = &#63;.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByGroupId_CompanyId_First(long groupId,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByGroupId_CompanyId_First(groupId, companyId,
			orderByComparator);
	}

	/**
	* Returns the first visitor tracker in the ordered set where groupId = &#63; and companyId = &#63;.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByGroupId_CompanyId_First(long groupId,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .fetchByGroupId_CompanyId_First(groupId, companyId,
			orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where groupId = &#63; and companyId = &#63;.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker
	* @throws NoSuchVisitorTrackerException if a matching visitor tracker could not be found
	*/
	public static VisitorTracker findByGroupId_CompanyId_Last(long groupId,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByGroupId_CompanyId_Last(groupId, companyId,
			orderByComparator);
	}

	/**
	* Returns the last visitor tracker in the ordered set where groupId = &#63; and companyId = &#63;.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static VisitorTracker fetchByGroupId_CompanyId_Last(long groupId,
		long companyId, OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence()
				   .fetchByGroupId_CompanyId_Last(groupId, companyId,
			orderByComparator);
	}

	/**
	* Returns the visitor trackers before and after the current visitor tracker in the ordered set where groupId = &#63; and companyId = &#63;.
	*
	* @param visitorId the primary key of the current visitor tracker
	* @param groupId the group ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next visitor tracker
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker[] findByGroupId_CompanyId_PrevAndNext(
		long visitorId, long groupId, long companyId,
		OrderByComparator<VisitorTracker> orderByComparator)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence()
				   .findByGroupId_CompanyId_PrevAndNext(visitorId, groupId,
			companyId, orderByComparator);
	}

	/**
	* Removes all the visitor trackers where groupId = &#63; and companyId = &#63; from the database.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	*/
	public static void removeByGroupId_CompanyId(long groupId, long companyId) {
		getPersistence().removeByGroupId_CompanyId(groupId, companyId);
	}

	/**
	* Returns the number of visitor trackers where groupId = &#63; and companyId = &#63;.
	*
	* @param groupId the group ID
	* @param companyId the company ID
	* @return the number of matching visitor trackers
	*/
	public static int countByGroupId_CompanyId(long groupId, long companyId) {
		return getPersistence().countByGroupId_CompanyId(groupId, companyId);
	}

	/**
	* Caches the visitor tracker in the entity cache if it is enabled.
	*
	* @param visitorTracker the visitor tracker
	*/
	public static void cacheResult(VisitorTracker visitorTracker) {
		getPersistence().cacheResult(visitorTracker);
	}

	/**
	* Caches the visitor trackers in the entity cache if it is enabled.
	*
	* @param visitorTrackers the visitor trackers
	*/
	public static void cacheResult(List<VisitorTracker> visitorTrackers) {
		getPersistence().cacheResult(visitorTrackers);
	}

	/**
	* Creates a new visitor tracker with the primary key. Does not add the visitor tracker to the database.
	*
	* @param visitorId the primary key for the new visitor tracker
	* @return the new visitor tracker
	*/
	public static VisitorTracker create(long visitorId) {
		return getPersistence().create(visitorId);
	}

	/**
	* Removes the visitor tracker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker that was removed
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker remove(long visitorId)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().remove(visitorId);
	}

	public static VisitorTracker updateImpl(VisitorTracker visitorTracker) {
		return getPersistence().updateImpl(visitorTracker);
	}

	/**
	* Returns the visitor tracker with the primary key or throws a {@link NoSuchVisitorTrackerException} if it could not be found.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker
	* @throws NoSuchVisitorTrackerException if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker findByPrimaryKey(long visitorId)
		throws com.swt.portal.visitor.exception.NoSuchVisitorTrackerException {
		return getPersistence().findByPrimaryKey(visitorId);
	}

	/**
	* Returns the visitor tracker with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker, or <code>null</code> if a visitor tracker with the primary key could not be found
	*/
	public static VisitorTracker fetchByPrimaryKey(long visitorId) {
		return getPersistence().fetchByPrimaryKey(visitorId);
	}

	public static java.util.Map<java.io.Serializable, VisitorTracker> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the visitor trackers.
	*
	* @return the visitor trackers
	*/
	public static List<VisitorTracker> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the visitor trackers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of visitor trackers
	*/
	public static List<VisitorTracker> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the visitor trackers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of visitor trackers
	*/
	public static List<VisitorTracker> findAll(int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the visitor trackers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of visitor trackers
	*/
	public static List<VisitorTracker> findAll(int start, int end,
		OrderByComparator<VisitorTracker> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the visitor trackers from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of visitor trackers.
	*
	* @return the number of visitor trackers
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static VisitorTrackerPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VisitorTrackerPersistence, VisitorTrackerPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(VisitorTrackerPersistence.class);

		ServiceTracker<VisitorTrackerPersistence, VisitorTrackerPersistence> serviceTracker =
			new ServiceTracker<VisitorTrackerPersistence, VisitorTrackerPersistence>(bundle.getBundleContext(),
				VisitorTrackerPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}