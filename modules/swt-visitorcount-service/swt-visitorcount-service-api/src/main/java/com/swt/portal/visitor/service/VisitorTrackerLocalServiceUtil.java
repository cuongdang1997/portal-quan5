/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.swt.portal.visitor.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for VisitorTracker. This utility wraps
 * {@link com.swt.portal.visitor.service.impl.VisitorTrackerLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see VisitorTrackerLocalService
 * @see com.swt.portal.visitor.service.base.VisitorTrackerLocalServiceBaseImpl
 * @see com.swt.portal.visitor.service.impl.VisitorTrackerLocalServiceImpl
 * @generated
 */
@ProviderType
public class VisitorTrackerLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.swt.portal.visitor.service.impl.VisitorTrackerLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.swt.portal.visitor.model.VisitorTracker addOrUpdateVisitorTracker(
		long groupId, long companyId, String cookieValue,
		java.util.Date visitorTime) {
		return getService()
				   .addOrUpdateVisitorTracker(groupId, companyId, cookieValue,
			visitorTime);
	}

	/**
	* Adds the visitor tracker to the database. Also notifies the appropriate model listeners.
	*
	* @param visitorTracker the visitor tracker
	* @return the visitor tracker that was added
	*/
	public static com.swt.portal.visitor.model.VisitorTracker addVisitorTracker(
		com.swt.portal.visitor.model.VisitorTracker visitorTracker) {
		return getService().addVisitorTracker(visitorTracker);
	}

	public static int countCurrentVisitorByGroupId(long groupId) {
		return getService().countCurrentVisitorByGroupId(groupId);
	}

	/**
	* Creates a new visitor tracker with the primary key. Does not add the visitor tracker to the database.
	*
	* @param visitorId the primary key for the new visitor tracker
	* @return the new visitor tracker
	*/
	public static com.swt.portal.visitor.model.VisitorTracker createVisitorTracker(
		long visitorId) {
		return getService().createVisitorTracker(visitorId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	/**
	* Deletes the visitor tracker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker that was removed
	* @throws PortalException if a visitor tracker with the primary key could not be found
	*/
	public static com.swt.portal.visitor.model.VisitorTracker deleteVisitorTracker(
		long visitorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteVisitorTracker(visitorId);
	}

	/**
	* Deletes the visitor tracker from the database. Also notifies the appropriate model listeners.
	*
	* @param visitorTracker the visitor tracker
	* @return the visitor tracker that was removed
	*/
	public static com.swt.portal.visitor.model.VisitorTracker deleteVisitorTracker(
		com.swt.portal.visitor.model.VisitorTracker visitorTracker) {
		return getService().deleteVisitorTracker(visitorTracker);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.swt.portal.visitor.model.impl.VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.swt.portal.visitor.model.impl.VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.swt.portal.visitor.model.VisitorTracker fetchVisitorTracker(
		long visitorId) {
		return getService().fetchVisitorTracker(visitorId);
	}

	/**
	* Returns the visitor tracker matching the UUID and group.
	*
	* @param uuid the visitor tracker's UUID
	* @param groupId the primary key of the group
	* @return the matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	public static com.swt.portal.visitor.model.VisitorTracker fetchVisitorTrackerByUuidAndGroupId(
		String uuid, long groupId) {
		return getService().fetchVisitorTrackerByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static String getAllVisitorCount(long companyId, long groupId) {
		return getService().getAllVisitorCount(companyId, groupId);
	}

	public static String getCookieByName(
		javax.servlet.http.HttpServletRequest request, String cookieName) {
		return getService().getCookieByName(request, cookieName);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	public static String getIpAddress(
		javax.servlet.http.HttpServletRequest request)
		throws java.io.IOException {
		return getService().getIpAddress(request);
	}

	public static String[] getOnlineVisitorCount(
		javax.servlet.http.HttpServletRequest request, String cookieName,
		long companyId, long groupId) {
		return getService()
				   .getOnlineVisitorCount(request, cookieName, companyId,
			groupId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static long getVisitorCountOlderThanVisitorTime(long companyId,
		long groupId, long expires) {
		return getService()
				   .getVisitorCountOlderThanVisitorTime(companyId, groupId,
			expires);
	}

	/**
	* Returns the visitor tracker with the primary key.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker
	* @throws PortalException if a visitor tracker with the primary key could not be found
	*/
	public static com.swt.portal.visitor.model.VisitorTracker getVisitorTracker(
		long visitorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getVisitorTracker(visitorId);
	}

	public static com.swt.portal.visitor.model.VisitorTracker getVisitorTrackerBy_G_UUID(
		long groupId, String cookieValue) {
		return getService().getVisitorTrackerBy_G_UUID(groupId, cookieValue);
	}

	/**
	* Returns the visitor tracker matching the UUID and group.
	*
	* @param uuid the visitor tracker's UUID
	* @param groupId the primary key of the group
	* @return the matching visitor tracker
	* @throws PortalException if a matching visitor tracker could not be found
	*/
	public static com.swt.portal.visitor.model.VisitorTracker getVisitorTrackerByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getVisitorTrackerByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the visitor trackers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.swt.portal.visitor.model.impl.VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of visitor trackers
	*/
	public static java.util.List<com.swt.portal.visitor.model.VisitorTracker> getVisitorTrackers(
		int start, int end) {
		return getService().getVisitorTrackers(start, end);
	}

	/**
	* Returns all the visitor trackers matching the UUID and company.
	*
	* @param uuid the UUID of the visitor trackers
	* @param companyId the primary key of the company
	* @return the matching visitor trackers, or an empty list if no matches were found
	*/
	public static java.util.List<com.swt.portal.visitor.model.VisitorTracker> getVisitorTrackersByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService().getVisitorTrackersByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of visitor trackers matching the UUID and company.
	*
	* @param uuid the UUID of the visitor trackers
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching visitor trackers, or an empty list if no matches were found
	*/
	public static java.util.List<com.swt.portal.visitor.model.VisitorTracker> getVisitorTrackersByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.swt.portal.visitor.model.VisitorTracker> orderByComparator) {
		return getService()
				   .getVisitorTrackersByUuidAndCompanyId(uuid, companyId,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of visitor trackers.
	*
	* @return the number of visitor trackers
	*/
	public static int getVisitorTrackersCount() {
		return getService().getVisitorTrackersCount();
	}

	public static int liveUsersCount(long companyId) {
		return getService().liveUsersCount(companyId);
	}

	/**
	* @param companyId
	* @param groupId
	* @param day
	* @param month
	* @param year
	* @return 1 mang gom 24 phan tu ung voi 24 gio trong 1 ngay, nguoc lai tra ve
	null
	*/
	public static long[] statisticVisitorByDate(long companyId, long groupId,
		int day, int month, int year) {
		return getService()
				   .statisticVisitorByDate(companyId, groupId, day, month, year);
	}

	/**
	* @param companyId
	* @param groupId
	* @param month
	* @param year
	* @return 1 mang co so phan tu ung voi so ngay trong thang do, nguoc lai tra ve
	null
	*/
	public static long[] statisticVisitorByMonth(long companyId, long groupId,
		int month, int year) {
		return getService()
				   .statisticVisitorByMonth(companyId, groupId, month, year);
	}

	/**
	* @param companyId
	* @param groupId
	* @param year
	* @return 1 mang co so phan tu ung voi so thang trong nam, nguoc lai tra ve
	null
	*/
	public static long[] statisticVisitorByYear(long companyId, long groupId,
		int year) {
		return getService().statisticVisitorByYear(companyId, groupId, year);
	}

	/**
	* Updates the visitor tracker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param visitorTracker the visitor tracker
	* @return the visitor tracker that was updated
	*/
	public static com.swt.portal.visitor.model.VisitorTracker updateVisitorTracker(
		com.swt.portal.visitor.model.VisitorTracker visitorTracker) {
		return getService().updateVisitorTracker(visitorTracker);
	}

	public static VisitorTrackerLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VisitorTrackerLocalService, VisitorTrackerLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(VisitorTrackerLocalService.class);

		ServiceTracker<VisitorTrackerLocalService, VisitorTrackerLocalService> serviceTracker =
			new ServiceTracker<VisitorTrackerLocalService, VisitorTrackerLocalService>(bundle.getBundleContext(),
				VisitorTrackerLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}