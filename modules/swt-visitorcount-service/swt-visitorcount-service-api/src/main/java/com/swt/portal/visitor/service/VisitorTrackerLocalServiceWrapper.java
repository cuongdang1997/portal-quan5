/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.swt.portal.visitor.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VisitorTrackerLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see VisitorTrackerLocalService
 * @generated
 */
@ProviderType
public class VisitorTrackerLocalServiceWrapper
	implements VisitorTrackerLocalService,
		ServiceWrapper<VisitorTrackerLocalService> {
	public VisitorTrackerLocalServiceWrapper(
		VisitorTrackerLocalService visitorTrackerLocalService) {
		_visitorTrackerLocalService = visitorTrackerLocalService;
	}

	@Override
	public com.swt.portal.visitor.model.VisitorTracker addOrUpdateVisitorTracker(
		long groupId, long companyId, String cookieValue,
		java.util.Date visitorTime) {
		return _visitorTrackerLocalService.addOrUpdateVisitorTracker(groupId,
			companyId, cookieValue, visitorTime);
	}

	/**
	* Adds the visitor tracker to the database. Also notifies the appropriate model listeners.
	*
	* @param visitorTracker the visitor tracker
	* @return the visitor tracker that was added
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker addVisitorTracker(
		com.swt.portal.visitor.model.VisitorTracker visitorTracker) {
		return _visitorTrackerLocalService.addVisitorTracker(visitorTracker);
	}

	@Override
	public int countCurrentVisitorByGroupId(long groupId) {
		return _visitorTrackerLocalService.countCurrentVisitorByGroupId(groupId);
	}

	/**
	* Creates a new visitor tracker with the primary key. Does not add the visitor tracker to the database.
	*
	* @param visitorId the primary key for the new visitor tracker
	* @return the new visitor tracker
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker createVisitorTracker(
		long visitorId) {
		return _visitorTrackerLocalService.createVisitorTracker(visitorId);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _visitorTrackerLocalService.deletePersistedModel(persistedModel);
	}

	/**
	* Deletes the visitor tracker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker that was removed
	* @throws PortalException if a visitor tracker with the primary key could not be found
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker deleteVisitorTracker(
		long visitorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _visitorTrackerLocalService.deleteVisitorTracker(visitorId);
	}

	/**
	* Deletes the visitor tracker from the database. Also notifies the appropriate model listeners.
	*
	* @param visitorTracker the visitor tracker
	* @return the visitor tracker that was removed
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker deleteVisitorTracker(
		com.swt.portal.visitor.model.VisitorTracker visitorTracker) {
		return _visitorTrackerLocalService.deleteVisitorTracker(visitorTracker);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _visitorTrackerLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _visitorTrackerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.swt.portal.visitor.model.impl.VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _visitorTrackerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.swt.portal.visitor.model.impl.VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _visitorTrackerLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _visitorTrackerLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _visitorTrackerLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.swt.portal.visitor.model.VisitorTracker fetchVisitorTracker(
		long visitorId) {
		return _visitorTrackerLocalService.fetchVisitorTracker(visitorId);
	}

	/**
	* Returns the visitor tracker matching the UUID and group.
	*
	* @param uuid the visitor tracker's UUID
	* @param groupId the primary key of the group
	* @return the matching visitor tracker, or <code>null</code> if a matching visitor tracker could not be found
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker fetchVisitorTrackerByUuidAndGroupId(
		String uuid, long groupId) {
		return _visitorTrackerLocalService.fetchVisitorTrackerByUuidAndGroupId(uuid,
			groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _visitorTrackerLocalService.getActionableDynamicQuery();
	}

	@Override
	public String getAllVisitorCount(long companyId, long groupId) {
		return _visitorTrackerLocalService.getAllVisitorCount(companyId, groupId);
	}

	@Override
	public String getCookieByName(
		javax.servlet.http.HttpServletRequest request, String cookieName) {
		return _visitorTrackerLocalService.getCookieByName(request, cookieName);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _visitorTrackerLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public String getIpAddress(javax.servlet.http.HttpServletRequest request)
		throws java.io.IOException {
		return _visitorTrackerLocalService.getIpAddress(request);
	}

	@Override
	public String[] getOnlineVisitorCount(
		javax.servlet.http.HttpServletRequest request, String cookieName,
		long companyId, long groupId) {
		return _visitorTrackerLocalService.getOnlineVisitorCount(request,
			cookieName, companyId, groupId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _visitorTrackerLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _visitorTrackerLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public long getVisitorCountOlderThanVisitorTime(long companyId,
		long groupId, long expires) {
		return _visitorTrackerLocalService.getVisitorCountOlderThanVisitorTime(companyId,
			groupId, expires);
	}

	/**
	* Returns the visitor tracker with the primary key.
	*
	* @param visitorId the primary key of the visitor tracker
	* @return the visitor tracker
	* @throws PortalException if a visitor tracker with the primary key could not be found
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker getVisitorTracker(
		long visitorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _visitorTrackerLocalService.getVisitorTracker(visitorId);
	}

	@Override
	public com.swt.portal.visitor.model.VisitorTracker getVisitorTrackerBy_G_UUID(
		long groupId, String cookieValue) {
		return _visitorTrackerLocalService.getVisitorTrackerBy_G_UUID(groupId,
			cookieValue);
	}

	/**
	* Returns the visitor tracker matching the UUID and group.
	*
	* @param uuid the visitor tracker's UUID
	* @param groupId the primary key of the group
	* @return the matching visitor tracker
	* @throws PortalException if a matching visitor tracker could not be found
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker getVisitorTrackerByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _visitorTrackerLocalService.getVisitorTrackerByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns a range of all the visitor trackers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.swt.portal.visitor.model.impl.VisitorTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @return the range of visitor trackers
	*/
	@Override
	public java.util.List<com.swt.portal.visitor.model.VisitorTracker> getVisitorTrackers(
		int start, int end) {
		return _visitorTrackerLocalService.getVisitorTrackers(start, end);
	}

	/**
	* Returns all the visitor trackers matching the UUID and company.
	*
	* @param uuid the UUID of the visitor trackers
	* @param companyId the primary key of the company
	* @return the matching visitor trackers, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.swt.portal.visitor.model.VisitorTracker> getVisitorTrackersByUuidAndCompanyId(
		String uuid, long companyId) {
		return _visitorTrackerLocalService.getVisitorTrackersByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of visitor trackers matching the UUID and company.
	*
	* @param uuid the UUID of the visitor trackers
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of visitor trackers
	* @param end the upper bound of the range of visitor trackers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching visitor trackers, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.swt.portal.visitor.model.VisitorTracker> getVisitorTrackersByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.swt.portal.visitor.model.VisitorTracker> orderByComparator) {
		return _visitorTrackerLocalService.getVisitorTrackersByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of visitor trackers.
	*
	* @return the number of visitor trackers
	*/
	@Override
	public int getVisitorTrackersCount() {
		return _visitorTrackerLocalService.getVisitorTrackersCount();
	}

	@Override
	public int liveUsersCount(long companyId) {
		return _visitorTrackerLocalService.liveUsersCount(companyId);
	}

	/**
	* @param companyId
	* @param groupId
	* @param day
	* @param month
	* @param year
	* @return 1 mang gom 24 phan tu ung voi 24 gio trong 1 ngay, nguoc lai tra ve
	null
	*/
	@Override
	public long[] statisticVisitorByDate(long companyId, long groupId, int day,
		int month, int year) {
		return _visitorTrackerLocalService.statisticVisitorByDate(companyId,
			groupId, day, month, year);
	}

	/**
	* @param companyId
	* @param groupId
	* @param month
	* @param year
	* @return 1 mang co so phan tu ung voi so ngay trong thang do, nguoc lai tra ve
	null
	*/
	@Override
	public long[] statisticVisitorByMonth(long companyId, long groupId,
		int month, int year) {
		return _visitorTrackerLocalService.statisticVisitorByMonth(companyId,
			groupId, month, year);
	}

	/**
	* @param companyId
	* @param groupId
	* @param year
	* @return 1 mang co so phan tu ung voi so thang trong nam, nguoc lai tra ve
	null
	*/
	@Override
	public long[] statisticVisitorByYear(long companyId, long groupId, int year) {
		return _visitorTrackerLocalService.statisticVisitorByYear(companyId,
			groupId, year);
	}

	/**
	* Updates the visitor tracker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param visitorTracker the visitor tracker
	* @return the visitor tracker that was updated
	*/
	@Override
	public com.swt.portal.visitor.model.VisitorTracker updateVisitorTracker(
		com.swt.portal.visitor.model.VisitorTracker visitorTracker) {
		return _visitorTrackerLocalService.updateVisitorTracker(visitorTracker);
	}

	@Override
	public VisitorTrackerLocalService getWrappedService() {
		return _visitorTrackerLocalService;
	}

	@Override
	public void setWrappedService(
		VisitorTrackerLocalService visitorTrackerLocalService) {
		_visitorTrackerLocalService = visitorTrackerLocalService;
	}

	private VisitorTrackerLocalService _visitorTrackerLocalService;
}