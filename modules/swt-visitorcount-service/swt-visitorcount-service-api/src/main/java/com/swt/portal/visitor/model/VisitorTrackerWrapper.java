/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.swt.portal.visitor.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link VisitorTracker}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VisitorTracker
 * @generated
 */
@ProviderType
public class VisitorTrackerWrapper implements VisitorTracker,
	ModelWrapper<VisitorTracker> {
	public VisitorTrackerWrapper(VisitorTracker visitorTracker) {
		_visitorTracker = visitorTracker;
	}

	@Override
	public Class<?> getModelClass() {
		return VisitorTracker.class;
	}

	@Override
	public String getModelClassName() {
		return VisitorTracker.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("visitorId", getVisitorId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("vistorTime", getVistorTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long visitorId = (Long)attributes.get("visitorId");

		if (visitorId != null) {
			setVisitorId(visitorId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date vistorTime = (Date)attributes.get("vistorTime");

		if (vistorTime != null) {
			setVistorTime(vistorTime);
		}
	}

	@Override
	public Object clone() {
		return new VisitorTrackerWrapper((VisitorTracker)_visitorTracker.clone());
	}

	@Override
	public int compareTo(VisitorTracker visitorTracker) {
		return _visitorTracker.compareTo(visitorTracker);
	}

	/**
	* Returns the company ID of this visitor tracker.
	*
	* @return the company ID of this visitor tracker
	*/
	@Override
	public long getCompanyId() {
		return _visitorTracker.getCompanyId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _visitorTracker.getExpandoBridge();
	}

	/**
	* Returns the group ID of this visitor tracker.
	*
	* @return the group ID of this visitor tracker
	*/
	@Override
	public long getGroupId() {
		return _visitorTracker.getGroupId();
	}

	/**
	* Returns the primary key of this visitor tracker.
	*
	* @return the primary key of this visitor tracker
	*/
	@Override
	public long getPrimaryKey() {
		return _visitorTracker.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _visitorTracker.getPrimaryKeyObj();
	}

	/**
	* Returns the uuid of this visitor tracker.
	*
	* @return the uuid of this visitor tracker
	*/
	@Override
	public String getUuid() {
		return _visitorTracker.getUuid();
	}

	/**
	* Returns the visitor ID of this visitor tracker.
	*
	* @return the visitor ID of this visitor tracker
	*/
	@Override
	public long getVisitorId() {
		return _visitorTracker.getVisitorId();
	}

	/**
	* Returns the vistor time of this visitor tracker.
	*
	* @return the vistor time of this visitor tracker
	*/
	@Override
	public Date getVistorTime() {
		return _visitorTracker.getVistorTime();
	}

	@Override
	public int hashCode() {
		return _visitorTracker.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _visitorTracker.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _visitorTracker.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _visitorTracker.isNew();
	}

	@Override
	public void persist() {
		_visitorTracker.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_visitorTracker.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this visitor tracker.
	*
	* @param companyId the company ID of this visitor tracker
	*/
	@Override
	public void setCompanyId(long companyId) {
		_visitorTracker.setCompanyId(companyId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_visitorTracker.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_visitorTracker.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_visitorTracker.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this visitor tracker.
	*
	* @param groupId the group ID of this visitor tracker
	*/
	@Override
	public void setGroupId(long groupId) {
		_visitorTracker.setGroupId(groupId);
	}

	@Override
	public void setNew(boolean n) {
		_visitorTracker.setNew(n);
	}

	/**
	* Sets the primary key of this visitor tracker.
	*
	* @param primaryKey the primary key of this visitor tracker
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_visitorTracker.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_visitorTracker.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the uuid of this visitor tracker.
	*
	* @param uuid the uuid of this visitor tracker
	*/
	@Override
	public void setUuid(String uuid) {
		_visitorTracker.setUuid(uuid);
	}

	/**
	* Sets the visitor ID of this visitor tracker.
	*
	* @param visitorId the visitor ID of this visitor tracker
	*/
	@Override
	public void setVisitorId(long visitorId) {
		_visitorTracker.setVisitorId(visitorId);
	}

	/**
	* Sets the vistor time of this visitor tracker.
	*
	* @param vistorTime the vistor time of this visitor tracker
	*/
	@Override
	public void setVistorTime(Date vistorTime) {
		_visitorTracker.setVistorTime(vistorTime);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<VisitorTracker> toCacheModel() {
		return _visitorTracker.toCacheModel();
	}

	@Override
	public VisitorTracker toEscapedModel() {
		return new VisitorTrackerWrapper(_visitorTracker.toEscapedModel());
	}

	@Override
	public String toString() {
		return _visitorTracker.toString();
	}

	@Override
	public VisitorTracker toUnescapedModel() {
		return new VisitorTrackerWrapper(_visitorTracker.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _visitorTracker.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VisitorTrackerWrapper)) {
			return false;
		}

		VisitorTrackerWrapper visitorTrackerWrapper = (VisitorTrackerWrapper)obj;

		if (Objects.equals(_visitorTracker,
					visitorTrackerWrapper._visitorTracker)) {
			return true;
		}

		return false;
	}

	@Override
	public VisitorTracker getWrappedModel() {
		return _visitorTracker;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _visitorTracker.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _visitorTracker.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_visitorTracker.resetOriginalValues();
	}

	private final VisitorTracker _visitorTracker;
}