create index IX_B21B696A on hcmportal_visitor_tracker (groupId, companyId);
create index IX_B2362520 on hcmportal_visitor_tracker (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_A74573A2 on hcmportal_visitor_tracker (uuid_[$COLUMN_LENGTH:75$], groupId);
create index IX_39426644 on hcmportal_visitor_tracker (vistorTime);