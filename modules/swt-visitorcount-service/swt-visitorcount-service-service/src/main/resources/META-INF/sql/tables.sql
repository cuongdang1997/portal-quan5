create table hcmportal_visitor_tracker (
	uuid_ VARCHAR(75) null,
	visitorId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	vistorTime DATE null
);