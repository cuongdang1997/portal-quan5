/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.swt.portal.visitor.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import com.swt.portal.visitor.model.VisitorTracker;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing VisitorTracker in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see VisitorTracker
 * @generated
 */
@ProviderType
public class VisitorTrackerCacheModel implements CacheModel<VisitorTracker>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VisitorTrackerCacheModel)) {
			return false;
		}

		VisitorTrackerCacheModel visitorTrackerCacheModel = (VisitorTrackerCacheModel)obj;

		if (visitorId == visitorTrackerCacheModel.visitorId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, visitorId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", visitorId=");
		sb.append(visitorId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", vistorTime=");
		sb.append(vistorTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VisitorTracker toEntityModel() {
		VisitorTrackerImpl visitorTrackerImpl = new VisitorTrackerImpl();

		if (uuid == null) {
			visitorTrackerImpl.setUuid("");
		}
		else {
			visitorTrackerImpl.setUuid(uuid);
		}

		visitorTrackerImpl.setVisitorId(visitorId);
		visitorTrackerImpl.setGroupId(groupId);
		visitorTrackerImpl.setCompanyId(companyId);

		if (vistorTime == Long.MIN_VALUE) {
			visitorTrackerImpl.setVistorTime(null);
		}
		else {
			visitorTrackerImpl.setVistorTime(new Date(vistorTime));
		}

		visitorTrackerImpl.resetOriginalValues();

		return visitorTrackerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		visitorId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();
		vistorTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(visitorId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(vistorTime);
	}

	public String uuid;
	public long visitorId;
	public long groupId;
	public long companyId;
	public long vistorTime;
}