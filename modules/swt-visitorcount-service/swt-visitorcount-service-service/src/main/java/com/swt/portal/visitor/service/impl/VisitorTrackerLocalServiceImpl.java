/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.swt.portal.visitor.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.UserTracker;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.liveusers.LiveUsers;
import com.swt.portal.visitor.model.VisitorTracker;
import com.swt.portal.visitor.service.VisitorTrackerLocalServiceUtil;
import com.swt.portal.visitor.service.base.VisitorTrackerLocalServiceBaseImpl;

/**
 * The implementation of the visitor tracker local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.swt.portal.visitor.service.VisitorTrackerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VisitorTrackerLocalServiceBaseImpl
 * @see com.swt.portal.visitor.service.VisitorTrackerLocalServiceUtil
 */
public class VisitorTrackerLocalServiceImpl extends VisitorTrackerLocalServiceBaseImpl {
	private static final String KEY_MIN_ONLINE_VISITOR_COUNT = "min_online_visitor_count";
	private static final String KEY_MIN_ALL_VISITOR_COUNT = "min_all_visitor_count";

	private static final int VAL_MIN_ONLINE_VISITOR_COUNT = GetterUtil
			.getInteger(PrefsPropsUtil.getString("hcm.visitor.default-online-visitor-count"), 0);
	private static final int VAL_MIN_ALL_VISITOR_COUNT = GetterUtil
			.getInteger(PrefsPropsUtil.getString("hcm.visitor.default-all-visitor-count"), 0);
	private static final long EXPIRES_VISITOR_TIME_VAL = GetterUtil
			.getLong(PrefsPropsUtil.getString("hcm.visitor.interval-check-online-user"), 360000);

	public int liveUsersCount(long companyId) {
		Map<String, UserTracker> sessionUsers = LiveUsers.getSessionUsers(companyId);

		List<UserTracker> userTrackers = new ArrayList<UserTracker>(sessionUsers.values());
		return userTrackers.size();
	}

	public int countCurrentVisitorByGroupId(long groupId) {
		return visitorTrackerPersistence.countByGroupId(groupId);
	}

	public VisitorTracker addOrUpdateVisitorTracker(long groupId, long companyId, String cookieValue,
			Date visitorTime) {

		VisitorTracker vTracker = visitorTrackerPersistence.fetchByUUID_G(cookieValue, groupId);

		if (vTracker == null) {
			long vTrackerId = counterLocalService.increment();
			vTracker = visitorTrackerPersistence.create(vTrackerId);
			vTracker.setGroupId(groupId);
			vTracker.setCompanyId(companyId);
		}
		vTracker.setVistorTime(visitorTime);
		vTracker = visitorTrackerPersistence.update(vTracker);

		return vTracker;
	}

	public VisitorTracker getVisitorTrackerBy_G_UUID(long groupId, String cookieValue) {
		return visitorTrackerPersistence.fetchByUUID_G(cookieValue, groupId);
	}

	public String getIpAddress(HttpServletRequest request) throws IOException {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		return ip;
	}

	public long getVisitorCountOlderThanVisitorTime(long companyId, long groupId, long expires) {
		return visitorTrackerFinder.getVisitorCountOlderThanVisitorTime(companyId, groupId, expires);
	}

	public String getCookieByName(HttpServletRequest request, String cookieName) {
		String cookiValue = GetterUtil.DEFAULT_STRING;

		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(cookieName)) {
					cookiValue = cookie.getValue();
				}
			}
		}

		return cookiValue;
	}

	public String[] getOnlineVisitorCount(HttpServletRequest request, String cookieName, long companyId, long groupId) {
		Date timeNow = new Date();
		String cookieValue = getCookieByName(request, cookieName);

		String[] responseOnlineData = new String[3]; // theo thu tu lan luot la expiresVisitor[0], uid[1],
														// visitorCount[2]

		responseOnlineData[0] = String.valueOf(timeNow.getTime() + EXPIRES_VISITOR_TIME_VAL);

		int result = 0;

		if (companyId == GetterUtil.DEFAULT_LONG || groupId == GetterUtil.DEFAULT_LONG) {
			result += VAL_MIN_ONLINE_VISITOR_COUNT;
		}

		// Add first visitor by cookieValue
		VisitorTracker visitorTracker = VisitorTrackerLocalServiceUtil.addOrUpdateVisitorTracker(groupId, companyId,
				cookieValue, timeNow);
		if (visitorTracker != null) {
			responseOnlineData[1] = visitorTracker.getUuid();
		}

		// Get minimum online visitor count from custom field.
		ExpandoColumn expandoColumn = ExpandoColumnLocalServiceUtil.getColumn(companyId, Group.class.getName(),
				"CUSTOM_FIELDS", KEY_MIN_ONLINE_VISITOR_COUNT);
		if (Validator.isNotNull(expandoColumn)) {
			result += Integer.parseInt(expandoColumn.getDefaultData());
		}

		// Get online visitor count from database.
		int onineVisitorCount = (int) VisitorTrackerLocalServiceUtil.getVisitorCountOlderThanVisitorTime(companyId,
				groupId, EXPIRES_VISITOR_TIME_VAL);
		result += onineVisitorCount;

		responseOnlineData[2] = String.valueOf(result);

		return responseOnlineData;
	}

	public String getAllVisitorCount(long companyId, long groupId) {
		int result = 0;

		if (Validator.isNull(companyId) || Validator.isNull(groupId)) {
			result += VAL_MIN_ALL_VISITOR_COUNT;
		}

		// Get min online visitor count in customfield
		ExpandoColumn expandoColumnMinLiveUser = ExpandoColumnLocalServiceUtil.getColumn(companyId,
				Group.class.getName(), "CUSTOM_FIELDS", KEY_MIN_ONLINE_VISITOR_COUNT);
		if (Validator.isNotNull(expandoColumnMinLiveUser)) {
			result += Integer.parseInt(expandoColumnMinLiveUser.getDefaultData());
		}

		// Get min all visitor count in customfield
		ExpandoColumn expandoColumnMinVisitor = ExpandoColumnLocalServiceUtil.getColumn(companyId,
				Group.class.getName(), "CUSTOM_FIELDS", KEY_MIN_ALL_VISITOR_COUNT);
		if (Validator.isNotNull(expandoColumnMinVisitor)) {
			result += Integer.parseInt(expandoColumnMinVisitor.getDefaultData());
		}

		// Get total Current visitor today
		int countCurrentVisitor = VisitorTrackerLocalServiceUtil.countCurrentVisitorByGroupId(groupId);
		result += countCurrentVisitor;

		return String.valueOf(result);
	}

	/**
	 * 
	 * @param companyId
	 * @param groupId
	 * @param day
	 * @param month
	 * @param year
	 * @return 1 mang gom 24 phan tu ung voi 24 gio trong 1 ngay, nguoc lai tra ve
	 *         null
	 */
	public long[] statisticVisitorByDate(long companyId, long groupId, int day, int month, int year) {
		int VISITOR_COUNT_DAY = 1;
		long[] results = null;

		if (day == 0 || month == 0 || year == 0) {
			return results;
		}

		List<Object[]> objects = null;
		try {
			objects = visitorTrackerFinder.getVisitorCountByTimeType(companyId, groupId, VISITOR_COUNT_DAY, day, month,
					year);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (objects == null) {
			return results;
		}

		if (objects != null && objects.size() > 0) {
			results = new long[24];
			for (Object[] obj : objects) {
				switch ((int) obj[0]) {
				case 0:
					results[0] = (long) obj[1];
					break;
				case 1:
					results[1] = (long) obj[1];
					break;
				case 2:
					results[2] = (long) obj[1];
					break;
				case 3:
					results[3] = (long) obj[1];
					break;
				case 4:
					results[4] = (long) obj[1];
					break;
				case 5:
					results[5] = (long) obj[1];
					break;
				case 6:
					results[6] = (long) obj[1];
					break;
				case 7:
					results[7] = (long) obj[1];
					break;
				case 8:
					results[8] = (long) obj[1];
					break;
				case 9:
					results[9] = (long) obj[1];
					break;
				case 10:
					results[10] = (long) obj[1];
					break;
				case 11:
					results[11] = (long) obj[1];
					break;
				case 12:
					results[12] = (long) obj[1];
					break;
				case 13:
					results[13] = (long) obj[1];
					break;
				case 14:
					results[14] = (long) obj[1];
					break;
				case 15:
					results[15] = (long) obj[1];
					break;
				case 16:
					results[16] = (long) obj[1];
					break;
				case 17:
					results[17] = (long) obj[1];
					break;
				case 18:
					results[18] = (long) obj[1];
					break;
				case 19:
					results[19] = (long) obj[1];
					break;
				case 20:
					results[20] = (long) obj[1];
					break;
				case 21:
					results[21] = (long) obj[1];
					break;
				case 22:
					results[22] = (long) obj[1];
					break;
				case 23:
					results[23] = (long) obj[1];
					break;
				default:
					break;
				}
			}
		}

		return results;
	}

	/**
	 * 
	 * @param companyId
	 * @param groupId
	 * @param month
	 * @param year
	 * @return 1 mang co so phan tu ung voi so ngay trong thang do, nguoc lai tra ve
	 *         null
	 */
	public long[] statisticVisitorByMonth(long companyId, long groupId, int month, int year) {
		int VISITOR_COUNT_MONTH = 2;
		long[] results = null;

		if (month == 0 || year == 0) {
			return results;
		}

		List<Object[]> objects = null;
		try {
			objects = visitorTrackerFinder.getVisitorCountByTimeType(companyId, groupId, VISITOR_COUNT_MONTH,
					/* day */0, month, year);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (objects == null) {
			return results;
		}

		if (objects != null && objects.size() > 0) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.MONTH, month - 1);

			results = new long[calendar.getActualMaximum(Calendar.DAY_OF_MONTH)];

			for (Object[] obj : objects) {
				switch ((int) obj[0]) {
				case 1:
					results[0] = (long) obj[1];
					break;
				case 2:
					results[1] = (long) obj[1];
					break;
				case 3:
					results[2] = (long) obj[1];
					break;
				case 4:
					results[3] = (long) obj[1];
					break;
				case 5:
					results[4] = (long) obj[1];
					break;
				case 6:
					results[5] = (long) obj[1];
					break;
				case 7:
					results[6] = (long) obj[1];
					break;
				case 8:
					results[7] = (long) obj[1];
					break;
				case 9:
					results[8] = (long) obj[1];
					break;
				case 10:
					results[9] = (long) obj[1];
					break;
				case 11:
					results[10] = (long) obj[1];
					break;
				case 12:
					results[11] = (long) obj[1];
					break;
				case 13:
					results[12] = (long) obj[1];
					break;
				case 14:
					results[13] = (long) obj[1];
					break;
				case 15:
					results[14] = (long) obj[1];
					break;
				case 16:
					results[15] = (long) obj[1];
					break;
				case 17:
					results[16] = (long) obj[1];
					break;
				case 18:
					results[17] = (long) obj[1];
					break;
				case 19:
					results[18] = (long) obj[1];
					break;
				case 20:
					results[19] = (long) obj[1];
					break;
				case 21:
					results[20] = (long) obj[1];
					break;
				case 22:
					results[21] = (long) obj[1];
					break;
				case 23:
					results[22] = (long) obj[1];
					break;
				case 24:
					results[23] = (long) obj[1];
					break;
				case 25:
					results[24] = (long) obj[1];
					break;
				case 26:
					results[25] = (long) obj[1];
					break;
				case 27:
					results[26] = (long) obj[1];
					break;
				case 28:
					results[27] = (long) obj[1];
					break;
				case 29:
					results[28] = (long) obj[1];
					break;
				case 30:
					results[29] = (long) obj[1];
					break;
				case 31:
					results[30] = (long) obj[1];
					break;
				default:
					break;
				}
			}
		}

		return results;
	}

	/**
	 * 
	 * @param companyId
	 * @param groupId
	 * @param year
	 * @return 1 mang co so phan tu ung voi so thang trong nam, nguoc lai tra ve
	 *         null
	 */
	public long[] statisticVisitorByYear(long companyId, long groupId, int year) {
		int VISITOR_COUNT_YEAR = 3;
		long[] results = null;

		if (year == 0) {
			return results;
		}

		List<Object[]> objects = null;
		try {
			objects = visitorTrackerFinder.getVisitorCountByTimeType(companyId, groupId, VISITOR_COUNT_YEAR, /* day */0,
					/* month */0, year);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (objects == null) {
			return results;
		}

		if (objects != null && objects.size() > 0) {
			results = new long[12];

			for (Object[] obj : objects) {
				switch ((int) obj[0]) {
				case 1:
					results[0] = (long) obj[1];
					break;
				case 2:
					results[1] = (long) obj[1];
					break;
				case 3:
					results[2] = (long) obj[1];
					break;
				case 4:
					results[3] = (long) obj[1];
					break;
				case 5:
					results[4] = (long) obj[1];
					break;
				case 6:
					results[5] = (long) obj[1];
					break;
				case 7:
					results[6] = (long) obj[1];
					break;
				case 8:
					results[7] = (long) obj[1];
					break;
				case 9:
					results[8] = (long) obj[1];
					break;
				case 10:
					results[9] = (long) obj[1];
					break;
				case 11:
					results[10] = (long) obj[1];
					break;
				case 12:
					results[11] = (long) obj[1];
					break;
				default:
					break;
				}
			}
		}

		return results;
	}
}