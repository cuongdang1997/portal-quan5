package com.swt.portal.visitor.service.persistence.impl;

import java.io.Console;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.swt.portal.visitor.service.persistence.VisitorTrackerFinder;

public class VisitorTrackerFinderImpl extends VisitorTrackerFinderBaseImpl implements VisitorTrackerFinder {

	public static final int VISITOR_COUNT_DAY = 1;
	public static final int VISITOR_COUNT_MONTH = 2;
	public static final int VISITOR_COUNT_YEAR = 3;

	public static final String TYPE_COLUMN_NAME = "TYPE_VALUE";

	public static final String GET_VISITOR_COUNT_OLDER_THAN_VISITOR_TIME = VisitorTrackerFinder.class.getName()
			+ ".getVisitorCountOlderThanVisitorTime";

	public static final String GET_VISITOR_COUNT_BY_TIME_TYPE = VisitorTrackerFinder.class.getName()
			+ ".getVisitorCountByTimeType";

	@Override
	public long getVisitorCountOlderThanVisitorTime(long companyId, long groupId, long expires) {

		Calendar now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
		System.out.println("vistorTime " + CalendarUtil.getTimestamp(now.getTime()));
		
		now.setTimeInMillis(now.getTimeInMillis() - expires);
		System.out.println("expires " + expires);
		
		Timestamp vistorTime = CalendarUtil.getTimestamp(now.getTime());
		System.out.println("vistorTime " + vistorTime);
		Session session = null;
		try {
			session = openSession();

			String sql = CustomSQLUtil.get(getClass(), GET_VISITOR_COUNT_OLDER_THAN_VISITOR_TIME);
			System.out.println("sql " + sql);
			SQLQuery q = session.createSynchronizedSQLQuery(sql);
			q.addScalar(COUNT_COLUMN_NAME, Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(groupId);
			qPos.add(companyId);
			qPos.add(vistorTime);
			
			Iterator<Long> itr = q.iterate();

			if (itr.hasNext()) {
				Long count = itr.next();

				if (count != null) {
					return count.intValue();
				}
			}

			return GetterUtil.DEFAULT_LONG;

		} catch (Exception e) {
			throw new SystemException(e);
		} finally {
			closeSession(session);
		}

	}

	@Override
	public List<Object[]> getVisitorCountByTimeType(long companyId, long groupId, int visitorType, int day, int month,
			int year) {

		Session session = null;
		try {
			session = openSession();

			String sql = CustomSQLUtil.get(getClass(), GET_VISITOR_COUNT_BY_TIME_TYPE);

			if (visitorType == VISITOR_COUNT_DAY) {
				sql = sql.replace("[$TIME_TYPE$]", "HOUR(vt.vistorTime)");
				sql = sql.replace("[$TIME_CONDITION_TYPE$]", "vt.vistorTime BETWEEN ? AND ?");
			} else if (visitorType == VISITOR_COUNT_MONTH) {
				sql = sql.replace("[$TIME_TYPE$]", "DAYOFMONTH(vt.vistorTime)");
				sql = sql.replace("[$TIME_CONDITION_TYPE$]", "MONTH(vt.vistorTime) =? AND YEAR(vistorTime) =?");
			} else {
				sql = sql.replace("[$TIME_TYPE$]", "MONTH(vt.vistorTime)");
				sql = sql.replace("[$TIME_CONDITION_TYPE$]", "YEAR(vistorTime) =?");
			}

			SQLQuery q = session.createSynchronizedSQLQuery(sql);
			q.addScalar(TYPE_COLUMN_NAME, Type.INTEGER);
			q.addScalar(COUNT_COLUMN_NAME, Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(groupId);
			qPos.add(companyId);

			if (visitorType == VISITOR_COUNT_DAY) {
				Calendar customDate = Calendar.getInstance();

				customDate.set(year, month - 1, day, 00, 00, 00);
				Timestamp vistorTime = CalendarUtil.getTimestamp(customDate.getTime());
				qPos.add(vistorTime);

				customDate.set(year, month - 1, day, 23, 59, 59);
				vistorTime = CalendarUtil.getTimestamp(customDate.getTime());
				qPos.add(vistorTime);
			} else if (visitorType == VISITOR_COUNT_MONTH) {
				qPos.add(month);
				qPos.add(year);
			} else {
				qPos.add(year);
			}

			return (List<Object[]>) QueryUtil.list(q, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);

		} catch (Exception e) {
			throw new SystemException(e);
		} finally {
			closeSession(session);
		}
	}
}
