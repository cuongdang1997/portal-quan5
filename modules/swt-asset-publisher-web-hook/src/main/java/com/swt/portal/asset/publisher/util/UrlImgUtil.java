package com.swt.portal.asset.publisher.util;

import java.util.ArrayList;
import java.util.List;

public class UrlImgUtil {
	
	/**
	 * 
	 * @param Content article 
	 * @return List url if content exits img  otherwise wise null
	 */
	public static List<String> getUrlImgByContent(String content){
		List<String> urls = null;
		if(content.length() == 0 || !content.contains("<img")) {
			return urls;
		}
		
		urls = new ArrayList<String>();
		
		int firstIndexImg = content.indexOf("<img");
		content = content.substring(firstIndexImg);
		String[] arrContent= content.split("/>", 2);
		
		String srcImg = "src=\"";
		int firstIndexSrcImg = content.indexOf(srcImg) + srcImg.length();
		
		String firstImgUrl = arrContent[0].substring(firstIndexSrcImg);
		int lastIndexSrcImg = firstImgUrl.indexOf("\"");
		
		firstImgUrl = firstImgUrl.substring(0, lastIndexSrcImg);
		if(firstImgUrl.length() > 0) {
			urls.add(firstImgUrl);
		}
		
		if(arrContent.length > 1) {
			if(getUrlImgByContent(arrContent[1])!=null) {
				urls.addAll(getUrlImgByContent(arrContent[1]));
			}
		}
		
		return urls;
	}
	
}
