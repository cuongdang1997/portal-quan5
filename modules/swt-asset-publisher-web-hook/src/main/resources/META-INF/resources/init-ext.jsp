<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@page import="com.swt.portal.asset.publisher.util.UrlImgUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletRequest" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletResponse" %>
<%@page import="com.liferay.portal.kernel.portlet.PortletURLUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetRenderer"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetRendererFactory"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.ArrayUtil"%>
<%@page import="com.liferay.asset.kernel.service.persistence.AssetEntryQuery"%>
<%@page import="com.liferay.portal.kernel.util.ArrayUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.journal.model.JournalArticle"%>
<%@page import="javax.portlet.PortletResponse" %>
<%@page import="javax.portlet.WindowState"%>
<%@page import="java.util.Comparator"%>

<% 

String portletNameId = assetPublisherCustomizer.getPortletId();
		 
PortletRequest portletRequest_tmp = (PortletRequest)request.getAttribute("javax.portlet.request");

PortletResponse portletResponse_tmp = (PortletResponse)request.getAttribute("javax.portlet.response");
		 
boolean isEnableOldNews = GetterUtil.getBoolean(portletPreferences.getValue("enableOldNews", String.valueOf(false)),false);

int numberOldNews = GetterUtil.getInteger(portletPreferences.getValue("numberOldNews", String.valueOf(GetterUtil.DEFAULT_INTEGER)),GetterUtil.DEFAULT_INTEGER);
		 
String dateDisplayFormatOldNews = portletPreferences.getValue("dateDisplayFormatOldNews", "dd/MM/yyyy");

boolean isEnableNewerNews = GetterUtil.getBoolean(portletPreferences.getValue("enableNewerNews", String.valueOf(false)),false);

int numberNewerNews = GetterUtil.getInteger(portletPreferences.getValue("numberNewerNews", String.valueOf(GetterUtil.DEFAULT_INTEGER)),GetterUtil.DEFAULT_INTEGER);
		 
String dateDisplayFormatNewerNews = portletPreferences.getValue("dateDisplayFormatNewerNews", "dd/MM/yyyy");

int numberLimitDate = GetterUtil.getInteger(portletPreferences.getValue("numberLimitDate", String.valueOf(GetterUtil.DEFAULT_INTEGER)),GetterUtil.DEFAULT_INTEGER);

boolean isEnableButtonEdit = GetterUtil.getBoolean(portletPreferences.getValue("enableButtonEdit", String.valueOf(false)),false);

//CmsVocabularyId dung cho tin moi hon va tin cu hon
long cmsVocabularyId = PrefsPropsUtil.getLong("hcm.constants.cms-vocabulary-id", 40588);

%>
