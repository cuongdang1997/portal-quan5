<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="com.liferay.asset.kernel.model.DDMFormValuesReader" %>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMFormFieldValue" %>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMFormValues" %>
<%@page import="com.liferay.journal.model.JournalArticle"%>
<%@page import="com.liferay.message.boards.kernel.model.MBMessageConstants"%>
<%@page import="com.liferay.message.boards.kernel.service.MBMessageLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.security.auth.AuthTokenUtil"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>

<%@page import="org.w3c.dom.html.HTMLUListElement"%>

<%@ include file="/init.jsp" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/picturefill.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/lightgallery-all.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.mousewheel.min.js"></script>
<link  rel="stylesheet" type="text/css"  href="<%=request.getContextPath()%>/css/lightgallery.min.css"/>

<style scoped>
	.h2-header-title {
		margin: 0 !important;
	}
	
	.h3-header-sub-title {
		margin: 0 !important;
	}
	
	.header-title {
		color: #0072bc;
		display: block;
		font-size: 16px;
		font-weight: bold;
		padding: 10px;
		text-decoration: none;
	}
	
	.header-sub-title {
		color: #0072bc;
		display: block;
		font-size: 14px;
		font-weight: bold;
		padding: 10px;
		text-decoration: none;
	}
	
	.row-top-panel {
		background-color: #F6F5F5;
		display: inline-block;
		margin-bottom: 10px;
		padding: 5px;
		vertical-align: middle;
		width: 100%;
	}
	
	.asset-publish-date {
		float: left;
	}
	
	.asset-full-content {
		padding: 10px;
	}
	
	.row-social-bookmarks {
		display: inline-block;
		width: 100%;
	}
	
	.asset-mail-to {
		margin-right: 3px;
	}
	
	.journal-content-article {
		font-size: 14px;
	}
	
	.row-bottom-panel {
		background-color: #F6F5F5;
		display: inline-block;
		margin-top: 10px;
		margin-bottom: 10px;
		padding: 5px;
		vertical-align: middle;
		width: 100%;
	}
	
	.asset-view-count {
		float: left;
	}
	
	.asset-back-to a {
		background: url("<%=request.getContextPath()%>/icons/back.png")
			no-repeat scroll left top transparent;
		color: #686868 !important;
		cursor: pointer;
		margin-left: 5px;
		padding: 7px 10px 6px 22px;
		background-position: 5px 7px;
	}
	
	@media only screen and (max-width: 767px) {

		.asset-publish-date {
			float: left;
			margin: 2px 0 4px 0;
		}
	
		.asset-convenient-actions{
			float: right;
		}
	
		.row-social-bookmarks{
			width: 100%;
			float: right;
			right: 0;
			margin-left: auto;
			position: relative;
		}
	
		.pull-right {
			float: right !important;
			width: unset;
			margin: 0;
		}
	
		.taglib-icon-list li {
			float: left;
			margin: 0 0 0 5px;
		}
	
		.row-bottom-panel{
			padding: 10px;
		}
	
		.asset-edit-menu {
			display: none;
		}
		
		.asset-view-count {
			width: 100%;
		}
		
		.asset-print {
			background: url(/o/asset-publisher-web/icons/print.gif) no-repeat scroll left top transparent;
			color: #686868 !important;
			cursor: pointer;
			margin-left: 10px;
			padding: 1px 0 4px 18px !important;
		}
		
		.asset-back-to a {
			padding: 7px 0 6px 22px;
		}
	}
	
	/*============ Discussion ============*/
	
	#swt-border-comment .add-comment .subscribe-link a {
		display: none;
	}
	
	.swt-style-comment {
		color: #015286;
		font-weight: bold;
		padding-right: 5px;
		font-size: 13px;
	}
	
	#swt-border-comment .add-comment .panel {
		box-shadow: unset;
		margin-bottom: 5px !important;
	}
	
	#swt-border-comment .add-comment .panel .panel-body {
		padding: unset;
	}
	
	#swt-border-comment .add-comment .panel .panel-body .alloy-editor-container {
		border: 1px solid;
		border-color: #BFBFBF #DEDEDE #DEDEDE #BFBFBF;
	}
	
	#swt-border-comment .add-comment .panel .panel-body .alloy-editor-container .alloy-editor {
		border-left: unset !important;
		padding-left: 10px;
		padding-top: 10px;
	}
	
	#swt-border-comment .add-comment .button-holder .btn-comment {
		padding: 6px 12px !important;
		font-size: 14px;
		float: right;
		background-color: #015286;
		border-radius: unset;
		position: relative;
		width: 105px;
	}
	
	#swt-border-comment .add-comment .lfr-discussion-details {
		display: none;
	}
	
	#swt-border-comment .lfr-discussion-form-reply .lfr-discussion-details {
		display: none;
	}
	
	#swt-border-comment .lfr-discussion-form-reply .button-holder .btn-comment {
		padding: 6px 12px !important;
		font-size: 14px;
		background-color: #015286;
	}
	
	#swt-border-comment .lfr-discussion-form-reply .button-holder .btn-cancel {
		background-color: transparent;
	}
	
	#swt-border-comment .add-comment .button-holder .btn-comment:after {
		content: 'Gửi bình luận';
		position: absolute;
		top: 5px;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #015286;
	}
	
	@media only screen and (min-width: 768px) {
		.taglib-discussion .lfr-discussion-body {
			margin-left: unset;
		}
	}
	
	#swt-border-comment .add-comment div[class] {
		margin-top: 5px;
	}
	
	#swt-border-comment .swt-comment-idea {
		background: #eee;
		height: 26px;
		width: 100%;
	}
	
	#swt-border-comment .swt-comment-lable-idea,
	swt-comment-count-idea {
		font: 400 14px/26px arial;
		color: #015286;
		font-weight: bold;
		padding-left: 1%;
	}
	
	#swt-border-comment .lfr-discussion-form-reply .alloy-editor-container .alloy-editor {
		border-left: unset !important;
	}
	
	#swt-border-comment .lfr-discussion-message-author {
		margin-left: 60px;
	}
	
	#swt-border-comment .taglib-workflow-status {
		margin-left: 16px;
	}
	
	#swt-border-comment .lfr-discussion-message-body {
		padding-top: 10px;
	}
	
	#swt-border-comment .lfr-discussion-form-reply .button-holder .btn-comment {
		border-radius: unset;
	}
	
	.swt-pading-unset-commnet {
		padding-left: unset !important;
		padding-right: unset !important;
	}

	/*============ Image Gallery ============*/
	
	.swt-border-gallery {
		display: none;
	}
	
	.swt-border-gallery ul {
		list-style: none;
		float: left;
		margin-left: 5px;
	}
	
	.swt-border-gallery ul li {
		float: left;
		width: 185px;
		padding: 0px;
	}
	
	.swt-border-gallery ul li a {
		text-decoration: none;
	}
	
	.swt-border-gallery ul li a img {
		width: 100%;
		height: 200px;
	}
	
	/*============ Video ============*/
	
	.liferayckevideo.video-container > .video {
		width: 100% !important;
		height: auto !important;
	}
	
	.liferayckevideo.video-container video {
		width: inherit !important;
		height: inherit !important;
	}
	
	.journal-content-article video{
		width: 100% ;
		height: 100% ;
	}
</style>

<%
	String redirect = PortalUtil.escapeRedirect(ParamUtil.getString(request, "redirect"));

	if (Validator.isNull(redirect)) {
		redirect = ParamUtil.getString(PortalUtil.getOriginalServletRequest(request), "redirect");
	}

	boolean showBackURL = GetterUtil.getBoolean(request.getAttribute("view.jsp-showBackURL"));

	if (Validator.isNull(redirect)) {
		PortletURL portletURL = renderResponse.createRenderURL();

		portletURL.setParameter("mvcPath", "/view.jsp");

		redirect = portletURL.toString();
	}

	List results = (List) request.getAttribute("view.jsp-results");

	int assetEntryIndex = ((Integer) request.getAttribute("view.jsp-assetEntryIndex")).intValue();

	AssetEntry assetEntry = (AssetEntry) request.getAttribute("view.jsp-assetEntry");
	AssetRendererFactory<?> assetRendererFactory = (AssetRendererFactory<?>) request
			.getAttribute("view.jsp-assetRendererFactory");
	AssetRenderer<?> assetRenderer = (AssetRenderer<?>) request.getAttribute("view.jsp-assetRenderer");

	String languageId = LanguageUtil.getLanguageId(request);

	String title = assetRenderer.getTitle(LocaleUtil.fromLanguageId(languageId));
	
	boolean print = ((Boolean) request.getAttribute("view.jsp-print")).booleanValue();
	boolean viewInContext = ((Boolean) request.getAttribute("view.jsp-viewInContext")).booleanValue();
	boolean workflowEnabled = WorkflowDefinitionLinkLocalServiceUtil.hasWorkflowDefinitionLink(
			assetEntry.getCompanyId(), assetEntry.getGroupId(), assetEntry.getClassName());

	assetPublisherDisplayContext.setLayoutAssetEntry(assetEntry);

	assetEntry = assetPublisherDisplayContext.incrementViewCounter(assetEntry);

	request.setAttribute("view.jsp-fullContentRedirect", workflowEnabled ? redirect : currentURL);
	request.setAttribute("view.jsp-showIconLabel", true);
	
	Object dummyObj = new Object();
	
	// Get the CMS DDMStructure ID. 40580 is the ID value of HCM Portal site.
	long cmsStructureId = PrefsPropsUtil.getLong("hcm.constants.cms-structure-id", 40580);
	long moduleVocabularyId = PrefsPropsUtil.getLong("hcm.constants.module-vocabulary-id", 33752);
	
	// If it's a CMS article, get the sub-title (if any).
	String subTitle = null;
	DDMFormValuesReader ddmFormValuesReader = assetRenderer.getDDMFormValuesReader();
	if (ddmFormValuesReader != null) {
		DDMFormValues ddmFormValues = ddmFormValuesReader.getDDMFormValues();
		for (DDMFormFieldValue ddmFormFieldValue : ddmFormValues.getDDMFormFieldValues()) {
			if (ddmFormFieldValue.getName().equals("subTitle")) {
				subTitle = ddmFormFieldValue.getValue().getString(locale).trim();
				break;
			}
		}
	}
	
	long cateRelated = GetterUtil.getLong(PortalUtil.escapeRedirect(ParamUtil.getString(request, "category-related")));
%>
<!-- Show category for CMS -->
<c:if test="<%=subTitle != null %>">
<%
//Get CMS article's main category
	String mainCategory = "";
	if(cateRelated != 0){
		AssetCategory cate = AssetCategoryLocalServiceUtil.getAssetCategory(cateRelated);
		mainCategory = cate.getName();
		cateRelated = cate.getCategoryId();
	}else{
		for(AssetCategory cate : assetEntry.getCategories()) {
			if(cate.getVocabularyId() != moduleVocabularyId){
				AssetVocabulary vocab = AssetVocabularyLocalServiceUtil.getAssetVocabulary(cate.getVocabularyId());
				if(vocab.getName().equals(String.valueOf(cmsStructureId) + "_Chuyên mục")){
					mainCategory = cate.getName();
					cateRelated = cate.getCategoryId();
				}
			}
		}
	}
%>
	<h2 class="portlet-title-text"> 
		<img class="portlet-title-icon" alt="title" src="/o/hcm-default-theme/images/imageVanBan/iconTitle.jpg">
			<span>
				<%=mainCategory %>
			</span> 
	</h2>
</c:if>
<div class="h2 h2-header-title">
	<c:if test="<%= assetPublisherDisplayContext.isShowAssetTitle() %>">
		<span class="header-title"><%= HtmlUtil.escape(title) %></span>
	</c:if>
</div>

<c:if test="<%= subTitle != null && subTitle.length() != 0 %>">
	<div class="h3 h3-header-sub-title">
		<span class="header-sub-title"><%= HtmlUtil.escape(subTitle) %></span>
	</div>
</c:if>

<div class="asset-full-content clearfix <%= assetPublisherDisplayContext.isDefaultAssetPublisher() ? "default-asset-publisher" : StringPool.BLANK %> <%= assetPublisherDisplayContext.isShowAssetTitle() ? "show-asset-title" : "no-title" %>">
	<c:if test="<%= !print %>">
		<div class="row-fluid row-top-panel">
			<div class="asset-publish-date">
				<%= DateUtil.getDate(assetEntry.getPublishDate() == null ? assetEntry.getCreateDate() : assetEntry.getPublishDate(), "EEEE, dd/MM/yyyy, HH:mm a", locale) %>
			</div>
			<div class="asset-convenient-actions">
				<%@ include file="convenient_actions.jspf" %>
			</div>
		</div>
	</c:if>
	
	<c:if test="<%= (assetPublisherDisplayContext.isShowAvailableLocales() && assetRenderer.isLocalizable()) %>">
		<div class="asset-user-actions">
			<c:if test="<%= assetPublisherDisplayContext.isShowAvailableLocales() && assetRenderer.isLocalizable() && !print %>">
				<%
					String[] availableLanguageIds = assetRenderer.getAvailableLanguageIds();
				%>
				
				<c:if test="<%= availableLanguageIds.length > 1 %>">
					<c:if test="<%= assetPublisherDisplayContext.isEnableConversions() || assetPublisherDisplayContext.isEnablePrint() %>">
						<div class="locale-separator"> </div>
					</c:if>
					<div class="locale-actions">
						<liferay-ui:language
							formAction="<%= currentURL %>"
							languageId="<%= languageId %>"
							languageIds="<%= availableLanguageIds %>"
						/>
					</div>
				</c:if>
			</c:if>
		</div>
	</c:if>

	<%
		PortletURL viewFullContentURL = renderResponse.createRenderURL();

		viewFullContentURL.setParameter("mvcPath", "/view_content.jsp");
		viewFullContentURL.setParameter("type", assetRendererFactory.getType());

		if (print) {
			viewFullContentURL.setParameter("viewMode", Constants.PRINT);
		}

		if (Validator.isNotNull(assetRenderer.getUrlTitle())) {
			if (assetRenderer.getGroupId() != scopeGroupId) {
				viewFullContentURL.setParameter("groupId", String.valueOf(assetRenderer.getGroupId()));
			}
			viewFullContentURL.setParameter("urlTitle", assetRenderer.getUrlTitle());
		}

		String socialBookmarksDisplayPosition = assetPublisherDisplayContext.getSocialBookmarksDisplayPosition();
	%>

	<div class="asset-content" id="<portlet:namespace /><%= assetEntry.getEntryId() %>">
		<c:if test='<%= assetPublisherDisplayContext.isEnableSocialBookmarks() && socialBookmarksDisplayPosition.equals("top") && !print %>'>
			<div class="row-fluid row-social-bookmarks">
				<div class="pull-right">
					<liferay-ui:social-bookmarks
						contentId="<%= String.valueOf(assetEntry.getEntryId()) %>"
						displayStyle="<%= assetPublisherDisplayContext.getSocialBookmarksDisplayStyle() %>"
						target="_blank"
						title="<%= title %>"
						url="<%= PortalUtil.getCanonicalURL(viewFullContentURL.toString(), themeDisplay, layout) %>"
					/>
				</div>
				<div class="pull-right asset-mail-to">
					<%
						String mailBody = assetEntry.getTitle(locale) + ": " + viewFullContentURL.toString();
					%>
					<a href='<%="mailto:%20?body=" + HtmlUtil.escape(mailBody)%>'>
						<img src="<%=request.getContextPath()%>/icons/share-email.png">
					</a>
				</div>
			</div>
		</c:if>
		
		<liferay-ui:asset-display
			assetEntry="<%= assetEntry %>"
			assetRenderer="<%= assetRenderer %>"
			assetRendererFactory="<%= assetRendererFactory %>"
			showExtraInfo="<%= assetPublisherDisplayContext.isShowExtraInfo() %>"
		/>
			
		<c:if test='<%= assetPublisherDisplayContext.isEnableSocialBookmarks() && socialBookmarksDisplayPosition.equals("bottom") && !print %>'>
			<div class="row-fluid row-social-bookmarks">
				<liferay-ui:social-bookmarks
					displayStyle="<%= assetPublisherDisplayContext.getSocialBookmarksDisplayStyle() %>"
					target="_blank"
					title="<%= title %>"
					url="<%= PortalUtil.getCanonicalURL(viewFullContentURL.toString(), themeDisplay, layout) %>"
				/>
			</div>
		</c:if>
		
		<div class="row-fluid row-bottom-panel">
			<c:if test="<%= !print && assetPublisherDisplayContext.isEnableViewCountIncrement() %>">
				<div class="asset-view-count">
					<span><%=LanguageUtil.format(request, "num-of-views", dummyObj) + ": " + assetEntry.getViewCount()%></span>
				</div>
			</c:if>
			
			<c:if test="<%= showBackURL && Validator.isNotNull(redirect) && !print %>">
				<div class="asset-back-to pull-right">
					<a href="<%=redirect%>" data-senna-off="true"><%=LanguageUtil.format(request, "back", dummyObj)%></a>
				</div>
			</c:if>
			
			<c:if test="<%= assetPublisherDisplayContext.isEnableConversions() && assetRenderer.isConvertible() && !print %>">
				<div class="export-actions pull-right">
					<%@ include file="/asset_export.jspf" %>
				</div>
			</c:if>
			
			<c:if test="<%= (assetPublisherDisplayContext.isEnablePrint() && assetRenderer.isPrintable()) %>">
				<div class="print-action pull-right">
					<%@ include file="/asset_print.jspf" %>
				</div>
			</c:if>
			
			<c:if test="<%= assetPublisherDisplayContext.isEnableFlags() && !print %>">
				<div class="asset-flag pull-right">
					<liferay-flags:flags
						className="<%= assetEntry.getClassName() %>"
						classPK="<%= assetEntry.getClassPK() %>"
						contentTitle="<%= title %>"
						reportedUserId="<%= assetRenderer.getUserId() %>"
					/>
				</div>
			</c:if>
	
			<c:if test="<%= assetPublisherDisplayContext.isEnableRatings() && assetRenderer.isRatable() && !print %>">
				<div class="asset-ratings pull-right">
					<liferay-ui:ratings
						className="<%= assetEntry.getClassName() %>"
						classPK="<%= assetEntry.getClassPK() %>"
					/>
				</div>
			</c:if>
			
			<c:if test="<%= !print %>">
				<liferay-util:include page="/asset_actions.jsp" servletContext="<%= application %>" />
			</c:if>
			
			<c:if test="<%= assetPublisherDisplayContext.isShowContextLink(assetRenderer.getGroupId(), assetRendererFactory.getPortletId()) && !print && assetEntry.isVisible() %>">
				<div class="asset-more">
					<a href="<%= assetRenderer.getURLViewInContext(liferayPortletRequest, liferayPortletResponse, HttpUtil.setParameter(viewFullContentURL.toString(), "redirect", currentURL)) %>"><liferay-ui:message key="<%= assetRenderer.getViewInContextMessage() %>" /> &raquo;</a>
				</div>
			</c:if>
		</div>
		
		<c:if test="<%= assetPublisherDisplayContext.isEnableRelatedAssets() && !print %>">
			<%
				PortletURL assetLingsURL = renderResponse.createRenderURL();
				assetLingsURL.setParameter("mvcPath", "/view_content.jsp");
				
				if (print) {
					assetLingsURL.setParameter("viewMode", Constants.PRINT);
				}
			%>

			<liferay-ui:asset-links
				assetEntryId="<%= assetEntry.getEntryId() %>"
				portletURL="<%= assetLingsURL %>"
				viewInContext="<%= viewInContext %>"
			/>
		</c:if>
		
		<!-- SWT display newer news -->
		<c:if test="<%=isEnableNewerNews%>">
			<%@ include file="/display/swt_newer_news.jspf" %>
		</c:if>
		
		<!-- SWT display old news -->
		<c:if test="<%=isEnableOldNews%>">
			<%@ include file="/display/swt_old_news.jspf" %>
		</c:if>

		<c:if test="<%= assetPublisherDisplayContext.isEnableComments() && assetRenderer.isCommentable() && !print %>">
			<div class="col-md-12 swt-pading-unset-commnet">
				<div id="swt-border-comment">
					<liferay-ui:discussion
						className="<%= assetEntry.getClassName() %>"
						classPK="<%= assetEntry.getClassPK() %>"
						formName='<%= "fm" + assetEntry.getClassPK() %>'
						ratingsEnabled="<%= assetPublisherDisplayContext.isEnableCommentRatings() %>"
						redirect="<%= currentURL %>"
						userId="<%= assetRenderer.getUserId() %>"
					/>
				</div>
			</div>
		</c:if>
	</div>

	<liferay-ui:asset-metadata
		className="<%= assetEntry.getClassName() %>"
		classPK="<%= assetEntry.getClassPK() %>"
		filterByMetadata="<%= true %>"
		metadataFields="<%= assetPublisherDisplayContext.getMetadataFields() %>"
	/>
</div>

<c:if test="<%= !assetPublisherDisplayContext.isShowAssetTitle() && ((assetEntryIndex + 1) < results.size()) %>">
	<div class="separator"><!-- --></div>
</c:if>

<%
List<String> urlImgContentArticles = new ArrayList<String>();
try {
	JournalArticle articleContent = JournalArticleLocalServiceUtil
			.getLatestArticle(assetEntry.getClassPK());

	String urlImg = GetterUtil.DEFAULT_STRING;

	urlImgContentArticles = UrlImgUtil.getUrlImgByContent(articleContent.getContent());

} catch (PortalException e) {
	e.printStackTrace();
}
%>

<div class="swt-border-gallery">
	<ul id="lightgallery" class="list-unstyled row">
		<% if (urlImgContentArticles != null && urlImgContentArticles.size() > 0) { %>
			<% int index = 0; %>
			<% for (String imgURL : urlImgContentArticles) { %>
				<li class="col-xs-6 col-sm-4 col-md-4" data-responsive="<%= imgURL %> 375, <%= imgURL %> 480, <%= imgURL %> 800" data-src="<%= imgURL %>" data-sub-html="">
					<a href="">
						<img class="img-responsive"src="<%=imgURL%>">
					</a>
				</li>
				<% index++; %>
			<% } %>
		<% } %>
	</ul>
</div>

<%
int statusApproved = 0;

long classNameId = PortalUtil.getClassNameId(JournalArticle.class.getName());

int messagesCount = MBMessageLocalServiceUtil.getDiscussionMessagesCount(classNameId, assetEntry.getClassPK(), statusApproved);
%>

<script>
	$(document).ready(function() {
		$('#lightgallery').lightGallery();
		
	    $('.journal-content-article img').on('click', function () {
		   	var	currentSRC = $(this).attr('src').trim();

		   	$("#lightgallery .img-responsive").each(function() {
		    	var srcImg = $(this).attr('src').trim();
		    	if (currentSRC == srcImg) {
		        	$(this).click();
		    	}
		    });
		});
		
		<% if (assetPublisherDisplayContext.isEnableComments() && assetRenderer.isCommentable() && !print) { %>
			$("#swt-border-comment .add-comment ").prepend( '<span class="glyphicon glyphicon-comment swt-style-comment"></span>' );
			$("#swt-border-comment .add-comment .glyphicon-comment").after( '<span class="swt-style-comment">BÌNH LUẬN </span>' );
			$("#swt-border-comment .add-comment").after('<div class="swt-comment-idea"><span class="swt-comment-lable-idea">Ý KIẾN BẠN ĐỌC <span class="swt-comment-count-idea">(<%=messagesCount%>)</span></span></div>');
			
			var checkDivBorderDiscriptionComment = $(".swt-comment-idea").next().next().children().size();
			if (checkDivBorderDiscriptionComment > 0) {
				$(".swt-comment-idea" ).next().next().css("margin-top", "20px" );
			}
		<% } %>
		
		// dung.nguyen: The image size (from old site data) was limited by outer-table width.
		$(".asset-full-content table[width='200']:has(img)").css("width", "100%");
		
		// dung.nguyen: If the user is not signed in, show "create anonymous account" form by default.
		if (!themeDisplay.isSignedIn()) {
			if ($("button[id*='_postReplyButton']").length !== 0) {
				$("button[id*='_postReplyButton']").prop("onclick", null);
				$("button[id*='_postReplyButton']").off("click").on("click", function() {
					// These namespaces will be used by popup after login.
					window.namespace = '<portlet:namespace />';
					window.randomNamespace = $(this).attr("id").substr(window.namespace.length, 5);
					
					<%
					// Get the authentication token for Fast Login portlet.
					String authToken = AuthTokenUtil.getToken(request, themeDisplay.getLayout().getPlid(), "com_liferay_login_web_portlet_FastLoginPortlet");
					%>
					
					// Construct the URL to Fast Login portlet.
					var createAnonymousAccountURL = themeDisplay.getLayoutURL();
					createAnonymousAccountURL += "?p_p_id=com_liferay_login_web_portlet_FastLoginPortlet";
					createAnonymousAccountURL += "&p_p_lifecycle=0";
					createAnonymousAccountURL += "&p_p_state=pop_up";
					createAnonymousAccountURL += "&p_p_mode=view";
					createAnonymousAccountURL += "&saveLastPath=false";
					createAnonymousAccountURL += "&_com_liferay_login_web_portlet_FastLoginPortlet_mvcRenderCommandName=%2Flogin%2Fcreate_anonymous_account";
					createAnonymousAccountURL += "&p_p_auth=<%= authToken %>";
					
					// Open Fast Login portlet in popup mode.
					Liferay.Util.openWindow({
						dialog: {
							height: 600,
							width: 480
						},
						id: '<portlet:namespace />signInDialog',
						title: 'Nhập thông tin của bạn',
						uri: createAnonymousAccountURL
					});
				});
			}
		}
	});
</script>
