<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

<%
String randomNamespace = PortalUtil.generateRandomKey(request, "portlet_asset_publisher_edit_query_rule") + StringPool.UNDERLINE;

long[] categorizableGroupIds = (long[])request.getAttribute("configuration.jsp-categorizableGroupIds");

if (categorizableGroupIds == null) {
	categorizableGroupIds = StringUtil.split(ParamUtil.getString(request, "categorizableGroupIds"), 0l);
}

int index = ParamUtil.getInteger(request, "index", GetterUtil.getInteger((String)request.getAttribute("configuration.jsp-index")));
int queryLogicIndex = GetterUtil.getInteger((String)request.getAttribute("configuration.jsp-queryLogicIndex"), -1);

boolean queryContains = true;
boolean queryAndOperator = false;
String queryName = "assetTags";
String queryValues = null;

if (queryLogicIndex >= 0) {
	queryContains = PrefsParamUtil.getBoolean(portletPreferences, request, "queryContains" + queryLogicIndex, true);
	queryAndOperator = PrefsParamUtil.getBoolean(portletPreferences, request, "queryAndOperator" + queryLogicIndex);
	queryName = PrefsParamUtil.getString(portletPreferences, request, "queryName" + queryLogicIndex, "assetTags");
	queryValues = StringUtil.merge(portletPreferences.getValues("queryValues" + queryLogicIndex, new String[0]));

	if (Objects.equals(queryName, "assetTags")) {
		queryValues = ParamUtil.getString(request, "queryTagNames" + queryLogicIndex, queryValues);

		queryValues = AssetPublisherUtil.filterAssetTagNames(scopeGroupId, queryValues);
	}
	else {
		queryValues = ParamUtil.getString(request, "queryCategoryIds" + queryLogicIndex, queryValues);
	}
}
%>

<style>
	li.tree-node {
		display: none;
	}
</style>

<div class="field-row form-inline query-row">
	<aui:select inlineField="<%= true %>" label="" name='<%= "queryContains" + index %>' title="query-contains">
		<aui:option label="contains" selected="<%= queryContains %>" value="<%= true %>" />
		<aui:option label="does-not-contain" selected="<%= !queryContains %>" value="<%= false %>" />
	</aui:select>

	<aui:select inlineField="<%= true %>" label="" name='<%= "queryAndOperator" + index %>' title="and-operator">
		<aui:option label="all" selected="<%= queryAndOperator %>" value="<%= true %>" />
		<aui:option label="any" selected="<%= !queryAndOperator %>" value="<%= false %>" />
	</aui:select>

	<aui:select cssClass="asset-query-name" id='<%= randomNamespace + "selector" %>' inlineField="<%= true %>" label="of-the-following" name='<%= "queryName" + index %>'>
		<aui:option label="tags" selected='<%= Objects.equals(queryName, "assetTags") %>' value="assetTags" />
		<aui:option label="categories" selected='<%= Objects.equals(queryName, "assetCategories") %>' value="assetCategories" />
	</aui:select>

	<div class="field tags-selector <%= Objects.equals(queryName, "assetTags") ? StringPool.BLANK : "hide" %>">
		<liferay-ui:asset-tags-selector
			curTags='<%= Objects.equals(queryName, "assetTags") ? queryValues : null %>'
			groupIds="<%= categorizableGroupIds %>"
			hiddenInput='<%= "queryTagNames" + index %>'
		/>
	</div>

	<div class="categories-selector field <%= Objects.equals(queryName, "assetCategories") ? StringPool.BLANK : "hide" %>">
		<liferay-ui:asset-categories-selector
			curCategoryIds='<%= Objects.equals(queryName, "assetCategories") ? queryValues : null %>'
			groupIds="<%= categorizableGroupIds %>"
			hiddenInput='<%= "queryCategoryIds" + index %>'
		/>
	</div>
</div>

<aui:script sandbox="<%= true %>">
	var select = $('#<portlet:namespace /><%= randomNamespace %>selector');

	var row = select.closest('.query-row');

	select.on(
		'change',
		function(event) {
			var tagsSelector = row.find('.tags-selector');
			var categoriesSelector = row.find('.categories-selector');

			var assetTags = (select.val() == 'assetTags');
			
			tagsSelector.toggleClass('hide', !assetTags);
			categoriesSelector.toggleClass('hide', assetTags);

			if (!assetTags) {
				$("div[id$='_assetCategoriesSelector'] button").off("click.swt")
					.on("click.swt", onAssetCategoriesSelectorButtonClicked);
			}
		}
	);
	
	if (select.val() != 'assetTags') {
		waitForElementToDisplay("div[id$='_assetCategoriesSelector'] button", document, 1000, function() {
			$("div[id$='_assetCategoriesSelector'] button").off("click.swt")
				.on("click.swt", onAssetCategoriesSelectorButtonClicked);
		});
	}
	
	function onAssetCategoriesSelectorButtonClicked() {
		var selectedStructureIds = [];
		
		// The user may choose "any" (true), "many" (false) or a specific structure ID.
		var structureSelectionVal = $("#_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_anyClassTypeJournalArticleAssetRendererFactory").val();
		if (structureSelectionVal) { // True or any numeric value greater than 0.
			if ($.isNumeric(structureSelectionVal)) {
				selectedStructureIds.push(structureSelectionVal);
			}
		} else {
			// The multiple category IDs will be stored in a select element.
			var options = $("#_com_liferay_portlet_configuration_web_portlet_PortletConfigurationPortlet_JournalArticleAssetRendererFactorycurrentClassTypeIds option");
			for (var i = 0; i < options.length; i++) {
				selectedStructureIds = $(options[i]).val();
			}
		}
		
		if (selectedStructureIds.length === 0) {
			return;
		}
		
		var str = "div.lfr-categories-selector-list div.tree-view > ul > li > div.tree-node-content > span.tree-label";
		
		if ($(str, window.parent.document).length === 0) {
			waitForElementToDisplay(str, window.parent.document, 500, function() {
				updateCategoryDisplay(selectedStructureIds);
			});
		} else {
			setTimeout(function() {
				updateCategoryDisplay(selectedStructureIds);
			}, 1000);
		}
	}
	
	function updateCategoryDisplay(selectedStructureIds) {
		var vocabularyName = "";
		var isBelongToStructure = false;
		
		$("div.lfr-categories-selector-list div.tree-view > ul > li > div.tree-node-content > span.tree-label", window.parent.document).each(function() {
			vocabularyName = $(this).text();
			isBelongToStructure = false;
			
			for (var i = 0; i < selectedStructureIds.length; i++) {
				if (vocabularyName.startsWith(selectedStructureIds[i])) {
					// Remove the structure ID from vocabulary name.
					$(this).text(" " + vocabularyName.substring(selectedStructureIds[i].toString().length + 1));
					
					// Highlight the vocabulary name.
					$(this).css("font-weight", "bold");
					$(this).css("text-transform", "uppercase");
					
					isBelongToStructure = true;
					break;
				}
			}
			
			if (!isBelongToStructure) {
				// Hide the vocabularies not belongs to selected structures.
				$(this).closest("div.lfr-categories-selector-list div.tree-view").css("display", "none");
			}
			
			var categoriesList = $(this).closest("li[id^='vocabulary']").find("ul.tree-container");
			
			if (categoriesList.children().length === 0) {
				// Wait for content gets loaded.
				var _this = this;
				setTimeout(function() {
					if (categoriesList.children().length === 0) {
						// Hide the empty vocabularies.
						$(_this).closest("div.lfr-categories-selector-list div.tree-view").css("display", "none");
					} else {
						// Show all sub-categories.
						categoriesList.find("span.tree-hitarea.glyphicon-plus").each(function() {
							$(this).trigger("click");
						});
						
						// Update the style of OK icon.
						categoriesList.find("span.glyphicon-check").css("color", "#5cb85c");
					}
				}, 1000);
			} else {
				// Show all sub-categories.
				categoriesList.find("span.tree-hitarea.glyphicon-plus").each(function() {
					$(this).trigger("click");
				});
				
				// Update the style of OK icon.
				categoriesList.find("span.glyphicon-check").css("color", "#5cb85c");
			}
		});
	}
	
	/**
	 * Wait for the element to display.
	 *
	 * @param {string} selector - The element selector.
	 * @param {object} context - The selector context.
	 * @param {number} time - The interval (in miliseconds) of checking loop.
	 * @param {function} callback - The callback function invoked when the element gets displayed.
	 */
	function waitForElementToDisplay(selector, context, time, callback) {
        if ($(selector, context).length != 0) {
        	callback();
            return;
        } else {
            setTimeout(function() {
                waitForElementToDisplay(selector, context, time, callback);
            }, time);
        }
    }
</aui:script>