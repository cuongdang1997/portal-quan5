package com.swt.portal.structuredsearch.model;

import javax.portlet.PortletRequest;

/**
 * @author dung.nguyen@smartworld.com.vn
 */
public class StructuredSearchTerms extends StructuredSearchDisplayTerms {

	public StructuredSearchTerms(PortletRequest portletRequest) {
		super(portletRequest);
	}

}
