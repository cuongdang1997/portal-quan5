package com.swt.portal.structuredsearch.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.swt.portal.structuredsearch.model.StructuredSearchConstants;

/**
 * @author dung.nguyen@smartworld.com.vn
 */
@Component(
	configurationPid = "com.swt.portal.structuredsearch.portlet.StructuredSearchConfiguration",
	configurationPolicy = ConfigurationPolicy.OPTIONAL,
	immediate = true,
	property = {
		"javax.portlet.name=" + StructuredSearchConstants.PORTLET_NAME
	},
	service = ConfigurationAction.class
)
public class StructuredSearchConfigurationAction extends DefaultConfigurationAction {
	
	@Reference
	private LogService _log;

	private volatile StructuredSearchConfiguration _config;

	/**
	 * Activate the configuration instance.
	 * 
	 * @param properties
	 */
	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_config = ConfigurableUtil.createConfigurable(StructuredSearchConfiguration.class, properties);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.portlet.SettingsConfigurationAction#processAction(
	 * javax.portlet.PortletConfig, javax.portlet.ActionRequest,
	 * javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		_log.log(LogService.LOG_INFO, StructuredSearchConstants.PORTLET_NAME + " configuration action.");
		
		String structureKey = ParamUtil.getString(actionRequest, "structureKey");
		setPreference(actionRequest, "structureKey", String.valueOf(structureKey));
		
		long displayStyle = ParamUtil.getLong(actionRequest, "displayStyle");
		setPreference(actionRequest, "displayStyle", String.valueOf(displayStyle));
		
		boolean searchByKeywords = ParamUtil.getBoolean(actionRequest, "searchByKeywords");
		setPreference(actionRequest, "searchByKeywords", String.valueOf(searchByKeywords));
		
		boolean showOrderInResult = ParamUtil.getBoolean(actionRequest, "showOrderInResult");
		setPreference(actionRequest, "showOrderInResult", String.valueOf(showOrderInResult));
		
		boolean showTitleInResult = ParamUtil.getBoolean(actionRequest, "showTitleInResult");
		setPreference(actionRequest, "showTitleInResult", String.valueOf(showTitleInResult));
		
		boolean showSummaryInResult = ParamUtil.getBoolean(actionRequest, "showSummaryInResult");
		setPreference(actionRequest, "showSummaryInResult", String.valueOf(showSummaryInResult));
		
		boolean showDownloadInResult = ParamUtil.getBoolean(actionRequest, "showDownloadInResult");
		setPreference(actionRequest, "showDownloadInResult", String.valueOf(showDownloadInResult));
		
		boolean advancedSearchByDefault = ParamUtil.getBoolean(actionRequest, "advancedSearchByDefault");
		setPreference(actionRequest, "advancedSearchByDefault", String.valueOf(advancedSearchByDefault));
		
		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.portlet.BaseJSPSettingsConfigurationAction#include(
	 * javax.portlet.PortletConfig, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		_log.log(LogService.LOG_INFO, StructuredSearchConstants.PORTLET_NAME + " configuration include.");
		
		httpServletRequest.setAttribute(StructuredSearchConfiguration.class.getName(), _config);
		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

}
