package com.swt.portal.structuredsearch.model;

/**
 * The constant values to be used in this portlet.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public class StructuredSearchConstants {

	/**
	 * The unique name of this portlet.
	 */
	public static final String PORTLET_NAME = "SWTStructuredSearch";

	/**
	 * The display name of this portlet.
	 */
	public static final String PORTLET_DISPLAY_NAME = "HCMGov Structured Search";

	/**
	 * The "EQUAL" search type. To be used to configure the search type of some
	 * structure field types.
	 */
	public static final int SEARCH_TYPE_EQUAL = 1;

	/**
	 * The "RANGE" search type (from.. to..). To be used to configure the search
	 * type of some structure field types.
	 */
	public static final int SEARCH_TYPE_RANGE = 2;

	/**
	 * The "TABLE" display style. It's the common display style of Document modules.
	 */
	public static final long TABLE_STYLE_1 = 1;
	public static final long TABLE_STYLE_2 = 2;
	public static final long TABLE_STYLE_3 = 3;
	public static final long TABLE_STYLE_4 = 4;

}