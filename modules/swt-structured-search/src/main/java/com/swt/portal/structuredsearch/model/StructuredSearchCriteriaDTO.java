package com.swt.portal.structuredsearch.model;

/**
 * The search criteria to be used in search form.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public class StructuredSearchCriteriaDTO implements Comparable<StructuredSearchCriteriaDTO> {
	
	// The field data type.
	private String type;
	// The field unique name.
	private String name;
	// The field label.
	private String label;
	// The search type.
	private int searchType;
	// The sequence of field in search form.
	private int sequence;

	public StructuredSearchCriteriaDTO(String type, int searchType, String name, String label, int sequence) {
		super();
		this.type = type;
		this.searchType = searchType;
		this.name = name;
		this.label = label;
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	public int compareTo(StructuredSearchCriteriaDTO o) {
		int tmp = sequence - o.getSequence();
		if (tmp == 0) {
			tmp = o.getLabel().compareTo(getLabel());
		}
		return tmp;
	}

}
