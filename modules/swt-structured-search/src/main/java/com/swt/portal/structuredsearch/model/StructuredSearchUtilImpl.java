package com.swt.portal.structuredsearch.model;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.kernel.DDMFormField;
import com.liferay.dynamic.data.mapping.kernel.DDMStructure;
import com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil;
import com.liferay.dynamic.data.mapping.util.DDMIndexer;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.BaseModelSearchResult;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

/**
 * @see StructuredSearchUtil
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
@SuppressWarnings("deprecation")
public class StructuredSearchUtilImpl implements StructuredSearchUtil {

	@Reference
	private LogService _log;

	// The date format used by Liferay indexing engine.
	private static DateFormat indexedDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.swt.portal.structuredsearch.model.StructuredSearchUtil#search(com.swt.
	 * portal.structuredsearch.model.StructuredSearchContainer,
	 * com.liferay.portal.kernel.theme.ThemeDisplay, java.lang.String, long,
	 * javax.portlet.PortletRequest)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<JournalArticle> search(StructuredSearchContainer searchContainer, ThemeDisplay themeDisplay,
			String structureKey, long categoryId, PortletRequest portletRequest) {
		long companyId = themeDisplay.getCompanyId();
		long groupId = themeDisplay.getScopeGroupId();
		PortletPreferences preferences = portletRequest.getPreferences();
		boolean isAdvancedSearch = GetterUtil.getBoolean(portletRequest.getParameter("isAdvancedSearch"), true);

		// Prefer the category ID attached in URL parameter (if any).
		long urlCategoryId = GetterUtil.getLong(portletRequest.getParameter("categoryId"));
		if (urlCategoryId != 0) {
			categoryId = urlCategoryId;
			isAdvancedSearch = true;
		}

		StructuredSearchDisplayTerms searchTerms = (StructuredSearchDisplayTerms) searchContainer.getSearchTerms();
		String keywords = searchTerms.getKeywords();
		Hits hits = null;

		if (isAdvancedSearch) {
			SearchContext sctx = new SearchContext();
			sctx.setCompanyId(companyId);
			sctx.setGroupIds(new long[] { groupId });

			List<BooleanQuery> boolQueries = new ArrayList<>();

			// Get the selected DDM structure.
			DDMStructure structure = null;
			try {
				structure = DDMStructureManagerUtil.getStructure(groupId,
						ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class), structureKey);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			} catch (SystemException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			// Get the DDM structure's fields.
			List<DDMFormField> fields = new ArrayList<>();
			if (structure != null) {
				fields = structure.getDDMFormFields(false);
			}

			String fieldSearchVal = null, fieldDataType = null, fieldEncodedName = null;
			int fieldSearchType = -1;
			BooleanQuery fieldBoolQuery = null;

			// Iterate through each field in the selected structure
			// and check if it's selected for search.
			for (DDMFormField f : fields) {
				fieldDataType = f.getType();

				switch (fieldDataType) {
				case "text":
				case "textarea":
				case "ddm-text-html":
					// Get the search parameter from request.
					fieldSearchVal = portletRequest.getParameter(f.getName());
					if (fieldSearchVal == null) {
						fieldSearchVal = StringPool.BLANK;
					}
					fieldSearchVal = fieldSearchVal.trim();
					if (fieldSearchVal.length() == 0) {
						continue;
					}

					fieldEncodedName = encodeName(structure.getStructureId(), f.getName(), themeDisplay.getLocale(),
							f.getIndexType());
					fieldBoolQuery = BooleanQueryFactoryUtil.create(sctx);
					try {
						fieldBoolQuery.addTerm(fieldEncodedName, fieldSearchVal, true);
					} catch (ParseException e) {
						_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					}
					boolQueries.add(fieldBoolQuery);

					break;

				case "ddm-date":
					fieldEncodedName = encodeName(structure.getStructureId(), f.getName(), themeDisplay.getLocale(),
							f.getIndexType());
					fieldBoolQuery = BooleanQueryFactoryUtil.create(sctx);

					fieldSearchType = GetterUtil
							.getInteger(preferences.getValue("structure" + structureKey + f.getName() + "type", "0"));

					switch (fieldSearchType) {
					case StructuredSearchConstants.SEARCH_TYPE_EQUAL:
						// Get the date parameter from request.
						fieldSearchVal = portletRequest.getParameter(f.getName());
						if (fieldSearchVal == null) {
							fieldSearchVal = StringPool.BLANK;
						}
						fieldSearchVal = fieldSearchVal.trim();
						if (fieldSearchVal.length() == 0) {
							break;
						}

						Date searchDateVal = null;
						try {
							if (themeDisplay.getLanguageId().equals("vi_VN")) {
								searchDateVal = new SimpleDateFormat("dd/MM/yyyy").parse(fieldSearchVal);
							} else {
								searchDateVal = new SimpleDateFormat("MM/dd/yyyy").parse(fieldSearchVal);
							}
						} catch (java.text.ParseException e) {
							_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
							break;
						}

						fieldBoolQuery.addExactTerm(fieldEncodedName, indexedDateFormat.format(searchDateVal));
						boolQueries.add(fieldBoolQuery);

						break;

					case StructuredSearchConstants.SEARCH_TYPE_RANGE:
						Date fromDateVal = null, toDateVal = null;

						// Get the "from date" parameter from request.
						fieldSearchVal = portletRequest.getParameter(f.getName() + "_from");
						if (fieldSearchVal == null) {
							fieldSearchVal = StringPool.BLANK;
						}
						fieldSearchVal = fieldSearchVal.trim();
						if (fieldSearchVal.length() != 0) {
							try {
								if (themeDisplay.getLanguageId().equals("vi_VN")) {
									fromDateVal = new SimpleDateFormat("dd/MM/yyyy").parse(fieldSearchVal);
								} else {
									fromDateVal = new SimpleDateFormat("MM/dd/yyyy").parse(fieldSearchVal);
								}
							} catch (java.text.ParseException e) {
								_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
								break;
							}
						}

						// Get the "to date" parameter from request.
						fieldSearchVal = portletRequest.getParameter(f.getName() + "_to");
						if (fieldSearchVal == null) {
							fieldSearchVal = StringPool.BLANK;
						}
						fieldSearchVal = fieldSearchVal.trim();
						if (fieldSearchVal.length() != 0) {
							try {
								if (themeDisplay.getLanguageId().equals("vi_VN")) {
									toDateVal = new SimpleDateFormat("dd/MM/yyyy").parse(fieldSearchVal);
								} else {
									toDateVal = new SimpleDateFormat("MM/dd/yyyy").parse(fieldSearchVal);
								}
							} catch (java.text.ParseException e) {
								_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
								break;
							}
						}

						// If both "from date" and "to date" are null -> break.
						// Otherwise, set default value for the null one.
						if (fromDateVal == null && toDateVal == null) {
							break;
						} else {
							if (fromDateVal == null) {
								fromDateVal = new Date(1970, 1, 1);
							}
							if (toDateVal == null) {
								toDateVal = new Date();
							}
						}

						// Add the date range search term.
						fieldBoolQuery.addRangeTerm(fieldEncodedName, indexedDateFormat.format(fromDateVal),
								indexedDateFormat.format(toDateVal));
						boolQueries.add(fieldBoolQuery);

						break;
					}
					break;

				default:
					break;
				}
			}

			// Add the search clause for structure fields.
			BooleanQuery fieldsBoolQuery = BooleanQueryFactoryUtil.create(sctx);
			for (BooleanQuery bq : boolQueries) {
				try {
					fieldsBoolQuery.add(bq, BooleanClauseOccur.MUST);
				} catch (ParseException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				}
			}
			//NHUT.DANG START SEARCH SCORES 
			String fullName = portletRequest.getParameter("fullName");
			if(Validator.isNotNull(fullName)) {
				fieldsBoolQuery.addRequiredTerm(Field.DESCRIPTION,fullName,true);
			}
			//NHUT.DANG END SEARCH SCORES 
		
			BooleanClause fieldsBoolClause = BooleanClauseFactoryUtil.create(sctx, fieldsBoolQuery,
					BooleanClauseOccur.MUST.getName());

			// Add the search clause for selected structure.
			BooleanClause ddmStructureKeyClause = BooleanClauseFactoryUtil.create(sctx, "ddmStructureKey", structureKey,
					BooleanClauseOccur.MUST.getName());

			sctx.setBooleanClauses(new BooleanClause[] { fieldsBoolClause, ddmStructureKeyClause });

			// Get the list of vocabularies which name starts with the structure ID.
			String vocaPrefix = structure.getStructureId() + "_";
			List<AssetVocabulary> vocabularies = null;
			try {
				BaseModelSearchResult<AssetVocabulary> av = AssetVocabularyLocalServiceUtil
						.searchVocabularies(companyId, groupId, vocaPrefix, 0, Byte.MAX_VALUE);
				vocabularies = av.getBaseModels();
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			// Iterate through each vocabulary and check if it's selected for search.
			List<Long> categoryIdsList = new ArrayList<>();
			if (vocabularies != null) {
				for (AssetVocabulary v : vocabularies) {
					fieldSearchVal = portletRequest.getParameter("vocabulary_" + v.getVocabularyId());
					if (fieldSearchVal == null) {
						fieldSearchVal = StringPool.BLANK;
					}
					fieldSearchVal = fieldSearchVal.trim();

					if (fieldSearchVal.length() != 0 && !fieldSearchVal.equals("0")) {
						long selectedCategoryId = Long.valueOf(fieldSearchVal);
						categoryIdsList.add(selectedCategoryId);
					}
				}
			}

			// Add filter for categories.
			if (categoryId != 0) {
				long[] cateIds = new long[1];
				cateIds[0] = categoryId;
				sctx.setAssetCategoryIds(cateIds);
			} else {
				long[] categoryIds = new long[categoryIdsList.size()];
				for (int i = 0; i < categoryIds.length; i++) {
					categoryIds[i] = categoryIdsList.get(i);
				}
				if (categoryIds.length != 0) {
					sctx.setAssetCategoryIds(categoryIds);
				}
			}

			// Add filter by keywords.
			if (keywords == null) {
				keywords = StringPool.BLANK;
			}
			keywords = keywords.replaceAll(" ", "+");
			sctx.setKeywords(keywords);
			// Other settings for search.
			sctx.setStart(searchContainer.getStart());
			sctx.setEnd(searchContainer.getEnd());
			sctx.setSorts(new Sort("displayDate", Sort.LONG_TYPE, true));
			sctx.setAttribute("paginationType", "none");
			Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);

			try {
				hits = indexer.search(sctx);
			} catch (SearchException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}
		} else {
			hits = JournalArticleLocalServiceUtil.search(companyId, groupId, Collections.EMPTY_LIST,
				JournalArticleConstants.CLASSNAME_ID_DEFAULT, structureKey, null, searchTerms.getKeywords(), null,
				searchContainer.getStart(), searchContainer.getEnd(), new Sort("displayDate", Sort.LONG_TYPE, true) );
		}

		List<JournalArticle> journalArticles = new ArrayList<JournalArticle>();

		if (hits != null) {
			List<Document> documents = ListUtil.toList(hits.getDocs());
			for (Document d : documents) {
				JournalArticle journalArticle = null;
				try {
					journalArticle = JournalArticleLocalServiceUtil.getArticle(groupId,
							GetterUtil.getString(d.get(Field.ARTICLE_ID)));

				} catch (PortalException e) {
					continue;
				}

				journalArticles.add(journalArticle);
			}
		}

		return journalArticles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.swt.portal.structuredsearch.model.StructuredSearchUtil#searchCount(com.
	 * swt.portal.structuredsearch.model.StructuredSearchContainer,
	 * com.liferay.portal.kernel.theme.ThemeDisplay, java.lang.String, long,
	 * javax.portlet.PortletRequest)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int searchCount(StructuredSearchContainer searchContainer, ThemeDisplay themeDisplay, String structureKey,
			long categoryId, PortletRequest portletRequest) {
		long companyId = themeDisplay.getCompanyId();
		long groupId = themeDisplay.getScopeGroupId();

		PortletPreferences preferences = portletRequest.getPreferences();
		boolean isAdvancedSearch = GetterUtil.getBoolean(portletRequest.getParameter("isAdvancedSearch"), true);
		
		// Prefer the category ID attached in URL parameter (if any).
		long urlCategoryId = GetterUtil.getLong(portletRequest.getParameter("categoryId"));
		if (urlCategoryId != 0) {
			categoryId = urlCategoryId;
			isAdvancedSearch = true;
		}

		StructuredSearchDisplayTerms searchTerms = (StructuredSearchDisplayTerms) searchContainer.getSearchTerms();
		String keywords = searchTerms.getKeywords();
		Hits hits = null;

		if (isAdvancedSearch) {

			SearchContext sctx = new SearchContext();
			sctx.setCompanyId(companyId);
			sctx.setGroupIds(new long[] { groupId });

			List<BooleanQuery> boolQueries = new ArrayList<>();

			// Get the selected DDM structure.
			DDMStructure structure = null;
			try {
				structure = DDMStructureManagerUtil.getStructure(groupId,
						ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class), structureKey);
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			} catch (SystemException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			// Get the DDM structure's fields.
			List<DDMFormField> fields = new ArrayList<>();
			if (structure != null) {
				fields = structure.getDDMFormFields(false);
			}

			String fieldSearchVal = null, fieldDataType = null, fieldEncodedName = null;
			int fieldSearchType = -1;
			BooleanQuery fieldBoolQuery = null;

			// Iterate through each field in the selected structure
			// and check if it's selected for search.
			for (DDMFormField f : fields) {
				fieldDataType = f.getType();

				switch (fieldDataType) {
				case "text":
				case "textarea":
				case "ddm-text-html":
					// Get the search parameter from request.
					fieldSearchVal = portletRequest.getParameter(f.getName());
					if (fieldSearchVal == null) {
						fieldSearchVal = StringPool.BLANK;
					}
					fieldSearchVal = fieldSearchVal.trim();
					if (fieldSearchVal.length() == 0) {
						continue;
					}

					fieldEncodedName = encodeName(structure.getStructureId(), f.getName(), themeDisplay.getLocale(),
							f.getIndexType());
					fieldBoolQuery = BooleanQueryFactoryUtil.create(sctx);
					try {
						fieldBoolQuery.addTerm(fieldEncodedName, fieldSearchVal, true);
					} catch (ParseException e) {
						_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
					}
					boolQueries.add(fieldBoolQuery);

					break;
					
				case "ddm-date":
					fieldEncodedName = encodeName(structure.getStructureId(), f.getName(), themeDisplay.getLocale(),
							f.getIndexType());
					fieldBoolQuery = BooleanQueryFactoryUtil.create(sctx);

					fieldSearchType = GetterUtil
							.getInteger(preferences.getValue("structure" + structureKey + f.getName() + "type", "0"));

					switch (fieldSearchType) {
					case StructuredSearchConstants.SEARCH_TYPE_EQUAL:
						// Get the date parameter from request.
						fieldSearchVal = portletRequest.getParameter(f.getName());
						if (fieldSearchVal == null) {
							fieldSearchVal = StringPool.BLANK;
						}
						fieldSearchVal = fieldSearchVal.trim();
						if (fieldSearchVal.length() == 0) {
							break;
						}

						Date searchDateVal = null;
						try {
							if (themeDisplay.getLanguageId().equals("vi_VN")) {
								searchDateVal = new SimpleDateFormat("dd/MM/yyyy").parse(fieldSearchVal);
							} else {
								searchDateVal = new SimpleDateFormat("MM/dd/yyyy").parse(fieldSearchVal);
							}
						} catch (java.text.ParseException e) {
							_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
							break;
						}

						fieldBoolQuery.addExactTerm(fieldEncodedName, indexedDateFormat.format(searchDateVal));
						boolQueries.add(fieldBoolQuery);

						break;
						
					case StructuredSearchConstants.SEARCH_TYPE_RANGE:
						Date fromDateVal = null, toDateVal = null;

						// Get the "from date" parameter from request.
						fieldSearchVal = portletRequest.getParameter(f.getName() + "_from");
						if (fieldSearchVal == null) {
							fieldSearchVal = StringPool.BLANK;
						}
						fieldSearchVal = fieldSearchVal.trim();
						if (fieldSearchVal.length() != 0) {
							try {
								if (themeDisplay.getLanguageId().equals("vi_VN")) {
									fromDateVal = new SimpleDateFormat("dd/MM/yyyy").parse(fieldSearchVal);
								} else {
									fromDateVal = new SimpleDateFormat("MM/dd/yyyy").parse(fieldSearchVal);
								}
							} catch (java.text.ParseException e) {
								_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
								break;
							}
						}

						// Get the "to date" parameter from request.
						fieldSearchVal = portletRequest.getParameter(f.getName() + "_to");
						if (fieldSearchVal == null) {
							fieldSearchVal = StringPool.BLANK;
						}
						fieldSearchVal = fieldSearchVal.trim();
						if (fieldSearchVal.length() != 0) {
							try {
								if (themeDisplay.getLanguageId().equals("vi_VN")) {
									toDateVal = new SimpleDateFormat("dd/MM/yyyy").parse(fieldSearchVal);
								} else {
									toDateVal = new SimpleDateFormat("MM/dd/yyyy").parse(fieldSearchVal);
								}
							} catch (java.text.ParseException e) {
								_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
								break;
							}
						}

						// If both "from date" and "to date" are null -> break.
						// Otherwise, set default value for the null one.
						if (fromDateVal == null && toDateVal == null) {
							break;
						} else {
							if (fromDateVal == null) {
								fromDateVal = new Date(1970, 1, 1);
							}
							if (toDateVal == null) {
								toDateVal = new Date();
							}
						}

						// Add the date range search term.
						fieldBoolQuery.addRangeTerm(fieldEncodedName, indexedDateFormat.format(fromDateVal),
								indexedDateFormat.format(toDateVal));
						boolQueries.add(fieldBoolQuery);

						break;
					}
					break;
					
				default:
					break;
				}
			}

			// Add the search clause for structure fields.
			BooleanQuery fieldsBoolQuery = BooleanQueryFactoryUtil.create(sctx);
			for (BooleanQuery bq : boolQueries) {
				try {
					fieldsBoolQuery.add(bq, BooleanClauseOccur.MUST);
				} catch (ParseException e) {
					_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
				}
			}
			BooleanClause fieldsBoolClause = BooleanClauseFactoryUtil.create(sctx, fieldsBoolQuery,
					BooleanClauseOccur.MUST.getName());

			// Add the search clause for selected structure.
			BooleanClause ddmStructureKeyClause = BooleanClauseFactoryUtil.create(sctx, "ddmStructureKey", structureKey,
					BooleanClauseOccur.MUST.getName());

			sctx.setBooleanClauses(new BooleanClause[] { fieldsBoolClause, ddmStructureKeyClause });

			// Get the list of vocabularies which name starts with the structure ID.
			String vocaPrefix = structure.getStructureId() + "_";
			List<AssetVocabulary> vocabularies = null;
			try {
				BaseModelSearchResult<AssetVocabulary> av = AssetVocabularyLocalServiceUtil
						.searchVocabularies(companyId, groupId, vocaPrefix, 0, Byte.MAX_VALUE);
				vocabularies = av.getBaseModels();
			} catch (PortalException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

			// Iterate through each vocabulary and check if it's selected for search.
			List<Long> categoryIdsList = new ArrayList<>();
			if (vocabularies != null) {
				for (AssetVocabulary v : vocabularies) {
					fieldSearchVal = portletRequest.getParameter("vocabulary_" + v.getVocabularyId());
					if (fieldSearchVal == null) {
						fieldSearchVal = StringPool.BLANK;
					}
					fieldSearchVal = fieldSearchVal.trim();

					if (fieldSearchVal.length() != 0 && !fieldSearchVal.equals("0")) {
						long selectedCategoryId = Long.valueOf(fieldSearchVal);
						categoryIdsList.add(selectedCategoryId);
					}
				}
			}

			// Add filter for categories.
			if (categoryId != 0) {
				long[] cateIds = new long[1];
				cateIds[0] = categoryId;
				sctx.setAssetCategoryIds(cateIds);
			} else {
				long[] categoryIds = new long[categoryIdsList.size()];
				for (int i = 0; i < categoryIds.length; i++) {
					categoryIds[i] = categoryIdsList.get(i);
				}
				if (categoryIds.length != 0) {
					sctx.setAssetCategoryIds(categoryIds);
				}
			}

			// Add filter by keywords.
			if (keywords == null) {
				keywords = StringPool.BLANK;
			}
			keywords = keywords.replaceAll(" ", "+");
			sctx.setKeywords(keywords);

			// Other settings for search.
			sctx.setAttribute("paginationType", "none");

			Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);

			try {
				hits = indexer.search(sctx);
			} catch (SearchException e) {
				_log.log(LogService.LOG_ERROR, e.toString(), e.getCause());
			}

		} else {
			hits = JournalArticleLocalServiceUtil.search(companyId, groupId, Collections.emptyList(),
					JournalArticleConstants.CLASSNAME_ID_DEFAULT, structureKey, null, searchTerms.getKeywords(), null,
					0, Byte.MAX_VALUE, null);
		}

		List<JournalArticle> journalArticles = new ArrayList<JournalArticle>();

		if (hits != null) {
			List<Document> documents = ListUtil.toList(hits.getDocs());
			for (Document d : documents) {
				JournalArticle journalArticle = null;
				try {

					journalArticle = JournalArticleLocalServiceUtil.getArticle(groupId,
							GetterUtil.getString(d.get(Field.ARTICLE_ID)));

				} catch (PortalException e) {
					continue;
				}

				journalArticles.add(journalArticle);
			}
		}

		return journalArticles.size();
	}

	/**
	 * Encode the name before using with Liferay indexing engine.
	 * 
	 * @param ddmStructureId The DDMStructure ID.
	 * @param fieldName      The DDMStucture field name.
	 * @param locale         The DDMStructure locale.
	 * @param indexType      The index type (none|text|key).
	 * 
	 * @return The encoded field name.
	 */
	private String encodeName(long ddmStructureId, String fieldName, Locale locale, String indexType) {
		StringBundler sb = new StringBundler(8);

		sb.append(DDMIndexer.DDM_FIELD_PREFIX);

		if (Validator.isNotNull(indexType)) {
			sb.append(indexType);
			sb.append(DDMIndexer.DDM_FIELD_SEPARATOR);
		}

		sb.append(ddmStructureId);
		sb.append(DDMIndexer.DDM_FIELD_SEPARATOR);
		sb.append(fieldName);

		if (locale != null) {
			sb.append(StringPool.UNDERLINE);
			sb.append(LocaleUtil.toLanguageId(locale));
		}

		return sb.toString();
	}
	/**
	 *  draft is being consulted
	 * @param listArticle result of search container.
	 * @return A list result draft is being consulted .
	 */
	@Override
	public List<JournalArticle> searchDraftIsBeingConsulted(List<JournalArticle> listArticle, ThemeDisplay themeDisplay) throws PortalException, Exception {
		List<JournalArticle> listResult = new ArrayList<JournalArticle>();
		DateFormat	fromDateVal = new SimpleDateFormat("dd/MM/yyyy");
		String newDate = fromDateVal.format(new Date()) ;
		Date dateCunrrent=	fromDateVal.parse(newDate);
		for(JournalArticle article : listArticle) {
		    try {
		    	String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
		    	com.liferay.portal.kernel.xml.Document articleDoc = null;
		    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
		    	Element root = articleDoc.getRootElement();
		    	Date startDate = null;
		    	Date endDate = null;
		    	for (Element em : root.elements()) {
		    		// element constain name startDate 
					if (em.attributeValue("name").equals("startDate")) {
						Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
						String startDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
						startDate =	fromDateVal.parse(startDatePare);
					}
					// element constain name endDate
					if (em.attributeValue("name").equals("endDate")) {
						Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
						String endDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
						endDate =	fromDateVal.parse(endDatePare);
						
					}
				}
		    	if(startDate!=null && endDate!=null) {
		    		// startDate <= current date
		    		startDate.setDate(startDate.getDate()-1);
		    		// endDate >= current date
		    		endDate.setDate(endDate.getDate()+1);
		    		if(endDate.after(dateCunrrent) && startDate.before(dateCunrrent)) {
		    			listResult.add(article);
		    		}
		    	}
		    
		    } catch (DocumentException e) {
		    } 
		}
		return listResult;
	
	}
	/**
	 *  draft is expired
	 * @param listArticle result of search container.
	 * @return A list result draft is expired .
	 */
	@Override
	public List<JournalArticle> searchDraftIsExpired(List<JournalArticle> listArticle, ThemeDisplay themeDisplay) throws Exception {

		List<JournalArticle> listResult = new ArrayList<JournalArticle>();
		DateFormat	fromDateVal = new SimpleDateFormat("dd/MM/yyyy");
		String newDate = fromDateVal.format(new Date()) ;
		Date dateCunrrent=	fromDateVal.parse(newDate);
		for(JournalArticle article : listArticle) {
		    try {
		    	String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
		    	com.liferay.portal.kernel.xml.Document articleDoc = null;
		    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
		    	Element root = articleDoc.getRootElement();
		    	Date startDate = null;
		    	Date endDate = null;
		    	for (Element em : root.elements()) {
		    		// element constain name startDate 
					if (em.attributeValue("name").equals("startDate")) {
						Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
						String startDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
						startDate =	fromDateVal.parse(startDatePare);
					}
					// element constain name endDate 
					if (em.attributeValue("name").equals("endDate")) {
						Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
						String endDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
						endDate =	fromDateVal.parse(endDatePare);
						
					}
				}
		    	if(startDate!=null && endDate!=null) {
		    		if((endDate.before(dateCunrrent) )&& startDate.before(dateCunrrent)) {
		    			listResult.add(article);
		    		}
		    	}
		    
		    } catch (DocumentException e) {
		    } 
		}
		return listResult;
	}

}
