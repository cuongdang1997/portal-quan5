package com.swt.portal.structuredsearch.model;

/**
 * The column to be displayed in search result.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public class StructuredSearchColumnDTO implements Comparable<StructuredSearchColumnDTO> {

	// The field data type.
	private String type;
	// The field unique name.
	private String name;
	// The field label.
	private String label;
	// The sequence of field column in search result.
	private int sequence;

	public StructuredSearchColumnDTO(String type, String name, String label, int sequence) {
		super();
		this.type = type;
		this.name = name;
		this.label = label;
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	public int compareTo(StructuredSearchColumnDTO o) {
		int tmp = sequence - o.getSequence();
		if (tmp == 0) {
			tmp = o.getLabel().compareTo(getLabel());
		}
		return tmp;
	}

}
