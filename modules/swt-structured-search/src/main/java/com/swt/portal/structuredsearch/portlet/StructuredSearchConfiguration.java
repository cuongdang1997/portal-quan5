package com.swt.portal.structuredsearch.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.liferay.portal.kernel.util.StringPool;

import aQute.bnd.annotation.metatype.Meta;

/**
 * @author dung.nguyen@smartworld.com.vn
 */
@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(
	factory = true,
	id = "com.swt.portal.structuredsearch.portlet.StructuredSearchConfiguration",
	localization = "content/Language"
)
public interface StructuredSearchConfiguration {

	/**
	 * @return The key of structure which associates to this portlet.
	 */
	@Meta.AD(deflt = StringPool.BLANK, required = false)
	public String structureKey();
	
	/**
	 * @return The display style of search result.
	 */
	@Meta.AD(deflt = "1", required = false)
	public long displayStyle();

	/**
	 * @return If true, the portlet should support search by keywords.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean searchByKeywords();
	
	/**
	 * @return If true, the order number should be shown in search result.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean showOrderInResult();
	
	/**
	 * @return If true, the article title should be shown in search result.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean showTitleInResult();
	
	/**
	 * @return If true, the article summary should be shown in search result.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean showSummaryInResult();
	
	/**
	 * @return If true, the download action should be shown in search result.
	 */
	@Meta.AD(deflt = "true", required = false)
	public boolean showDownloadInResult();
	
	/**
	 * @return If true, display the advanced search form by default.
	 */
	@Meta.AD(deflt = "false", required = false)
	public boolean advancedSearchByDefault();

}
