package com.swt.portal.structuredsearch.model;

import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;

/**
 * The utility methods for search function.
 * 
 * @author dung.nguyen@smartworld.com.vn
 */
public interface StructuredSearchUtil {

	/**
	 * Search the content by structure key, category ID and other parameters
	 * retrieved from search form.
	 * 
	 * @param searchContainer The search container.
	 * @param themeDisplay    The theme display.
	 * @param structurekey    The stucture key.
	 * @param categoryId      The category ID.
	 * @param portletRequest  The portlet request.
	 * 
	 * @return A list or search result.
	 */
	List<JournalArticle> search(StructuredSearchContainer searchContainer, ThemeDisplay themeDisplay,
			String structurekey, long categoryId, PortletRequest portletRequest);
	/**
	 *  draft is being consulted
	 * @param listArticle result of search container.
	 * @return A list result draft is being consulted .
	 */
	List<JournalArticle> searchDraftIsBeingConsulted(List<JournalArticle> listArticle, ThemeDisplay themeDisplay) throws PortalException, Exception;
	
	/**
	 *  draft is expired
	 * @param listArticle result of search container.
	 * @return A list result draft is expired .
	 */
	List<JournalArticle> searchDraftIsExpired(List<JournalArticle> listArticle, ThemeDisplay themeDisplay) throws Exception;

	/**
	 * Count the number of records match search criterias.
	 * 
	 * @param searchContainer The search container.
	 * @param themeDisplay    The theme display.
	 * @param structurekey    The stucture key.
	 * @param categoryId      The category ID.
	 * @param portletRequest  The portlet request.
	 * 
	 * @return The number of records that match.
	 */
	int searchCount(StructuredSearchContainer searchContainer, ThemeDisplay themeDisplay, String structurekey,
			long categoryId, PortletRequest portletRequest);

}
