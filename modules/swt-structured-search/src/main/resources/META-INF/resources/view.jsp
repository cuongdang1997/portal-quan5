<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.swt.portal.structuredsearch.model.StructuredSearchDisplayTerms"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.asset.kernel.model.AssetRenderer"%>
<%@page import="com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.journal.web.asset.JournalArticleAssetRenderer"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>

<%@page import="com.swt.portal.structuredsearch.model.StructuredSearchColumnDTO"%>
<%@page import="com.swt.portal.structuredsearch.model.StructuredSearchContainer"%>
<%@page import="com.swt.portal.structuredsearch.model.StructuredSearchCriteriaDTO"%>
<%@page import="com.swt.portal.structuredsearch.model.StructuredSearchUtil"%>
<%@page import="com.swt.portal.structuredsearch.model.StructuredSearchUtilImpl"%>

<%@page import="java.io.StringReader"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>

<%@include file="init.jsp"%>

<style scoped>
	.search-form {
		width: 100%;
	}
	
	.advanced-search {
		display: none;
	}
	
	.input-group {
		margin-bottom: 15px;
	}
	
	.search-form .btn-search {
		text-shadow: none;
	    width: 87px;
	    background: #085791;
	    color: #ffffff;
	    cursor: pointer;
	    border: none;
	    text-transform: uppercase;
	    border-radius: 0!important;
	    overflow: visible;
	    margin-left: 0;
	    font-weight: normal;
	    font-family: arial;
	    height: 26px;
	    margin-left: 10px;
	    padding: 4px;
	}
	
	.search-form .col-label {
		padding-right: 0;
		line-height: 34px;
	}
	
	.search-form .btn-switch-search-mode {
		float: left;
	    padding-top: 6px;
	    cursor: pointer;
	    color: #085791;
	    font-size: 14px;
	    font-weight: bold;
	    margin-left: -10px;
	}
	.search-form .search-datepicker input::placeholder {
		visibility: hidden;
	}
	
	/* I don't want to show the success message on submit. */
	.lfr-alert-container {
		visibility: hidden;
	}
	
	.lfr-search-container {
		margin-top: 0;
	}
	
	.row-total-records {
		width: 100%;
		padding: 10px 0px 10px 10px;
		color: #c31d1d;
		font-size: 14px;
		font-weight: bold;
	}
	
	.table-search-result th {
		background: #085791;
		color: white;
		font-size: 14px;
		font-weight: normal;
		text-align: center;
		text-transform: uppercase;
		vertical-align: middle !important;
		border: none !important;
	}
	
	.table-search-result td a {
		color: inherit;
		text-decoration: none;
	}
	
	.table-search-result tbody tr:hover > .table-cell{
		color: #d70000;
	}
	
	.table-search-resul tbody tr:hover > .table-cell a{
        color: #d70000;
    }
    
    .table-search-result .table-striped > tbody > tr:nth-of-type(odd) {
    	background-color: #ecf7ff;
	}
	
    .table-search-result .table-bordered > tbody > tr > td {
    	border:none;
	}
	
    .table-search-result .table-bordered > tbody > tr{
    	border-bottom: 1px solid #ddd;
	}
	.table-search-result .table-bordered{
		border: none;
	}
	
	.col-download-attachments {
		min-width: 100px;
		text-align: center;
		vertical-align: middle !important;
	}
	
	.col-order-number{
		min-width: 45px;
	}
	
	.padding-top-15px{
        padding-top: 15px;
	}
	
	.padding-top-35px{
        padding-top: 35px;
	}
	.table-result-list-draft{
	    width: 50%;
	    height: 35px;
   		background: #085791;
	    float: left;
	    border: 1px solid #d4d2d2;
	    color: white;
	    text-align: justify;
	    margin-top: 20px;
	    padding-top: 5px;
	    border-radius: 0px 30% 0px 0px;
	}
	.table-result-list-draft p{
		text-transform: uppercase;
	    font-size: 13px;
	    margin-top: 4px;
	    font-weight: bold;
	}
	.list-result-draft{
		width: 100%;
		float: left;
	}
	.alert-info{
		float: left;
		width: 100%;
	}
	
	.swt-displayStyle3-title{
	 	font-weight: 600;
	    background-color: #deefff;
	    color: #0b5090;
	    padding: 5px;
	    border-bottom: 1px #a6d4ff solid;
	    border-top: 1px #a6d4ff solid;
	    cursor: pointer;
	 }
 
	.swt-displayStyle3-title a >span , .swt-displayStyle3-text{
		line-height: 18px;
		 		font-size: 14px;
	  		font-family: tahoma;
	}
	
	.swt-displayStyle3-flag{
		width: 90px;
		height: auto;
	}
	
	.swt-displayStyle3-pad7px{
		padding-top: 7px;
		padding-bottom: 7px;
	}
	
	.swt-displayStyle3-main .table-striped > tbody > tr:nth-of-type(odd){
		background-color: unset;
	}
	
	.swt-displayStyle3-main .table-bordered > tbody > tr {
   		 border-bottom: unset;
	}
	
	.swt-displayStyle3-main .table-bordered {
    	border-top: unset;
	}
	
	.swt-displayStyle3-main .table-bordered {
    	border: unset;
	}
	
	.swt-displayStyle3-main .table>tbody tr td:last-child{
		padding-right: 0 !important;
	}
	
	.swt-displayStyle3-main .table>tbody tr td:first-child{
		padding-left: 0 !important;
	}
	
	.swt-displayStyle3-main .table>tbody tr td{
		padding: 0 !important;
	}
	
	@media only screen and (max-width: 767px){

		.taglib-page-iterator {
			padding: 0 6px;
		}
		.taglib-page-iterator .lfr-pagination-buttons{
			display: flex;
		}
		.taglib-page-iterator .lfr-pagination-buttons>li>a{
			width: 99%;
			border-radius: 4px;
		}
		.taglib-page-iterator .lfr-pagination-config .current-page-menu .btn{
			margin: 0 0 5px 0;
		}
	}
	.icon-download-document{
		font-size: 16px;
	}
	.input-search-keywords{
	    float: left;
	    height: 26px;
	    border: 1px solid #085791 !important;
	    font-size: 12px;
	    border-radius: 0px;
	}
	.search-range-date{
		float: left;
	    font-weight: bold;
	    padding: 6px;
	}
	.search-datepicker .form-control{
		border: none !important; 
		height: 24px !important;
	}
	.search-range-date-right{
	    float: right;
	}
	.select-advanced-search{
	    float: left;
	    padding: 4px;
	    border: 1px solid #337ab7;
	    height: 28px !important;
	    border-radius: 0px;
	    font-size: 12px;
    	color: #000000e8;
	}
</style>
<%if(displayStyle == StructuredSearchConstants.TABLE_STYLE_4){ %>
<style>
	.search-scores{
	    margin-left: 25%;
	}
	.search-scores-header{
		height: 100px;
	}
	.search-scores-header-title{
	 	text-transform: uppercase;
	 	font-size: 16px;
	 	color: black;
	 	font-weight: bold;
	 	text-align: center;
	}
	.search-scores-header-image img{
		width: 60px;
		height: 60px;
		margin-left: 46%;
	}
	.input-search-scores{
	    float: left;
	    width: 100%;
	    height: 50px;
	    border-top: none !important;
	    border-left: none !important;
	    border-right: none !important;
	    border-radius: 0px;
	    border-bottom: 1px solid #7d7d7d82 !important;
		padding: 20px 0px 0px !important;
		box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.075);
	}
	.input-search-scores .form-control{
		float: left;
	    width: 100%;
	    height: 50px !important;
	    border-top: none !important;
	    border-left: none !important;
	    border-right: none !important;
	    border-radius: 0px;
		padding: 6px 0px 0px !important;
		border-bottom: 1px solid #7d7d7d82 !important;
		box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.075);
	}
	.input-search-scores::-webkit-input-placeholder{
		color: #544e4e !important;
		font-size: 14px;
	}
	.search-form .search-datepicker input::placeholder {
		visibility: inherit !important;
		color:#544e4e !important;
		font-size: 14px;
	}
	input:focus {
	 	border-top: none !important;
	    border-left: none !important;
	    border-right: none !important;
	    border-radius: 0px;
	    border-bottom: 1px solid #7d7d7d82 !important;
	    box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.075) !important;
	}
	.search-datepicker{
	    margin-left: 10%;
	    width: 120%;
	}
	.button-search-scores{
	    margin-top: 5%;
	    width: 50%;
	    float: left;
	    background-color: #085791;
	    height: 50px;
	    padding: 12px;
	}
	.search-form .btn-search{
		width: 100%  !important;
	    height: 100%  !important;
	    margin-left: 0px  !important;
	    padding: 0px  !important;
	    font-weight: 600  !important;
	    font-size: 14px  !important;
	}
	.warning-validator {
		float: left;
		width: 100%;
		display: none;
	}
	.swt-displayStyle4-main{
	    width: 100%;
    	float: left;
	}
	.input-checkbox-wrapper{
		display:none;
	}
	.swt-displayStyle4-main .table-search-result th{
		background: #085791;
	    color: white;
	    font-size: 12px !important;
	    font-weight: 600 !important;
	    text-align: center;
	    text-transform: uppercase;
	    vertical-align: middle !important;
	    border: none !important;
	}
	.table-search-result .table-bordered > tbody > tr > td {
		font-size: 14px;
		text-align: center;
	}
	.table-search-result .table-striped > tbody > tr:nth-of-type(odd){
	    background-color: #ffffff !important;
	}
	.search-scores-header-image{
	    margin-top: 1%;
	}
</style>
<%} %>
<%
	PortletURL portletURL = renderResponse.createRenderURL();

	StructuredSearchContainer structuredSearchContainer = new StructuredSearchContainer(renderRequest,
			portletURL);
	StructuredSearchDisplayTerms searchTerms = (StructuredSearchDisplayTerms)structuredSearchContainer.getSearchTerms();
	
	StructuredSearchUtil searchUtil = new StructuredSearchUtilImpl();
	
	// The list of columns to be displayed in result form.
	List<StructuredSearchColumnDTO> displayColumns = new ArrayList<>();

	// The list of search criterias to be displayed in search form.
	List<StructuredSearchCriteriaDTO> searchCriterias = new ArrayList<>();

	// Temporary variables used in below loops.
	boolean selected = false, showInResult = false;
	int searchType = -1, sequence = -1;

	// Iterate through structure fields to determine the list of search criterias/display columns.
	for (DDMFormField f : fields) {
		selected = GetterUtil.getBoolean(
				portletPreferences.getValue("structure" + structureKey + f.getName() + "selected", "false"));
		showInResult = GetterUtil.getBoolean(portletPreferences
				.getValue("structure" + structureKey + f.getName() + "showInResult", "false"));
		searchType = GetterUtil.getInteger(
				portletPreferences.getValue("structure" + structureKey + f.getName() + "type", "0"));
		sequence = GetterUtil.getInteger(
				portletPreferences.getValue("structure" + structureKey + f.getName() + "sequence", "0"));

		if (selected) {
			StructuredSearchCriteriaDTO dto = new StructuredSearchCriteriaDTO(f.getType(), searchType,
					f.getName(), f.getLabel().getString(locale), sequence);
			searchCriterias.add(dto);
		}
		if (showInResult) {
			StructuredSearchColumnDTO dto = new StructuredSearchColumnDTO(f.getType(), f.getName(),
					f.getLabel().getString(locale), sequence);
			displayColumns.add(dto);
		}
	}

	// Iterate through structure vocabularies to determine the list of search criterias/display columns.
	for (AssetVocabulary v : vocabularies) {
		selected = GetterUtil.getBoolean(
				portletPreferences.getValue("vocabulary" + v.getVocabularyId() + "selected", "false"));
		showInResult = GetterUtil.getBoolean(
				portletPreferences.getValue("vocabulary" + v.getVocabularyId() + "showInResult", "false"));
		searchType = GetterUtil
				.getInteger(portletPreferences.getValue("vocabulary" + v.getVocabularyId() + "type", "0"));
		sequence = GetterUtil
				.getInteger(portletPreferences.getValue("vocabulary" + v.getVocabularyId() + "sequence", "0"));
		String customedTitle = StringPool.BLANK;
		if(showInResult){
			customedTitle = GetterUtil.getString(
				portletPreferences.getValue("vocabulary" + v.getVocabularyId() + "title", StringPool.BLANK));
			if(customedTitle.equals(StringPool.BLANK))
				customedTitle = v.getTitle(locale);
		}

		if (selected) {
			StructuredSearchCriteriaDTO dto = new StructuredSearchCriteriaDTO("vocabulary", searchType,
					String.valueOf(v.getVocabularyId()), v.getTitle(locale), sequence);
			searchCriterias.add(dto);
		}
		if (showInResult) {
			StructuredSearchColumnDTO dto = new StructuredSearchColumnDTO("vocabulary",
					String.valueOf(v.getVocabularyId()), customedTitle, sequence);
			displayColumns.add(dto);
		}
	}
	
	// Sort by sequence.
	Collections.sort(searchCriterias);
	Collections.sort(displayColumns);

	// Retrieve the search result from request parameter (if any).
	String articleIds = request.getParameter("articleIds");
	if (articleIds == null) {
		articleIds = StringPool.BLANK;
	}

	// Check if the current view is basic or advanced search.
	boolean isAdvancedSearch = GetterUtil.getBoolean(request.getParameter("isAdvancedSearch"), false);
	boolean isFirstLoad = GetterUtil.getBoolean(request.getParameter("isFirstLoad"), true);
	
	// Get parameters in URL
	HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
	long categoryId = GetterUtil.getLong(httpServletRequest.getParameter("categoryId"));
	
	// The date formatter in Vietnamese locale.
	SimpleDateFormat viDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	// The date format used when Liferay stores value by structure.
	SimpleDateFormat lfDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	String structuredsearchKeyWord = "";
	if(displayStyle != StructuredSearchConstants.TABLE_STYLE_4){
		structuredsearchKeyWord = LanguageUtil.format(locale, "structuredsearch.enter-keywords", dummyObj);
	}else{
		structuredsearchKeyWord =LanguageUtil.format(locale, "structuredsearch.identifier-number", dummyObj);
	}
%>

<c:choose>
	<c:when test="<%=structureKey == null || structureKey.length() == 0%>">
		<p><span style="font-style: italic; color: red;">Please configure the structure and search settings for this porlet.</span></p>
	</c:when>
	<c:otherwise>
		<div class="container-fluid padding-top-15px">
		<%if(displayStyle == StructuredSearchConstants.TABLE_STYLE_4){ %>
			<div class="search-scores-header">
				<div class="search-scores-header-title"><%=LanguageUtil.format(locale, "information-search", dummyObj)%></div>
				<div class="search-scores-header-image"><img src="<%=themeDisplay.getPathThemeImages() %>/searchscores/iconfinder_icons_user.jpg"></div>
			</div>
			<div class="warning-validator">
				<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-warning-sign"></span>
					<span id="errorSearchScores"> </span>
				</div>
			</div>
		<%} %>
			<aui:form action="<%=portletURL.toString()%>" method="post" name="fm" cssClass="navbar-search search-form">
				<aui:input name="isAdvancedSearch" type="hidden" value="<%=isAdvancedSearch%>" />
				<aui:input type="hidden" value="<%=categoryId %>" name="categoryId" />
				<!-- BASIC SEARCH: Search by keywords. -->
					<div class="row row-swt-responsive">
					<% if (searchByKeywords) { %>
							<!--NHUT.DANG START SEARCH SCORES  -->
								<%if(displayStyle != StructuredSearchConstants.TABLE_STYLE_4){ %>
									<div class="col-md-2 col-swt col-label">
										<label><strong><%=LanguageUtil.format(locale, "keywords", dummyObj)%></strong></label>
									</div>
								<%} %>
							<!--NHUT.DANG END SEARCH SCORES  -->
							<div class="col-md-6 col-swt search-scores">
								<aui:input cssClass="input-search-keywords input-search-scores" name="keywords" type="text" label="<%=StringPool.BLANK%>"
									placeholder='<%=structuredsearchKeyWord %>' />
							<%if(displayStyle == StructuredSearchConstants.TABLE_STYLE_4){ %>
							<aui:input cssClass="input-search-keywords input-search-scores" name="fullName" type="text" label="<%=StringPool.BLANK%>"
									placeholder='<%=LanguageUtil.format(locale, "structuredsearch.fullName", dummyObj)%>' />
							<%} %>
							</div>
							
					<% } %>
						<%if(displayStyle != StructuredSearchConstants.TABLE_STYLE_4){ %>
						<div class="col-md-2 col-swt">
							<aui:button type="submit" cssClass="btn-search" id="btnSearch"
							value='<%=LanguageUtil.format(request, "search", dummyObj)%>' />
						</div>
						<div class="col-md-2 col-swt">
							<% if (searchCriterias.size() > 0) { %>
							<span class="btn-switch-search-mode hidden-xs" id="btnSwitchSearchMode" 
								onClick="switchSearchMode(false);" >
									<%=LanguageUtil.format(request, "search", dummyObj) + " " 
									+ LanguageUtil.format(request, "advanced", dummyObj).toLowerCase()%></span>
									
						<% } %>
						</div>
						<%} %>
					</div>
				
				<!-- ADVANCED SEARCH: Search by selected fields/vocabularies. -->
				<% for (StructuredSearchCriteriaDTO c : searchCriterias) { %>
					<% if (c.getType().equals("text") || c.getType().equals("textarea") || c.getType().equals("ddm-text-html")) { %>
						<div class="row advanced-search row-swt-responsive">
						<%if(displayStyle != StructuredSearchConstants.TABLE_STYLE_4){ %>
			 				<div class="col-md-2 col-swt col-label">
								<label><strong><%=c.getLabel()%></strong></label>
							</div>
							<%} %>
			 				<div class="col-md-10 col-swt search-scores">
		 						<aui:input cssClass="input-search-keywords input-search-scores" type="text" name="<%=c.getName()%>" label="<%=StringPool.BLANK%>" />
			 				</div>
			 			</div>
			 			
			 		<% } else if (c.getType().equals("ddm-date")) { %>
			 			<div class="row advanced-search row-swt-responsive">
			 			<%if(displayStyle != StructuredSearchConstants.TABLE_STYLE_4){ %>
			 				<div class="col-md-2 col-label col-swt">
			 					<label><strong><%=c.getLabel()%></strong></label>
			 				</div>
			 				<%} %>
							<%
								Calendar today = Calendar.getInstance();
								today.setTime(new Date());
							%>
							<div class="col-md-10 col-swt">
								<div class="row row-swt-responsive">
								<% if (c.getSearchType() == StructuredSearchConstants.SEARCH_TYPE_EQUAL) { %>
									<div class="col-md-6 col-swt search-scores">
										<liferay-ui:input-date 
											cssClass="search-datepicker input-search-keywords input-search-scores" 
											dayParam='<%=c.getName() + "_day"%>'
											firstDayOfWeek="<%=today.getFirstDayOfWeek() - 1%>"
											monthParam='<%=c.getName() + "_month"%>'
											name='<%=c.getName()%>'
											nullable="<%=true%>"
											yearParam='<%=c.getName() + "_year"%>' />
									</div>
								<% } else if (c.getSearchType() == StructuredSearchConstants.SEARCH_TYPE_RANGE) { %>
									<div class="col-md-6 col-swt">
										<div class="input-group">
											<span class="search-range-date"><%=LanguageUtil.format(request, "from", dummyObj)%></span>
											<liferay-ui:input-date 
												cssClass="search-datepicker input-search-keywords"
												dayParam='<%=c.getName() + "_from_day"%>'
												firstDayOfWeek="<%=today.getFirstDayOfWeek() - 1%>"
												monthParam='<%=c.getName() + "_from_month"%>'
												name='<%=c.getName() + "_from"%>'
												nullable="<%=true%>"
												yearParam='<%=c.getName() + "_from_year"%>'
											/>
										</div>
									</div>
									<div class="col-md-6 col-swt">
										<div class="input-group search-range-date-right">
											<span class="search-range-date"><%=LanguageUtil.format(request, "to", dummyObj)%></span>
											<liferay-ui:input-date
												cssClass="search-datepicker input-search-keywords"
												dayParam='<%=c.getName() + "_to_day"%>'
												firstDayOfWeek="<%=today.getFirstDayOfWeek() - 1%>"
												monthParam='<%=c.getName() + "_to_month"%>'
												name='<%=c.getName() + "_to"%>'
												nullable="<%=true%>"
												yearParam='<%=c.getName() + "_to_year"%>' 
											/>
										</div>
									</div>
								<% } else { %>
									<%=LanguageUtil.format(request, "structuredsearch.the-search-type-x-is-unsupported-for-x", 
											new Object[] { c.getSearchType(), c.getType() })%>
								<% } %>
								</div>
							</div>
			 			</div>
			 			
					<% } else if (c.getType().equals("vocabulary")) { %>
						<%
							long vocabularyId = Long.valueOf(c.getName());
							AssetVocabulary vocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(vocabularyId);
							List<AssetCategory> categories = vocabulary.getCategories();
						%>
						<div class="row advanced-search row-swt-responsive">
							<div class="col-md-2 col-swt col-label">
								<label><strong><%=c.getLabel().substring((structureKey + "_").length())%></strong></label>
							</div>
							<div class="col-md-10 col-swt">
								<aui:select cssClass="select-advanced-search" name='<%="vocabulary_" + vocabularyId%>' label="<%=StringPool.BLANK%>">
									<aui:option value='0' label='<%="--- " + LanguageUtil.format(request, "all", dummyObj) + " ---" %>'>
									</aui:option>
									<% for (AssetCategory a : categories) { %>
										<aui:option value='<%=a.getCategoryId()%>' label='<%=a.getName()%>'
										selected="<%=a.getCategoryId() == categoryId %>">
										</aui:option>
									<% } %>
								</aui:select>
							</div>
						</div>
						
					<% } %>
				<% } %>
				
				<%if(displayStyle == StructuredSearchConstants.TABLE_STYLE_4){ %>
						<div class="row search-scores button-search-scores">
							<aui:button type="button" onClick="searchScoreValidate()" cssClass="btn-search" id="btnSearch"
							value='<%=LanguageUtil.format(request, "btn-search-scores", dummyObj)%>' />
						</div>
					<script>
						$( document ).ready(function() {
							var textPlacehoder = '<%=LanguageUtil.format(request, "swt-placehoder-date", dummyObj)%>';
							var reTextPlacehoder = '<%=LanguageUtil.format(request, "swt-placehoder-birthaday", dummyObj)%>';
							var placeholder = $("input[placeholder='"+textPlacehoder+"']");
							if(placeholder!=""){
						    	$(placeholder).attr("placeholder", reTextPlacehoder);
							}
						});	
						function searchScoreValidate(){
							var keywords = $('#<portlet:namespace />keywords').val();
							var fullName = $('#<portlet:namespace />fullName').val();
							var ngaySinh = $('#<portlet:namespace />ngaySinh').val();
							if(keywords=='' || fullName=='' || ngaySinh==''){
								$("#errorSearchScores").empty();
								$("#errorSearchScores").text("<%=LanguageUtil.format(request, "warning-user-search-scores", new Object())%>");
								$(".warning-validator").slideDown();
							}else{
							 $('#<portlet:namespace />fm').submit();
							}
							
						}
								
				</script>		
				<%} %>
			<!-- type display style table 1 -->	
		<%if(displayStyle ==StructuredSearchConstants.TABLE_STYLE_1){ %>	
		<!-- SEARCH CONTAINER -->
		<div id="<portlet:namespace />searchContainer" class="padding-top-35px">
			<liferay-ui:search-container id="tblSearchResults" cssClass="table-search-result"
				searchContainer="<%=structuredSearchContainer%>" emptyResultsMessage="there-are-no-results"
				delta="3" deltaConfigurable="true" >
				
				<%
					int totalArticle = searchUtil.searchCount(structuredSearchContainer, themeDisplay, structureKey, categoryId, renderRequest);
					structuredSearchContainer.setTotal(totalArticle);
				%>
				  
				<div class="row-fluid row-total-records">
					<span><%=LanguageUtil.format(request, "structuredsearch.total-records", dummyObj) + ": " + totalArticle%></span>
				</div>
				
				<liferay-ui:search-container-results
					results="<%=searchUtil.search(structuredSearchContainer, themeDisplay, structureKey, categoryId, renderRequest) %>" />
				
				<liferay-ui:search-container-row className="com.liferay.journal.model.JournalArticle" 
					modelVar="article" indexVar="index" keyProperty="id" >
					<%
						// Create the URL for detail view. It's required a display page configured for the entry.
						AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
						String viewURL =  entry.getAssetRenderer().getURLView(liferayPortletResponse, WindowState.NORMAL);
						viewURL = entry.getAssetRenderer().getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewURL);
						
						String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
						Document articleDoc = null;
					    try {
					    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
					    } catch (DocumentException e) {
					        e.printStackTrace();
					    }
					%>
	 				
					<!-- Order number column. -->
					<% if (showOrderInResult) { %>
						<liferay-ui:search-container-column-text
							align="center"
							name="<%=orderInResultTxtDisplay%>"
							value="<%=String.valueOf(structuredSearchContainer.getStart() +  index + 1)%>"
							href="<%=viewURL%>"
							cssClass="col-order-number" />
					<% } %>
					
					<!-- Title column. -->
					<% if (showTitleInResult) { %>
						<liferay-ui:search-container-column-text
							name='<%=titleInResultTxtDisplay%>'
							value="<%=HtmlUtil.escape(article.getTitle(locale))%>"
							href="<%=viewURL%>" />
					<% } %>
					
					<!-- Summary column. -->
					<% if (showSummaryInResult) { %>
						<liferay-ui:search-container-column-text
							name='<%=summaryInResultTxtDisplay%>'
							value="<%=HtmlUtil.escape((article.getDescription(locale)))%>"
							href="<%=viewURL%>" />
					<% } %>
					
					<!-- Additional columns from structure and vocabularies. -->
					<% if (articleDoc != null) { %>
						<% Node node = null; %>
						<% for (StructuredSearchColumnDTO col : displayColumns) { %>
							<%
								String title = null, valueStr = null;
								
								switch (col.getType()) {
								case "text":
								case "textarea":
								case "ddm-text-html":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							        }
									break;
									
								case "ddm-date":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							            
							            if (valueStr == null) {
							            	valueStr = StringPool.BLANK;
							            }
							            
							            // Parse date value to expected format.
							            if (valueStr.length() != 0) {
							            	try {
							            		Date valueDate = lfDateFormat.parse(valueStr);
							            		valueStr = viDateFormat.format(valueDate);
							            	} catch (ParseException e) {
							            		break;
							            	}
							            }
							        }
									break;
									
								case "vocabulary":
									title = col.getLabel();
									if (title.startsWith(vocabularyPrefix)) {
										title = title.substring(vocabularyPrefix.length());
									}
									
									List<AssetCategory> categories = AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), article.getResourcePrimKey());
									valueStr = StringPool.BLANK;
									
									for (AssetCategory ac : categories) {
										if (!String.valueOf(ac.getVocabularyId()).equals(col.getName())) {
											continue;
										}
										if (valueStr.length() != 0) {
											valueStr += ", " + ac.getTitle(locale);
										} else {
											valueStr += ac.getTitle(locale);
										}
									}
									
									break;
									
								default:
									break;
								}
							%>
							<% if (title != null) { %>
								<liferay-ui:search-container-column-text
									name="<%=title.trim()%>"
									value="<%=(valueStr == null ? StringPool.BLANK : HtmlUtil.escape(valueStr.trim()))%>"
									href="<%=viewURL%>" />
							<% } %>
						<% } %>
					<% } %>
					
					<!-- Download all column. -->

					<%
						if (showDownloadInResult) {
							String file = GetterUtil.DEFAULT_STRING;
							String codeTxt = GetterUtil.DEFAULT_STRING;
							Document document = SAXReaderUtil.read(article.getContent());
							Element root = document.getRootElement();

							for (Element em : root.elements()) {
								if (em.attributeValue("name").equals("attachment")) {
									String attachedFile = GetterUtil.getString(em.element("dynamic-content").getText());
									file = file + "'" + attachedFile + "',";
								}
								
								if (em.attributeValue("name").equals("code")) {
									codeTxt = GetterUtil.getString(em.element("dynamic-content").getText());
								}
							}

							if (!file.equals(GetterUtil.DEFAULT_STRING)) {
								file = "[" + file.substring(0, file.lastIndexOf(",")) + "]";
							}
					%>
						<c:set var="entryId" value="<%=entry.getEntryId() %>" />
						<c:set var="file" value="<%=file.trim()%>" />
						<c:set var="codeTxt" value="<%=codeTxt.trim()%>" />
					
						<!-- The default value if file does not exit is ['']  -->
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "download", dummyObj)%>'
							cssClass="col-download-attachments"
							href="javascript:downloadAllFiles(${entryId}, ${file}, '${codeTxt}')">
							<i class='icon-download-document <%=file.length() > 10 ? "icon-download-alt" : "" %>'></i>
						</liferay-ui:search-container-column-text>
					<% } %>
					
				</liferay-ui:search-container-row>
				
				<liferay-ui:search-iterator />
			</liferay-ui:search-container>
		</div>
		<%} %>
		<!-- type display table 2  -->
		<%if(displayStyle ==StructuredSearchConstants.TABLE_STYLE_2){
			List<JournalArticle> listArticle = searchUtil.search(structuredSearchContainer, themeDisplay, structureKey, categoryId, renderRequest);
			//  draft is being consulted
			List<JournalArticle> listResultDraftIsBeingConsulted = searchUtil.searchDraftIsBeingConsulted(listArticle, themeDisplay);
			// draft is expired
			List<JournalArticle> listResultDraftIsExpired = searchUtil.searchDraftIsExpired(listArticle, themeDisplay);
			%>	
		<!-- SEARCH CONTAINER -->
		<!-- draft is being consulted -->
		<div id="<portlet:namespace />searchContainer" class="padding-top-35px">
			<liferay-ui:search-container id="tblSearchResults" cssClass="table-search-result list-result-draft"
				searchContainer="<%=structuredSearchContainer%>" emptyResultsMessage="there-are-no-results"
				delta="3" deltaConfigurable="true" >
				<%
				int totalArticle = listResultDraftIsBeingConsulted.size()+ listResultDraftIsExpired.size();
				structuredSearchContainer.setTotal(totalArticle);
				%>
				  
				<div class="row-fluid row-total-records">
					<span><%=LanguageUtil.format(request, "structuredsearch.total-records", dummyObj) + ": " + totalArticle%></span>
				</div>
				
				<div class="table-result-list-draft col-md-4 col-sm-4 col-xs-12"> 
				<p><%=LanguageUtil.format(request, "structuredsearch.DraftIsBeingConsulted", dummyObj)%></p></div>
				<liferay-ui:search-container-results
					results="<%=listResultDraftIsBeingConsulted%>" />
				<liferay-ui:search-container-row className="com.liferay.journal.model.JournalArticle" 
					modelVar="article" indexVar="index" keyProperty="id" >
					<%
						// Create the URL for detail view. It's required a display page configured for the entry.
						AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
						String viewURL =  entry.getAssetRenderer().getURLView(liferayPortletResponse, WindowState.NORMAL);
						viewURL = entry.getAssetRenderer().getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewURL);
						
						String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
						Document articleDoc = null;
						Element root = null;
					    try {
					    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
					    	 root = articleDoc.getRootElement();
					    } catch (DocumentException e) {
					        e.printStackTrace();
					    }
					%>
	 				
					<!-- Order number column. -->
					<% if (showOrderInResult) { %>
						<liferay-ui:search-container-column-text
							align="center"
							name="<%=LanguageUtil.format(request, "structuredsearch.order-number", dummyObj)%>"
							value="<%=String.valueOf(structuredSearchContainer.getStart() +  index + 1)%>"
							href="<%=viewURL%>"
							cssClass="col-order-number" />
					<% } %>
					
					<!-- Title column. -->
					<% if (showTitleInResult) { %>
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "title", dummyObj)%>'
							value="<%=HtmlUtil.escape(article.getTitle(locale))%>"
							href="<%=viewURL%>" />
					<% } %>
					
					<!-- Summary column. -->
					<% if (showSummaryInResult) { %>
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "description", dummyObj)%>'
							value="<%=HtmlUtil.escape(article.getDescription(locale))%>"
							href="<%=viewURL%>" />
					<% } %>
					
						<%
						String startDatePare="";
						String endDatePare="";
						
						if(root!=null){
							for (Element em : root.elements()) {
								if (em.attributeValue("name").equals("startDate")) {
									Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
									 startDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
								}
								if (em.attributeValue("name").equals("endDate")) {
									Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
									 endDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
									
								}
							}
						}
						%>	
					<liferay-ui:search-container-column-text value="<%=startDatePare%>"
							name='<%=LanguageUtil.format(request, "structuredsearch.startDate", dummyObj)%>'
							href="<%=viewURL%>"/>
						
					<liferay-ui:search-container-column-text value="<%=endDatePare%>"
							name='<%=LanguageUtil.format(request, "structuredsearch.endDate", dummyObj)%>'
							href="<%=viewURL%>" />
					<!-- Additional columns from structure and vocabularies. -->
					<% if (articleDoc != null) { %>
						<% Node node = null; %>
						<% for (StructuredSearchColumnDTO col : displayColumns) { %>
							<%
								String title = null, valueStr = null;
								
								switch (col.getType()) {
								case "text":
								case "textarea":
								case "ddm-text-html":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							        }
									break;
									
								case "ddm-date":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							            
							            if (valueStr == null) {
							            	valueStr = StringPool.BLANK;
							            }
							            
							            // Parse date value to expected format.
							            if (valueStr.length() != 0) {
							            	try {
							            		Date valueDate = lfDateFormat.parse(valueStr);
							            		valueStr = viDateFormat.format(valueDate);
							            	} catch (ParseException e) {
							            		break;
							            	}
							            }
							        }
									break;
									
								case "vocabulary":
									title = col.getLabel();
									if (title.startsWith(vocabularyPrefix)) {
										title = title.substring(vocabularyPrefix.length());
									}
									
									List<AssetCategory> categories = AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), article.getResourcePrimKey());
									valueStr = StringPool.BLANK;
									
									for (AssetCategory ac : categories) {
										if (!String.valueOf(ac.getVocabularyId()).equals(col.getName())) {
											continue;
										}
										if (valueStr.length() != 0) {
											valueStr += ", " + ac.getTitle(locale);
										} else {
											valueStr += ac.getTitle(locale);
										}
									}
									
									break;
									
								default:
									break;
								}
							%>
							<% if (title != null) { %>
								<liferay-ui:search-container-column-text
									name="<%=title.trim()%>"
									value="<%=(valueStr == null ? StringPool.BLANK : HtmlUtil.escape(valueStr.trim()))%>"
									href="<%=viewURL%>" />
							<% } %>
						<% } %>
					<% } %>
					
					<!-- Download all column. -->

					<%
						if (showDownloadInResult) {
							String file = GetterUtil.DEFAULT_STRING;
							String codeTxt = GetterUtil.DEFAULT_STRING;
							Document document = SAXReaderUtil.read(article.getContent());
							 root = document.getRootElement();

							for (Element em : root.elements()) {
								if (em.attributeValue("name").equals("attachment")) {
									String attachedFile = GetterUtil.getString(em.element("dynamic-content").getText());
									file = file + "'" + attachedFile + "',";
								}
								
								if (em.attributeValue("name").equals("code")) {
									codeTxt = GetterUtil.getString(em.element("dynamic-content").getText());
								}
							}

							if (!file.equals(GetterUtil.DEFAULT_STRING)) {
								file = "[" + file.substring(0, file.lastIndexOf(",")) + "]";
							}
					%>
						<c:set var="entryId" value="<%=entry.getEntryId() %>" />
						<c:set var="file" value="<%=file.trim()%>" />
						<c:set var="codeTxt" value="<%=codeTxt.trim()%>" />
					
						<!-- The default value if file does not exit is ['']  -->
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "download", dummyObj)%>'
							cssClass="col-download-attachments"
							href="javascript:downloadAllFiles(${entryId}, ${file}, '${codeTxt}')">
							<i class='icon-download-document <%=file.length() > 10 ? "icon-download-alt" : "" %>'></i>
						</liferay-ui:search-container-column-text>
					<% } %>
					
				</liferay-ui:search-container-row>
				
				<liferay-ui:search-iterator />
			</liferay-ui:search-container>
		</div>
		<!--draft is expired -->
			<liferay-ui:search-container id="tblSearchResults" cssClass="table-search-result list-result-draft"
				emptyResultsMessage="there-are-no-results"
				delta="3" deltaConfigurable="true" >
				
				<div class="table-result-list-draft col-md-4 col-sm-4 col-xs-12"> 
				<p><%=LanguageUtil.format(request, "structuredsearch.DraftIsExpired", dummyObj)%></p></div>
				<liferay-ui:search-container-results
					results="<%=listResultDraftIsExpired%>" />
				<liferay-ui:search-container-row className="com.liferay.journal.model.JournalArticle" 
					modelVar="article" indexVar="index" keyProperty="id" >
					<%
						// Create the URL for detail view. It's required a display page configured for the entry.
						AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
						String viewURL =  entry.getAssetRenderer().getURLView(liferayPortletResponse, WindowState.NORMAL);
						viewURL = entry.getAssetRenderer().getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewURL);
						
						String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
						Document articleDoc = null;
						Element root = null;
					    try {
					    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
					    	 root = articleDoc.getRootElement();
					    } catch (DocumentException e) {
					        e.printStackTrace();
					    }
					%>
	 				
					<!-- Order number column. -->
					<% if (showOrderInResult) { %>
						<liferay-ui:search-container-column-text
							align="center"
							name="<%=LanguageUtil.format(request, "structuredsearch.order-number", dummyObj)%>"
							value="<%=String.valueOf(structuredSearchContainer.getStart() +  index + 1)%>"
							href="<%=viewURL%>"
							cssClass="col-order-number" />
					<% } %>
					
					<!-- Title column. -->
					<% if (showTitleInResult) { %>
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "title", dummyObj)%>'
							value="<%=HtmlUtil.escape(article.getTitle(locale))%>"
							href="<%=viewURL%>" />
					<% } %>
					
					<!-- Summary column. -->
					<% if (showSummaryInResult) { %>
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "description", dummyObj)%>'
							value="<%=HtmlUtil.escape(article.getDescription(locale))%>"
							href="<%=viewURL%>" />
					<% } %>
					
						<%
						String startDatePare="";
						String endDatePare="";
						if(root!=null){
						for (Element em : root.elements()) {
				    		// ngay bat dau va ngay ket thuc
							if (em.attributeValue("name").equals("startDate")) {
								Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
								 startDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
							}
							if (em.attributeValue("name").equals("endDate")) {
								Date text = DateUtil.parseDate("yyyy-MM-dd",em.element("dynamic-content").getText(), themeDisplay.getLocale()) ;
								 endDatePare= DateUtil.getDate(text, "dd/MM/yyyy", themeDisplay.getLocale());
								
							}
						}
						}
						%>	
					<liferay-ui:search-container-column-text value="<%=startDatePare%>"
							name='<%=LanguageUtil.format(request, "structuredsearch.startDate", dummyObj)%>'
							href="<%=viewURL%>"/>
						
					<liferay-ui:search-container-column-text value="<%=endDatePare%>"
							name='<%=LanguageUtil.format(request, "structuredsearch.endDate", dummyObj)%>'
							href="<%=viewURL%>" />
					<!-- Additional columns from structure and vocabularies. -->
					<% if (articleDoc != null) { %>
						<% Node node = null; %>
						<% for (StructuredSearchColumnDTO col : displayColumns) { %>
							<%
								String title = null, valueStr = null;
								
								switch (col.getType()) {
								case "text":
								case "textarea":
								case "ddm-text-html":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							        }
									break;
									
								case "ddm-date":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							            
							            if (valueStr == null) {
							            	valueStr = StringPool.BLANK;
							            }
							            
							            // Parse date value to expected format.
							            if (valueStr.length() != 0) {
							            	try {
							            		Date valueDate = lfDateFormat.parse(valueStr);
							            		valueStr = viDateFormat.format(valueDate);
							            	} catch (ParseException e) {
							            		break;
							            	}
							            }
							        }
									break;
									
								case "vocabulary":
									title = col.getLabel();
									if (title.startsWith(vocabularyPrefix)) {
										title = title.substring(vocabularyPrefix.length());
									}
									
									List<AssetCategory> categories = AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), article.getResourcePrimKey());
									valueStr = StringPool.BLANK;
									
									for (AssetCategory ac : categories) {
										if (!String.valueOf(ac.getVocabularyId()).equals(col.getName())) {
											continue;
										}
										if (valueStr.length() != 0) {
											valueStr += ", " + ac.getTitle(locale);
										} else {
											valueStr += ac.getTitle(locale);
										}
									}
									
									break;
									
								default:
									break;
								}
							%>
							<% if (title != null) { %>
								<liferay-ui:search-container-column-text
									name="<%=title.trim()%>"
									value="<%=(valueStr == null ? StringPool.BLANK : HtmlUtil.escape(valueStr.trim()))%>"
									href="<%=viewURL%>" />
							<% } %>
						<% } %>
					<% } %>
					
					<!-- Download all column. -->

					<%
						if (showDownloadInResult) {
							String file = GetterUtil.DEFAULT_STRING;
							String codeTxt = GetterUtil.DEFAULT_STRING;
							Document document = SAXReaderUtil.read(article.getContent());
							 root = document.getRootElement();

							for (Element em : root.elements()) {
								if (em.attributeValue("name").equals("attachment")) {
									String attachedFile = GetterUtil.getString(em.element("dynamic-content").getText());
									file = file + "'" + attachedFile + "',";
								}
								
								if (em.attributeValue("name").equals("code")) {
									codeTxt = GetterUtil.getString(em.element("dynamic-content").getText());
								}
							}

							if (!file.equals(GetterUtil.DEFAULT_STRING)) {
								file = "[" + file.substring(0, file.lastIndexOf(",")) + "]";
							}
					%>
						<c:set var="entryId" value="<%=entry.getEntryId() %>" />
						<c:set var="file" value="<%=file.trim()%>" />
						<c:set var="codeTxt" value="<%=codeTxt.trim()%>" />
					
						<!-- The default value if file does not exit is ['']  -->
						<liferay-ui:search-container-column-text
							name='<%=LanguageUtil.format(request, "download", dummyObj)%>'
							cssClass="col-download-attachments"
							href="javascript:downloadAllFiles(${entryId}, ${file}, '${codeTxt}')">
							<i class='icon-download-document <%=file.length() > 10 ? "icon-download-alt" : "" %>'></i>
						</liferay-ui:search-container-column-text>
					<% } %>
					
				</liferay-ui:search-container-row>
				
				<liferay-ui:search-iterator />
			</liferay-ui:search-container>
		<%} %>
		
		<!-- type display style table 3  -->
		<%if(displayStyle == StructuredSearchConstants.TABLE_STYLE_3){ %>
		<!-- SEARCH CONTAINER -->
			<div id="<portlet:namespace />searchContainer" class="padding-top-35px swt-displayStyle3-main">
				<liferay-ui:search-container id="tblSearchResults" cssClass="table-search-result"
					searchContainer="<%=structuredSearchContainer%>" emptyResultsMessage="there-are-no-results"
					delta="3" deltaConfigurable="true" >
					<%
						int totalArticle = searchUtil.searchCount(structuredSearchContainer, themeDisplay, structureKey, categoryId, renderRequest);
						structuredSearchContainer.setTotal(totalArticle);
					%>
					  
					<div class="row-fluid row-total-records">
						<span><%=LanguageUtil.format(request, "structuredsearch.total-records", dummyObj) + ": " + totalArticle%></span>
					</div>
					
					<liferay-ui:search-container-results
					results="<%=searchUtil.search(structuredSearchContainer, themeDisplay, structureKey, categoryId, renderRequest) %>" />
					
					<liferay-ui:search-container-row className="com.liferay.journal.model.JournalArticle" 
						modelVar="article" indexVar="index" keyProperty="id" >
						<%
							// Create the URL for detail view. It's required a display page configured for the entry.
							AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
							String viewURL =  entry.getAssetRenderer().getURLView(liferayPortletResponse, WindowState.NORMAL);
							viewURL = entry.getAssetRenderer().getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewURL);
							
							String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
							Document articleDoc = null;
						    try {
						    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
						    } catch (DocumentException e) {
						        e.printStackTrace();
						    }
						%>
						
						<!-- Title column. -->
						<liferay-ui:search-container-column-text >
							<div class="row-fluid">
							  <div class="col-md-12 swt-displayStyle3-title">
							  	<a href="<%=viewURL%>"><span><%=HtmlUtil.escape(article.getTitle(locale))%></span></a>
							  </div>
							</div>
					
								<!-- Additional columns from structure and vocabularies. -->
								
								<%
								List<String[]> resultDisplay = new ArrayList<>();
								String imgUrl = themeDisplay.getTheme().getContextPath() + "/images/newsdefault.jpg";
								%>
								<% if (articleDoc != null) { %>
									<%
										Node nodeImg  = articleDoc.selectSingleNode("/root/dynamic-element[@name='image']/dynamic-content");
								        if (nodeImg != null && nodeImg.getText().length() > 0) {
								        	imgUrl = nodeImg.getText();
								        }
									%>
									<% Node node = null; %>
									<% for (StructuredSearchColumnDTO col : displayColumns) {%>
										<%
											String valueStr = null;
											
											switch (col.getType()) {
											case "text":
												node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
										        if (node != null && node.getText().length() > 0) {
										            valueStr = node.getText();
										        }
												break;
												
											case "text_box":
												node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
										        if (node != null && node.getText().length() > 0) {
										            valueStr = node.getText();
										        }
												break;
												
											case "textarea":
												node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
										        if (node != null && node.getText().length() > 0) {
										            valueStr = node.getText();
										        }
												break;
											default:
												break;
											}
											
											String[] arrStr = new String[]{col.getLabel(),valueStr};
											resultDisplay.add(arrStr);
										%>
										
									<% } %>
								<% } %>
							
								<% if (articleDoc != null) { %>
								 
									<div class="row-fluid">
									  <div class="col-md-3 swt-displayStyle3-pad7px">
									  	<img class="swt-displayStyle3-flag" src="<%=imgUrl%>" />
									  </div>
									  <div class="col-md-9 swt-displayStyle3-pad7px">
									 	<%for(String[] str  : resultDisplay){ %>
									  	<span class="swt-displayStyle3-text"><strong><%=str[0]%>:</strong> <%=HtmlUtil.escape(str[1]) %></span>
									  	<br/>
									  	<%} %>
									  </div>
									</div>
								
								<%} %>
							</liferay-ui:search-container-column-text>
						</liferay-ui:search-container-row>
					<liferay-ui:search-iterator />
				</liferay-ui:search-container>
			</div>
		<%} %>
		
		<!-- type display style table 4  -->
		<%if(displayStyle == StructuredSearchConstants.TABLE_STYLE_4){ %>
		<% String keyWord = GetterUtil.getString(httpServletRequest.getParameter(themeDisplay.getPortletDisplay().getNamespace()+"keywords")); 
		%>
		<%if(Validator.isNotNull(keyWord)){ %> 
		<!-- SEARCH CONTAINER -->
			<div id="<portlet:namespace />searchContainer" class="padding-top-35px swt-displayStyle4-main">
				<liferay-ui:search-container id="tblSearchResults" cssClass="table-search-result"
					searchContainer="<%=structuredSearchContainer%>" emptyResultsMessage="there-are-no-results"
					delta="3" deltaConfigurable="true" >
					
					<liferay-ui:search-container-results
					results="<%=searchUtil.search(structuredSearchContainer, themeDisplay, structureKey, categoryId, renderRequest) %>" />
					
					<liferay-ui:search-container-row className="com.liferay.journal.model.JournalArticle" 
						modelVar="article" indexVar="index" keyProperty="id" >
						<%
							// Create the URL for detail view. It's required a display page configured for the entry.
							AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
							String viewURL =  entry.getAssetRenderer().getURLView(liferayPortletResponse, WindowState.NORMAL);
							viewURL = entry.getAssetRenderer().getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewURL);
							
							String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
							Document articleDoc = null;
						    try {
						    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
						    } catch (DocumentException e) {
						        e.printStackTrace();
						    }
						%>
						
								<!-- Title column. -->
							<% if (showTitleInResult) { %>
								<liferay-ui:search-container-column-text align="center"
									name='SBD'
									value="<%=HtmlUtil.escape(article.getTitle(locale))%>" />
							<% } %>
							
							<!-- Summary column. -->
							<% if (showSummaryInResult) { %>
								<liferay-ui:search-container-column-text align="center"
									name='<%=LanguageUtil.format(locale, "structuredsearch.fullName-description", dummyObj)%>'
									value="<%=HtmlUtil.escape(article.getDescription(locale))%>" />
							<% } %>
								<!-- Additional columns from structure and vocabularies. -->
								
								<%
								List<String[]> resultDisplay = new ArrayList<>();
								%>
								<!-- Additional columns from structure and vocabularies. -->
					<% if (articleDoc != null) { %>
						<% Node node = null; %>
						<% for (StructuredSearchColumnDTO col : displayColumns) { %>
							<%
								String title = null, valueStr = null;
								
								switch (col.getType()) {
								case "text":
								case "textarea":
								case "ddm-text-html":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							        }
									break;
									
								case "ddm-date":
									node = articleDoc.selectSingleNode("/root/dynamic-element[@name='" + col.getName() + "']/dynamic-content");
							        if (node != null && node.getText().length() > 0) {
							            title = col.getLabel();
							            valueStr = node.getText();
							            
							            if (valueStr == null) {
							            	valueStr = StringPool.BLANK;
							            }
							            
							            // Parse date value to expected format.
							            if (valueStr.length() != 0) {
							            	try {
							            		Date valueDate = lfDateFormat.parse(valueStr);
							            		valueStr = viDateFormat.format(valueDate);
							            	} catch (ParseException e) {
							            		break;
							            	}
							            }
							        }
									break;
									
								case "vocabulary":
									title = col.getLabel();
									if (title.startsWith(vocabularyPrefix)) {
										title = title.substring(vocabularyPrefix.length());
									}
									
									List<AssetCategory> categories = AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), article.getResourcePrimKey());
									valueStr = StringPool.BLANK;
									
									for (AssetCategory ac : categories) {
										if (!String.valueOf(ac.getVocabularyId()).equals(col.getName())) {
											continue;
										}
										if (valueStr.length() != 0) {
											valueStr += ", " + ac.getTitle(locale);
										} else {
											valueStr += ac.getTitle(locale);
										}
									}
									
									break;
									
								default:
									break;
								}
							%>
							<% if (title != null) { %>
								<liferay-ui:search-container-column-text
									name="<%=title.trim()%>"
									value="<%=(valueStr == null ? StringPool.BLANK : HtmlUtil.escape(valueStr.trim()))%>"
									href="<%=viewURL%>" />
							<% } %>
						<% } %>
					<% } %>
							
						</liferay-ui:search-container-row>
					<liferay-ui:search-iterator />
				</liferay-ui:search-container>
			</div>
			 <%} %>
		<%} %>
		</aui:form>
		</div>
	</c:otherwise>
</c:choose>

<a id="hiddenDownload" style="display: none;"></a>

<aui:script>
	/** Perform download all files of the selected record. */
	function downloadAllFiles(id, urls, code) {
		zipFiles(id, urls, code);
	}

	function zipFiles(id, urls, code) {
		var jszip = new JSZip();
		doZip(jszip, urls, 0, code);
	}
	
	function doZip(jszip, urls, index, code) {
		var fileName = urls[index].split('/')[4];
		
		JSZipUtils.getBinaryContent(urls[index], function(err, data) {
			if (!err) {
				jszip.file(fileName, data, {
					binary : true
				});
				
				if (++index == urls.length) {
					jszip.generateAsync({
						type : "blob"
					}).then(function(content) {
						var zipName = "tailieudinhkem";
						if(code != null){
							zipName = code;
						}
						var a = $("#hiddenDownload");
						a.attr("download", zipName);
						a.attr("href", URL.createObjectURL(content));
						document.getElementById("hiddenDownload").click();
					});
				} else {
					doZip(jszip, urls, index, code);
				}
			}
		});
	}

	/** Switch the search form to advanced or basic display mode. */
	function switchSearchMode(toBasic) {
		"use strict";

		if (toBasic === true) {
			$('.advanced-search').css('display', 'none');
			$('.advanced-search input').prop("disabled", true);
			$('[id$="btnSwitchSearchMode"]').html(
							 Liferay.Language.get('search') + ' '
							+ Liferay.Language.get('advanced').toLowerCase());
			$('[id$="btnSwitchSearchMode"]').attr('onclick',
					'switchSearchMode(false);');
			$('[id$="isAdvancedSearch"]').val("false");
			
		} else {
			$('.advanced-search').css('display', 'inherit');
			$('.advanced-search input').prop("disabled", false);
			$('[id$="btnSwitchSearchMode"]').html(
							Liferay.Language.get('search') + ' '
							+ Liferay.Language.get('basic').toLowerCase());
			$('[id$="btnSwitchSearchMode"]').attr('onclick',
					'switchSearchMode(true);');
			$('[id$="isAdvancedSearch"]').val("true");
			
		}

		return false;
	}
	
	<% if (isAdvancedSearch || advancedSearchByDefault) { %>
		switchSearchMode(false);
	<% } %>
	
	// Remove the "Disable" check-boxes of date-pickers.
	$('input[id$="_column2_0_disable"]').parent().remove();
	
	$('[id$="btnSearch"]').click(function(){
		$('#<portlet:namespace />categoryId').val('0');
	});
	
</aui:script>