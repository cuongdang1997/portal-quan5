<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page
	import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMFormField"%>
<%@page import="com.liferay.dynamic.data.mapping.kernel.DDMStructure"%>
<%@page
	import="com.liferay.dynamic.data.mapping.kernel.DDMStructureManagerUtil"%>
<%@page import="com.liferay.journal.model.JournalArticle"%>
<%@page
	import="com.liferay.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.search.BaseModelSearchResult"%>
<%@page
	import="com.liferay.portal.kernel.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>

<%@page
	import="com.swt.portal.structuredsearch.model.StructuredSearchConstants"%>
<%@page
	import="com.swt.portal.structuredsearch.portlet.StructuredSearchConfiguration"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="javax.portlet.PortletPreferences"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	long companyId = themeDisplay.getCompanyId();
	long groupId = themeDisplay.getScopeGroupId();
	Object dummyObj = new Object();

	StructuredSearchConfiguration configuration = (StructuredSearchConfiguration) GetterUtil
			.getObject(renderRequest.getAttribute(StructuredSearchConfiguration.class.getName()));
	String structureKey = StringPool.BLANK;
	long displayStyle = StructuredSearchConstants.TABLE_STYLE_1;
	boolean searchByKeywords = true, showOrderInResult = true, showTitleInResult = true,
			showSummaryInResult = true, showDownloadInResult = true, advancedSearchByDefault = true;

	if (Validator.isNotNull(configuration)) {
		structureKey = GetterUtil
				.getString(portletPreferences.getValue("structureKey", configuration.structureKey()));
		displayStyle = GetterUtil.getLong(
				portletPreferences.getValue("displayStyle", String.valueOf(configuration.displayStyle())));
		searchByKeywords = GetterUtil.getBoolean(portletPreferences.getValue("searchByKeywords",
				String.valueOf(configuration.searchByKeywords())));
		showOrderInResult = GetterUtil.getBoolean(portletPreferences.getValue("showOrderInResult",
				String.valueOf(configuration.showOrderInResult())));
		showTitleInResult = GetterUtil.getBoolean(portletPreferences.getValue("showTitleInResult",
				String.valueOf(configuration.showTitleInResult())));
		showSummaryInResult = GetterUtil.getBoolean(portletPreferences.getValue("showSummaryInResult",
				String.valueOf(configuration.showSummaryInResult())));
		showDownloadInResult = GetterUtil.getBoolean(portletPreferences.getValue("showDownloadInResult",
				String.valueOf(configuration.showDownloadInResult())));
		advancedSearchByDefault = GetterUtil.getBoolean(portletPreferences.getValue("advancedSearchByDefault",
				String.valueOf(configuration.advancedSearchByDefault())));
	}
	
	//Customed column titles
	String orderInResultTxt = GetterUtil.getString(portletPreferences.getValue("orderInResultTxt", 
			StringPool.BLANK));
	String orderInResultTxtDisplay = orderInResultTxt == StringPool.BLANK ? LanguageUtil.format(
			request, "structuredsearch.order-number", dummyObj) : orderInResultTxt;
	String titleInResultTxt = GetterUtil.getString(
			portletPreferences.getValue("titleInResultTxt", StringPool.BLANK));
	String titleInResultTxtDisplay = titleInResultTxt == StringPool.BLANK ? LanguageUtil.format(
			request, "title", dummyObj) : titleInResultTxt;
	String summaryInResultTxt = GetterUtil.getString(
			portletPreferences.getValue("summaryInResultTxt", StringPool.BLANK));
	String summaryInResultTxtDisplay = summaryInResultTxt == StringPool.BLANK ? LanguageUtil.format(
			request, "description", dummyObj) : summaryInResultTxt;
	

	// The list of structure fields.
	List<DDMFormField> fields = new ArrayList<>();
	long journalArticleClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);

	// The list of vocabularies which name is prefixed with structure ID.
	List<AssetVocabulary> vocabularies = new ArrayList<>();
	String vocabularyPrefix = StringPool.BLANK;

	if (structureKey != null && structureKey.length() != 0) {
		// Get the selected structure by its ID.
		DDMStructure selectedStructure = null;
		try {
			selectedStructure = DDMStructureManagerUtil.getStructure(groupId, journalArticleClassNameId,
					structureKey);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if (selectedStructure != null) {
			// Get the fields from selected structure.
			fields = selectedStructure.getDDMFormFields(false);
		}

		vocabularyPrefix = selectedStructure.getStructureId() + "_";
		
		try {
			BaseModelSearchResult<AssetVocabulary> tmp = AssetVocabularyLocalServiceUtil.searchVocabularies(
					themeDisplay.getCompanyId(), groupId, vocabularyPrefix, 0, Byte.MAX_VALUE);
			vocabularies = tmp.getBaseModels();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
%>
