<%@ include file="init.jsp"%>

<style scoped>
.fieldset {
	margin-top: 20px;
	padding: 20px;
}

.control-group {
	margin-bottom: 0 ! important;
}

legend {
	text-transform: uppercase;
	font-weight: bold;
}

.btn-bottom-unset {
	bottom: unset !important;
}
</style>

<%
	// Get the list of Web Content structures.
	List<DDMStructure> structures = DDMStructureManagerUtil.getStructures(new long[] { groupId }, journalArticleClassNameId);
%>
<liferay-portlet:actionURL portletConfiguration="<%=true%>"
	var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="<%=true%>"
	var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL.toString()%>" method="post"
	name="<portlet:namespace />fm">
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden"
			value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden"
			value="<%=configurationRenderURL%>" />

		<aui:fieldset-group>
			<!-- Basic settings. -->
			<aui:fieldset cssClass="panel panel-default">
				<legend><%=LanguageUtil.format(request, "structuredsearch.basic-settings", dummyObj)%></legend>
				<div class="row-fluid col-md-12" style="padding: 0;">
					<div class="col-md-4">
						<aui:select name="structureKey"
							label='<%=LanguageUtil.format(request, "structure", dummyObj)%>'>
							<% for (DDMStructure i : structures) { %>
							<aui:option label="<%=i.getName(locale)%>"
								value="<%=i.getStructureKey()%>"
								selected="<%=structureKey.equals(i.getStructureKey())%>">
							</aui:option>
							<% } %>
						</aui:select>
					</div>
					<div class="col-md-4">
						<aui:select name="displayStyle"
							label='<%=LanguageUtil.format(request, "structuredsearch.display-style", dummyObj)%>'>
							<aui:option label="Table style 1"
								value="<%=StructuredSearchConstants.TABLE_STYLE_1%>"
								selected="<%=displayStyle == StructuredSearchConstants.TABLE_STYLE_1%>">
							</aui:option>
							<aui:option label="Table style 2"
								value="<%=StructuredSearchConstants.TABLE_STYLE_2%>"
								selected="<%=displayStyle == StructuredSearchConstants.TABLE_STYLE_2%>">
							</aui:option>
							<aui:option label="Table style 3"
								value="<%=StructuredSearchConstants.TABLE_STYLE_3%>"
								selected="<%=displayStyle == StructuredSearchConstants.TABLE_STYLE_3%>">
							</aui:option>
							<aui:option label="Search score"
								value="<%=StructuredSearchConstants.TABLE_STYLE_4%>"
								selected="<%=displayStyle == StructuredSearchConstants.TABLE_STYLE_4%>">
							</aui:option>
						</aui:select>
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-4">
						<aui:input type="checkbox" name="showOrderInResult"
							checked="<%=showOrderInResult%>"
							label='<%=LanguageUtil.format(request, "structuredsearch.show-order-number-in-result", dummyObj)%>' />
					</div>
					<div class="col-md-4 text-right">
						<label><%=LanguageUtil.format(request, "title", dummyObj) + ":"%></label>
					</div>
					<div class="col-md-4">
						<aui:input type="text" name="preferences--orderInResultTxt--"
							value="<%=orderInResultTxt%>"
							label='' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-4">
						<aui:input type="checkbox" name="showTitleInResult"
							checked="<%=showTitleInResult%>"
							label='<%=LanguageUtil.format(request, "structuredsearch.show-title-in-result", dummyObj)%>' />
					</div>
					<div class="col-md-4 text-right">
						<label><%=LanguageUtil.format(request, "title", dummyObj) + ":"%></label>
					</div>
					<div class="col-md-4">
						<aui:input type="text" name="preferences--titleInResultTxt--"
							value="<%=titleInResultTxt%>"
							label='' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-4">
						<aui:input type="checkbox" name="showSummaryInResult"
							checked="<%=showSummaryInResult%>"
							label='<%=LanguageUtil.format(request, "structuredsearch.show-summary-in-result", dummyObj)%>' />
					</div>
					<div class="col-md-4 text-right">
						<label><%=LanguageUtil.format(request, "title", dummyObj) + ":"%></label>
					</div>
					<div class="col-md-4">
						<aui:input type="text" name="preferences--summaryInResultTxt--"
							value="<%=summaryInResultTxt%>"
							label='' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="showDownloadInResult"
							checked="<%=showDownloadInResult%>"
							label='<%=LanguageUtil.format(request, "structuredsearch.show-download-in-result", dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="searchByKeywords"
							checked="<%=searchByKeywords%>"
							label='<%=LanguageUtil.format(request, "structuredsearch.search-by-keywords", dummyObj)%>' />
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-md-12">
						<aui:input type="checkbox" name="advancedSearchByDefault"
							checked="<%=advancedSearchByDefault%>"
							label='<%=LanguageUtil.format(request, "structuredsearch.advanced-search-by-default", dummyObj)%>' />
					</div>
				</div>
			</aui:fieldset>

			<!-- Advanced search. -->
			<aui:fieldset cssClass="panel panel-default">
				<legend><%=LanguageUtil.format(request, "structuredsearch.advanced-search", dummyObj)%></legend>

				<!-- Search by structure fields (advanced search). -->
				<% if (fields != null && fields.size() > 0) { %>
				<p>
					<strong><%=LanguageUtil.format(request, "structuredsearch.search-by-structure-fields", dummyObj)%></strong>
				</p>
				<table class="table table-bordered table-striped">
					<thead class="alert alert-info">
						<tr>
							<th><strong><%=LanguageUtil.format(request, "field", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "title", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.search-type", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.show-in-search-box", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.show-in-result", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.display-sequence", dummyObj)%></strong></th>
						</tr>
					</thead>
					<tbody>
						<!-- Display search options basing on DDM field type. -->
						<% for (DDMFormField field : fields) { %>
						<%
									String ddmType = field.getType();
								
									String prefix = "structure" + structureKey + field.getName();
									
					 				boolean searchable = false;
					 				String title = GetterUtil.getString(portletPreferences.getValue(prefix + "title", StringPool.BLANK), StringPool.BLANK);
					 				boolean selected = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "selected", "false"), false);
					 				int searchType = GetterUtil.getInteger(portletPreferences.getValue(prefix + "type", "1"), 1);
					 				int sequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "sequence", "0"), 0);
					 				boolean showInResult = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "showInResult", "false"), false);
					 			%>
						<tr>
							<td><%=field.getLabel().getString(locale)%></td>
							<td valign="middle">
								<aui:input type="text"
									label="<%=StringPool.BLANK%>" value="<%=title%>"
									name='<%="preferences--" + prefix + "title--"%>' />
							</td>
							<td>
								<% if (ddmType.equals("text") || ddmType.equals("textarea") || ddmType.equals("ddm-text-html")) { %>
								<% searchable = true;%> <%=LanguageUtil.format(request, "structuredsearch.the-search-type-depends-on-indexing-type", dummyObj)%>
								<% } else if (ddmType.equals("ddm-date")) { %> <% searchable = true;%>
								<aui:select name='<%="preferences--" + prefix + "type--"%>'
									label="<%=StringPool.BLANK%>">
									<aui:option
										value='<%=StructuredSearchConstants.SEARCH_TYPE_EQUAL%>'
										label='<%=LanguageUtil.format(request, "structuredsearch.search-equal", dummyObj)%>'
										selected='<%=searchType == StructuredSearchConstants.SEARCH_TYPE_EQUAL%>'>
									</aui:option>
									<aui:option
										value='<%=StructuredSearchConstants.SEARCH_TYPE_RANGE%>'
										label='<%=LanguageUtil.format(request, "structuredsearch.search-range", dummyObj)%>'
										selected='<%=searchType == StructuredSearchConstants.SEARCH_TYPE_RANGE%>'>
									</aui:option>
								</aui:select> <% } else { %> <%=LanguageUtil.format(request, "structuredsearch.this-ddm-type-is-unsupported", dummyObj)%>
								<% } %>
							</td>
							<td valign="middle">
								<% if (searchable) { %> <aui:input type="checkbox"
									label="<%=StringPool.BLANK%>" checked="<%=selected%>"
									name='<%="preferences--" + prefix + "selected--"%>' /> <% } %>
							</td>
							<td valign="middle">
								<% if (searchable) { %> <aui:input type="checkbox"
									label="<%=StringPool.BLANK%>" checked="<%=showInResult%>"
									name='<%="preferences--" + prefix + "showInResult--"%>' /> <% } %>
							</td>
							<td valign="middle">
								<% if (searchable) { %> <aui:input type="number"
									label="<%=StringPool.BLANK%>"
									value="<%=sequence > 0 ? String.valueOf(sequence) : StringPool.BLANK%>"
									name='<%="preferences--" + prefix + "sequence--"%>' /> <% } %>
							</td>
						</tr>
						<% } %>
					</tbody>
				</table>
				<% } %>

				<!-- Search by vocabularies. -->
				<% if (structureKey != null && structureKey.length() != 0) { %>
				<% if (vocabularies != null && vocabularies.size() != 0) { %>
				<p>
					<strong><%=LanguageUtil.format(request, "structuredsearch.search-by-vocabularies", dummyObj)%></strong>
				</p>
				<table class="table table-bordered table-striped">
					<thead class="alert alert-info">
						<tr>
							<th><strong><%=LanguageUtil.format(request, "vocabulary", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "title", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.show-in-search-box", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.show-in-result", dummyObj)%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "structuredsearch.display-sequence", dummyObj)%></strong></th>
						</tr>
					</thead>
					<tbody>
						<% for (AssetVocabulary vocabulary : vocabularies) { %>
						<%
										String prefix = "vocabulary" + vocabulary.getVocabularyId();
						 				
										String title = GetterUtil.getString(portletPreferences.getValue(prefix + "title", StringPool.BLANK), StringPool.BLANK);
						 				boolean selected = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "selected", "false"), false);
						 				int searchType = GetterUtil.getInteger(portletPreferences.getValue(prefix + "type", "1"), 1);
						 				int sequence = GetterUtil.getInteger(portletPreferences.getValue(prefix + "sequence", "-1"), -1);
						 				boolean showInResult = GetterUtil.getBoolean(portletPreferences.getValue(prefix + "showInResult", "false"), false);
									%>
						<tr>
							<td><%=vocabulary.getName().substring(vocabularyPrefix.length())%>
							</td>
							<td valign="middle">
								<aui:input type="text"
									label="<%=StringPool.BLANK%>" value="<%=title%>"
									name='<%="preferences--" + prefix + "title--"%>' />
							</td>
							<td valign="middle"><aui:input type="checkbox"
									label="<%=StringPool.BLANK%>" checked="<%=selected%>"
									name='<%="preferences--" + prefix + "selected--"%>' /></td>
							<td valign="middle"><aui:input type="checkbox"
									label="<%=StringPool.BLANK%>" checked="<%=showInResult%>"
									name='<%="preferences--" + prefix + "showInResult--"%>' /></td>
							<td valign="middle"><aui:input type="number"
									label="<%=StringPool.BLANK%>"
									value="<%=sequence > 0 ? String.valueOf(sequence) : StringPool.BLANK%>"
									name='<%="preferences--" + prefix + "sequence--"%>' /></td>
						</tr>
						<% } %>
					</tbody>
				</table>
				<% } %>
				<% } %>
			</aui:fieldset>
		</aui:fieldset-group>

		<!-- Form actions -->
		<aui:button-row cssClass="btn-bottom-unset">
			<aui:button type="submit"
				cssClass="btn btn-lg btn-primary btn-default " />
		</aui:button-row>
	</div>
</aui:form>