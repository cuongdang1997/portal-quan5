package com.swt.questionanswer.form.util;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.dao.search.DisplayTerms;

public class StructuredSearchDisplayTerms extends DisplayTerms {

	public StructuredSearchDisplayTerms(PortletRequest portletRequest) {
		super(portletRequest);
	}
	
}
