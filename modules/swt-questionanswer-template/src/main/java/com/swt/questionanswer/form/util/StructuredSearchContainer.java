package com.swt.questionanswer.form.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.dao.search.SearchContainer;

public class StructuredSearchContainer extends SearchContainer<JournalArticle> {

	static List<String> headerNames = new ArrayList<>();
	static Map<String, String> orderableHeaders = new HashMap<String, String>();

	static {
		headerNames.add("status");
		headerNames.add("alias");
	}

	public static final String EMPTY_RESULTS_MESSAGE = "there-are-no-results";

	public StructuredSearchContainer(PortletRequest portletRequest, PortletURL iteratorURL) {
		super(portletRequest, new StructuredSearchDisplayTerms(portletRequest),
				new StructuredSearchTerms(portletRequest), DEFAULT_CUR_PARAM,DEFAULT_DELTA, iteratorURL,
				headerNames, EMPTY_RESULTS_MESSAGE);
	}

}
