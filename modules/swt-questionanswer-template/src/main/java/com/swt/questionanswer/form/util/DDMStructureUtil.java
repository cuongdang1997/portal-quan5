package com.swt.questionanswer.form.util;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

public class DDMStructureUtil {

	public static List<DDMFormField> getDDMFormFieldByIdStructure(String idStructure) {
		DDMStructure structure = null;
		try {
			structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(idStructure));
		} catch (PortalException e) {
		}

		// Get the DDM structure's fields.
		List<DDMFormField> fields = new ArrayList<DDMFormField>();
		if (structure != null) {
			fields = structure.getDDMFormFields(false);
		}
		return fields;
	}

	public static List<JournalFolder> getJournalFolder(long groupId, long idCompany) {

		List<JournalFolder> resultList = new ArrayList<JournalFolder>();
		JournalFolderLocalServiceUtil.getFolders(groupId);
		List<JournalFolder> fields = JournalFolderLocalServiceUtil.getFolders(groupId);
		if (fields.size() != 0) {
			for (JournalFolder object : fields) {
				if (object.getCompanyId() == idCompany) {
					resultList.add(object);
				}
			}
		}

		return resultList;
	}

	public static List<AssetCategory> getListCategoryByIdVocabulary(ThemeDisplay themeDisplay, String vocabularyId) throws PortalException {
		List<AssetCategory> listCate = new ArrayList<AssetCategory>();
		DynamicQuery dynamic = DynamicQueryFactoryUtil.forClass(AssetCategory.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(themeDisplay.getCompanyId()))
				.add(PropertyFactoryUtil.forName("vocabularyId").eq(GetterUtil.getLong(vocabularyId)));
		listCate = AssetCategoryLocalServiceUtil.dynamicQuery(dynamic);
		List<AssetCategoryProperty> listAssetCategoryProperty = new ArrayList<AssetCategoryProperty>();
		if(listCate.size()!=0) {
			for(AssetCategory object : listCate) {
				AssetCategoryProperty categoryProperty = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(object.getCategoryId(), "status");
				if(categoryProperty.getValue().equals("1")) {
					listAssetCategoryProperty.add(categoryProperty);
				}
			}
			
		}
		List<AssetCategory> listCateReturn = new ArrayList<AssetCategory>();
		if(listAssetCategoryProperty.size()!=0) {
			for(AssetCategoryProperty objectPoperty : listAssetCategoryProperty) {
				AssetCategory objectLast = AssetCategoryLocalServiceUtil.getAssetCategory(objectPoperty.getCategoryId());
				listCateReturn.add(objectLast);
			}
		}
		return listCateReturn;

	}

	public static List<JournalArticle> getListFeedbackByArticleCurrent(ThemeDisplay themeDisplay, String structureKey,
			JournalArticle articleByUrlCurrent) throws PortalException {
		// get list article by structure key
		List<JournalArticle> listArticleByStructure = JournalArticleLocalServiceUtil
				.getStructureArticles(themeDisplay.getScopeGroupId(), structureKey);
		List<JournalArticle> listLastVersion = new ArrayList<JournalArticle>();
		for(JournalArticle o : listArticleByStructure) {
		 if(JournalArticleLocalServiceUtil.isLatestVersion(themeDisplay.getScopeGroupId(), o.getArticleId(), o.getVersion())) {
			 listLastVersion.add(o);
		 }
		}
		
		// idArticle current
		long articleIdCurrent = GetterUtil.getLong(articleByUrlCurrent.getArticleId());
		// list article result
		List<JournalArticle> listArticle = new ArrayList<JournalArticle>();
		long idArticleParent = 0;
		if (listArticleByStructure.size() != 0) {
			for (JournalArticle article : listLastVersion) {
				Document articleDoc = null;
				try {
					String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
					articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				Element root = articleDoc.getRootElement();
				for (Element element : root.elements()) {
					// filter with field idArticleParent
					if (element.attributeValue("name").equals("idArticleParent")) {
						idArticleParent = GetterUtil.getLong(element.element("dynamic-content").getText());
					}

				}
				// get article with idArticle current
				if (idArticleParent == articleIdCurrent) {
					listArticle.add(article);
				}
			}
		}

		return listArticle;

	}
}
