package com.swt.questionanswer.form.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.util.DDMIndexer;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

public class JournalArticleUtil {
	@Reference
	private static LogService _log;


	public static List<JournalArticle> getBySearch(StructuredSearchContainer searchContainer, ThemeDisplay themeDisplay,
			String structureForm, String listFieldSearch, String vocabulary, long categoryId,
			PortletRequest portletRequest, HttpServletRequest request) throws Exception {
		
		StructuredSearchDisplayTerms searchTerms = (StructuredSearchDisplayTerms) searchContainer.getSearchTerms();
		
		long companyId = themeDisplay.getCompanyId();
		long groupId = themeDisplay.getScopeGroupId();
		List<JournalArticle> articlesLast = new ArrayList<JournalArticle>();
		DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureForm));
		List<DDMFormField> listFile = DDMStructureUtil.getDDMFormFieldByIdStructure(structureForm);
		long id = GetterUtil.getLong(portletRequest.getParameter("categoryId"));
		SearchContext searchContext = SearchContextFactory.getInstance(request);
		
		//SearchContext searchContext =new SearchContext();
		BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);
		 searchQuery.addRequiredTerm(Field.ENTRY_CLASS_NAME, JournalArticle.class.getName());
		 searchQuery.addRequiredTerm("ddmStructureKey", structure.getStructureKey());
		 searchQuery.addRequiredTerm(Field.COMPANY_ID,companyId);
		 searchQuery.addRequiredTerm(Field.GROUP_ID,groupId);
		 //searchQuery.addExactTerm(field, value)
		 String keywords =  searchTerms.getKeywords();
		BooleanClause fieldsBoolClause = BooleanClauseFactoryUtil.create(searchContext, searchQuery,
					BooleanClauseOccur.MUST.getName());
		 BooleanClause ddmStructureKeyClause = BooleanClauseFactoryUtil.create(searchContext, "ddmStructureKey", structure.getStructureKey(),
					BooleanClauseOccur.MUST.getName());
		 searchContext.setBooleanClauses(new BooleanClause[] { fieldsBoolClause, ddmStructureKeyClause });
		if (id != 0) {
			categoryId = id;
		}
		if (Validator.isNotNull(keywords)) {
			if (listFieldSearch != "") {
				String fieldDataType = "";
				String fieldSelectedSearch = portletRequest.getParameter("timTheo");
				if(fieldSelectedSearch.equals("searchTitle")) {
					  searchQuery.addExactTerm(Field.TITLE, keywords);
					
				}else {
					
					for (DDMFormField field : listFile) {
						if (field.getName().equals(fieldSelectedSearch)) {
							fieldDataType = field.getIndexType();
						}
					}
					String fieldEncodedName = encodeName(structure.getStructureId(), fieldSelectedSearch,
							themeDisplay.getLocale(), fieldDataType);
					searchQuery.addRequiredTerm(fieldEncodedName, keywords, true);
				}
			} else {
				searchQuery.addRequiredTerm(Field.CONTENT, keywords, true);
			}
		}
		if (vocabulary != "") {
			String fieldSearchVal = portletRequest.getParameter("vocabulary_" + vocabulary);

			if (GetterUtil.getLong(fieldSearchVal) != 0) {
				long[] cateIds = {GetterUtil.getLong(fieldSearchVal)};
				searchContext.setAssetCategoryIds(cateIds);
			}
		}
		searchContext.setKeywords(keywords);
		searchContext.setCompanyId(companyId);
		searchContext.setLocale(themeDisplay.getLocale());
		searchContext.setStart(searchContainer.getStart());
		searchContext.setEnd(searchContainer.getEnd());
		searchContext.setAttribute("paginationType", "more");
		Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);
		Hits hits = indexer.search(searchContext);
		//searchContext.
		List<Document> documents =  hits.toList();
		List<JournalArticle> journalArticles = new ArrayList<JournalArticle>();
		for (Document d : documents) {
			JournalArticle journalArticle = null;
			try {
				journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(groupId,
						GetterUtil.getString(d.get(Field.ARTICLE_ID)));
				
			} catch (PortalException e) {
				continue;
			}

			journalArticles.add(journalArticle);
		}
		
		// set display Date to show article
		List<JournalArticle> journalLast = new ArrayList<JournalArticle>();
		for(JournalArticle o : journalArticles) {
			if(Validator.isNotNull(o.getDisplayDate())&& o.getStatus()==0) {
				journalLast.add(o);
				
			}
		}
		return journalLast;

	}

	private static String encodeName(long ddmStructureId, String fieldName, Locale locale, String indexType) {
		StringBundler sb = new StringBundler(8);

		sb.append(DDMIndexer.DDM_FIELD_PREFIX);

		if (Validator.isNotNull(indexType)) {
			sb.append(indexType);
			sb.append(DDMIndexer.DDM_FIELD_SEPARATOR);
		}

		sb.append(ddmStructureId);
		sb.append(DDMIndexer.DDM_FIELD_SEPARATOR);
		sb.append(fieldName);

		if (locale != null) {
			sb.append(StringPool.UNDERLINE);
			sb.append(LocaleUtil.toLanguageId(locale));
		}

		return sb.toString();
	}
	
}
