package swt.questionanswer.template.portlet.constants;

/**
 * @author Nhut.Dang
 */
public class SwtTemplatePortletKeys {

	public static final String SwtTemplatePortlet = "SWTFormTemplate";
	public static final String SwtTemplatePortlet_DisplayName = "HCMGov Form Template";
}