package swt.questionanswer.template.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "platform")
@Meta.OCD(factory = true, id = "swt.questionanswer.template.portlet.SwtTemplateConfiguration", localization = "content/Language")
public interface SwtTemplateConfiguration {
	// structure
	@Meta.AD(deflt = "", required = false)
	public String structureForm();

	// list field check to search
	@Meta.AD(deflt = "", required = false)
	public String listFieldSearch();

	// return field search type select or text
	@Meta.AD(deflt = "", required = false)
	public String displayStyle();

	// seach vocab
	@Meta.AD(deflt = "", required = false)
	public String vocabulary();

	// Search Title
	@Meta.AD(deflt = "true", required = false)
	public boolean searchTitle();
}
