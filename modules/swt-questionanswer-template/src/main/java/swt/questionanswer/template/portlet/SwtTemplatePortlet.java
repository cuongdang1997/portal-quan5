package swt.questionanswer.template.portlet;

import swt.questionanswer.template.portlet.constants.SwtTemplatePortletKeys;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

/**
 * @author Nhut.Dang
 */
@Component(configurationPid = "swt.questionanswer.template.portlet.SwtTemplateConfiguration", immediate = true, property = {
		"com.liferay.portlet.display-category=category.portal",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=" + SwtTemplatePortletKeys.SwtTemplatePortlet_DisplayName,
		"javax.portlet.init-param.config-template=/configuration.jsp",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + SwtTemplatePortletKeys.SwtTemplatePortlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class SwtTemplatePortlet extends MVCPortlet {
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		renderRequest.setAttribute(SwtTemplateConfiguration.class.getName(), formConfiguration);

		super.doView(renderRequest, renderResponse);
	}
	
	public String getStructureForm(Map labels) {
		return (String) labels.get(formConfiguration.structureForm());
	}

	public String getListFieldSearch(Map labels) {
		return (String) labels.get(formConfiguration.listFieldSearch());
	}
	public String getTypeSelectFieldSearch(Map labels) {
		return  (String) labels.get(formConfiguration.displayStyle());
	}

	public String getVocabulary(Map labels) {
		return (String) labels.get(formConfiguration.vocabulary());
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		formConfiguration = ConfigurableUtil.createConfigurable(SwtTemplateConfiguration.class, properties);
	}

	private volatile SwtTemplateConfiguration formConfiguration;
}