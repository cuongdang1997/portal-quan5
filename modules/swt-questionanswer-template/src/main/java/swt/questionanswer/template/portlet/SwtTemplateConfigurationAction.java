package swt.questionanswer.template.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import swt.questionanswer.template.portlet.constants.SwtTemplatePortletKeys;

@Component(configurationPid = "swt.questionanswer.template.portlet.SwtTemplateConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true, property = {
		"javax.portlet.name=" + SwtTemplatePortletKeys.SwtTemplatePortlet }, service = ConfigurationAction.class)
public class SwtTemplateConfigurationAction extends DefaultConfigurationAction {
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {

		String structureForm = ParamUtil.getString(actionRequest, "structureForm");
		setPreference(actionRequest, "structureForm", structureForm);

		String listFieldSearch = ParamUtil.getString(actionRequest, "listFieldSearch");
		setPreference(actionRequest, "listFieldSearch", listFieldSearch);

		String displayStyle = ParamUtil.getString(actionRequest, "displayStyle");
		setPreference(actionRequest, "displayStyle", displayStyle);

		String vocabulary = ParamUtil.getString(actionRequest, "vocabulary");
		setPreference(actionRequest, "vocabulary", vocabulary);

		String searchTitle = ParamUtil.getString(actionRequest, "searchTitle");
		setPreference(actionRequest, "searchTitle", searchTitle);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {

		httpServletRequest.setAttribute(SwtTemplateConfiguration.class.getName(), _formConfiguration);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_formConfiguration = ConfigurableUtil.createConfigurable(SwtTemplateConfiguration.class, properties);
	}

	private volatile SwtTemplateConfiguration _formConfiguration;

}
