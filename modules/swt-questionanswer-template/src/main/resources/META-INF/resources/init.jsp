<%@page import="java.util.HashMap"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="swt.questionanswer.template.portlet.SwtTemplateConfiguration"%>
<%@page import="com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="com.liferay.journal.model.JournalFolder"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMStructure"%>
<%@page import="com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@taglib
	uri="http://liferay.com/tld/theme" prefix="liferay-theme"%><%@
taglib
	uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@page import="javax.portlet.PortletPreferences"%>
<liferay-theme:defineObjects />

<portlet:defineObjects />
<%
HashMap<String, Integer> listMap = new HashMap<String, Integer>();
List<DDMStructure> listStructure  =DDMStructureLocalServiceUtil.getStructures();
List<AssetVocabulary> listVocabulary = AssetVocabularyLocalServiceUtil.getCompanyVocabularies(themeDisplay.getCompanyId());
SwtTemplateConfiguration formConfiguration = (SwtTemplateConfiguration) renderRequest
			.getAttribute(SwtTemplateConfiguration.class.getName());
	String structureForm = "";
	String listFieldSearch ="";
	String vocabulary ="";
	String displayStyle = ""; 
	boolean searchTitle = true;
	
	 if (Validator.isNotNull(formConfiguration)) {
		structureForm = portletPreferences.getValue("structureForm", formConfiguration.structureForm());
		listFieldSearch = portletPreferences.getValue("listFieldSearch", formConfiguration.listFieldSearch());
		displayStyle = portletPreferences.getValue("displayStyle", formConfiguration.displayStyle());
		vocabulary = portletPreferences.getValue("vocabulary", formConfiguration.vocabulary());
		searchTitle = GetterUtil.getBoolean(portletPreferences.getValue("searchTitle", String.valueOf(formConfiguration.searchTitle())));
	}
	String linkDeatailDefault = themeDisplay.getURLCurrent();

if (linkDeatailDefault.indexOf("-/") > 0) {
	linkDeatailDefault = linkDeatailDefault.substring(0,
			linkDeatailDefault.indexOf("-/"));
}
	
%>