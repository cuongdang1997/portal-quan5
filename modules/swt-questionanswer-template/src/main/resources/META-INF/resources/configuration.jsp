<%@page import="java.util.HashMap"%>
<%@page import="com.swt.questionanswer.form.util.DDMStructureUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMFormField"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="init.jsp"%>

<style>
	.css-config {
		margin-top: 20px;
		padding: 15px;
	}
	.div-footer-form{
		padding: 5px;
	    margin-left: 8px;
	}
</style>
<liferay-portlet:actionURL portletConfiguration="<%=true%>"
	var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%=true%>"
	var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" id="fm"
	name="fm" class="form">
	<div class="container-fluid-1280">
		<aui:input name="<%=Constants.CMD%>" type="hidden"
			value="<%=Constants.UPDATE%>" />
		<aui:input name="redirect" type="hidden"
			value="<%=configurationRenderURL%>" />
		<aui:fieldset cssClass="panel panel-default css-config">
			<%=LanguageUtil.format(request, "chon-cau-truc-form", new Object())%>
			<aui:select id="structureForm" name="structureForm" label=""
				value="<%=structureForm%>">
				<aui:option value="">
				 ---<%=LanguageUtil.format(request, "chon-cau-truc-form", new Object())%>---
				 </aui:option> ---
				<%
					if (listStructure.size() != 0) {

									for (DDMStructure object : listStructure) {
										if (object.getCompanyId() == themeDisplay.getCompanyId()
												&& object.getUserName() != "") {
				%>

				<aui:option value="<%=String.valueOf(object.getStructureId())%>"><%=object.getName(themeDisplay.getLocale())%></aui:option>
				<%
					}
									}

								}
				%>
			</aui:select>
			<%=LanguageUtil.format(request, "chon-kieu-hien-thi-form", new Object())%>
			<aui:select id="displayStyle" name="displayStyle" label=""
				value="<%=displayStyle%>">
				<aui:option value="0"><%=LanguageUtil.format(request, "hien.thi.kieu.tim.kiem", new Object())%></aui:option>
				<aui:option value="1"><%=LanguageUtil.format(request, "hien.thi.kieu.danh.sach.bai.viet", new Object())%></aui:option>
			</aui:select>
			<%
				if (structureForm != "") {
							List<DDMFormField> listFile = DDMStructureUtil.getDDMFormFieldByIdStructure(structureForm);
			%>
				<%=LanguageUtil.format(request, "tim-theo", new Object())%>
				<table class="table table-bordered table-striped">
					<thead class="alert alert-info">
						<tr>
							<th><strong><%=LanguageUtil.format(request, "truong-search", new Object())%></strong></th>
							<th><strong><%=LanguageUtil.format(request, "show-in-search-box", new Object())%></strong></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><%=LanguageUtil.format(request, "tieu.de", new Object())%></td>
							<td>
							<aui:input type="checkbox" name="searchTitle"
							checked="<%=searchTitle%>" label=""/>
							</td>
						</tr>
						<%
							for (DDMFormField field : listFile) {
												if (!field.getType().equals("ddm-documentlibrary")
														&& !field.getType().contains("select")) {
						%>
						
						<tr>
							<td><%=field.getLabel().getString(locale)%></td>
							<td><input
								<%if (String.valueOf(listFieldSearch).contains(String.valueOf(field.getName()))) {%>
								checked="checked" <%}%> name="nameSelected"
								value="<%=field.getName()%>" type="checkbox"></td>
						</tr>

						<%
							}
											}
						%>
					</tbody>
				</table>
			<%
				}
			%>
				<%=LanguageUtil.format(request, "chon-linh-vuc", new Object())%>
				<aui:select id="vocabulary" name="vocabulary" label=""
					value="<%=vocabulary%>">
					<aui:option value="">
				 ---<%=LanguageUtil.format(request, "chon-linh-vuc", new Object())%>---
				 </aui:option>
					<%
						if (listVocabulary.size() != 0) {
											for (AssetVocabulary object : listVocabulary) {
					%>
					<aui:option value="<%=String.valueOf(object.getVocabularyId())%>"><%=object.getName()%></aui:option>
					<%
						}
										}
					%>
				</aui:select>
			</aui:fieldset>
			<aui:input type="hidden" id="listFieldSearch" name="listFieldSearch"
				value="<%=listFieldSearch%>">
			</aui:input>
		
	</div>
		<div class="div-footer-form">
		<%
			if (structureForm != "") {
		%>
		<button type="button" onClick="saveConfig()" class="btn btn-lg btn-primary btn-default">
			<span class="lfr-btn-label"><%=LanguageUtil.format(request, "config.OK", new Object())%></span>
			</button>
		<%
			} else {
		%>
			<button type="submit" class="btn btn-lg btn-primary btn-default">
			<span class="lfr-btn-label"><%=LanguageUtil.format(request, "config.OK", new Object())%></span>
			</button>
		<%
			}
		%>
		</div>
	
</aui:form>
<script>
	function saveConfig() {
		var listFieldSearch = "";
		var listTypeSearchOption = "";
		var checkboxes = document.getElementsByName("nameSelected");
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].checked) {
				listFieldSearch += checkboxes[i].value + "/";
			}
		}
		$("#<portlet:namespace/>listFieldSearch").val(listFieldSearch);
		$("#<portlet:namespace/>fm").submit();

	}
</script>