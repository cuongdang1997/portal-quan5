<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@page import="com.swt.questionanswer.form.util.StructuredSearchDisplayTerms"%>
<%@page import="com.swt.questionanswer.form.util.StructuredSearchContainer"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="java.io.StringReader"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.asset.kernel.model.AssetEntry"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.swt.questionanswer.form.util.JournalArticleUtil"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.journal.model.JournalArticle"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="com.swt.questionanswer.form.util.DDMStructureUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMStructure"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.dynamic.data.mapping.model.DDMFormField"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>

<%@ include file="init.jsp"%>

<style scoped="scoped">
	.table {
		border-color: none;
	}
	
	.table-bordered {
		border: 0px;
	}
	
	.table-bordered>tbody>tr>td {
		border: 0px;
	}
	
	.table-bordered>tbody>tr>td:hover {
		background-color: white;
	}
	
	.table-cell {
		padding-left: 0px !important;
		padding-right: 0px !important;
	}
	
	.table-question-list {
		float: left;
		width: 100%;
	}
	
	.lfr-meta-actions {
		padding-top: 0px !important;
	}
	
	.main-content-body {
		margin-top: 0px !important;
	}
	
	.total-question-answer {
		color: #d70000 !important;
		font-size: 14px;
		font-style: normal;
		font-weight: bold;
		font-family: Arial, Helvetica, sans-serif;
		TEXT-DECORATION: none;
	}
	.button-question {
		padding: 6px;
		height: 30px;
		width: 81px !important;
		text-shadow: none;
		font-weight: 700;
		border-radius: unset;
		font-size: 13px !important;
		color: white;
	    border: none;
	    background-color: #1570ad;
		margin-top: 14px;
	}
	.sidenav-content {
		display: none;
	}
	
	.table-question-answer {
		font-size: 14px;
		margin-right: 0px;
		margin-left: 0px;
		width: 100%;
		margin-top: 4%;
	}
	
	.table-question-answer tr td {
		border-top: none !important;
	}
	
	.tr-title-question {
		font-weight: bold;
		background-color: #deefff;
		color: #0b5090;
		border-bottom: 1px #e1e1e1 solid !important;
		width: 100%;
		float: left;
	}
	
	.tr-title-question a {
		color: #0b5090 !important;
	}
	
	.icon-img {
		padding-right: 5px;
	}
	
	.tr-date-request {
		font-weight: bold;
		color: #444;
		background-color: #f0f7ff;
	}
	
	.tr-date-request td {
		width: 50%;
		float: left;
	}
	
	.question-content {
		padding-left: 15px !important;
	}
	
	.title-information {
		padding-left: 10px;
		padding-top: 10px;
		color: black;
		font-size: 14px;
		font-style: normal;
		font-weight: bold;
		font-family: Arial, Helvetica, sans-serif;
		TEXT-DECORATION: none;
		text-transform: uppercase;
	}
	
	.button-back {
		margin-top: 20px;
		margin-bottom: 20px;
		margin-left: 20px;
	}
	
	.btn-search-form {
		padding: 4px;
		width: 80px;
		text-shadow: none;
		font-weight: 700;
		border-radius: unset;
		height: 30px;
		font-size: 14px;
		color: white;
	    border: none;
	    background-color: #085791;
	}
	
	.table-question-answer-tr {
		width: 100%;
		float: left;
		border-top: 1px #a6d4ff solid !important
	}
	
	.table-question-answer-tr:first-child {
		border-top: none;
	}
	
	.table-question-answer-tr:hover {
		background-color: #ffffcc !important;
	}
	
	.table-question-answer-tr:hover .tr-title-question {
		background-color: #ffffcc !important;
	}
	
	.table-question-answer-tr:hover .tr-date-request {
		background-color: #ffffcc !important;
	}
	
	.table-hover>tbody>tr:hover {
		background: none;
	}
	
	.table .table {
		background: none;
	}
	
	.table>tbody>tr>td {
		border-top: none;
	}
	
	.table-cell {
		padding-bottom: 0px !important;
		padding-top: 0px !important;
	}
	
	.table-feedback-list {
		width: 100%;
		float: left;
	}
	
	.table-feedback-list .name-user-send {
		width: 50%;
		float: left;
	}
	
	.table-feedback-list .date-send {
		width: 50%;
		float: left;
	}
	
	.table-feedback-list .content {
		width: 100%;
		float: left;
	}
	
	.title-list-feedback {
		text-transform: uppercase;
		width: 100%;
		background: -webkit-linear-gradient(#144673, #3982ad);
		color: white;
		font-size: 14px;
		font-style: normal;
		font-weight: bold;
		font-family: Arial, Helvetica, sans-serif;
		padding: 8px;
		height: 30px;
	}
	
	@media only screen and (min-width: 768px) {
		.button-search-question-answer {
			margin-top: 0px;
			padding: 0px;
		}
	}
	@media only screen and (max-width: 767px){

		.taglib-page-iterator {
			padding: 0 6px;
		}
		.taglib-page-iterator .lfr-pagination-buttons{
			display: flex;
		}
		.taglib-page-iterator .lfr-pagination-buttons>li>a{
			width: 99%;
			border-radius: 4px;
		}
		.taglib-page-iterator .lfr-pagination-config .current-page-menu .btn{
			margin: 0 0 5px 0;
		}
	}
</style>

<%
	if (structureForm != "") {
		DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(GetterUtil.getLong(structureForm));
%>
<!-- Display structure search -->
<%if(displayStyle.equals("0")){ 
	PortletURL portletURL = renderResponse.createRenderURL();
	StructuredSearchContainer structuredSearchContainer = new StructuredSearchContainer(renderRequest,
			portletURL);
	StructuredSearchDisplayTerms searchTerms = (StructuredSearchDisplayTerms)structuredSearchContainer.getSearchTerms();
	HttpServletRequest httpReq = PortalUtil
			.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
	//Get parameters in URL
	long categoryId = GetterUtil.getLong(httpReq.getParameter("categoryId"));
	
	if(categoryId == 0){
		categoryId = GetterUtil.getLong(request.getParameter("categoryId"));
	}
	String timTheoQuestion  = httpReq.getParameter("timTheo");
	try {
		
	} catch (Exception e) {
	}
	List<DDMFormField> listFile = DDMStructureUtil.getDDMFormFieldByIdStructure(structureForm);
	List<JournalArticle> articlesLast = JournalArticleUtil.getBySearch(structuredSearchContainer,themeDisplay, structureForm,listFieldSearch,vocabulary,
			categoryId ,renderRequest,request);
%>

<div id="swt-questionanswer-template">
<div class="row row-swt-responsive">
		<aui:form action="<%=portletURL.toString()%>" method="post" name="fm" cssClass="navbar-search search-form">
			<aui:input type="hidden" value="<%=categoryId %>" name="categoryId" />
			<aui:input type="hidden" value="<%=searchTitle %>" name="searchTitle" />
			<div class="col-sm-10 col-xs-12 col-swt">
			<div class="form-group">
				<div class="row-fluid">
					<div class=" col-sm-3">
						<label class="control-label"
						style="font-size: 14px; padding-right: 0px; margin-top: 6px"><%=LanguageUtil.format(request, "noi-dung-tim-kiem", new Object())%>:</label>
					</div>
					<div class="col-sm-9 col-xs-12">
						<aui:input name="keywords" label="" type="text" inputCssClass="admin-inputext" class="form-control"
							placeholder="<%=LanguageUtil.format(request, "vui-long-nhap-tu-khoa-tim-kiem", new Object())%>"
							style="height: 30px !important; padding-left: 2%; border-radius: unset; border-color: #085791; font-size: 14px;"/>
	
					</div>
				</div>
			</div>
			<%
				if (listFieldSearch != "") {
			%>
			<div class="form-group">
				<div class="row-fluid">
					<div class="col-sm-3">
						<label class="control-label" style="font-size: 14px; margin-top: 6px;"><%=LanguageUtil.format(request, "tim-theo", new Object())%>:</label>
					</div>
					<div class="col-sm-9 col-xs-12">
						<aui:select class="form-control" id="timTheo" label="" name="timTheo"
							style="padding: 0px; height: 30px; border-radius: unset; border-color: #085791; font-size: 14px;">
						 <% if(searchTitle==true){ %>
						 <aui:option value="searchTitle"><%=LanguageUtil.format(request, "tieu.de", new Object())%></aui:option>
						 <%} %>
	
							<%
								String[] listSelected = listFieldSearch.split("/");
										for (String s : listSelected) {
											for (DDMFormField field : listFile) {
												if (field.getName().equals(s)) {
							%>
							<aui:option
								 value="<%=field.getName()%>"><%=field.getLabel().getString(themeDisplay.getLocale())%></aui:option>
							<%
								}
											}
										}
							%>
						</aui:select>
					</div>
				</div>
			</div>
			<%
				}
			%>
			<%
				if (vocabulary != "") {
					List<AssetCategory> listField = DDMStructureUtil.getListCategoryByIdVocabulary(themeDisplay, vocabulary);
			%>
			<div class="form-group">
				<div class="row-fluid">
					<div class="col-sm-3">
						<label class="control-label" style="font-size: 14px; margin-top: 6px;"><%=LanguageUtil.format(request, "linh-vuc", new Object())%>:</label>
					</div>
					<div class="col-sm-9 col-xs-12">
						<aui:select class="form-control" name='<%="vocabulary_" + vocabulary%>' label=""
							style="padding: 0px; height: 30px; border-radius: unset; border-color: #085791; font-size: 14px;">
							<aui:option value="0"><%=LanguageUtil.format(request, "toan-bo", new Object())%></aui:option>
							<%
								if (listField.size() != 0) {
									for (AssetCategory objectField : listField) {
							%>
							<aui:option selected="<%=objectField.getCategoryId() == categoryId %>"
								value="<%=objectField.getCategoryId()%>">
								<%=objectField.getName()%>
							</aui:option>
							<%
								}
										}
							%>
						</aui:select>
					</div>
				</div>
			</div>
			<%
				}
			%>
			</div>
			<div class="col-sm-2 col-xs-12 button-search-question-answer">
			<button type="submit" class="btn-search-form btn btn-success">
				<%=LanguageUtil.format(request, "tim-kiem-button", new Object())%>
			</button>
			</div> 
			<div class="col-sm-2 col-xs-5 button-search-question-answer">
			<div type="button" class="btn btn-info button-question"
				onclick="form_question()">
				<%=LanguageUtil.format(request, "dat-cau-hoi", new Object())%>
			</div>
			</div>
		</aui:form>
	</div>
	<div class="row row-swt-responsive">
		<div class="col-sm-8 col-md-8 col-lg-8 col-xs-7 total-question-answer">
			<%=LanguageUtil.format(request, "tong-so-cau-hoi", new Object())%>:
			<%=articlesLast.size()%>
		</div>
	</div>

<liferay-ui:search-container searchContainer="<%=structuredSearchContainer%>" delta="1" deltaConfigurable="true"
	emptyResultsMessage="there-are-no-results">
	 <liferay-ui:search-container-results>
		<%
			results = articlesLast;
			ListUtil.subList(articlesLast, structuredSearchContainer.getStart(), structuredSearchContainer.getEnd());
			total = articlesLast.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
			structuredSearchContainer.setTotal(total);
			structuredSearchContainer.setResults(articlesLast);
		%>
	</liferay-ui:search-container-results> 
	<liferay-ui:search-container-row
		className="com.liferay.journal.model.JournalArticle"
		modelVar="article" indexVar="index" keyProperty="id">
		<%
			// Create the URL for detail view. It's required a display page configured for the entry.
			AssetEntry entry =null;
			String viewURL ="";
					try{
						entry =	AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
						viewURL =  entry.getAssetRenderer().getURLView(liferayPortletResponse, WindowState.NORMAL); 
						viewURL = entry.getAssetRenderer().getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewURL);
						
					}catch(Exception e){
						e.printStackTrace();
					}
			
			String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
			Document articleDoc = null;
		    try {
		    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
		    } catch (DocumentException e) {
		    } 
			Element root = articleDoc.getRootElement();
		%>
		<liferay-ui:search-container-column-text name=''>
					<div class="table-question-answer-tr">
					<a href="<%=viewURL%>">
					<table class="table table-hover table-question-list">
					
							<tr class="row row-swt-responsive tr-title-question">
								<td colspan="2"><i class="icon-caret-right"></i>
								  <%=HtmlUtil.escape(article.getTitle(themeDisplay.getLocale()))%>
								</td>
							</tr>
							<tr class="tr-date-request">
								<td><%=LanguageUtil.format(request, "nguoi-gui", new Object())%>:
									<%
									for (Element element : root.elements()) {
									%> 
										<%
				 						if (element.attributeValue("name").equals("name")) {
									 	%>						
							  			<%=HtmlUtil.escape(GetterUtil.getString(element.element("dynamic-content").getText()))%>
										<%
										}
										%> 
										<%
		 								}
 										%>
 								</td>
								<td><%=LanguageUtil.format(request, "ngay-gui", new Object())%>:
									<%=DateUtil.getDate(article.getCreateDate(), "dd/MM/yyyy", themeDisplay.getLocale())%>
								</td>
							</tr>
							<tr colspan="2">
								<td class="question-content"><%=LanguageUtil.format(request, "noi-dung", new Object())%>:
									<%
									for (Element element : root.elements()) {
									%> 
										<%
	 									if (element.attributeValue("name").equals("content")) {
	 									%> 
 										<%=HtmlUtil.escape(GetterUtil.getString(element.element("dynamic-content").getText()))%>
									<%
										}
									%> 
									<%
									 	}
									 %>							
 							</td>
							</tr>
							
						</table>
						</a>
					</div>
						
			</liferay-ui:search-container-column-text>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator>
	</liferay-ui:search-iterator>
</liferay-ui:search-container>
</div>

<script type="text/javascript">
	function form_question(){
		$("#swt-questionanswer-template").css("display","none");
		$("#form-question-answer").css("display","block");
	}
	function backListQuestion(){
		$("#swt-questionanswer-template").css("display","block");
		$("#form-question-answer").css("display","none");
	}
</script>
	<%} %>
<!--End Display structure search -->

<!--Start Dispaly list with idArticle -->
	<%if(displayStyle.equals("1")){ 
	%>
	 
	<liferay-portlet:renderURL varImpl="iteratorURL">
	</liferay-portlet:renderURL> 
	<%
	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setParameter("keywords", themeDisplay.getURLCurrent().replace("/web/guest/-/", ""));
	HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest)); 
	String  _mvcPath = httpReq.getParameter("_"+iteratorURL.getPortletId()+"_keywords");
	
	JournalArticle articleByUrlCurrent = null;
	try{
		articleByUrlCurrent =JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), themeDisplay.getURLCurrent().replace("/web/guest/-/", ""));
	}catch(Exception e){
		articleByUrlCurrent =JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), _mvcPath);
		
	}
	if(Validator.isNotNull(articleByUrlCurrent)){
	List<JournalArticle> listFeedbackByArticleCurrent = DDMStructureUtil.getListFeedbackByArticleCurrent(themeDisplay,structure.getStructureKey(), articleByUrlCurrent);
	%>
	<div class="title-list-feedback">
	<%=LanguageUtil.format(request, "list.feedback", new Object()) %>
	</div>
	<liferay-ui:search-container var="searchContainer"  delta="20" deltaConfigurable="true"  iteratorURL="<%=portletURL%>"

	emptyResultsMessage="there-are-no-results">
	 <liferay-ui:search-container-results>
		<%
						results = ListUtil.subList(listFeedbackByArticleCurrent, searchContainer.getStart(), searchContainer.getEnd());
						total = listFeedbackByArticleCurrent.size();
						pageContext.setAttribute("results", results);
						pageContext.setAttribute("total", total);
						searchContainer.setTotal(total);
		%>
	</liferay-ui:search-container-results> 
	<liferay-ui:search-container-row 
		className="com.liferay.journal.model.JournalArticle"
		modelVar="article" indexVar="index" keyProperty="id">
		<%
						// Create the URL for detail view. It's required a display page configured for the entry.
						String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
						Document articleDoc = null;
					    try {
					    	articleDoc = SAXReaderUtil.read(new StringReader(articleContent));
					    } catch (DocumentException e) {
					    } 
								Element root = articleDoc.getRootElement();
					%> 
		<liferay-ui:search-container-column-text name=''>
					<div class="table-question-answer-tr" >
					<table class="table table-hover table-feedback-list">
							
							<tr class="content">
								<td class="name-user-send">
								<span class="tr-date-request">
								<%=LanguageUtil.format(request, "nguoi-gui", new Object())%>:
								</span>
								<%
								for (Element element : root.elements()) {
								%> 
									<%
									 	if (element.attributeValue("name").equals("name")) {
									 %> 
									 <%=GetterUtil.getString(element.element("dynamic-content").getText())%>
									<%
											}
									%>
								<%
		 							}
 								%>
 								</td>
								<td class="date-send">
								<span class="tr-date-request"><%=LanguageUtil.format(request, "ngay-gui", new Object())%>:</span>
									<%=DateUtil.getDate(article.getCreateDate(), "dd/MM/yyyy", themeDisplay.getLocale())%>
								</td>
							</tr>
							<tr class="content">
								<td class="content" colspan="2">
								<span class="tr-date-request">
								<%=LanguageUtil.format(request, "noi-dung", new Object())%>:
								</span>
								<%
									for (Element element : root.elements()) {
								%> 
									<%
									 	if (element.attributeValue("name").equals("content")) {
									 %> 
									 <%=HtmlUtil.escape(element.element("dynamic-content").getText())%>
									<%
										}
									%>
								<%
								 	}
								 %>
								 </td>
							</tr>
						</table>
					</div>
						
			</liferay-ui:search-container-column-text>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator paginate="<%=true%>" type="table">
	</liferay-ui:search-iterator>
	</liferay-ui:search-container>

<% } 
 }
%>
<!--End Dispaly list with idArticle -->

<%
	} else {
%>
<div class="alert alert-danger" role="alert">
	<span class="glyphicon glyphicon-warning-sign"></span>
	<%=LanguageUtil.format(request, "chua-cau-hinh", new Object())%>
</div>

<%
	}
%>