package com.swt.portal.hook.language;

import java.util.Enumeration;
import java.util.ResourceBundle;
import org.osgi.service.component.annotations.Component;
import com.liferay.portal.kernel.language.UTF8Control;

@Component(property = { "language.id=vi_VN" }, service = ResourceBundle.class)
public class VietnameseResourceBundle extends ResourceBundle {

	private final ResourceBundle bundle = ResourceBundle.getBundle("content.Language_vi", UTF8Control.INSTANCE);

	@Override
	protected Object handleGetObject(String key) {
		return bundle.getObject(key);
	}

	@Override
	public Enumeration<String> getKeys() {
		return bundle.getKeys();
	}

}