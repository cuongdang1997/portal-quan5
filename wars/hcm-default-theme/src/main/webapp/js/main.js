AUI().ready(

	/*
	This function gets loaded when all the HTML, not including the portlets, is
	loaded.
	*/

	function() {
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {
		/* Register click handler for portal search button (in header part). */
		$("#button-search-all-in-portal").on("click", searchAllInPortal);
		
		/* Register enter key handler for portal search text-box (in header part). */
		$('#khung-nhap-tim-kiem-id-toan-bo').on("keypress", function(e){
			if (e.which === 13){
				searchAllInPortal();
			}
		});
		
		/* Register click handler for mobile search icon. */
		$("#image_search_navigation_mobile").on("click", function() {
			$("#_com_liferay_portal_search_web_portlet_SearchPortlet_keywords").val("");
			$("form#_com_liferay_portal_search_web_portlet_SearchPortlet_fm").submit();
		});
		
		function searchAllInPortal(){
			var frmSearchPortlet = $("form#_com_liferay_portal_search_web_portlet_SearchPortlet_fm");
			if (frmSearchPortlet.length != 0) {
				var keyword = $.trim($('#khung-nhap-tim-kiem-id-toan-bo').val());
				if (keyword.length == 0) {
					$('#khung-nhap-tim-kiem-id-toan-bo').val("");
					$('#khung-nhap-tim-kiem-id-toan-bo').focus();
					return;
				}
				$("#_com_liferay_portal_search_web_portlet_SearchPortlet_keywords").val(keyword);
				
				var firstCategoryId = $("#swt-search-portlet-first-categoryId").val();
				
				// Set gia tri assetCategoryIds cho lan submit.
				frmSearchPortlet.fm('assetCategoryIds').val(firstCategoryId);
				
				frmSearchPortlet.submit();
			}
		}
		
		$(function(){
			$(window).scroll(function() {
				if ($(this).scrollTop() > 100) {
					$('#goTop').fadeIn();
				} else{
					$('#goTop').fadeOut();
				}
			});
			
			$('#goTop').click(function () {
				$('body,html').animate({scrollTop: 0}, 'slow');
			});
		});
	}
);

function setCookieVisitorCount(cname,cvalue,exTime) {
	var d = new Date();
	var localTime = Number(exTime) + Number(d.getTimezoneOffset()*60*1000*-1);
	d.setTime(localTime);
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookieVisitorCount(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    
    return "";
}

/*
<!-- Facebook -->
(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2';
	  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

*/
