<div id="content-commond-navigation-menu-all-id">
	<div class="hidden-sm hidden-xs main_menu min_wrap">
		<div class="row row-swt-responsive header-menu">
			<div id="navbar" class="col-md-12 col-swt-responsive header-menu-col">
				<div class="row" style="width: 80%;">
				<div class="col-md-12">
				<nav class="${nav_css_class}" id="navigation" role="navigation">
					<h1 class="hide-accessible"><@liferay.language key="navigation" /></h1>

					<ul class="nav-child-father" aria-label="<@liferay.language key="site-pages" />" role="menubar">
						<#list nav_items as nav_item>

							<#assign activeclass = '' />
							<#if nav_item.isSelected()>
								<#assign activeclass = 'active-menu' />
							</#if>

							<#if nav_item?counter == 1>
							<li class="${activeclass}" style="background-image:none;">
							<#else>
								<li class="${activeclass}">
							</#if>
							<a class="nav-child-father-a" href="${nav_item.getURL()}" ${nav_item.getTarget()} data-senna-off="true">
								<#--
                                <#if nav_item.isSelected()>
                                    <a class="nav-child-father-a a-selected-nav" href="${nav_item.getURL()}" ${nav_item.getTarget()} data-senna-off="true">
                                <#else>
                                    <a class="nav-child-father-a" href="${nav_item.getURL()}" ${nav_item.getTarget()} data-senna-off="true">
                                </#if>

                                <# if nav_item?counter == 1>
                                    <span class="nav-home-icon-all"></span>
                                <#else>
                                    <span class="nav-child-icon-all"></span>
                                </#if>
                                -->

								<span>${nav_item.getName()}</span>
							</a>

							<#if nav_item.hasChildren()>
								<ul class="nav-child-menu">
									<#list nav_item.getChildren() as nav_child>
										<li class="child-menu-child-page">
											<a class="child-menu-child-page-a" href="${nav_child.getURL()}" ${nav_child.getTarget()} data-senna-off="true">
												<span>
													${nav_child.getName()}
												</span>
											</a>
											<#if nav_child.hasChildren()>
												<ul class="child-child-menu">
													<#list nav_child.getChildren() as nav_childchild>
														<li class="child-menu-child-child-page">
															<a href="${nav_childchild.getURL()}" ${nav_childchild.getTarget()} data-senna-off="true">
																${nav_childchild.getName()}
															</a>
														</li>
													</#list>
												</ul>
											</#if>
										</li>
									</#list>
								</ul>
							</#if>
							</li>
						</#list>
						<#assign styleSearch = ""/>
						<#if is_signed_in>
							<#assign styleSearch = "margin-left: 6%;"/>
							<li>
								<#assign adminPageUrl = prefsPropsUtil.getString("hcm.sites.admin.vi.url", "") />
								<#if (adminPageUrl?length != 0)>
									<a href="${adminPageUrl}" class="nav-child-father-a" data-senna-off="true">
										<span class="nav-child-icon-all"></span>
										<span><@liferay.language key="category.admin" /></span>

									</a>
								</#if>
							</li>
						</#if>
					</ul>
				</nav>
				</div>
				</div>
				<div class="wp-search" style="${styleSearch}">
					<div class="search header-site-info">
						<input type="search" id="khung-nhap-tim-kiem-id-toan-bo" placeholder="Tìm kiếm...">
						<div id="button-search-all-in-portal" class="button_search"></div>
					</div>
					<div class="d-none">
						<@liferay.search />
					</div>
				</div>
			</div>
		</div>
	</div>

	<#-- ------------------------ -->

</div>

<script>

	$(document).ready(function() {

		$(".drawermenu").drawermenu({
			speed: 100,
			position: "left"
		});


		$('.menu ul li').click(function () {
			$('.menu li').removeClass("active");
			$(this).addClass("active");
		});


		/* Register click handler for portal search button (in header part). */
		$("#button-search-all-in-portal").on("click", searchAllInPortal);

		/* Register enter key handler for portal search text-box (in header part). */
		$('#khung-nhap-tim-kiem-id-toan-bo').on("keypress", function(e){
			if (e.which === 13){
				searchAllInPortal();
			}
		});

		/* Register click handler for mobile search icon. */
		$("#image_search_navigation_mobile").on("click", function() {
			$("#_com_liferay_portal_search_web_portlet_SearchPortlet_keywords").val("");
			$("form#_com_liferay_portal_search_web_portlet_SearchPortlet_fm").attr("data-senna-off", "true");
			$("form#_com_liferay_portal_search_web_portlet_SearchPortlet_fm").submit();
		});
	});

	function searchAllInPortal(){
		var frmSearchPortlet = $("form#_com_liferay_portal_search_web_portlet_SearchPortlet_fm");

		if (frmSearchPortlet.length != 0) {
			frmSearchPortlet.attr("data-senna-off", "true");

			var keyword = $.trim($('#khung-nhap-tim-kiem-id-toan-bo').val());
			if (keyword.length == 0) {
				$("#khung-nhap-tim-kiem-id-toan-bo").val("");
				$("#khung-nhap-tim-kiem-id-toan-bo").focus();
				return;
			}
			$("#_com_liferay_portal_search_web_portlet_SearchPortlet_keywords").val(keyword);

			var firstCategoryId = $("#swt-search-portlet-first-categoryId").val();

			// Set gia tri assetCategoryIds cho request
			frmSearchPortlet.fm('assetCategoryIds').val(escape(firstCategoryId));

			frmSearchPortlet.submit();
		}
	}
</script>
