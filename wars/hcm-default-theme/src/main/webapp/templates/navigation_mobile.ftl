<div class="mobile-menu-container min_wrap" style="display: none;">
		<div class="mobile-menu background-logo-mobile">
			<div class="navbar" style="height: 100px !important;">
				<div class="menu-left col-sm-4 col-xs-4 col-swt drawer-toggle">
					<svg class="hamburger" id="hamburger" y="0px" viewBox="0 0 302 212.9" 
						style="enable-background:new 0 0 302 212.9;" xml:space="preserve">
						<g><rect width="302" height="26.3" /></g>
						<g><rect y="93.3" width="302" height="26.3"/></g>
						<g><rect y="186.7" width="302" height="26.3"/></g>
					</svg>
				</div>
				<span class="logo-mobile-hcm col-sm-4 col-xs-4 col-swt" >
					<a href="${themeDisplay.getURLHome()}" data-senna-off="true">
						<img alt="$logo_description" src="${themeDisplay.getPathThemeImages()}/mobile/logo_mobile.png"/>
					</a>
				</span>
				<span id="image_search_navigation_mobile" class="col-sm-4 col-xs-4 col-swt">
					<img src="${themeDisplay.getPathThemeImages()}/mobile/search_mobile.svg"/>
				</span>
			</div>
			
			<nav class="drawermenu">
				<div class="drawermenu-logo">
					<img src="${themeDisplay.getPathThemeImages()}/mobile/logo_mobile.png">
				</div>
				<ul>
					<#list nav_items as nav_item>
						<#if nav_item.hasChildren()>
							<li>
								<a href="#" class="down" data-senna-off="true">${nav_item.getName()}</a>
								<ul>
									<#list nav_item.getChildren() as nav_child>
										<#if nav_child.hasChildren()>
											<li>
												<a href="#" class="down" data-senna-off="true">${nav_child.getName()}</a>
												<ul style="display: none;">
													<#list nav_child.getChildren() as nav_childchild>
														<li>
															<a href="${nav_childchild.getURL()}" ${nav_childchild.getTarget()} data-senna-off="true">
																${nav_childchild.getName()}
															</a>
														</li>
													</#list>
												</ul>
											</li>
										<#else>
											<li>
												<a href="${nav_child.getURL()}" ${nav_child.getTarget()} data-senna-off="true">
													${nav_child.getName()}
												</a>
											</li>
										</#if>
									</#list>
								</ul>
							</li>
						<#else>
							<li>
								<a href="${nav_item.getURL()}" ${nav_item.getTarget()} data-senna-off="true">
									${nav_item.getName()}
								</a>
							</li>
						</#if>
					</#list>
				</ul>
			</nav>
			
			<div class="drawermenu-overlay">
			</div>
		</div>
	</div>