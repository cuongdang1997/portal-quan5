<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">
<head>
	<title>${the_title} - ${company_name}</title>
	<meta content="initial-scale=1.0, width=device-width" name="viewport" />
		        <meta charset="UTF-8">

	<@liferay_util["include"] page=top_head_include />
	
	<link rel="stylesheet" type="text/css" href="${themeDisplay.getPathThemeCss()}/jquery.es-drawermenu.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.1/jszip.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip-utils/0.0.2/jszip-utils.js"></script>
	<script src="${themeDisplay.getPathThemeJavaScript()}/jquery.es-drawermenu.js"></script>
	<script src="${themeDisplay.getPathThemeJavaScript()}/jquery.nicescroll.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${themeDisplay.getPathThemeCss()}/responsive.css">
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div id="wrapper">
	<header id="banner" role="banner">
		<div id="heading" >
			<#--start-email-hotline-->
			<div class="toolbar_head">
			<div class="header-site-info hidden-xs hidden-sm">
				<div class="toolbar col-md-9" >
					<div  class="top-menusite-nav-header">
						<div id="info_hotline">
							<#assign
                                emailLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.EmailAddressLocalService")
                                phoneLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.PhoneLocalService")
							/>
                            <#assign
                                emails = emailLocalService.getEmailAddresses()
                                phones = phoneLocalService.getPhones()
                            />

							<#if emails?has_content>
								<#assign email = emails[0].getAddress()/>
							<#else>
								<#assign email = "stttt@tphcm.gov.vn"/>
							</#if>

                            <#if phones?has_content>
                                <#assign phone = phones[0].getNumber()/>
                            <#else>
                                <#assign phone = "(028) 3829 5110"/>
                            </#if>
							<a style="margin-right: 10px;" href="/lien-he">
								<img src="${themeDisplay.getPathThemeImages()}/hotline.png">
								Hotline: ${phone}
							</a>
							<a href="mailto:stttt@tphcm.gov.vn">
								<img src="${themeDisplay.getPathThemeImages()}/email.png">
								Email: ${email}
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 login-nav-header">
					<#if themeDisplay.isSignedIn()>
						<#assign user = themeDisplay.getUser() />
						<#assign fullName = user.getFullName(false, false) />
						<span class="user-sign-in">
							<a href="${themeDisplay.getURLSignOut()}">
								<img src="${themeDisplay.getPathThemeImages()}/user/Exit.png" alt="logout" title="Đăng xuất" />
							</a>
						</span>
						<span class="sign-in-notify">
						<#assign user_notification_event_location_service = serviceLocator.findService("com.liferay.portal.kernel.service.UserNotificationEventLocalService") />
							<#assign notification_count = user_notification_event_location_service.getArchivedUserNotificationEventsCount(themeDisplay.getUserId(),false) />
							<#assign notificationMessage = "Bạn không có thông báo mới" />
							<#if (notification_count > 0)>
								<#assign notificationMessage = "Bạn có " + notification_count + " thông báo chưa đọc" />
							</#if>
						<a class="notify-user" title="${notificationMessage}" data-senna-off="true"
						   href="/group${themeDisplay.getControlPanelGroup().getFriendlyURL()}${themeDisplay.getControlPanelLayout().getFriendlyURL()}?p_p_id=com_liferay_notifications_web_portlet_NotificationsPortlet" >
							<img src="${themeDisplay.getPathThemeImages()}/user/Message.png" alt="notify" />
							${notification_count}
						</a>
					    </span>
						<span class="user-sign-in">
							<a class=" icon-user  name-user-login" title="Đi đến trang tài khoản" data-senna-off="true"
							   href="/group${themeDisplay.getControlPanelGroup().getFriendlyURL()}${themeDisplay.getControlPanelLayout().getFriendlyURL()}?p_p_id=com_liferay_my_account_web_portlet_MyAccountPortlet">
								${fullName}
							</a>
						</span>
					<#else>
						<a href="/dang-nhap" id="sign-in" class="icon-user" rel="nofollow">
							<span>Đăng nhập</span>
						</a>
					</#if>
				</div>
			</div>
			<#--end-email-hotline-->
			<div class="mainbar_head">
				<div class="content_head">
				   <div class="logo ">
						<@liferay_portlet["runtime"]
							portletName="com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet"
						/>
					</div>
				</div>

				<#--start-navigation-->
				<div id="content-commond-navigation-menu-all-id">
					<div class="hidden-sm hidden-xs visible-md-block visible-lg-block">
						<div class="col-lg-12 col-swt">
							<#if has_navigation && is_setup_complete>
								<#include "${full_templates_path}/navigation.ftl" />
							</#if>
						</div>
					</div>
					<#--  start-responsive-div-home-max-width-991px  -->
					<#--  end-responsive-div-home-max-width-991px  -->
				</div>
				<#--<div class="col-lg-12 col-swt">
				  <#if has_navigation && is_setup_complete>
						<#include "${full_templates_path}/navigation.ftl" />
				  </#if>
				</div>-->
				<#--end-navigation-->
			</div>
			<#if has_navigation && is_setup_complete>
				<#include "${full_templates_path}/navigation_mobile.ftl" />
			</#if>
		</div>
	</header>

	<div id="content">
		<div class="min_wrap">
			<h1 class="hide-accessible">${the_title}</h1>
			<#assign show_breadcrumbs = getterUtil.getBoolean(theme_settings["show-breadcrumbs"])>
			<#if show_breadcrumbs>
				<nav id="breadcrumbs">
					<@liferay.breadcrumbs />
				</nav>
			</#if>

			<#if selectable>
				<@liferay_util["include"] page=content_include />
			<#else>
				${portletDisplay.recycle()}
				${portletDisplay.setTitle(the_title)}
				<@liferay_theme["wrap-portlet"] page="portlet.ftl">
					<@liferay_util["include"] page=content_include />
				</@>
			</#if>
		</div>
	</div>

  	<footer id="footer">
	<@liferay_portlet["runtime"]
			portletName="com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_footer"
		/>
  	</footer>
</div>
<#--start-navigation-for-mobile-->
<#--<div id="id_display_navigation_for_mobile" style="display: none;">
<#if has_navigation && is_setup_complete>
	<#include "${full_templates_path}/navigation_for_mobile.ftl" />
</#if>
</div>-->
<#--end-navigation-for-mobile-->
<@liferay_util["include"] page=body_bottom_include />
<@liferay_util["include"] page=bottom_include />

<#assign nameCookieVisitorCount = "HCM_PORTAL_VISITOR_COUNT" />
<#assign onlineVisitorCount = "0" />
<#assign expiresVisitor = "0" />
<#assign valueCookieVisitor = "" />
<#assign allVisitorCount = "0" />

<#attempt>
	<#assign visitorTrackerLocalService =  serviceLocator.findService("com.swt.portal.visitor.service.VisitorTrackerLocalService")/>
	
	<#-- Tra ve onlineVisitor[] theo thu tu lan luot la expiresVisitor[0], uid[1], visitorCount[2] -->
	<#assign onlineVisitor = visitorTrackerLocalService.getOnlineVisitorCount(request, nameCookieVisitorCount, company_id, group_id) />
	<#assign expiresVisitor = onlineVisitor[0] />
	<#assign valueCookieVisitor = onlineVisitor[1] />
	<#assign onlineVisitorCount = onlineVisitor[2] />
	
	<#assign allVisitorCount = visitorTrackerLocalService.getAllVisitorCount(company_id, group_id) />
<#recover>
</#attempt>

<script>
	(function ($) {
		$.fn.btrmenu = function (options) {
			// START options
			var settings = $.extend({
				desktopsubmenuicon: false,
				mobileiconpack: "glyphicon",
				mobileopenicon: "glyphicon-chevron-down",
				mobilecloseicon: "glyphicon-chevron-up",
			}, options);
			// END options

			var thisul = this.find('ul').first();
			thisul.addClass("btr-menu");
			var responsive = false;
			if ($(window).width() < 768) {
				responsive = true;
			};
			//Submenu icon
			if (settings.desktopsubmenuicon && !responsive) {
				thisul.children('li').each(function () {
					var thiselement = $(this);
					if (thiselement.has('ul').length) {
						thiselement.find('a').first().append(" <i class=\"caret\"></i>");
					};
					thiselement.find('li').each(function () {
						var subelement = $(this);
						if (subelement.has('ul').length) {
							subelement.find('a').first().append(" <i class=\"caret\"></i>");
						};
					});
				});
			}
			else if (responsive) {
				thisul.find('li').each(function () {
					var thiselement = $(this);
					if (thiselement.has('ul').length) {
						thiselement.addClass('btr-dropdown');
						thiselement.prepend("<span class='" + settings.mobileiconpack + " " + settings.mobileopenicon + " btrsubmenu-btn'></span>");
					}
				});

				$(".btr-menu .btrsubmenu-btn").click(function () {
					var element = $(this);
					var thisli = element.closest("li");
					var thisdropdown = element.closest("li").children("ul");
					var hasactive = false;
					if (!thisdropdown.hasClass("active")) {
						hasactive = true;
					}
					// Clear all active
					thisli.closest('ul').find('ul').removeClass("active");
					if (hasactive) {
						thisdropdown.addClass("active");
					};

					$(".btr-menu .btr-dropdown").each(function () {
						var thisdropdownul = $(this).children('ul');
						var thisdropdownbtn = $(this).children('.btrsubmenu-btn.' + settings.mobileiconpack);
						if (thisdropdownul.hasClass("active") && thisdropdownbtn.hasClass(settings.mobileopenicon)) {
							thisdropdownbtn.removeClass(settings.mobileopenicon).addClass(settings.mobilecloseicon);
						}
						else if (!thisdropdownul.hasClass("active") && thisdropdownbtn.hasClass(settings.mobilecloseicon)) {
							thisdropdownbtn.removeClass(settings.mobilecloseicon).addClass(settings.mobileopenicon);
						}
					});
				});
			}
		};
	}(jQuery));
	$(document).ready(function() {
		$("#navbar").btrmenu();
		updateonlineVisitor();
		updateAllVisitorCount();
	
		$(window).scroll(function() {
			if ($(this).scrollTop() > 100) {
				$("#goTop").fadeIn();
			} else{
				$("#goTop").fadeOut();
			}
		});
		
		$("#goTop").click(function () {
			$("body,html").animate({scrollTop: 0}, "slow");
		});
	});
	
	// Get online visitor count.
	function updateonlineVisitor() {
	    var newLiveUser = ["0", "0", "0", "0", "0"];
	    var html = '';
	
	    //Set cookie value and expires
	    var expiresVisitor = '${expiresVisitor}';
	    setCookieVisitorCount('${nameCookieVisitorCount}', '${valueCookieVisitor}', expiresVisitor);
	
	    //Set online visitor count
	    var strResult = '${onlineVisitorCount}';
	    var arrayStr = strResult.split('');
	
	    if (arrayStr.length < 5) {
	        var maxIndex = 4;
	        for (var i = arrayStr.length; i > 0; i--) {
	            newLiveUser[maxIndex] = arrayStr[i - 1];
	            maxIndex--;
	        }
	        arrayStr = newLiveUser;
	    }
	
	    $.each(arrayStr, function (index, value) {
	        html = html + '<span class="dem-hang1">' + value + '</span>';
	    });
	
	    $(".live-user-count").children("span").remove();
	    $(".live-user-count").append(html);
	}

	// Get all visitor count.
	function updateAllVisitorCount() {
	    var html = '';
	    var newLiveUser = ["0", "0", "0", "0", "0", "0", "0", "0"];
	
	    var allVisitorCount = '${allVisitorCount}';
	    var arrayStr = allVisitorCount.split('');
	
	    if (arrayStr.length < 8) {
	        var maxIndex = 7;
	        for (var i = arrayStr.length; i > 0; i--) {
	            newLiveUser[maxIndex] = arrayStr[i - 1];
	            maxIndex--;
	        }
	        arrayStr = newLiveUser;
	    }
	
	    $.each(arrayStr, function (index, value) {
	        html = html + '<span class="dem-hang1">' + value + '</span>';
	    });
	
	    $(".visitor-count").children("span").remove();
	    $(".visitor-count").append(html);
	}
	
	//Keep header on top
	/*window.onscroll = function() {myFunction()};

	var header = document.getElementById("header");
	var sticky = header.offsetTop;
	
	function myFunction() {
	  if (window.pageYOffset > sticky) {
	    header.classList.add("sticky");
	  } else {
	    header.classList.remove("sticky");
	  }
	}*/
</script>

</body>
</html>