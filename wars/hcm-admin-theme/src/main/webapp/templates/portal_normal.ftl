<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div id="wrapper">
	<header id="header">
            <div id="heading" class="header">
            
            <#if themeDisplay.isSignedIn()>
                <div style="margin-top:5px;" class="toolbar_head">
            <#else>
                <div class="toolbar_head">
            </#if>    
                    <div class="element_toolbar min_wrap">
                         <div class="toolbar dang_nhap col-lg-10"  >
                			<#if themeDisplay.isSignedIn()>
								<#assign user = themeDisplay.getUser() />
								<#assign fullName = user.getFullName(false, false) />
								
								
								<span class="user-sign-in">
									<a href="${themeDisplay.getURLSignOut()}">
										<img src="${themeDisplay.getPathThemeImages()}/user/Exit.png" alt="logout" title="Đăng xuất" />
									</a>
								</span>
								<span class="sign-in-notify">
									<#assign user_notification_event_location_service = serviceLocator.findService("com.liferay.portal.kernel.service.UserNotificationEventLocalService") />
 									<#assign notification_count = user_notification_event_location_service.getArchivedUserNotificationEventsCount(themeDisplay.getUserId(),false) />
									
									<#assign notificationMessage = "Bạn không có thông báo mới" />
									<#if (notification_count > 0)>
										<#assign notificationMessage = "Bạn có " + notification_count + " thông báo chưa đọc" />
									</#if>
									
									<a class="notify-user" title="${notificationMessage}" data-senna-off="true"
										href="/group${themeDisplay.getControlPanelGroup().getFriendlyURL()}${themeDisplay.getControlPanelLayout().getFriendlyURL()}?p_p_id=com_liferay_notifications_web_portlet_NotificationsPortlet" >
										<img src="${themeDisplay.getPathThemeImages()}/user/Message.png" alt="notify" />
										${notification_count}
									</a>
								</span>
								<span class="user-sign-in">
									<a class="name-user-login" title="Đi đến trang tài khoản" data-senna-off="true" 
										href="/group${themeDisplay.getControlPanelGroup().getFriendlyURL()}${themeDisplay.getControlPanelLayout().getFriendlyURL()}?p_p_id=com_liferay_my_account_web_portlet_MyAccountPortlet">	
										<img src="${themeDisplay.getPathThemeImages()}/user/user.svg" alt="user-logo" />
										${fullName}
									</a>
								</span>

							<#else>
								  <a href="/dang-nhap">
		                                <span>
		                                    <img src="${themeDisplay.getPathThemeImages()}/home/user.svg" style="width: 24px; float: right;">
		                                </span>
		                            </a>
							</#if>
                        </div>
                       
                    </div>
                    
                    
                </div>
                <div class="mainbar_head">
                    <div class="content_head min_wrap">
             
                       <div class="col-lg-3 logo ">
	                        <@liferay_portlet["runtime"]
							    portletName="com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_vzVedOOt1WRI"
							/>
						</div>
						<div class="col-lg-9 header-menu-admin">
						    <#if has_navigation && is_setup_complete>
								<#include "${full_templates_path}/navigation.ftl" />
							</#if>		
						</div>				 
                    </div>
             
                </div>
			</div>
		
		
		<div class="wrapper-image-header-footer">
		</div>
     </header>

	<section id="content">
		<h1 class="hide-accessible">${the_title}</h1>

		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>
	
	<div id="goTop">
		<img src="${themeDisplay.getPathThemeImages()}/gototop.svg" alt="Go to top" title="Lên đầu trang" />
	</div>
	
	<footer id="footer" role="contentinfo">

	
	<@liferay_portlet["runtime"]
						    portletName="com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_vzVedOOt2WRI"
						/>
	</footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

</html>