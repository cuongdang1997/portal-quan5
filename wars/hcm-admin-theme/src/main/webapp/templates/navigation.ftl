<div id="content-commond-navigation-menu-all-id">
	<div class="containerdiv hidden-sm hidden-xs visible-md-block visible-lg-block">
		<div class="row">
			<div class="col-md-12">
				<nav class="${nav_css_class}" id="navigation" role="navigation">
					<h1 class="hide-accessible"><@liferay.language key="navigation" /></h1>
				
					<ul class="nav-child-father" aria-label="<@liferay.language key="site-pages" />" role="menubar">
						<#list nav_items as nav_item>
						<#assign activeclass = '' />
							<#if nav_item.isSelected()>
								<#assign activeclass = 'active-menu' />
							</#if>
							<#if nav_item?counter == 1>
								<li class="${activeclass}" style="background-image:none;">
							<#else>
								<li class="${activeclass}">
							</#if>
							
							<#if nav_item.isSelected()>
								<a class="nav-child-father-a a-selected-nav" href="${nav_item.getURL()}" ${nav_item.getTarget()} data-senna-off="true">
							<#else>
								<a class="nav-child-father-a" href="${nav_item.getURL()}" ${nav_item.getTarget()} data-senna-off="true">
							</#if>
								<span>${nav_item.iconURL()} ${nav_item.getName()}</span>
								</a>
							
							<#if nav_item.hasChildren()>
								<ul class="nav-child-menu">
									<#list nav_item.getChildren() as nav_child>
										<li class="child-menu-child-page">
											<a class="child-menu-child-page-a" href="${nav_child.getURL()}" ${nav_child.getTarget()} data-senna-off="true">
												<span style="float: left;width: 90%;overflow: hidden;">
													${nav_child.getName()}
												</span>
												<#if nav_child.hasChildren()>
													<span class="glyphicon glyphicon-chevron-right" style="float: left;width: 10%;"></span>
												</#if>
											</a>
											<#if nav_child.hasChildren()>
												<ul class="child-child-menu">
													<#list nav_child.getChildren() as nav_childchild>
														<li class="child-menu-child-child-page">
															<a href="${nav_childchild.getURL()}" ${nav_childchild.getTarget()} data-senna-off="true">${nav_childchild.getName()}</a>
														</li>
													</#list>
												</ul>
											</#if>
										</li>
									</#list>
								</ul>
							</#if>
						</#list>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>