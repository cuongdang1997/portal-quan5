<style>
    .row-swt-responsive1 {
        margin-right: 0!important;
        margin-left: 0!important;
        display: flex;
        width: 100%;
    }

    .col-swt1 {
        margin-left: 10px;
        padding: 1px 5px!important;
        background-color: white;
    }

    .col-swt-home-layout-right {
        margin-top: 3px;
    }

   /* .portlet-column-first {
        padding-left: 9px;
        float: left;
    }

    .portlet-column-last {
        padding-right: 9px;
        float: left;
    }*/
</style>
<div class="hcm-layout-home-page-quan5" id="main-content" role="main"
     style="width: 80%;margin-left: auto;margin-right: auto;margin-top: 2.5%;">
    <div class="portlet-layout">
        <div class="row row-swt-responsive1">
            <div class="col-xs-12 col-sm-12 col-md-9 col-swt-home-layout-right portlet-column portlet-column-first" id="column-4">
                ${processor.processColumn("column-4", "portlet-column-content portlet-column-content-first")}
            </div>
            <div class="hidden-xs hidden-sm col-md-3 col-swt1 portlet-column portlet-column-last" id="column-5">
                ${processor.processColumn("column-5", "portlet-column-content portlet-column-content-last")}
            </div>
        </div>
    </div>
</div>