<div class="hcm-layout-home-body" id="main-content" role="main">
	<div class="portlet-layout">
		<div class="row row-swt-responsive1">
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-swt portlet-column portlet-column-first" id="column-1">
				${processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")}
			</div>
			<div class="hidden-xs hidden-sm col-md-3 col-lg-3 col-swt portlet-column portlet-column-last" id="column-2">
				${processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")}
			</div>
		</div>
	</div>
</div>