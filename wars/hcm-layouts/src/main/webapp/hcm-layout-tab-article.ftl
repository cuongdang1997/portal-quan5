	<style>
	.tabNameHighLightArticle-center{
		background: #085791;
	    width: 100%;
	    margin: 10px 0px 0;
		
	}
	.tabNameHighLightArticle-top{
	    list-style-type: none;
	    display: flex;
	    padding-top: 24px;
	   
		
	}
	.tabNameHighLightArticle{
		color: #c5c5c5;
	    text-transform: uppercase;
	    font-size: 12px;
	    font-weight: bold;
	    text-decoration: none;
	    margin: 0;
	    padding: 8px;
	    cursor: pointer;
	  
	}
	.tabNameHighLightArticle-active{
		color: #fff;
	    background: #007bff;
	    padding: 8px;
	    text-transform: uppercase;
	    font-size: 12px;
	    font-weight: bold;
	    text-decoration: none;
	    transition: 0.5s;
	    margin: 0;
	    cursor: pointer;
	  
	}
	@media only screen and (max-width: 1023px){
	.tabNameHighLightArticle-center {
	    border-bottom: 2px solid #007bff;
	}
	.tabNameHighLightArticle-active{
		background: #007bff;
	    padding: 4px;
	    width: 50px;
	    height: 40px;
	    border-radius: 3px 3px 0 0;
	    }
	.tabNameHighLightArticle{
		display: block !important;
	    width: 50px;
	    height: 40px;
	    padding: 0 5px;
	  
	}
	.tabNameHighLightArticle img{
	    width: 40px;
	    height: 40px;
	     padding: 4px;
	  
	}
	  
	}
	
	</style>
	
			<div class="article-highlight-right" id="article-highlight-right">
				<div class="tabNameHighLightArticle-center">
				<div class="tabNameHighLightArticle-top min_wrap">
					<div class="tabNameHighLightArticle tabNameHighLightArticle-active" id="tabNameHighLightArticle1" onclick="onpenTabHighlight('1')">
					<img class= "picture1" src=""/>
					</div>
					<div class="tabNameHighLightArticle" id="tabNameHighLightArticle2" onclick="onpenTabHighlight('2')">
					<img class= "picture2" src=""/>
					</div>
					<div class="tabNameHighLightArticle" id="tabNameHighLightArticle3" onclick="onpenTabHighlight('3')">
					<img class= "picture3" src=""/>
					</div>
					<div class="tabNameHighLightArticle" id="tabNameHighLightArticle4" onclick="onpenTabHighlight('4')">
					<img class= "picture4" src=""/>
					</div>
				</div>
				</div>
			
				<div class="portlet-layout">
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-first col-md-12 col-swt" id="tabBodyHighLightArticle1">
							$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
						
					</div>
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-second col-md-12 col-swt" id="tabBodyHighLightArticle2" style="display:none;">
							$processor.processColumn("column-2", "portlet-column-content portlet-column-content-second")
					
					</div>
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-three col-md-12 col-swt" id="tabBodyHighLightArticle3" style="display:none;">
							$processor.processColumn("column-3", "portlet-column-content portlet-column-content-second")
					
					</div>
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-last col-md-12 col-swt" id="tabBodyHighLightArticle4" style="display:none;">
							$processor.processColumn("column-4", "portlet-column-content portlet-column-content-second")
					
					</div>
					
				</div>
				
			</div>
			
		
	<script type="text/javascript">
		$(document).ready(function() {
		var x = screen.width;
		if(x > 1023){
		var namePortlet1 = $('.portlet-column-content-first').find('#namePortlet').val();
		var namePortlet2 = $('.portlet-column-content-second').find('#namePortlet').val();
		var namePortlet3 = $('.portlet-column-content-three').find('#namePortlet').val();
		var namePortlet4 = $('.portlet-column-content-last').find('#namePortlet').val();
    		$("div[id$='tabNameHighLightArticle1']").append(namePortlet1);
    		$("div[id$='tabNameHighLightArticle2']").append(namePortlet2);
    		$("div[id$='tabNameHighLightArticle3']").append(namePortlet3);
    		$("div[id$='tabNameHighLightArticle4']").append(namePortlet4);
		}else{
		var imgCategory1 = $('.portlet-column-content-first').find('#imgCategory').val();
		var imgCategory2 = $('.portlet-column-content-second').find('#imgCategory').val();
		var imgCategory3 = $('.portlet-column-content-three').find('#imgCategory').val();
		var imgCategory4 = $('.portlet-column-content-last').find('#imgCategory').val();
    		$('.picture1').attr('src', imgCategory1);
    		$('.picture2').attr('src', imgCategory2);
    		$('.picture3').attr('src', imgCategory3);
    		$('.picture4').attr('src', imgCategory4);
    		}
		});
		
		function onpenTabHighlight(id) {
			$("div[id$='highlight-article-left']").empty();
			var	tabNameHighLightArticle = document.getElementsByClassName("tabNameHighLightArticle");
			var	tabBodyHighLightArticle = document.getElementsByClassName("tabBodyHighLightArticle");
			for (var i = 1; i < tabNameHighLightArticle.length+1; i++) {
				if(i==id){
				
				$("div[id$='tabNameHighLightArticle"+i+"']").addClass("tabNameHighLightArticle-active");
				}else{
				$("div[id$='tabNameHighLightArticle"+i+"']").removeClass("tabNameHighLightArticle-active");
				}
			}
			for (var i = 1; i < tabBodyHighLightArticle.length+1; i++) {
				if(i==id){
				var carouselLeftAcitve = $("div[id$='tabBodyHighLightArticle"+i+"']").find('#id-article-left').html();
				$("div[id$='highlight-article-left']").append(carouselLeftAcitve);
				$("div[id$='tabBodyHighLightArticle"+i+"']").show();
				}else{
				$("div[id$='tabBodyHighLightArticle"+i+"']").hide();
				}
			}
			
		
		}
	</script>
