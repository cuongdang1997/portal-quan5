<style>
.col-swt-thong-bao{
	padding: 0px !important;
    width: 44%;
}
.col-swt-vanbandientu{
 	width: 28%;
 	padding: 0px !important;
}
.col-swt-motcuadientu{
	width: 28%;
	padding: 0px !important;
   
}
@media only screen and (max-width: 1023px){
    
    .col-swt-thong-bao{
        width: 100% !important;
    }
    .col-swt-vanbandientu{
	 	display: none;
	}
	.col-swt-motcuadientu{
		display: none;
	   
}
    
}
</style>

<div class="columns-3" id="main-content" role="main">
	<div class="portlet-layout row-fluid">
		<div class="col-md-6 portlet-column col-swt-thong-bao portlet-column-first" id="column-1">
			${processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")}
		</div>

		<div class="col-md-3 portlet-column col-swt-vanbandientu" id="column-2">
			${processor.processColumn("column-2", "portlet-column-content")}
		</div>

		<div class="col-md-3 portlet-column col-swt-motcuadientu portlet-column-last" id="column-3">
			${processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")}
		</div>
	</div>
</div>