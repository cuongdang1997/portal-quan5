<div class="hcm-layout-content-body" id="main-content" role="main">
	<div class="portlet-layout">
		<div class="row row-swt-responsive1">
			<div class="hidden-xs hidden-sm col-md-2 col-swt1 col-swt-2 portlet-column portlet-column-first" id="column-1">
				${processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")}
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-swt col-swt-8 portlet-column" id="column-2">
				${processor.processColumn("column-2", "portlet-column-content")}
			</div>
			<div class="hidden-xs hidden-sm col-md-2 col-swt1 col-swt-2 portlet-column portlet-column-last" id="column-3">
				${processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")}
			</div>
		</div>
	</div>
</div>