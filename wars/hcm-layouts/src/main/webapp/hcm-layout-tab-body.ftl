<style>
	.tabNameBodyArticle-center{
		background: #085791;
	    width: 100%;
	    margin-bottom: -1px;
		
	}
	.tabNameBodyArticle-top{
	    list-style-type: none;
	    display: flex;
	    padding-top: 24px;
	    padding-left: 20px;
	   
		
	}
	.tabNameBodyArticle{
		color: #c5c5c5;
	    text-transform: uppercase;
	    font-size: 12px;
	    font-weight: bold;
	    text-decoration: none;
	    margin: 0;
	    padding: 8px;
	    cursor: pointer;
	  
	}
	.tabNameBodyArticle-active{
		color: #fff;
	    background: #007bff;
	    padding: 8px;
	    text-transform: uppercase;
	    font-size: 12px;
	    font-weight: bold;
	    text-decoration: none;
	    transition: 0.5s;
	    margin: 0;
	    cursor: pointer;
	  
	}
	</style>
	
			<div class="article-highlight-right" id="article-highlight-right">
				<div class="tabNameBodyArticle-center">
				<div class="tabNameBodyArticle-top min_wrap">
					<div class="tabNameBodyArticle tabNameBodyArticle-active" id="tabNameBodyArticle1" onclick="onpenTabBody('1')">
					</div>
					<div class="tabNameBodyArticle" id="tabNameBodyArticle2" onclick="onpenTabBody('2')">
					</div>
					<div class="tabNameBodyArticle" id="tabNameBodyArticle3" onclick="onpenTabBody('3')">
					</div>
					<div class="tabNameBodyArticle" id="tabNameBodyArticle4" onclick="onpenTabBody('4')">
					</div>
					<div class="tabNameBodyArticle" id="tabNameBodyArticle5" onclick="onpenTabBody('5')">
					</div>
				</div>
				</div>
			
				<div class="portlet-layout">
					<div class="tabNameArticle portlet-column portlet-column-content-1 col-md-12 col-swt" id="tabNameArticle1">
							$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
					
					</div>
					<div class="tabNameArticle portlet-column portlet-column-content-2 col-md-12 col-swt" id="tabNameArticle2" style="display:none;">
							$processor.processColumn("column-2", "portlet-column-content portlet-column-content-second")
					
					</div>
					<div class="tabNameArticle portlet-column portlet-column-content-3 col-md-12 col-swt" id="tabNameArticle3" style="display:none;">
							$processor.processColumn("column-3", "portlet-column-content portlet-column-content-second")
					
					</div>
					<div class="tabNameArticle portlet-column portlet-column-content-4 col-md-12 col-swt" id="tabNameArticle4" style="display:none;">
							$processor.processColumn("column-4", "portlet-column-content portlet-column-content-second")
					
					</div>
					<div class="tabNameArticle portlet-column portlet-column-content-5 col-md-12 col-swt" id="tabNameArticle5" style="display:none;">
							$processor.processColumn("column-5", "portlet-column-content portlet-column-content-second")
					
					</div>
					
				</div>
				
			</div>
			
		
	<script type="text/javascript">
		$(document).ready(function() {
		var namePort1 = $('.portlet-column-content-1').find('#namePortlet').val();
		var namePort2 = $('.portlet-column-content-2').find('#namePortlet').val();
		var namePort3 = $('.portlet-column-content-3').find('#namePortlet').val();
		var namePort4 = $('.portlet-column-content-4').find('#namePortlet').val();
		var namePort5 = $('.portlet-column-content-5').find('#namePortlet').val();
    		$("div[id$='tabNameBodyArticle1']").append(namePort1);
    		$("div[id$='tabNameBodyArticle2']").append(namePort2);
    		$("div[id$='tabNameBodyArticle3']").append(namePort3);
    		$("div[id$='tabNameBodyArticle4']").append(namePort4);
    		$("div[id$='tabNameBodyArticle5']").append(namePort5);
		
    		
		});
		
		function onpenTabBody(id) {
			$("div[id$='highlight-article-left']").empty();
			var	tabNameBodyArticle = document.getElementsByClassName("tabNameBodyArticle");
			var	tabNameArticle = document.getElementsByClassName("tabNameArticle");
			for (var i = 1; i < tabNameBodyArticle.length+1; i++) {
				if(i==id){
				
				$("div[id$='tabNameBodyArticle"+i+"']").addClass("tabNameBodyArticle-active");
				}else{
				$("div[id$='tabNameBodyArticle"+i+"']").removeClass("tabNameBodyArticle-active");
				}
			}
			for (var i = 1; i < tabNameArticle.length+1; i++) {
				if(i==id){
				var carouselLeftAcitve = $("div[id$='tabNameArticle"+i+"']").find('#id-article-left').html();
				$("div[id$='highlight-article-left']").append(carouselLeftAcitve);
				$("div[id$='tabNameArticle"+i+"']").show();
				}else{
				$("div[id$='tabNameArticle"+i+"']").hide();
				}
			}
			
		
		}
	</script>
