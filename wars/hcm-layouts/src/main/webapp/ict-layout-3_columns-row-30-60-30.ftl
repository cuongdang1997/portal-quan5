<div class="columns-3" id="main-content" role="main">
	<div class="portlet-layout row-fluid">
		<div class="col-md-3 hidden-xs hidden-sm col-swt-3 portlet-column portlet-column-first" id="column-1">
			${processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")}
		</div>

		<div class="col-md-6 col-sm-12 col-xs-12 col-swt-6 portlet-column" id="column-2">
			${processor.processColumn("column-2", "portlet-column-content")}
		</div>

		<div class="col-md-3 hidden-xs hidden-sm col-swt-3 col-swt portlet-column portlet-column-last" id="column-3">
			${processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")}
		</div>
	</div>
</div>