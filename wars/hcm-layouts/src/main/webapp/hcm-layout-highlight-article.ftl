	<style>
	.article-highlight-right{
		float: left;
	    width: 98.5%;
	    min-height: 324px;
	    border: 1px solid #d1d1d2;
	    overflow: hidden;
	    margin-left: 5px;
	}
	.tabNameHighLightArticle-top{
		float: left;
	    width: 100%;
	    height: 36px;
	    background-repeat: no-repeat;
	    background-size: cover;
	    background-position: 0px;
	    padding-left: 10px;
	    box-shadow: inset 0px -1px 1px 0px #d1d1d1;
	}
	.tabNameHighLightArticle{
	    float: left;
	    height: 17px;
	    text-align: center;
	    color: gray;
	    cursor: pointer;
	    margin-top: 15px;
	    padding: 0px 12px;
	    width: auto;
	    font-size: 12px;
	    font-weight: bold;
	}
	.tabNameHighLightArticle-active{
	  	position: relative;
	    color: #c10100;
	    background-color: white;
	    border-bottom: 1px solid white;
	    border-left: 1px solid #d1d1d2;
	    border-right: 1px solid #d1d1d2;
	    height: 30px;
	    line-height: 26px;
	    border-top-left-radius: 7px;
	    border-top-right-radius: 7px;
	    font-weight: bold;
	    margin-top: 6px;
	    font-family: Arial, Tahoma;
	    font-size: 12px;
	    padding-top: 3px;
	    box-shadow: 0px -1px 0px 0px #c3bebe;
	}
	</style>
	<div class="content row row-swt-responsive" id="main-content" role="main">
		
		<div class="col-sm-7 col-swt col-xs-12">
			<div id="highlight-article-left">
			
			</div>
		</div>
		
		<div class="col-sm-5 col-swt col-xs-12">
			
			<div class="article-highlight-right" id="article-highlight-right">
				
				<div class="tabNameHighLightArticle-top">
					<div class="tabNameHighLightArticle tabNameHighLightArticle-active" id="tabNameHighLightArticle1" onclick="onpenTabHighlight('1')">
					</div>
					<div class="tabNameHighLightArticle" id="tabNameHighLightArticle2" onclick="onpenTabHighlight('2')">
					</div>
					<div class="tabNameHighLightArticle" id="tabNameHighLightArticle3" onclick="onpenTabHighlight('3')">
					</div>
				</div>
			
				<div class="portlet-layout">
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-first col-md-12 col-swt" id="tabBodyHighLightArticle1">
							$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
					
					</div>
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-second col-md-12 col-swt" id="tabBodyHighLightArticle2" style="display:none;">
							$processor.processColumn("column-2", "portlet-column-content portlet-column-content-second")
					
					</div>
					<div class="tabBodyHighLightArticle portlet-column portlet-column-content-last col-md-12 col-swt" id="tabBodyHighLightArticle3" style="display:none;">
							$processor.processColumn("column-3", "portlet-column-content portlet-column-content-second")
					
					</div>
					
				</div>
				
			</div>
			
		</div>
		
		
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
		var namePortlet1 = $('.portlet-column-content-first').find('#namePortlet').val();
		var namePortlet2 = $('.portlet-column-content-second').find('#namePortlet').val();
		var namePortlet3 = $('.portlet-column-content-last').find('#namePortlet').val();
    		$("div[id$='tabNameHighLightArticle1']").append(namePortlet1);
    		$("div[id$='tabNameHighLightArticle2']").append(namePortlet2);
    		$("div[id$='tabNameHighLightArticle3']").append(namePortlet3);
		var carouselLeft = $('.portlet-column-content-first').find('#id-article-left').html();
		$("div[id$='highlight-article-left']").append(carouselLeft);
    		
		});
		
		function onpenTabHighlight(id) {
			$("div[id$='highlight-article-left']").empty();
			var	tabNameHighLightArticle = document.getElementsByClassName("tabNameHighLightArticle");
			var	tabBodyHighLightArticle = document.getElementsByClassName("tabBodyHighLightArticle");
			for (var i = 1; i < tabNameHighLightArticle.length+1; i++) {
				if(i==id){
				
				$("div[id$='tabNameHighLightArticle"+i+"']").addClass("tabNameHighLightArticle-active");
				}else{
				$("div[id$='tabNameHighLightArticle"+i+"']").removeClass("tabNameHighLightArticle-active");
				}
			}
			for (var i = 1; i < tabBodyHighLightArticle.length+1; i++) {
				if(i==id){
				var carouselLeftAcitve = $("div[id$='tabBodyHighLightArticle"+i+"']").find('#id-article-left').html();
				$("div[id$='highlight-article-left']").append(carouselLeftAcitve);
				$("div[id$='tabBodyHighLightArticle"+i+"']").show();
				}else{
				$("div[id$='tabBodyHighLightArticle"+i+"']").hide();
				}
			}
			
		
		}
	</script>
