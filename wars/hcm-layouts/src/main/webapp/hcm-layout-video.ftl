<style>
.content-video{
	width: 980px;
	margin: 0 auto;
 }
</style>

<div class="hcm-layout--video" id="main-content" role="main">
	<div class="portlet-layout">
		<div class="row row-swt-responsive1">
			<div class="portlet-column portlet-column-only col-md-12 col-swt col-lg-12  col-xs-12 col-sm-12" id="column-1">
				${processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")}
			</div>
		</div>
		<div class="row row-swt-responsive1">
			
			<div class="content-video"  portlet-column" id="column-2">
				${processor.processColumn("column-2", "portlet-column-content")}
			</div>
			
		</div>
	</div>
</div>