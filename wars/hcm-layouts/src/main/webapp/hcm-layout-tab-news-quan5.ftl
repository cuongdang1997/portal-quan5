<style>
    .tabNameNews-center{
        /* background: #085791; */
        width: 100%;
        margin: 10px 0px 0;
    }
    .tabNameNews-top{
        list-style-type: none;
        display: flex;
        /* padding-top: 24px; */
    }
    .tabNameNews{
        color: #c5c5c5;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
        text-decoration: none;
        margin: 0;
        /* padding: 8px; */
        cursor: pointer;
        background: #999999;
        border-radius: 10px 10px 0px 0px;
    }
    .tabNameNews-active{
        background: linear-gradient(#d9251c, #970b04);
        color: white;
        border-radius: 10px 10px 0px 0px;
        cursor: pointer;
    }

    .tabNameNews span {
        font-weight: bold;
        font-size: 13px;
        float: left;
        border-right: white 0.5px solid;
        color: #ffffff;
        outline: none;
        cursor: pointer;
        padding: 9px 7px;
        transition: 0.3s;
        border-radius: 10px 10px 0px 0px;
    }

    .tabNameNews  span:hover {
        background: linear-gradient(#d9251c, #970b04);
        border-radius: 10px 10px 0px 0px;
    }

    .menu-cms-chosen {
        z-index: 3;
        float: left;
        width: 100%;
        position: absolute;
        height: 100%;
        margin-top: -5px;
        display: none;
    }

    .menu-cms-chosen ul li {
        list-style: none;
        float: right;
        width: 100%;
        padding: 5px 8px 6px 0;
        font-size: 12px;
        font-weight: bold;
        border-bottom: 1px solid #dad6d6;
    }

    .padding-select-ul-chosen {
        list-style: none;
        text-align: right;
        z-index: 11;
        background: #e3f7ff;
        position: absolute;
        float: right;
        width: 50%;
        border-right: 1px solid #d8d6d6;
        margin: 0 0 0 50%;
        box-shadow: -2px 1px 10px -4px black;
    }

    @media only screen and (max-width: 767px) {
        .padding-select{
            padding: 9px 7px;
            transition: .3s;
            text-transform: uppercase;
            color: white;
            background-color: #f30a0a;
            font-weight: bold;
        }

        .tab-snv{
            display: none;
        }

        .swt-select-responsive1{
            margin-bottom: 2%!important;
            padding-right: 0;
            display: block;
            width: 100%;
            padding-left: 0;
            z-index: 4;
        }
    }

    @media only screen and (min-width: 768px) {
        .menu-cms-chosen{
            display: none !important;
        }
    }
</style>

<div class="article-highlight-right" id="article-highlight-right">
    <div class="tabNameNews-center">
        <div class="tabNameNews-top min_wrap tab-snv">
            <div class="tabNameNews tabNameNews-active" id="tabNameNews1" onclick="onpenTabHighlight('1')">
                <span class="tablinks">Bản tin quận</span>
            </div>
            <div class="tabNameNews" id="tabNameNews2" onclick="onpenTabHighlight('2')">
                <span class="tablinks">Thông tin tuyên truyền</span>
            </div>
            <div class="tabNameNews" id="tabNameNews3" onclick="onpenTabHighlight('3')">
                <span class="tablinks">Thông tin cần biết</span>
            </div>
            <div class="tabNameNews" id="tabNameNews4" onclick="onpenTabHighlight('4')">
                <span class="tablinks">Học tập và làm theo đạo đức HCM</span>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 hidden-md hidden-sm hidden-lg swt-select-responsive1">
            <div class="padding-select">
                <span class="tablinks" id="tab_title">Bản tin Quận</span>
                <span style="float: right;" class="glyphicon glyphicon-menu-hamburger" onclick="moFormMenuChosen()"></span>
            </div>
        </div>

        <div class="row row-swt-responsive" style="float:left;width:100%;" id="stttt-khung-cha-id-">
            <div class="menu-cms-chosen">
                <ul class="padding-select-ul-chosen" id="chonTabHienThi">
                    <li class="tablinks" onclick="onpenTabHighlight('1')">Bản tin Quận</li>
                    <li class="tablinks" onclick="onpenTabHighlight('2')">Thông tin tuyên truyền</li>
                    <li class="tablinks" onclick="onpenTabHighlight('3')">Thông tin cần biết</li>
                    <li class="tablinks" onclick="onpenTabHighlight('4')">Học tập và làm theo đạo đức HCM</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="portlet-layout">
        <div class="tabBodyNews portlet-column portlet-column-content-first col-md-12 col-swt" id="tabBodyNews1">
            $processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
        </div>
        <div class="tabBodyNews portlet-column portlet-column-content-second col-md-12 col-swt" id="tabBodyNews2" style="display:none;">
            $processor.processColumn("column-2", "portlet-column-content portlet-column-content-second")
        </div>
        <div class="tabBodyNews portlet-column portlet-column-content-three col-md-12 col-swt" id="tabBodyNews3" style="display:none;">
            $processor.processColumn("column-3", "portlet-column-content portlet-column-content-second")
        </div>
        <div class="tabBodyNews portlet-column portlet-column-content-last col-md-12 col-swt" id="tabBodyNews4" style="display:none;">
            $processor.processColumn("column-4", "portlet-column-content portlet-column-content-second")
        </div>
    </div>
</div>

<script type="text/javascript">
    function moFormMenuChosen(){
        $(".menu-cms-chosen").slideToggle();
    }

    function onpenTabHighlight(id) {
        $(".news-quan5").niceScroll();
        $("div[id$='highlight-article-left']").empty();
        var	tabNameNews = document.getElementsByClassName("tabNameNews");
        var	tabBodyNews = document.getElementsByClassName("tabBodyNews");
        for (var i = 1; i < tabNameNews.length+1; i++) {
            if(i==id){
                $("div[id$='tabNameNews"+i+"']").addClass("tabNameNews-active");
            }else{
                $("div[id$='tabNameNews"+i+"']").removeClass("tabNameNews-active");
            }
        }
        for (var i = 1; i < tabBodyNews.length+1; i++) {
            if(i==id){
                var carouselLeftAcitve = $("div[id$='tabBodyNews"+i+"']").find('#id-article-left').html();
                $("div[id$='highlight-article-left']").append(carouselLeftAcitve);
                $("div[id$='tabBodyNews"+i+"']").show();
            }else{
                $("div[id$='tabBodyNews"+i+"']").hide();
            }
        }
    }
</script>